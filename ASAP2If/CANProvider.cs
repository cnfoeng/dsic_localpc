﻿/*!
 * @file    
 * @brief   Implements a base class for vendor specific CAN providers.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    April 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace jnsoft.Comm.CAN
{
  /// <summary>
  /// Implements a base class for a vendor specific CAN provider.
  /// 
  /// - Provides access to a single CAN Bus channel.
  /// - Provides functionality for registering callbacks for multiple CAN clients.
  /// - Handles multiple XCPonCAN/CCP clients on a single CANProvider instance.
  /// - Decouples the CAN receive thread from notifying registered clients in case not to 
  /// block receiving date from the CANProvider.
  /// </summary>
  public abstract class CANProvider : ICANProvider
  {
    #region local types
    /// <summary>
    /// Provides a list of CAN notify handlers.
    /// </summary>
    sealed class HandlerList : List<EventHandler<CANEventArgs>> { }
    /// <summary>
    /// Provides a dictionary from CAN ID to a HandlerList.
    /// 
    /// The key UINT32.MaxValue maps to a handler list receiving all received CAN messages.
    /// </summary>
    sealed class NotifyHandlerDict : Dictionary<UInt32, HandlerList> { }
    #endregion
    #region members
    /// <summary>Allowed ID mask for standard CAN IDs</summary>
    public const UInt32 CAN_STD_ID_MASK = 0x7ff;
    /// <summary>Allowed ID mask for extended CAN IDs</summary>
    public const UInt32 CAN_EXT_ID_MASK = 0x1fffffff;
    /// <summary>Extended CAN Id flag</summary>
    public const UInt32 CAN_EXT_FLAG = 0x80000000;
    /// <summary>Maximum message size CAN in bytes</summary>
    public const int MAX_DLC = 8;
    /// <summary>Maximum message size CANFD in bytes</summary>
    public const int MAX_FD_DLC = MAX_DLC * 8;
    const UInt32 NI_CAN_EXT_FLAG = 0x20000000;
    static int mNextBusID;
    int mUniqueBusID;
    /// <summary>
    /// Holds the current CAN configuration.
    /// </summary>
    protected CANConfiguration mConfiguration;
    ManualResetEvent mNotifyEvent = new ManualResetEvent(false);
    volatile bool mIsActivated;
    ManualResetEvent mCancelEvent = new ManualResetEvent(false);
    string mName;
    Thread mReceiveThread;
    Thread mNotifyThread;
    NotifyHandlerDict mNotifyHandlerDict = new NotifyHandlerDict();
    List<CANFrame> mNotifyFramesList = new List<CANFrame>();
    long mLastBusloadQuery = -1;
    long mBitsTransferred;
    long mMsgTransferred;
    #endregion
    #region constructors
    /// <summary>
    /// Initialises a new instance of CANProvider.
    /// </summary>
    /// <param name="canProviderName">A unique name for the CAN provider instance</param>
    protected CANProvider(string canProviderName)
    {
      mName = canProviderName;
      mUniqueBusID = ++mNextBusID;
    }
    #endregion
    #region properties
    public int UniqueBusID { get { return mUniqueBusID; } }
    #endregion
    #region methods
    public abstract bool isAvailable();

    public abstract bool configure(IWin32Window hWnd, CANConfiguration configuration);

    public abstract bool open(CANConfiguration configuration);

    public abstract void close();

    public abstract int send(UInt32 canID, ref byte[] data);

    public abstract int onReceive(out CANFrame frame);

    void ICANProvider.getBusload(out double bitsPerSec, out int msgPerSec)
    {
      bitsPerSec = msgPerSec = 0;
      if (!mIsActivated)
        return;
      if (mLastBusloadQuery == -1)
      { // first call
        mLastBusloadQuery = DateTime.Now.Ticks;
        return;
      }
      long now = DateTime.Now.Ticks;
      double diffSecs = TimeSpan.FromTicks(now - mLastBusloadQuery).TotalSeconds;
      bitsPerSec = (double)mBitsTransferred / diffSecs;
      msgPerSec = (int)(Math.Round((double)mMsgTransferred / diffSecs));
      mMsgTransferred = mBitsTransferred = 0;
      mLastBusloadQuery = now;
    }

    /// <summary>
    /// Starts the CANProvider.
    /// </summary>
    protected void start()
    {
      if (mIsActivated)
        return;
      lock (this)
      {
        mCancelEvent.Reset();
        mNotifyEvent.Reset();
        mLastBusloadQuery = -1;
        mBitsTransferred = mMsgTransferred = 0;
        lock (mNotifyFramesList)
          mNotifyFramesList.Clear();

        // Create the one and only notification thread per CAN provider
        mNotifyThread = new Thread(new ThreadStart(notifyThread));
        mNotifyThread.IsBackground = true;
        mNotifyThread.Name = $"{mName} notify";
        // Create the one and only receiver thread per CAN provider
        mReceiveThread = new Thread(new ThreadStart(receiveThread));
        mReceiveThread.IsBackground = true;
        mReceiveThread.Name = $"{mName} rcv";
        // start the receive and notifier thread
        mNotifyThread.Start();
        mReceiveThread.Start();
        mIsActivated = true;
      }
    }

    /// <summary>
    /// Stops the CANProvider.
    /// </summary>
    protected void stop()
    {
      if (!mIsActivated)
        return;

      // stop the threads
      mCancelEvent.Set();

      Thread.Sleep(10);

      mReceiveThread = mNotifyThread = null;

      lock (mNotifyFramesList)
        // clear outstanding notifications
        mNotifyFramesList.Clear();
      mIsActivated = false;
    }

    /// <summary>
    /// Called when a frame has sent to the CAN Bus.
    /// </summary>
    /// <param name="frame">The CAN frame sent</param>
    /// <returns>The data length of the frame sent</returns>
    protected int hasSent(CANFrame frame)
    {
      ++mMsgTransferred;
      mBitsTransferred += frame.getRawFrameLength();
      CANFrameQueue.Instance.enqueueFrame(frame, this);
      return frame.Data.Length;
    }

    void ICANProvider.registerClient(EventHandler<CANEventArgs> handler)
    {
      ((ICANProvider)this).registerClient(UInt32.MaxValue, handler);
    }

    void ICANProvider.registerClient(UInt32 canID, EventHandler<CANEventArgs> handler)
    {
      lock (mNotifyFramesList)
        lock (mNotifyHandlerDict)
        { // Adds the specified CAN listener handler
          HandlerList handlerList;
          if (!mNotifyHandlerDict.TryGetValue(canID, out handlerList))
          { // create receiver list
            handlerList = new HandlerList();
            mNotifyHandlerDict[canID] = handlerList;
          }
          // add handler
          handlerList.Add(handler);
        }
    }

    void ICANProvider.registerClient(UInt32[] canIds, EventHandler<CANEventArgs> handler)
    {
      for (int i = canIds.Length - 1; i >= 0; --i)
        ((ICANProvider)this).registerClient(canIds[i], handler);
    }

    void ICANProvider.registerXCPCCPClient(UInt32[] canXCPCCPIds, EventHandler<CANEventArgs> handler)
    {
      for (int i = canXCPCCPIds.Length - 1; i >= 0; --i)
        ((ICANProvider)this).registerClient(canXCPCCPIds[i], handler);
    }

    void ICANProvider.unregisterClient(EventHandler<CANEventArgs> handler)
    {
      lock (mNotifyFramesList)
      {
        lock (mNotifyHandlerDict)
        { // removes all instances of the specified handler
          var en = mNotifyHandlerDict.GetEnumerator();
          while (en.MoveNext())
          {
            var handlerList = en.Current.Value;
            for (int i = handlerList.Count - 1; i >= 0; --i)
              if (handlerList[i] == handler)
                handlerList.RemoveAt(i);
          }
        }
      }
    }

    /// <summary>
    /// The CAN receive thread.
    /// 
    /// - Stops only if the thread is aborted from disposing the instance!
    /// - Ay exception thrown from the registered CAN handler is caught!
    /// </summary>
    void receiveThread()
    {
      var localList = new List<CANFrame>();
      while (true)
      {
        // read sent and received frames
        CANFrame frame;
        while (0 == ((ICANProvider)this).onReceive(out frame) && frame != null)
        { // add received can frames only if no error is received and the returned frame is valid
          CANFrameQueue.Instance.enqueueFrame(frame, this);
          if (!frame.IsMasterFrame)
          {
            ++mMsgTransferred;
            mBitsTransferred += frame.getRawFrameLength();
          }
          localList.Add(frame);
        }

        if (localList.Count == 0)
        {
          if (mCancelEvent.WaitOne(1))
            break;
          continue;
        }

        lock (mNotifyFramesList)
        {
          mNotifyFramesList.AddRange(localList);
          // set signal for notify thread
          mNotifyEvent.Set();
        }
        localList.Clear();
      }
    }

    /// <summary>
    /// The CAN notify thread.
    /// 
    /// - Stops only if the thread is aborted from disposing the instance!
    /// - Any exceptions thrown from the registered CAN handler is caught!
    /// </summary>
    void notifyThread()
    {
      var localList = new List<CANFrame>();
      var events = new WaitHandle[] { mCancelEvent, mNotifyEvent };
      int idx;
      while ((idx = WaitHandle.WaitAny(events)) > 0)
      {
        if (idx == 1)
        {
          lock (mNotifyFramesList)
          { // copy references of all frames received
            localList.AddRange(mNotifyFramesList);
            mNotifyFramesList.Clear();
            mNotifyEvent.Reset();
          }

          lock (mNotifyHandlerDict)
          { // synchronize handler dictionary
            if (mNotifyHandlerDict.Count > 0)
            {
              for (int j = 0; j < localList.Count; ++j)
              {
                var frame = localList[j];
                var args = new CANEventArgs(frame);

                HandlerList handlerList;
                if (mNotifyHandlerDict.TryGetValue(frame.ID, out handlerList))
                { // CAN ID specific handlers found
                  for (int i = handlerList.Count - 1; i >= 0; --i)
                  { // call the CAN ID listener
                    try { handlerList[i](this, args); }
                    catch { }
                  }
                }
                if (mNotifyHandlerDict.TryGetValue(uint.MaxValue, out handlerList))
                { // Handlers listening for any CAN message
                  for (int i = handlerList.Count - 1; i >= 0; --i)
                  { // call the CAN listener
                    try { handlerList[i](this, args); }
                    catch { }
                  }
                }
              }
            }
            localList.Clear();
          }
        }
      }
    }

    /// <summary>
    /// Gets a UINT64 value from a CAN data message.
    /// </summary>
    /// <param name="data">The CAN payload data</param>
    /// <returns>The value</returns>
    public static UInt64 getDataFromArray(ref byte[] data)
    {
      UInt64 result = 0;
      for (int i = data.Length - 1; i >= 0; --i)
      {
        result |= data[i];
        if (i > 0)
          result <<= 8;
      }
      return result;
    }

    /// <summary>
    /// Returns the name of the CAN provider.
    /// </summary>
    /// <returns>The name of the CAN provider</returns>
    public override string ToString() { return mName; }

    /// <summary>
    /// Converts a CAN ID into a string in hexdecimal notation.
    /// 
    /// - The extended CAN ID Flag will be returned as postfixed (X).
    /// </summary>
    /// <param name="canID">The CAN ID to convert</param>
    /// <returns>The CAN ID as string representation</returns>
    public static string toCANIDString(UInt32 canID)
    {
      return $"{(canID & ~CANProvider.CAN_EXT_FLAG):X}{((canID & CANProvider.CAN_EXT_FLAG) > 0 ? "(X)" : string.Empty)}";
    }

    /// <summary>
    /// Converts a CAN ID pair into a string in hexdecimal notation.
    /// 
    /// - The extended CAN ID Flags will be returned as postfixed (X).
    /// </summary>
    /// <param name="canID1">The first CAN ID to convert</param>
    /// <param name="canID2">The second CAN ID to convert</param>
    /// <returns>The CAN ID pair as string representation</returns>
    public static string toCANIDPairString(UInt32 canID1, UInt32 canID2)
    {
      return $"{toCANIDString(canID1)}/{toCANIDString(canID2)}";
    }

    /// <summary>
    /// Sorts CAN Frames according its ID (ascending).
    /// </summary>
    /// <param name="a">first CAN ID</param>
    /// <param name="b">second CAN ID</param>
    /// <returns></returns>
    public static int CANIDSort(UInt32 a, UInt32 b)
    {
      UInt32 idA = a & ~CANProvider.CAN_EXT_FLAG;
      UInt32 idB = b & ~CANProvider.CAN_EXT_FLAG;
      bool aExtended = (a & CANProvider.CAN_EXT_FLAG) > 0;
      bool bExtended = (b & CANProvider.CAN_EXT_FLAG) > 0;
      int result = idA.CompareTo(idB);
      if (result == 0)
        result = aExtended.CompareTo(bExtended);
      return result;
    }

    /// <summary>
    /// Converts a length to a DLC byte.
    /// 
    /// - Typically used for CAN FD frames.
    /// </summary>
    /// <param name="length">Length to send (e.g. in a CAN FD frame)</param>
    /// <returns>DLC byte to set</returns>
    /// <exception cref="NotSupportedException">Thrown for all unsupported length values</exception>
    public static byte lengthToDLC(int length)
    {
      byte result = (byte)length;
      if (result > 8)
      {
        if (length <= 12) result = 9;
        else if (length <= 16) result = 10;
        else if (length <= 20) result = 11;
        else if (length <= 24) result = 12;
        else if (length <= 32) result = 13;
        else if (length <= 48) result = 14;
        else if (length <= 64) result = 15;
        else throw new NotSupportedException($"Length {length} is not allowed in a CAN frame.");
      }
      return result;
    }

    /// <summary>
    /// Converts the DLC byte to a length.
    /// 
    /// Typically used for CAN FD frames.
    /// </summary>
    /// <param name="dlc">A CAN DLC byte</param>
    /// <returns>The length of a CAN frame</returns>
    /// <exception cref="NotSupportedException">Thrown for all unsupported DLC values</exception>
    public static byte dlcToLength(byte dlc)
    {
      if (dlc <= 8)
        return (byte)dlc;
      byte result = dlc;
      switch (dlc)
      {
        case 15: result = 64; break;
        case 14: result = 48; break;
        case 13: result = 32; break;
        case 12: result = 24; break;
        case 11: result = 20; break;
        case 10: result = 16; break;
        case 09: result = 12; break;
        default: throw new NotSupportedException($"Invalid DLC {dlc} for a CAN Frame");
      }
      return result;
    }

    /// <summary>
    /// Gets the required CAN (FD) message length from the specified payload size.
    /// </summary>
    /// <param name="payloadSize">payload size to send</param>
    /// <param name="forceMaxDLC">For payloads smaller than 8, false is allowed</param>
    /// <returns>allowed legth to send</returns>
    public static int getSendMsgLen(int payloadSize, bool forceMaxDLC = true)
    {
      int result = forceMaxDLC ? 8 : payloadSize;
      if (payloadSize > 8)
      { // CAN FD
        if (payloadSize > 48) result = 64;
        else if (payloadSize > 32) result = 48;
        else if (payloadSize > 24) result = 32;
        else if (payloadSize > 20) result = 24;
        else if (payloadSize > 16) result = 20;
        else if (payloadSize > 12) result = 16;
        else result = 12;
      }
      return result;
    }

    /// <summary>
    /// Replace the standard extended mark (Bit 31, zero based) with the special NI CAN extended mark (Bit 29, zero based).
    /// </summary>
    /// <param name="id"></param>
    /// <returns>The CAN Id in NI CAN format</returns>
    public static UInt32 ToNICANId(UInt32 id)
    {
      return ((id & CANProvider.CAN_EXT_FLAG) > 0 ? id | NI_CAN_EXT_FLAG : id) & ~CANProvider.CAN_EXT_FLAG;
    }

    /// <summary>
    /// Replace the special NI CAN extended mark (Bit 29, zero based) with the standard extended mark (Bit 31, zero based).
    /// </summary>
    /// <param name="id"></param>
    /// <returns>The CAN Id in standard CAN format</returns>
    public static UInt32 FromNICANId(UInt32 id)
    {
      return ((id & NI_CAN_EXT_FLAG) > 0 ? id | CANProvider.CAN_EXT_FLAG : id) & ~NI_CAN_EXT_FLAG;
    }
    #endregion
    #region IDisposable Members
    /// <summary>
    /// Disposes all used resources from the instance.
    /// 
    /// - The receiver thread
    /// - The notifier thread
    /// </summary>
    public virtual void Dispose()
    {
      ((ICANProvider)this).close();
      stop();
      mNotifyEvent?.Dispose();
      mCancelEvent?.Dispose();
    }
    #endregion
  }

  /// <summary>
  /// EventArgs class to notify transferred CAN frames.
  /// </summary>
  public class CANEventArgs : EventArgs
  {
    #region properties
    /// <summary>
    /// The transferred CAN frame.
    /// </summary>
    public CANFrame Frame { get; private set; }
    #endregion
    #region ctors
    /// <summary>
    /// Initialises a new instance of CANEventArgs with the specified paraneters.
    /// </summary>
    /// <param name="frame">The transferred CAN frame</param>
    public CANEventArgs(CANFrame frame) { Frame = frame; }
    #endregion
  }

  /// <summary>
  /// CAN Configuration exception.
  /// </summary>
  public class CANConfigException : ApplicationException
  {
    #region properties
    /// <summary>
    /// Gets the CAN hardware Id.
    /// </summary>
    public string HardwareId { get; private set; }
    #endregion
    #region ctors
    /// <summary>
    /// Creates a new instance of CANConfigException with the specified parameter.
    /// </summary>
    /// <param name="hardwareId">The hardware Id.</param>
    /// <param name="message">An error message string</param>
    public CANConfigException(string hardwareId, string message) : base(message) { HardwareId = hardwareId; }
    #endregion
  }

  /// <summary>
  /// Provides managed handling of native events (used on CAN receive threads)
  /// </summary>
  public sealed class NativeWaitHandle : WaitHandle
  {
    #region ctors
    public NativeWaitHandle(IntPtr handle)
    { SafeWaitHandle = new SafeWaitHandle(handle, false); }
    public NativeWaitHandle(int handle) : this(new IntPtr(handle)) { }
    #endregion
  }
}