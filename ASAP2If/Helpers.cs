﻿/*!
 * @file    
 * @brief   Defines and implememts common helper types.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;

/// <summary>
/// Defines and implememts common helper types.
/// </summary>
namespace jnsoft.Helpers
{
  /// <summary>
  /// Implements basic helper stuff.
  /// </summary>
  public static class Extensions
  {
    /// <summary>
    /// Copies a block of memory from one location to another.
    /// </summary>
    /// <param name="dst">A pointer to the starting address of the copied block's destination.</param>
    /// <param name="src">A pointer to the starting address of the block of memory to copy.</param>
    /// <param name="count">The size of the block of memory to copy, in bytes.</param>
    public static unsafe void copyMemory(IntPtr dst, IntPtr src, uint count)
    {
      var pSrc = (byte*)src.ToPointer();
      var pDst = (byte*)dst.ToPointer();
      for (int n = 0; n < count; ++n)
        *(pDst + n) = *(pSrc + n);
    }
    /// <summary>
    /// Fills a block of memory with a specified value.
    /// </summary>
    /// <param name="dst">A pointer to the starting address of the block of memory to fill</param>
    /// <param name="len">The size of the block of memory to fill, in bytes. This value must be less than the size of the Destination buffer</param>
    /// <param name="fill">The byte value with which to fill the memory block</param>
    public static unsafe void fillMemory(IntPtr dst, int len, byte fill)
    {
      var pDst = (byte*)dst.ToPointer();
      for (int n = 0; n < len; ++n)
        *(pDst + n) = fill;
    }

    /// <summary>
    /// Sets memory in an array in a fast but unsafe manner.
    /// </summary>
    /// <param name="dst">The destination array</param>
    /// <param name="fillByte">The byte value to fill in</param>
    public static unsafe void fillMemory(ref byte[] dst, byte fillByte)
    {
      fixed (byte* Dst = dst)
      {
        byte* pDst = Dst;
        for (int n = 0; n < dst.Length; ++n)
          *(pDst + n) = fillByte;
      }
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// </summary>
    /// <param name="value"></param>
    /// <returns>The value in the 'other' endianess</returns>
    public static UInt64 changeEndianess(UInt64 value)
    {
      return (UInt64)((UInt64)changeEndianess((UInt32)(value)) << 32) | changeEndianess((UInt32)(value >> 32));
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// </summary>
    /// <param name="value"></param>
    /// <returns>The value in the 'other' endianess</returns>
    public static UInt32 changeEndianess(UInt32 value)
    {
      return (UInt32)((UInt32)changeEndianess((UInt16)(value)) << 16) | changeEndianess((UInt16)(value >> 16));
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// </summary>
    /// <param name="value"></param>
    /// <returns>The value in the 'other' endianess</returns>
    public static UInt16 changeEndianess(UInt16 value)
    {
      return (UInt16)((UInt16)(((byte)value) << 8) | (byte)(value >> 8));
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// </summary>
    /// <param name="value"></param>
    /// <returns>The value in the 'other' endianess</returns>
    public static float changeEndianess(float value)
    {
      return BitConverter.ToSingle(changeEndianess(BitConverter.GetBytes(value), 0, 4), 0);
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// </summary>
    /// <param name="value"></param>
    /// <returns>The value in the 'other' endianess</returns>
    public static double changeEndianess(double value)
    {
      return BitConverter.ToDouble(changeEndianess(BitConverter.GetBytes(value), 0, 8), 0);
    }

    /// <summary>
    /// Turns the endianess around (from/to little/big endian).
    /// 
    /// This method should be used for float/double values in a byte array format.
    /// </summary>
    /// <param name="data">The data</param>
    /// <param name="offset">The offset into the array</param>
    /// <param name="len">The length (4 or 8 bytes)</param>
    /// <returns>The value in the 'other' endianess</returns>
    public static byte[] changeEndianess(byte[] data, int offset, int len)
    {
      switch (len)
      {
        case 4:
          return BitConverter.GetBytes(changeEndianess(BitConverter.ToUInt32(data, offset)));
        case 8:
          return BitConverter.GetBytes(changeEndianess(BitConverter.ToUInt64(data, offset)));
        default: throw new ArgumentException();
      }
    }

    /// <summary>
    /// Turns the endianess of the contained fields around (from/to little/big endian).
    /// 
    /// Only the following fields will be changed:
    /// - public and non-public including inherited fields
    /// - Type Int16/UInt16/Int32/UInt32/Int64/UInt64/float/double (excluding enums!)
    /// </summary>
    /// <typeparam name="T">The reference value type</typeparam>
    /// <param name="instance">The instance (will be modified directly)</param>
    public static void changeEndianess<T>(ref T instance) where T : class
    {
      var fields = typeof(T).GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
      for (int i = fields.Length - 1; i >= 0; --i)
      {
        var fi = fields[i];
        if (fi.IsStatic || !fi.FieldType.IsValueType)
          continue;
        object value = fi.GetValue(instance);
        if (value is Int16)
          fi.SetValue(instance, (Int16)changeEndianess(Convert.ToUInt16(value)));
        else if (value is UInt16)
          fi.SetValue(instance, (UInt16)changeEndianess((UInt16)value));
        else if (value is Int32)
          fi.SetValue(instance, (Int32)changeEndianess(Convert.ToUInt32(value)));
        else if (value is UInt32)
          fi.SetValue(instance, (UInt16)changeEndianess((UInt32)value));
        else if (value is Int64)
          fi.SetValue(instance, (Int64)changeEndianess(Convert.ToUInt64(value)));
        else if (value is UInt64)
          fi.SetValue(instance, (UInt64)changeEndianess((UInt64)value));
        else if (value is float)
          fi.SetValue(instance, (float)changeEndianess((float)value));
        else if (value is double)
          fi.SetValue(instance, (double)changeEndianess((double)value));
      }
    }

    /// <summary>
    /// Aligns address up to a dataSize byte boundary. dataSize must be a power of 2.
    /// 
    /// Taken from Linux kernel align.h
    /// </summary>
    /// <param name="address">address</param>
    /// <param name="dataSize">size of data</param>
    /// <returns>The aligned address</returns>
    public static int alignUp(int address, int dataSize)
    {
      return (((address) + ((dataSize) - 1)) & (~((dataSize) - 1)));
    }

    /// <summary>
    /// Aligns address down to a dataSize byte boundary. dataSize must be a power of 2.
    /// 
    /// Taken from Linux kernel align.h
    /// </summary>
    /// <param name="address">address</param>
    /// <param name="dataSize">size of data</param>
    /// <returns>The aligned address</returns>
    public static int alignDown(int address, int dataSize)
    {
      return (address & ~((dataSize) - 1));
    }

    /// <summary>
    /// Gets an XMLSerializer Instance.
    /// 
    /// Works around a .NET framework problem
    /// (see http://connect.microsoft.com/VisualStudio/feedback/details/422414/xmlserializer-crashes-if-environment-currentdirectory-is-invalid)
    /// </summary>
    /// <param name="type">The type of the object that this System.Xml.Serialization.XmlSerializer can serialize</param>
    /// <returns>An XmlSerializer instance</returns>
    public static XmlSerializer getXmlSerializer(Type type)
    {
      return getXmlSerializer(type, null);
    }

    /// <summary>
    /// Gets an XMLSerializer Instance.
    /// 
    /// Works around a .NET framework problem
    /// (see http://connect.microsoft.com/VisualStudio/feedback/details/422414/xmlserializer-crashes-if-environment-currentdirectory-is-invalid)
    /// </summary>
    /// <param name="type">The type of the object that this System.Xml.Serialization.XmlSerializer can serialize</param>
    /// <param name="extraTypes">A System.Type array of additional object types to serialize.</param>
    /// <returns>An XmlSerializer instance</returns>
    public static XmlSerializer getXmlSerializer(Type type, Type[] extraTypes)
    {
      // try to set an accessible directory
      string currDir = Environment.CurrentDirectory;
      try
      { // build the serializer
        Environment.CurrentDirectory = Path.GetTempPath();
        return new XmlSerializer(type, extraTypes);
      }
      finally
      { // try to restore the current directory
        try { Environment.CurrentDirectory = currDir; }
        catch { }
      }
    }

    /// <summary>
    /// Tries to convert a double raw value back to an integer value.
    /// </summary>
    /// <param name="rawValue"></param>
    /// <param name="dataType"></param>
    /// <param name="len">Lenght of the returned value in Bits</param>
    /// <returns></returns>
    static ulong convertBack(double rawValue, DATA_TYPE dataType, out int len)
    {
      unchecked
      {
        ulong result;
        switch (dataType)
        {
          case DATA_TYPE.SBYTE: result = (byte)Convert.ToSByte(rawValue); len = 8; break;
          case DATA_TYPE.UBYTE: result = Convert.ToByte(rawValue); len = 8; break;
          case DATA_TYPE.SWORD: result = (ulong)Convert.ToInt16(rawValue); len = 16; break;
          case DATA_TYPE.UWORD: result = Convert.ToUInt16(rawValue); len = 16; break;
          case DATA_TYPE.SLONG: result = (ulong)Convert.ToInt32(rawValue); len = 32; break;
          case DATA_TYPE.ULONG: result = Convert.ToUInt32(rawValue); len = 32; break;
          case DATA_TYPE.A_INT64: result = (ulong)Convert.ToInt64(rawValue); len = 64; break;
          case DATA_TYPE.A_UINT64: result = Convert.ToUInt64(rawValue); len = 64; break;
          case DATA_TYPE.FLOAT32_IEEE:
            var bytes = BitConverter.GetBytes(Convert.ToSingle(rawValue));
            result = BitConverter.ToUInt32(bytes, 0);
            len = 32;
            break;
          case DATA_TYPE.FLOAT64_IEEE:
            bytes = BitConverter.GetBytes(rawValue);
            result = BitConverter.ToUInt64(bytes, 0);
            len = 64;
            break;
          default: throw new NotSupportedException(dataType.ToString());
        }
        return result;
      }
    }

    /// <summary>
    /// Delivers a decimal string representation for the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="dataType">The corresponding raw value data type</param>
    /// <returns>The string representation</returns>
    public static string toDecimalString(double rawValue, DATA_TYPE dataType)
    {
      if (double.IsNaN(rawValue))
        return Constants.NOT_A_NUMBER;

      try { int len = 0; return convertBack(rawValue, dataType, out len).ToString(); }
      catch (OverflowException) { return Constants.NOT_A_NUMBER; }
    }

    /// <summary>
    /// Delivers a decimal string representation for the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="dataType">The corresponding raw value data type</param>
    /// <returns>The string representation</returns>
    public static string toDecimalString(ulong rawValue, DATA_TYPE dataType) { return rawValue.ToString(); }

    /// <summary>
    /// Delivers a hexadecimal string representation for the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="dataType">The corresponding raw value data type</param>
    /// <param name="removeLeadingZeros">true, if leading zeros should not be displayed</param>
    /// <returns>The string representation</returns>
    public static string toHexString(double rawValue, DATA_TYPE dataType, bool removeLeadingZeros)
    {
      if (double.IsNaN(rawValue))
        return Constants.NOT_A_NUMBER;

      try { int len; return toHexString(convertBack(rawValue, dataType, out len), dataType, removeLeadingZeros); }
      catch (OverflowException) { return Constants.NOT_A_NUMBER; }
    }

    public static string toHexString(ulong rawValue, DATA_TYPE dataType, bool removeLeadingZeros)
    {
      var sb = new StringBuilder(string.Format(Constants.HEX_FMT, rawValue));
      if (removeLeadingZeros)
        // Delete leading zero's
        while (sb.Length > 3)
        {
          if (sb[2] != '0')
            // found a bit set
            break;
          sb.Remove(2, 1);
        }
      return sb.ToString();
    }

    /// <summary>
    /// Delivers a binary string representation for the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="dataType">The corresponding raw value data type</param>
    /// <param name="removeLeadingZeros">true, if leading zeros should not be displayed</param>
    /// <returns>The string representation</returns>
    public static string toBinaryString(double rawValue, DATA_TYPE dataType, bool removeLeadingZeros)
    {
      if (double.IsNaN(rawValue))
        return Constants.NOT_A_NUMBER;

      try { int len; return toBinaryString(convertBack(rawValue, dataType, out len), dataType, removeLeadingZeros); }
      catch (OverflowException) { return Constants.NOT_A_NUMBER; }
    }

    /// <summary>
    /// Delivers a binary string representation for the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="dataType">The corresponding raw value data type</param>
    /// <param name="removeLeadingZeros">true, if leading zeros should not be displayed</param>
    /// <returns>The string representation</returns>
    public static string toBinaryString(ulong rawValue, DATA_TYPE dataType, bool removeLeadingZeros)
    {
      var sb = new StringBuilder();
      UInt64 currentBit = 0x01;
      var bitLen = dataType.getSizeInBit();
      for (int i = 0; i < bitLen; ++i)
      {
        sb.Insert(0, 0 < (currentBit & rawValue) ? '1' : '0');
        currentBit <<= 1;
      }
      if (removeLeadingZeros)
        // Delete leading zero's
        while (sb.Length > 1)
        {
          if (sb[0] == '1')
            // found a bit set
            break;
          sb.Remove(0, 1);
        }
      return sb.ToString();
    }

    /// <summary>
    /// Gets the shift count for a specified bitmask (also known as bit offset).
    /// </summary>
    /// <param name="bitmask">The bitmask</param>
    /// <returns>The count of left shift to get the first bit as 1</returns>
    public static int getShiftCount(UInt64 bitmask)
    {
      int count = 0;
      while (count < 64 && (bitmask & 0x01) == 0)
      {
        bitmask >>= 1;
        count++;
      }
      return count;
    }

    /// <summary>
    /// Gets the count of significant bits defined by the bitmask and data type.
    /// </summary>
    /// <param name="type">data type</param>
    /// <param name="bitmask">The bitmask</param>
    /// <returns>The count of significant bits defined by the bitmask</returns>
    public static int getBitCount(DATA_TYPE type, UInt64 bitmask)
    {
      int max = type.getSizeInByte();
      int count = 0, breaker = 0;
      while (breaker++ < max)
      {
        if ((bitmask & 0x01) == 1)
          count++;
        bitmask >>= 1;
      }
      return count;
    }

    /// <summary>
    /// Trim text to a maximum size of columns (word wrap).
    /// </summary>
    /// <param name="text">text to trim </param>
    /// <param name="maxColumns">maximum column size</param>
    /// <returns>trimmed text</returns>
    public static string getTrimmedText(string text, int maxColumns)
    {
      if (string.IsNullOrEmpty(text))
        return string.Empty;
      var sb = new StringBuilder();
      int i = 0, colCount = 0;
      var words = text.Split(' ');
      while (i++ < words.Length)
      {
        var w = words[i - 1];
        sb.Append(w);
        colCount += w.Length;
        if (!w.EndsWith("\n") && colCount > maxColumns)
        {
          sb.Append("\r\n");
          colCount = 0;
        }
        else
          sb.Append(' ');
      }
      return sb.ToString().Trim();
    }

    /// <summary>
    /// Converts a byte array into the specified object reference.
    /// </summary>
    /// <typeparam name="T">The resulting object type</typeparam>
    /// <param name="data">The byte array to copy from</param>
    /// <param name="result">The resulting object</param>
    /// <returns>true if successful, false, if the memory size of the supplied data is smaller than the destination type</returns>
    public static bool convertToReference<T>(byte[] data, out T result) where T : class
    {
      return convertToReference(data, 0, out result);
    }

    /// <summary>
    /// Converts a byte array into the specified object reference.
    /// </summary>
    /// <typeparam name="T">The resulting object type</typeparam>
    /// <param name="data">The byte array to copy from</param>
    /// <param name="offset">Offset to the data to convert</param>
    /// <param name="result">The resulting object</param>
    /// <returns>true if successful, false, if the memory size of the supplied data is smaller than the destination type</returns>
    public static bool convertToReference<T>(byte[] data, int offset, out T result) where T : class
    {
      unsafe
      {
        fixed (byte* p = &data[offset])
          result = (T)Marshal.PtrToStructure((IntPtr)p, typeof(T));
        return true;
      }
    }

    /// <summary>
    /// Converts a byte array into the specified object reference.
    /// </summary>
    /// <typeparam name="T">The resulting object type</typeparam>
    /// <param name="data">The byte array to copy from</param>
    /// <param name="result">The resulting object</param>
    /// <param name="additionalData">available additional data or null</param>
    /// <returns>true if successful, false, if the memory size of the supplied data is smaller than the destination type</returns>
    public static bool convertToReference<T>(List<byte> data, out T result, out byte[] additionalData) where T : class
    {
      var arrData = data.ToArray();
      result = null;
      additionalData = null;
      Type t = typeof(T);
      int size = Marshal.SizeOf(t);
      int remainingBytes = arrData.Length - size;
      if (!convertToReference(arrData, out result))
        return false;
      if (remainingBytes > 0)
      {
        additionalData = new byte[remainingBytes];
        Array.Copy(arrData, size, additionalData, 0, remainingBytes);
      }
      return true;
    }

    /// <summary>
    /// Converts a (marshallable) class to a byte array.
    /// </summary>
    /// <typeparam name="T">THe instance type</typeparam>
    /// <param name="instance">The instance to serialize</param>
    /// <returns>The byte array representation</returns>
    public static byte[] convertToByte<T>(T instance) where T : class
    {
      return convertToByte<T>(instance, null);
    }

    /// <summary>
    /// Converts a (marshallable) class to a byte array.
    /// </summary>
    /// <typeparam name="T">THe instance type</typeparam>
    /// <param name="instance">The instance to serialize</param>
    /// <param name="additionalBytes">Additional bytes to add</param>
    /// <returns>The byte array representation</returns>
    public static byte[] convertToByte<T>(T instance, byte[] additionalBytes) where T : class
    {
      IntPtr mem = IntPtr.Zero;
      try
      {
        int marshalSize = (UInt16)Marshal.SizeOf(typeof(T));
        int overAllSize = marshalSize + (additionalBytes != null ? additionalBytes.Length : 0);
        mem = Marshal.AllocHGlobal(overAllSize);
        Marshal.StructureToPtr(instance, mem, true);

        byte[] result = new byte[overAllSize];
        Marshal.Copy(mem, result, 0, marshalSize);
        if (additionalBytes != null)
          Array.Copy(additionalBytes, 0, result, marshalSize, additionalBytes.Length);
        return result;
      }
      finally
      { // force to free the global memory
        if (mem != IntPtr.Zero)
          Marshal.FreeHGlobal(mem);
      }
    }

    /// <summary>
    /// Builds a HTML table line.
    /// </summary>
    /// <param name="args">html table columns</param>
    /// <returns>html table column string</returns>
    public static string buildHtmlTableLine(params string[] args)
    {
      var sb = new StringBuilder();
      sb.Append("<tr>");
      foreach (var arg in args)
        sb.Append($"<td>{arg}</td>");
      sb.Append("</tr>");
      return sb.ToString();
    }

    /// <summary>
    /// Gets the length in bits of a DATA_TYPE instance.
    /// </summary>
    /// <param name="instance"></param>
    /// <returns>length in bits</returns>
    public static int getSizeInBit(this DATA_TYPE instance)
    {
      return instance.getSizeInByte() * 8;
    }

    /// <summary>
    /// Gets the length in bytes of a DATA_TYPE instance.
    /// </summary>
    /// <param name="instance"></param>
    /// <returns>length in bytes</returns>
    public static int getSizeInByte(this DATA_TYPE instance)
    {
      switch (instance)
      {
        case DATA_TYPE.SBYTE:
        case DATA_TYPE.UBYTE: return 1;
        case DATA_TYPE.UWORD:
        case DATA_TYPE.SWORD: return 2;
        case DATA_TYPE.ULONG:
        case DATA_TYPE.SLONG:
        case DATA_TYPE.FLOAT32_IEEE: return 4;
        case DATA_TYPE.A_UINT64:
        case DATA_TYPE.A_INT64:
        case DATA_TYPE.FLOAT64_IEEE: return 8;
        default: throw new NotSupportedException(instance.ToString());
      }
    }
  }

  /// <summary>
  /// EventArgs class to notify progress and support a cancel possibility.
  /// </summary>
  public sealed class ProgressArgs : EventArgs
  {
    /// <summary>Progress in percent</summary>
    public int Percent { get; }
    /// <summary>Set to true to cancel process</summary>
    public bool Cancel { get; set; }
    /// <summary>
    /// Creates a new instance of ProgressArgs with the specified arguments.
    /// </summary>
    /// <param name="percent"></param>
    public ProgressArgs(int percent)
    { Percent = percent; }
  }

  /// <summary>Possible types of parser messages.</summary>
  public enum MessageType
  {
    /// <summary>parser info</summary>
    Info,
    /// <summary>parser warning</summary>
    Warning,
    /// <summary>parser error</summary>
    Error,
  }

  /// <summary>
  /// Parser message EventArgument generated while parsing a file.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed class ParserEventArgs : EventArgs
  {
    /// <summary>Get the time the message occured</summary>
    public DateTime TimeStamp { get; private set; }
    /// <summary>Gets the corresponding a2l object</summary>
    public object Source { get; private set; }
    /// <summary>Gets the message type</summary>
    public MessageType Type { get; private set; }
    /// <summary>Gets the parser message</summary>
    public string Message { get; private set; }
    /// <summary>
    /// Creates a new instance of ODXParserEventArgs.
    /// </summary>
    /// <param name="source">The source object instance</param>
    /// <param name="type">Message type</param>
    /// <param name="message">Message as string</param>
    public ParserEventArgs(object source, MessageType type, string message)
    { TimeStamp = DateTime.Now; Source = source; Type = type; Message = message; }
  }

  /// <summary>Enumeration for description of the basic data types in the ECU program
  /// 
  /// Note: If ECU values of type integer 64 are converted in physical values depending on the computation 
  /// formula a higher precision in the physical area is necessary. The currently used float 64 format 
  /// supports less precision than int 64. Therefore the precision of the physical representation is reduced 
  /// to the precision of float 64. This is relevant for ASAM MCD-3 standards [ASAM MCD-3] where 
  /// data transfer is defined as physical. This is additionally relevant for all tools working 
  /// on PCs / Operating systems that do not support higher precision than float 64. Here the representation is 
  /// rounded for physical and maybe also for internal representation.
  /// </summary>
  public enum DATA_TYPE
  {
    /// <summary>unsigned byte (8 Bit)</summary>
    UBYTE,
    /// <summary>signed byte (8 Bit)</summary>
    SBYTE,
    /// <summary>unsigned word (16 Bit)</summary>
    UWORD,
    /// <summary>signed word (16 Bit)</summary>
    SWORD,
    /// <summary>unsigned long (32 Bit)</summary>
    ULONG,
    /// <summary>signed long (32 Bit)</summary>
    SLONG,
    /// <summary>float (32 Bit)</summary>
    FLOAT32_IEEE,
    /// <summary>unsigned longlong (64 Bit)</summary>
    A_UINT64,
    /// <summary>signed longlong (64 Bit)</summary>
    A_INT64,
    /// <summary>double (64 Bit)</summary>
    FLOAT64_IEEE,
  }

  /// <summary>Enumeration for description of the byte order in the control unit program. 
  /// 
  /// Note: Use of LITTLE_ENDIAN and BIG_ENDIAN defined with keyword BYTE_ORDER leads to 
  /// mistakes because it is in contradiction to general use of terms „little endian“ and „big endian“. 
  /// The keywords LITTLE_ENDIAN and BIG_ENDIAN should no longer be used, they should be 
  /// replaced by MSB_LAST and MSB_FIRST which are equivalent (definition of MSB_LAST and 
  /// MSB_FIRST: see keyword BYTE_ORDER).
  /// </summary>
  public enum BYTEORDER_TYPE
  {
    /// <summary>Big endian</summary>
    [Description("Big endian")]
    MSB_FIRST = 1,
    /// <summary>Big endian (compatibility with former ASAP standard)</summary>
    [Description("Big endian")]
    BIG_ENDIAN = MSB_FIRST,
    /// <summary>Little endian</summary>
    [Description("Little endian")]
    MSB_LAST = 2,
    /// <summary>Little endian (compatibility with former ASAP standard)</summary>
    [Description("Little endian")]
    LITTLE_ENDIAN = MSB_LAST,
    /// <summary> Used when byteorder is 'not set'.</summary>
    [Description("Not set")]
    NotSet = 0,
    //MSB_FIRST_MSW_LAST = 3,
    //MSB_LAST_MSW_FIRST = 4,
  }

  /// <summary>
  /// Possible value object formats.
  /// </summary>
  public enum ValueObjectFormat
  {
    /// <summary>
    /// Values are physical values.
    /// 
    /// Currently only double values are supported
    /// (not verbal values as strings). This means 
    /// a verbal value is represented by it's raw 
    /// value, even if the physical value is requested.
    /// </summary>
    [Description("Physical values")]
    Physical,
    /// <summary>
    /// Values are raw values (in decimal representation).
    /// </summary>
    [Description("Raw values (decimal)")]
    Raw,
    /// <summary>
    /// Values are raw values (in hexadezimal representation).
    /// </summary>
    [Description("Raw values (hexadecimal)")]
    RawHex,
    /// <summary>
    /// Values are raw values (in binary representation).
    /// </summary>
    [Description("Raw values (binary)")]
    RawBin,
  }

  /// <summary>
  /// Globally used constants.
  /// </summary>
  public static class Constants
  {
    public const string CATData = "Data";
    public const string CATDataXCPplus = "Data.XCPpLus";
    public const string CATDataLimit = "Data.Limit";
    public const string CATDataLink = "Data.Link";
    public const string CATNavigation = "Navigation";
    public const string DESCTagProperty = "This property may be used to bind any user reference to this instance.";
    public const string NOT_A_NUMBER = "NaN";
    public const string HEX_FMT = "0x{0:X}";
    public const string POINT_STR_FMT = "[{0}/{1}]";
  }

  /// <summary>Represents a value range with a Min and Max border.</summary>
  public struct sRange
  {
    /// <summary>Gets or sets the minimum</summary>
    public double Min;
    /// <summary>Gets or sets the maximum</summary>
    public double Max;
    public override string ToString()
    {
      return string.Format(CultureInfo.InvariantCulture, "Range: {0:f2}..{1:f2}", Min, Max);
    }
  }

  /// <summary>Typeconverter to edit a reference the Byte order.</summary>
  public sealed class TCByteOrder : EnumConverter
  {
    public TCByteOrder(Type type) : base(type) { }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var standardValues = new List<int>();
      standardValues.Add((int)BYTEORDER_TYPE.MSB_FIRST);
      standardValues.Add((int)BYTEORDER_TYPE.MSB_LAST);
      //standardValues.Add((int)BYTEORDER_TYPE.MSB_FIRST_MSW_LAST);
      //standardValues.Add((int)BYTEORDER_TYPE.MSB_LAST_MSW_FIRST);
      return new StandardValuesCollection(standardValues);
    }
  }

  /// <summary>Typeconverter to edit a reference the Byte order.</summary>
  public sealed class TCByteOrderWithNotSet : EnumConverter
  {
    public TCByteOrderWithNotSet(Type type) : base(type) { }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var standardValues = new List<int>();
      standardValues.Add((int)BYTEORDER_TYPE.NotSet);
      standardValues.Add((int)BYTEORDER_TYPE.MSB_FIRST);
      standardValues.Add((int)BYTEORDER_TYPE.MSB_LAST);
      //standardValues.Add((int)BYTEORDER_TYPE.MSB_FIRST_MSW_LAST);
      //standardValues.Add((int)BYTEORDER_TYPE.MSB_LAST_MSW_FIRST);
      return new StandardValuesCollection(standardValues);
    }
  }

  /// <summary>Typeconverter to display and edit hex values.</summary>
  public sealed class TCHexType : TypeConverter
  {
    public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
    { return destinationType == typeof(string); }

    public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
    {
      return string.Format(Constants.HEX_FMT, value);
    }

    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
        return true;
      return base.CanConvertFrom(context, sourceType);
    }

    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      string strValue = value as string;
      if (strValue != null)
      {
        object defaultValue = null;
        foreach (Attribute attr in context.PropertyDescriptor.Attributes)
        { // try to get the defaultvalue
          if (attr is DefaultValueAttribute)
          {
            DefaultValueAttribute dv = (DefaultValueAttribute)attr;
            defaultValue = dv.Value;
          }
        }

        if (strValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
          strValue = strValue.Substring(2);
        try
        {
          if (context.PropertyDescriptor.PropertyType == typeof(UInt64))
            return UInt64.Parse(strValue, NumberStyles.HexNumber);
          if (context.PropertyDescriptor.PropertyType == typeof(UInt32))
            return UInt32.Parse(strValue, NumberStyles.HexNumber);
          if (context.PropertyDescriptor.PropertyType == typeof(UInt16))
            return UInt16.Parse(strValue, NumberStyles.HexNumber);
          if (context.PropertyDescriptor.PropertyType == typeof(byte))
            return byte.Parse(strValue, NumberStyles.HexNumber);
        }
        catch (Exception ex)
        {
          throw new ApplicationException(string.Format("Failed to convert {0} to {1}\r\n\r\nDetails: {2}"
            , value, context.PropertyDescriptor.PropertyType.Name, ex.Message)
            , ex
            );
        }
      }
      return base.ConvertFrom(context, culture, value);
    }
  }

  /// <summary>
  /// Represents a data point in double floating point (64 Bit) precision.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 8, Size = 16)]
  public struct DataPoint
  {
    /// <summary>
    /// Size in bytes of the DatatPoint.
    /// </summary>
    public static int Size = Marshal.SizeOf(typeof(DataPoint));
    /// <summary>
    /// The X-Coordinate.
    /// </summary>
    public double X;
    /// <summary>
    /// The Y-Coordinate.
    /// </summary>
    public double Y;
    /// <summary>
    /// Creates a new instance with the specified coordinates.
    /// </summary>
    /// <param name="x">The x coordinate</param>
    /// <param name="y">The y coordinate</param>
    public DataPoint(double x, double y) { X = x; Y = y; }
    /// <summary>
    /// Serializes the data point to the specified binary writer.
    /// </summary>
    /// <param name="bw">The binary writer to serialize to</param>
    public void serialize(BinaryWriter bw) { bw.Write(X); bw.Write(Y); }
    /// <summary>
    /// Deserializes a data point from the specified binary reader.
    /// </summary>
    /// <param name="br">The binary reader to the data point read from</param>
    /// <returns>A data point instance</returns>
    public static DataPoint deserialize(BinaryReader br) { return new DataPoint(br.ReadDouble(), br.ReadDouble()); }
    /// <summary>Gets a string representation</summary>
    /// <returns>a string representation</returns>
    public override string ToString() { return string.Format(CultureInfo.InvariantCulture, Constants.POINT_STR_FMT, X, Y); }
  }
  /// <summary>
  /// Represents a data point in single floating point (32 Bit) precision.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 8)]
  public struct DataPointF
  {
    /// <summary>
    /// Size in bytes of the DatatPoint.
    /// </summary>
    public static int Size = Marshal.SizeOf(typeof(DataPointF));
    /// <summary>
    /// The X-Coordinate.
    /// </summary>
    public float X;
    /// <summary>
    /// The Y-Coordinate.
    /// </summary>
    public float Y;
    /// <summary>
    /// Creates a new instance with the specified coordinates.
    /// </summary>
    /// <param name="x">The x coordinate</param>
    /// <param name="y">The y coordinate</param>
    public DataPointF(float x, float y) { X = x; Y = y; }
    /// <summary>
    /// Serializes the data point to the specified binary writer.
    /// </summary>
    /// <param name="bw">The binary writer to serialize to</param>
    public void serialize(BinaryWriter bw) { bw.Write(X); bw.Write(Y); }
    /// <summary>
    /// Deserializes a data point from the specified binary reader.
    /// </summary>
    /// <param name="br">The binary reader to the data point read from</param>
    /// <returns>A data point instance</returns>
    public static DataPointF deserialize(BinaryReader br) { return new DataPointF(br.ReadSingle(), br.ReadSingle()); }
    /// <summary>Gets a string representation</summary>
    /// <returns>a string representation</returns>
    public override string ToString() { return string.Format(CultureInfo.InvariantCulture, Constants.POINT_STR_FMT, X, Y); }
  }

  /// <summary>
  /// Represents data limits.
  /// </summary>
  public struct DataLimits
  {
    /// <summary>The data minimum</summary>
    public double Min;
    /// <summary>The data maximum</summary>
    public double Max;
    public DataLimits(double min, double max)
    {
      Min = min; Max = max;
      if (0 == min.CompareTo(max))
      {
        Min -= 1;
        Max += 1;
      }
    }
    /// <summary>
    /// Returns the absolute difference between the limits.
    /// </summary>
    public double AbsoluteDiff { get { return Math.Abs(Max - Min); } }
  }

  /// <summary>
  /// Holds a memory range.
  /// </summary>
  public sealed class MemoryRange
  {
    /// <summary>
    /// Start address.
    /// </summary>
    public UInt32 Start;
    /// <summary>
    /// Next address after the segment end.
    /// </summary>
    public UInt32 Next;
    /// <summary>
    /// Initialises a new instance of Range.
    /// </summary>
    /// <param name="start">The start address</param>
    /// <param name="next">The Next address after the segment end</param>
    public MemoryRange(UInt32 start, UInt32 next) { Start = start; Next = next; }
#if DEBUG
    public override string ToString()
    {
      return $"Start:{Start:X} End:{Next:X} Size:{Next - Start}";
    }
#endif
  }

  /// <summary>
  /// Holds a range of memory.
  /// </summary>
  public sealed class MemoryRangeList : List<MemoryRange>
  {
#region members
    bool mIsSorted;
#endregion
#region methods
    /// <summary>
    /// Sorts the memory ranges in case it is unsorted.
    /// </summary>
    public void ensureSorted()
    {
      if (!mIsSorted)
      { // ensure a sorted list
        Sort(sortByAddress);
        mIsSorted = true;
      }
    }

    /// <summary>
    /// Adds(merges) a memory blck to the list of memory ranges.
    /// </summary>
    /// <param name="address">The start address of the memory block to add</param>
    /// <param name="size">The size of the memory block to add</param>
    public void add(UInt32 address, int size)
    {
      var start = address;
      var next = (UInt32)(start + size);
      bool contained = false;
      int i = 0;
      while (!contained && i < Count)
      {
        var range = this[i];
        if (start >= range.Start && next <= range.Next)
        { // fully contained
          contained = true;
          continue;
        }
        if (next == range.Start)
        { // extend to lower
          range.Start = Math.Min(start, range.Start);
          contained = true;
        }
        if (start == range.Next)
        { // extend to upper
          range.Next = Math.Max(next, range.Next);
          contained = true;
        }
        ++i;
      }
      if (!contained)
      { // new range
        Add(new MemoryRange(start, next));
        mIsSorted = false;
      }
    }

    /// <summary>
    /// FInds the memory range index containing the specified address and size.
    /// 
    /// Requires an instance sorted by start(address).
    /// </summary>
    /// <param name="address">The start address</param>
    /// <param name="size">The size</param>
    /// <returns>-1 if corresponding range is not found, else a valid index</returns>
    public int findRangeIndex(UInt32 address, int size)
    {
      if (!mIsSorted)
      { // ensure a sorted list
        Sort(sortByAddress);
        mIsSorted = true;
      }

      if (Count == 0)
        return -1;
      if (address < this[0].Start)
        return -1;
      if (address >= this[Count - 1].Next)
        return -1;

      int nLow = 0;
      int nHigh = Count;
      while (nLow < nHigh)
      {
        int nMid = nLow + (nHigh - nLow) / 2; // workaround for sum overflow
        UInt32 compareStart = this[nMid].Start;
        if (compareStart <= address)
          nLow = nMid + 1;
        else
          nHigh = nMid;
      }
      int index = Math.Max(nLow - 1, 0);
      return this[index].Next < address + size
        ? -1
        : index;
    }

    /// <summary>
    /// Sort MemoryRangeList by start (address).
    /// </summary>
    /// <param name="r1">First MemoryRange</param>
    /// <param name="r2">Second MemoryRange</param>
    /// <returns>See CompareTo return value</returns>
    static int sortByAddress(MemoryRange r1, MemoryRange r2)
    {
      if (r1 == r2)
        return 0;
      return r1.Start.CompareTo(r2.Start);
    }

#if DEBUG
    public override string ToString()
    {
      uint size = 0;
      foreach (var r in this)
        size += r.Next - r.Start;
      return $"Count ranges={Count}, overall size={size}";
    }
#endif
#endregion
  }
}