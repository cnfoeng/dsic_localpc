﻿/*!
 * @file    
 * @brief   Defines the Interface for dynamically compiled formulas.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    February 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
namespace jnsoft.ASAP2.Formulas
{
  /// <summary>
  /// Interface for dynamically compiled formulas.
  /// 
  /// Used to create compiled formulas (see A2LFormulaDict).
  /// </summary>
  public interface IEvaluator
  {
    /// <summary>
    /// Returns the f(x) formula result.
    /// </summary>
    /// <param name="x">The x value as input into the formula</param>
    /// <returns>The computed f(x) value or double.NaN if any exception or error occured</returns>
    double eval(double x);
  }
}
