﻿/*!
 * @file    
 * @brief   Implements the Application Timebase.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Diagnostics;

namespace jnsoft.Helpers
{
  /// <summary>
  /// Provides system/application time functions.
  /// </summary>
  public static class TimeBase
  {
    static Stopwatch mStopWatch = new Stopwatch();
    static long mFirstAccessTime = DateTime.Now.Ticks;
    static double mDAQLastTimestamp;
    static double mEndTime = double.NaN;
    static double mCurrentTime = double.NaN;
    /// <summary>
    /// Gets the time elapsed since Aplication start.
    /// </summary>
    public static TimeSpan Elapsed { get { return TimeSpan.FromTicks(DateTime.Now.Ticks - mFirstAccessTime); } }
    #region DAQ support
    /// <summary>
    /// Gets or sets the most recently received data acquisition time.
    /// </summary>
    public static double DAQLastTimestamp
    {
      get { return mDAQLastTimestamp; }
      set { mDAQLastTimestamp = Math.Max(value, mDAQLastTimestamp); }
    }
    /// <summary>
    /// Gets the seconds elapsed since resetDAQTime is called.
    /// </summary>
    public static double ElapsedDAQSeconds { get { return (double)mStopWatch.ElapsedTicks / (double)Stopwatch.Frequency; } }
    /// <summary>
    /// Resets the measured time.
    /// </summary>
    public static void resetDAQTime()
    {
      mStopWatch.Restart();
      mDAQLastTimestamp = 0;
      mEndTime = mCurrentTime = double.NaN;
    }
    /// <summary>
    /// Stops the data acquisition time.
    /// </summary>
    public static void stopDAQTime()
    {
      mStopWatch.Stop();
      mEndTime = mCurrentTime = mDAQLastTimestamp;
    }
    /// <summary>
    /// Gets the current measuring time set for displaying measured values.
    /// </summary>
    /// <returns>The time in ticks</returns>
    public static double getDAQTime()
    {
      return !double.IsNaN(mCurrentTime)
        ? mCurrentTime
        : !double.IsNaN(mEndTime)
          ? mEndTime
          : mDAQLastTimestamp;
    }
    /// <summary>
    /// Sets the measured time from seconds.
    /// </summary>
    /// <param name="time">The time in seconds</param>
    public static void setCurrentDAQTimeFromSeconds(double time)
    {
      mCurrentTime = time;
    }
    #endregion
  }
}