﻿/*!
 * @file    
 * @brief   Implements the Frame/FrameQueue.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2016
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2If.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace jnsoft.Comm
{
  /// <summary>
  /// Base class for any frame type (CAN, CCP, XCP, UDS).
  /// </summary>
  public abstract class Frame
  {
    /// <summary>
    /// The data bytes of the frame.
    /// </summary>
    public byte[] Data;
    /// <summary>
    /// The time elapsed since the start.
    /// 
    /// The Elapsed time is set by the FrameQueue, when the frame is queued.
    /// </summary>
    public TimeSpan Elapsed { get; set; }
    /// <summary>
    /// Indicates if the frame is sent by the master or sent by the slave.
    /// </summary>
    public bool IsMasterFrame { get; set; }
    /// <summary>
    /// Builds a CSV representation of the frame.
    /// </summary>
    /// <returns>the string representation</returns>
    abstract public string toCSV();
    /// <summary>
    /// Builds a tab delimited representation of the frame.
    /// </summary>
    /// <returns>the string representation</returns>
    abstract public string toClipboard();
    /// <summary>
    /// Gets the frame's data.
    /// </summary>
    /// <returns>returns the frame's data as string representation</returns>
    public string getDataStr()
    {
      var sb = new StringBuilder();
      for (int i = 0; i < Data.Length; ++i)
        sb.Append($"{Data[i]:X2} ");
      if (sb.Length > 0)
        sb.Remove(sb.Length - 1, 1);
      return sb.ToString();
    }
    /// <summary>
    /// Gets the timestamp string.
    /// </summary>
    /// <returns></returns>
    public string getTimeStr(double offset = 0.0)
    {
      return (Elapsed.TotalSeconds - offset).ToString($"f{Settings.Default.LogTimeDecimalCount}", CultureInfo.InvariantCulture);
    }
    /// <summary>
    /// Gets the frame's data in ASCII representation.
    /// 
    /// Not printable characters are filtered by default.
    /// </summary>
    /// <param name="notAllowedChar">Defines a printable character which get filtered from the resulting string</param>
    /// <returns>returns the frame's data in ASCII representation</returns>
    public string getDataASCIIStr(char notAllowedChar)
    {
      var sb = new StringBuilder();
      for (int i = 0; i < Data.Length; ++i)
      {
        char c = (char)Data[i];
        sb.Append(Char.IsControl(c) || c > 0x7f || c == notAllowedChar ? '.' : c);
      }
      return sb.ToString();
    }
  }
  /// <summary>
  /// Base class for any frame type queue (CAN, CCP, XCP, UDS,...)
  /// </summary>
  public abstract class FrameQueue<T> : IDisposable where T : Frame
  {
    #region members
    /// <summary>Stop logging to file if less than 512 MB is free on the logging drive.</summary>
    const long mMinFreeSpace = 512 * 1000 * 1024;
    const int mMaxFrames = 100000;
    const int mMaxFramesToClear = (int)(mMaxFrames + mMaxFrames * 0.1);
    protected List<T> mFrames = new List<T>(mMaxFramesToClear);
    static DriveInfo mDriveInfo;
    uint mWrittenFrameCtr;
    int mUnreadFrameIndex;
    StreamWriter mLog;
    string mActiveLogFilePath;
    #endregion
    #region properties
    /// <summary>Indicates if writing to file is enabled.</summary>
    public bool IsWritingToFile
    {
      get
      {
        lock (this)
          return mLog != null;
      }
    }
    /// <summary>Get constant Filename without path</summary>
    public abstract string LogFileName { get; }
    /// <summary>Gets the constant CSV header for the CSV log</summary>
    public abstract string LogFileCSVHeader { get; }
    #endregion
    #region methods
    /// <summary>
    /// Sets the time elapsed to the frame, logs if the file log is activated, removes frames preventing memory overruns.
    /// </summary>
    /// <param name="frame"></param>
    protected void addFrame(T frame)
    {
      lock (mFrames)
      {
        mFrames.Add(frame);
        frame.Elapsed = TimeBase.Elapsed;

        if (mLog != null)
        { // Write any frame to file
          try { mLog.WriteLine(frame.toCSV()); }
          catch { }
          ++mWrittenFrameCtr;

          if (mWrittenFrameCtr % 10000 == 0)
          {
            if (mLog.BaseStream.Position > int.MaxValue)
            {  // close file after more than 2 GB are written
              mLog.Close();
              mLog = null;
            }
            else if (mDriveInfo != null && mDriveInfo.AvailableFreeSpace < mMinFreeSpace)
            { // Stop logging to file if less than 512 MB is free on the logging drive.
              mLog.Close();
              mLog = null;
            }
          }
        }

        if (mFrames.Count > mMaxFramesToClear)
        { // release frames to prevent memory overflows
          int clearCount = mFrames.Count - mMaxFrames;
          mFrames.RemoveRange(0, clearCount);
          mUnreadFrameIndex -= clearCount;
          if (mUnreadFrameIndex < 0)
            mUnreadFrameIndex = 0;
        }
      }
    }
    /// <summary>
    /// Clears the frame queue.
    /// </summary>
    public virtual void clear()
    {
      lock (mFrames)
      {
        mFrames.Clear();
        mUnreadFrameIndex = 0;
      }
    }
    /// <summary>
    /// Gets all available frames (in order of occurence).
    /// </summary>
    /// <param name="cntFrames">Count of available frames</param>
    /// <returns>all available frames</returns>
    public T[] getFrames(out int cntFrames)
    {
      lock (mFrames)
      {
        mUnreadFrameIndex = cntFrames = mFrames.Count;
        return mFrames.ToArray();
      }
    }
    /// <summary>
    /// Gets all available unread frames (in order of occurence).
    /// </summary>
    /// <param name="cntFrames">Count of available frames</param>
    /// <returns>all unread frames</returns>
    public T[] getUnreadFrames(out int cntFrames)
    {
      lock (mFrames)
      {
        cntFrames = mFrames.Count;
        int count = mUnreadFrameIndex < 0 ? mFrames.Count : mFrames.Count - mUnreadFrameIndex;
        var result = new T[count];
        mFrames.CopyTo(mUnreadFrameIndex, result, 0, count);
        mUnreadFrameIndex = mFrames.Count;
        return result;
      }
    }
    /// <summary>
    /// Starts or stops logging of frames to file.
    /// </summary>
    /// <param name="logToFile">true to start logging to logFilePath, false to stop logging</param>
    /// <param name="logFilePath">The path to log the frames to</param>
    public void updateFileLogSetting(bool logToFile, string logFilePath)
    {
      lock (this)
      {
        if (logToFile
          && mLog != null
          && mActiveLogFilePath.Equals(logFilePath, StringComparison.OrdinalIgnoreCase)
          )
          // already running and the same log file path
          return;

        if (mLog != null)
        { // stop requested
          mLog.Close();
          mLog.Dispose();
          mLog = null;
        }
        if (!logToFile)
          // only stop requested
          return;

        // start new log file
        mActiveLogFilePath = logFilePath;
        try
        {
          string path = Path.Combine(mActiveLogFilePath, LogFileName);
          var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, 16 * UInt16.MaxValue);
          mLog = new StreamWriter(fs, Encoding.UTF8);
          try { mLog.WriteLine(LogFileCSVHeader); }
          catch { }
          mWrittenFrameCtr = 0;
          mDriveInfo = new DriveInfo(Path.GetPathRoot(path).Substring(0, 1));
        }
        catch (IOException) { }
      }
    }
    #endregion
    #region IDisposable Members
    /// <summary>
    /// Closes am opened log.
    /// </summary>
    public virtual void Dispose()
    {
      lock (this)
      {
        if (mLog == null)
          return;
        mLog.Close();
        mLog.Dispose();
      }
    }
    #endregion
  }
}