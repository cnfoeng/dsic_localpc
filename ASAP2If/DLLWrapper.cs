﻿/*!
 * @file    
 * @brief   Implements the ability to dynamically load a native windows DLL.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    MArch 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;

namespace jnsoft.Helpers
{
  /// <summary>
  /// Implements the ability to dynamically load a native windows DLL.
  /// 
  /// Use the Dispose method to free (unload the loaded native DLL).
  /// </summary>
  public abstract class DLLWrapper : IDisposable
  {
    #region Fields
    const string mKernel32DLL = "kernel32.dll";
    /// <summary>The handle to the loaded library.</summary>
    protected IntPtr mHandle = IntPtr.Zero;
#if WIN32
    FileVersionInfo mFileVersionInfo;
#endif
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance of DLLWrapper with the specified parameters. 
    /// </summary>
    /// <param name="pathToDLL">The fully qualified path to a windows native DLL</param>
    /// <exception cref="ArgumentException">Thrown, if the specified file is not found or accessible or isn't a windows native DLL</exception>
    public DLLWrapper(string pathToDLL)
    {
      FilePath = pathToDLL;
      mHandle = LoadLibrary(pathToDLL);
      if (mHandle == IntPtr.Zero)
        throw new ArgumentException($"Failed to load {pathToDLL}!");
      try
      { // try to get the file DLL information
        StringBuilder path = new StringBuilder(256);
        GetModuleFileName(mHandle, path, (UInt32)path.Capacity);
#if WIN32
        mFileVersionInfo = FileVersionInfo.GetVersionInfo(path.ToString());
#endif
      }
      catch { }
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the file path of the library.
    /// </summary>
    public string FilePath { get; private set; }
    /// <summary>
    /// Gets the file version of the loaded DLL.
    /// </summary>
    public string FileVersion { get { return mFileVersionInfo != null ? mFileVersionInfo.FileVersion : string.Empty; } }
    #endregion
    #region methods
    #region windows api
    /// <summary>
    /// Imported Windows API function.
    /// </summary>
    /// <param name="module">the handle to the loaded library</param>
    /// <returns>true if successful</returns>
    [DllImport(mKernel32DLL)]
    static extern bool FreeLibrary(IntPtr module);
    /// <summary>
    /// Imported Windows API function.
    /// </summary>
    /// <param name="filename">The filename of the DLL to load</param>
    /// <returns>true if successful</returns>
    [DllImport(mKernel32DLL)]
    static extern IntPtr LoadLibrary(string filename);
    /// <summary>
    /// Imported Windows API function.
    /// </summary>
    /// <param name="module">the handle to the loaded library</param>
    /// <param name="name">the name of the function within the library</param>
    /// <returns>a function handle to the named function</returns>
    [DllImport(mKernel32DLL)]
    protected static extern IntPtr GetProcAddress(IntPtr module, string name);
    /// <summary>
    /// Imported Windows API function.
    /// </summary>
    /// <param name="module">the handle to the loaded library</param>
    /// <param name="ordinal">the ordinal number of the function</param>
    /// <returns>a function handle to the function</returns>
    [DllImport(mKernel32DLL)]
    protected static extern IntPtr GetProcAddress(IntPtr module, UInt32 ordinal);
    /// <summary>
    /// Retrieves the fully qualified path for the file containing the specified module.
    /// </summary>
    /// <param name="module">A handle to the loaded module whose path is being requested.</param>
    /// <param name="filename">A pointer to a buffer that receives the fully qualified path of the module.</param>
    /// <param name="size">The size of the lpFilename buffer</param>
    /// <returns></returns>
    [DllImport(mKernel32DLL)]
    static extern UInt32 GetModuleFileName(IntPtr module, StringBuilder filename, UInt32 size);
    #endregion
    #endregion
    #region IDisposable Members
    ~DLLWrapper()
    {
      Dispose(false);
    }
    /// <summary>
    /// frees used resources.
    /// </summary>
    /// <param name="dispose"></param>
    public void Dispose(bool dispose)
    {
      if (mHandle != IntPtr.Zero)
      {
        FreeLibrary(mHandle);
        mHandle = IntPtr.Zero;
      }
    }
    /// <summary>
    /// frees used resources.
    /// </summary>
    public virtual void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
    #endregion
  }
}