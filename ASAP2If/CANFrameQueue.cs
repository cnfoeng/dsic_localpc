﻿/*!
 * @file    
 * @brief   Implements a Queue to store CANFrame's.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using jnsoft.ASAP2If.Properties;

namespace jnsoft.Comm.CAN
{
  /// <summary>Serializes CAN filter IDs.</summary>
  public sealed class CANFilterIDs : HashSet<UInt32>
  {
    public CANFilterIDs() { }
    public CANFilterIDs(IEnumerable<UInt32> collection) : base(collection) { }
  }
  /// <summary>
  /// Maps CAN Id to CAN frames.
  /// </summary>
  public sealed class CANFrameDict : SortedDictionary<UInt64, CANFrame> { }
  /// <summary>
  /// Queue implementation to store CANFrame's.
  /// </summary>
  public sealed class CANFrameQueue : FrameQueue<CANFrame>
  {
    static CANFrameQueue mInstance;
    public override string LogFileName { get { return "CANLog.csv"; } }
    public override string LogFileCSVHeader { get { return "Time[s];Source;ID;Len;Data;ASCII"; } }
    /// <summary>
    /// Returns the singleton instance of the queue.
    /// </summary>
    public static CANFrameQueue Instance { get { return mInstance ?? (mInstance = new CANFrameQueue()); } }
    static CANFrameDict mFrameDict = new CANFrameDict();
    /// <summary>
    /// Gets the most recent unique CAN Frames.
    /// 
    /// This can be cleared by clearFrameDict.
    /// </summary>
    public CANFrameDict FrameDict
    {
      get
      {
        var result = new CANFrameDict();
        lock (mFrameDict)
        {
          var en = mFrameDict.GetEnumerator();
          while (en.MoveNext())
            result[en.Current.Key] = en.Current.Value;
          return result;
        }
      }
    }
    /// <summary>
    /// Adds a sent or received frame to the frame queue.
    /// 
    /// This method gets called from various receive threads, 
    /// therefore acces to members is synchronized.
    /// 
    /// The frames are filtered according the global user settings.
    /// </summary>
    /// <param name="frame">The frame to add</param>
    /// <param name="provider">The CAN provider Instance (may be null)</param>
    public void enqueueFrame(CANFrame frame, CANProvider provider)
    {
      if (frame.IsExtendedID)
        J1939FrameQueue.Instance.enqueueFrame(new J1939Frame(frame), provider);

      if (!Settings.Default.CANLogShowCMD && frame.IsMasterFrame)
        return;
      if (!Settings.Default.CANLogShowRES && !frame.IsMasterFrame)
        return;

      if (Settings.Default.CANIDFilter != null
        && Settings.Default.CANIDFilter.Count > 0
        && !Settings.Default.CANIDFilter.Contains(frame.ID)
        )
        return;

      lock (mFrameDict)
      { // keep only filtered items in memory
        UInt64 key = provider != null ? ((UInt64)provider.UniqueBusID << 32 | frame.ID) : frame.ID;
        mFrameDict[key] = frame;
      }

      addFrame(frame);
    }
    public override void clear()
    {
      base.clear();
      lock (this)
        mFrameDict.Clear();
    }
  }
}