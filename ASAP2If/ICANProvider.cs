﻿/*!
 * @file    
 * @brief   Defines the Interface for CAN adapters.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.ComponentModel;

namespace jnsoft.Comm
{
  /// <summary>
  /// All the stuff needed to communicate with CAN hardware.
  /// </summary>
  namespace CAN
  {
    /// <summary>
    /// CAN Adapter interface to implement for each CAN Bus provider.
    /// </summary>
    public interface ICANProvider : IDisposable
    {
      /// <summary>
      /// Gets the unique CAN Bus ID.
      /// </summary>
      int UniqueBusID { get; }
      /// <summary>
      /// Returns true if the CAN adapter is available (installed).
      /// </summary>
      /// <returns>true if the CAN adapter is available</returns>
      bool isAvailable();
      /// <summary>
      /// May open a UI and lets the user configure the CAN adapter to use.
      /// </summary>
      /// <param name="hWnd">The parent window</param>
      /// <param name="configuration">[in/out] The CAN configuration</param>
      /// <returns>true, if the adapter is configured correctly</returns>
      bool configure(IWin32Window hWnd, CANConfiguration configuration);
      /// <summary>
      /// Open the CAN resource
      /// </summary>
      /// <param name="configuration">The required CAN configuration</param>
      /// <returns>true if successfully opened</returns>
      bool open(CANConfiguration configuration);
      /// <summary>
      /// Close the CAN resource
      /// </summary>
      void close();
      /// <summary>
      /// Send data to the CAN resource.
      /// 
      /// - Use the uppermost Bit (Bit 31, zero based) to mark the canID as extended CAN ID (see CANProvider.CAN_EXT_FLAG).
      /// - If the canID to send is read from a DBC file (jnsoft.DBC.DBCFile.MsgType.ID), it is already in the correct format.
      /// 
      /// <example>Usage example (send 2 bytes of data with a standard CAN ID):
      /// <code>
      /// var canProvider = new Comm.CAN.Vector.VectorCAN();
      /// var data = new byte[] { 0x01, 0x02 };
      /// if (data.Length == canProvider.send(1, ref data))
      ///   Console.WriteLine("data successfully sent!");
      /// </code>
      /// </example>
      /// <example>Usage example (send 2 bytes of data with an extended CAN ID):
      /// <code>
      /// var canProvider = new Comm.CAN.Vector.VectorCAN();
      /// var data = new byte[] { 0x01, 0x02 };
      /// if (data.Length == canProvider.send(1 | CANProvider.CAN_EXT_FLAG, ref data))
      ///   Console.WriteLine("data successfully sent!");
      /// </code>
      /// </example>
      /// </summary>
      /// <param name="canID">
      /// The CAN ID to send:
      /// - A standard CAN Identifier is valid between 0..0x7ff (11 Bit)
      /// - An extended CAN Identifier is valid between 0..1fffff (29 Bit). In this case you have to mark the canID parameter with the CANProvider.CAN_EXT_FLAG.
      /// </param>
      /// <param name="data">
      /// The data to send (the valid length of the data depends on the configured CAN type and the caller is responsible for).
      /// - For CAN the length is valid from 0..8 bytes
      /// - For CAN FD the length is valid from 0..8, 12, 16, 20, 24, 32, 48 and 64 bytes.
      /// </param>
      /// <returns>The count of bytes sent</returns>
      int send(UInt32 canID, ref byte[] data);
      /// <summary>
      /// Registers a CAN listener for all CANIDs.
      /// 
      /// To avoid memory leaks any registered handler must
      /// be unregistered on disposing the client instance
      /// by calling unregisterClient!
      /// </summary>
      /// <param name="handler">The CAN listener event handler instance</param>
      void registerClient(EventHandler<CANEventArgs> handler);
      /// <summary>
      /// Registers a CAN listener for a specific CAN ID.
      /// 
      /// To avoid memory leaks any registered handler must
      /// be unregistered on disposing the client instance
      /// by calling unregisterClient!
      /// </summary>
      /// <param name="canID">The CAN ID to listen to</param>
      /// <param name="handler">The CAN listener event handler instance</param>
      void registerClient(UInt32 canID, EventHandler<CANEventArgs> handler);
      /// <summary>
      /// Registers a specific CAN handler.
      /// 
      /// To avoid memory leaks this handler must
      /// be unregistered on disposing the client instance
      /// by calling unregisterClient!
      /// </summary>
      /// <param name="canIds">The list of specific CAN IDs</param>
      /// <param name="handler">The handler to be called</param>
      void registerClient(UInt32[] canIds, EventHandler<CANEventArgs> handler);
      /// <summary>
      /// Registers a XCP/CCP specific CAN handler.
      /// 
      /// To avoid memory leaks this handler must
      /// be unregistered on disposing the XCP/CCP client instance
      /// by calling unregisterClient!
      /// </summary>
      /// <param name="canXCPCCPIds">The list of XCP/CCP specific CAN IDs used for Command, Response and DAQ messages</param>
      /// <param name="handler">The handler to be called</param>
      void registerXCPCCPClient(UInt32[] canXCPCCPIds, EventHandler<CANEventArgs> handler);
      /// <summary>
      /// Unregisters a CAN listener.
      /// 
      /// To avoid memory leaks this method must
      /// be called on disposing the client instance!
      /// </summary>
      /// <param name="handler">The CAN listener event handler instance</param>
      void unregisterClient(EventHandler<CANEventArgs> handler);
      /// <summary>
      /// Cyclically called to receive CAN frames.
      /// 
      /// - Provides all CAN frames read from the CAN bus 
      /// - The method should never block
      /// </summary>
      /// <param name="frame">The CAN frame received or sent, or null if no frame is received or sent</param>
      /// <returns>0 if no error occured (The frame parameter may be null in this case, too)</returns>
      int onReceive(out CANFrame frame);
      /// <summary>
      /// Gets the current bus load.
      /// 
      /// - Needs at least two calls in activated state to get correct values.
      /// </summary>
      /// <param name="bitsPerSec">Gets the count of bits transferred per second</param>
      /// <param name="msgPerSec">Gets the count of messages transferred per second</param>
      void getBusload(out double bitsPerSec, out int msgPerSec);
    }
    /// <summary>
    /// Defines possible CAN FD Baudrates.
    /// </summary>
    public enum CANFDBaudrate
    {
      /// <summary>CAN FD is not used</summary>
      [Description("Not used")]
      NotUsed,
      /// <summary>500 kBit/s</summary>
      [Description("500 kBit/s")]
      _500kBit = 500000,
      /// <summary>1 MBit/s</summary>
      [Description("1 MBit/s")]
      _1MBit = 1000000,
      /// <summary>2 MBit/s</summary>
      [Description("2 MBit/s")]
      _2MBit = 2000000,
      /// <summary>4 MBit/s</summary>
      [Description("4 MBit/s")]
      _4MBit = 4000000,
      /// <summary>8 MBit/s</summary>
      [Description("8 MBit/s")]
      _8MBit = 8000000,
    }
    /// <summary>
    /// Defines possible CAN Baudrates.
    /// </summary>
    public enum CANBaudrate
    {
      /// <summary>Not set value</summary>
      [Description("Not set")]
      NotSet = 0,
      /// <summary>10 kBit/s</summary>
      [Description("10 kBit/s")]
      _10kBit = 10000,
      /// <summary>20 kBit/s</summary>
      [Description("20 kBit/s")]
      _20kBit = 20000,
      /// <summary>50 kBit/s</summary>
      [Description("50 kBit/s")]
      _50kBit = 50000,
      /// <summary>100 kBit/s</summary>
      [Description("100 kBit/s")]
      _100kBit = 100000,
      /// <summary>125 kBit/s</summary>
      [Description("125 kBit/s")]
      _125kBit = 125000,
      /// <summary>250 kBit/s</summary>
      [Description("250 kBit/s")]
      _250kBit = 250000,
      /// <summary>500 kBit/s</summary>
      [Description("500 kBit/s")]
      _500kBit = 500000,
      /// <summary>800 kBit/s</summary>
      [Description("800 kBit/s")]
      _800kBit = 800000,
      /// <summary>1 MBit/s</summary>
      [Description("1 MBit/s")]
      _1MBit = 1000000,
      /// <summary>2 MBit/s</summary>
      [Description("2 MBit/s")]
      _2MBit = 2000000,
      /// <summary>4 MBit/s</summary>
      [Description("4 MBit/s")]
      _4MBit = 4000000,
      /// <summary>8 MBit/s</summary>
      [Description("8 MBit/s")]
      _8MBit = 8000000,
    }
    /// <summary>
    /// Represents the vendor(hardware) independant CAN configuration.
    /// 
    /// - May be used to serialize and deserialize CAN settings.
    /// - May be extended with additional settings for a CAN Bus configuration.
    /// </summary>
    public class CANConfiguration
    {
      /// <summary>
      /// The CAN channel to communicate on.
      /// </summary>
      [XmlAttribute, DefaultValue(0)]
      public int Channel;
      /// <summary>
      /// The baudrate to configure.
      /// </summary>
      [XmlAttribute, DefaultValue(CANBaudrate.NotSet)]
      public CANBaudrate Baudrate;
      /// <summary>
      /// The CAN FD baudrate to configure.
      /// 
      /// If this Buadrate is used, the Baudrate member must be set to an concrete arbitration Baudrate, too.
      /// </summary>
      [XmlAttribute, DefaultValue(CANFDBaudrate.NotUsed)]
      public CANFDBaudrate BaudrateFD;
      /// <summary>
      /// Hardwaretye (currently use for vector configuration).
      /// </summary>
      [XmlAttribute, DefaultValue(0)]
      public int HardwareType;
      /// <summary>
      /// A potentially used (virtual) COM port to access the CAN device.
      /// </summary>
      [XmlAttribute, DefaultValue(null)]
      public string COMPort;
      /// <summary>
      /// A potentially used COMPort configuration (something like '9600,8,n,1')
      /// </summary>
      [XmlAttribute, DefaultValue(null)]
      public string COMPortConfig;
      /// <summary>
      /// Deserialization constructor.
      /// </summary>
      public CANConfiguration() { }
      /// <summary>
      /// Initialises a new instance with the specified parameters.
      /// </summary>
      /// <param name="channel">The channel to configure</param>
      /// <param name="baudrate">The baudrate to configure</param>
      /// <param name="baudrateFD">The CAN FD data baudrate to configure</param>
      public CANConfiguration(int channel = 0
        , CANBaudrate baudrate = CANBaudrate.NotSet
        , CANFDBaudrate baudrateFD = CANFDBaudrate.NotUsed
        )
      { Channel = channel; Baudrate = baudrate; BaudrateFD = baudrateFD; }
    }
  }
}