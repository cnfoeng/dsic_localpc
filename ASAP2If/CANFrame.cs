﻿/*!
 * @file    
 * @brief   Implements the CAN message representation.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;

namespace jnsoft.Comm.CAN
{
  /// <summary>
  /// Represents a sent or received CAN message.
  /// </summary>
  public class CANFrame : Frame
  {
    #region members
    const int mRawFrame11BitMin = 44;
    const int mRawFrame29BitMin = 64;
    const string mCSVFormat = "{0};\"{1}\";\"{2}\";{3};\"{4}\";\"{5}\"";
    const string mClpFormat = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}";
    /// <summary>
    /// The CAN Bus source identifcation (free text, maybe something like 'Vector virtual CAN Bus 1')
    /// </summary>
    public string BusID;
    #endregion
    #region constructors
    /// <summary>
    /// Initialises a new instance of CANFrame with the specified parameters.
    /// </summary>
    /// <param name="busID">The CAN Bus identification</param>
    /// <param name="canID">The CAN ID from where the frame is sent or received</param>
    /// <param name="data">The complete frame data</param>
    /// <param name="len">THe data length</param>
    /// <param name="isMasterFrame">true if the frame is sent by the master, else false</param>
    public CANFrame(ref string busID, UInt32 canID, UInt64 data, byte len, bool isMasterFrame)
    {
      BusID = busID;
      ID = canID;
      IsMasterFrame = isMasterFrame;
      Data = new byte[len];
      for (int i = 0; i < len; ++i)
      {
        Data[i] = (byte)(data & 0xff);
        data >>= 8;
      }
    }
    /// <summary>
    /// Initialises a new instance of CANFrame with the specified parameters.
    /// </summary>
    /// <param name="busID">The CAN Bus identification</param>
    /// <param name="canID">The CAN ID from where the frame is sent or received</param>
    /// <param name="data">The complete frame data</param>
    /// <param name="isMasterFrame">true if the frame is sent by the master, else false</param>
    public CANFrame(ref string busID, UInt32 canID, ref byte[] data, bool isMasterFrame)
    {
      BusID = busID;
      ID = canID;
      Data = data;
      IsMasterFrame = isMasterFrame;
    }
    /// <summary>
    /// Initialises a new instance of CANFrame with the specified parameters.
    /// </summary>
    /// <param name="busID">The CAN Bus identification</param>
    /// <param name="canID">The CAN ID from where the frame is sent or received</param>
    /// <param name="data">The complete frame data</param>
    /// <param name="len">The length of data</param>
    /// <param name="isMasterFrame">true if the frame is sent by the master, else false</param>
    public CANFrame(ref string busID, UInt32 canID, ref byte[] data, byte len, bool isMasterFrame)
    {
      BusID = busID;
      ID = canID;
      IsMasterFrame = isMasterFrame;
      if (len == data.Length)
        // takeover
        Data = data;
      else
      { // copy the data
        Data = new byte[len];
        Array.Copy(data, Data, len);
      }
    }
    #endregion
    #region properties
    /// <summary>
    /// The address sent or received from.
    /// </summary>
    public string Address { get { return $"{(IsMasterFrame ? "\u2192" : "\u2190")} {CANProvider.toCANIDString(ID)}"; } }
    /// <summary>
    /// Gets the CAN ID sent or received.
    /// 
    /// Bit 31 is used to mark extended CAN IDs (see CANProvider.CAN_EXT_FLAG).
    /// </summary>
    public UInt32 ID { get; private set; }
    /// <summary>
    /// Gets the CAN ID sent or received always without a potential extended flag (Bit 31, zero based) set.
    /// </summary>
    public UInt32 RawID { get { return ID & ~CANProvider.CAN_EXT_FLAG; } }
    /// <summary>
    /// Indicates if the ID is an extended CAN Id.
    /// </summary>
    public bool IsExtendedID { get { return (ID & CANProvider.CAN_EXT_FLAG) > 0; } }
    #endregion
    #region methods
    /// <summary>
    /// Builds a CSV representation of the CAN frame.
    /// </summary>
    /// <returns>the string representation</returns>
    public override string toCSV()
    {
      return string.Format(mCSVFormat
        , getTimeStr()
        , BusID
        , Address
        , Data.Length
        , getDataStr()
        , getDataASCIIStr('"')
        );
    }
    /// <summary>
    /// Builds a tab delimited representation of the CAN frame.
    /// </summary>
    /// <returns>the string representation</returns>
    public override string toClipboard()
    {
      return string.Format(mClpFormat
        , getTimeStr()
        , BusID
        , Address
        , Data.Length
        , getDataStr()
        , getDataASCIIStr('\t')
        );
    }
    /// <summary>
    /// Gets the frame length in Bits on the CAN Bus depending on the CAN ID used.
    /// </summary>
    /// <returns>The frame length in Bits on the CAN bus</returns>
    public int getRawFrameLength()
    {
      return getRawFrameLength(ID, ref Data);
    }
    /// <summary>
    /// Gets the frame length in Bits on the CAN Bus depending on the CAN ID used.
    /// </summary>
    /// <param name="canID">The CAN Id used</param>
    /// <param name="data">The CAN msg data</param>
    /// <returns>The frame length in Bits on the CAN bus</returns>
    public static int getRawFrameLength(UInt32 canID, ref byte[] data)
    {
      int baseLen = (canID & CANProvider.CAN_EXT_FLAG) > 0
        ? mRawFrame29BitMin
        : mRawFrame11BitMin;
      return baseLen + data.Length * 8;
    }
    #endregion
  }
}