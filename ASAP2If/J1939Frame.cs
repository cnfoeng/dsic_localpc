﻿/*!
 * @file    
 * @brief   Implements the J1939 message representation.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    December 2016
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;

namespace jnsoft.Comm.CAN
{
  /// <summary>
  /// Represents a sent or received J1939 message.
  /// </summary>
  public sealed class J1939Frame : CANFrame
  {
    #region members
    const string mCSVFormat = "{0};\"{1}\";\"{2}\";{3};{4};{5};{6};{7};{8};{9};{10};\"{11}\";\"{12}\"";
    const string mClpFormat = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{8}\t{10}\t{11}\t{12}";
    #endregion
    #region constructors
    /// <summary>
    /// Initialises the J1939 frame from a CAN frame
    /// </summary>
    /// <param name="frame"></param>
    public J1939Frame(CANFrame frame)
      : base(ref frame.BusID, frame.ID, ref frame.Data, frame.IsMasterFrame)
    {

    }
    #endregion
    #region properties
    /// <summary>
    /// Message priority.
    /// 
    /// A value of 000 has the highest priority. Higher priority messages would typically be used for high speed control messages.
    /// </summary>
    public byte Priority { get { return (byte)((ID >> 26) & 0x07); } }
    /// <summary>
    /// Data Page (DP) bit.
    /// 
    /// The DP bit is used as a page selector. Page 0 contains all the messages which are
    /// presently being defined.Page 1 provides additional expansion capacity for the future.
    /// </summary>
    public byte DataPage { get { return (byte)((ID >> 24) & 0x01); } }
    /// <summary>
    /// PDU Format (PF) field.
    /// 
    /// The PF field identifies one of two PDU formats able to be transmitted.
    /// </summary>
    public byte PDUFormat { get { return (byte)((ID >> 16) & 0xff); } }
    /// <summary>
    /// PDU Specific (PS).
    /// 
    /// If the PF value is between 0 and 239 (PDU1), this PS field contains a destination address.
    /// If the PF field is between 240 and 255 (PDU2), the PS field contains a Group Extension(GE) 
    /// to the PDU Format.The Group Extension provides a larger set of values to identify messages 
    /// which can be broadcast to all ECUs on the network.
    /// </summary>
    public byte PDUSpecific { get { return (byte)((ID >> 8) & 0xff); } }
    /// <summary>
    /// Parameter Group.
    /// </summary>
    public UInt32 PG { get { return (ID >> 8) & 0x3ffff; } }
    /// <summary>
    /// Parameter Group Number.
    /// </summary>
    public UInt32 PGN { get { return (ID >> 8) & 0x1fffff; } }
    /// <summary>
    /// Source Address-
    /// 
    /// </summary>
    public byte SourceAddress { get { return (byte)(ID & 0xff); } }
    #endregion
    #region methods
    /// <summary>
    /// Builds a CSV representation of the CAN frame.
    /// </summary>
    /// <returns>the string representation</returns>
    public override string toCSV()
    {
      return string.Format(mCSVFormat
        , getTimeStr()
        , BusID
        , (ID & CANProvider.CAN_EXT_ID_MASK).ToString("X")
        , PGN
        , Priority
        , PG
        , DataPage
        , PDUFormat
        , PDUSpecific
        , SourceAddress
        , Data.Length
        , getDataStr()
        , getDataASCIIStr('"')
        );
    }
    /// <summary>
    /// Builds a tab delimited representation of the CAN frame.
    /// </summary>
    /// <returns>the string representation</returns>
    public override string toClipboard()
    {
      return string.Format(mClpFormat
        , getTimeStr()
        , BusID
        , (ID & CANProvider.CAN_EXT_ID_MASK).ToString("X")
        , PGN
        , Priority
        , PG
        , DataPage
        , PDUFormat
        , PDUSpecific
        , SourceAddress
        , Data.Length
        , getDataStr()
        , getDataASCIIStr('\t')
        );
    }
    #endregion
  }
}