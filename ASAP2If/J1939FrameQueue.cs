﻿/*!
 * @file    
 * @brief   Implements a Queue to store J1939Frame's.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    December 2016
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using jnsoft.ASAP2If.Properties;

namespace jnsoft.Comm.CAN
{
  /// <summary>
  /// Maps J1939 Id to J1939 frames.
  /// </summary>
  public sealed class J1939FrameDict : SortedDictionary<UInt64, J1939Frame> { }
  /// <summary>
  /// Queue implementation to store J1939 Frame's.
  /// </summary>
  public sealed class J1939FrameQueue : FrameQueue<J1939Frame>
  {
    static J1939FrameQueue mInstance;
    public override string LogFileName { get { return "J1939Log.csv"; } }
    public override string LogFileCSVHeader { get { return "Time[s];Source;ID;PGN;Prio;PG;DP;PF;PS;SA;Len;Payload;ASCII"; } }
    /// <summary>
    /// Returns the singleton instance of the queue.
    /// </summary>
    public static J1939FrameQueue Instance { get { return mInstance ?? (mInstance = new J1939FrameQueue()); } }
    static J1939FrameDict mFrameDict = new J1939FrameDict();
    /// <summary>
    /// Gets the most recent unique CAN Frames.
    /// 
    /// This can be cleared by clearFrameDict.
    /// </summary>
    public J1939FrameDict FrameDict
    {
      get
      {
        var result = new J1939FrameDict();
        lock (mFrameDict)
        {
          var en = mFrameDict.GetEnumerator();
          while (en.MoveNext())
            result[en.Current.Key] = en.Current.Value;
          return result;
        }
      }
    }
    /// <summary>
    /// Adds a sent or received frame to the frame queue.
    /// 
    /// This method gets called from various receive threads, 
    /// therefore acces to members is synchronized.
    /// 
    /// The frames are filtered according the global user settings.
    /// </summary>
    /// <param name="frame">The frame to add</param>
    /// <param name="provider">The CAN provider Instance (may be null)</param>
    public void enqueueFrame(J1939Frame frame, CANProvider provider)
    {
      if (!Settings.Default.J1939LogShowCMD && frame.IsMasterFrame)
        return;
      if (!Settings.Default.J1939LogShowRES && !frame.IsMasterFrame)
        return;

      if (Settings.Default.J1939IDFilter != null 
        && Settings.Default.J1939IDFilter.Count>0 
        && !Settings.Default.J1939IDFilter.Contains(frame.ID)
        )
        return;

      lock (mFrameDict)
      { // keep only filtered items in memory
        UInt64 key = provider != null ? ((UInt64)provider.UniqueBusID << 32 | frame.ID) : frame.ID;
        mFrameDict[key] = frame;
      }

      addFrame(frame);
    }
  }
}