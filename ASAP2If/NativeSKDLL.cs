﻿/*!
 * @file    
 * @brief   Implements a wrapper for Seed&Key DLLs.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    February 2017
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace jnsoft.Helpers
{
  /// <summary>
  /// Available Seed and key DLL type.
  /// </summary>
  [Flags]
  public enum SKType
  {
    /// <summary>
    /// Does not contain CCP nor XCP Sed and Key methods.
    /// </summary>
    Unknown,
    /// <summary>
    /// Contains the CCP Seed and key method.
    /// </summary>
    CCP = 0x01,
    /// <summary>
    /// Contains XCP Seed and key methods.
    /// </summary>
    XCP = CCP << 1,
    /// <summary>
    /// Contains UDS Seed and key methods.
    /// </summary>
    UDS = CCP << 2,
  }
  /// <summary>
  /// Allows access to (native Windows) Seed&Key DLLs.
  /// 
  /// - To compute a CCP key from a seed, the DLL must contain the function ASAP1A_CCP_ComputeKeyFromSeed
  /// - To compute a XCP key from a seed, the DLL must contain the functions XCP_GetAvailablePrivileges and XCP_ComputeKeyFromSeed
  /// - When running In a 64 Bit Process the DLL must be a 64 Bit DLL too!
  /// </summary>
  public sealed class NativeSKDLL : DLLWrapper
  {
    /// <summary>
    /// XCP: Possible native Seed&Key return values.
    /// </summary>
    public enum ResultSk
    {
      /// <summary>
      /// Function call was successful
      /// </summary>
      FncAck,
      /// <summary>
      /// Requested privilege is not available
      /// </summary>
      ErrPrivilegeNotAvailable,
      /// <summary>
      /// Seed length is invalid.
      /// </summary>
      ErrInvalidSeedLength,
      /// <summary>
      /// Key length is not sufficient
      /// </summary>
      ErrUnsufficientKeyLength,
      /// <summary>
      /// All other errors
      /// </summary>
      ErrGlobal,
    }
    /// <summary>UDS GenerateKeyEx result.</summary>
    public enum VKeyGenResultEx
    {
      /// <summary>Ok</summary>
      Ok = 0,
      /// <summary>Specified buffer too small</summary>
      BufferToSmall = 1,
      /// <summary>Specified security level is invalid</summary>
      SecurityLevelInvalid = 2,
      /// <summary>Specified variant is invalid</summary>
      VariantInvalid = 3,
      /// <summary>Unspecified error</summary>
      UnspecifiedError = 4,
    }

    #region members
    const string mStrGetAvalaiblePriv = "XCP_GetAvailablePrivileges";
    const string mStrComputeKeyFromSeed = "XCP_ComputeKeyFromSeed";
    const string mStrCCPComputeKeyFromSeed = "ASAP1A_CCP_ComputeKeyFromSeed";
    string[] mStrCCPSeed2Key = { "seed2key", "seed2Key", "Seed2Key" };
    const string mStrUDSComputeKeyFromSeed = "GenerateKeyEx";
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    delegate ResultSk FncGetAvailablePriv(ref byte privilege);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    delegate ResultSk FncComputeKeyFromSeed(byte privilege, byte seedLen, byte[] seed, ref byte keyLen, IntPtr res);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    delegate bool FncCCPComputeKeyFromSeed(byte[] seed, UInt16 sizeSeed, byte[] key, UInt16 maxSizeKey, ref UInt16 sizeKey);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    private delegate uint FncSeed2Key(string request, ref uint value);
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    delegate VKeyGenResultEx FncUDSComputeKeyFromSeed(byte[] seed, uint sizeSeed, uint securityLevel, byte[] variant, byte[] key, uint maxSizeKey, out uint sizeKey);
    FncGetAvailablePriv mXCPGetAvailPriv;
    FncComputeKeyFromSeed mXCPSeedKey;
    FncCCPComputeKeyFromSeed mCCPSeedKey;
    FncSeed2Key mCCPSeed2Key;
    FncUDSComputeKeyFromSeed mUDSSeedKey;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance of DLLWrapper with the specified parameters. 
    /// </summary>
    /// <param name="pathToDLL">The fully qualified path to a windows native DLL</param>
    /// <exception cref="ArgumentException">Thrown, if the specified file is not found or accessible or isn't a windows native DLL</exception>
    public NativeSKDLL(string pathToDLL)
      : base(pathToDLL)
    {
      if (mHandle == IntPtr.Zero)
        return;

      // Test for XCP
      var ptr = GetProcAddress(mHandle, mStrGetAvalaiblePriv);
      if (ptr != IntPtr.Zero)
        mXCPGetAvailPriv = ((FncGetAvailablePriv)Marshal.GetDelegateForFunctionPointer(ptr, typeof(FncGetAvailablePriv)));

      ptr = GetProcAddress(mHandle, mStrComputeKeyFromSeed);
      if (ptr != IntPtr.Zero)
        mXCPSeedKey = ((FncComputeKeyFromSeed)Marshal.GetDelegateForFunctionPointer(ptr, typeof(FncComputeKeyFromSeed)));

      if (mXCPGetAvailPriv != null & mXCPSeedKey != null)
        Type |= SKType.XCP;

      // Test for CCP
      ptr = GetProcAddress(mHandle, mStrCCPComputeKeyFromSeed);
      if (ptr != IntPtr.Zero)
        mCCPSeedKey = ((FncCCPComputeKeyFromSeed)Marshal.GetDelegateForFunctionPointer(ptr, typeof(FncCCPComputeKeyFromSeed)));

      foreach( var seed2key in mStrCCPSeed2Key)
      {
        ptr = GetProcAddress(mHandle, seed2key);
        if (ptr == IntPtr.Zero)
          continue;
        mCCPSeed2Key = ((FncSeed2Key)Marshal.GetDelegateForFunctionPointer(ptr, typeof(FncSeed2Key)));
        break;
      }

      if (mCCPSeedKey != null || mCCPSeed2Key != null)
        Type |= SKType.CCP;

      // Test for UDS
      ptr = GetProcAddress(mHandle, mStrUDSComputeKeyFromSeed);
      if (ptr != IntPtr.Zero)
        mUDSSeedKey = ((FncUDSComputeKeyFromSeed)Marshal.GetDelegateForFunctionPointer(ptr, typeof(FncUDSComputeKeyFromSeed)));

      if (mUDSSeedKey != null)
        Type |= SKType.UDS;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the type of the DLL.
    /// 
    /// May be a combination of CCP, XCP, UDS.
    /// </summary>
    public SKType Type { get; }
    #endregion
    #region methods
    /// <summary>
    /// Gets the supported privileges of the seed&key dll.
    /// </summary>
    /// <param name="privileges">The privileges (jnsoft.Comm.XCP.ResourceType)</param>
    /// <returns>A ResultSk value</returns>
    public ResultSk GetAvailablePrivileges(ref byte privileges)
    {
      if (mXCPGetAvailPriv == null)
        // function not available
        return ResultSk.ErrGlobal;
      try { return mXCPGetAvailPriv(ref privileges); }
      catch { return ResultSk.ErrGlobal; }
    }

    /// <summary>
    /// CCP: Computes the key from a specified seed.
    /// </summary>
    /// <param name="seed">The seed to get the key for</param>
    /// <param name="resultingKey">The resulting key (may be null if return value is false)</param>
    /// <returns>true if successful</returns>
    public bool ComputeKeyFromSeed(byte[] seed, out byte[] resultingKey)
    {
      resultingKey = null;
      if ((Type & SKType.CCP) == 0)
        // function not available
        return false;

      if (null != mCCPSeed2Key)
      {
        var requestKey = new StringBuilder("1 ");
        foreach (var s in seed)
          requestKey.Append($"{s} ");
        requestKey.Remove(requestKey.Length - 1, 1);

        uint value = 0;
        uint result = mCCPSeed2Key(requestKey.ToString(), ref value);
        resultingKey = BitConverter.GetBytes(value);
        return true;
      }

      if (null != mCCPSeedKey)
      {
        UInt16 sizeKey = 0;
        //var key = new byte[byte.MaxValue];
        var key = new byte[seed.Length];
        bool result = mCCPSeedKey(seed, (UInt16)seed.Length, key, (ushort)key.Length, ref sizeKey);
        if (result && sizeKey > 0 && sizeKey <= key.Length)
        { // copy if data is delivered
          resultingKey = new byte[sizeKey];
          Array.Copy(key, resultingKey, sizeKey);
        }
        return result;
      }

      return false;
    }

    /// <summary>
    /// XCP: Computes the key from a specified seed.
    /// </summary>
    /// <param name="privilege">The privilege (jnsoft.Comm.XCP.ResourceType) to compute the seed for</param>
    /// <param name="seed">The seed</param>
    /// <param name="resultingKey">The resulting key</param>
    /// <returns>A ResultSk value</returns>
    public ResultSk ComputeKeyFromSeed(byte privilege, byte[] seed, out byte[] resultingKey)
    {
      resultingKey = null;
      if (mXCPSeedKey == null)
        // function not available
        return ResultSk.ErrGlobal;

      if (seed == null || seed.Length > byte.MaxValue)
        return ResultSk.ErrGlobal;

      IntPtr res = IntPtr.Zero;
      try
      {
        byte resultLen = byte.MaxValue;
        res = Marshal.AllocHGlobal(resultLen);
        ResultSk result;
        if (ResultSk.FncAck == (result = mXCPSeedKey(privilege
          , (byte)seed.Length
          , seed
          , ref resultLen
          , res
          )))
        { // copy result
          resultingKey = new byte[resultLen];
          Marshal.Copy(res, resultingKey, 0, resultLen);
        }
        return result;
      }
      catch { return ResultSk.ErrGlobal; }
      finally
      {
        if (res != IntPtr.Zero)
          Marshal.FreeHGlobal(res);
      }
    }

    /// <summary>
    /// UDS: Computes the key from a specified seed.
    /// </summary>
    /// <param name="securityLevel">The security level</param>
    /// <param name="variant">The used variant</param>
    /// <param name="seed">The seed to get the key for</param>
    /// <param name="resultingKey">The resulting key (may be null if return value is false)</param>
    /// <returns>true if successful</returns>
    public VKeyGenResultEx ComputeKeyFromSeed(uint securityLevel, byte[] variant, byte[] seed, out byte[] resultingKey)
    {
      resultingKey = null;
      if (mUDSSeedKey == null)
        // function not available
        return VKeyGenResultEx.UnspecifiedError;

      uint maxSizeKey = 255;
      byte[] key = new byte[maxSizeKey];
      uint sizeKey;
      var result = mUDSSeedKey(seed, (uint)seed.Length, securityLevel, variant, key, maxSizeKey, out sizeKey);
      if (result == VKeyGenResultEx.Ok && sizeKey > 0 && sizeKey <= maxSizeKey)
      { // copy if data is delivered
        resultingKey = new byte[sizeKey];
        Array.Copy(key, resultingKey, sizeKey);
      }
      return result;
    }
    #endregion
  }
}