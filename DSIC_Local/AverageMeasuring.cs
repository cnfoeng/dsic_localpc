﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local
{
    public partial class AverageMeasuring : DevExpress.XtraEditors.XtraForm
    {
        int elapsed;
        int count;
        string[] varNames = new string[8];
        double[] averagingValues = new double[8];
        public AverageMeasuring()
        {
            InitializeComponent();

            tbSpeedVarName.Text = Main.project.AveragingValue1;
            tbCorrectedWeightVarName.Text = Main.project.AveragingValue2;
            tbCorrectedTorqueVarName.Text = Main.project.AveragingValue3;
            tbCorrectedPowerVarName.Text = Main.project.AveragingValue4;
            tbFuelEfficiencyVarName.Text = Main.project.AveragingValue5;
            tbOilPressureVarName.Text = Main.project.AveragingValue6;
            tbOption1VarName.Text = Main.project.AveragingValue7;
            tbOption2VarName.Text = Main.project.AveragingValue8;

            tbAveragingTime.Text = Main.project.AveragingTime.ToString();
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            varNames[0] = tbSpeedVarName.Text;
            varNames[1] = tbCorrectedWeightVarName.Text;
            varNames[2] = tbCorrectedTorqueVarName.Text;
            varNames[3] = tbCorrectedPowerVarName.Text;
            varNames[4] = tbFuelEfficiencyVarName.Text;
            varNames[5] = tbOilPressureVarName.Text;
            varNames[6] = tbOption1VarName.Text;
            varNames[7] = tbOption2VarName.Text;

            elapsed = 0;
            count = 0;
            for (int i = 0; i < averagingValues.Length; i++)
            {
                averagingValues[i] = 0;
            }

            progressBarControl1.Properties.Maximum = int.Parse(tbAveragingTime.Text) * 1000;

            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (elapsed > int.Parse(tbAveragingTime.Text) * 1000)
            {
                timer1.Stop();

                for(int i=0; i<averagingValues.Length; i++)
                {
                    var variable = Main.project.sysVarInfo.FindVariable(varNames[i]);
                    if (variable != null)
                    {
                        averagingValues[i] = averagingValues[i] / count;
                        averagingValues[i] = Math.Round(averagingValues[i], variable.Round);
                    }
                }

                tbSpeedValue.Text = averagingValues[0].ToString();
                tbCorrectedWeightValue.Text = averagingValues[1].ToString();
                tbCorrectedTorqueValue.Text = averagingValues[2].ToString();
                tbCorrectedPowerValue.Text = averagingValues[3].ToString();
                tbFuelEfficiencyValue.Text = averagingValues[4].ToString();
                tbOilPressureValue.Text = averagingValues[5].ToString();
                tbOption1Value.Text = averagingValues[6].ToString();
                tbOption2Value.Text = averagingValues[7].ToString();
                
            }
            else
            {
                elapsed += timer1.Interval;

                for (int i = 0; i < averagingValues.Length; i++)
                {
                    var variable = Main.project.sysVarInfo.FindVariable(varNames[i]);
                    if (variable != null)
                    {
                        averagingValues[i] += Main.project.sysVarInfo.FindVariable(varNames[i]).RealValue;
                    }
                }
                progressBarControl1.EditValue = elapsed;
                count++;
            }
        }

        private void AverageMeasuring_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.project.AveragingValue1 = tbSpeedVarName.Text;
            Main.project.AveragingValue2 = tbCorrectedWeightVarName.Text;
            Main.project.AveragingValue3 = tbCorrectedTorqueVarName.Text;
            Main.project.AveragingValue4 = tbCorrectedPowerVarName.Text;
            Main.project.AveragingValue5 = tbFuelEfficiencyVarName.Text;
            Main.project.AveragingValue6 = tbOilPressureVarName.Text;
            Main.project.AveragingValue7 = tbOption1VarName.Text;
            Main.project.AveragingValue8 = tbOption2VarName.Text;

            Main.project.AveragingTime = int.Parse(tbAveragingTime.Text);

            Main.averagingMeasuringForm = null;
        }
        
    }
}