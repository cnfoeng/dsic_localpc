﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace DSIC_Local.Alarm
{
    public class AlarmClass
    {
        public static bool alarmRun = false;
        public static List<VariableInfo> listWarning = new List<VariableInfo>();
        public static List<VariableInfo> listAlarm = new List<VariableInfo>();

        public static void Start_Alarm()
        {
            alarmRun = true;
            Stopwatch sw = new Stopwatch();

            while (alarmRun)
            {
                
                sw.Start();

                foreach (VariableInfo var in Main.project.sysVarInfo.ALL_Variables)
                {
                    if (var.AlarmUse)
                    {

                        if (var.RealValue <= var.Alarm_Low || var.RealValue >= var.Alarm_High)
                        {
                            if (!var.AlarmAlreadyError)
                            {
                                var.AlarmStartTime = sw.Elapsed;
                                var.AlarmAlreadyError = true;
                            }

                            var.AlarmElapsedTime = sw.Elapsed;

                            TimeSpan timeGap = var.AlarmElapsedTime.Subtract(var.AlarmStartTime);

                            if (timeGap.Seconds > var.AlarmAllowableTime)
                            {
                                var.alarmState = AlarmState.Alarm;
                            }
                        }
                        else if (var.RealValue <= var.Warning_Low || var.RealValue >= var.Warning_High)
                        {

                            if (!var.WarningAlreadyError)
                            {
                                var.WarningStartTime = sw.Elapsed;
                                var.WarningAlreadyError = true;
                            }

                            var.WarningElapsedTime = sw.Elapsed;

                            TimeSpan timeGap = var.WarningElapsedTime.Subtract(var.WarningStartTime);

                            if (timeGap.Seconds > var.AlarmAllowableTime)
                            {
                                var.alarmState = AlarmState.Warning;
                            }
                            
                        }
                        else
                        {
                            var.alarmState = AlarmState.Normal;
                        }

                        if (var.alarmState == AlarmState.Normal)
                        {
                            if (listAlarm.Contains(var))
                            {
                                listAlarm.Remove(var);
                            }
                            else if (listWarning.Contains(var))
                            {
                                listWarning.Remove(var);
                            }
                        }
                        else if (var.alarmState == AlarmState.Warning)
                        {
                            if (listAlarm.Contains(var))
                            {
                                listAlarm.Remove(var);
                                listWarning.Add(var);
                            }
                            if (listWarning.Contains(var))
                            {

                            }
                            else
                            {
                                listWarning.Add(var);
                            }
                        }
                        else if (var.alarmState == AlarmState.Alarm)
                        {
                            if (listAlarm.Contains(var))
                            {

                            }
                            else
                            {
                                listAlarm.Add(var);
                                AlarmLogWrite(var);
                                ExcuteAlarmMethod(var);
                            }
                        }

                    }
                    else
                    {
                        var.alarmState = AlarmState.Normal;

                        if (listAlarm.Contains(var))
                        {
                            listAlarm.Remove(var);
                        }
                        else if (listWarning.Contains(var))
                        {
                            listWarning.Remove(var);
                        }


                    }
                }
                System.Threading.Thread.Sleep(1000);
            }

            sw.Stop();
            sw.Reset();
            
        }

        public static void Stop_Alarm()
        {
            alarmRun = false;
            System.Threading.Thread.Sleep(100);
            AlarmListReset();
        }


        public static void ExcuteAlarmMethod(VariableInfo var)
        {
            if (var.alarmMethod == AlarmMethod.None)
            {

            }
            else if (var.alarmMethod == AlarmMethod.Idle)
            {

            }
            else if (var.alarmMethod == AlarmMethod.Stop)
            {
                double[] t_TargetValue = new double[5] { 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
        }

        public static void AlarmListReset()
        {
            listAlarm.Clear();
            listWarning.Clear();
        }

        public static void AlarmLogWrite(VariableInfo var)
        {
            ///////////////////////
            //string Header = "Time, Variable, Value";
            //string strAlarmLogFileNameOnly = "AlarmLog_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            //string strAlarmLogFileName = Ethercat.sysInfo.sysSettings.AlarmLogDir + "\\" + strAlarmLogFileNameOnly;
            ///////////////////////
            //if (!File.Exists(strAlarmLogFileName))
            //{
            //    try
            //    {
            //        System.IO.File.AppendAllText(strAlarmLogFileName, Header + "\r\n");
            //    }
            //    catch (DirectoryNotFoundException e)
            //    {
            //        Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.AlarmLogDir);
            //        System.IO.File.AppendAllText(strAlarmLogFileName, Header + "\r\n");
            //    }
            //}
            //string Time = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
            //System.IO.File.AppendAllText(strAlarmLogFileName, Time + "," + var.VariableName + "," + var.RealValue + "\r\n");


            ///////////////////////

        }
    }
}
