﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DSIC_Local.DataLogging;

namespace DSIC_Local.Settings
{
    public partial class Settings_Main : DevExpress.XtraEditors.XtraForm
    {

        public Settings_Main()
        {
            InitializeComponent();
            if (Main.project.currentEngine.Engine == Engine.DX22)
            {
                comboBoxEdit1.SelectedIndex = 3;
            }
            else
            {
                if (Main.project.currentEngine.Ees == EES.Euro3)
                {
                    comboBoxEdit1.SelectedIndex = 0;
                }
                else if (Main.project.currentEngine.Ees == EES.Euro4)
                {
                    comboBoxEdit1.SelectedIndex = 1;
                }
                else if (Main.project.currentEngine.Ees == EES.Euro5)
                {
                    comboBoxEdit1.SelectedIndex = 2;
                }
            }

            tbEngineNumber.Text = Main.resultData.engineNumber;
            tbWorker.Text = Main.project.worker;
            tbCellNumber.Text = Main.project.cellNumber;
            tbInspector.Text = Main.project.inspector;
        }

        private void btnDataLoggingSetting_Click(object sender, EventArgs e)
        {
            DataLogSetting form = new DataLogSetting();
            form.Show();
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxEdit1.SelectedIndex == 0)//Euro3
            {
                Main.project.currentEngine = Main.project.engines[0];
            }
            else if (comboBoxEdit1.SelectedIndex == 1)//Euro4
            {
                Main.project.currentEngine = Main.project.engines[1];
            }
            else if (comboBoxEdit1.SelectedIndex == 2)//Euro5
            {
                Main.project.currentEngine = Main.project.engines[2];
            }
            else if (comboBoxEdit1.SelectedIndex == 3)//DX22
            {
                Main.project.currentEngine = Main.project.engines[3];
            }
        }

        private void Settings_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.resultData.engineNumber = tbEngineNumber.Text;
            Main.project.worker = tbWorker.Text;
            Main.project.cellNumber = tbCellNumber.Text;
            Main.project.inspector = tbInspector.Text;
        }

        private void btnCommunicationSetup_Click(object sender, EventArgs e)
        {
            CommunicationSetup form = new CommunicationSetup();
            form.Show();
        }
        string prevCellNumber = string.Empty;

        private void tbCellNumber_TextChanged(object sender, EventArgs e)
        {
            string cellNumber = tbCellNumber.Text;

            if(prevCellNumber == tbCellNumber.Text)
            {
                return;
            }
            
            int result;
            if (int.TryParse(tbCellNumber.Text, out result))
            {
                tbCellNumber.Text = result.ToString();
            }
            else
            {
                XtraMessageBox.Show("<size=14>셀 번호는 한 자리 정수형 수만 입력이 가능합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                prevCellNumber = tbCellNumber.Text;
                tbCellNumber.Text = "0";
                tbCellNumber.Refresh();
                
            }
        }
    }
}