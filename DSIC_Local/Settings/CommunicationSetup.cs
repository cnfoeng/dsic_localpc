﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local.Settings
{
    public partial class CommunicationSetup : DevExpress.XtraEditors.XtraForm
    {
        public CommunicationSetup()
        {
            InitializeComponent();

            cbKLineComport.Text = Main.project.OBD_COMPort;
            cbSmokeMeterComport.Text = Main.project.SmokeMeter_COMPort;

            tbDBIP.Text = Main.project.dBConnectionData.IP;
            tbDBPort.Text = Main.project.dBConnectionData.Port;
            tbDBUserID.Text = Main.project.dBConnectionData.UserID;
            tbDBPwd.Text = Main.project.dBConnectionData.Pwd;
            tbDBTableName.Text = Main.project.dBConnectionData.TableName;

        }

        private void cbSmokeMeterComport_SelectedIndexChanged(object sender, EventArgs e)
        {
            Main.project.SmokeMeter_COMPort = cbSmokeMeterComport.Text;
        }

        private void cbKLineComport_SelectedIndexChanged(object sender, EventArgs e)
        {
            Main.project.OBD_COMPort = cbKLineComport.Text;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CommunicationSetup_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.project.dBConnectionData.IP = tbDBIP.Text;
            Main.project.dBConnectionData.Port = tbDBPort.Text;
            Main.project.dBConnectionData.UserID = tbDBUserID.Text;
            Main.project.dBConnectionData.Pwd = tbDBPwd.Text;
            Main.project.dBConnectionData.TableName = tbDBTableName.Text;
        }
    }
}