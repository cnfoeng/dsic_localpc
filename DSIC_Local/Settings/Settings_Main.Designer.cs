﻿namespace DSIC_Local.Settings
{
    partial class Settings_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings_Main));
            this.btnDataLoggingSetting = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tbEngineNumber = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCommunicationSetup = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label5 = new System.Windows.Forms.Label();
            this.tbInspector = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCellNumber = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbWorker = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEngineNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbInspector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCellNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDataLoggingSetting
            // 
            this.btnDataLoggingSetting.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDataLoggingSetting.Appearance.Options.UseFont = true;
            this.btnDataLoggingSetting.Location = new System.Drawing.Point(54, 96);
            this.btnDataLoggingSetting.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnDataLoggingSetting.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDataLoggingSetting.Name = "btnDataLoggingSetting";
            this.btnDataLoggingSetting.Size = new System.Drawing.Size(198, 95);
            this.btnDataLoggingSetting.TabIndex = 1;
            this.btnDataLoggingSetting.Text = "데이터 로깅 설정";
            this.btnDataLoggingSetting.Click += new System.EventHandler(this.btnDataLoggingSetting_Click);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(1140, 21);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "DV11 Euro3",
            "DV11 Euro4",
            "DV11 Euro5",
            "DX22"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(170, 30);
            this.comboBoxEdit1.TabIndex = 2;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // tbEngineNumber
            // 
            this.tbEngineNumber.Location = new System.Drawing.Point(806, 21);
            this.tbEngineNumber.Name = "tbEngineNumber";
            this.tbEngineNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEngineNumber.Properties.Appearance.Options.UseFont = true;
            this.tbEngineNumber.Size = new System.Drawing.Size(250, 30);
            this.tbEngineNumber.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(716, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "엔진 번호";
            // 
            // btnCommunicationSetup
            // 
            this.btnCommunicationSetup.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommunicationSetup.Appearance.Options.UseFont = true;
            this.btnCommunicationSetup.Location = new System.Drawing.Point(334, 96);
            this.btnCommunicationSetup.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnCommunicationSetup.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnCommunicationSetup.Name = "btnCommunicationSetup";
            this.btnCommunicationSetup.Size = new System.Drawing.Size(167, 95);
            this.btnCommunicationSetup.TabIndex = 5;
            this.btnCommunicationSetup.Text = "통신 설정";
            this.btnCommunicationSetup.Click += new System.EventHandler(this.btnCommunicationSetup_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.tbInspector);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.tbCellNumber);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.tbEngineNumber);
            this.panelControl1.Controls.Add(this.tbWorker);
            this.panelControl1.Controls.Add(this.comboBoxEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 281);
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1352, 73);
            this.panelControl1.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(282, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 23);
            this.label5.TabIndex = 12;
            this.label5.Text = "검사자";
            // 
            // tbInspector
            // 
            this.tbInspector.Location = new System.Drawing.Point(349, 21);
            this.tbInspector.Name = "tbInspector";
            this.tbInspector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInspector.Properties.Appearance.Options.UseFont = true;
            this.tbInspector.Size = new System.Drawing.Size(142, 30);
            this.tbInspector.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(529, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 23);
            this.label4.TabIndex = 10;
            this.label4.Text = "셀 번호";
            // 
            // tbCellNumber
            // 
            this.tbCellNumber.Location = new System.Drawing.Point(602, 21);
            this.tbCellNumber.Name = "tbCellNumber";
            this.tbCellNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCellNumber.Properties.Appearance.Options.UseFont = true;
            this.tbCellNumber.Properties.MaxLength = 1;
            this.tbCellNumber.Size = new System.Drawing.Size(66, 30);
            this.tbCellNumber.TabIndex = 9;
            this.tbCellNumber.TextChanged += new System.EventHandler(this.tbCellNumber_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1090, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "타입";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "작업자";
            // 
            // tbWorker
            // 
            this.tbWorker.Location = new System.Drawing.Point(101, 21);
            this.tbWorker.Name = "tbWorker";
            this.tbWorker.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWorker.Properties.Appearance.Options.UseFont = true;
            this.tbWorker.Size = new System.Drawing.Size(142, 30);
            this.tbWorker.TabIndex = 7;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnCommunicationSetup);
            this.panelControl2.Controls.Add(this.btnDataLoggingSetting);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1352, 354);
            this.panelControl2.TabIndex = 7;
            // 
            // Settings_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 354);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings_Main";
            this.Text = "Settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Settings_Main_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEngineNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbInspector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCellNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnDataLoggingSetting;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.TextEdit tbEngineNumber;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnCommunicationSetup;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit tbCellNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit tbWorker;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit tbInspector;
    }
}