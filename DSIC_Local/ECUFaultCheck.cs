﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local
{
    public partial class ECUFaultCheck : DevExpress.XtraEditors.XtraForm
    {
        public ECUFaultCheck()
        {
            InitializeComponent();

            gridView1.ActiveFilter.NonColumnFilter = "[Checked] = true";

            gridControl1.DataSource = Main.project.currentEngine.ECUFaults;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}