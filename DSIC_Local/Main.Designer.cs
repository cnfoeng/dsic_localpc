﻿namespace DSIC_Local
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState1 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState2 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState3 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState4 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState5 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState6 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState7 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState8 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions5 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAveragingMeasuring = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.btnReConnect = new DevExpress.XtraEditors.SimpleButton();
            this.btnSettings = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.cbOutsideMonitor = new DevExpress.XtraEditors.CheckButton();
            this.cbtnResultScreen = new DevExpress.XtraEditors.CheckButton();
            this.cbtnMonitoringScreen = new DevExpress.XtraEditors.CheckButton();
            this.cbtnMainScreen = new DevExpress.XtraEditors.CheckButton();
            this.cbtnBasicScreen = new DevExpress.XtraEditors.CheckButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.gcEngineComStatus = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.teElapsedTime = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tbScreen = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnStopStepMode = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.btnStartStepMode = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.windowsUIButtonPanel2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.windowsUIButtonPanel3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.panelContent = new DevExpress.XtraEditors.PanelControl();
            this.panelMainMonitoring = new DevExpress.XtraEditors.PanelControl();
            this.panelMonitoring = new DevExpress.XtraEditors.PanelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teElapsedTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbScreen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelContent)).BeginInit();
            this.panelContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMainMonitoring)).BeginInit();
            this.panelMainMonitoring.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMonitoring)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnAveragingMeasuring);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.toggleSwitch1);
            this.panelControl1.Controls.Add(this.btnReConnect);
            this.panelControl1.Controls.Add(this.btnSettings);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1900, 144);
            this.panelControl1.TabIndex = 0;
            // 
            // btnAveragingMeasuring
            // 
            this.btnAveragingMeasuring.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAveragingMeasuring.Appearance.Options.UseFont = true;
            this.btnAveragingMeasuring.Location = new System.Drawing.Point(1386, 21);
            this.btnAveragingMeasuring.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnAveragingMeasuring.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAveragingMeasuring.Name = "btnAveragingMeasuring";
            this.btnAveragingMeasuring.Size = new System.Drawing.Size(98, 62);
            this.btnAveragingMeasuring.TabIndex = 8;
            this.btnAveragingMeasuring.Text = "측정";
            this.btnAveragingMeasuring.Click += new System.EventHandler(this.btnAveragingMeasuring_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(1548, 31);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 14);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "DataLogging";
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(1548, 51);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.OffText = "Off";
            this.toggleSwitch1.Properties.OnText = "On";
            this.toggleSwitch1.Size = new System.Drawing.Size(95, 21);
            this.toggleSwitch1.TabIndex = 6;
            this.toggleSwitch1.Toggled += new System.EventHandler(this.toggleSwitch1_Toggled);
            // 
            // btnReConnect
            // 
            this.btnReConnect.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReConnect.Appearance.Options.UseFont = true;
            this.btnReConnect.Location = new System.Drawing.Point(1271, 21);
            this.btnReConnect.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnReConnect.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnReConnect.Name = "btnReConnect";
            this.btnReConnect.Size = new System.Drawing.Size(98, 62);
            this.btnReConnect.TabIndex = 5;
            this.btnReConnect.Text = "재연결";
            this.btnReConnect.Click += new System.EventHandler(this.btnReConnect_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSettings.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Appearance.Options.UseFont = true;
            this.btnSettings.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnSettings.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnSettings.ImageOptions.SvgImage")));
            this.btnSettings.ImageOptions.SvgImageSize = new System.Drawing.Size(20, 20);
            this.btnSettings.Location = new System.Drawing.Point(1674, 21);
            this.btnSettings.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnSettings.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(98, 62);
            this.btnSettings.TabIndex = 4;
            this.btnSettings.Text = "설정";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.ImageOptions.SvgImageSize = new System.Drawing.Size(20, 20);
            this.simpleButton1.Location = new System.Drawing.Point(1790, 21);
            this.simpleButton1.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(98, 62);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "프로그램 종료";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.cbOutsideMonitor);
            this.groupControl3.Controls.Add(this.cbtnResultScreen);
            this.groupControl3.Controls.Add(this.cbtnMonitoringScreen);
            this.groupControl3.Controls.Add(this.cbtnMainScreen);
            this.groupControl3.Controls.Add(this.cbtnBasicScreen);
            this.groupControl3.Location = new System.Drawing.Point(620, 5);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(582, 94);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = "Screen";
            // 
            // cbOutsideMonitor
            // 
            this.cbOutsideMonitor.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOutsideMonitor.Appearance.Options.UseFont = true;
            this.cbOutsideMonitor.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbOutsideMonitor.Location = new System.Drawing.Point(459, 27);
            this.cbOutsideMonitor.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.cbOutsideMonitor.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbOutsideMonitor.Name = "cbOutsideMonitor";
            this.cbOutsideMonitor.Size = new System.Drawing.Size(121, 65);
            this.cbOutsideMonitor.TabIndex = 8;
            this.cbOutsideMonitor.Text = "외부 모니터";
            this.cbOutsideMonitor.Click += new System.EventHandler(this.cbOutsideMonitor_Click);
            // 
            // cbtnResultScreen
            // 
            this.cbtnResultScreen.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnResultScreen.Appearance.Options.UseFont = true;
            this.cbtnResultScreen.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbtnResultScreen.Location = new System.Drawing.Point(338, 27);
            this.cbtnResultScreen.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.cbtnResultScreen.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnResultScreen.Name = "cbtnResultScreen";
            this.cbtnResultScreen.Size = new System.Drawing.Size(121, 65);
            this.cbtnResultScreen.TabIndex = 7;
            this.cbtnResultScreen.Text = "결과 화면";
            this.cbtnResultScreen.Click += new System.EventHandler(this.cbtnResultScreen_Click);
            // 
            // cbtnMonitoringScreen
            // 
            this.cbtnMonitoringScreen.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnMonitoringScreen.Appearance.Options.UseFont = true;
            this.cbtnMonitoringScreen.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbtnMonitoringScreen.Location = new System.Drawing.Point(226, 27);
            this.cbtnMonitoringScreen.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.cbtnMonitoringScreen.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnMonitoringScreen.Name = "cbtnMonitoringScreen";
            this.cbtnMonitoringScreen.Size = new System.Drawing.Size(112, 65);
            this.cbtnMonitoringScreen.TabIndex = 6;
            this.cbtnMonitoringScreen.Text = "모니터링 화면";
            this.cbtnMonitoringScreen.Click += new System.EventHandler(this.cbtnMonitoringScreen_Click);
            // 
            // cbtnMainScreen
            // 
            this.cbtnMainScreen.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnMainScreen.Appearance.Options.UseFont = true;
            this.cbtnMainScreen.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbtnMainScreen.Location = new System.Drawing.Point(114, 27);
            this.cbtnMainScreen.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.cbtnMainScreen.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnMainScreen.Name = "cbtnMainScreen";
            this.cbtnMainScreen.Size = new System.Drawing.Size(112, 65);
            this.cbtnMainScreen.TabIndex = 4;
            this.cbtnMainScreen.Text = "메인 화면";
            this.cbtnMainScreen.Click += new System.EventHandler(this.cbtnMainScreen_Click);
            // 
            // cbtnBasicScreen
            // 
            this.cbtnBasicScreen.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnBasicScreen.Appearance.Options.UseFont = true;
            this.cbtnBasicScreen.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbtnBasicScreen.Location = new System.Drawing.Point(2, 27);
            this.cbtnBasicScreen.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.cbtnBasicScreen.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnBasicScreen.Name = "cbtnBasicScreen";
            this.cbtnBasicScreen.Size = new System.Drawing.Size(112, 65);
            this.cbtnBasicScreen.TabIndex = 5;
            this.cbtnBasicScreen.Text = "기본 화면";
            this.cbtnBasicScreen.Click += new System.EventHandler(this.cbtnBasicScreen_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.Controls.Add(this.labelControl6);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Controls.Add(this.gaugeControl1);
            this.panelControl3.Controls.Add(this.gcEngineComStatus);
            this.panelControl3.Controls.Add(this.teElapsedTime);
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.tbScreen);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.textEdit2);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.textEdit1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 103);
            this.panelControl3.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1896, 39);
            this.panelControl3.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl6.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl6.ImageOptions.SvgImageSize = new System.Drawing.Size(21, 21);
            this.labelControl6.Location = new System.Drawing.Point(1168, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(128, 29);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Database Server";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl5.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl5.ImageOptions.SvgImageSize = new System.Drawing.Size(21, 21);
            this.labelControl5.Location = new System.Drawing.Point(930, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(173, 29);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Engine Communication";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge2});
            this.gaugeControl1.Location = new System.Drawing.Point(1138, 1);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(34, 36);
            this.gaugeControl1.TabIndex = 8;
            // 
            // stateIndicatorGauge2
            // 
            this.stateIndicatorGauge2.Bounds = new System.Drawing.Rectangle(6, 6, 22, 24);
            this.stateIndicatorGauge2.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent2});
            this.stateIndicatorGauge2.Name = "stateIndicatorGauge2";
            // 
            // stateIndicatorComponent2
            // 
            this.stateIndicatorComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent2.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent2.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent2.StateIndex = 1;
            indicatorState1.Name = "State1";
            indicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState2.Name = "State2";
            indicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState3.Name = "State3";
            indicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState4.Name = "State4";
            indicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent2.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState1,
            indicatorState2,
            indicatorState3,
            indicatorState4});
            // 
            // gcEngineComStatus
            // 
            this.gcEngineComStatus.BackColor = System.Drawing.Color.Transparent;
            this.gcEngineComStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gcEngineComStatus.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.gcEngineComStatus.Location = new System.Drawing.Point(900, 1);
            this.gcEngineComStatus.Name = "gcEngineComStatus";
            this.gcEngineComStatus.Size = new System.Drawing.Size(35, 36);
            this.gcEngineComStatus.TabIndex = 7;
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 23, 24);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 1;
            indicatorState5.Name = "State1";
            indicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState6.Name = "State2";
            indicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState7.Name = "State3";
            indicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState8.Name = "State4";
            indicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState5,
            indicatorState6,
            indicatorState7,
            indicatorState8});
            // 
            // teElapsedTime
            // 
            this.teElapsedTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.teElapsedTime.EditValue = "00:00:00";
            this.teElapsedTime.Location = new System.Drawing.Point(1685, 7);
            this.teElapsedTime.Name = "teElapsedTime";
            this.teElapsedTime.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.teElapsedTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teElapsedTime.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.teElapsedTime.Properties.Appearance.Options.UseBackColor = true;
            this.teElapsedTime.Properties.Appearance.Options.UseFont = true;
            this.teElapsedTime.Properties.Appearance.Options.UseForeColor = true;
            this.teElapsedTime.Properties.Appearance.Options.UseTextOptions = true;
            this.teElapsedTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teElapsedTime.Properties.ReadOnly = true;
            this.teElapsedTime.Size = new System.Drawing.Size(201, 26);
            this.teElapsedTime.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl3.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl3.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl3.ImageOptions.SvgImage")));
            this.labelControl3.ImageOptions.SvgImageSize = new System.Drawing.Size(21, 21);
            this.labelControl3.Location = new System.Drawing.Point(1546, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(133, 29);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Elapsed Time";
            // 
            // tbScreen
            // 
            this.tbScreen.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbScreen.EditValue = "기본 화면";
            this.tbScreen.Location = new System.Drawing.Point(714, 7);
            this.tbScreen.Name = "tbScreen";
            this.tbScreen.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.tbScreen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbScreen.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.tbScreen.Properties.Appearance.Options.UseBackColor = true;
            this.tbScreen.Properties.Appearance.Options.UseFont = true;
            this.tbScreen.Properties.Appearance.Options.UseForeColor = true;
            this.tbScreen.Properties.Appearance.Options.UseTextOptions = true;
            this.tbScreen.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbScreen.Properties.ReadOnly = true;
            this.tbScreen.Size = new System.Drawing.Size(145, 26);
            this.tbScreen.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl2.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl2.ImageOptions.SvgImage")));
            this.labelControl2.ImageOptions.SvgImageSize = new System.Drawing.Size(21, 21);
            this.labelControl2.Location = new System.Drawing.Point(626, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(88, 29);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Screen";
            // 
            // textEdit2
            // 
            this.textEdit2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textEdit2.EditValue = "자동 운전";
            this.textEdit2.Location = new System.Drawing.Point(425, 7);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(145, 26);
            this.textEdit2.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl1.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl1.ImageOptions.SvgImage")));
            this.labelControl1.ImageOptions.SvgImageSize = new System.Drawing.Size(21, 21);
            this.labelControl1.Location = new System.Drawing.Point(310, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(109, 29);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Test Mode";
            // 
            // textEdit1
            // 
            this.textEdit1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textEdit1.EditValue = "대기중";
            this.textEdit1.Location = new System.Drawing.Point(10, 7);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(254, 26);
            this.textEdit1.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnStopStepMode);
            this.groupControl2.Controls.Add(this.btnStartStepMode);
            this.groupControl2.Location = new System.Drawing.Point(356, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(260, 94);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Action";
            this.groupControl2.SizeChanged += new System.EventHandler(this.groupControl2_SizeChanged);
            // 
            // btnStopStepMode
            // 
            this.btnStopStepMode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnStopStepMode.ButtonInterval = 40;
            windowsUIButtonImageOptions1.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions1.SvgImage")));
            windowsUIButtonImageOptions1.SvgImageSize = new System.Drawing.Size(20, 20);
            this.btnStopStepMode.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("운전 정지", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.btnStopStepMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStopStepMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.btnStopStepMode.Location = new System.Drawing.Point(130, 27);
            this.btnStopStepMode.Name = "btnStopStepMode";
            this.btnStopStepMode.Size = new System.Drawing.Size(128, 65);
            this.btnStopStepMode.TabIndex = 2;
            this.btnStopStepMode.Text = "windowsUIButtonPanel2";
            // 
            // btnStartStepMode
            // 
            this.btnStartStepMode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnStartStepMode.ButtonInterval = 40;
            windowsUIButtonImageOptions2.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions2.SvgImage")));
            windowsUIButtonImageOptions2.SvgImageSize = new System.Drawing.Size(20, 20);
            this.btnStartStepMode.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("운전 시작", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.btnStartStepMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStartStepMode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.btnStartStepMode.Location = new System.Drawing.Point(2, 27);
            this.btnStartStepMode.Name = "btnStartStepMode";
            this.btnStartStepMode.Size = new System.Drawing.Size(128, 65);
            this.btnStartStepMode.TabIndex = 1;
            this.btnStartStepMode.Text = "windowsUIButtonPanel2";
            this.btnStartStepMode.Click += new System.EventHandler(this.btnStartStepMode_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.windowsUIButtonPanel2);
            this.groupControl1.Controls.Add(this.windowsUIButtonPanel3);
            this.groupControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.groupControl1.Location = new System.Drawing.Point(4, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(348, 94);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Mode";
            this.groupControl1.SizeChanged += new System.EventHandler(this.groupControl1_SizeChanged);
            // 
            // windowsUIButtonPanel2
            // 
            this.windowsUIButtonPanel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.windowsUIButtonPanel2.ButtonInterval = 40;
            windowsUIButtonImageOptions3.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions3.SvgImage")));
            windowsUIButtonImageOptions3.SvgImageSize = new System.Drawing.Size(23, 23);
            this.windowsUIButtonPanel2.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("스텝 운전", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.windowsUIButtonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowsUIButtonPanel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.windowsUIButtonPanel2.Location = new System.Drawing.Point(97, 27);
            this.windowsUIButtonPanel2.Name = "windowsUIButtonPanel2";
            this.windowsUIButtonPanel2.Size = new System.Drawing.Size(154, 65);
            this.windowsUIButtonPanel2.TabIndex = 2;
            this.windowsUIButtonPanel2.Text = "windowsUIButtonPanel2";
            this.windowsUIButtonPanel2.Click += new System.EventHandler(this.windowsUIButtonPanel2_Click);
            // 
            // windowsUIButtonPanel3
            // 
            this.windowsUIButtonPanel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.windowsUIButtonPanel3.ButtonInterval = 40;
            windowsUIButtonImageOptions4.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions4.SvgImage")));
            windowsUIButtonImageOptions4.SvgImageSize = new System.Drawing.Size(23, 23);
            this.windowsUIButtonPanel3.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("자동 운전", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.windowsUIButtonPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.windowsUIButtonPanel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.windowsUIButtonPanel3.Location = new System.Drawing.Point(251, 27);
            this.windowsUIButtonPanel3.Name = "windowsUIButtonPanel3";
            this.windowsUIButtonPanel3.Size = new System.Drawing.Size(95, 65);
            this.windowsUIButtonPanel3.TabIndex = 3;
            this.windowsUIButtonPanel3.Text = "windowsUIButtonPanel3";
            this.windowsUIButtonPanel3.Click += new System.EventHandler(this.windowsUIButtonPanel3_Click);
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.windowsUIButtonPanel1.ButtonInterval = 40;
            windowsUIButtonImageOptions5.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("windowsUIButtonImageOptions5.SvgImage")));
            windowsUIButtonImageOptions5.SvgImageSize = new System.Drawing.Size(23, 23);
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("수동 운전", true, windowsUIButtonImageOptions5, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.windowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.windowsUIButtonPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 27);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(95, 65);
            this.windowsUIButtonPanel1.TabIndex = 1;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Click += new System.EventHandler(this.windowsUIButtonPanel1_Click);
            // 
            // panelContent
            // 
            this.panelContent.Controls.Add(this.panelMainMonitoring);
            this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContent.Location = new System.Drawing.Point(0, 144);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(1900, 896);
            this.panelContent.TabIndex = 1;
            // 
            // panelMainMonitoring
            // 
            this.panelMainMonitoring.Controls.Add(this.panelMonitoring);
            this.panelMainMonitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainMonitoring.Location = new System.Drawing.Point(2, 2);
            this.panelMainMonitoring.Name = "panelMainMonitoring";
            this.panelMainMonitoring.Size = new System.Drawing.Size(1896, 892);
            this.panelMainMonitoring.TabIndex = 0;
            this.panelMainMonitoring.Visible = false;
            // 
            // panelMonitoring
            // 
            this.panelMonitoring.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelMonitoring.Appearance.Options.UseBackColor = true;
            this.panelMonitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMonitoring.Location = new System.Drawing.Point(2, 2);
            this.panelMonitoring.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelMonitoring.Name = "panelMonitoring";
            this.panelMonitoring.Size = new System.Drawing.Size(1892, 888);
            this.panelMonitoring.TabIndex = 0;
            this.panelMonitoring.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1900, 1040);
            this.Controls.Add(this.panelContent);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teElapsedTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbScreen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelContent)).EndInit();
            this.panelContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelMainMonitoring)).EndInit();
            this.panelMainMonitoring.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelMonitoring)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel btnStartStepMode;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelContent;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit teElapsedTime;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit tbScreen;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckButton cbtnBasicScreen;
        private DevExpress.XtraEditors.CheckButton cbtnMainScreen;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckButton cbtnResultScreen;
        private DevExpress.XtraEditors.CheckButton cbtnMonitoringScreen;
        private DevExpress.XtraEditors.SimpleButton btnSettings;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btnReConnect;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.CheckButton cbOutsideMonitor;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge2;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent2;
        private DevExpress.XtraGauges.Win.GaugeControl gcEngineComStatus;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelMainMonitoring;
        private DevExpress.XtraEditors.PanelControl panelMonitoring;
        private DevExpress.XtraEditors.SimpleButton btnAveragingMeasuring;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel3;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel2;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel btnStopStepMode;
    }
}