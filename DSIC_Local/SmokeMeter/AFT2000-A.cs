﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Globalization;
using DSIC_Local.Log;
using System.Diagnostics;

namespace DSIC_Local.SmokeMeter
{
    public class AFT2000_A
    {
        SerialPort SP = new SerialPort();
        public bool bConnected = false;
        public Int16 Sooth = 0;
        int portCount = 0;

        public AFT2000_A()
        {
            Init();
        }

        void Init()
        {
            string[] comports = SerialPort.GetPortNames();
            //SerialPort 초기 설정
            if (comports.Length > 0)
            {
                //SP.PortName = comports[0];
                SP.PortName = Main.project.SmokeMeter_COMPort;
            }
            else
            {
                SP.PortName = Main.project.SmokeMeter_COMPort;
            }

            portCount = comports.Length;

            SP.BaudRate = (int)9600;
            SP.DataBits = (int)8;
            SP.Parity = Parity.None;
            SP.StopBits = StopBits.One;
            SP.ReadTimeout = (int)1000;
            SP.WriteTimeout = (int)1000;

        }

        byte[] FullBuffer = new byte[50000];
        int ReadByte = 0;

        private void SP_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] ReceivedPacket = null;
            int count = SP.BytesToRead;
            byte[] NewBuffer = new byte[count];
            if (count > 0)
            {
                SP.Read(NewBuffer, 0, count);

                Buffer.BlockCopy(NewBuffer, 0, FullBuffer, ReadByte, count);
                ReadByte += count;


                if (!ChkkSumRead(FullBuffer, ReadByte))
                {
                    return;
                }
                else
                {
                    String strData = String.Format("Receive : {0}\n , {1}\n : {2}\n\n", count, ReadByte, BitConverter.ToString(FullBuffer, 0, ReadByte));
                    //rbText.Text += strData;
                    
                    if (FullBuffer[1] == 0x56 && ReadByte >= 28) // measure
                    {
                        string[] arrData = BitConverter.ToString(FullBuffer, 0, ReadByte).Split('-');
                        //rbText.Text += "\nRPM: " + Int16.Parse(arrData[3] + arrData[4], NumberStyles.HexNumber);
                        //rbText.Text += "\nVol: " + Int16.Parse(arrData[5] + arrData[6], NumberStyles.HexNumber);
                        //rbText.Text += "\nPressure: " + Int16.Parse(arrData[7] + arrData[8], NumberStyles.HexNumber);
                        //rbText.Text += "\nTemp: " + Int16.Parse(arrData[9] + arrData[10], NumberStyles.HexNumber);
                        Sooth = Int16.Parse(arrData[11] + arrData[12], NumberStyles.HexNumber);

                        Main.project.sysVarInfo.FindVariable("매연").IOValue = Sooth;
                        //rbText.Text += "\nmg/m3: " + Int16.Parse(arrData[13] + arrData[14], NumberStyles.HexNumber);
                    }
                    ReadByte = 0;
                }
            }
        }
        private bool ChkkSumRead(byte[] bytes, int nLen)
        {
            byte checksum = 0;

            for (int for_i = 0; for_i < bytes.Length && for_i < nLen - 1; for_i++)
                checksum += bytes[for_i];

            checksum = (byte)~checksum;
            checksum++;

            if (checksum <= 0)
                return false;

            if (bytes[nLen - 1] == checksum)
                return true;

            return false;
        }
        private byte Getchksum3(byte[] bytes)
        {
            byte checksum = (byte)(bytes[0] + bytes[1] + bytes[2]);
            checksum = (byte)~checksum;
            checksum++;
            return checksum;
        }

        private byte Getchksum4(byte[] bytes)
        {
            byte checksum = (byte)(bytes[0] + bytes[1] + bytes[2] + bytes[3]);
            checksum = (byte)~checksum;
            checksum++;
            return checksum;
        }
        public bool Open()
        {
            Init();

            if (portCount != 0)
            {
                try
                {
                    SP.Open();
                }
                catch (System.IO.IOException)
                {
                }
                SP.ReadTimeout = 500;
                SP.DataReceived += new SerialDataReceivedEventHandler(SP_DataReceived);
                if (SP.IsOpen)
                {
                    bConnected = true;
                    Main.AddLog("시리얼 통신 컨버터 연결 성공", LogType.OK);
                    return true;
                }
            }

            Main.AddLog("시리얼 통신 컨버터 연결 실패", LogType.Error);
            return false;
        }
        public bool Close()
        {
            SP.Close();
            return true;
        }
        private void Send(byte[] bytes)
        {
            if (SP.IsOpen)
            {
                SP.Write(bytes, 0, bytes.Length);
            }
        }
        public void ClickMode()
        {
            byte checksum;
            byte[] bytesHead;
            bytesHead = new byte[] { 0x45, 0x04, 0x4B, 0xDC };
            checksum = Getchksum4(bytesHead);
            byte[] bytes = new byte[] { bytesHead[0], bytesHead[1], bytesHead[2], bytesHead[3], 0x01, 0xFF, checksum }; // key command
            Send(bytes);
            
        }

        public void ReadMeasure()
        {
            byte checksum;
            byte[] bytesHead;
            byte[] bytes;
            bytesHead = new byte[] { 0x45, 0x01, 0x56, 0x64 };//Read Measured
                                                              //bytesHead = new byte[] { 0x45, 0x01, 0x61, 0x59 }; // Read Accel
                                                              //bytesHead = new byte[] { 0x45, 0x01, 0x31, 0x89 }; // Read System Status
            checksum = Getchksum3(bytesHead);
            bytes = new byte[] { bytesHead[0], bytesHead[1], bytesHead[2], checksum }; // key command

            Send(bytes);
            
        }

        public void ClickTest()
        {
            byte checksum;
            byte[] bytesHead;
            bytesHead = new byte[] { 0x45, 0x04, 0x4B, 0xF4 };
            checksum = Getchksum4(bytesHead);
            byte[] bytes = new byte[] { bytesHead[0], bytesHead[1], bytesHead[2], bytesHead[3], 0x01, 0xFF, checksum }; // key command
            Send(bytes);

            System.Threading.Thread.Sleep(500);

            ReadMeasure();
        }
    }
}
