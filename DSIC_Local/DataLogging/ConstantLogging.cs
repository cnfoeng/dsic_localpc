﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
namespace DSIC_Local.DataLogging
{
    public class ConstantLogging
    {
        private string filePath = string.Empty;
        private bool dataLoggingConstant = false;
        private string strConstantFileName = string.Empty;
        private string strConstantFileNameOnly = string.Empty;
        private int fileCountConstant = 0;
        private long prevStopWatchTickConstant = 0;
        private long nowStopWatchTickConstant = 0;
        private string dataFilePrefix = "Data";
        private int loggingInterval = 1000;
        private int rowCountConstant = 0;
        private int maxRowsPerFileConstant = 10000;

        private Stopwatch swConstant = new Stopwatch();

        Thread thConstantLog;

        private List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();

        public int RowCountConstant { get => rowCountConstant; set => rowCountConstant = value; }
        public int LoggingInterval { get => loggingInterval; set => loggingInterval = value; }
        public string DataFilePrefix { get => dataFilePrefix; set => dataFilePrefix = value; }
        public string FilePath { get => filePath; set => filePath = value; }

        public ConstantLogging(string filepath, string prefix, int interval, int maxrowcount)
        {
            this.filePath = filepath;
            dataFilePrefix = prefix;
            loggingInterval = interval;
            maxRowsPerFileConstant = maxrowcount;
        }
        public void StartLogging()
        {
            thConstantLog = new Thread(new ThreadStart(StartConstantLog));
            thConstantLog.Start();
        }

        public void StopLogging()
        {
            swConstant.Reset();
            dataLoggingConstant = false;
            RowCountConstant = 0;
            fileCountConstant = 0;
            prevStopWatchTickConstant = 0;

            if (thConstantLog != null)
            {
                thConstantLog.Abort();
                thConstantLog.Join();
            }
        }
        private void StartConstantLog()
        {
            DataSaveStart();
            Tick();
        }
        private void DataSaveStart()
        {
            dataLoggingConstant = true;
            ConstantDataLogging_Variables = Main.project.sysVarInfo.ConstantDataLogging_Variables;

            if((filePath == string.Empty))
            {
                filePath = Application.StartupPath;
            }

            string Header;
            ///////////////////////
            Header = "StartTime";
            ///////////////////////
            ///

            foreach (VariableInfo var in ConstantDataLogging_Variables)
                Header = Header + "," + var.VariableName;
            strConstantFileNameOnly = DataFilePrefix + "_Constant_" + DateTime.Now.ToString("yyyyMMddHHmm") + "_" + fileCountConstant + ".csv";
            strConstantFileName = filePath + "\\Constant\\" + strConstantFileNameOnly;
            try
            {
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.Default);
            }
            catch (DirectoryNotFoundException e)
            {
                Directory.CreateDirectory(filePath + "\\Constant");
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.Default);
            }
        }

        private void DataSave()
        {
            if (dataLoggingConstant)
            {
                if (nowStopWatchTickConstant - prevStopWatchTickConstant > LoggingInterval)
                {
                    string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                    prevStopWatchTickConstant = nowStopWatchTickConstant;

                    foreach (VariableInfo var in ConstantDataLogging_Variables)
                    {
                        Value = Value + "," + var.RealValue.ToString();
                    }

                    System.IO.File.AppendAllText(strConstantFileName, Value + "\r\n", Encoding.Default);
                    RowCountConstant++;
                }
            }
        }

        private void Tick()
        {
            if (dataLoggingConstant == true)
            {

                swConstant.Start();

                while (swConstant.IsRunning)
                {

                    if (RowCountConstant < maxRowsPerFileConstant)
                    {
                        if (swConstant.ElapsedMilliseconds < 100000000)
                        {
                            nowStopWatchTickConstant = swConstant.ElapsedMilliseconds;

                            DataSave();
                        }
                        else
                        {
                            swConstant.Restart();
                            prevStopWatchTickConstant = 0;
                        }
                    }
                    else
                    {
                        swConstant.Reset();
                        RowCountConstant = 0;
                        prevStopWatchTickConstant = 0;
                        fileCountConstant++;
                        DataSaveStart();

                        swConstant.Start();
                        Thread.Sleep(1);
                    }
                    Thread.Sleep(10);
                }
            }
        }
    }
}
