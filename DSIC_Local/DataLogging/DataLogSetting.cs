﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local.DataLogging
{
    public partial class DataLogSetting : DevExpress.XtraEditors.XtraForm
    {

        List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();
        public DataLogSetting()
        {
            InitializeComponent();
            ConstantDataLogging_Variables = Main.project.sysVarInfo.ConstantDataLogging_Variables;
            gridControl1.DataSource = ConstantDataLogging_Variables;


            tbLoggingInterval.Text = Main.project.logSetting.LoggingInterval.ToString();
            tbMaxRowCount.Text = Main.project.logSetting.MaxRowCount.ToString();
            tbFilePrefix.Text = Main.project.logSetting.DataPrefix;
            if (Main.project.logSetting.DataLoggingConstantPath != string.Empty)
            {
                tbFilePath.Text = Main.project.logSetting.DataLoggingConstantPath;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Main.project.logSetting.LoggingInterval = int.Parse(tbLoggingInterval.Text);
            Main.project.logSetting.MaxRowCount = int.Parse(tbMaxRowCount.Text);
            Main.project.logSetting.DataPrefix = tbFilePrefix.Text;
            Main.project.logSetting.DataLoggingConstantPath = tbFilePath.Text;

            this.Close();
        }

        private void btnChangePath_Click(object sender, EventArgs e)
        {
            xtraFolderBrowserDialog1.ShowDialog();

            if (xtraFolderBrowserDialog1.SelectedPath == "xtraFolderBrowserDialog1")
            {
                tbFilePath.Text = Main.project.logSetting.DataLoggingConstantPath;
            }
            else
            {
                tbFilePath.Text = xtraFolderBrowserDialog1.SelectedPath;
            }

        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();
            if (selected.Length > 1)
            {
                XtraMessageBox.Show("<size=14>한 줄만 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //TODO 하나만 선택. 아니면 에러메시지.
            VariableInfo Var = null;
            int index = gridView1.FocusedRowHandle;

            if (index > 0)
            {
                Var = ConstantDataLogging_Variables[index];
                ConstantDataLogging_Variables.RemoveAt(index);
                ConstantDataLogging_Variables.Insert(index - 1, Var);

                gridView1.FocusedRowHandle = index - 1;
            }


            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();
            if (selected.Length > 1)
            {
                XtraMessageBox.Show("<size=14>한 줄만 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //TODO 하나만 선택. 아니면 에러메시지.
            VariableInfo Var = null;
            int index = gridView1.FocusedRowHandle;

            if (index >= 0 && index < ConstantDataLogging_Variables.Count-1)
            {
                Var = ConstantDataLogging_Variables[index];
                ConstantDataLogging_Variables.RemoveAt(index);
                ConstantDataLogging_Variables.Insert(index + 1, Var);

                gridView1.FocusedRowHandle = index + 1;
            }


            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DataSelect form = new DataSelect();
            form.ShowDialog();

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();

            if (selected[0] == 0)
            {
                return;
            }

            for(int i=0; i < selected.Length; i++)
            {
                ConstantDataLogging_Variables.RemoveAt(i);
            }

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }
    }
}