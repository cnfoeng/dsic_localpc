﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local.DataLogging
{
    public partial class DataSelect : DevExpress.XtraEditors.XtraForm
    {

        List<VariableInfo> Variables = null;

        public DataSelect()
        {
            InitializeComponent();

            cbChannelList.SelectedIndex = 0;
        }

        private void cbChannelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variables = Main.project.sysVarInfo.getVariables(cbChannelList.SelectedIndex);

            gridControl1.BeginUpdate();
            gridControl1.DataSource = Variables;
            gridControl1.EndUpdate();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();

            for (int i = 0; i < selected.Length; i++)
            {
                Main.project.sysVarInfo.ConstantDataLogging_Variables.Add(Variables[selected[i]]);
            }
        }
    }
}