﻿namespace DSIC_Local
{
    partial class AverageMeasuring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AverageMeasuring));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tbAveragingTime = new DevExpress.XtraEditors.TextEdit();
            this.tbOption2Value = new DevExpress.XtraEditors.TextEdit();
            this.tbOption2VarName = new DevExpress.XtraEditors.TextEdit();
            this.tbOption1Value = new DevExpress.XtraEditors.TextEdit();
            this.tbOption1VarName = new DevExpress.XtraEditors.TextEdit();
            this.tbOilPressureValue = new DevExpress.XtraEditors.TextEdit();
            this.tbFuelEfficiencyValue = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedPowerValue = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedTorqueValue = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedWeightValue = new DevExpress.XtraEditors.TextEdit();
            this.tbSpeedValue = new DevExpress.XtraEditors.TextEdit();
            this.tbOilPressureVarName = new DevExpress.XtraEditors.TextEdit();
            this.tbFuelEfficiencyVarName = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedPowerVarName = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedTorqueVarName = new DevExpress.XtraEditors.TextEdit();
            this.tbCorrectedWeightVarName = new DevExpress.XtraEditors.TextEdit();
            this.tbSpeedVarName = new DevExpress.XtraEditors.TextEdit();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAveragingTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption2Value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption2VarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption1Value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption1VarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOilPressureValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFuelEfficiencyValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedPowerValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedTorqueValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedWeightValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOilPressureVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFuelEfficiencyVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedPowerVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedTorqueVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedWeightVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 332);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(646, 34);
            this.panelControl1.TabIndex = 0;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarControl1.Location = new System.Drawing.Point(2, 2);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(642, 30);
            this.progressBarControl1.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(646, 332);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.dataLayoutControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(2, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(642, 328);
            this.panelControl4.TabIndex = 1;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.tbAveragingTime);
            this.dataLayoutControl1.Controls.Add(this.tbOption2Value);
            this.dataLayoutControl1.Controls.Add(this.tbOption2VarName);
            this.dataLayoutControl1.Controls.Add(this.tbOption1Value);
            this.dataLayoutControl1.Controls.Add(this.tbOption1VarName);
            this.dataLayoutControl1.Controls.Add(this.tbOilPressureValue);
            this.dataLayoutControl1.Controls.Add(this.tbFuelEfficiencyValue);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedPowerValue);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedTorqueValue);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedWeightValue);
            this.dataLayoutControl1.Controls.Add(this.tbSpeedValue);
            this.dataLayoutControl1.Controls.Add(this.tbOilPressureVarName);
            this.dataLayoutControl1.Controls.Add(this.tbFuelEfficiencyVarName);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedPowerVarName);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedTorqueVarName);
            this.dataLayoutControl1.Controls.Add(this.tbCorrectedWeightVarName);
            this.dataLayoutControl1.Controls.Add(this.tbSpeedVarName);
            this.dataLayoutControl1.Controls.Add(this.btnStart);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(2, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.ShareLookAndFeelWithChildren = false;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(638, 324);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // tbAveragingTime
            // 
            this.tbAveragingTime.EditValue = "30";
            this.tbAveragingTime.Location = new System.Drawing.Point(92, 288);
            this.tbAveragingTime.Name = "tbAveragingTime";
            this.tbAveragingTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAveragingTime.Properties.Appearance.Options.UseFont = true;
            this.tbAveragingTime.Size = new System.Drawing.Size(365, 24);
            this.tbAveragingTime.TabIndex = 21;
            // 
            // tbOption2Value
            // 
            this.tbOption2Value.Location = new System.Drawing.Point(516, 222);
            this.tbOption2Value.Name = "tbOption2Value";
            this.tbOption2Value.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOption2Value.Properties.Appearance.Options.UseFont = true;
            this.tbOption2Value.Properties.ReadOnly = true;
            this.tbOption2Value.Size = new System.Drawing.Size(110, 26);
            this.tbOption2Value.TabIndex = 20;
            // 
            // tbOption2VarName
            // 
            this.tbOption2VarName.Location = new System.Drawing.Point(92, 222);
            this.tbOption2VarName.Name = "tbOption2VarName";
            this.tbOption2VarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOption2VarName.Properties.Appearance.Options.UseFont = true;
            this.tbOption2VarName.Size = new System.Drawing.Size(365, 26);
            this.tbOption2VarName.TabIndex = 19;
            // 
            // tbOption1Value
            // 
            this.tbOption1Value.Location = new System.Drawing.Point(516, 192);
            this.tbOption1Value.Name = "tbOption1Value";
            this.tbOption1Value.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOption1Value.Properties.Appearance.Options.UseFont = true;
            this.tbOption1Value.Properties.ReadOnly = true;
            this.tbOption1Value.Size = new System.Drawing.Size(110, 26);
            this.tbOption1Value.TabIndex = 18;
            // 
            // tbOption1VarName
            // 
            this.tbOption1VarName.Location = new System.Drawing.Point(92, 192);
            this.tbOption1VarName.Name = "tbOption1VarName";
            this.tbOption1VarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOption1VarName.Properties.Appearance.Options.UseFont = true;
            this.tbOption1VarName.Size = new System.Drawing.Size(365, 26);
            this.tbOption1VarName.TabIndex = 17;
            // 
            // tbOilPressureValue
            // 
            this.tbOilPressureValue.Location = new System.Drawing.Point(516, 162);
            this.tbOilPressureValue.Name = "tbOilPressureValue";
            this.tbOilPressureValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOilPressureValue.Properties.Appearance.Options.UseFont = true;
            this.tbOilPressureValue.Properties.ReadOnly = true;
            this.tbOilPressureValue.Size = new System.Drawing.Size(110, 26);
            this.tbOilPressureValue.TabIndex = 15;
            // 
            // tbFuelEfficiencyValue
            // 
            this.tbFuelEfficiencyValue.Location = new System.Drawing.Point(516, 132);
            this.tbFuelEfficiencyValue.Name = "tbFuelEfficiencyValue";
            this.tbFuelEfficiencyValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFuelEfficiencyValue.Properties.Appearance.Options.UseFont = true;
            this.tbFuelEfficiencyValue.Properties.ReadOnly = true;
            this.tbFuelEfficiencyValue.Size = new System.Drawing.Size(110, 26);
            this.tbFuelEfficiencyValue.TabIndex = 14;
            // 
            // tbCorrectedPowerValue
            // 
            this.tbCorrectedPowerValue.Location = new System.Drawing.Point(516, 102);
            this.tbCorrectedPowerValue.Name = "tbCorrectedPowerValue";
            this.tbCorrectedPowerValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedPowerValue.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedPowerValue.Properties.ReadOnly = true;
            this.tbCorrectedPowerValue.Size = new System.Drawing.Size(110, 26);
            this.tbCorrectedPowerValue.TabIndex = 13;
            // 
            // tbCorrectedTorqueValue
            // 
            this.tbCorrectedTorqueValue.Location = new System.Drawing.Point(516, 72);
            this.tbCorrectedTorqueValue.Name = "tbCorrectedTorqueValue";
            this.tbCorrectedTorqueValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedTorqueValue.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedTorqueValue.Properties.ReadOnly = true;
            this.tbCorrectedTorqueValue.Size = new System.Drawing.Size(110, 26);
            this.tbCorrectedTorqueValue.TabIndex = 12;
            // 
            // tbCorrectedWeightValue
            // 
            this.tbCorrectedWeightValue.Location = new System.Drawing.Point(516, 42);
            this.tbCorrectedWeightValue.Name = "tbCorrectedWeightValue";
            this.tbCorrectedWeightValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedWeightValue.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedWeightValue.Properties.ReadOnly = true;
            this.tbCorrectedWeightValue.Size = new System.Drawing.Size(110, 26);
            this.tbCorrectedWeightValue.TabIndex = 11;
            // 
            // tbSpeedValue
            // 
            this.tbSpeedValue.Location = new System.Drawing.Point(516, 12);
            this.tbSpeedValue.Name = "tbSpeedValue";
            this.tbSpeedValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSpeedValue.Properties.Appearance.Options.UseFont = true;
            this.tbSpeedValue.Properties.ReadOnly = true;
            this.tbSpeedValue.Size = new System.Drawing.Size(110, 26);
            this.tbSpeedValue.TabIndex = 10;
            // 
            // tbOilPressureVarName
            // 
            this.tbOilPressureVarName.Location = new System.Drawing.Point(92, 162);
            this.tbOilPressureVarName.Name = "tbOilPressureVarName";
            this.tbOilPressureVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOilPressureVarName.Properties.Appearance.Options.UseFont = true;
            this.tbOilPressureVarName.Size = new System.Drawing.Size(365, 26);
            this.tbOilPressureVarName.TabIndex = 9;
            // 
            // tbFuelEfficiencyVarName
            // 
            this.tbFuelEfficiencyVarName.Location = new System.Drawing.Point(92, 132);
            this.tbFuelEfficiencyVarName.Name = "tbFuelEfficiencyVarName";
            this.tbFuelEfficiencyVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFuelEfficiencyVarName.Properties.Appearance.Options.UseFont = true;
            this.tbFuelEfficiencyVarName.Size = new System.Drawing.Size(365, 26);
            this.tbFuelEfficiencyVarName.TabIndex = 8;
            // 
            // tbCorrectedPowerVarName
            // 
            this.tbCorrectedPowerVarName.Location = new System.Drawing.Point(92, 102);
            this.tbCorrectedPowerVarName.Name = "tbCorrectedPowerVarName";
            this.tbCorrectedPowerVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedPowerVarName.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedPowerVarName.Size = new System.Drawing.Size(365, 26);
            this.tbCorrectedPowerVarName.TabIndex = 7;
            // 
            // tbCorrectedTorqueVarName
            // 
            this.tbCorrectedTorqueVarName.Location = new System.Drawing.Point(92, 72);
            this.tbCorrectedTorqueVarName.Name = "tbCorrectedTorqueVarName";
            this.tbCorrectedTorqueVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedTorqueVarName.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedTorqueVarName.Size = new System.Drawing.Size(365, 26);
            this.tbCorrectedTorqueVarName.TabIndex = 6;
            // 
            // tbCorrectedWeightVarName
            // 
            this.tbCorrectedWeightVarName.Location = new System.Drawing.Point(92, 42);
            this.tbCorrectedWeightVarName.Name = "tbCorrectedWeightVarName";
            this.tbCorrectedWeightVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCorrectedWeightVarName.Properties.Appearance.Options.UseFont = true;
            this.tbCorrectedWeightVarName.Size = new System.Drawing.Size(365, 26);
            this.tbCorrectedWeightVarName.TabIndex = 5;
            // 
            // tbSpeedVarName
            // 
            this.tbSpeedVarName.Location = new System.Drawing.Point(92, 12);
            this.tbSpeedVarName.Name = "tbSpeedVarName";
            this.tbSpeedVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSpeedVarName.Properties.Appearance.Options.UseFont = true;
            this.tbSpeedVarName.Size = new System.Drawing.Size(365, 26);
            this.tbSpeedVarName.TabIndex = 4;
            // 
            // btnStart
            // 
            this.btnStart.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Appearance.Options.UseFont = true;
            this.btnStart.Location = new System.Drawing.Point(461, 288);
            this.btnStart.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnStart.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(165, 24);
            this.btnStart.TabIndex = 16;
            this.btnStart.Text = "측정";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem2,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(638, 324);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.tbSpeedVarName;
            this.layoutControlItem1.CustomizationFormText = "변수 1 이름";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem1.Text = "변수 1 이름";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.tbCorrectedWeightVarName;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem2.Text = "변수 2 이름";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.tbCorrectedTorqueVarName;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem3.Text = "변수 3 이름";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.tbCorrectedPowerVarName;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem4.Text = "변수 4 이름";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.tbFuelEfficiencyVarName;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem5.Text = "변수 5 이름";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.tbOilPressureVarName;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem6.Text = "변수 6 이름";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.tbSpeedValue;
            this.layoutControlItem7.Location = new System.Drawing.Point(449, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem7.Text = "측정값";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.tbCorrectedWeightValue;
            this.layoutControlItem8.Location = new System.Drawing.Point(449, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem8.Text = "측정값";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.tbCorrectedTorqueValue;
            this.layoutControlItem9.Location = new System.Drawing.Point(449, 60);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem9.Text = "측정값";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.tbCorrectedPowerValue;
            this.layoutControlItem10.Location = new System.Drawing.Point(449, 90);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem10.Text = "측정값";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.tbFuelEfficiencyValue;
            this.layoutControlItem11.Location = new System.Drawing.Point(449, 120);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem11.Text = "측정값";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.tbOilPressureValue;
            this.layoutControlItem12.Location = new System.Drawing.Point(449, 150);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem12.Text = "측정값";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 240);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(618, 36);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnStart;
            this.layoutControlItem13.Location = new System.Drawing.Point(449, 276);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(169, 28);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.tbOption1VarName;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem14.Text = "변수 7 이름";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.Control = this.tbOption1Value;
            this.layoutControlItem15.Location = new System.Drawing.Point(449, 180);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem15.Text = "측정값";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.tbOption2VarName;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(449, 30);
            this.layoutControlItem16.Text = "변수 8 이름";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(77, 18);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.Control = this.tbOption2Value;
            this.layoutControlItem17.Location = new System.Drawing.Point(449, 210);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(169, 30);
            this.layoutControlItem17.Text = "측정값";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.Control = this.tbAveragingTime;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 276);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(449, 28);
            this.layoutControlItem18.Text = "측정시간(초)";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(77, 18);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // AverageMeasuring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 366);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AverageMeasuring";
            this.Text = "Average Measuring";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AverageMeasuring_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAveragingTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption2Value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption2VarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption1Value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOption1VarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOilPressureValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFuelEfficiencyValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedPowerValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedTorqueValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedWeightValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOilPressureVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFuelEfficiencyVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedPowerVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedTorqueVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectedWeightVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit tbOilPressureValue;
        private DevExpress.XtraEditors.TextEdit tbFuelEfficiencyValue;
        private DevExpress.XtraEditors.TextEdit tbCorrectedPowerValue;
        private DevExpress.XtraEditors.TextEdit tbCorrectedTorqueValue;
        private DevExpress.XtraEditors.TextEdit tbCorrectedWeightValue;
        private DevExpress.XtraEditors.TextEdit tbSpeedValue;
        private DevExpress.XtraEditors.TextEdit tbOilPressureVarName;
        private DevExpress.XtraEditors.TextEdit tbFuelEfficiencyVarName;
        private DevExpress.XtraEditors.TextEdit tbCorrectedPowerVarName;
        private DevExpress.XtraEditors.TextEdit tbCorrectedTorqueVarName;
        private DevExpress.XtraEditors.TextEdit tbCorrectedWeightVarName;
        private DevExpress.XtraEditors.TextEdit tbSpeedVarName;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.TextEdit tbOption1Value;
        private DevExpress.XtraEditors.TextEdit tbOption1VarName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit tbOption2Value;
        private DevExpress.XtraEditors.TextEdit tbOption2VarName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.TextEdit tbAveragingTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
    }
}