﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using DWORD = System.UInt32;
using System.IO.Ports;

namespace DSIC_Local
{
    public class KWP2000
    {
        public KlineStatus status = KlineStatus.Normal;
        AutoResetEvent mre = new AutoResetEvent(false);
        string Response;
        string[] Responses;
        int commandDelay = 200;
        public SerialPort OBDSerial = new SerialPort();
        public bool bConnected = false;
        public bool doClear = false;
        public Thread thGetData;
        public StringBuilder log = new StringBuilder();
        public int progressbarValue = 0;
        public int progressbarMaximum = 0;

        public string mapVersion = string.Empty;
        public string flashDate = string.Empty;
        public string engineSerial = string.Empty;

        public enum KlineStatus
        {
            Normal,
            Flash,
            None,
        }
        public void OBDCommunicationStart()
        {
            OBDSerial = new SerialPort();

            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(1);

            //Connect OBDLinkSX
            try
            {
                if (!OBDSerial.IsOpen)
                {
                    //string[] comports = SerialPort.GetPortNames();
                    //OBDSerial.PortName = comports[0];
                    OBDSerial.PortName = Main.project.OBD_COMPort;
                    this.OBDSerial.BaudRate = 115200;
                    this.OBDSerial.DiscardNull = true;
                    this.OBDSerial.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(this.OBDSerial_ErrorReceived);
                    this.OBDSerial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.OBDSerial_DataReceived);
                    OBDSerial.Open();

                    Debug.Print("Connect");
                    Main.AddLog("K-Line 장비 연결 성공", Log.LogType.OK);
                }
            }
            catch (Exception err)
            {
                Debug.Print(err.ToString());
                Main.AddLog("K-Line 장비 연결 실패", Log.LogType.Error);
                return;
            }

            string msg;

            msg = "ATAL" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
                Main.AddLog("K-line HW Error", Log.LogType.Error);
                return;
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("ATAL Success - Increase sending data length");
            }

            msg = "ATH1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
                Main.AddLog("K-line HW Error", Log.LogType.Error);
                return;
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("ATH1 Success - Header Add");
            }

            msg = "STP 25" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
                Main.AddLog("K-line HW Error", Log.LogType.Error);
                return;
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Protocol Change");
            }

            msg = "STIP4 0" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("STIP4 Success - Interbyte time 0ms");
            }

            Thread.Sleep(1);

            msg = "STPTRQ 1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("STIP4 Success - Interbyte time 0ms");
            }

            Thread.Sleep(1);

            msg = "ATSH8210F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8210F1 Change Success");
            }

            Thread.Sleep(1);

            msg = "ATE0" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Echo Off Success");
            }


            Thread.Sleep(1);

            msg = "ATSH8110F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8110F1 Change Success");
            }
            //StartCommunication

            msg = "81" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');

            if (ArrResponse.Length == 3) //ECU 처음 켜지는 경우 메시지에 BUS INIT OK가 붙어서 오기때문에 OK로 정상동작 여부 체크
            {
                ResponseSid = ArrResponse[2];

                if (ResponseSid == "OK")
                {
                    Debug.Print("StartCommunication(81) Success");
                    Main.AddLog("K-Line 엔진 통신 성공", Log.LogType.OK);
                    bConnected = true;
                }
                else
                {
                    Debug.Print("StartCommunication(81) Fail");
                    Main.AddLog("K-Line 엔진 통신 실패", Log.LogType.Error);
                    bConnected = false;
                    return;
                }
            }
            else//ECU가 처음 켜지는 경우가 아닌 경우 기존 메시지들과 같은 방식으로 처리
            {

                if (ArrResponse.Length < 4)
                {
                    Debug.Print("Error - ECU Power Off");
                    Main.AddLog("K-Line 엔진 통신 실패", Log.LogType.Error);
                    bConnected = false;
                    return;
                }
                //TODO: ECU 안켜져있을때 에러처리
                try
                {
                    ResponseSid = ArrResponse[3];
                }
                catch (IndexOutOfRangeException)
                {
                    ECUFlashing.FlashLogs.Add("ECU 통신 실패");
                    bConnected = false;
                    return;
                }
                if (ResponseSid == "C1")
                {
                    Debug.Print("StartCommunication(81) Success");
                    Main.AddLog("K-Line 엔진 통신 성공", Log.LogType.OK);
                    bConnected = true;
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("StartCommunication(81) Fail");
                    Main.AddLog("K-Line 엔진 통신 실패", Log.LogType.Error);
                    bConnected = false;
                    return;
                }
            }

            msg = "ATSH8210F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8210F1 Change Success");
            }

            //StartSession
            bool bStartSession = false;
            msg = "1087" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }
            ArrResponse = Response.Split(' ');
            try
            {
                ResponseSid = ArrResponse[3];
            }
            catch (IndexOutOfRangeException)
            {

                Debug.Print("Bus Init Error");
                return;

            }
            if (ResponseSid == "50")
            {
                Debug.Print("StartSession 1087 Success");
                bConnected = true;
                bStartSession = true;
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("StartSession 1087 Fail");
                bConnected = false;
                bStartSession = false;
            }

            if (!bStartSession)
            {
                //SecurityAccess
                msg = "270B" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];

                string seed = string.Empty;
                string key = string.Empty;

                if (ResponseSid == "67")
                {
                    Debug.Print("SecurityAccess Success");
                    Main.AddLog("K-Line SecurityAccess 성공", Log.LogType.OK);
                    for (int i = 4; i < 9; i++)
                    {
                        seed += ArrResponse[i];
                    }

                    key = MakeSecurityKeyKeyForEngine(seed);
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("SecurityAccess Fail");
                    Main.AddLog("K-Line SecurityAccess 실패", Log.LogType.Error);
                    return;
                }

                msg = "ATSH8610F1" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(1);

                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[0];

                if (ResponseSid == "OK")
                {
                    Debug.Print("Header 8610F1 Change Success");
                }

                msg = "270C" + key + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];

                if (ResponseSid == "67")
                {
                    Debug.Print("KeyValueCalculation Success");
                    Main.AddLog("K-Line KeyValueCalculation 성공", Log.LogType.OK);
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("KeyValueCalculation Fail");
                    Main.AddLog("K-Line KeyValueCalculation 실패", Log.LogType.Error);
                    return;
                }
                msg = "ATSH8210F1" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(1);

                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[0];

                if (ResponseSid == "OK")
                {
                    Debug.Print("Header 8210F1 Change Success");
                }
                //StartSession Again
                msg = "1087" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }
                ArrResponse = Response.Split(' ');
                if (ArrResponse.Length == 4)
                {
                    if (ArrResponse[3] == "ERROR")
                    {
                        Main.AddLog("K-Line StartSession 실패", Log.LogType.Error);
                        return;
                    }
                }
                else if (ArrResponse.Length == 2 && ArrResponse[0] == "NO")
                {
                    Thread.Sleep(3000);
                    msg = "1087" + Environment.NewLine;
                    OBDSerial.Write(msg);
                    Debug.Print(msg);
                    Thread.Sleep(commandDelay);
                    if (!mre.WaitOne(responseTimeout))
                    {
                        Debug.Print("Did not receive response");
                    }
                    ArrResponse = Response.Split(' ');
                    ResponseSid = ArrResponse[4];
                }
                if (ResponseSid == "50" || ResponseSid == "OK")
                {
                    Debug.Print("StartSession 1087 Success");
                    Main.AddLog("K-Line StartSession 성공", Log.LogType.OK);
                    bConnected = true;
                    bStartSession = true;
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("StartSession 1087 Fail");
                    Main.AddLog("K-Line StartSession 실패", Log.LogType.Error);
                    bConnected = false;
                    bStartSession = false;
                    return;
                }

            }

            status = KlineStatus.Normal;
        }
        public void OBDCommunicationFlash(string checksum)
        {
            string msg;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string ResponseSid;
            string[] ArrResponse;


            status = KlineStatus.Flash;
            Thread.Sleep(5000);
            StopGetData();
            Thread.Sleep(5000);

            msg = "ATSH8210F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8210F1 Change Success");
            }
            //StartSession Again
            msg = "1085" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }
            ArrResponse = Response.Split(' ');
            if (ArrResponse.Length == 4 && ArrResponse[3] == "ERROR")
            {
                ResponseSid = ArrResponse[3];
                ECUFlashing.FlashLogs.Add("BUS INIT ERROR");
                return;
            }
            else if (ArrResponse.Length == 2 && ArrResponse[0] == "NO")
            {
                Thread.Sleep(5000);
                msg = "1085" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);
                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }
                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[4];
            }
            if (ResponseSid == "50")
            {
                Debug.Print("StartSession 1085 Success");
                ECUFlashing.FlashLogs.Add("StartSession 성공");
                bConnected = true;

            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("StartSession 1085 Fail");
                ECUFlashing.FlashLogs.Add("StartSession 실패");
                bConnected = false;
                return;
            }
            msg = "ATSH8110F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8110F1 Change Success");
            }

            msg = "ATSH8210F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8110F1 Change Success");
            }
            //TesterPresent
            for (int i = 0; i < 10; i++)
            {
                msg = "3E01" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[4];

                if (ResponseSid == "7E")
                {
                    Debug.Print("TesterPresent Success");
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("TesterPresent Fail");
                    return;
                }
            }

            //StartRoutineByLocalIdentifier - EraseFlash
            string startAddress = Main.project.currentEngine.A2LMemorySegments[0].Address.ToString("X6");
            string memorySize = Main.project.currentEngine.A2LMemorySegments[0].Size.ToString("X6");
            string endAddress = (Main.project.currentEngine.A2LMemorySegments[0].Address + Main.project.currentEngine.A2LMemorySegments[0].Size - 1).ToString("X6");

            string endAddressTemp;
            if ((startAddress == "1E0000") && (memorySize == "01FE00"))
            {
                endAddressTemp = "1FFFFF";
            }
            else
            {
                endAddressTemp = endAddress;
            }
            //return;
            msg = "ATSH8810F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8810F1 Change Success");
            }
            Thread.Sleep(100);
            msg = "3102" + startAddress + endAddressTemp + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[3];

            if (ResponseSid == "71")
            {
                Debug.Print("StartRoutineByLocalIdentifier - EraseFlash Success");
                ECUFlashing.FlashLogs.Add("StartRoutineByLocalIdentifier - EraseFlash 성공");
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("StartRoutineByLocalIdentifier - EraseFlash Fail");
                ECUFlashing.FlashLogs.Add("StartRoutineByLocalIdentifier - EraseFlash 실패");
                return;
            }

            msg = "ATSH8210F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8210F1 Change Success");
            }
            //RequestRoutineResultByLocalIdentifier - EraseFlash
            bool bCompleterequestRoutineResult = true;
            while (bCompleterequestRoutineResult)
            {
                msg = "3302" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];

                if (ResponseSid == "73")
                {
                    Debug.Print("RequestRoutineResultByLocalIdentifier - EraseFlash Success");
                    ECUFlashing.FlashLogs.Add("RequestRoutineResultByLocalIdentifier - EraseFlash 성공");
                    bCompleterequestRoutineResult = false;
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("RequestRoutineResultByLocalIdentifier - EraseFlash Fail");
                }

                //Thread.Sleep(10);
            }
            Thread.Sleep(300);
            msg = "ATSH8810F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8810F1 Change Success");
            }
            //RequestDownload - WriteFlash
            msg = "34" + startAddress + "00" + memorySize + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);
            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }
            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[3];
            if (ResponseSid == "74")
            {
                Debug.Print("RequestDownload - WriteFlash Success");
                ECUFlashing.FlashLogs.Add("RequestDownload - WriteFlash 성공");
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("RequestDownload - WriteFlash Fail");
                ECUFlashing.FlashLogs.Add("RequestDownload - WriteFlash 실패");
                return;
            }


            msg = "ATSH8010F1" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(1);

            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[0];

            if (ResponseSid == "OK")
            {
                Debug.Print("Header 8010F1 Change Success");
            }

            Thread.Sleep(1);
            //TransferData - WriteFlash
            foreach (ExtractedHexFileData hexFileData in Main.project.currentEngine.extractedHexFiles)
            {

                if (hexFileData.prgType != jnsoft.ASAP2.MEMORYPRG_TYPE.DATA)
                {
                    continue;
                }

                int offsetSendedStrByteLen = 0;
                int sumSendedSize = 0;
                int sendingByteSize = 0;
                int MaxSize = 128;
                progressbarMaximum = hexFileData.Size;
                ECUFlashing.FlashLogs.Add("TransferData 시작");
                for (int i = 0; i <= hexFileData.Size / MaxSize; i++)
                {
                    int remaining = hexFileData.Size - sumSendedSize;
                    progressbarValue = sumSendedSize;
                    Debug.Print((remaining / MaxSize).ToString());
                    if (remaining > MaxSize)
                    {
                        sendingByteSize = MaxSize;
                    }
                    else if (remaining <= MaxSize && remaining > 0)
                    {
                        sendingByteSize = remaining;
                    }
                    else if (remaining == 0)
                    {
                        break;
                    }
                    string sendingData = hexFileData.Data.Substring(offsetSendedStrByteLen, sendingByteSize * 2);

                    msg = "36" + sendingData + Environment.NewLine;
                    OBDSerial.Write(msg);
                    Debug.Print(msg);

                    Thread.Sleep(1);
                    if (!mre.WaitOne(responseTimeout))
                    {
                        Debug.Print("Did not receive response");
                    }

                    ArrResponse = Response.Split(' ');

                    if (ArrResponse.Length == 2 && ArrResponse[0] == "NO")
                    {
                        Thread.Sleep(1000);

                        offsetSendedStrByteLen += sendingByteSize * 2; // SubString 용도
                        sumSendedSize += sendingByteSize; //전송된 바이트 사이즈 체크 용도

                        continue;
                        //bool bCompleteTransfer = true;
                        //while(bCompleteTransfer)
                        //{
                        //    msg = "36" + sendingData + Environment.NewLine;
                        //    OBDSerial.Write(msg);
                        //    Debug.Print(msg);

                        //    Thread.Sleep(1);
                        //    if (!mre.WaitOne(responseTimeout))
                        //    {
                        //        Debug.Print("Did not receive response");
                        //    }

                        //    ArrResponse = Response.Split(' ');

                        //    try
                        //    {
                        //        ResponseSid = ArrResponse[4];
                        //        bCompleteTransfer = false;
                        //    }
                        //    catch (Exception)
                        //    {
                        //        Debug.Print("TransferData - WriteFlash Fail");
                        //        return;
                        //    }
                        //}

                    }

                    ResponseSid = ArrResponse[4];

                    if (ResponseSid == "76")
                    {
                        Debug.Print("TransferData - WriteFlash {0} Success", i + 1);
                    }
                    else if (ResponseSid == "7F")
                    {
                        Debug.Print("TransferData - WriteFlash Fail");
                        return;
                    }

                    offsetSendedStrByteLen += sendingByteSize * 2; // SubString 용도
                    sumSendedSize += sendingByteSize; //전송된 바이트 사이즈 체크 용도

                }
                ECUFlashing.FlashLogs.Add("TransferData 완료");
                Thread.Sleep(1);

                msg = "ATSH8110F1" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(1);

                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[0];

                if (ResponseSid == "OK")
                {
                    Debug.Print("Header 8110F1 Change Success");
                }
                Thread.Sleep(1);

                //RequestTransferExit - WriteFlash
                msg = "37" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];

                if (ResponseSid == "77")
                {
                    Debug.Print("RequestTransferExit - WriteFlash Success");
                    ECUFlashing.FlashLogs.Add("RequestTransferExit - WriteFlash 성공");
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("RequestTransferExit - WriteFlash Fail");
                    ECUFlashing.FlashLogs.Add("RequestTransferExit - WriteFlash 실패");
                    return;
                }
                Thread.Sleep(1);

                msg = "ATSH8A10F1" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(1);

                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[0];

                if (ResponseSid == "OK")
                {
                    Debug.Print("Header 8A10F1 Change Success");
                }
                Thread.Sleep(1);

                //StartRoutineByLocalIdentifier – compareChecksum
                msg = "3101" + startAddress + endAddress + Main.project.currentEngine.extractedHexFiles[0].Checksum + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];

                if (ResponseSid == "71")
                {
                    Debug.Print("StartRoutineByLocalIdentifier – compareChecksum Success");
                    ECUFlashing.FlashLogs.Add("StartRoutineByLocalIdentifier – compareChecksum 성공");
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("StartRoutineByLocalIdentifier – compareChecksum Fail");
                    ECUFlashing.FlashLogs.Add("StartRoutineByLocalIdentifier – compareChecksum 실패");
                    return;
                }

                msg = "ATSH8210F1" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(1);

                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }

                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[0];

                if (ResponseSid == "OK")
                {
                    Debug.Print("Header 8210F1 Change Success");
                }
                //RequestRoutineResultsByLocalIdentifier - compareChecksum
                msg = "3301" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }
                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];
                if (ResponseSid == "73")
                {
                    Debug.Print("RequestRoutineResultsByLocalIdentifier - compareChecksum Success");
                    ECUFlashing.FlashLogs.Add("RequestRoutineResultsByLocalIdentifier - compareChecksum 성공");
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("RequestRoutineResultsByLocalIdentifier - compareChecksum Fail");
                    ECUFlashing.FlashLogs.Add("RequestRoutineResultsByLocalIdentifier - compareChecksum 실패");
                    return;
                }

                //ECU Reset
                msg = "1101" + Environment.NewLine;
                OBDSerial.Write(msg);
                Debug.Print(msg);

                Thread.Sleep(commandDelay);
                if (!mre.WaitOne(responseTimeout))
                {
                    Debug.Print("Did not receive response");
                }
                ArrResponse = Response.Split(' ');
                ResponseSid = ArrResponse[3];
                if (ResponseSid == "51")
                {
                    Debug.Print("ECU Reset Success");
                    ECUFlashing.FlashLogs.Add("ECU Reset 성공");
                }
                else if (ResponseSid == "7F")
                {
                    Debug.Print("ECU Reset Fail");
                    ECUFlashing.FlashLogs.Add("ECU Reset 실패");
                    return;
                }

            }

            ECUFlashing.FlashLogs.Add("ECU 전원 조작 중...");
            ECUFlashing.ReadyFlash(10000);
            OBDCommunicationStop();
            status = KlineStatus.Normal;
            OBDCommunicationStart();
            Thread.Sleep(1000);
            OBDCommunicationStop();
            Thread.Sleep(1000);
            OBDCommunicationStart();

            if (ReadMapVersion())
            {
                ECUFlashing.FlashLogs.Add("ECU 통신 성공");

                ReadEngineSerial();
                ReadFlashDate();

                ECUFlashing.FlashLogs.Add("플래시 후 ECU 통신 성공 - 정상 Flash");
                ECUFlashing.FlashLogs.Add("플래시 Map Version : " + mapVersion);
                ECUFlashing.FlashLogs.Add("플래시 날짜 : " + flashDate);
                ECUFlashing.FlashLogs.Add("엔진 시리얼 번호 : " + engineSerial);

                ClearDTC();
                ECUFlashing.FlashLogs.Add("DTC Clear");
            }
            else
            {
                ECUFlashing.FlashLogs.Add("ECU 통신 실패");
            }

            ECUFlashing.FlashLogs.Add("작업 완료");

            StartGetData();
        }
        void Run()
        {
            while (status == KlineStatus.Normal)
            {
                GetMeasuredData();

                ReadDTC();

                if (doClear)
                {
                    ClearDTC();
                    doClear = false;
                }
                System.Threading.Thread.Sleep(800);
            }
        }
        bool ReadMapVersion()
        {
            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;
            if (!OBDSerial.IsOpen)
            {
                return false;
            }
            msg = "1A97" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');

            if (ArrResponse[0] == "NO")
            {
                return false;
            }

            ResponseSid = ArrResponse[3];

            if (ResponseSid == "5A")
            {
                string str = string.Empty;

                for (int i = 5; i < ArrResponse.Length - 2; i++)//마지막에 ? 찍히는거 천무기준 ArrResponse.Length-2. 다른 엔진으로 확인필요.
                {
                    if (ArrResponse[i] == "")
                    {
                        continue;
                    }
                    str += ArrResponse[i];
                }

                byte[] ba = KWP2000.StringToByteArray(str);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                this.mapVersion = temp;

                return true;
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("ReadMapVersion Fail");
                return false;
            }

            return false;
        }
        void ReadEngineSerial()
        {
            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;
            if (!OBDSerial.IsOpen)
            {
                return;
            }
            msg = "1A9B" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[3];

            if (ResponseSid == "5A")
            {
                string str = string.Empty;

                for (int i = 5; i < ArrResponse.Length - 2; i++)//마지막에 ? 찍히는거 천무기준 ArrResponse.Length-2. 다른 엔진으로 확인필요.
                {
                    if (ArrResponse[i] == "")
                    {
                        continue;
                    }
                    str += ArrResponse[i];
                }

                byte[] ba = KWP2000.StringToByteArray(str);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                this.engineSerial = temp;
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("ReadEngineSerial Fail");
                return;
            }
        }
        void ReadFlashDate()
        {
            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;
            if (!OBDSerial.IsOpen)
            {
                return;
            }
            msg = "1A99" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[3];

            if (ResponseSid == "5A")
            {
                string str = string.Empty;

                for (int i = 5; i < ArrResponse.Length - 2; i++)//마지막에 ? 찍히는거 천무기준 ArrResponse.Length-2. 다른 엔진으로 확인필요.
                {
                    if (ArrResponse[i] == "")
                    {
                        continue;
                    }
                    str += ArrResponse[i];
                }

                byte[] ba = KWP2000.StringToByteArray(str);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                this.flashDate = temp;
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("ReadFlashDate Fail");
                return;
            }
        }
        void GetMeasuredData()
        {
            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;
            if (!OBDSerial.IsOpen)
            {
                return;
            }

            msg = "2102" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }
            ArrResponse = Response.Split(' ');
            //전원꺼졌을때 NO DATA 오류처리 필요.
            if (ArrResponse[0] == "NO" || ArrResponse[0] == "" || ArrResponse[0] == "STOPPED")
            {
                status = KlineStatus.None;
                bConnected = false;
                return;

            }
            ResponseSid = ArrResponse[4];
            if (ResponseSid == "61")
            {
                Debug.Print("ReadData Success");


                List<VariableInfo> varlist = Main.project.sysVarInfo.EngineCom_Variables;

                foreach (VariableInfo var in varlist)
                {
                    int FirstByteNum = var.BitIndex;
                    int ByteLen = var.ByteNumber;
                    try
                    {
                        int acutualHighByteNum = FirstByteNum + 6;
                        string hi = ArrResponse[acutualHighByteNum];
                        string lo = ArrResponse[acutualHighByteNum + 1];
                        byte[] ba = StringToByteArray(lo + hi);

                        var.IOValue = BitConverter.ToUInt16(ba, 0);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Debug.Print("Data Error. 중간에 연결 끊어짐.");
                    }
                }


            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("ReadData Fail");
                return;
            }
        }

        int prevDTCNum = -1;
        string prevDTCCode = string.Empty;
        void ReadDTC()
        {
            string ResponseSid1;
            string ResponseSid2;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;

            msg = "1800FF00" + Environment.NewLine;
            if (!OBDSerial.IsOpen)
            {
                return;
            }
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }
            ArrResponse = Response.Split(' ');
            //NO DATA 처리
            if (ArrResponse[0] == "NO" || ArrResponse[0] == "")
            {
                status = KlineStatus.None;
                bConnected = false;
                return;
            }
            ResponseSid1 = ArrResponse[3];
            ResponseSid2 = ArrResponse[4];
            if (ResponseSid1 == "58" || ResponseSid2 == "58")
            {
                Debug.Print("ReadDTC Success");
                int dtcNum = -1;
                int forLoopStartNum = -1;
                if (ResponseSid1 == "58")
                {
                    dtcNum = Convert.ToInt32(ArrResponse[4], 16);
                    forLoopStartNum = 5;
                }
                else
                {
                    dtcNum = Convert.ToInt32(ArrResponse[5], 16);
                    forLoopStartNum = 6;
                }

                if (dtcNum == 0)
                {

                    if (prevDTCNum == dtcNum)
                    {
                        return;
                    }

                    for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
                    {
                        Main.project.currentEngine.ECUFaults[i].Checked = false;
                    }

                    prevDTCNum = 0;
                }
                else
                {

                    string str = string.Empty;

                    for (int i = forLoopStartNum; i < forLoopStartNum + (dtcNum * 3); i += 3)
                    {
                        str += ArrResponse[i] + ArrResponse[i + 1];

                    }

                    if (prevDTCCode == str)
                    {
                        return;
                    }

                    prevDTCCode = str;

                    CheckECUFaultList(str);
                }
            }
            else if (ResponseSid1 == "7F")
            {
                Debug.Print("ReadDTC Fail");
                return;
            }
        }
        void CheckECUFaultList(string str)
        {
            if (Main.project.currentEngine.ECUFaults == null)
            {
                return;
            }

            for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
            {
                Main.project.currentEngine.ECUFaults[i].Checked = false;
            }

            while (str.Length >= 4)
            {

                string subCode = str.Substring(0, 4);

                if (subCode == "AAAA")
                {
                    break;
                }

                str = str.Substring(4, str.Length - 4);

                string firstString = subCode[0].ToString();
                int firstValue = int.Parse(firstString, System.Globalization.NumberStyles.HexNumber);
                if (firstValue >= 0 && firstValue <= 3)
                {
                    string prefix = "P";
                    prefix = prefix + firstValue.ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 4 && firstValue <= 7)
                {
                    string prefix = "C";
                    prefix = prefix + (firstValue - 4).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 8 && firstValue <= 11)
                {
                    string prefix = "B";
                    prefix = prefix + (firstValue - 8).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 12 && firstValue <= 15)
                {
                    string prefix = "U";
                    prefix = prefix + (firstValue - 12).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }

                for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
                {

                    if (Main.project.currentEngine.ECUFaults[i].PCode == subCode)
                    {
                        Main.project.currentEngine.ECUFaults[i].Checked = true;
                    }
                }
            }
        }
        void ClearDTC()
        {
            string ResponseSid;
            string[] ArrResponse;
            var responseTimeout = TimeSpan.FromSeconds(10);
            string msg = string.Empty;

            //RequestRoutineResultsByLocalIdentifier - compareChecksum
            msg = "14FF00" + Environment.NewLine;
            OBDSerial.Write(msg);
            Debug.Print(msg);

            Thread.Sleep(commandDelay);
            if (!mre.WaitOne(responseTimeout))
            {
                Debug.Print("Did not receive response");
            }

            ArrResponse = Response.Split(' ');
            ResponseSid = ArrResponse[3];

            if (ResponseSid == "54")
            {
                Debug.Print("ClearDTC Success");
            }
            else if (ResponseSid == "7F")
            {
                Debug.Print("ClearDTC Fail");
                return;
            }
        }
        public void StartGetData()
        {
            if (bConnected)
            {
                thGetData = new Thread(new ThreadStart(Run));
                thGetData.Start();
            }
        }
        public void StopGetData()
        {
            status = KlineStatus.None;
            thGetData?.Abort();
            thGetData?.Join();
        }
        public void OBDCommunicationStop()
        {
            status = KlineStatus.None;
            OBDSerial.Close();

            if (thGetData != null)
            {
                thGetData.Abort();
            }
        }
        byte[] FullBuffer = new byte[50000];
        int ReadByte = 0;

        private void OBDSerial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                if (OBDSerial.IsOpen)
                {

                    List<string> listResponse = new List<string>();

                    int count = OBDSerial.BytesToRead;
                    byte[] NewBuffer = new byte[count];

                    OBDSerial.Read(NewBuffer, 0, count);

                    Buffer.BlockCopy(NewBuffer, 0, FullBuffer, ReadByte, count);
                    ReadByte += count;

                    string response = Encoding.Default.GetString(NewBuffer);
                    if (response.Contains(">"))
                    {
                        response = Encoding.Default.GetString(FullBuffer, 0, ReadByte);
                        ReadByte = 0;

                        string[] arrSplit = new string[1] { "\r" };
                        string[] arrResoponse = response.Split(arrSplit, StringSplitOptions.None);

                        if (arrResoponse.Length <= 4)
                        {
                            Response = arrResoponse[0];
                        }
                        else if (arrResoponse.Length > 4)
                        {
                            Responses = ReadECUIdentification(arrResoponse);
                        }
                        Debug.Print(response);
                        mre.Set();
                    }
                }
            }
            catch (Exception err)
            {
                Debug.Print(err.Message);
            }
        }

        private void OBDSerial_ErrorReceived(object sender, System.IO.Ports.SerialErrorReceivedEventArgs e)
        {
            Debug.Print(e.ToString());
        }
        private string[] ReadECUIdentification(string[] responses)
        {
            string[] result = new string[responses.Length - 3];
            for (int i = 1; i < responses.Length - 2; i++)
            {
                string[] arrTemp = responses[i].Split(' ');
                string hexString = "";
                for (int j = 5; j < arrTemp.Length; j++)
                {
                    hexString += arrTemp[j];
                }

                result[i - 1] = Encoding.ASCII.GetString(StringToByteArray(hexString));
            }

            return result;
        }
        private string MakeSecurityKeyKeyForEngine(string strseed)
        {
            byte[] seed = StringToByteArray(strseed);

            byte[] localseed = new byte[5] { 0, 0, 0, 0, 0 };
            DWORD localseed_wort;
            byte[] key = new byte[4] { 0, 0, 0, 0 };


            const DWORD mask = 0xAC806D45; // DHIM Factory Access03 04
            const DWORD mask1 = 0x8CA645D3; // Vehicle manufacturer Access 07 08
            const DWORD mask2 = 0x0D3E9C6B;  // Service adjustment Access 0b 0c

            DWORD masktemp = 0;

            switch (seed[0])
            {
                case 0x03: masktemp = mask; break;
                case 0x07: masktemp = mask1; break;
                case 0x0b: masktemp = mask2; break;
            }

            if (seed[1] == 0 && seed[2] == 0)
            {
                return null;
            }
            else
            {
                var a = (DWORD)seed[1];
                var b = (DWORD)seed[1] << 24;
                localseed_wort = ((DWORD)seed[1] << 24) + ((DWORD)seed[2] << 16) +
            ((DWORD)seed[3] << 8) + (DWORD)seed[4];

                for (int i = 0; i < 35; i++)
                {
                    uint condition = localseed_wort & 0x80000000;
                    if (condition > 1)
                    {
                        localseed_wort = localseed_wort << 1;
                        //
                        localseed_wort = localseed_wort ^ masktemp;
                    }
                    else
                    {
                        localseed_wort = localseed_wort << 1;
                    }
                }
                localseed = BitConverter.GetBytes(localseed_wort);
                for (int i = 0; i < 4; i++)
                {
                    key[3 - i] = localseed[i];
                }
            }

            return ByteArrayToString(key);
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:X2}", b);
            return hex.ToString();
        }

        public bool Import_Variables(string path)
        {
            VariableInfo varItem;
            int Index = 0;
            string[] Lines;
            try
            {
                Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            }
            catch (Exception)
            {
                return false;
            }
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            if (Main.project.sysVarInfo.EngineCom_Variables != null)
            {
                Main.project.sysVarInfo.EngineCom_Variables.Clear();
            }
            else
            {
                Main.project.sysVarInfo.EngineCom_Variables = new List<VariableInfo>();
            }

            if (varlist != null)
            {
                try
                {
                    foreach (string Kline_Data in varlist)
                    {
                        string[] arrKline_Data = Kline_Data.Split(',');
                        string variableName = arrKline_Data[1];
                        string norminalName = arrKline_Data[2];
                        string unit = arrKline_Data[3];
                        bool use_Alarm = (arrKline_Data[4] == "TRUE") ? true : false;
                        int alarm_Low = int.Parse(arrKline_Data[5]);
                        int alarm_High = int.Parse(arrKline_Data[6]);
                        bool use_Warning = (arrKline_Data[7] == "TRUE") ? true : false;
                        int warning_Low = int.Parse(arrKline_Data[8]);
                        int warning_High = int.Parse(arrKline_Data[9]);
                        bool use_Averaging = (arrKline_Data[10] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrKline_Data[11]);
                        int round = int.Parse(arrKline_Data[12]);
                        string formula = arrKline_Data[13];
                        int FirstByteNum = int.Parse(arrKline_Data[14]);
                        int ByteLen = int.Parse(arrKline_Data[15]);
                        varItem = new VariableInfo(variableName, norminalName);

                        varItem.CalType = CalibrationType.CalByValue;
                        varItem.Unit = unit;
                        varItem.AlarmUse = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.WarningUse = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.Formula = formula;
                        varItem.BitIndex = FirstByteNum;
                        varItem.ByteNumber = ByteLen;
                        varItem.Index = Index;

                        Main.project.sysVarInfo.EngineCom_Variables.Add(varItem);
                        Index++;
                    }
                }
                catch (Exception)
                {
                    Debug.Print("Kline Engine File Import 에러");
                    return false;
                }
            }

            return true;

        }
    }
}
