﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using org.mariuszgromada.math.mxparser;
using System.Collections;
namespace DSIC_Local
{
    public enum CalibrationType
    {
        None = 0,
        CalByTable = 1,
        CalByValue = 2,
        CalByMapping = 3,
        Formula = 4,
        BitInValue = 5,
        String = 6,
    }

    public enum VariableType
    {
        CNFODAQ,
        CALCULATION,
        ENGINE,
        RGV,
        NONE,
    }

    [Serializable]
    public class SysVarInfo
    {

        public int DataLoggingInterval = 100;
        public bool DataLoggingIntervalData = false;

        public List<VariableInfo> ALL_Variables = new List<VariableInfo>();

        public List<VariableInfo> CNFODAQ_Variables = new List<VariableInfo>();
        public List<VariableInfo> RGV_Variables = new List<VariableInfo>();

        public List<VariableInfo> EngineCom_Variables = new List<VariableInfo>();

        public List<VariableInfo> Calculation_Variables = new List<VariableInfo>();

        public List<VariableInfo> InstantDataLogging_Variables = new List<VariableInfo>();
        public List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();

        public List<VariableInfo> Alarm_Variables = new List<VariableInfo>();

        public DataTable table = new DataTable();

        public VariableType getPredefineLabel(VariableInfo var)
        {

            foreach (VariableInfo IOvar in CNFODAQ_Variables)
            {
                if (var.NorminalName == IOvar.NorminalName)
                    return VariableType.CNFODAQ;
            }
            foreach (VariableInfo IOvar in Calculation_Variables)
            {
                if (var.NorminalName == IOvar.NorminalName)
                {
                    return VariableType.CALCULATION;
                }
            }
            foreach (VariableInfo IOvar in EngineCom_Variables)
            {
                if (var.NorminalName == IOvar.NorminalName)
                    return VariableType.ENGINE;
            }
            return VariableType.NONE;

        }

        public List<VariableInfo> getVariables(int Index)
        {
            List<VariableInfo> Variables = null;

            switch ((VariableType)Index)
            {
                case VariableType.CNFODAQ:
                    Variables = CNFODAQ_Variables;
                    break;
                case VariableType.CALCULATION:
                    Variables = Calculation_Variables;
                    break;
                case VariableType.ENGINE:
                    Variables = EngineCom_Variables;
                    break;
                case VariableType.RGV:
                    Variables = RGV_Variables;
                    break;

            }

            return Variables;

        }

        public void RenewAllvariables()
        {
            try
            {
                ALL_Variables.Clear();

                foreach (VariableInfo var in CNFODAQ_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in Calculation_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in EngineCom_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in RGV_Variables)
                {
                    ALL_Variables.Add(var);
                }

                foreach (VariableInfo var in ALL_Variables)
                {

                    for (int ind = 0; ind < InstantDataLogging_Variables.Count; ind++)
                    {
                        if (var.NorminalName == InstantDataLogging_Variables[ind].NorminalName)
                        {
                            InstantDataLogging_Variables[ind] = var;
                        }
                    }
                    for (int ind = 0; ind < ConstantDataLogging_Variables.Count; ind++)
                    {
                        if (var.NorminalName == ConstantDataLogging_Variables[ind].NorminalName)
                        {
                            ConstantDataLogging_Variables[ind] = var;
                        }
                    }
                    for (int ind = 0; ind < Alarm_Variables.Count; ind++)
                    {
                        if (var.NorminalName == Alarm_Variables[ind].NorminalName)
                        {
                            Alarm_Variables[ind] = var;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
            }
        }

        public SysVarInfo()
        { }

        public void ClearLists()
        {
            ALL_Variables.Clear();

            CNFODAQ_Variables.Clear();

            Calculation_Variables.Clear();

            EngineCom_Variables.Clear();

            RGV_Variables.Clear();

            InstantDataLogging_Variables.Clear();
            ConstantDataLogging_Variables.Clear();

            Alarm_Variables.Clear();
        }

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }
        public void Load(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysVarInfo varinfo = new SysVarInfo();
            varinfo = (SysVarInfo)dd;


            this.CNFODAQ_Variables = varinfo.CNFODAQ_Variables;

            this.Calculation_Variables = varinfo.Calculation_Variables;

            this.EngineCom_Variables = varinfo.EngineCom_Variables;

            this.RGV_Variables = varinfo.RGV_Variables;

            this.InstantDataLogging_Variables = varinfo.InstantDataLogging_Variables;
            this.ConstantDataLogging_Variables = varinfo.ConstantDataLogging_Variables;

            RenewAllvariables();

            this.ConstantDataLogging_Variables = varinfo.ConstantDataLogging_Variables;
            MatchingVariables(ConstantDataLogging_Variables);
            this.Alarm_Variables = varinfo.Alarm_Variables;
            MatchingVariables(Alarm_Variables);

            varinfo = null;

        }
        public VariableInfo FindVariable(string VarName)
        {
            VariableInfo searchedVar = null;
            foreach (VariableInfo Var in ALL_Variables)
            {
                if (Var.NorminalName == VarName)
                {
                    searchedVar = Var;
                    break;
                }
            }
            if (searchedVar == null)
            {
                foreach (VariableInfo Var in ALL_Variables)
                {
                    if (Var.VariableName == VarName)
                    {
                        searchedVar = Var;
                        break;
                    }
                }
            }

            return searchedVar;
        }
        private void AddAllvariables(List<VariableInfo> newVariables)
        {
            foreach (VariableInfo var in newVariables)
            {
                ALL_Variables.Add(var);
            }
        }


        private void MatchingVariables(List<VariableInfo> varList)
        {
            for (int i = 0; i < varList.Count; i++)
            {
                VariableInfo var = varList[i];
                foreach (VariableInfo varTarget in ALL_Variables)
                {
                    if (var.VariableName == varTarget.VariableName && var.Unit == varTarget.Unit && var.NorminalName == varTarget.NorminalName)
                    {
                        varList[i] = varTarget;
                        break;
                    }
                }

            }

        }

        public void SaveVariables(string FileName, List<VariableInfo> Variables)
        {
            ALL_Variables.Clear();
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(Variables.GetType());
            xs.Serialize(fs, Variables);
            fs.Close();

            RenewAllvariables();

        }

        public List<VariableInfo> LoadVariables(string FileName)
        {
            List<VariableInfo> Variables = new List<VariableInfo>();
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(this.GetType());
                Object dd = xs.Deserialize(fs);
                fs.Close();
                Variables = (List<VariableInfo>)dd;
            }
            catch
            {
                MessageBox.Show("File Format Not Matched. Try other file.");
            }
            return Variables;
        }
    }

    [Serializable]
    public class ValuePair
    {
        public double IOValue = 0;
        public double RealValue = 0.0;
        public ValuePair()
        {
        }

        public ValuePair(double IOValue, double RealValue)
        {
            this.IOValue = IOValue;
            this.RealValue = RealValue;
        }
    }

    public enum AlarmState
    {
        Normal = 0,
        Warning = 1,
        Alarm = 2,
        INF = 3,
        NAN = 4,
    }
    public enum AlarmMethod
    {
        None = 0,
        PopUp = 1,
        Idle,
        Stop,
    }


    [Serializable]
    [DefaultPropertyAttribute("VariableName")]
    public class VariableInfo
    {
        private double _IOValue = 0.0;
        private double _RealValue = 0.0;
        private string _Unit;
        private string _Formula;
        private string _VariableName;
        public string _NorminalName;
        private string _NorminalDescription;

        private double _Range_High = 0;
        private double _Range_Low = 0;

        private bool _AlarmUse;
        private bool _WarningUse;

        private double _Warning_Low;
        private double _Warning_High;
        private double _Alarm_Low;
        private double _Alarm_High;
        private int _Round = 2;
        private double _Offset = 0;
        private double _Factor = 1;
        
        private int byteNumber = 0;
        private int bitIndex = 0;


        private int alarmAllowableTime = 5;

        private TimeSpan alarmStartTime;
        private TimeSpan alarmElapsedTime;
        private bool alarmAlreadyError = false;

        private TimeSpan warningStartTime;
        private TimeSpan warningElapsedTime;
        private bool warningAlreadyError = false;


        private CalibrationType _CalType = CalibrationType.CalByValue;

        private bool _Checked = false; //Variable Assign 용도로만 쓰임.

        [CategoryAttribute("Value"), DescriptionAttribute("Calibration Type. by Table or by Value")]
        public CalibrationType CalType
        {
            get { return _CalType; }
            set { _CalType = value; }
        }

        [CategoryAttribute("Value"), DescriptionAttribute("Converting Offset for Real Value")]
        public double Offset
        {
            get { return _Offset; }
            set { _Offset = value; }
        }
        [CategoryAttribute("Value"), DescriptionAttribute("Converting Factor for Real Value")]
        public double Factor
        {
            get { return _Factor; }
            set { _Factor = value; }
        }
        [CategoryAttribute("Limit"), DescriptionAttribute("Warning Limit at Low Level")]
        public double Warning_Low
        {
            get { return _Warning_Low; }
            set { _Warning_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Warning Limit at High Level")]
        public double Warning_High
        {
            get { return _Warning_High; }
            set { _Warning_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Alarm Limit at Low Level")]
        public double Alarm_Low
        {
            get { return _Alarm_Low; }
            set { _Alarm_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Alarm Limit at High Level")]
        public double Alarm_High
        {
            get { return _Alarm_High; }
            set { _Alarm_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Minumum Value")]
        public double Range_Low
        {
            get { return _Range_Low; }
            set { _Range_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Maximum Value")]
        public double Range_High
        {
            get { return _Range_High; }
            set { _Range_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Use Alarm Limit")]
        public bool AlarmUse
        {
            get { return _AlarmUse; }
            set { _AlarmUse = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Use Warning Limit")]
        public bool WarningUse
        {
            get { return _WarningUse; }
            set { _WarningUse = value; }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Round Number below decimal point.")]
        public int Round
        {
            get { return _Round; }
            set { _Round = value; }
        }

        [CategoryAttribute("Basic Property"), DescriptionAttribute("Alarm Limit at High Level")]
        public string VariableName
        {
            get { return _VariableName; }
            set { _VariableName = value; }
        }

        [CategoryAttribute("Basic Property"), DescriptionAttribute("Norminal Name for the Variable. Read Only")]
        public string NorminalName
        {
            get { return _NorminalName; }
        }
        [CategoryAttribute("Basic Property"), DescriptionAttribute("Description for the Variable. Read Only")]
        public string NorminalDescription
        {
            get { return _NorminalDescription; }
            set { _NorminalDescription = value; }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("Calculation Formula for Calculating Variable. Not Permitable For Other Type Variable")]
        public string Formula
        {
            get { return _Formula; }
            set { _Formula = value; }
        }
        [CategoryAttribute("Basic Property"), DescriptionAttribute("Unit of the Variable. ")]
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("If true, Calculating Average Value. ")]
        public bool Averaging
        {
            get
            {
                return _Averaging;
            }
            set
            {
                _Averaging = value;
                if (!value)
                {
                    _IOValueList.Clear();
                }
            }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("Averaging Count")]
        public int AveragingCount
        {
            get
            {
                return _AveragingCount;
            }
            set
            {
                _AveragingCount = value;
                if (value < 2)
                {
                    _Averaging = false;
                }
                _IOValueList.Clear();
            }
        }


        public int Tag;
        public int Index;
        public bool Removable;
        public int IOIndex;
        public bool _Averaging = false;
        public int _AveragingCount = 5;
        public List<ValuePair> CalibrationTable = new List<ValuePair>();

        public AlarmState alarmState;
        public AlarmMethod alarmMethod;

        //public List<VariableInfo> CalcVariables = new List<VariableInfo>();
        public List<double> _IOValueList = new List<double>();
        public string abbreviation;
        
        [CategoryAttribute("Value"), DescriptionAttribute("Value by Hardware")]
        public double IOValue
        {
            get
            {
                if (!_Averaging || _IOValueList.Count == 0)
                    return _IOValue;
                else
                    return _IOValueList[0];
            }
            set
            {
                if (!_Averaging)
                    _IOValue = value;
                else
                {
                    try
                    {
                        lock (_IOValueList)
                        {
                            if (_IOValueList.Count > _AveragingCount)
                                _IOValueList.RemoveAt(_IOValueList.Count - 1);
                            _IOValueList.Insert(0, value);
                        }
                    }
                    catch (Exception Exception) { Debug.Print(Exception.ToString()); }
                }
            }
        }


        [CategoryAttribute("Value"), DescriptionAttribute("Value by Calculating")]
        public double RealValue
        {

            get
            {

                double sum = 0;
                if (!_Averaging || _IOValueList.Count == 0)
                    return Math.Round(GetRealValue(_IOValue), Round);
                else
                {
                    int maxNum = 0;
                    lock (_IOValueList)
                    {
                        maxNum = (_IOValueList.Count > _AveragingCount ? _AveragingCount : _IOValueList.Count);
                        for (int i = 0; i < maxNum; i++)
                        {
                            sum += GetRealValue(_IOValueList[i]);
                        }
                    }

                    return Math.Round(sum / maxNum, Round);
                }
            }
            set
            {
                _RealValue = value;
            }
        }

        public bool Checked { get => _Checked; set => _Checked = value; }
        public int ByteNumber { get => byteNumber; set => byteNumber = value; }
        public int BitIndex { get => bitIndex; set => bitIndex = value; }
        public int AlarmAllowableTime { get => alarmAllowableTime; set => alarmAllowableTime = value; }
        public TimeSpan AlarmElapsedTime { get => alarmElapsedTime; set => alarmElapsedTime = value; }
        public bool AlarmAlreadyError { get => alarmAlreadyError; set => alarmAlreadyError = value; }
        public TimeSpan AlarmStartTime { get => alarmStartTime; set => alarmStartTime = value; }
        public TimeSpan WarningStartTime { get => warningStartTime; set => warningStartTime = value; }
        public TimeSpan WarningElapsedTime { get => warningElapsedTime; set => warningElapsedTime = value; }
        public bool WarningAlreadyError { get => warningAlreadyError; set => warningAlreadyError = value; }

        public object ShallowCopy()
        {
            return (VariableInfo)this.MemberwiseClone();
        }
        public double GetRealValue(double IOValue)
        {

            if (this.VariableName == "수정하중2")
            {

            }
            double RValue = 0;

            if (_CalType == CalibrationType.CalByTable)
            {

                if (CalibrationTable.Count == 0)
                {
                    RValue = IOValue;
                }
                else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
                {
                    RValue = (double)CalibrationTable[0].RealValue / (double)CalibrationTable[0].IOValue * (double)IOValue; // physical value = d / c * Rawdata.
                }
                else if (CalibrationTable.Count == 2)//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
                {
                    RValue = ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) / ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) * ((double)IOValue - (double)CalibrationTable[1].IOValue) + (double)CalibrationTable[1].RealValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
                }
                else if (CalibrationTable.Count > 2)//2개 인것과 똑같은데 처음에 적정 구간 찾는 부분이 추가됨
                {
                    int i = 0;

                    for (i = 0; i < CalibrationTable.Count - 1; i++)
                    {
                        if (CalibrationTable[i].IOValue <= IOValue && CalibrationTable[i + 1].IOValue >= IOValue)
                        {
                            break;
                        }
                    }

                    RValue = ((double)CalibrationTable[i].RealValue - (double)CalibrationTable[i + 1].RealValue) / ((double)CalibrationTable[i].IOValue - (double)CalibrationTable[i + 1].IOValue) * ((double)IOValue - (double)CalibrationTable[i + 1].IOValue) + (double)CalibrationTable[i + 1].RealValue;
                }
            }
            else if (_CalType == CalibrationType.CalByValue)
            {
                string strFormula = this.Formula;
                strFormula = strFormula.Replace(" ", "");
                string[] split = strFormula.Split('=');

                strFormula = split[1].Replace("int", IOValue.ToString());

                Expression e = new Expression(strFormula);

                try
                {
                    RValue = e.calculate();
                }
                catch (Exception e1)
                {
                    RValue = 0;
                }
            }
            else if (_CalType == CalibrationType.Formula)
            {
                bool isRadian = false;


                if (this.Formula != "")
                {
                    if (this.VariableName == "수정계수_전자")
                    {
                        RValue = Main.K;
                    }
                    else
                    {
                        if (this.VariableName == "수정연료소비량")// 수정출력에 출력이라는 변수이름이 포함됐을때 생기는 문제 어떻게 해결할건지
                        {

                        }
                        string strFormula = VariableToRealValue(Formula);
                        Expression e = new Expression(strFormula);
                        try
                        {
                            RValue = e.calculate();
                        }
                        catch (Exception e1)
                        {

                        }
                    }
                }
                else
                {
                    if (this.VariableName == "매연" || this.VariableName == "오일압력_Kline")
                    {
                        RValue = IOValue;
                    }
                    else
                    {
                        RValue = 0;
                    }
                }
            }
            else if (_CalType == CalibrationType.BitInValue)
            {
                BitArray ba;
                IOValue = Main.project.sysVarInfo.FindVariable(this.NorminalDescription).RealValue;

                int value = (int)IOValue;
                ba = new BitArray(new int[] { value });

                if (ba[BitIndex])
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else if (_CalType == CalibrationType.String)
            {
                RValue = IOValue;
            }
            else
            {
                RValue = IOValue * _Factor + _Offset;
            }

            return RValue;
        }
        public static string VariableToRealValue(string Formula)
        {
            string strFormula = Formula.Replace(" ","");

            char[] chars = new char[] { '+', '-', '*', '/', '^', '(', ')' };

            string[] temp = strFormula.Split(chars);
            List<Tuple<string,string>> pairs = new List<Tuple<string, string>>();

            for (int j = 0; j < temp.Length; j++)
            {
                for (int k = 0; k < Main.project.sysVarInfo.ALL_Variables.Count; k++)
                {
                    if (temp[j] == Main.project.sysVarInfo.ALL_Variables[k].VariableName)
                    {
                        if (Main.project.sysVarInfo.ALL_Variables[k].VariableName == "수정계수두번째")
                        {

                        }
                        pairs.Add(new Tuple<string, string>(temp[j], Main.project.sysVarInfo.ALL_Variables[k].RealValue.ToString()));
                        break;
                    }
                }
            }
            
            for(int r=0; r<pairs.Count; r++)
            {
                strFormula = strFormula.Replace(pairs[r].Item1, pairs[r].Item2);
            }

            strFormula = strFormula.Replace("+-", "-");

            return strFormula;
        }
        public VariableInfo Clone()
        {
            VariableInfo var = new VariableInfo();
            var.VariableName = this.VariableName;
            var._NorminalName = this.NorminalName;
            var.NorminalDescription = this.NorminalDescription;
            var.Tag = this.Tag;
            var.Index = this.Index;
            var.Removable = this.Removable;
            var.Formula = this.Formula;
            var.IOIndex = this.IOIndex;
            var.IOValue = this.IOValue;
            var.CalibrationTable = this.CalibrationTable;
            var.Unit = this.Unit;
            var.Warning_Low = this.Warning_Low;
            var.Warning_High = this.Warning_High;
            var.Alarm_Low = this.Alarm_Low;
            var.Alarm_High = this.Alarm_High;
            var.alarmState = this.alarmState;
            var.alarmMethod = this.alarmMethod;
            var.Round = this.Round;
            foreach (ValuePair vp in this.CalibrationTable)
            {
                var.CalibrationTable.Add(new ValuePair(vp.IOValue, vp.RealValue));
            }

            return var;
        }
        public VariableInfo()
        { }

        public VariableInfo(string VariableName, string NorminalName)
        {
            this._NorminalName = NorminalName;
            this.VariableName = VariableName;
        }
        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Tag, int Index)
        {
            this._NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Tag = Tag;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Index)
        {
            this._NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(string VariableName)
        {
            this.VariableName = VariableName;
            this.Removable = true;
        }
    }

}
