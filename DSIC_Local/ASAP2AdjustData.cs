﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jnsoft.ASAP2;
using jnsoft.ASAP2.Values;
using jnsoft.Helpers;
using System.Globalization;
using System.IO;
using System.Diagnostics;

namespace DSIC_Local
{
    public class ASAP2AdjustData
    {
        public bool CreateMemorySegmentOnlyFromA2L(EngineInformation currentEngine)
        {
            try
            {
                using (var a2lParser = new A2LParser())
                {
                    // parse specified A2L file
                    if (!a2lParser.parse(Application.StartupPath + currentEngine.A2LPath))
                        // not an a2l file
                        return false;

                    // do further checks
                    var project = a2lParser.Project;
                    var module = project.getNode<A2LMODULE>(true);
                    var initialSegments = module.createInitialMemorySegments(false);

                    var dataFile = DataFile.open(Application.StartupPath + currentEngine.ShipmentHexPath, initialSegments);

                    var modPar = module.getNode<A2LMODPAR>(true);
                    if (modPar != null && !string.IsNullOrEmpty(modPar.EPK) && !dataFile.checkEPK(modPar.EPKAddress, modPar.EPK))
                        // epk check failed
                        throw new ArgumentException("EPK check failed");

                    currentEngine.A2LMemorySegments = dataFile.SegmentList;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
          }
        public string MakeNewHexFile(EngineInformation currentEngine, FlashType flashType)
        {
            var prevColor = Console.ForegroundColor;

            try
            {
                using (var a2lParser = new A2LParser())
                {
                    // parse specified A2L file
                    if (!a2lParser.parse(Application.StartupPath + currentEngine.A2LPath))
                        // not an a2l file
                        return null;

                    // do further checks
                    var project = a2lParser.Project;
                    var module = project.getNode<A2LMODULE>(true);
                    var initialSegments = module.createInitialMemorySegments(false);

                    // Load the data file
                    var dataFile = DataFile.open(Main.localMapFilePath, initialSegments);

                    // Do the EPK check on the file
                    var modPar = module.getNode<A2LMODPAR>(true);
                    if (modPar != null && !string.IsNullOrEmpty(modPar.EPK) && !dataFile.checkEPK(modPar.EPKAddress, modPar.EPK))
                        // epk check failed
                        throw new ArgumentException("EPK check failed");

                    currentEngine.A2LMemorySegments = dataFile.SegmentList;

                    uint minAddress = 0;
                    uint maxAddress = 0;
                    foreach (var characteristic in module.enumChildNodes<A2LCHARACTERISTIC>())
                    {
                        if (minAddress == 0)
                            minAddress = characteristic.Address;
                        else if (minAddress > characteristic.Address)
                            minAddress = characteristic.Address;

                        if (maxAddress == 0)
                            maxAddress = characteristic.Address;
                        else if (maxAddress < characteristic.Address)
                            maxAddress = characteristic.Address;
                    }

                    string strMinAddress = minAddress.ToString("X2");
                    string strMaxAddress = maxAddress.ToString("X2");
                    if(flashType == FlashType.Dyno)
                    {
                        foreach (QsettingParameter param in currentEngine.qsettingParameters)
                        {
                            if (param.Usage == false)
                            {
                                continue;
                            }
                            
                            var searchedCharacteristic = module.findNode<A2LCHARACTERISTIC>(param.Name);

                            switch (searchedCharacteristic.CharType)
                            {
                                case CHAR_TYPE.VALUE:
                                    ICharValue singleValue = CharacteristicValue.getValue<SingleValue>(dataFile, searchedCharacteristic, ValueObjectFormat.Physical);

                                    singleValue.Value = (double)param.Value;
                                    CharacteristicValue.setValue(dataFile, searchedCharacteristic, singleValue);

                                    break;
                                case CHAR_TYPE.ASCII:
                                    ICharValue asciiValue = CharacteristicValue.getValue<ASCIIValue>(dataFile, searchedCharacteristic, ValueObjectFormat.Physical);

                                    asciiValue.Value = (string)param.Value;
                                    CharacteristicValue.setValue(dataFile, searchedCharacteristic, asciiValue);
                                    break;
                                case CHAR_TYPE.CURVE:
                                    ICharValue curveValue = CharacteristicValue.getValue<CurveValue>(dataFile, searchedCharacteristic, ValueObjectFormat.Physical);

                                    double[][] arrcurve = param.Value as double[][];

                                    double[][] arrTemp = new double[1][];
                                    arrTemp[0] = arrcurve[0];

                                    curveValue.AxisValue = arrTemp;
                                    curveValue.Value = arrcurve[1];
                                    CharacteristicValue.setValue(dataFile, searchedCharacteristic, curveValue);
                                    break;
                            }
                        }
                    }

                    var engineSerial = module.findNode<A2LCHARACTERISTIC>("CUID_D_DSID_CUC[]");

                    ICharValue serialNumber = CharacteristicValue.getValue<ASCIIValue>(dataFile, engineSerial, ValueObjectFormat.Physical);

                    if (Main.resultData.engineNumber.Length > 30)
                    {
                        Main.resultData.engineNumber = Main.resultData.engineNumber.Substring(0, 30);
                    }

                    serialNumber.Value = Main.resultData.engineNumber;
                    CharacteristicValue.setValue(dataFile, engineSerial, serialNumber);

                    var flashDate = module.findNode<A2LCHARACTERISTIC>("CUID_D_PRGDAT_CUC[]");
                    //var flashDate = module.findNode<A2LCHARACTERISTIC>("CUID_D_SYS_CUC[]");
                    flashDate.CharType = CHAR_TYPE.ASCII;

                    ICharValue date = CharacteristicValue.getValue<ASCIIValue>(dataFile, flashDate, ValueObjectFormat.Physical);

                    date.Value = DateTime.Now.ToString("yyyyMMdd");
                    CharacteristicValue.setValue(dataFile, flashDate, date);
                    //Main.localMapFilePath
                    Console.WriteLine($"{Path.GetFileName(Main.localMapFilePath)}={(dataFile.IsDirty ? "dirty" : "unchanged")}");
                    if (dataFile.IsDirty)
                    { // save the changed datafile
                        var destFile = Application.StartupPath + currentEngine.ChangedHexFolderPath + "Hex_" +flashType.ToString()+"_"+ DateTime.Now.ToString("yyMMdd_HHmm") + Path.GetExtension(Main.localMapFilePath);
                        bool saved = dataFile.save(destFile, false);

                        if(currentEngine.Engine == Engine.DV11 && currentEngine.Ees != EES.Euro3)//위의 헥스 트리트먼트 라이브러리는 모든 인텔 로우 데이터가 렝스가 동일한 경우만 고려함. 그러나 Euro 4, 5의 데이터의 경우 렝스가 변하는 부분이 있고 그 부분의 데이터가 지워짐. 원본에서 지워진 부분을 찾아 수정된 Hex파일에 추가해주는 부분.
                        {
                            AddRemovedData(Main.localMapFilePath, destFile);
                        }

                        Console.WriteLine($"{(saved ? "successfully saved" : "failed to save")} to {destFile}");

                        return destFile;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something failed {ex.Message}");
                return null;
            }
            finally { Console.ForegroundColor = prevColor; }
        }
        public void AddRemovedData(string originPath, string changedPath)
        {
            //string prevLine = string.Empty;
            string line = string.Empty;
            string type = string.Empty;
            int count = 0;
            string removedData = string.Empty;
            StreamReader origin = new StreamReader(originPath, Encoding.Default);

            while ((line = origin.ReadLine()) != null)
            {
                type = line.Substring(1, 2);
                
                if (type == "02" || type == "20" || type == "10")
                {
                    if (line == ":02000004001FDB")
                    {
                        count++;
                    }

                    if (count > 1)
                    {
                        //if(removedData == string.Empty)
                        //{
                        //    removedData += prevLine + "\n";
                        //}
                        removedData += line + "\n";
                    }
                }
                else//마지막에 type 00으로 종료됨.
                {
                    break;
                }

                //prevLine = line;
            }
            removedData = removedData.Substring(0, removedData.Length - 1);
            string[] removed = removedData.Split('\n');
            
            string[] changed = File.ReadAllLines(changedPath);
            List<string> list = new List<string>();
            list.AddRange(changed);

            int startIndex = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if(list[i]== ":020000040001F9")
                {
                    startIndex = i;
                }
            }
            
            list.InsertRange(startIndex, removed);

            File.WriteAllLines(changedPath, list.ToArray());
        }
        public List<ExtractedHexFileData> ExtractDataFromHexFile(EngineInformation engine, string destFile)
        {
            List<ExtractedHexFileData> extractedHexFiles = new List<ExtractedHexFileData>();
            List<MemorySegment> msl = engine.A2LMemorySegments;

            foreach (MemorySegment ms in msl)
            {
                if (engine.Engine == Engine.DX22)
                {
                    if (!(ms.PrgType == MEMORYPRG_TYPE.OFFLINE_DATA || ms.PrgType == MEMORYPRG_TYPE.CODE || ms.PrgType == MEMORYPRG_TYPE.DATA))
                    {
                        continue;
                    }
                }
                else if (engine.Engine == Engine.DV11)
                {
                    if (!(ms.PrgType == MEMORYPRG_TYPE.DATA))
                    {
                        continue;
                    }

                    if(engine.Ees != EES.Euro3)
                    {
                        ms.Size = 0x01FE00;
                    }
                }
                

                string strAddress = ms.Address.ToString("X8");
                uint a2lFrontAddress = Convert.ToUInt32(strAddress.Substring(0, 4) + "0000", 16);

                string line = string.Empty;
                string type = string.Empty;
                uint dataLen = 0;
                uint prevDataOffset = 0;
                uint dataOffset = 0;
                uint extractedDataSize = 0;
                StringBuilder data = new StringBuilder();
                string dataOnLine = string.Empty;
                bool checkCompleteDataExtraction = false;
                
                StreamReader sr = new StreamReader(destFile, Encoding.Default);

                while ((line = sr.ReadLine()) != null)
                {
                    type = line.Substring(1, 2);


                    if (type != "02")
                    {
                        continue;
                    }

                    uint hexFrontAddress = Convert.ToUInt32(line.Substring(9, 4) + "0000", 16);

                    if (a2lFrontAddress < hexFrontAddress || a2lFrontAddress > hexFrontAddress + 65535)
                    {
                        continue;
                    }

                    Debug.Print(hexFrontAddress.ToString("X2"));

                    //StartAddress에 맞는 FrontAddress 찾은 상태
                    dataLen = 0;
                    prevDataOffset = 0;
                    dataOffset = 0;
                    extractedDataSize = 0;
                    data = new StringBuilder();
                    dataOnLine = string.Empty;

                    while ((line = sr.ReadLine()) != null)
                    {

                        dataLen = Convert.ToUInt32(line.Substring(1, 2), 16);
                        dataOffset += dataLen;

                        //prevDataOffset = prevDataOffset == 0 ? 0 : dataOffset - dataLen;

                        if (!(ms.Address >= hexFrontAddress + prevDataOffset && ms.Address < hexFrontAddress + prevDataOffset + dataLen))
                        {
                            prevDataOffset += dataLen;
                            continue;
                        }

                        //if (!(a2lFrontAddress >= hexFrontAddress + prevDataOffset && a2lFrontAddress < hexFrontAddress + dataOffset))
                        //{
                        //    continue;
                        //}

                        //여기로 오면 startAddress를 포함한 row를 읽은 상태
                        uint startPointOnRow = hexFrontAddress + prevDataOffset - ms.Address;

                        //일단 한 줄의 길이보다 사이즈가 작은 경우는 배제. 메모리세그먼트 사이즈는 32바이트보다 길다고 가정. 만약 이에 벗어나는 케이스가 존재하면 코드 수정 필요
                        dataOnLine = line.Substring((int)(9 + (startPointOnRow * 2)), (int)dataLen * 2);//첫번째줄 찾음
                        data.Append(dataOnLine);
                        extractedDataSize += dataLen - startPointOnRow;

                        //Debug.Print(dataOnLine);
                        //Front Address변경되는 경우 체크하면서 이어서 읽어나감. 
                        while ((line = sr.ReadLine()) != null)
                        {
                            type = line.Substring(1, 2);

                            if (type == dataLen.ToString("X2"))
                            {

                                if (ms.Size - extractedDataSize <= dataLen)
                                {
                                    //마지막 row
                                    var remaining = ms.Size - extractedDataSize;
                                    dataOnLine = line.Substring(9, (int)remaining * 2);
                                    data.Append(dataOnLine);
                                    //Debug.Print(dataOnLine);
                                    int dataSize = data.Length / 2;

                                    if (dataSize == ms.Size)
                                    {
                                        //사이즈 일치하면 일단 맞게 추출된걸로 간주
                                        checkCompleteDataExtraction = true;
                                    }
                                    break;
                                }
                                else
                                {
                                    dataOnLine = line.Substring(9, (int)dataLen * 2);
                                    data.Append(dataOnLine);
                                    //Debug.Print(dataOnLine);
                                    extractedDataSize += dataLen;
                                }
                            }
                            else if (type == "02")
                            {
                                //Front Address가 바뀜 또는 이전 줄에서 렝스가 변함. 그냥 넘기면 됨. 데이터는 어찌됐든 이어져야 하니까. 실제로도 이어짐.
                                //Debug.Print(line);
                            }
                            else if(type == "10") // 위에 코드는 렝스 20인 경우로 가정하고 짜여짐. 10(dec로 16)일때 따로 맞춰서 하드코딩.
                            {
                                if (ms.Size - extractedDataSize <= 16)
                                {
                                    //마지막 row
                                    var remaining = ms.Size - extractedDataSize;
                                    dataOnLine = line.Substring(9, (int)remaining * 2);
                                    data.Append(dataOnLine);
                                    //Debug.Print(dataOnLine);
                                    int dataSize = data.Length / 2;

                                    if (dataSize == ms.Size)
                                    {
                                        //사이즈 일치하면 일단 맞게 추출된걸로 간주
                                        checkCompleteDataExtraction = true;
                                    }
                                    break;
                                }
                                else
                                {
                                    dataOnLine = line.Substring(9, (int)16 * 2);
                                    data.Append(dataOnLine);
                                    //Debug.Print(dataOnLine);
                                    extractedDataSize += 16;
                                }
                            }
                        }
                        break;
                    }

                    break;
                }
                
                if (checkCompleteDataExtraction && engine.Engine == Engine.DV11 && engine.Ees == EES.Euro3)
                {
                    string data_CalculatedChecksum = MakeInternalChecksum_Euro3(data.ToString());
                    string calculatedChecksum = CalculateChecksum(data_CalculatedChecksum, ms.Address, ms.Size);
                    extractedHexFiles.Add(new ExtractedHexFileData(ms.PrgType, data_CalculatedChecksum, calculatedChecksum));
                }
                else if (checkCompleteDataExtraction && engine.Engine == Engine.DV11 && (engine.Ees == EES.Euro4 || engine.Ees == EES.Euro5))
                {
                    string data_CalculatedChecksum = MakeInternalChecksum_Euro45(data.ToString());
                    string calculatedChecksum = CalculateChecksum(data_CalculatedChecksum, ms.Address, ms.Size);
                    extractedHexFiles.Add(new ExtractedHexFileData(ms.PrgType, data_CalculatedChecksum, calculatedChecksum));
                }
                else if(checkCompleteDataExtraction && engine.Engine != Engine.DV11)
                {
                    string calculatedChecksum = CalculateChecksum(data.ToString(), ms.Address, ms.Size);
                    extractedHexFiles.Add(new ExtractedHexFileData(ms.PrgType, data.ToString(), calculatedChecksum));
                }

            }

            return extractedHexFiles;
        }
        string MakeInternalChecksum_Euro45(string data)
        {
            string monitoring = data.Substring(0x13A9C * 2, 0x158 * 2);
            byte[] baMonitoring = KWP2000.StringToByteArray(monitoring);
            data = data.Remove(0x13BF4 * 2, 8).Insert(0x13BF4 * 2, MakeInternalChecksumString(baMonitoring));

            string data0 = data.Substring(0, (0xFFF4 * 2));
            byte[] baData0 = KWP2000.StringToByteArray(data0);
            data = data.Remove(0x00FFF4 * 2, 8).Insert(0xFFF4 * 2, MakeInternalChecksumString(baData0));

            string data1 = data.Substring(0x010000 * 2, 0xDFF4 * 2);
            byte[] baData1 = KWP2000.StringToByteArray(data1);
            data = data.Remove(0x1DFF4 * 2, 8).Insert(0x1DFF4 * 2, MakeInternalChecksumString(baData1));

            return data;
        }
        string MakeInternalChecksum_Euro3(string data)
        {
            string monitoring = data.Substring(0xFD98 * 2, 0x154 * 2);
            byte[] baMonitoring = KWP2000.StringToByteArray(monitoring);
            data = data.Remove(0xFEEC * 2, 8).Insert(0xFEEC * 2, MakeInternalChecksumString(baMonitoring));

            string data0 = data.Substring(0x0000, 0x7F7C * 2);
            byte[] baData0 = KWP2000.StringToByteArray(data0);
            data = data.Remove(0x7F7C * 2, 8).Insert(0x7F7C * 2, MakeInternalChecksumString(baData0));

            string data1 = data.Substring(0x8000 * 2, 0x7F7C * 2);
            byte[] baData1 = KWP2000.StringToByteArray(data1);
            data = data.Remove(0xFF7C * 2, 8).Insert(0xFF7C * 2, MakeInternalChecksumString(baData1));


            return data;
        }
        string MakeInternalChecksumString(byte[] ba)
        {
            long checksum = 0;
            long checksumTemp = 0;

            for (int i = 0; i < ba.Length; i++)
            {
                checksumTemp = MakeInternalChecksumByCurByte(ba[i], i);
                checksum += checksumTemp;
            }

            checksumTemp = (uint)(checksum & 0x00000000FFFFFFFF);
            checksumTemp = 0xCAFEAFFE - (checksumTemp + 0xFADECAFE);

            checksum = checksumTemp;

            checksum = (uint)(checksum & 0x00000000FFFFFFFF);

            byte[] baChecksumTemp = BitConverter.GetBytes(checksum);
            byte[] baChecksum = new byte[4] { baChecksumTemp[3], baChecksumTemp[2], baChecksumTemp[1], baChecksumTemp[0] };
            string str = BitConverter.ToString(baChecksum).Replace("-", "");

            return str;
        }
        int MakeInternalChecksumByCurByte(byte msg, int index)
        {
            int calcResult = 0;
            int extractedMilestone = index % 4;

            switch (extractedMilestone)
            {
                case 0:
                    calcResult = (int)((int)msg << 24);
                    break;
                case 1:
                    calcResult = (int)((int)msg << 16);
                    break;
                case 2:
                    calcResult = (int)((int)msg << 8);
                    break;
                case 3:
                default:
                    calcResult = (int)msg;
                    break;
            }

            return calcResult;
        }
        public string CalculateChecksum(string data)
        {
            ushort sumOfByte = 0;

            byte[] arrchangedData = KWP2000.StringToByteArray(data);

            for (int i = 0; i < arrchangedData.Length; i++)
            {
                sumOfByte += arrchangedData[i];
            }

            var result = sumOfByte.ToString("X4");

            return result;

        }
        public string CalculateChecksum(string data, uint startAddress, int size)
        {
            var endAddress = startAddress + size;
            var strEndAddress = endAddress.ToString("X8");
            var last2Address = strEndAddress.Substring(6, 2);


            var changedData = data.Substring(0, size * 2);



            ushort sumOfByte = 0;

            byte[] arrchangedData = KWP2000.StringToByteArray(changedData);


            for (int i = 0; i < arrchangedData.Length; i++)
            {
                sumOfByte += arrchangedData[i];
            }

            var result = sumOfByte.ToString("X4");

            return result;
        }
    }
}


