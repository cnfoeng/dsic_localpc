﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DSIC_Local.Step;

namespace DSIC_Local.Control
{
    public partial class ManualControlPad : DevExpress.XtraEditors.XtraForm
    {
        string RemoteDemandName = Properties.Settings.Default.Remote_Demand;
        string RemoteFeedbackName = Properties.Settings.Default.Remote_Feedback;
        string ResetName = Properties.Settings.Default.Reset;
        string EngineStartStatusName = Properties.Settings.Default.PLC_Engine_Start_STATUS;
        string EngineStartOnName = Properties.Settings.Default.PLC_Engine_Start_ON;
        string ECUPowerStatusName = Properties.Settings.Default.PLC_ECU_Power_STATUS;
        string ECUPowerOnName = Properties.Settings.Default.PLC_ECU_Power_ON;

        string SpeedDemandName = Properties.Settings.Default.BME_Speed_Demand;
        string TorqueDemandName = Properties.Settings.Default.BME_Torque_Demand;
        string ThrottleDemandName = Properties.Settings.Default.PLC_Throttle_Demand;

        string SpeedFeedbackName = Properties.Settings.Default.BME_Speed_FeedBack;
        string TorqueFeedbackName = Properties.Settings.Default.BME_Torque_Feedback;

        string BME_Mode_Speed = Properties.Settings.Default.BME_Mode_Speed;
        string BME_Mode_Torque = Properties.Settings.Default.BME_Mode_Torque;

        VariableInfo RemoteDemandVar;
        VariableInfo RemoteFeedbackVar;
        VariableInfo ResetVar;
        VariableInfo EngineStartStatusVar;
        VariableInfo EngineStartOnVar;
        VariableInfo ECUPowerStatusVar;
        VariableInfo ECUPowerOnVar;

        VariableInfo SpeedDemandVar;
        VariableInfo TorqueDemandVar;
        VariableInfo ThrottleDemandVar;

        VariableInfo SpeedFeedbackVar;
        VariableInfo TorqueFeedbackVar;

        VariableInfo modeSpeedVar;
        VariableInfo modeTorqueVar;

        double dynoControlDuration = 0;
        double dynoControlElapsed = 0;
        double engineControlDuration = 0;
        double engineControlElapsed = 0;

        bool dynoRampRun = false;
        bool engineRampRun = false;

        bool useTorque = true;

        double CurrentTarget = 0;
        public ManualControlPad()
        {
            InitializeComponent();

            RemoteDemandVar = Main.project.sysVarInfo.FindVariable(RemoteDemandName);
            RemoteFeedbackVar = Main.project.sysVarInfo.FindVariable(RemoteFeedbackName);
            ResetVar = Main.project.sysVarInfo.FindVariable(ResetName);
            EngineStartStatusVar = Main.project.sysVarInfo.FindVariable(EngineStartStatusName);
            EngineStartOnVar = Main.project.sysVarInfo.FindVariable(EngineStartOnName);
            ECUPowerStatusVar = Main.project.sysVarInfo.FindVariable(ECUPowerStatusName);
            ECUPowerOnVar = Main.project.sysVarInfo.FindVariable(ECUPowerOnName);

            SpeedDemandVar = Main.project.sysVarInfo.FindVariable(SpeedDemandName);
            TorqueDemandVar = Main.project.sysVarInfo.FindVariable(TorqueDemandName);
            ThrottleDemandVar = Main.project.sysVarInfo.FindVariable(ThrottleDemandName);

            SpeedFeedbackVar = Main.project.sysVarInfo.FindVariable(SpeedFeedbackName);
            TorqueFeedbackVar = Main.project.sysVarInfo.FindVariable(TorqueFeedbackName);


            if (RemoteFeedbackVar.RealValue == 1)
            {
                if (Main.project.sysVarInfo.FindVariable(BME_Mode_Speed).RealValue == 1 && Main.project.sysVarInfo.FindVariable(BME_Mode_Torque).RealValue == 0)
                {
                    cbDynoMode.Text = "스피드";
                    lbDynoFeedback.Text = "스피드 피드백값";
                    lbDynoTarget.Text = "스피드 타겟값";
                    lbDynoSet.Text = "스피드 설정값";
                }
                else if (Main.project.sysVarInfo.FindVariable(BME_Mode_Speed).RealValue == 0 && Main.project.sysVarInfo.FindVariable(BME_Mode_Torque).RealValue == 1)
                {
                    cbDynoMode.Text = "토크";
                    lbDynoFeedback.Text = "토크 피드백값";
                    lbDynoTarget.Text = "토크 타겟값";
                    lbDynoSet.Text = "토크 설정값";
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (cbDynoMode.Text == "스피드")
            {
                lbDynoFeedback.Text = "스피드 피드백값";
                lbDynoTarget.Text = "스피드 타겟값";
                lbDynoSet.Text = "스피드 설정값";
                useTorque = true;
            }
            else if (cbDynoMode.Text == "토크")
            {
                lbDynoFeedback.Text = "토크 피드백값";
                lbDynoTarget.Text = "토크 타겟값";
                lbDynoSet.Text = "토크 설정값";
                useTorque = true;
            }
            else if (cbDynoMode.Text == "하중")
            {
                lbDynoFeedback.Text = "하중 피드백값";
                lbDynoTarget.Text = "하중 타겟값";
                lbDynoSet.Text = "하중 설정값";
                useTorque = false;
            }

            if (RemoteFeedbackVar.RealValue == 1)
            {
                cbtnRemote.Checked = true;
                cbtnRemote.Appearance.BackColor = Color.FromArgb(192, 255, 192);


                if (cbDynoMode.Text == "스피드")
                {
                    if (!(Main.project.sysVarInfo.FindVariable(BME_Mode_Speed).RealValue == 1 && Main.project.sysVarInfo.FindVariable(BME_Mode_Torque).RealValue == 0))
                    {
                        SetMode_Speed();
                    }
                }
                else if (cbDynoMode.Text == "토크")
                {
                    if (!(Main.project.sysVarInfo.FindVariable(BME_Mode_Speed).RealValue == 0 && Main.project.sysVarInfo.FindVariable(BME_Mode_Torque).RealValue == 1))
                    {
                        SetMode_Torque();
                        useTorque = true;
                    }
                }
                else if (cbDynoMode.Text == "하중")
                {
                    if (!(Main.project.sysVarInfo.FindVariable(BME_Mode_Speed).RealValue == 0 && Main.project.sysVarInfo.FindVariable(BME_Mode_Torque).RealValue == 1))
                    {
                        SetMode_Torque();
                        useTorque = false;
                    }
                }
            }
            else
            {
                cbtnRemote.Checked = false;
                cbtnRemote.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (cbDynoMode.Text == "스피드")
            {
                teDynoFeedback.Text = SpeedFeedbackVar.RealValue.ToString();
                teDynoTarget.Text = SpeedDemandVar.RealValue.ToString();
            }
            else if (cbDynoMode.Text == "토크")
            {
                teDynoFeedback.Text = TorqueFeedbackVar.RealValue.ToString();
                teDynoTarget.Text = TorqueDemandVar.RealValue.ToString();
            }
            else if ((cbDynoMode.Text == "하중"))
            {
                teDynoFeedback.Text = Math.Round((TorqueFeedbackVar.RealValue / 0.7162), 0).ToString();
                teDynoTarget.Text = Math.Round((TorqueDemandVar.RealValue / 0.7162), 0).ToString();
            }

            if (EngineStartStatusVar.RealValue == 1)
            {
                cbtnEngineStart.Checked = true;
            }
            else
            {
                cbtnEngineStart.Checked = false;
            }

            if (ECUPowerStatusVar.RealValue == 1)
            {
                cbtnECUPower.Checked = true;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnECUPower.Checked = false;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (dynoRampRun || engineRampRun)
            {
                btnRampStop.Enabled = true;
            }
            else
            {
                btnRampStop.Enabled = false;
            }

            System.Threading.Thread.Sleep(300);
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            Main.statusForDAQ.ResetDemand = true;
            Main.statusForDAQ.SetPLCStatus();
            System.Threading.Thread.Sleep(300);
            Main.statusForDAQ.ResetDemand = false;
            Main.statusForDAQ.SetPLCStatus();
        }

        double dynoFirstValue;
        double dynoTargetValue;
        double engineFirstValue;
        double engineTargetValue;

        VariableInfo dynoVarForSet;
        VariableInfo engineVarForSet;
        private void btnDynoSet_Click(object sender, EventArgs e)
        {
            SetDynoValue(double.Parse(teDynoSet.Text));
        }

        private void btnEngineSet_Click(object sender, EventArgs e)
        {
            SetEngineValue(double.Parse(teEngineSet.Text));
        }
        private void SetDynoValue(double setValue)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            double dynoSetValue = 0;

            try
            {
                dynoSetValue = setValue;
            }
            catch (Exception)
            {

            }

            if (!useTorque)
            {
                dynoTargetValue = Math.Round((dynoSetValue * 0.7162), 0);
            }
            else
            {
                dynoTargetValue = dynoSetValue;
            }

            dynoControlElapsed = 0;
            tmrDynoRamp.Start();
            dynoRampRun = true;
        }

        private void SetEngineValue(double setValue)
        {
            if (!ReadyEngineSetValue())
            {
                return;
            }

            double engineSetValue = 0;

            try
            {
                engineSetValue = double.Parse(teEngineSet.Text);
            }
            catch (Exception)
            {

            }

            if (engineSetValue > 100)
            {
                engineSetValue = 100;
                teEngineSet.Text = "100";
            }

            engineTargetValue = engineSetValue;

            engineControlElapsed = 0;
            tmrEngineRamp.Start();
            engineRampRun = true;
        }
        bool ReadyDynoSetValue()
        {
            if (btnDynoSet.Enabled != true)
            {
                return false;
            }

            btnDynoSet.Enabled = false;
            btnDynoPlus1.Enabled = false;
            btnDynoPlus10.Enabled = false;
            btnDynoPlus100.Enabled = false;
            btnDynoMinus1.Enabled = false;
            btnDynoMinus10.Enabled = false;
            btnDynoMinus100.Enabled = false;
            cbDynoMode.Enabled = false;
            btnDynoSetZero.Enabled = false;

            dynoControlDuration = double.Parse(seRamp.Value.ToString());

            if (cbDynoMode.Text == "스피드")
            {
                dynoVarForSet = (VariableInfo)SpeedDemandVar;
                dynoFirstValue = SpeedFeedbackVar.RealValue;
            }
            else if (cbDynoMode.Text == "토크" || cbDynoMode.Text == "하중")
            {
                dynoVarForSet = (VariableInfo)TorqueDemandVar;
                dynoFirstValue = TorqueFeedbackVar.RealValue;
            }

            return true;
        }
        bool ReadyEngineSetValue()
        {
            if (btnEngineSet.Enabled != true)
            {
                return false;
            }

            btnEngineSet.Enabled = false;
            btnEngineSetZero.Enabled = false;

            engineControlDuration = double.Parse(seRamp.Value.ToString());
            
            engineVarForSet = (VariableInfo)ThrottleDemandVar;
            engineFirstValue = ThrottleDemandVar.RealValue;

            return true;
        }
        private void tmrDynoRamp_Tick(object sender, EventArgs e)
        {

            double sendData = 0;

            sendData = (dynoTargetValue - dynoFirstValue) / dynoControlDuration * dynoControlElapsed + dynoFirstValue;

            sendData = StepMode.GetIOValue(sendData, dynoVarForSet.CalibrationTable);

            if (dynoControlDuration > dynoControlElapsed)
            {
                Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)sendData);
                dynoControlElapsed = dynoControlElapsed + tmrDynoRamp.Interval / 1000.0;
            }
            else
            {
                sendData = dynoTargetValue;
                sendData = StepMode.GetIOValue(sendData, dynoVarForSet.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)sendData);

                btnDynoSet.Enabled = true;
                btnDynoPlus1.Enabled = true;
                btnDynoPlus10.Enabled = true;
                btnDynoPlus100.Enabled = true;
                btnDynoMinus1.Enabled = true;
                btnDynoMinus10.Enabled = true;
                btnDynoMinus100.Enabled = true;
                cbDynoMode.Enabled = true;
                btnDynoSetZero.Enabled = true;
                tmrDynoRamp.Stop();
                dynoRampRun = false;
            }
        }

        private void tmrEngineRamp_Tick(object sender, EventArgs e)
        {
            double sendData = 0;

            sendData = (engineTargetValue - engineFirstValue) / engineControlDuration * engineControlElapsed + engineFirstValue;

            sendData = StepMode.GetIOValue(sendData, engineVarForSet.CalibrationTable);

            if (engineControlDuration > engineControlElapsed)
            {
                Main.CNFODAQCom.SetMelsecData(engineVarForSet.NorminalDescription, (int)sendData);
                engineControlElapsed = engineControlElapsed + tmrEngineRamp.Interval / 1000.0;
            }
            else
            {
                sendData = engineTargetValue;
                sendData = StepMode.GetIOValue(sendData, engineVarForSet.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(engineVarForSet.NorminalDescription, (int)sendData);

                btnEngineSet.Enabled = true;
                btnEngineSetZero.Enabled = true;
                tmrEngineRamp.Stop();
                engineRampRun = false;
            }
        }

        private void cbtnRemote_Click(object sender, EventArgs e)
        {
            if (cbtnRemote.Checked)
            {
                Main.statusForDAQ.RemoteDemand = false;
                Main.statusForDAQ.SetPLCStatus();
            }
            else
            {
                Main.statusForDAQ.RemoteDemand = true;
                Main.statusForDAQ.SetPLCStatus();
            }
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDynoMode.Text == "스피드")
            {
                SetMode_Speed();
            }
            else if (cbDynoMode.Text == "토크" || cbDynoMode.Text == "하중")
            {
                SetMode_Torque();
            }
        }

        void SetMode_Speed()
        {
            Main.statusForDAQ.ModeDynoSpeed = true;
            Main.statusForDAQ.ModeDynoTorque = false;
            Main.statusForDAQ.SetPLCStatus();

            teDynoSet.Text = "3000";
            teEngineSet.Text = "0";
            seRamp.Text = "5";

            SetDynoValue(3000);
            SetEngineValue(0);
        }

        void SetMode_Torque()
        {

            Main.statusForDAQ.ModeDynoSpeed = false;
            Main.statusForDAQ.ModeDynoTorque = true;
            Main.statusForDAQ.SetPLCStatus();

            teDynoSet.Text = "0";
            teEngineSet.Text = "0";
            seRamp.Text = "5";

            SetDynoValue(0);
            SetEngineValue(0);
        }

        private void btnDynoPlus1_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (!useTorque)
            {
                dynoTargetValue = dynoVarForSet.RealValue + (1 * 0.7162);
            }
            else
            {
                dynoTargetValue = dynoVarForSet.RealValue + 1;
            }
            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;
        }

        private void btnDynoPlus10_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (!useTorque)
            {
                dynoTargetValue = dynoVarForSet.RealValue + (10 * 0.7162);
            }
            else
            {
                dynoTargetValue = dynoVarForSet.RealValue + 10;
            }

            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;
        }

        private void btnDynoPlus100_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (!useTorque)
            {
                dynoTargetValue = dynoVarForSet.RealValue + (100 * 0.7162);
            }
            else
            {
                dynoTargetValue = dynoVarForSet.RealValue + 100;
            }

            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;
        }

        private void btnDynoMinus1_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (dynoVarForSet.RealValue - 1 >= 0)
            {

                if (!useTorque)
                {
                    dynoTargetValue = dynoVarForSet.RealValue - (1 * 0.7162);
                }
                else
                {
                    dynoTargetValue = dynoVarForSet.RealValue - 1;
                }
            }
            else
            {
                dynoTargetValue = 0;
            }
            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;

        }

        private void btnDynoMinus10_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (dynoVarForSet.RealValue - 10 >= 0)
            {
                if (!useTorque)
                {
                    dynoTargetValue = dynoVarForSet.RealValue - (10 * 0.7162);
                }
                else
                {
                    dynoTargetValue = dynoVarForSet.RealValue - 10;
                }
            }
            else
            {
                dynoTargetValue = 0;
            }

            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;
        }

        private void btnDynoMinus100_Click(object sender, EventArgs e)
        {
            if (!ReadyDynoSetValue())
            {
                return;
            }
            if (dynoVarForSet.RealValue - 100 >= 0)
            {
                if (!useTorque)
                {
                    dynoTargetValue = dynoVarForSet.RealValue - (100 * 0.7162);
                }
                else
                {
                    dynoTargetValue = dynoVarForSet.RealValue - 100;
                }
            }
            else
            {
                dynoTargetValue = 0;
            }

            Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable));

            if (!useTorque)
            {
                teDynoSet.Text = Math.Round((dynoTargetValue / 0.7162), 0).ToString();
            }
            else
            {
                teDynoSet.Text = dynoTargetValue.ToString();
            }

            btnDynoSet.Enabled = true;
            btnDynoPlus1.Enabled = true;
            btnDynoPlus10.Enabled = true;
            btnDynoPlus100.Enabled = true;
            btnDynoMinus1.Enabled = true;
            btnDynoMinus10.Enabled = true;
            btnDynoMinus100.Enabled = true;
            cbDynoMode.Enabled = true;
            btnDynoSetZero.Enabled = true;
        }

        private void btnDynoSetZero_Click(object sender, EventArgs e)
        {

            if (!ReadyDynoSetValue())
            {
                return;
            }

            if (cbDynoMode.Text == "스피드")
            {
                dynoVarForSet = (VariableInfo)SpeedDemandVar;
            }
            else if (cbDynoMode.Text == "토크" || cbDynoMode.Text == "하중")
            {
                dynoVarForSet = (VariableInfo)TorqueDemandVar;
            }

            teDynoTarget.Text = "0";
            teDynoSet.Text = "0";
            dynoTargetValue = 0;

            dynoControlElapsed = 0;

            //Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable)); //SetZero 램프 존재 X
            tmrDynoRamp.Start(); //SetZero 램프 존재

            dynoRampRun = true;
        }

        private void btnEngineSetZero_Click(object sender, EventArgs e)
        {

            if (!ReadyEngineSetValue())
            {
                return;
            }

            engineVarForSet = (VariableInfo)ThrottleDemandVar;

            engineTargetValue = 0;

            teEngineSet.Text = "0";

            engineControlElapsed = 0;

            //Main.CNFODAQCom.SetMelsecData(engineVarForSet.NorminalDescription, (int)StepMode.GetIOValue(engineTargetValue, engineVarForSet.CalibrationTable)); //SetZero 램프 존재 X
            tmrEngineRamp.Start(); //SetZero 램프 존재

            engineRampRun = true;
        }

        private void cbtnEngineStart_Click(object sender, EventArgs e)
        {
            Idle();
            System.Threading.Thread.Sleep(500);

            if (cbtnEngineStart.Checked)
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }
        private void Idle()
        {
            string modeSpeedVariableName = Properties.Settings.Default.BME_Mode_Speed;
            string modeTorqueVariableName = Properties.Settings.Default.BME_Mode_Torque;

            modeSpeedVar = Main.project.sysVarInfo.FindVariable(modeSpeedVariableName);
            modeTorqueVar = Main.project.sysVarInfo.FindVariable(modeTorqueVariableName);

            if (modeSpeedVar.RealValue == 1 && modeTorqueVar.RealValue == 0)
            {

                double CurrentDyno = StepMode.GetIOValue(3000, SpeedDemandVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, ThrottleDemandVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(SpeedDemandVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(ThrottleDemandVar.NorminalDescription, (int)CurrentEngine);
            }
            else if (modeSpeedVar.RealValue == 0 && modeTorqueVar.RealValue == 1)
            {

                double CurrentDyno = StepMode.GetIOValue(0, TorqueDemandVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, ThrottleDemandVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(TorqueDemandVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(ThrottleDemandVar.NorminalDescription, (int)CurrentEngine);
            }
        }
        private void btnRampStop_Click(object sender, EventArgs e)
        {
            if (dynoRampRun)
            {
                double sendData = 0;
                dynoTargetValue = (dynoTargetValue - dynoFirstValue) / dynoControlDuration * dynoControlElapsed + dynoFirstValue;
                sendData = StepMode.GetIOValue(dynoTargetValue, dynoVarForSet.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(dynoVarForSet.NorminalDescription, (int)sendData);

                btnDynoSet.Enabled = true;
                btnDynoPlus1.Enabled = true;
                btnDynoPlus10.Enabled = true;
                btnDynoPlus100.Enabled = true;
                btnDynoMinus1.Enabled = true;
                btnDynoMinus10.Enabled = true;
                btnDynoMinus100.Enabled = true;
                cbDynoMode.Enabled = true;
                btnDynoSetZero.Enabled = true;
                tmrDynoRamp.Stop();
                dynoRampRun = false;
            }

            if (engineRampRun)
            {
                double sendData = 0;

                dynoTargetValue = (engineTargetValue - engineFirstValue) / engineControlDuration * engineControlElapsed + engineFirstValue;
                sendData = StepMode.GetIOValue(dynoTargetValue, engineVarForSet.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(engineVarForSet.NorminalDescription, (int)sendData);

                btnEngineSet.Enabled = true;
                btnEngineSetZero.Enabled = true;
                tmrEngineRamp.Stop();
                engineRampRun = false;
            }
        }


        private void seRamp_EditValueChanged(object sender, EventArgs e)
        {

            if (seRamp.Value < 0)
            {
                seRamp.Value = 0;
            }
        }

        private void ManualControlPad_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.manualControlPadForm = null;
        }

        private void cbtnECUPower_Click(object sender, EventArgs e)
        {
            if (cbtnECUPower.Checked)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }
    }
}