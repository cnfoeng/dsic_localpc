﻿namespace DSIC_Local.Control
{
    partial class ManualControlPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManualControlPad));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.seRamp = new DevExpress.XtraEditors.SpinEdit();
            this.btnRampStop = new DevExpress.XtraEditors.SimpleButton();
            this.cbtnEngineStart = new DevExpress.XtraEditors.CheckButton();
            this.btnEngineSet = new DevExpress.XtraEditors.SimpleButton();
            this.btnEngineSetZero = new DevExpress.XtraEditors.SimpleButton();
            this.teDynoTarget = new DevExpress.XtraEditors.TextEdit();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoMinus100 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoPlus100 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoMinus10 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoPlus10 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoMinus1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoPlus1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnDynoSet = new DevExpress.XtraEditors.SimpleButton();
            this.teDynoFeedback = new DevExpress.XtraEditors.TextEdit();
            this.cbtnRemote = new DevExpress.XtraEditors.CheckButton();
            this.cbDynoMode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.teDynoSet = new DevExpress.XtraEditors.TextEdit();
            this.btnDynoSetZero = new DevExpress.XtraEditors.SimpleButton();
            this.teEngineSet = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbDynoFeedback = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbDynoSet = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbDynoTarget = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tmrRefresh = new System.Windows.Forms.Timer(this.components);
            this.tmrDynoRamp = new System.Windows.Forms.Timer(this.components);
            this.tmrEngineRamp = new System.Windows.Forms.Timer(this.components);
            this.cbtnECUPower = new DevExpress.XtraEditors.CheckButton();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seRamp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoTarget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoFeedback.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDynoMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEngineSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoFeedback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dataLayoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(699, 497);
            this.panelControl1.TabIndex = 0;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.cbtnECUPower);
            this.dataLayoutControl1.Controls.Add(this.seRamp);
            this.dataLayoutControl1.Controls.Add(this.btnRampStop);
            this.dataLayoutControl1.Controls.Add(this.cbtnEngineStart);
            this.dataLayoutControl1.Controls.Add(this.btnEngineSet);
            this.dataLayoutControl1.Controls.Add(this.btnEngineSetZero);
            this.dataLayoutControl1.Controls.Add(this.teDynoTarget);
            this.dataLayoutControl1.Controls.Add(this.btnReset);
            this.dataLayoutControl1.Controls.Add(this.btnDynoMinus100);
            this.dataLayoutControl1.Controls.Add(this.btnDynoPlus100);
            this.dataLayoutControl1.Controls.Add(this.btnDynoMinus10);
            this.dataLayoutControl1.Controls.Add(this.btnDynoPlus10);
            this.dataLayoutControl1.Controls.Add(this.btnDynoMinus1);
            this.dataLayoutControl1.Controls.Add(this.btnDynoPlus1);
            this.dataLayoutControl1.Controls.Add(this.btnDynoSet);
            this.dataLayoutControl1.Controls.Add(this.teDynoFeedback);
            this.dataLayoutControl1.Controls.Add(this.cbtnRemote);
            this.dataLayoutControl1.Controls.Add(this.cbDynoMode);
            this.dataLayoutControl1.Controls.Add(this.teDynoSet);
            this.dataLayoutControl1.Controls.Add(this.btnDynoSetZero);
            this.dataLayoutControl1.Controls.Add(this.teEngineSet);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(2, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.ShareLookAndFeelWithChildren = false;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(695, 493);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // seRamp
            // 
            this.seRamp.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.seRamp.Location = new System.Drawing.Point(632, 58);
            this.seRamp.Name = "seRamp";
            this.seRamp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seRamp.Properties.Appearance.Options.UseFont = true;
            this.seRamp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seRamp.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.seRamp.Properties.IsFloatValue = false;
            this.seRamp.Properties.Mask.EditMask = "N00";
            this.seRamp.Size = new System.Drawing.Size(51, 32);
            this.seRamp.TabIndex = 23;
            this.seRamp.EditValueChanged += new System.EventHandler(this.seRamp_EditValueChanged);
            // 
            // btnRampStop
            // 
            this.btnRampStop.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRampStop.Appearance.Options.UseFont = true;
            this.btnRampStop.Enabled = false;
            this.btnRampStop.Location = new System.Drawing.Point(337, 58);
            this.btnRampStop.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRampStop.Name = "btnRampStop";
            this.btnRampStop.Size = new System.Drawing.Size(108, 32);
            this.btnRampStop.TabIndex = 22;
            this.btnRampStop.Text = "Ramp Stop";
            this.btnRampStop.Click += new System.EventHandler(this.btnRampStop_Click);
            // 
            // cbtnEngineStart
            // 
            this.cbtnEngineStart.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cbtnEngineStart.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnEngineStart.Appearance.Options.UseBackColor = true;
            this.cbtnEngineStart.Appearance.Options.UseFont = true;
            this.cbtnEngineStart.Location = new System.Drawing.Point(117, 58);
            this.cbtnEngineStart.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnEngineStart.Name = "cbtnEngineStart";
            this.cbtnEngineStart.Size = new System.Drawing.Size(104, 32);
            this.cbtnEngineStart.TabIndex = 21;
            this.cbtnEngineStart.Text = "Engine Start";
            this.cbtnEngineStart.Click += new System.EventHandler(this.cbtnEngineStart_Click);
            // 
            // btnEngineSet
            // 
            this.btnEngineSet.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEngineSet.Appearance.Options.UseFont = true;
            this.btnEngineSet.Location = new System.Drawing.Point(349, 401);
            this.btnEngineSet.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEngineSet.Name = "btnEngineSet";
            this.btnEngineSet.Size = new System.Drawing.Size(334, 32);
            this.btnEngineSet.TabIndex = 20;
            this.btnEngineSet.Text = "설정";
            this.btnEngineSet.Click += new System.EventHandler(this.btnEngineSet_Click);
            // 
            // btnEngineSetZero
            // 
            this.btnEngineSetZero.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEngineSetZero.Appearance.Options.UseFont = true;
            this.btnEngineSetZero.Location = new System.Drawing.Point(478, 357);
            this.btnEngineSetZero.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEngineSetZero.Name = "btnEngineSetZero";
            this.btnEngineSetZero.Size = new System.Drawing.Size(205, 30);
            this.btnEngineSetZero.TabIndex = 19;
            this.btnEngineSetZero.Text = "Set 0";
            this.btnEngineSetZero.Click += new System.EventHandler(this.btnEngineSetZero_Click);
            // 
            // teDynoTarget
            // 
            this.teDynoTarget.Location = new System.Drawing.Point(485, 104);
            this.teDynoTarget.Name = "teDynoTarget";
            this.teDynoTarget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDynoTarget.Properties.Appearance.Options.UseFont = true;
            this.teDynoTarget.Properties.ReadOnly = true;
            this.teDynoTarget.Size = new System.Drawing.Size(198, 32);
            this.teDynoTarget.TabIndex = 17;
            // 
            // btnReset
            // 
            this.btnReset.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Appearance.Options.UseFont = true;
            this.btnReset.Location = new System.Drawing.Point(225, 58);
            this.btnReset.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(108, 32);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnDynoMinus100
            // 
            this.btnDynoMinus100.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoMinus100.Appearance.Options.UseFont = true;
            this.btnDynoMinus100.Location = new System.Drawing.Point(349, 299);
            this.btnDynoMinus100.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoMinus100.Name = "btnDynoMinus100";
            this.btnDynoMinus100.Size = new System.Drawing.Size(334, 22);
            this.btnDynoMinus100.TabIndex = 15;
            this.btnDynoMinus100.Text = "-100";
            this.btnDynoMinus100.Click += new System.EventHandler(this.btnDynoMinus100_Click);
            // 
            // btnDynoPlus100
            // 
            this.btnDynoPlus100.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoPlus100.Appearance.Options.UseFont = true;
            this.btnDynoPlus100.Location = new System.Drawing.Point(12, 299);
            this.btnDynoPlus100.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoPlus100.Name = "btnDynoPlus100";
            this.btnDynoPlus100.Size = new System.Drawing.Size(333, 22);
            this.btnDynoPlus100.TabIndex = 14;
            this.btnDynoPlus100.Text = "+100";
            this.btnDynoPlus100.Click += new System.EventHandler(this.btnDynoPlus100_Click);
            // 
            // btnDynoMinus10
            // 
            this.btnDynoMinus10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoMinus10.Appearance.Options.UseFont = true;
            this.btnDynoMinus10.Location = new System.Drawing.Point(349, 273);
            this.btnDynoMinus10.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoMinus10.Name = "btnDynoMinus10";
            this.btnDynoMinus10.Size = new System.Drawing.Size(334, 22);
            this.btnDynoMinus10.TabIndex = 13;
            this.btnDynoMinus10.Text = "-10";
            this.btnDynoMinus10.Click += new System.EventHandler(this.btnDynoMinus10_Click);
            // 
            // btnDynoPlus10
            // 
            this.btnDynoPlus10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoPlus10.Appearance.Options.UseFont = true;
            this.btnDynoPlus10.Location = new System.Drawing.Point(12, 273);
            this.btnDynoPlus10.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoPlus10.Name = "btnDynoPlus10";
            this.btnDynoPlus10.Size = new System.Drawing.Size(333, 22);
            this.btnDynoPlus10.TabIndex = 12;
            this.btnDynoPlus10.Text = "+10";
            this.btnDynoPlus10.Click += new System.EventHandler(this.btnDynoPlus10_Click);
            // 
            // btnDynoMinus1
            // 
            this.btnDynoMinus1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoMinus1.Appearance.Options.UseFont = true;
            this.btnDynoMinus1.Location = new System.Drawing.Point(349, 247);
            this.btnDynoMinus1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoMinus1.Name = "btnDynoMinus1";
            this.btnDynoMinus1.Size = new System.Drawing.Size(334, 22);
            this.btnDynoMinus1.TabIndex = 11;
            this.btnDynoMinus1.Text = "-1";
            this.btnDynoMinus1.Click += new System.EventHandler(this.btnDynoMinus1_Click);
            // 
            // btnDynoPlus1
            // 
            this.btnDynoPlus1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoPlus1.Appearance.Options.UseFont = true;
            this.btnDynoPlus1.Location = new System.Drawing.Point(12, 247);
            this.btnDynoPlus1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoPlus1.Name = "btnDynoPlus1";
            this.btnDynoPlus1.Size = new System.Drawing.Size(333, 22);
            this.btnDynoPlus1.TabIndex = 10;
            this.btnDynoPlus1.Text = "+1";
            this.btnDynoPlus1.Click += new System.EventHandler(this.btnDynoPlus1_Click);
            // 
            // btnDynoSet
            // 
            this.btnDynoSet.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoSet.Appearance.Options.UseFont = true;
            this.btnDynoSet.Location = new System.Drawing.Point(349, 193);
            this.btnDynoSet.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoSet.Name = "btnDynoSet";
            this.btnDynoSet.Size = new System.Drawing.Size(334, 32);
            this.btnDynoSet.TabIndex = 9;
            this.btnDynoSet.Text = "설정";
            this.btnDynoSet.Click += new System.EventHandler(this.btnDynoSet_Click);
            // 
            // teDynoFeedback
            // 
            this.teDynoFeedback.Location = new System.Drawing.Point(148, 104);
            this.teDynoFeedback.Name = "teDynoFeedback";
            this.teDynoFeedback.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDynoFeedback.Properties.Appearance.Options.UseFont = true;
            this.teDynoFeedback.Properties.ReadOnly = true;
            this.teDynoFeedback.Size = new System.Drawing.Size(197, 32);
            this.teDynoFeedback.TabIndex = 6;
            // 
            // cbtnRemote
            // 
            this.cbtnRemote.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cbtnRemote.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnRemote.Appearance.Options.UseBackColor = true;
            this.cbtnRemote.Appearance.Options.UseFont = true;
            this.cbtnRemote.Location = new System.Drawing.Point(12, 58);
            this.cbtnRemote.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnRemote.Name = "cbtnRemote";
            this.cbtnRemote.Size = new System.Drawing.Size(101, 32);
            this.cbtnRemote.TabIndex = 5;
            this.cbtnRemote.Text = "Remote";
            this.cbtnRemote.Click += new System.EventHandler(this.cbtnRemote_Click);
            // 
            // cbDynoMode
            // 
            this.cbDynoMode.EditValue = "스피드";
            this.cbDynoMode.Location = new System.Drawing.Point(148, 12);
            this.cbDynoMode.Name = "cbDynoMode";
            this.cbDynoMode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDynoMode.Properties.Appearance.Options.UseFont = true;
            this.cbDynoMode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDynoMode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbDynoMode.Properties.AppearanceItemSelected.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDynoMode.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.cbDynoMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbDynoMode.Properties.Items.AddRange(new object[] {
            "스피드",
            "토크",
            "하중"});
            this.cbDynoMode.Size = new System.Drawing.Size(535, 32);
            this.cbDynoMode.TabIndex = 4;
            this.cbDynoMode.SelectedIndexChanged += new System.EventHandler(this.cbMode_SelectedIndexChanged);
            // 
            // teDynoSet
            // 
            this.teDynoSet.Location = new System.Drawing.Point(148, 193);
            this.teDynoSet.Name = "teDynoSet";
            this.teDynoSet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teDynoSet.Properties.Appearance.Options.UseFont = true;
            this.teDynoSet.Size = new System.Drawing.Size(197, 32);
            this.teDynoSet.TabIndex = 8;
            // 
            // btnDynoSetZero
            // 
            this.btnDynoSetZero.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDynoSetZero.Appearance.Options.UseFont = true;
            this.btnDynoSetZero.Location = new System.Drawing.Point(359, 150);
            this.btnDynoSetZero.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDynoSetZero.Name = "btnDynoSetZero";
            this.btnDynoSetZero.Size = new System.Drawing.Size(324, 29);
            this.btnDynoSetZero.TabIndex = 18;
            this.btnDynoSetZero.Text = "Set 0";
            this.btnDynoSetZero.Click += new System.EventHandler(this.btnDynoSetZero_Click);
            // 
            // teEngineSet
            // 
            this.teEngineSet.Location = new System.Drawing.Point(148, 401);
            this.teEngineSet.Name = "teEngineSet";
            this.teEngineSet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teEngineSet.Properties.Appearance.Options.UseFont = true;
            this.teEngineSet.Size = new System.Drawing.Size(197, 32);
            this.teEngineSet.TabIndex = 8;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.lbDynoFeedback,
            this.emptySpaceItem1,
            this.simpleLabelItem1,
            this.emptySpaceItem6,
            this.lbDynoSet,
            this.emptySpaceItem8,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem7,
            this.simpleLabelItem2,
            this.emptySpaceItem9,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.lbDynoTarget,
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem6,
            this.emptySpaceItem10,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(695, 493);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.cbDynoMode;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(675, 36);
            this.layoutControlItem1.Text = "동력계 모드";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(133, 25);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 36);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(675, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.cbtnRemote;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(88, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(105, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(536, 46);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 36);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 82);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(675, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lbDynoFeedback
            // 
            this.lbDynoFeedback.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDynoFeedback.AppearanceItemCaption.Options.UseFont = true;
            this.lbDynoFeedback.Control = this.teDynoFeedback;
            this.lbDynoFeedback.Location = new System.Drawing.Point(0, 92);
            this.lbDynoFeedback.Name = "lbDynoFeedback";
            this.lbDynoFeedback.Size = new System.Drawing.Size(337, 36);
            this.lbDynoFeedback.Text = "동력계 피드백값";
            this.lbDynoFeedback.TextSize = new System.Drawing.Size(133, 25);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 128);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(675, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 138);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(347, 33);
            this.simpleLabelItem1.Text = "동력계 컨트롤";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(133, 29);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 171);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(675, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lbDynoSet
            // 
            this.lbDynoSet.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDynoSet.AppearanceItemCaption.Options.UseFont = true;
            this.lbDynoSet.Control = this.teDynoSet;
            this.lbDynoSet.CustomizationFormText = "가감시간";
            this.lbDynoSet.Location = new System.Drawing.Point(0, 181);
            this.lbDynoSet.MinSize = new System.Drawing.Size(160, 36);
            this.lbDynoSet.Name = "lbDynoSet";
            this.lbDynoSet.Size = new System.Drawing.Size(337, 36);
            this.lbDynoSet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbDynoSet.Text = "설정값";
            this.lbDynoSet.TextSize = new System.Drawing.Size(133, 25);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 217);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(675, 18);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnDynoPlus1;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 235);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(337, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnDynoMinus1;
            this.layoutControlItem8.Location = new System.Drawing.Point(337, 235);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(338, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnDynoPlus10;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 261);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(337, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnDynoMinus10;
            this.layoutControlItem10.Location = new System.Drawing.Point(337, 261);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(338, 26);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnDynoPlus100;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 287);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(337, 26);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnDynoMinus100;
            this.layoutControlItem12.Location = new System.Drawing.Point(337, 287);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(338, 26);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 313);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(675, 32);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem2.CustomizationFormText = "컨트롤";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 345);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(466, 34);
            this.simpleLabelItem2.Text = "쓰로틀 컨트롤";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(133, 29);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 425);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(675, 48);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.teEngineSet;
            this.layoutControlItem16.CustomizationFormText = "가감시간";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 389);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(160, 36);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(337, 36);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "설정값";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(133, 25);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnEngineSet;
            this.layoutControlItem18.Location = new System.Drawing.Point(337, 389);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(338, 36);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // lbDynoTarget
            // 
            this.lbDynoTarget.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDynoTarget.AppearanceItemCaption.Options.UseFont = true;
            this.lbDynoTarget.Control = this.teDynoTarget;
            this.lbDynoTarget.Location = new System.Drawing.Point(337, 92);
            this.lbDynoTarget.Name = "lbDynoTarget";
            this.lbDynoTarget.Size = new System.Drawing.Size(338, 36);
            this.lbDynoTarget.Text = "동력계 타겟값";
            this.lbDynoTarget.TextSize = new System.Drawing.Size(133, 25);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnReset;
            this.layoutControlItem13.Location = new System.Drawing.Point(213, 46);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(46, 30);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(112, 36);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnDynoSetZero;
            this.layoutControlItem15.Location = new System.Drawing.Point(347, 138);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(328, 33);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnDynoSet;
            this.layoutControlItem6.Location = new System.Drawing.Point(337, 181);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(338, 36);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 379);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(675, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnEngineSetZero;
            this.layoutControlItem17.Location = new System.Drawing.Point(466, 345);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(209, 34);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.cbtnEngineStart;
            this.layoutControlItem19.Location = new System.Drawing.Point(105, 46);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(51, 30);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(108, 36);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnRampStop;
            this.layoutControlItem20.Location = new System.Drawing.Point(325, 46);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(89, 26);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(112, 36);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "Ramp Stop";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.seRamp;
            this.layoutControlItem3.Location = new System.Drawing.Point(546, 46);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(129, 36);
            this.layoutControlItem3.Text = "가감시간";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(64, 25);
            this.layoutControlItem3.TextToControlDistance = 10;
            // 
            // tmrRefresh
            // 
            this.tmrRefresh.Enabled = true;
            this.tmrRefresh.Interval = 500;
            this.tmrRefresh.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tmrDynoRamp
            // 
            this.tmrDynoRamp.Interval = 500;
            this.tmrDynoRamp.Tick += new System.EventHandler(this.tmrDynoRamp_Tick);
            // 
            // tmrEngineRamp
            // 
            this.tmrEngineRamp.Interval = 500;
            this.tmrEngineRamp.Tick += new System.EventHandler(this.tmrEngineRamp_Tick);
            // 
            // cbtnECUPower
            // 
            this.cbtnECUPower.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cbtnECUPower.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnECUPower.Appearance.Options.UseBackColor = true;
            this.cbtnECUPower.Appearance.Options.UseFont = true;
            this.cbtnECUPower.Location = new System.Drawing.Point(449, 58);
            this.cbtnECUPower.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnECUPower.Name = "cbtnECUPower";
            this.cbtnECUPower.Size = new System.Drawing.Size(95, 32);
            this.cbtnECUPower.TabIndex = 24;
            this.cbtnECUPower.Text = "ECU Power";
            this.cbtnECUPower.Click += new System.EventHandler(this.cbtnECUPower_Click);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cbtnECUPower;
            this.layoutControlItem5.Location = new System.Drawing.Point(437, 46);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(99, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(99, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // ManualControlPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 497);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManualControlPad";
            this.Text = "Manual Control Pad";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManualControlPad_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seRamp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoTarget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoFeedback.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDynoMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDynoSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEngineSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoFeedback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDynoTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit teDynoFeedback;
        private DevExpress.XtraEditors.CheckButton cbtnRemote;
        private DevExpress.XtraEditors.ComboBoxEdit cbDynoMode;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem lbDynoFeedback;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SimpleButton btnDynoSet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit teDynoSet;
        private DevExpress.XtraLayout.LayoutControlItem lbDynoSet;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraEditors.SimpleButton btnDynoMinus100;
        private DevExpress.XtraEditors.SimpleButton btnDynoPlus100;
        private DevExpress.XtraEditors.SimpleButton btnDynoMinus10;
        private DevExpress.XtraEditors.SimpleButton btnDynoPlus10;
        private DevExpress.XtraEditors.SimpleButton btnDynoMinus1;
        private DevExpress.XtraEditors.SimpleButton btnDynoPlus1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Timer tmrRefresh;
        private System.Windows.Forms.Timer tmrDynoRamp;
        private DevExpress.XtraEditors.TextEdit teDynoTarget;
        private DevExpress.XtraLayout.LayoutControlItem lbDynoTarget;
        private DevExpress.XtraEditors.SimpleButton btnDynoSetZero;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraEditors.TextEdit teEngineSet;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.SimpleButton btnEngineSet;
        private DevExpress.XtraEditors.SimpleButton btnEngineSetZero;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.CheckButton cbtnEngineStart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private System.Windows.Forms.Timer tmrEngineRamp;
        private DevExpress.XtraEditors.SimpleButton btnRampStop;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.SpinEdit seRamp;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckButton cbtnECUPower;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}