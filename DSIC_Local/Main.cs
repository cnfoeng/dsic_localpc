﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DSIC_Local.Panels;
using DSIC_Local.BarcodeScanner;
using DSIC_Local.Mitsubishi;
using DSIC_Local.Step;
using DSIC_Local.Log;
using System.Threading;
using DSIC_Local.Control;
using DSIC_Local.CAN;
using DSIC_Local.SmokeMeter;
using DSIC_Local.Settings;
using DSIC_Local.DataLogging;
using DSIC_Local.Alarm;
using DSIC_Local.Database;

namespace DSIC_Local
{
    public partial class Main : DevExpress.XtraEditors.XtraForm
    {
        public static Project project = new Project();
        public BasicScreen basicScreen;
        public MainScreen mainScreen;
        public MonitoringScreen monitoringScreen;
        public OutsideMonitor outsideMonitor;
        public ResultScreen resultScreen = new ResultScreen();

        public static ConstantLogging constantLogging;

        Zebra zebra = new Zebra();

        public static MelsecCom CNFODAQCom = new MelsecCom();
        public static MelsecCom RGVCom = new MelsecCom();
        public static CANCom canCom = new CANCom();

        public static ISOTP_UDS isotp_uds = new ISOTP_UDS();
        public static KWP2000 kline = new KWP2000();
        public static AFT2000_A smokeMeter = new AFT2000_A();
        public static List<SystemLog> logs = new List<SystemLog>();
        public static StepMode stepMode = new StepMode();
        public static StepmodeOutput stepmodeOutput = new StepmodeOutput();
        public static StatusForDAQ statusForDAQ = new StatusForDAQ();
        public static StatusForRGV statusForRGV = new StatusForRGV();
        public static ResultData resultData = new ResultData();
        public static double K;
        public static double klineoilpressure = 0;//임시
        public static double klinecoolanttemperature = 0;//임시
        string speedVarName = Properties.Settings.Default.BME_Speed_FeedBack;
        VariableInfo speedVar;

        public static string localMapFilePath = string.Empty;
        public static string localScheduleFilePath = string.Empty;
        public static string localEngineMeasuringFilePath = string.Empty;
        public static string localEngineFaultListFilePath = string.Empty;

        Thread thAlarm;

        public static MSSQL database = new MSSQL();
        public static bool bReadBarcodeEvent = false;
        public static string barcode = string.Empty;

        public Main()
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DSIC_Local.Loading), true, true);

            InitializeComponent();

            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            ContextMenu cm = new ContextMenu();

            cm.MenuItems.Add("모니터 1");
            cm.MenuItems[0].Click += Outside_Monitor1_Click;
            cm.MenuItems.Add("모니터 2");
            cm.MenuItems[1].Click += Outside_Monitor2_Click;

            cbOutsideMonitor.ContextMenu = cm;

            Main.AddLog("프로그램 시작", LogType.Information);

            project = project.LoadProject(Application.StartupPath + "\\Data.dat");


            if (project == null)
            {
                project = new Project();

                project.MakeDefaultDV11Engines();
                project.MakeDefaultDX22Engines();

                project.MakeDefaultMonitorItem();

                project.currentEngine = project.engines[0]; //DX22 = [11]
            }

            project.MakeDefaultDV11Engines();
            project.MakeDefaultDX22Engines();

            mainScreen = new MainScreen(project.list_MonitorItems[0]);
            monitoringScreen = new MonitoringScreen(project.list_MonitorItems[1]);
            outsideMonitor = new OutsideMonitor(project.list_MonitorItems[2]);

            localEngineMeasuringFilePath = Application.StartupPath +"\\EngineMeasuring.csv";
            localEngineFaultListFilePath = Application.StartupPath + "\\FaultList.xlsx";

            Connect();

            basicScreen = new BasicScreen();
            panelContent.Controls.Add(basicScreen);
            panelContent.Controls.Add(mainScreen);
            panelContent.Controls.Add(monitoringScreen);
            panelContent.Controls.Add(resultScreen);
            mainScreen.Hide();
            monitoringScreen.Hide();
            resultScreen.Hide();
            speedVar = Main.project.sysVarInfo.FindVariable(speedVarName);
        }
        private void Connect()
        {
            if (CNFODAQCom.Import_CNFODAQ_Variables(Application.StartupPath + "\\Melsec_ChannelDefine.csv"))
            {
                CNFODAQCom.Start_MelsecCom(1);
            }

            //if (RGVCom.Import_RGV_Variables(Application.StartupPath + "\\RGV_ChannelDefine.csv"))
            //{
            //    RGVCom.Start_MelsecCom(2);
            //}

            //project.currentEngine = project.engines[0];// 2번 - DV11 euro3, 11번 - DX22 , 4번 - Euro5 , 0번 - Euro4

            Import_CalculationVariables(Application.StartupPath + "\\Calculation_ChannelDefine.csv");
            System.Threading.Thread.Sleep(500);
            if (project.currentEngine.Engine == Engine.DX22)
            {
                if (canCom.Import_Variables(localEngineMeasuringFilePath))
                {
                    project.currentEngine.MakeFaultList(localEngineFaultListFilePath);

                    canCom.StartCANCom();
                    canCom.StartGetData();
                }
            }
            else if (project.currentEngine.Engine == Engine.DV11)
            {
                if (kline.Import_Variables(localEngineMeasuringFilePath))
                {
                    project.currentEngine.MakeFaultList(localEngineFaultListFilePath);
                    kline.OBDCommunicationStart();
                    Thread.Sleep(1000);
                    kline.StartGetData();
                }
                else
                {
                    AddLog(localEngineMeasuringFilePath + "Import 실패", Log.LogType.Error);
                }
            }
            //Todo K-line도 Comport라 겹침.

            smokeMeter.Open();
            System.Threading.Thread.Sleep(1000);


            project.sysVarInfo.RenewAllvariables();

            RemappingByVarName();
            MonitorAssignVarRemapping();

            thAlarm = new Thread(new ThreadStart(AlarmClass.Start_Alarm));
            thAlarm.Start();

            //if (!database.Connect("localhost", "1433", "phj", "cnfoeng!", "DHIM"))

            if(project.dBConnectionData== null)
            {
                project.dBConnectionData = new DBConnectionData();
            }

            if (!database.Connect(project.dBConnectionData.IP, project.dBConnectionData.Port, project.dBConnectionData.UserID, project.dBConnectionData.Pwd, project.dBConnectionData.TableName))
            {
                //XtraMessageBox.Show("<size=14>데이터베이스 서버 연결에 실패했습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                stateIndicatorComponent2.StateIndex = 1;
                AddLog("DB 연결 실패", Log.LogType.Error);
            }
            else
            {
                stateIndicatorComponent2.StateIndex = 3;
                AddLog("DB 연결 성공", Log.LogType.OK);
            }
        }
        private void Outside_Monitor1_Click(object sender, EventArgs e)
        {
            outsideMonitor.screenNumber = 0;
            outsideMonitor.Show();
        }
        private void Outside_Monitor2_Click(object sender, EventArgs e)
        {
            outsideMonitor.screenNumber = 1;
            outsideMonitor.Show();
        }
        void ReConnecting()
        {
            CNFODAQCom.Stop_MelsecCom();
            canCom.StopCom();
            canCom.StopGetData();
            kline.OBDCommunicationStop();
            kline.StopGetData();
            smokeMeter.Close();
            Thread.Sleep(1000);

            Connect();

            //CNFODAQCom.Start_MelsecCom(1);
            ////RGVCom.Start_MelsecCom(2);
            //if (project.currentEngine.Engine == Engine.DX22)
            //{
            //    canCom.StartCANCom();
            //    canCom.StartGetData();
            //}
            //else if(project.currentEngine.Engine == Engine.DV11)
            //{
            //    kline.OBDCommunicationStart();
            //    kline.StartGetData();
            //}
            //smokeMeter.Open();
        }
        void RemappingByVarName()
        {

        }
        bool Import_CalculationVariables(string path)
        {
            VariableInfo varItem;
            int Index = 0;

            string[] Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            if (Main.project.sysVarInfo.Calculation_Variables != null)
            {
                Main.project.sysVarInfo.Calculation_Variables.Clear();
            }
            else
            {
                Main.project.sysVarInfo.Calculation_Variables = new List<VariableInfo>();
            }

            if (varlist != null)
            {
                try
                {
                    foreach (string Calculation_Data in varlist)
                    {
                        string[] arrCalculation_Data = Calculation_Data.Split(',');
                        string variableName = arrCalculation_Data[1];
                        string norminalName = arrCalculation_Data[2];
                        //string type = arrMelsec_Data[3];
                        string unit = arrCalculation_Data[3];
                        bool use_Alarm = (arrCalculation_Data[4] == "TRUE") ? true : false;
                        int alarm_Low = int.Parse(arrCalculation_Data[5]);
                        int alarm_High = int.Parse(arrCalculation_Data[6]);
                        bool use_Warning = (arrCalculation_Data[7] == "TRUE") ? true : false;
                        int warning_Low = int.Parse(arrCalculation_Data[8]);
                        int warning_High = int.Parse(arrCalculation_Data[9]);
                        bool use_Averaging = (arrCalculation_Data[10] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrCalculation_Data[11]);
                        int round = int.Parse(arrCalculation_Data[12]);
                        string description = arrCalculation_Data[13];
                        string formula = arrCalculation_Data[14];

                        varItem = new VariableInfo(variableName, norminalName);

                        varItem.CalType = CalibrationType.Formula;
                        varItem.Unit = unit;
                        varItem.AlarmUse = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.WarningUse = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.NorminalDescription = description;
                        varItem.Formula = formula;
                        varItem.Index = Index;

                        Main.project.sysVarInfo.Calculation_Variables.Add(varItem);
                        Index++;
                    }
                }
                catch (Exception)
                {
                    AddLog("Calculation File Import 에러", LogType.Error);
                    return false;
                }
            }

            return true;


        }

        public void MonitorAssignVarRemapping()
        {
            for (int i = 0; i < project.list_MonitorItems.Count; i++)
            {
                for (int j = 0; j < project.list_MonitorItems[i].Count; j++)
                {
                    if (project.list_MonitorItems[i][j].si.assignedVar == null)
                    {
                        continue;
                    }
                    for (int k = 0; k < project.sysVarInfo.CNFODAQ_Variables.Count; k++)
                    {
                        if (project.list_MonitorItems[i][j].si.assignedVar.VariableName == project.sysVarInfo.CNFODAQ_Variables[k].VariableName)
                        {
                            project.list_MonitorItems[i][j].si.assignedVar = project.sysVarInfo.CNFODAQ_Variables[k];
                        }
                    }

                    for (int k = 0; k < project.sysVarInfo.Calculation_Variables.Count; k++)
                    {
                        if (project.list_MonitorItems[i][j].si.assignedVar.VariableName == project.sysVarInfo.Calculation_Variables[k].VariableName)
                        {
                            project.list_MonitorItems[i][j].si.assignedVar = project.sysVarInfo.Calculation_Variables[k];
                        }
                    }

                    for (int k = 0; k < project.sysVarInfo.EngineCom_Variables.Count; k++)
                    {
                        if (project.list_MonitorItems[i][j].si.assignedVar.VariableName == project.sysVarInfo.EngineCom_Variables[k].VariableName)
                        {
                            project.list_MonitorItems[i][j].si.assignedVar = project.sysVarInfo.EngineCom_Variables[k];
                        }
                    }
                }
            }
        }

        private void btnECUDownload_Click(object sender, EventArgs e)
        {
            ECUFlashing form = new ECUFlashing(FlashType.None);
            form.Show();
        }

        private void cbtnBasicScreen_Click(object sender, EventArgs e)
        {
            if (cbtnBasicScreen.Checked)
            {
                cbtnBasicScreen.Checked = false;
                return;
            }

            cbtnMainScreen.Checked = false;
            cbtnMonitoringScreen.Checked = false;
            cbtnResultScreen.Checked = false;

            //panelContent.Controls.Clear();
            //panelContent.Controls.Add(basicScreen);

            basicScreen.Show();
            mainScreen.Hide();
            monitoringScreen.Hide();
            resultScreen.Hide();

            tbScreen.Text = "기본 화면";
        }

        private void cbtnMainScreen_Click(object sender, EventArgs e)
        {
            if (cbtnMainScreen.Checked)
            {
                cbtnMainScreen.Checked = false;
                return;
            }

            cbtnBasicScreen.Checked = false;
            cbtnMonitoringScreen.Checked = false;
            cbtnResultScreen.Checked = false;

            //panelContent.Controls.Clear();
            //panelContent.Controls.Add(mainScreen);

            basicScreen.Hide();
            mainScreen.Show();
            monitoringScreen.Hide();
            resultScreen.Hide();

            tbScreen.Text = "메인 화면";
        }

        private void cbtnMonitoringScreen_Click(object sender, EventArgs e)
        {
            if (cbtnMonitoringScreen.Checked)
            {
                cbtnMonitoringScreen.Checked = false;
                return;
            }

            cbtnBasicScreen.Checked = false;
            cbtnMainScreen.Checked = false;
            cbtnResultScreen.Checked = false;

            basicScreen.Hide();
            mainScreen.Hide();
            monitoringScreen.Show();
            resultScreen.Hide();

            panelMonitoring.BringToFront();
            tbScreen.Text = "모니터링 화면";

        }

        private void cbtnResultScreen_Click(object sender, EventArgs e)
        {
            if (cbtnResultScreen.Checked)
            {
                cbtnResultScreen.Checked = false;
                return;
            }

            cbtnBasicScreen.Checked = false;
            cbtnMainScreen.Checked = false;
            cbtnMonitoringScreen.Checked = false;

            basicScreen.Hide();
            mainScreen.Hide();
            monitoringScreen.Hide();
            resultScreen.Show();

            resultScreen.MakeGridMeasurementTable();
            resultScreen.MakeGridLeft();
            resultScreen.MakeGridRight();

            resultScreen.PassOrNot();

            tbScreen.Text = "결과 화면";
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Thread.Sleep(1000);

            project.SaveProject(Application.StartupPath + "\\data.dat");

            mainScreen.monitoringForm.SaveXMLLayout();
            monitoringScreen.monitoringForm.SaveXMLLayout();
            outsideMonitor.monitoringForm.SaveXMLLayout();

            CNFODAQCom.Stop_MelsecCom();
            RGVCom.Stop_MelsecCom();
            canCom.StopCom();
            kline.OBDCommunicationStop();

            AlarmClass.Stop_Alarm();
            thAlarm?.Abort();
        }

        public static ControlStepMode controlStepModeForm = null;
        
        public void RefreshDocuments()
        {
            mainScreen.RefreshDocument();
        }

        public static void AddLog(string message, LogType logType)
        {
            logs.Add(new SystemLog(message, logType, DateTime.Now));
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings_Main form = new Settings_Main();
            form.Show();
        }
        public static ManualControlPad manualControlPadForm = null;
        private void windowsUIButtonPanel1_Click(object sender, EventArgs e)
        {
            if (!CNFODAQCom.bConnected)
            {
                XtraMessageBox.Show("<size=14>PLC 연결을 확인하세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (manualControlPadForm == null)
            {
                manualControlPadForm = new ManualControlPad();
                manualControlPadForm.Show();
            }
            else
            {
                manualControlPadForm.BringToFront();
            }

            textEdit2.Text = "수동 운전";
        }

        private void windowsUIButtonPanel2_Click(object sender, EventArgs e)
        {
            if (controlStepModeForm == null)
            {
                controlStepModeForm = new ControlStepMode(this);
                controlStepModeForm.Show();
            }
            else
            {
                controlStepModeForm.BringToFront();
            }

            textEdit2.Text = "스텝 운전";
        }

        private void windowsUIButtonPanel3_Click(object sender, EventArgs e)
        {

        }

        private void btnStartStepMode_Click(object sender, EventArgs e)
        {
            basicScreen.Idle();
            Thread.Sleep(500);

            Main.statusForDAQ.StOnDemand = true;
            Main.statusForDAQ.SetPLCStatus();
            System.Threading.Thread.Sleep(500);
            Main.statusForDAQ.StOnDemand = false;
            Main.statusForDAQ.SetPLCStatus();

            if (controlStepModeForm == null)
            {
                controlStepModeForm = new ControlStepMode(this);
                controlStepModeForm.Show();
            }
            else
            {
                controlStepModeForm.BringToFront();
            }
        }
        TimeSpan elapsedTime = new TimeSpan();
        TimeSpan addTime;

        private void timer1_Tick(object sender, EventArgs e)
        {
            cbOutsideMonitor.Checked = outsideMonitor.visibility;

            CalculateK();

            if (speedVar.RealValue > 300)
            {
                addTime = new TimeSpan(timer1.Interval * 10000);
                statusForRGV.Testing = true;
                textEdit1.Text = "엔진 시작";
            }
            else
            {
                if (elapsedTime.Ticks != 0)
                {
                    elapsedTime = new TimeSpan();
                }

                statusForRGV.Testing = false;

                textEdit1.Text = "대기중";
            }
            elapsedTime = elapsedTime.Add(addTime);

            if (CNFODAQCom.bConnected && RGVCom.bConnected && canCom.isConnected && isotp_uds.isConnected)
            {
                statusForRGV.TestReady = true;
            }
            else
            {
                statusForRGV.TestReady = false;
            }

            if (statusForRGV.Watchdog)
            {
                statusForRGV.Watchdog = false;
            }
            else
            {
                statusForRGV.Watchdog = true;
            }

            teElapsedTime.Text = elapsedTime.ToString("hh\\:mm\\:ss");

            //if (project.sysVarInfo.FindVariable("RFID READ REQ").RealValue == 1)
            //{
            //    statusForRGV.RFIDReadOK = true;

            //    //int palletNO = RGVCom.GetMelsecData("R1902");
            //    //int dynoNO = RGVCom.GetMelsecData("R1903");
            //}


            if (RGVCom.bConnected)
            {
                int status = RGVCom.GetMelsecData_RGV("R1900");
                int palletNO = RGVCom.GetMelsecData_RGV("R1902");
                int dynoNO = RGVCom.GetMelsecData_RGV("R1903");
                string str = RGVCom.ID_Catch_Block("R1904");
                double d = project.sysVarInfo.FindVariable("DYNAMO NO").RealValue;
                RGVCom.SetMelsecData("R1800", statusForRGV.MakeIntFromBitarray());
            }

            if (project.currentEngine.Engine == Engine.DV11)
            {
                if (kline.bConnected)
                {
                    stateIndicatorComponent1.StateIndex = 3;
                }
                else
                {
                    stateIndicatorComponent1.StateIndex = 1;
                }
            }
            else if (project.currentEngine.Engine == Engine.DX22)
            {
                if (isotp_uds.isConnected)
                {
                    stateIndicatorComponent1.StateIndex = 3;
                }
                else
                {
                    stateIndicatorComponent1.StateIndex = 1;
                }
            }

            if (bReadBarcodeEvent)
            {
                textEdit2.Text = "자동 운전";
             

                bReadBarcodeEvent = false;
                string suffix = basicScreen.BarcodeRead(barcode);

                if (suffix == null)
                {

                    XtraMessageBox.Show("<size=14>바코드 검색 실패. DB 연결을 확인하거나 작업 지시서, 엔진 정보가 정확히 서버에 입력돼있는지 확인해 주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (!EngineFileLocalization())
                {
                    XtraMessageBox.Show("<size=14>C드라이브 공유폴더에 자동 운전에 필요한 파일(Map파일, Schedule파일, Measuring파일 또는 FaultList 파일)이 존재하지 않습니다. 파일 경로가 Local로 복구됩니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                FindEngineType(suffix);
                ReConnecting();
                Thread.Sleep(1000);

                //cbtnMainScreen.PerformClick();

                ECUFlashing form = new ECUFlashing(FlashType.Dyno);
                form.ShowDialog();
                
                cbtnMainScreen.PerformClick();
                
            }


        }

        double P_SAT_DRY;
        double P_VAPOR;
        double T_DRY;
        double RH;
        double P_ATM;
        double FA;
        double BOOST_RATIO;
        double VDISP;
        double SPEED;
        double Q_CYC;
        double CYCLES;
        double FUEL_GS;
        double QR;
        double FM;

        bool EngineFileLocalization()
        {
            bool existFiles = true;
            string map = "C:\\SharedFolder\\MapFile\\" + Main.localMapFilePath;
            string schedule = "C:\\SharedFolder\\ScheduleFile\\" + Main.localScheduleFilePath;
            string measuring = "C:\\SharedFolder\\MeasuringFile\\" + Main.localEngineMeasuringFilePath;
            string faultList = "C:\\SharedFolder\\FaultListFile\\" + Main.localEngineFaultListFilePath;

            if (!File.Exists(map))
            {
                existFiles = false;
            }
            if (!File.Exists(schedule))
            {
                existFiles = false;
            }
            if (!File.Exists(measuring))
            {
                existFiles = false;
            }
            if (!File.Exists(faultList))
            {
                existFiles = false;
            }

            if (existFiles)
            {
                Main.localMapFilePath = map;
                Main.localScheduleFilePath = schedule;
                Main.localEngineMeasuringFilePath = measuring;
                Main.localEngineFaultListFilePath = faultList;
            }

            return existFiles;

        }
        void CalculateK()
        {
            FUEL_GS = (project.sysVarInfo.FindVariable("연비").RealValue * 1000) / 3600;
            CYCLES = 4;
            VDISP = 7.64;
            SPEED = project.sysVarInfo.FindVariable(Properties.Settings.Default.BME_Speed_FeedBack).RealValue;
            T_DRY = project.sysVarInfo.FindVariable("건구온도").RealValue;
            double T_ATM = T_DRY;
            RH = project.sysVarInfo.FindVariable("습도(RH)").RealValue;
            P_ATM = project.sysVarInfo.FindVariable("대기압").RealValue * 0.133322;
            P_SAT_DRY = project.sysVarInfo.FindVariable("포화수증기압").RealValue;
            P_VAPOR = RH * P_SAT_DRY / 100;
            FA = 1;
            double RT = ((project.sysVarInfo.FindVariable("인터쿨러압력A").RealValue * 100) + P_ATM) / P_ATM;

            if (P_ATM > P_VAPOR)
            {
                BOOST_RATIO = RT;

                FA = Math.Pow(99 / (P_ATM - P_VAPOR), 0.7) * Math.Pow((T_DRY + 273.15) / 298.13, 1.5);
            }

            if (BOOST_RATIO > 0)
            {
                Q_CYC = ((30000 * CYCLES) * FUEL_GS) / (SPEED * VDISP);
                QR = Q_CYC / BOOST_RATIO;


                if (QR < 40)
                {
                    FM = 0.3;
                }
                else if (QR < 65)
                {
                    FM = 0.036 * QR - 1.14;
                }
                else
                {
                    FM = 1.2;
                }
            }
            else
            {
                FM = 1;
            }

            if (P_ATM > 100)
            {
                K = Math.Pow(FA, FM);
            }
            else
            {
                K = 1;
            }

        }

        private void btnReConnect_Click(object sender, EventArgs e)
        {
            ReConnecting();
        }

        private void toggleSwitch1_Toggled(object sender, EventArgs e)
        {
            if (toggleSwitch1.IsOn)
            {
                constantLogging = new ConstantLogging(project.logSetting.DataLoggingConstantPath, project.logSetting.DataPrefix, project.logSetting.LoggingInterval, project.logSetting.MaxRowCount);

                constantLogging.StartLogging();
            }
            else
            {
                constantLogging.StopLogging();
            }
        }

        private void cbOutsideMonitor_Click(object sender, EventArgs e)
        {
            outsideMonitor.screenNumber = 1;
            outsideMonitor.Show();
        }
        public static AverageMeasuring averagingMeasuringForm = null;
        private void btnAveragingMeasuring_Click(object sender, EventArgs e)
        {
            if (averagingMeasuringForm == null)
            {
                averagingMeasuringForm = new AverageMeasuring();
                averagingMeasuringForm.Show();
            }
            else
            {
                averagingMeasuringForm.BringToFront();
            }
        }

        private void groupControl1_SizeChanged(object sender, EventArgs e)
        {
            windowsUIButtonPanel1.Width = windowsUIButtonPanel2.Width = windowsUIButtonPanel3.Width = groupControl1.Width / 3;
        }

        private void groupControl2_SizeChanged(object sender, EventArgs e)
        {
            btnStartStepMode.Width = btnStopStepMode.Width = groupControl2.Width / 2;
        }

        void FindEngineType(string suffix)
        {
            string[] dv11Euro3 = new string[3] { "DV11-EUJBI", "DV11-EUJEB", "DV11-EUJED" };
            string[] dv11Euro4 = new string[1] { "DV11-EQJTA" };
            string[] dv11Euro5 = new string[7] { "DV11-BEC22", "DV11-BEC31", "DV11-BEC32", "DV11-BEC33", "DV11-BEC35", "DV11-BEC57", "DV11-BEC59" };

            if (suffix.Contains("DX22"))
            {
                project.currentEngine = Main.project.engines[3];
            }
            else if (dv11Euro3.Contains(suffix))
            {
                project.currentEngine = Main.project.engines[0];
            }
            else if (dv11Euro4.Contains(suffix))
            {
                project.currentEngine = Main.project.engines[1];
            }
            else if (dv11Euro5.Contains(suffix))
            {
                project.currentEngine = Main.project.engines[2];
            }

        }
        
    }
}