﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DSIC_Local.Mitsubishi
{
    public class StatusForDAQ
    {
        bool remoteDemand = false;
        bool resetDemand = false;
        bool dynoOnDemand = false;
        bool modeDynoSpeed = false;
        bool modeDynoTorque = false;
        bool igOnDemand = false;
        bool stOnDemand = false;
        bool ecuPowerOnDemand = false;
        bool pcWatchDog = false;

        public bool RemoteDemand { get => remoteDemand; set => remoteDemand = value; }
        public bool ResetDemand { get => resetDemand; set => resetDemand = value; }
        public bool DynoOnDemand { get => dynoOnDemand; set => dynoOnDemand = value; }
        public bool ModeDynoSpeed { get => modeDynoSpeed; set => modeDynoSpeed = value; }
        public bool ModeDynoTorque { get => modeDynoTorque; set => modeDynoTorque = value; }
        public bool IgOnDemand { get => igOnDemand; set => igOnDemand = value; }
        public bool StOnDemand { get => stOnDemand; set => stOnDemand = value; }
        public bool EcuPowerOnDemand { get => ecuPowerOnDemand; set => ecuPowerOnDemand = value; }
        public bool PcWatchDog { get => pcWatchDog; set => pcWatchDog = value; }

        public int MakeIntFromBitarray()
        {
            BitArray ba = new BitArray(16);

            ba[0] = RemoteDemand;
            ba[1] = ResetDemand;
            ba[2] = DynoOnDemand;
            ba[3] = ModeDynoSpeed;
            ba[4] = ModeDynoTorque;
            ba[5] = IgOnDemand;
            ba[6] = StOnDemand;
            ba[7] = EcuPowerOnDemand;
            ba[8] = PcWatchDog;

            int[] intArray = new int[1];
            ba.CopyTo(intArray, 0);
            return intArray[0];
        }

        public void SetPLCStatus()
        {
            Main.CNFODAQCom.SetMelsecData("D6100", MakeIntFromBitarray());
        }
    }

    public class StatusForRGV
    {
        bool testComplete = false;
        bool testing = false;
        bool testReady = false;
        bool testOK = false;
        bool testNG = false;
        bool watchdog = false;
        bool _RFIDReadOK = false;

        public bool TestComplete { get => testComplete; set => testComplete = value; }
        public bool Testing { get => testing; set => testing = value; }
        public bool TestReady { get => testReady; set => testReady = value; }
        public bool TestOK { get => testOK; set => testOK = value; }
        public bool TestNG { get => testNG; set => testNG = value; }
        public bool Watchdog { get => watchdog; set => watchdog = value; }
        public bool RFIDReadOK { get => _RFIDReadOK; set => _RFIDReadOK = value; }

        public int MakeIntFromBitarray()
        {
            BitArray ba = new BitArray(16);

            ba[0] = TestComplete;
            ba[1] = Testing;
            ba[2] = TestReady;
            ba[3] = TestOK;
            ba[4] = TestNG;
            ba[5] = Watchdog;
            ba[6] = RFIDReadOK;

            int[] intArray = new int[1];
            ba.CopyTo(intArray, 0);
            return intArray[0];
        }
    }
}
