﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
using System.Timers;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace DSIC_Local.Mitsubishi
{
    public class MelsecCom
    {
        // PLC연결을 위한 정의
        ActUtlTypeLib.ActUtlType ActUtilType;
        public double testvalue;
        public bool bConnected = false;
        private static System.Timers.Timer aTimer1;

        public static string sFTime;

        private int mATTimer = 500;

        private int logicalStationNumber = -1;

        Thread thGetData;
        DeviceGroups deviceGroups = new DeviceGroups();
        int temp1;
        string str2 = "";
        int[] iData;
        char[] char_arr;
        int iData_length;

        public MelsecCom()
        {

        }

        public bool Import_CNFODAQ_Variables(string path)
        {
            VariableInfo varItem;
            int Index = 0;

            string[] Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            Main.project.sysVarInfo.CNFODAQ_Variables?.Clear();

            if (varlist != null)
            {
                try
                {
                    foreach (string Melsec_Data in varlist)
                    {

                        string[] arrMelsec_Data = Melsec_Data.Split(',');


                        if (arrMelsec_Data[1] == "연비")
                        {

                        }

                        string variableName = arrMelsec_Data[1];
                        string norminalName = arrMelsec_Data[2];
                        string type = arrMelsec_Data[3];

                        int bitIndex = -1;
                        if (arrMelsec_Data[4] != "")
                        {
                            bitIndex = int.Parse(arrMelsec_Data[4]);
                        }

                        string unit = arrMelsec_Data[5];
                        double min = double.Parse(arrMelsec_Data[6]);
                        double max = double.Parse(arrMelsec_Data[7]);
                        double ioMin = double.Parse(arrMelsec_Data[8]);
                        double ioMax = double.Parse(arrMelsec_Data[9]);
                        bool use_Alarm = (arrMelsec_Data[10] == "TRUE") ? true : false;
                        double alarm_Low = double.Parse(arrMelsec_Data[11]);
                        double alarm_High = double.Parse(arrMelsec_Data[12]);
                        bool use_Warning = (arrMelsec_Data[13] == "TRUE") ? true : false;
                        double warning_Low = double.Parse(arrMelsec_Data[14]);
                        double warning_High = double.Parse(arrMelsec_Data[15]);
                        bool use_Averaging = (arrMelsec_Data[16] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrMelsec_Data[17]);
                        int round = int.Parse(arrMelsec_Data[18]);
                        string description = arrMelsec_Data[19];


                        varItem = new VariableInfo(variableName, norminalName);
                        if (type == "BIT")
                        {
                            varItem.CalType = CalibrationType.BitInValue;
                            varItem.BitIndex = bitIndex;

                        }
                        else if (type == "WORD")
                        {
                            varItem.CalType = CalibrationType.CalByTable;
                        }
                        else
                        {
                            varItem.CalType = CalibrationType.None;
                        }

                        varItem.Unit = unit;
                        varItem.CalibrationTable.Add(new ValuePair(ioMin, min));
                        varItem.CalibrationTable.Add(new ValuePair(ioMax, max));
                        varItem.AlarmUse = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.WarningUse = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.NorminalDescription = description;
                        varItem.Index = Index;

                        Main.project.sysVarInfo.CNFODAQ_Variables.Add(varItem);
                        Index++;
                    }
                }
                catch (Exception)
                {
                    Debug.Print("CNFO DAQ File Import 에러");
                    return false;
                }
            }

            MakeDeviceGroup();

            return true;

        }
        public bool Import_RGV_Variables(string path)
        {
            VariableInfo varItem;
            int Index = 0;

            string[] Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            Main.project.sysVarInfo.RGV_Variables.Clear();

            if (varlist != null)
            {
                try
                {
                    foreach (string Melsec_Data in varlist)
                    {
                        string[] arrMelsec_Data = Melsec_Data.Split(',');
                        string variableName = arrMelsec_Data[1];
                        string norminalName = arrMelsec_Data[2];
                        string type = arrMelsec_Data[3];
                        int byteNum = 0;
                        int bitIndex = -1;
                        if (arrMelsec_Data[4] != "" && arrMelsec_Data[5] != "")
                        {
                            byteNum = int.Parse(arrMelsec_Data[4]);
                            bitIndex = int.Parse(arrMelsec_Data[5]);
                        }
                        string address = arrMelsec_Data[6];

                        varItem = new VariableInfo(variableName, norminalName);
                        if (type == "BIT")
                        {
                            varItem.CalType = CalibrationType.BitInValue;
                            varItem.ByteNumber = byteNum;
                            varItem.BitIndex = bitIndex;

                        }
                        else if (type == "WORD")
                        {
                            varItem.CalType = CalibrationType.None;
                        }
                        else
                        {
                            varItem.CalType = CalibrationType.String;
                        }

                        varItem.NorminalDescription = address;
                        varItem.Round = 2;
                        varItem.Averaging = false;
                        varItem.Index = Index;

                        Main.project.sysVarInfo.RGV_Variables.Add(varItem);
                        Index++;
                    }
                }
                catch (Exception)
                {
                    Debug.Print("RGV File Import 에러");
                    return false;
                }
            }

            return true;

        }
        public bool Export_CNFODAQ_Variables(string path)
        {
            string Header = string.Empty;
            string Value = string.Empty;

            Header = "Index,VARIABLENAME,NORMINALNAME,UNIT,MIN,MAX,IO MIN,IO MAX,USE_ALARM,ALARM LOW,ALARM HIGH,USE_WARNING,WARNING LOW,WARNING HIGH,USE_AVERAGE,AVERAGECOUNT,ROUND,DESCRIPTION";
            try
            {
                File.WriteAllText(path, Header);

                foreach (VariableInfo var in Main.project.sysVarInfo.CNFODAQ_Variables)
                {
                    Value = $"{var.Index},{var.VariableName},{var.NorminalName},{var.Unit},{var.CalibrationTable[0].RealValue},{var.CalibrationTable[1].RealValue},{var.CalibrationTable[0].IOValue},{var.CalibrationTable[1].IOValue},{var.AlarmUse},{var.Alarm_Low},{var.Alarm_High},{var.WarningUse},{var.Warning_Low},{var.Warning_High},{var.Averaging},{var.AveragingCount},{var.Round},{var.NorminalDescription}";
                    System.IO.File.AppendAllText(path, Value + "\r\n");
                }

            }
            catch (Exception)
            {
                Debug.Print("Melsec File Export 에러");
                return false;
            }

            return true;
        }
        private void ErorMsg(COMException Ex)
        {
            string Str = "Error from " + Ex.Source + "\n\r";
            Str = Str + Ex.Message + "\n\r";
            Str = Str + "HRESULT:" + String.Format("0x{0:X}", (Ex.ErrorCode));
            Console.Write(Str);
        }
        public void Stop_MelsecCom()
        {
            thGetData?.Abort();

            ActUtilType?.Close();

            try
            {
                bConnected = false;
            }
            catch (System.NullReferenceException)
            {
                Console.Write("비정상 종료가 발생하였습니다.");
            }
        }

        public void Start_MelsecCom(int logicalStationNumber)
        {
            iData_length = 20;
            iData = new int[iData_length];
            char_arr = new char[iData_length];
            Array.Clear(iData, 0, iData_length);


            try
            {
                // PLC 연결을 위한 라이브러리 정의
                ActUtilType = new ActUtlTypeLib.ActUtlType();

                // 여기의 Station 번호는 MX Component에서 설정한 번호를 정의 한다. 0 = simulator, 1 = 끝자리 IP 41
                this.logicalStationNumber = logicalStationNumber;
                ActUtilType.ActLogicalStationNumber = logicalStationNumber;


                // PLC 연결. 리턴값이 "0"이면 성공
                int nRtn = ActUtilType.Open();
                if (nRtn == 0)
                {
                    if (logicalStationNumber == 1)
                    {
                        Main.AddLog("DAQ PLC 연결 성공", Log.LogType.OK);
                        bConnected = true;
                    }
                    else if (logicalStationNumber == 2)
                    {
                        Main.AddLog("RGV PLC연결 성공", Log.LogType.OK);
                        bConnected = true;
                    }
                    thGetData = new Thread(new ThreadStart(OnRun));
                    thGetData.Start();

                }
                else
                {
                    if (logicalStationNumber == 1)
                    {
                        Main.AddLog("DAQ PLC 연결 실패", Log.LogType.Error);
                        bConnected = false;
                    }
                    else if (logicalStationNumber == 2)
                    {
                        Main.AddLog("RGV PLC 연결 실패", Log.LogType.Error);
                        bConnected = false;
                    }
                }
            }
            catch (COMException Ex)
            {
                ErorMsg(Ex);
            }
            catch (Exception ex)
            {
                Stop_MelsecCom();
                Main.AddLog("PLC 연결 실패", Log.LogType.OK);
            }

        }


        // bit / word
        public int GetMelsecData_DAQ(string strAddr)
        {
            int plcData = -1;
            try
            {
                ActUtilType.GetDevice(strAddr, out plcData);
                int[] test = new int[100];
                //ActUtilType.ReadDeviceBlock("D6000",100,out test[0]);
            }
            catch (Exception ex)
            {
                Debug.Print("MELSEC GetMelsecData 오류" + ex.Message + ex.StackTrace);
            }
            return plcData;
        }
        public int GetMelsecData_RGV(string strAddr)
        {
            int plcData = -1;
            try
            {
                ActUtilType.GetDevice(strAddr, out plcData);
            }
            catch (Exception ex)
            {
                Debug.Print("MELSEC GetMelsecData 오류" + ex.Message + ex.StackTrace);
            }
            return plcData;
        }
        public void SetMelsecData(string strAddr, int nVal)
        {
            try
            {
                ActUtilType.SetDevice(strAddr, nVal);
            }
            catch (Exception ex)
            {
                Debug.Print("MELSEC SetMelsecData 오류" + ex.Message + ex.StackTrace);
            }
        }


        //string 
        private void ID_Throw_Block(string strAddr, string strData)         // WriteDeviceBlock    글자 긁어와서 char 변환 후 아스키코드로 변환해서 넣음. 
        {
            temp1 = 0;
            Array.Clear(iData, 0, iData_length);
            for (int i = 0; i < iData_length; i++)
            {
                char_arr[i] = strData[i];
                iData[i] = Convert.ToInt32(char_arr[i]);
            }
            temp1 = ActUtilType.WriteDeviceBlock(strAddr, iData_length, ref iData[0]);
        }

        public string ID_Catch_Block(string strAddr) //ReadDeviceBlock   iData배열에서 값을 가져와서 가져온 값을 문자열로 만들고 출력.
        {
            string strData = "";
            temp1 = 0;
            Array.Clear(iData, 0, iData_length);
            temp1 = ActUtilType.ReadDeviceBlock(strAddr, iData_length, out iData[0]);
            if (temp1 == 0)
            {
                for (int i = 0; i < iData_length; i++)
                {
                    str2 += Convert.ToChar(iData[i]);
                }
                strData = str2;
            }

            return strData;
        }




        //private void OnRun(Object source, ElapsedEventArgs e)
        //{
        //    try
        //    {
        //        foreach(VariableInfo var in Main.project.sysVarInfo.Mitsubishi_Variables)
        //        {
        //            var.IOValue = GetMelsecData(var.NorminalDescription);
        //        }
        //        Application.DoEvents();
        //    }
        //    catch (Exception)
        //    {
        //        Stop_MelsecCom();
        //    }
        //}

        private void OnRun()
        {
            if (logicalStationNumber == 1)
            {
                while (true)
                {
                    try
                    {
                        foreach(DeviceGroup dg in deviceGroups)
                        {
                            ActUtilType.ReadDeviceBlock(dg.StartAddress, dg.AddressLength, out dg.buffer[0]);

                            int startNumber = dg.devices[0].Number;

                            for(int i=0; i< dg.devices.Count; i++)
                            {
                                int bufferindex = dg.devices[i].Number - startNumber;
                                dg.devices[i].IOValue = dg.buffer[bufferindex];
                            }
                            
                        }

                        foreach (VariableInfo var in Main.project.sysVarInfo.CNFODAQ_Variables)
                        {
                           var.IOValue = deviceGroups.getIOValue(var.NorminalDescription);
                        }


                        //하나하나 읽는 방식. 느림.
                        //foreach (VariableInfo var in Main.project.sysVarInfo.CNFODAQ_Variables)
                        //{
                        //    if (var.CalType == CalibrationType.CalByTable)
                        //    {
                        //        var.IOValue = GetMelsecData_DAQ(var.NorminalDescription);
                        //    }

                        //}

                    }
                    catch (Exception)
                    {
                        Stop_MelsecCom();
                    }

                    System.Threading.Thread.Sleep(mATTimer);
                }
            }
            else if (logicalStationNumber == 2)
            {
                while (true)
                {
                    try
                    {
                        foreach (VariableInfo var in Main.project.sysVarInfo.RGV_Variables)
                        {
                            if (var.CalType == CalibrationType.None || var.CalType == CalibrationType.String)
                                var.IOValue = GetMelsecData_RGV(var.NorminalDescription);
                        }
                    }
                    catch (Exception)
                    {
                        Stop_MelsecCom();
                    }

                    System.Threading.Thread.Sleep(mATTimer);
                }
            }
        }
        void MakeDeviceGroup()
        {
            foreach (VariableInfo var in Main.project.sysVarInfo.CNFODAQ_Variables)
            {
                if (var.CalType == CalibrationType.BitInValue)
                {
                    continue;
                }

                string groupString = var.NorminalDescription.Substring(0, 1);
                string strAddress = var.NorminalDescription.Substring(1, var.NorminalDescription.Length - 1);
                int address = int.Parse(strAddress);

                if (deviceGroups.ExistGroup(groupString))
                {
                    int index = deviceGroups.FindGroupIndex(groupString);
                    deviceGroups[index].AddDevice(groupString, address);
                }
                else
                {
                    deviceGroups.Add(new DeviceGroup(groupString));
                    deviceGroups.Last().AddDevice(groupString, address);
                }
            }

            foreach (DeviceGroup dg in deviceGroups)
            {
                dg.devices = dg.devices.OrderBy(x => x.Number).ToList();
                dg.buffer = new int[dg.AddressLength];
            }

            
        }


    }
    class DeviceGroups : List<DeviceGroup>
    {
        public bool ExistGroup(string groupString)
        {
            foreach (DeviceGroup deviceGroup in this)
            {
                if (groupString == deviceGroup.GroupString)
                {
                    return true;
                }
            }

            return false;
        }

        public int FindGroupIndex(string groupString)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].GroupString == groupString)
                {
                    return i;
                }
            }

            return -1;
        }

        public double getIOValue(string Address)
        {
            string groupString = Address.Substring(0, 1);
            int number = int.Parse(Address.Substring(1, Address.Length -1));

            int index = FindGroupIndex(groupString);

            if (index == -1)
            {
                return 0;
            }

            foreach (Device device in this[index].devices)
            {
                if(device.Number == number)
                {
                    return device.IOValue;
                }
            }

            return 0;
        }
    }
    class DeviceGroup
    {
        string groupString = string.Empty;
        int startAddress = -1;
        int addressLength = -1;

        public int[] buffer;
        public List<Device> devices = new List<Device>();

        public void AddDevice(string groupString, int number)
        {
            devices.Add(new Device(groupString, number));
        }

        public string GroupString { get => groupString; set => groupString = value; }

        public string StartAddress
        {
            get
            {
                return devices.First().DeviceAddress;
            }
        }

        public int StartNumber
        {
            get
            {
                return devices.First().Number;
            }
        }

        public int AddressLength { get { return devices.Last().Number - devices.First().Number + 1; } }

        public DeviceGroup(string groupString)
        {
            this.groupString = groupString;
        }
    }

    class Device
    {
        string groupString = string.Empty;
        int number;
        double ioValue = 0.0;

        public string DeviceAddress { get => GroupString + Number.ToString("D4"); }
        public string GroupString { get => groupString; set => groupString = value; }
        public int Number { get => number; set => number = value; }
        public double IOValue { get => ioValue; set => ioValue = value; }

        public Device(string groupString, int number)
        {
            this.GroupString = groupString;
            this.Number = number;
        }
    }
}
