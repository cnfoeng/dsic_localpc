﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using System.Threading;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace DSIC_Local
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = true;
            
            bool bnew;
            Mutex mutex = new Mutex(true, "MutexName", out bnew);
            if (bnew)
            {
                Process.GetCurrentProcess().PriorityBoostEnabled = true;
                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;

                string deploymentKey = "lgCAAMOOmPwMBNYBJABVcGRhdGVhYmxlVGlsbD0yMDIxLTAzLTI3I1JldmlzaW9uPTABDUH9IhH+QhhoFGxHCqWnYJHuG6HG+8ducBal1exGGiVPXE76goHVAp6ToPLJmfXHykjmT/8QitrHrxXqNy5Thmbn64Q1osEvzjeof3VahIKOjwJ/tbhWxooOYH0F44ZW6qWDacAyMV/755bKlEdzFgZio5RTcuBeMu3z6kUSddQOCuT/3Cqkh43RerMz2D+HvqRU07Sl+/yZqznhZWcBKCn2CZqp7cV4t9Y6J3YSSAu8OsyAycYlMHIcfF3y/3qfXs7XLFHyERaz0wbfsO4W3pnDP31fJfmyTE/LUuHs5SE/nwF1Cnb0KXlaUieSwxYCvxu2M6liYDY23Aoi3juKzPZdSydKrvM4zvnline7bE7T/sQZFQCFtnKHAPBE1KYUqlEZrCQPfNVgxOoKC47K1ZT/FqxEuW4RrCkh4fgC+uF2v04n9icNuZ9YkArJ525SbwwjYgup+TLv5Gwd6RRboaNhyJ/ZOQttc+pX7IJb/b2uASKReWh78uWPg45Ouj4qiw==";

                Arction.WinForms.Charting.LightningChartUltimate.SetDeploymentKey(deploymentKey);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
                mutex.ReleaseMutex();
            }
            else
            {
                XtraMessageBox.Show("<size=14>프로그램이 이미 실행 중입니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();

            }
        }
    }
}
