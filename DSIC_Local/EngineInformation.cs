﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using jnsoft.ASAP2;
using jnsoft.ASAP2.Values;
using jnsoft.Helpers;
using DSIC_Local.Monitoring;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using DevExpress.Spreadsheet;

namespace DSIC_Local
{
    public enum Engine
    {
        DV11,
        DX22,
    }
    public enum EES
    {
        Euro3,
        Euro3Tier3,
        Euro4,
        Euro5,
        None
    }
    public enum QsettingType
    {
        Value,
        Map,
        Curve,
    }

    [Serializable]
    public class Project
    {
        public List<EngineInformation> engines = new List<EngineInformation>();
        public List<List<MonitorItem>> list_MonitorItems = new List<List<MonitorItem>>();
        public EngineInformation currentEngine;
        public string OBD_COMPort = "COM8";
        public string SmokeMeter_COMPort = "COM2";
        public string cellNumber = "0";
        public string worker = string.Empty;
        public string inspector = string.Empty;
        public LogSetting logSetting = new LogSetting();
        public SysVarInfo sysVarInfo = new SysVarInfo();
        public DBConnectionData dBConnectionData = new DBConnectionData();

        //Averaging용
        public string AveragingValue1 = "스피드";
        public string AveragingValue2 = "수정하중_기계전기";
        public string AveragingValue3 = "수정토크_기계전기";
        public string AveragingValue4 = "수정출력_기계전기";
        public string AveragingValue5 = "연비(초)";
        public string AveragingValue6 = "오일압력";
        public string AveragingValue7 = "";
        public string AveragingValue8 = "";

        public int AveragingTime = 30;
        public void MakeDefaultMonitorItem()
        {
            list_MonitorItems.Clear();
            List<MonitorItem> main = new List<MonitorItem>();
            list_MonitorItems.Add(main);
            List<MonitorItem> monitoring = new List<MonitorItem>();
            list_MonitorItems.Add(monitoring);
            List<MonitorItem> outside = new List<MonitorItem>();
            list_MonitorItems.Add(outside);
        }
        public void SaveProject(string saveFilePath)
        {
            FileStream fileStreamObject;
            string FileName = saveFilePath;
            fileStreamObject = new FileStream(FileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStreamObject, this);
            fileStreamObject.Close();
        }

        public Project LoadProject(string loadFilePath)
        {
            try
            {
                FileStream fileStreamObject;
                string FileName = loadFilePath;
                fileStreamObject = new FileStream(FileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object dd = binaryFormatter.Deserialize(fileStreamObject);
                fileStreamObject.Close();
                return (Project)dd;
            }
            catch (System.IO.FileNotFoundException)
            {
                return null;
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                return null;
            }
        }
        public void MakeDefaultDX22Engines()
        {
            string DefaultPath = @"\MapData\";

            EngineInformation ei = new EngineInformation(Engine.DX22, EES.None, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
            MemorySegmentList msl = new MemorySegmentList();
            MemorySegment ms = new MemorySegment(0x8001C000, 0x01DD00, MEMORYPRG_TYPE.OFFLINE_DATA);//CB
            msl.Add(ms);
            ms = new MemorySegment(0x80080000, 0x120100, MEMORYPRG_TYPE.CODE);//ASW0
            msl.Add(ms);
            ms = new MemorySegment(0x80200000, 0x08DD00, MEMORYPRG_TYPE.CODE);//ASW1
            msl.Add(ms);
            ms = new MemorySegment(0x80300000, 0x032F00, MEMORYPRG_TYPE.CODE);//ASW2
            msl.Add(ms);
            ms = new MemorySegment(0x80400000, 0x0C0000, MEMORYPRG_TYPE.DATA);//DS0
            msl.Add(ms);
            ms = new MemorySegment(0x804C0000, 0x140000, MEMORYPRG_TYPE.DATA);//DS0
            msl.Add(ms);
            ei.A2LMemorySegments = msl;
            engines.Add(ei);
        }
        public void MakeDefaultDV11Engines()
        {

            engines.Clear();

            string DefaultPath = @"\MapData\";

            engines.Add(new EngineInformation(Engine.DV11, EES.Euro3, string.Empty, DefaultPath + @"DV11-Euro3,Tier3\P41741.a2l", string.Empty, string.Empty, DefaultPath + @"DV11-Euro3,Tier3\ChangedHexFile\"));
            engines.Add(new EngineInformation(Engine.DV11, EES.Euro4, string.Empty, DefaultPath + @"DV11-EQJTA\P68431.a2l", string.Empty, string.Empty, DefaultPath + @"DV11-EQJTA\ChangedHexFile\"));
            engines.Add(new EngineInformation(Engine.DV11, EES.Euro5, string.Empty, DefaultPath + @"DV11-Euro4,5\P68471.a2l", string.Empty, string.Empty, DefaultPath + @"DV11-Euro4,5\ChangedHexFile\"));
        }

        public void MakeDefaultDL06Engines()
        {
            string DefaultPath = @"\MapData\";

            EngineInformation ei = new EngineInformation(Engine.DV11, EES.Euro3, "DL06", string.Empty, DefaultPath + "DL06_EUTFA_A01.hex", DefaultPath + "DL06_EUTFA_A01.hex", string.Empty);
            MemorySegmentList msl = new MemorySegmentList();
            MemorySegment ms = new MemorySegment(0x1E0000, 0x1FFFFF, MEMORYPRG_TYPE.DATA);
            msl.Add(ms);

            ei.A2LMemorySegments = msl;
            engines.Add(ei);
        }
    }

    [Serializable]
    public class EngineInformation
    {
        Engine engine;
        EES ees;
        string suffix;
        string a2lPath;
        string dynoHexPath;
        string shipmentHexPath;
        string changedHexFolderPath;

        public Engine Engine { get => engine; set => engine = value; }
        public EES Ees { get => ees; set => ees = value; }
        public string Suffix { get => suffix; set => suffix = value; }
        public string A2LPath { get => a2lPath; set => a2lPath = value; }
        public string DynoHexPath { get => dynoHexPath; set => dynoHexPath = value; }
        public string ShipmentHexPath { get => shipmentHexPath; set => shipmentHexPath = value; }
        public string ChangedHexFolderPath { get => changedHexFolderPath; set => changedHexFolderPath = value; }

        [NonSerialized]
        public MemorySegmentList A2LMemorySegments;

        [NonSerialized]
        public List<ExtractedHexFileData> extractedHexFiles = new List<ExtractedHexFileData>();

        public List<QsettingParameter> qsettingParameters = new List<QsettingParameter>();

        [NonSerialized]
        public List<ECUFault> ECUFaults = new List<ECUFault>();

        public EngineInformation(Engine engine, EES ees, string suffix, string a2lPath, string dynoHexPath, string shipmentHexPath, string changedHexFolderPath)
        {
            this.engine = engine;
            this.ees = ees;
            this.suffix = suffix;
            this.a2lPath = a2lPath;
            this.dynoHexPath = dynoHexPath;
            this.shipmentHexPath = shipmentHexPath;
            this.changedHexFolderPath = changedHexFolderPath;

            if (ees == EES.Euro4)
            {
                double[][] eapp_pv_norn_cur_value = new double[2][];
                eapp_pv_norn_cur_value[0] = new double[] { 600.0, 3500.0 };
                eapp_pv_norn_cur_value[1] = new double[] { 0.0, 100.0 };
                //qsettingParameters.Add(new QsettingParameter("EAPP_PV_NORM_CUR", eapp_pv_norn_cur_value, "Normalization curve for accelerator pedal", QsettingType.Curve, "", true));
                //qsettingParameters.Add(new QsettingParameter("EAPP_S_CANINPUT_CB", 0.0, "Select signal source ,TRUE= CAN i/p ,FALSE = analog sensor", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("EAPP_S_LISLOG_CB", 0.0, "Logical level low idle switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_SBRMODE_CUC", 0.0, "Modeswitch for plausibility check with service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_FL_CW", 5000.0, "Threshold-voltage acc.pedal-low idle sw. in full load pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_LI_CW", 0.0, "Threshold-voltage acc.pedal-low idle sw. in low idle pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MAX_CW", 5000.0, "Threshold-voltage (short to UBat) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MIN_CW", 0.0, "Threshold-voltage (short to GND) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESBR_DT_DEF_CUW", 3276750.0, "Time for defect recognition of service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESTP_S_LOGIC_CB", 1.0, "Logic level: Engine shutdown switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_ATS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_SBR_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("FMON_LPS02_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_STSP_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS03_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("FMON_LPS09_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("FMON_LPS10_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS11_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS11_CSTR.SELF", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS11_CSTR.WARN", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS16_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("FMON_STSP_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("FMON_VSS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("MOOR_S_ENABLE_CB", 0.0, "overall switch for fault reaction (must be TRUE)", QsettingType.Value, "", true));
                //qsettingParameters.Add(new QsettingParameter("MOOR_S_RECOVERY_CB", 0.0, "overall switch for recovery (must be TRUE)", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS02_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                //추가
                qsettingParameters.Add(new QsettingParameter("FMON_OPS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
            }
            else if (ees == EES.Euro5)
            {
                double[][] eapp_pv_norn_cur_value = new double[2][];
                eapp_pv_norn_cur_value[0] = new double[] { 600.0, 3500.0 };
                eapp_pv_norn_cur_value[1] = new double[] { 0.0, 100.0 };
                qsettingParameters.Add(new QsettingParameter("EAPP_PV_NORM_CUR", eapp_pv_norn_cur_value, "Normalization curve for accelerator pedal", QsettingType.Curve, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_CANINPUT_CB", 0.0, "Select signal source ,TRUE= CAN i/p ,FALSE = analog sensor", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_LISLOG_CB", 0.0, "Logical level low idle switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_SBRMODE_CUC", 0.0, "Modeswitch for plausibility check with service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_FL_CW", 5000.0, "Threshold-voltage acc.pedal-low idle sw. in full load pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_LI_CW", 0.0, "Threshold-voltage acc.pedal-low idle sw. in low idle pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MAX_CW", 5000.0, "Threshold-voltage (short to UBat) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MIN_CW", 0.0, "Threshold-voltage (short to GND) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESBR_DT_DEF_CUW", 3276750.0, "Time for defect recognition of service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESTP_S_LOGIC_CB", 1.0, "Logic level: Engine shutdown switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_ATS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS02_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS03_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS09_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS10_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS11_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_LPS16_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_STSP_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_VSS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("MOOR_S_ENABLE_CB", 0.0, "overall switch for fault reaction (must be TRUE)", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("MOOR_S_RECOVERY_CB", 0.0, "overall switch for recovery (must be TRUE)", QsettingType.Value, "", true));
            }
            else if (ees == EES.Euro3)
            {
                double[][] eapp_pv_norn_cur_value = new double[2][];
                eapp_pv_norn_cur_value[0] = new double[] { 600.0, 3500.0 };
                eapp_pv_norn_cur_value[1] = new double[] { 0.0, 100.0 };
                qsettingParameters.Add(new QsettingParameter("EAPP_PV_NORM_CUR", eapp_pv_norn_cur_value, "Normalization curve for accelerator pedal", QsettingType.Curve, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_CANINPUT_CB", 0.0, "Select signal source ,TRUE= CAN i/p ,FALSE = analog sensor", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_LISLOG_CB", 0.0, "Logical level low idle switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_S_SBRMODE_CUC", 0.0, "Modeswitch for plausibility check with service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_FL_CW", 5000.0, "Threshold-voltage acc.pedal-low idle sw. in full load pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_LIS_LI_CW", 0.0, "Threshold-voltage acc.pedal-low idle sw. in low idle pos.", QsettingType.Value, "mV", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MAX_CW", 5000.0, "Threshold-voltage (short to UBat) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("EAPP_U_MIN_CW", 0.0, "Threshold-voltage (short to GND) accelerator pedal", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESBR_DT_DEF_CUW", 3276750.0, "Time for defect recognition of service brake", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("ESTP_S_LOGIC_CB", 1.0, "Logic level: Engine shutdown switch", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_ATS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_STSP_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_VSS_CSTR.MASK", 0.0, "Mask the fault type for linked list", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("MOOR_S_ENABLE_CB", 0.0, "overall switch for fault reaction (must be TRUE)", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("MOOR_S_RECOVERY_CB", 0.0, "overall switch for recovery (must be TRUE)", QsettingType.Value, "", true));
                //새로 추가
                qsettingParameters.Add(new QsettingParameter("FMON_HP2_CSTR.MASK", 0.0, "", QsettingType.Value, "", true));
                qsettingParameters.Add(new QsettingParameter("FMON_HP4_CSTR.MASK", 0.0, "", QsettingType.Value, "", true));
            }


        }
        public void ClearMemorySegment()
        {
            A2LMemorySegments = null;
        }
        public void MakeFaultList(string path)
        {
            if (ECUFaults != null)
                ECUFaults.Clear();
            else
                ECUFaults = new List<ECUFault>();

            Workbook workbook = new Workbook();

            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                workbook.LoadDocument(stream, DocumentFormat.Xlsx);

                var worksheet = workbook.Worksheets[0];

                for (int i = 1; i < 500; i++)
                {
                    if (worksheet.Columns["A"][i].Value.ToString() == "")
                    {
                        break;
                    }

                    string faultName = worksheet.Columns["B"][i].Value.ToString();
                    string pcode = worksheet.Columns["C"][i].Value.ToString();
                    string description = worksheet.Columns["D"][i].Value.ToString();

                    ECUFaults.Add(new ECUFault(faultName, pcode, description));
                }


            }

        }
    }
    [Serializable]
    public class QsettingParameter
    {
        string name;
        object value;
        string description;
        QsettingType qsettingType;
        string unit;
        bool usage = true;
        public string strQsettingType
        {
            get
            {
                return this.QsettingType.ToString();
            }
            set
            {
                strQsettingType = value;
            }
        }
        public string DisplayValue
        {
            get
            {
                if (QsettingType == QsettingType.Value)
                {
                    return Value.ToString();
                }
                else if (QsettingType == QsettingType.Curve)
                {
                    double[][] arr = (double[][])Value;
                    return $"{{{arr[0][0]},{arr[0][1]}}} {{{arr[1][0]},{arr[1][1]}}}";
                }
                return null;
            }

            set
            {
                if (QsettingType == QsettingType.Value)
                {
                    Value = double.Parse(value);
                }
                else if (QsettingType == QsettingType.Curve)
                {
                    string changedStr = value;

                    double[][] arr = new double[2][];
                    string trimmed = changedStr.Replace(" ", "");
                    string[] splited = trimmed.Split('{', '}', ',');


                    arr[0] = new double[2] { double.Parse(splited[1]), double.Parse(splited[2]) };
                    arr[1] = new double[2] { double.Parse(splited[4]), double.Parse(splited[5]) };

                    Value = arr;

                }

            }
        }
        public string Name { get => name; set => name = value; }
        public object Value { get => value; set => this.value = value; }
        public string Description { get => description; set => description = value; }
        internal QsettingType QsettingType { get => qsettingType; set => qsettingType = value; }
        public string Unit { get => unit; set => unit = value; }
        public bool Usage { get => usage; set => usage = value; }

        public QsettingParameter(string name, object value, string description, QsettingType qsettingType, string unit, bool usage)
        {
            this.name = name;
            this.value = value;
            this.description = description;
            this.qsettingType = qsettingType;
            this.unit = unit;
            this.usage = usage;
        }
    }
    [Serializable]
    public class ExtractedHexFileData
    {
        public MEMORYPRG_TYPE prgType;
        string data;
        string checksum;
        public int Size
        {
            get
            {
                return Data.Length / 2;
            }
        }

        public string Data { get => data; set => data = value; }
        public string Checksum { get => checksum; set => checksum = value; }

        public ExtractedHexFileData(MEMORYPRG_TYPE prgType, string data, string checksum)
        {
            this.prgType = prgType;
            this.Data = data;
            this.checksum = checksum;
        }
    }

    public class ECUFault
    {
        private string faultName;
        private string pCode;
        private string description;
        private bool check;

        public string FaultName { get => faultName; set => faultName = value; }
        public string PCode { get => pCode; set => pCode = value; }
        public string Description { get => description; set => description = value; }
        public bool Checked { get => check; set => check = value; }

        public ECUFault(string faultname, string pcode, string description)
        {
            this.faultName = faultname;
            this.pCode = pcode;
            this.description = description;
        }
    }

    [Serializable]
    public class LogSetting
    {
        private string dataLoggingConstantPath = string.Empty;
        private int maxRowCount = 10000;
        private int loggingInterval = 1000;
        private string dataPrefix = "Data";

        public string DataLoggingConstantPath { get => dataLoggingConstantPath; set => dataLoggingConstantPath = value; }
        public int MaxRowCount { get => maxRowCount; set => maxRowCount = value; }
        public int LoggingInterval { get => loggingInterval; set => loggingInterval = value; }
        public string DataPrefix { get => dataPrefix; set => dataPrefix = value; }
    }

    public class ResultData
    {
        public string engineNumber = string.Empty;
        public string suffix = string.Empty;
        public string model = string.Empty;
        public string forWhat = string.Empty;
        public string schduleFilePath = string.Empty;
        public string turboCharger = string.Empty;
        public string fuelSpecificGravity = string.Empty;
        public string strK = string.Empty;
        public string mapFilePath = string.Empty;
        public string injectorInformation = string.Empty;
        public string engineCategory = string.Empty;
        public string engineType = string.Empty;


        public string startDate = string.Empty;
        public string startTime = string.Empty;
        public string endTime = string.Empty;
        public string cellNumber = string.Empty;
        public string worker = string.Empty;
        public string inspector = string.Empty;
        public string manager = string.Empty;
        public double AirTemp = 0;
        public double RelativeHumidity = 0;
        public double AtmosphericPressure = 0;
        public double KNum = 0;
        public double FuelTemp = 0;
        public double CoolantTemp = 0;
        public double FuelDensity = 0;
        public double ExhaustPressure = 0;
    }

    [Serializable]
    public class DBConnectionData
    {
        public string IP = "10.32.35.229";
        public string Port = "1433";
        public string UserID = "dacos";
        public string Pwd = "socad";
        public string TableName = "DHIM";
    }
}
