﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;
using DWORD = System.UInt32;
using System.Threading;
using jnsoft.ASAP2;
using System.Windows.Forms;

namespace DSIC_Local.CAN
{
    public class ISOTP_UDS
    {
        public CANStatus status = CANStatus.Normal;
        public bool isConnected = false;
        public bool doClear = false;
        public bool doReadDTC = false;
        public string mapVersion = string.Empty;
        public string flashDate = string.Empty;
        public string engineSerial = string.Empty;
        public int progressbarMaximum = 0;
        public int progressbarValue = 0;
        public StringBuilder log = new StringBuilder();

        public enum CANStatus
        {
            Normal,
            Flash,
            None,
        }
        public enum FlashType
        {
            NoFlash,//수동 데이터 입력용 = 개발용
            FullFlash,
            DataOnlyFlash
        }
        public enum DelayType
        {
            Delay,
            NoDelay,
        }

        CANCom can;

        public ISOTP_UDS()
        {
            can = Main.canCom;
        }
        public void ECUFlash(FlashType flashType)
        {
            if (!SecurityAccess(true))
            {
                ECUFlashing.FlashLogs.Add("SecurityAccess 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("SecurityAccess 성공");


            if (!StartProgrammingSession(true))
            {
                ECUFlashing.FlashLogs.Add("Programming Session 오픈 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("Programming Session 오픈 성공");

            if (!TesterPresent(true))
            {
                ECUFlashing.FlashLogs.Add("TesterPresent 실패");
                return;
            }

            if (!ECUReset_JumpToCB(true))
            {
                ECUFlashing.FlashLogs.Add("ECU Reset - Jump To CB 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("ECU Reset - Jump To CB 성공");

            if (!WriteDataByIdentifier_FlashDate(true))
            {
                ECUFlashing.FlashLogs.Add("Flash 날짜 기입 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("Flash 날짜 기입 성공");
            if (flashType == FlashType.FullFlash)
            {

                progressbarMaximum = 0;
                progressbarValue = 0;

                for (int i=0; i < 6; i++)
                {
                    progressbarMaximum += Main.project.currentEngine.extractedHexFiles[i].Size;
                }

                //CB Area Flashing
                ECUFlashing.FlashLogs.Add("CB Area Flash 시작");
                if (!EraseMemory("00", true))
                {
                    ECUFlashing.FlashLogs.Add("EraseMemory 실패");
                    return;
                }
                if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[0], null, true))
                {
                    ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                    return;
                }

                if (!TransferData(Main.project.currentEngine.extractedHexFiles[0], true))
                {
                    ECUFlashing.FlashLogs.Add("TransferData 실패");
                    return;
                }

                if (!RequestTransferExit(true))
                {
                    ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                    return;
                }

                if (!CheckMemory("00", true))
                {
                    ECUFlashing.FlashLogs.Add("CheckMemory 실패");
                    return;
                }
                ECUFlashing.FlashLogs.Add("CB Area Flash 완료");


                //ASW Area Flashing
                ECUFlashing.FlashLogs.Add("ASW0 Area Flash 시작");
                if (!EraseMemory("01", true))
                {
                    ECUFlashing.FlashLogs.Add("EraseMemory 실패");
                    return;
                }

                //ASW 0 Area
                if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[1], null, true))
                {
                    ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                    return;
                }

                if (!TransferData(Main.project.currentEngine.extractedHexFiles[1], true))
                {
                    ECUFlashing.FlashLogs.Add("TransferData 실패");
                    return;
                }

                if (!RequestTransferExit(true))
                {
                    ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                    return;
                }
                ECUFlashing.FlashLogs.Add("ASW0 Area Flash 완료");

                //ASW 1 Area
                ECUFlashing.FlashLogs.Add("ASW1 Area Flash 시작");
                if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[2], null, true))
                {
                    ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                    return;
                }

                if (!TransferData(Main.project.currentEngine.extractedHexFiles[2], true))
                {
                    ECUFlashing.FlashLogs.Add("TransferData 실패");
                    return;
                }

                if (!RequestTransferExit(true))
                {
                    ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                    return;
                }
                ECUFlashing.FlashLogs.Add("ASW1 Area Flash 완료");

                //ASW 2 Area
                ECUFlashing.FlashLogs.Add("ASW2 Area Flash 시작");
                if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[3], null, true))
                {
                    ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                    return;
                }

                if (!TransferData(Main.project.currentEngine.extractedHexFiles[3], true))
                {
                    ECUFlashing.FlashLogs.Add("TransferData 실패");
                    return;
                }

                if (!RequestTransferExit(true))
                {
                    ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                    return;
                }

                if (!CheckMemory("01", true))
                {
                    ECUFlashing.FlashLogs.Add("CheckMemory 실패");
                    return;
                }
                ECUFlashing.FlashLogs.Add("ASW2 Area Flash 완료");
            }

            //Data Area Flashing 
            if (!EraseMemory("02", true))
            {
                ECUFlashing.FlashLogs.Add("EraseMemory 실패");
                return;
            }
            if (!EraseMemory("03", true))
            {
                ECUFlashing.FlashLogs.Add("EraseMemory 실패");
                return;
            }

            //DS 0 Area
            ECUFlashing.FlashLogs.Add("DS0 Area Flash 시작");
            if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[4], null, true))
            {
                ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                return;
            }

            if (!TransferData(Main.project.currentEngine.extractedHexFiles[4], true))
            {
                ECUFlashing.FlashLogs.Add("TransferData 실패");
                return;
            }

            if (!RequestTransferExit(true))
            {
                ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                return;
            }

            if (!CheckMemory("02", true))
            {
                ECUFlashing.FlashLogs.Add("CheckMemory 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("DS0 Area Flash 완료");

            //DS 1 Area
            ECUFlashing.FlashLogs.Add("DS1 Area Flash 시작");
            if (!RequestDownload(Main.project.currentEngine.A2LMemorySegments[5], null, true))
            {
                ECUFlashing.FlashLogs.Add("RequestDownload 실패");
                return;
            }

            if (!TransferData(Main.project.currentEngine.extractedHexFiles[5], true))
            {
                ECUFlashing.FlashLogs.Add("TransferData 실패");
                return;
            }

            if (!RequestTransferExit(true))
            {
                ECUFlashing.FlashLogs.Add("RequestTransferExit 실패");
                return;
            }

            if (!CheckMemory("03", true))
            {
                ECUFlashing.FlashLogs.Add("CheckMemory 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("DS1 Area Flash 완료");

            //ECU Hard Reset
            if (!ECUReset_HardReset(true))
            {
                ECUFlashing.FlashLogs.Add("ECU Hard Reset 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("ECU Hard Reset 완료");

            Main.AddLog("Flash 완료", Log.LogType.OK);
            ECUFlashing.FlashLogs.Add("Flash 완료");
            status = CANStatus.Normal;

            Thread.Sleep(4000);

            if (!SecurityAccess(true))
            {
                ECUFlashing.FlashLogs.Add("SecurityAccess 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("SecurityAccess 성공");

            if (!StartExtendedDiagnosticSession(true))
            {
                ECUFlashing.FlashLogs.Add("ExtendedDiagnostic Session 오픈 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("ExtendedDiagnostic Session 오픈 성공");

            if (!TesterPresent(true))
            {
                ECUFlashing.FlashLogs.Add("TesterPresent 실패");
                return;
            }
            if (flashType == FlashType.NoFlash)
            {
                return;
            }
            if (!WriteDataByIdentifier_EngineSerial(Main.resultData.engineNumber, true))
            {
                ECUFlashing.FlashLogs.Add("엔진 시리얼 번호 기입 실패");
                return;
            }
            ECUFlashing.FlashLogs.Add("엔진 시리얼 번호 기입 성공");

            if (Main.canCom.StartGetData())
            {
                ECUFlashing.FlashLogs.Add("플래시 후 ECU 통신 성공 - 정상 Flash");
                ECUFlashing.FlashLogs.Add("플래시 Map Version : " + mapVersion);
                ECUFlashing.FlashLogs.Add("플래시 날짜 : " + flashDate);
                ECUFlashing.FlashLogs.Add("엔진 시리얼 번호 : " + engineSerial);

                if (ClearDTC(true))
                {
                    ECUFlashing.FlashLogs.Add("DTC Clear 성공");
                }
                else
                {
                    ECUFlashing.FlashLogs.Add("DTC Clear 실패");
                }
                
            }
            else
            {
                ECUFlashing.FlashLogs.Add("플래시 후 ECU 통신 실패 - 비정상 Flash");
            }



            ECUFlashing.FlashLogs.Add("작업 완료");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + "\\log.txt"))
            {
                file.WriteLine(log.ToString()); // "sb" is the StringBuilder
            }
            //Main.isotp_uds.doReadDTC = true;
        }
        public void Run()
        {
            bool measure_ok = true;
            bool readDTC_ok = true;
            int disconnectCount = 0;

            while (status == CANStatus.Normal)
            {
                if (measure_ok)
                {
                    measure_ok = GetMeasuredData();
                    isConnected = true;
                    disconnectCount = 0;
                }
                else
                {
                    disconnectCount++;
                    if (disconnectCount > 5)
                    {
                        isConnected = false;
                    }
                }

                //if (doReadDTC)
                //{

                if (readDTC_ok)
                {
                    readDTC_ok = ReadDTC(true);
                    isConnected = true;
                    disconnectCount = 0;
                }
                else
                {
                    disconnectCount++;
                    if (disconnectCount > 5)
                    {
                        isConnected = false;
                    }
                }
                //doReadDTC = false;
                //}
                if (doClear)
                {
                    ClearDTC(true);
                    doClear = false;
                }
                System.Threading.Thread.Sleep(800);
            }
        }

        bool ClearDTC(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"14FFFFFF");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x54)
            {
                Debug.Print($"Clear DTC Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                Debug.Print($"Clear DTC Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(100);

                    if (ClearDTC(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {

            }
            return false;
        }
        bool GetMeasuredData()
        {
            List<VariableInfo> varlist = Main.project.sysVarInfo.EngineCom_Variables;

            foreach (VariableInfo var in varlist)
            {
                string LID = var.NorminalDescription;

                if (LID == null)
                {
                    return false;
                }
                LID = LID.Replace(" ", "");
                char[] splitchar = new char[2] { 'x', 'X' };
                string[] split = LID.Split(splitchar);

                byte[] result;
                try
                {
                    result = can.WriteMsgWithReturn($"0322{split[1]}");
                }
                catch (IndexOutOfRangeException)
                {
                    Main.AddLog("Measuring 설정파일이 잘못되었습니다.", Log.LogType.Error);
                    //status = CANStatus.None;
                    return false;
                }
                if (result[1] == 0x62)
                {
                    byte[] data = new byte[2] { result[5], result[4] };
                    int i = BitConverter.ToInt16(data, 0);

                    var.IOValue = i;
                    double d = var.RealValue;
                    
                }
                else
                {
                    return false;
                }

            }


            return true;
        }
        public bool Programsession()
        {
            if (!SecurityAccess(true))
            {
                return false;
            }

            if (!StartProgrammingSession(true))
            {
                return false;
            }

            return true;
        }
        public bool StartDefaultSession(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("1001");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result == null)
            {
                StartDefaultSession(true);
                return true;
            }

            if (result[1] == 0x50)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start Default Session Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start Default Session Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(100);

                    if (StartDefaultSession(false))
                    {
                        return true;
                    }
                }
                return false;
            }

            return false;
        }
        public bool ReadMapVersion(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"22F121");
                PrintResult(result);
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[2] == 0x62)
            {
                string strCodes = result[5].ToString("X2") + result[6].ToString("X2") + result[7].ToString("X2");
                string newline;
                string prevline = string.Empty;

                for (int i = 0; i < 200; i++)//같은거 2번 들어오면 마지막으로 인식.
                {
                    result = ISOTP_WriteMsg("300000");
                    newline = PrintResult(result);

                    if (newline == prevline)
                    {
                        break;
                    }

                    prevline = newline;

                    newline = ExtractCodeOnly(newline);

                    strCodes += newline;

                }

                byte[] ba = KWP2000.StringToByteArray(strCodes);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                temp = temp.Split('*')[0];
                mapVersion = temp;
            }
            else if (result[1] == 0x7F)
            {
                Debug.Print("Read MapVersion Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(100);

                    if (ReadMapVersion(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }

            return false;

        }
        public bool ReadFlashDate(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"22F199");
                PrintResult(result);
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[2] == 0x62)
            {
                string strCodes = result[5].ToString("X2") + result[6].ToString("X2") + result[7].ToString("X2");
                string newline;
                string prevline = string.Empty;

                for (int i = 0; i < 200; i++)//같은거 2번 들어오면 마지막으로 인식.
                {
                    result = ISOTP_WriteMsg("300000");
                    newline = PrintResult(result);

                    if (newline == prevline)
                    {
                        break;
                    }

                    prevline = newline;

                    newline = ExtractCodeOnly(newline);

                    strCodes += newline;

                }

                strCodes = strCodes.Substring(0, 16);
                byte[] ba = KWP2000.StringToByteArray(strCodes);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                flashDate = temp;
            }
            else if (result[1] == 0x7F)
            {
                Debug.Print("Read FlashDate Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(100);

                    if (ReadFlashDate(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
            return false;

        }
        public bool ReadEngineSerial(bool writeMsg)
        {

            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"22F197");
                PrintResult(result);
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[2] == 0x62)
            {
                string strCodes = result[5].ToString("X2") + result[6].ToString("X2") + result[7].ToString("X2");
                string newline;
                string prevline = string.Empty;

                for (int i = 0; i < 200; i++)//같은거 2번 들어오면 마지막으로 인식.
                {
                    result = ISOTP_WriteMsg("300000");
                    newline = PrintResult(result);

                    if (newline == prevline)
                    {
                        break;
                    }

                    prevline = newline;

                    newline = ExtractCodeOnly(newline);

                    strCodes += newline;

                }

                byte[] ba = KWP2000.StringToByteArray(strCodes);
                string temp = System.Text.ASCIIEncoding.ASCII.GetString(ba);
                temp = temp.Split('*')[0];
                engineSerial = temp;
            }
            else if (result[1] == 0x7F)
            {
                Debug.Print("Read EngineSerial Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(100);

                    if (ReadEngineSerial(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
            return false;
        }

        string prevCodes = string.Empty;
        string prevResult = string.Empty;

        public bool ReadDTC(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("19020F");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            string str = PrintResult(result);

            if (str == "03 59 02 FF AA AA AA AA")//Error 없음
            {

                if (prevResult == str)
                {
                    return true;
                }

                for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
                {
                    Main.project.currentEngine.ECUFaults[i].Checked = false;
                }

                prevResult = str;
                return true;
            }
            else
            {
                if (result[1] == 0x59)//한 줄에 에러 1개 또는 2개 있는 경우
                {
                    string strCodes = result[4].ToString("X2") + result[5].ToString("X2");

                    if (result[6] != 0xAA && result[7] != 0xAA)
                    {
                        strCodes += result[6].ToString("X2") + result[7].ToString("X2");
                    }

                    CheckECUFaultList(strCodes);

                    return true;
                }
                else if (result[2] == 0x59) // 에러있고 한 줄 넘어가는 경우
                {
                    string strCodes = result[5].ToString("X2") + result[6].ToString("X2") + result[7].ToString("X2");
                    string newline;
                    string prevline = string.Empty;
                    for (int i = 0; i < 200; i++)//같은거 2번 들어오면 마지막으로 인식.
                    {
                        result = ISOTP_WriteMsg("300000");
                        newline = PrintResult(result);

                        if (newline == prevline)
                        {
                            break;
                        }

                        prevline = newline;

                        newline = ExtractCodeOnly(newline);

                        strCodes += newline;
                    }

                    if (prevCodes != strCodes)
                    {
                        prevCodes = strCodes;

                        CheckECUFaultList(strCodes);
                    }

                    return true;

                }
                else if (result[1] == 0x7F)
                {
                    Debug.Print($"ReadDTC Fail. NRC : 0x{result[3].ToString("X2")}");

                    if (result[3] == 0x78)
                    {
                        Debug.Print("Request Pending..");
                        Thread.Sleep(100);
                        if (ReadDTC(false))
                        {
                            return true;
                        }
                    }
                    
                }
                else
                {
                    //Debug.Print(result[0].ToString());
                    //ReadDTC(false);
                    //return true;
                    return false;
                }
                return false;
            }
        }
        void CheckECUFaultList(string str)
        {
            if (Main.project.currentEngine.ECUFaults == null)
            {
                return;
            }

            for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
            {
                Main.project.currentEngine.ECUFaults[i].Checked = false;
            }

            while (str.Length >= 4)
            {

                string subCode = str.Substring(0, 4);

                if (subCode == "AAAA")
                {
                    break;
                }

                str = str.Substring(4, str.Length - 4);

                string firstString = subCode[0].ToString();
                int firstValue = int.Parse(firstString, System.Globalization.NumberStyles.HexNumber);
                if (firstValue >= 0 && firstValue <= 3)
                {
                    string prefix = "P";
                    prefix = prefix + firstValue.ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 4 && firstValue <= 7)
                {
                    string prefix = "C";
                    prefix = prefix + (firstValue - 4).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 8 && firstValue <= 11)
                {
                    string prefix = "B";
                    prefix = prefix + (firstValue - 8).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }
                else if (firstValue >= 12 && firstValue <= 15)
                {
                    string prefix = "U";
                    prefix = prefix + (firstValue - 12).ToString("X1");

                    subCode = subCode.Remove(0, 1);
                    subCode = subCode.Insert(0, prefix);
                }

                for (int i = 0; i < Main.project.currentEngine.ECUFaults.Count; i++)
                {

                    if (Main.project.currentEngine.ECUFaults[i].PCode == subCode)
                    {
                        Main.project.currentEngine.ECUFaults[i].Checked = true;
                    }
                }
            }
        }
        string ExtractCodeOnly(string str)
        {
            string result = string.Empty;
            string[] bytes = str.Split(' ');

            for (int i = 2; i < bytes.Length; i++)//2인 이유 맨 앞에 ""존재
            {
                result += bytes[i];
            }

            return result;
        }
        string PrintResult(byte[] result)
        {
            string str = "";
            for (int i = 0; i < result.Length; i++)
            {
                str = str + " " + result[i].ToString("X2");
            }

            Debug.Print("Rx: " + str);
            return str;
        }
        bool TesterPresent(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("3E00");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x7E)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"TesterPresent Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"TesterPresent Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (TesterPresent(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {

            }
            return false;
        }
        bool CheckMemory(string code, bool writeMsg)
        {
            string area = string.Empty;

            switch (code)
            {
                case "00":
                    area = "CB";
                    break;
                case "01":
                    area = "ASW";
                    break;
                case "02":
                    area = "DS 0";
                    break;
                case "03":
                    area = "DS 1";
                    break;
            }
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"3101020201{code}");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x71)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"CheckMemory_{area} Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"CheckMemory_{area} Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (CheckMemory(code, false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {

            }
            return false;
        }

        bool RequestTransferExit(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("37");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x77)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"RequestTransferExit Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"RequestTransferExit Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (RequestTransferExit(false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {

            }
            return false;
        }
        bool TransferData(ExtractedHexFileData hexdata, bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg(hexdata.Data);
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x76)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"TransferData Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"TransferData Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (TransferData(hexdata, false))
                    {
                        return true;
                    }
                }
                else
                {

                }
                return false;
            }

            return false;
        }

        bool RequestDownload(MemorySegment msl, string areaIndex, bool writeMsg)
        {
            string area = string.Empty;

            switch (msl.PrgType)
            {
                case MEMORYPRG_TYPE.OFFLINE_DATA:
                    area = "CB";
                    break;
                case MEMORYPRG_TYPE.CODE:
                    area = "ASW";
                    break;
                case MEMORYPRG_TYPE.DATA:
                    area = "DS";
                    break;
            }

            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"340044{msl.Address.ToString("X8")}00{msl.Size.ToString("X6")}");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x74)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"RequestDownload_{area} {areaIndex} Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"RequestDownload_{area} {areaIndex} Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (RequestDownload(msl, areaIndex, false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
            }
            return false;
        }
        bool EraseMemory(string code, bool writeMsg) //code : 00=CB 01=ASW 02=DS
        {
            string area = string.Empty;

            switch (code)
            {
                case "00":
                    area = "CB";
                    break;
                case "01":
                    area = "ASW";
                    break;
                case "02":
                    area = "DS 0";
                    break;
                case "03":
                    area = "DS 1";
                    break;
            }

            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"3101FF0001{code}");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x71)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"EraseMemory_{area} Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print($"EraseMemory_{area} Fail. NRC : 0x{result[3].ToString("X2")}");

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (EraseMemory(code, false))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {

            }
            return false;

        }
        bool WriteDataByIdentifier_FlashDate(bool writeMsg)
        {
            byte[] result;
            byte[] ba;
            string hexString;


            ba = Encoding.Default.GetBytes(DateTime.Now.ToString("yyyyMMdd"));
            hexString = BitConverter.ToString(ba);
            hexString = hexString.Replace("-", "");

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"2EF199{hexString}");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x6E)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("WriteDataByIdentifier_FlashingDate Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("WriteDataByIdentifier_FlashingDate Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (WriteDataByIdentifier_FlashDate(false))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {

            }

            Thread.Sleep(100);

            return false;
        }

        bool WriteDataByIdentifier_EngineSerial(string engineNumber, bool writeMsg)
        {
            byte[] result;
            byte[] ba;
            string hexString;

            engineNumber = Main.resultData.engineNumber + "*";//*는 문자완료 구분용

            if (engineNumber.Length < 50)
            {
                int len = 50 - engineNumber.Length;
                for (int i = 0; i < len; i++)
                {
                    engineNumber = engineNumber + " ";
                }
            }
            else if (engineNumber.Length > 50)
            {
                engineNumber = engineNumber.Substring(0, 50);
            }
            ba = Encoding.Default.GetBytes(engineNumber);
            hexString = BitConverter.ToString(ba);
            hexString = hexString.Replace("-", "");

            if (writeMsg)
            {
                result = ISOTP_WriteMsg($"2EF197{hexString}");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x6E)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("WriteDataByIdentifier_EngineSerial Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("WriteDataByIdentifier_EngineSerial Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (WriteDataByIdentifier_EngineSerial(engineNumber, false))
                    {
                        return true;
                    }
                }

                return false;
            }

            Thread.Sleep(100);

            return false;
        }
        bool ECUReset_HardReset(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("1101");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }



            if (result[1] == 0x51)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("ECU Reset_Hard Reset Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("ECU Reset_Hard Reset Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (ECUReset_HardReset(false))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {

            }
            return false;
        }
        bool ECUReset_JumpToCB(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("1170");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }


            if (result[1] == 0x51)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("ECU Reset_Jump To CB Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("ECU Reset_Jump To CB Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (ECUReset_JumpToCB(false))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {

            }
            return false;
        }
        bool StartProgrammingSession(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("1002");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x50)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start Programming Session Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start Programming Session Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (StartProgrammingSession(false))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {

            }
            return false;
        }
        bool StartExtendedDiagnosticSession(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("1003");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }

            if (result[1] == 0x50)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start ExtendedDiagnostic Session Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Start ExtendedDiagnostic Session Fail. NRC : 0x" + result[3].ToString("X2"));

                if (result[3] == 0x78)
                {
                    Debug.Print("Request Pending..");
                    Thread.Sleep(1);

                    if (StartExtendedDiagnosticSession(false))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {

            }
            return false;
        }
        bool SecurityAccess(bool writeMsg)
        {
            byte[] result;

            if (writeMsg)
            {
                result = ISOTP_WriteMsg("2709");
            }
            else
            {
                result = ISOTP_JustReadMsg();
            }
            if (result[1] == 0x67)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Security Access _ Make Seed Success");
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Security Access _ Make Seed Fail. NRC : 0x" + result[3].ToString("X2"));
                return false;
            }
            else
            {
                if (result[0] == 0x00)
                {
                    Debug.Print("Security Access _ ECU Connection Fail");
                    return false;
                }
                else
                {
                    result = null;
                    SecurityAccess(false);
                    return true;
                }
            }
            //3,4,5,6이 Seed

            byte[] seed = new byte[4];

            for (int i = 0; i < seed.Length; i++)
            {
                seed[i] = result[i + 3];
            }
            string strSeed = KWP2000.ByteArrayToString(seed);
            string strKey2 = MakeSecurityKeyForEngine("0A" + strSeed);

            result = ISOTP_WriteMsg($"270A{strKey2}");

            if (result[1] == 0x67)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Security Access _ Validate Key Success");
                return true;
            }
            else if (result[1] == 0x7F)
            {
                log.AppendLine(PrintResult(result));
                Debug.Print("Security Access _ Validate Key Fail. NRC : 0x" + result[3].ToString("X2"));
                return false;
            }

            return false;
        }
        private string MakeSecurityKeyForEngine(string strseed)
        {
            byte[] seed = KWP2000.StringToByteArray(strseed);

            byte[] localseed = new byte[5] { 0, 0, 0, 0, 0 };
            DWORD localseed_wort;
            byte[] key = new byte[4] { 0, 0, 0, 0 };


            const DWORD mask = 0x2B59374B;

            DWORD masktemp = mask;


            if (seed[1] == 0 && seed[2] == 0)
            {
                return null;
            }
            else
            {
                var a = (DWORD)seed[1];
                var b = (DWORD)seed[1] << 24;
                localseed_wort = ((DWORD)seed[1] << 24) + ((DWORD)seed[2] << 16) +
            ((DWORD)seed[3] << 8) + (DWORD)seed[4];

                for (int i = 0; i < 35; i++)
                {
                    uint condition = localseed_wort & 0x80000000;
                    if (condition > 1)
                    {
                        localseed_wort = localseed_wort << 1;

                        localseed_wort = localseed_wort ^ masktemp;
                    }
                    else
                    {
                        localseed_wort = localseed_wort << 1;
                    }
                }
                localseed = BitConverter.GetBytes(localseed_wort);

                for (int i = 0; i < 4; i++)
                {
                    key[3 - i] = localseed[i];
                }
            }

            return KWP2000.ByteArrayToString(key);
        }
        public byte[] ISOTP_JustReadMsg()
        {
            byte[] result = null;
            result = can.ReadMessagesWithReturn();
            return result;
        }
        public byte[] ISOTP_WriteMsg(string cmd)
        {
            int cmdLength = cmd.Length / 2;
            byte[] result = null;

            if (cmd.Substring(0, 2) == "30" && cmd.Length == 6)
            {
                result = can.WriteMsgWithReturn(cmd + "AAAAAAAAAA");
                Debug.Print("Tx : 300000");
                return result;
            }

            if (cmdLength <= 7)// 1 line CMD
            {
                string emptystring = string.Empty;
                int emptyLength = 8;
                emptyLength = emptyLength - 1 - cmdLength;

                for (int i = 0; i < emptyLength; i++)
                {
                    emptystring += "CC";
                }
                string msg = $"0{cmdLength}{cmd}{emptystring}";
                Debug.Print("Tx: " + msg);
                log.AppendLine("Tx: " + msg);
                result = can.WriteMsgWithReturn(msg);

                return result;
            }
            else if (cmdLength > 7 && cmdLength <= 4095) // 2 line CMD 이상. TransferData는 아닌 경우.
            {
                string firstCmd = cmd.Substring(0, 12);
                string secondCmd = cmd.Substring(12, cmd.Length - 12);
                string hexCmdLength = cmdLength.ToString("X3");

                string msg = $"1{hexCmdLength}{firstCmd}";
                log.AppendLine("Tx: " + msg);
                Debug.Print("Tx: " + msg);
                result = can.WriteMsgWithReturn(msg);
                log.AppendLine("Rx : " + PrintResult(result));
                if (result[0] != 0x30)//Positive Response
                {
                    return result;
                }
                int secondCmdLength = secondCmd.Length / 2;
                byte sequence = 0;
                string data = string.Empty;
                while (secondCmdLength != 0)
                {

                    sequence++;
                    if (sequence > 15)
                    {
                        sequence = 0;
                    }

                    if (secondCmdLength > 7)
                    {

                        data = secondCmd.Substring(0, 7 * 2);
                        secondCmd = secondCmd.Substring(7 * 2, secondCmd.Length - 7 * 2);
                        secondCmdLength = secondCmd.Length / 2;
                        msg = $"2{sequence.ToString("X1")}{data}";
                        log.AppendLine("Tx: " + msg);
                        can.WriteMsgWithoutReturn(msg);
                        Debug.Print("Tx: " + msg);
                    }
                    else
                    {

                        string emptystring = string.Empty;
                        int emptyLength = 8;

                        emptyLength = emptyLength - 1 - secondCmdLength; // ISOTP에 해당하는건 포함하면 안됨. 그 해당하는 줄만 빼면되니까 21. -1

                        for (int i = 0; i < emptyLength; i++)
                        {
                            emptystring += "AA";
                        }

                        msg = $"2{sequence.ToString("X1")}{secondCmd}{emptystring}";
                        log.AppendLine("Tx: " + msg);
                        result = can.WriteMsgWithReturn(msg);
                        Debug.Print("Tx: " + msg);
                        return result;
                    }
                }
            }
            else if (cmdLength > 4095)//Transfer Data
            {
                string substring_CMD = string.Empty;
                int substringLength = 0;
                byte blockSequence = 0;
                cmdLength = cmd.Length / 2; //cmdLength = 실제 바이트 수, cmd.Length = 텍스트파일 상에서 1바이트는 2개의 텍스트로 구성됨. 실제 텍스트의 수. 바이트 수는 논리연산에 필요하고 실제 텍스트의 수는 스트링 처리에 필요

                int maxNum = 4093; //한 번에 보낼 수 있는 바이트 수. 프로토콜 상 4095가 최대. 그런데 기본적으로 36 + block sequence가 포함되기에 2 빼준다. 

                while (cmdLength != 0)
                {
                    if (cmdLength >= maxNum)
                    {
                        substring_CMD = cmd.Substring(0, maxNum * 2);//원하는만큼 자르고
                        substringLength = maxNum;
                        cmd = cmd.Substring(maxNum * 2, cmd.Length - (maxNum * 2)); //잘라낸 만큼 앞당김
                        cmdLength = cmd.Length / 2;  //cmdLength의 수로 남은 hex 데이터의 수를 체크함
                    }
                    else//남은 cmd의 수가 프로토콜로 보낼 수 있는 최대값보다 낮은 경우
                    {
                        substring_CMD = cmd;
                        substringLength = cmdLength;
                        cmdLength = 0;
                    }

                    int firstCmdDataLen = 0;

                    //if (cmdLength >= 4)//혹시 남은 렝스가 4이하여서 Consecontive frame 안 쓰게 될 경우 에러 방지용인데 이 경우 어떻게 처리할지 모르겠다. 발생하지 않을 것 같긴 함.
                    //{
                    //    firstCmdDataLen = 8;
                    //}
                    //else
                    //{
                    //    firstCmdDataLen = substringLength * 2;
                    //}

                    firstCmdDataLen = 8;

                    //First frame
                    blockSequence++;

                    int sendedSize = 0;
                    string firstCmd = substring_CMD.Substring(0, firstCmdDataLen);
                    string hexCmdLength = (substringLength + 2).ToString("X3");// +2하는 이유는 36{blockSequence}길이만큼 더해줌.

                    string msg = $"1{hexCmdLength}36{blockSequence.ToString("X2")}{firstCmd}";
                    log.AppendLine("Tx: " + msg);
                    result = can.WriteMsgWithReturn(msg);
                    sendedSize += 4;

                    if (result[0] != 0x30)
                    {
                        return result;
                    }

                    //Consecutive frame
                    int dataLenOn1Frame = 7;
                    byte sequence = 0;
                    string data = string.Empty;

                    if (substringLength < maxNum)
                    {
                        maxNum = substringLength;
                    }


                    while (sendedSize != maxNum) //기본적으로 First frame에서 데이터가 4개가 추가돼서 sendedSize에 4가 추가되는데 substringLength가 4보다 작은 경우 문제가 발생할거임.
                    {
                        sequence++;
                        if (sequence > 15)
                        {
                            sequence = 0;
                        }

                        if (maxNum - sendedSize <= 7)//4095 중 마지막
                        {
                            string empty = string.Empty;
                            dataLenOn1Frame = maxNum - sendedSize;

                            for (int i = 0; i < (7 - dataLenOn1Frame); i++)
                            {
                                empty += "CC";
                            }

                            data = substring_CMD.Substring(sendedSize * 2, dataLenOn1Frame * 2);
                            msg = $"2{sequence.ToString("X1")}{data}{empty}";
                            log.AppendLine("Tx: " + msg);
                            result = can.WriteMsgWithReturn(msg);

                            sendedSize += dataLenOn1Frame;

                            progressbarValue += sendedSize;
                            Debug.Print(progressbarValue.ToString() + "/" + progressbarMaximum.ToString());
                            if (result[1] == 0x76)
                            {
                                break;
                            }
                            else
                            {
                                return result;
                            }

                        }

                        data = substring_CMD.Substring(sendedSize * 2, dataLenOn1Frame * 2);
                        msg = $"2{sequence.ToString("X1")}{data}";
                        log.AppendLine("Tx: " + msg);
                        can.WriteMsgWithoutReturn(msg);

                        sendedSize += dataLenOn1Frame;
                        
                    }



                }
            }

            return result;

        }


    }
}

