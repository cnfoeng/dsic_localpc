﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Peak.Can.Basic;
using TPCANHandle = System.UInt16;
using TPCANTimestampFD = System.UInt64;
using System.Diagnostics;
using System.Threading;

namespace DSIC_Local.CAN
{
    public class CANCom
    {
        public CANStatus status = CANStatus.Normal;
        public Thread thGetData;
        public bool isConnected = false;
        public enum CANStatus
        {
            Normal,
            Flash,
            None,
        }
        public bool Import_Variables(string path)
        {
            VariableInfo varItem;
            int Index = 0;

            string[] Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            if (Main.project.sysVarInfo.EngineCom_Variables != null)
            {
                Main.project.sysVarInfo.EngineCom_Variables.Clear();
            }
            else
            {
                Main.project.sysVarInfo.EngineCom_Variables = new List<VariableInfo>();
            }

            if (varlist != null)
            {
                try
                {
                    foreach (string CAN_Data in varlist)
                    {
                        string[] arrCAN_Data = CAN_Data.Split(',');
                        string variableName = arrCAN_Data[1];
                        string norminalName = arrCAN_Data[2];
                        string unit = arrCAN_Data[3];
                        bool use_Alarm = (arrCAN_Data[4] == "TRUE") ? true : false;
                        int alarm_Low = int.Parse(arrCAN_Data[5]);
                        int alarm_High = int.Parse(arrCAN_Data[6]);
                        bool use_Warning = (arrCAN_Data[7] == "TRUE") ? true : false;
                        int warning_Low = int.Parse(arrCAN_Data[8]);
                        int warning_High = int.Parse(arrCAN_Data[9]);
                        bool use_Averaging = (arrCAN_Data[10] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrCAN_Data[11]);
                        int round = int.Parse(arrCAN_Data[12]);
                        string formula = arrCAN_Data[13];
                        string LID = arrCAN_Data[14];

                        varItem = new VariableInfo(variableName, norminalName);

                        varItem.CalType = CalibrationType.CalByValue;
                        varItem.Unit = unit;
                        varItem.AlarmUse = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.WarningUse = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.Formula = formula;
                        varItem.NorminalDescription = LID;
                        varItem.Index = Index;

                        Main.project.sysVarInfo.EngineCom_Variables.Add(varItem);
                        Index++;
                    }
                }
                catch (Exception)
                {
                    Debug.Print("CAN Engine File Import 에러");
                    return false;
                }
            }

            return true;

        }
        private class MessageStatus
        {
            private TPCANMsgFD m_Msg;
            private TPCANTimestampFD m_TimeStamp;
            private TPCANTimestampFD m_oldTimeStamp;
            private int m_iIndex;
            private int m_Count;
            private bool m_bShowPeriod;
            private bool m_bWasChanged;

            public MessageStatus(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp, int listIndex)
            {
                m_Msg = canMsg;
                m_TimeStamp = canTimestamp;
                m_oldTimeStamp = canTimestamp;
                m_iIndex = listIndex;
                m_Count = 1;
                m_bShowPeriod = true;
                m_bWasChanged = false;
            }

            public void Update(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp)
            {
                m_Msg = canMsg;
                m_oldTimeStamp = m_TimeStamp;
                m_TimeStamp = canTimestamp;
                m_bWasChanged = true;
                m_Count += 1;
            }

            public TPCANMsgFD CANMsg
            {
                get { return m_Msg; }
            }

            public TPCANTimestampFD Timestamp
            {
                get { return m_TimeStamp; }
            }

            public int Position
            {
                get { return m_iIndex; }
            }

            public string TypeString
            {
                get { return GetMsgTypeString(); }
            }

            public string IdString
            {
                get { return GetIdString(); }
            }

            public string DataString
            {
                get { return GetDataString(); }
            }

            public int Count
            {
                get { return m_Count; }
            }

            public bool ShowingPeriod
            {
                get { return m_bShowPeriod; }
                set
                {
                    if (m_bShowPeriod ^ value)//xor연산자. 같으면 0, 다르면 1.
                    {
                        m_bShowPeriod = value;
                        m_bWasChanged = true;
                    }
                }
            }

            public bool MarkedAsUpdated
            {
                get { return m_bWasChanged; }
                set { m_bWasChanged = value; }
            }

            public string TimeString
            {
                get { return GetTimeString(); }
            }

            private string GetTimeString()
            {
                double fTime;

                fTime = (m_TimeStamp / 1000.0);
                if (m_bShowPeriod)
                    fTime -= (m_oldTimeStamp / 1000.0);
                return fTime.ToString("F1");
            }

            private string GetDataString()
            {
                string strTemp;

                strTemp = "";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    return "Remote Request";
                else
                    for (int i = 0; i < GetLengthFromDLC(m_Msg.DLC, (m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == 0); i++)
                        strTemp += string.Format("{0:X2} ", m_Msg.DATA[i]);

                return strTemp;
            }

            private string GetIdString()
            {
                // We format the ID of the message and show it
                //
                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)////ㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁ
                    return string.Format("{0:X8}h", m_Msg.ID);
                else
                    return string.Format("{0:X3}h", m_Msg.ID);
            }

            private string GetMsgTypeString()
            {
                string strTemp;

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_STATUS) == TPCANMessageType.PCAN_MESSAGE_STATUS)
                    return "STATUS";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    strTemp = "EXT";
                else
                    strTemp = "STD";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    strTemp += "/RTR";
                else
                    if ((int)m_Msg.MSGTYPE > (int)TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                {
                    strTemp += " [ ";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == TPCANMessageType.PCAN_MESSAGE_FD)
                        strTemp += " FD";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_BRS) == TPCANMessageType.PCAN_MESSAGE_BRS)
                        strTemp += " BRS";
                    if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_ESI) == TPCANMessageType.PCAN_MESSAGE_ESI)
                        strTemp += " ESI";
                    strTemp += " ]";
                }

                return strTemp;
            }

        }
        public static int GetLengthFromDLC(int dlc, bool isSTD)// IF문과 SWITCH문으로 DLC 값에 따라 리턴값 경우의 수를 만든다. (DLC 값을 실제 문자 길이로 리턴한다. DLC[Data Length Code] 값 -> 실제 Data의 길이). 표참고
        {
            if (dlc <= 8)
                return dlc;

            if (isSTD)
                return 8;

            switch (dlc)
            {
                case 9: return 12;
                case 10: return 16;
                case 11: return 20;
                case 12: return 24;
                case 13: return 32;
                case 14: return 48;
                case 15: return 64;
                default: return dlc;
            }
        }

        System.Timers.Timer tmrRead = new System.Timers.Timer();
       


        private delegate void ReadDelegateHandler();
        /// <summary>
        /// Saves the handle of a PCAN hardware
        /// </summary>
        public TPCANHandle m_PcanHandle;
        /// <summary>
        /// Saves the baudrate register for a conenction
        /// </summary>
        private TPCANBaudrate m_Baudrate;
        /// <summary>
        /// Saves the type of a non-plug-and-play hardware
        /// </summary>
        private TPCANType m_HwType;
        /// <summary>
        /// Stores the status of received messages for its display
        /// </summary>
        private System.Collections.ArrayList m_LastMsgsList;
        /// <summary>
        /// Read Delegate for calling the function "ReadMessages"
        /// </summary>
        private ReadDelegateHandler m_ReadDelegate;
        /// <summary>
        /// Receive-Event
        /// </summary>
        private System.Threading.AutoResetEvent m_ReceiveEvent;
        /// <summary>
        /// Thread for message reading (using events)
        /// </summary>
        private System.Threading.Thread m_ReadThread;
        public CANCom()
        {
            InitializeBasicComponents();
        }
        private void InitializeBasicComponents()
        {
            // Creates the list for received messages
            //
            m_LastMsgsList = new System.Collections.ArrayList();
            // Creates the delegate used for message reading  ; 콜백으로 알아서 읽게 만듦. ReadDelegateHandler를 Delegate에 등록하여 사용.
            //
            m_ReadDelegate = new ReadDelegateHandler(ReadMessages);
            // Creates the event used for signalize incomming messages ; 사전 콜백 메시지에 사용되는 이벤트를 생성합니다.
            //
            m_ReceiveEvent = new System.Threading.AutoResetEvent(false);

            tmrRead.Interval = 50;
        }
        public void StartCANCom()
        {
            TPCANStatus stsResult;

            m_PcanHandle = Convert.ToUInt16("51", 16); //USB1
            m_Baudrate = TPCANBaudrate.PCAN_BAUD_1M;
            m_HwType = TPCANType.PCAN_TYPE_ISA;

            stsResult = PCANBasic.Initialize(
                    m_PcanHandle,
                    m_Baudrate,
                    m_HwType,
                    Convert.ToUInt32("0100", 16),
                    Convert.ToUInt16(3));


            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                if (stsResult != TPCANStatus.PCAN_ERROR_CAUTION)
                {
                    Debug.Print(GetFormatedError(stsResult));
                    Main.AddLog("CAN 장비 연결 실패", Log.LogType.Error);

                    return;
                }
                else
                {
                    stsResult = TPCANStatus.PCAN_ERROR_OK;
                }
            }

            Main.AddLog("CAN 장비 연결 성공", Log.LogType.OK);

            isConnected = true;
        }

        public bool StartGetData()
        {

            if (Main.isotp_uds.StartDefaultSession(true))
            {
                Main.AddLog("CAN 엔진 통신 성공", Log.LogType.OK);
                Main.isotp_uds.isConnected = true;

                Main.isotp_uds.ReadMapVersion(true);
                Main.isotp_uds.ReadFlashDate(true);
                Main.isotp_uds.ReadEngineSerial(true);

                thGetData = new Thread(new ThreadStart(Main.isotp_uds.Run));
                thGetData.Start();

                return true;
            }
            else
            {
                Main.AddLog("CAN 엔진 통신 실패", Log.LogType.Error);
                Main.isotp_uds.isConnected = false;
                return false;
            }
        }

        public void StopGetData()
        {
            status = CANStatus.None;
            thGetData?.Abort();
        }

        byte[] returned;


        public byte[] WriteMsgWhileReturn(string hex)
        {
            TPCANStatus stsResult;

            // Send the message
            //
            stsResult = WriteFrame(hex);

            // The message was successfully sent
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
            {
                //Msg Send OK
            }
            // An error occurred.  We show the error.
            //			
            else
            {
                Debug.Print(GetFormatedError(stsResult));
            }


            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;

                //} while (isRun && (Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));
            } while (isConnected);

            string str = string.Empty;

            for (int i = 0; i < returned.Length; i++)
            {
                str += returned[i].ToString("X2");
            }
            //Debug.Print(str);
            return returned;
        }

        public byte[] WriteMsgWithReturn(string hex)
        {
            TPCANStatus stsResult;

            // Send the message
            //
            stsResult = WriteFrame(hex);
            System.Threading.Thread.Sleep(50);
            // The message was successfully sent
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
            {
                //Msg Send OK
            }
            // An error occurred.  We show the error.
            //			
            else
            {
                Debug.Print(GetFormatedError(stsResult));
            }


            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //

            Stopwatch sw = new Stopwatch();
            sw.Start();
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;


              
            } while (isConnected && (Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)) && sw.ElapsedMilliseconds <300);
            sw.Stop();
            //string str = string.Empty;

            //for (int i = 0; i < returned.Length; i++)
            //{
            //    str += returned[i].ToString("X2");
            //}
            //Debug.Print(str);
            return returned;
        }
        public byte[] WriteMsgWithReturnNoDelay(string hex)
        {
            TPCANStatus stsResult;

            // Send the message
            //
            stsResult = WriteFrame(hex);
            // The message was successfully sent
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
            {
                //Msg Send OK
            }
            // An error occurred.  We show the error.
            //			
            else
            {
                Debug.Print(GetFormatedError(stsResult));
            }


            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;

            } while (isConnected && (Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));

            //string str = string.Empty;

            //for (int i = 0; i < returned.Length; i++)
            //{
            //    str += returned[i].ToString("X2");
            //}
            //Debug.Print(str);
            return returned;
        }
        public void WriteMsgWithoutReturn(string hex)
        {
            TPCANStatus stsResult;

            // Send the message
            //
            stsResult = WriteFrame(hex);

            // The message was successfully sent
            //
            if (stsResult == TPCANStatus.PCAN_ERROR_OK)
            {
                //Msg Send OK
            }
            // An error occurred.  We show the error.
            //			
            else
            {
                Debug.Print(GetFormatedError(stsResult));
            }

        }

        private TPCANStatus WriteFrame(string hex)
        {
            TPCANMsg CANMsg;

            // We create a TPCANMsg message structure 
            //
            CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];

            // We configurate the Message.  The ID,
            // Length of the Data, Message Type
            // and the data
            //
            CANMsg.ID = Convert.ToUInt32("7E0", 16); // Tool ID : 7E0 , DX22 ECU ID : 7E8
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;
            // If a remote frame will be sent, the data bytes are not important.
            //
            // We get so much data as the Len of the message
            //
            for (int i = 0; i < hex.Length / 2; i++)
            {
                CANMsg.DATA[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }


            // The message is sent to the configured hardware
            //
            return PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }

        private string GetFormatedError(TPCANStatus error)
        {
            StringBuilder strTemp;

            // Creates a buffer big enough for a error-text
            //
            strTemp = new StringBuilder(256);
            // Gets the text using the GetErrorText API function
            // If the function success, the translated error is returned. If it fails,
            // a text describing the current error is returned.
            //
            if (PCANBasic.GetErrorText(error, 0, strTemp) != TPCANStatus.PCAN_ERROR_OK)
                return string.Format("An error occurred. Error-code's text ({0:X}) couldn't be retrieved", error);
            else
                return strTemp.ToString();
        }
        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsgFD theMsg, TPCANTimestampFD itsTimeStamp)
        {
            // We search if a message (Same ID and Type) is 
            // already received or if this is a new message
            //
            lock (m_LastMsgsList.SyncRoot)
            {
                foreach (MessageStatus msg in m_LastMsgsList)
                {
                    if ((msg.CANMsg.ID == theMsg.ID) && (msg.CANMsg.MSGTYPE == theMsg.MSGTYPE))
                    {
                        // Modify the message and exit
                        //
                        msg.Update(theMsg, itsTimeStamp);
                        return;
                    }
                }

                // Message not found. It will created
                //
                //InsertMsgEntry(theMsg, itsTimeStamp);
            }
        }

        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsg theMsg, TPCANTimestamp itsTimeStamp)
        {
            TPCANMsgFD newMsg;
            TPCANTimestampFD newTimestamp;

            newMsg = new TPCANMsgFD();
            newMsg.DATA = new byte[8];
            newMsg.ID = theMsg.ID;
            newMsg.DLC = theMsg.LEN;
            for (int i = 0; i < ((theMsg.LEN > 8) ? 8 : theMsg.LEN); i++)
                newMsg.DATA[i] = theMsg.DATA[i];
            newMsg.MSGTYPE = theMsg.MSGTYPE;

            newTimestamp = Convert.ToUInt64(itsTimeStamp.micros + 1000 * itsTimeStamp.millis + 0x100000000 * 1000 * itsTimeStamp.millis_overflow);
            ProcessMessage(newMsg, newTimestamp);

            returned = newMsg.DATA;
        }
        /// <summary>
        /// Function for reading CAN messages on normal CAN devices
        /// </summary>
        /// <returns>A TPCANStatus error code</returns>
        private TPCANStatus ReadMessage()
        {
            TPCANMsg CANMsg;
            TPCANTimestamp CANTimeStamp;
            TPCANStatus stsResult;

            // We execute the "Read" function of the PCANBasic                
            //
            stsResult = PCANBasic.Read(m_PcanHandle, out CANMsg, out CANTimeStamp);
            if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                // We process the received message
                //
                ProcessMessage(CANMsg, CANTimeStamp);

            string str = string.Empty;

            //for (int i = 0; i < CANMsg.DATA.Length; i++)
            //{
            //    str += CANMsg.DATA[i].ToString("X2");
            //}
            //Debug.Print(str);

            return stsResult;
        }
        /// <summary>
        /// Function for reading PCAN-Basic messages
        /// </summary>
        public void ReadMessages()
        {
            TPCANStatus stsResult;

            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;
            } while (isConnected && (!Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));
        }
        public byte[] ReadMessagesWithReturn()
        {
            TPCANStatus stsResult;

            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;
            } while (isConnected && (!Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));

            return returned;
        }
        public void StopCom()
        {
            isConnected = false;

            StopGetData();
            // Releases a current connected PCAN-Basic channel
            //
            PCANBasic.Reset(m_PcanHandle);
            PCANBasic.Uninitialize(m_PcanHandle);

        }

    }
}
