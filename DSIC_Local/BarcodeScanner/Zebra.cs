﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CoreScanner;

namespace DSIC_Local.BarcodeScanner
{
    class Zebra
    {
        CCoreScannerClass cCoreScannerClass;

        public Zebra()
        {
            try
            {
                //Instantiate CoreScanner Class
                cCoreScannerClass = new CCoreScannerClass();
                //Call Open API
                short[] scannerTypes = new short[1];//Scanner Types you are interested in
                scannerTypes[0] = 1; // 1 for all scanner types
                short numberOfScannerTypes = 1; // Size of the scannerTypes array
                int status; // Extended API return code
                cCoreScannerClass.Open(0, scannerTypes, numberOfScannerTypes, out status);
                // Subscribe for barcode events in cCoreScannerClass
                cCoreScannerClass.BarcodeEvent += new
                _ICoreScannerEvents_BarcodeEventEventHandler(OnBarcodeEvent);
                // Let's subscribe for events
                int opcode = 1001; // Method for Subscribe events
                string outXML; // XML Output
                string inXML = "<inArgs>" +
                "<cmdArgs>" +
                "<arg-int>1</arg-int>" + // Number of events you want to subscribe
                "<arg-int>1</arg-int>" + // Comma separated event IDs
                "</cmdArgs>" +
                "</inArgs>";
                cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
                Console.WriteLine(outXML);
            }
            catch (Exception exp)
            {
                Console.WriteLine("Barcode has something wrong. please check... " + exp.Message);
            }
        }

        void OnBarcodeEvent(short eventType, ref string pscanData)
        {
            string barcode = pscanData;
            string[] splitString = new string[2] { "<datalabel>", "</datalabel>" };
            string DataLabelHex = (barcode.Split(splitString,StringSplitOptions.RemoveEmptyEntries))[1];

            DataLabelHex = (DataLabelHex.Replace("0x","")).Replace(" ","-");
			
            string DataLabelString = Encoding.ASCII.GetString(FromHex(DataLabelHex));

            Main.barcode = DataLabelString;
            Main.bReadBarcodeEvent = true;
        }

        public static byte[] FromHex(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return raw;
        }
    }
}
