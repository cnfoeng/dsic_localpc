﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSIC_Local.Monitoring.Controls
{
    public partial class Digital : UserControl
    {
        public string unit;
        public string value;
        MonitorItem searchedMI = new MonitorItem();

        public Digital(MonitorItem searchedMI)
        {
            InitializeComponent();
            ChangeLocationAndSize();

            if (searchedMI == null)
            {
                return;
            }

            this.searchedMI = searchedMI;

            timerUpdateValue.Enabled = true;

            if (searchedMI.si.assignedVar == null)
            {
                return;
            }

            UpdateAppearance();
        }

        private void timerUpdateValue_Tick(object sender, EventArgs e)
        {
            if (searchedMI.si.assignedVar == null)
            {
                return;
            }
            if (searchedMI.Request_Update)
            {
                UpdateAppearance();
                searchedMI.Request_Update = false;
            }
            DetectWarningAlarm();
            UpdateValue();
        }

        private void Digital_SizeChanged(object sender, EventArgs e)
        {
            ChangeLocationAndSize();
        }

        private void ChangeLocationAndSize()
        {
            panelTop.Height = panelBack.Height / 2 - 10;

            int LocationX;
            int LocationY;

            LocationX = lbName.Location.X;
            LocationY = panelTop.Height / 2 - (lbName.Height / 2);
            lbName.Location = new Point(LocationX, LocationY);
            LocationX = panelTop.Width - lbUnit.Size.Width - 20;
            LocationY = panelTop.Height / 2 - (lbUnit.Height / 2);
            lbUnit.Location = new Point(LocationX, LocationY);
            LocationX = panelFill.Width - lbValue.Size.Width - 20;
            LocationY = panelFill.Height / 2 - (lbValue.Height / 2);
            lbValue.Location = new Point(LocationX, LocationY);
        }

        
        private void UpdateValue()
        {
            string formatString = "F" + searchedMI.si.assignedVar.Round.ToString();
            lbSizeCheck.Text = searchedMI.si.assignedVar.RealValue.ToString(formatString);

            int LocationX;
            int LocationY;

            LocationX = panelFill.Width - lbSizeCheck.Size.Width - 20;
            LocationY = panelFill.Height / 2 - (lbSizeCheck.Height / 2);

            lbValue.Location = new Point(LocationX, LocationY);
            lbValue.Text = searchedMI.si.assignedVar.RealValue.ToString(formatString);
        }
        private void DetectWarningAlarm()
        {
            if (searchedMI.si.assignedVar.alarmState == AlarmState.Normal)
            {
                panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }
            else if (searchedMI.si.assignedVar.alarmState == AlarmState.Warning)
            {
                if (panelFill.BackColor == searchedMI.si.BackgroundColor_Bottom)
                    panelFill.BackColor = Color.Yellow;
                else
                    panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }
            else if (searchedMI.si.assignedVar.alarmState == AlarmState.Alarm)
            {
                if (panelFill.BackColor == searchedMI.si.BackgroundColor_Bottom)
                    panelFill.BackColor = Color.Red;
                else
                    panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }
        }
        private void UpdateAppearance()
        {
            if (searchedMI.si.NameFontSize < 1)
            {
                searchedMI.si.NameFontSize = 1;
            }

            lbName.Font = new Font("Segoe UI", searchedMI.si.NameFontSize);
            lbName.ForeColor = searchedMI.si.NameColor;

            if (searchedMI.si.UnitFontSize < 1)
            {
                searchedMI.si.UnitFontSize = 1;
            }

            lbUnit.Font = new Font("Segoe UI", searchedMI.si.UnitFontSize);
            lbUnit.Visible = searchedMI.si.UnitVisible;
            lbUnit.ForeColor = searchedMI.si.UnitColor;

            //lbUnit.Size = lbName.Size;
            if (searchedMI.si.ValueFontSize < 1)
            {
                searchedMI.si.ValueFontSize = 1;

            }
            lbSizeCheck.Font = new Font("Segoe UI", searchedMI.si.ValueFontSize);

            lbValue.Font = new Font("Segoe UI", searchedMI.si.ValueFontSize);
            lbValue.ForeColor = searchedMI.si.ValueColor;

            ChangeLocationAndSize();
            
            panelTop.BackColor = searchedMI.si.BackgroundColor_Top;

           

            lbName.Text = searchedMI.si.assignedVar.VariableName;
            lbUnit.Text = searchedMI.si.assignedVar.Unit;
            
            string str = searchedMI.documentName;
            //try
            //{
            searchedMI.Document.AppearanceCaption.BackColor = searchedMI.si.DocumentColor;
            searchedMI.Document.Caption = searchedMI.si.DocumentCaption;
            searchedMI.Document.AppearanceCaption.ForeColor = searchedMI.si.DocumentCaptionColor;
            //}
            //catch (NullReferenceException)
            //{

            //}
        }

    }
}
