﻿namespace DSIC_Local.Monitoring.Controls
{
    partial class Digital
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerUpdateValue = new System.Windows.Forms.Timer(this.components);
            this.panelBack = new DevExpress.XtraEditors.PanelControl();
            this.panelFill = new DevExpress.XtraEditors.PanelControl();
            this.lbValue = new DevExpress.XtraEditors.LabelControl();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.lbName = new DevExpress.XtraEditors.LabelControl();
            this.lbUnit = new DevExpress.XtraEditors.LabelControl();
            this.lbSizeCheck = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).BeginInit();
            this.panelBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).BeginInit();
            this.panelFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerUpdateValue
            // 
            this.timerUpdateValue.Interval = 1000;
            this.timerUpdateValue.Tick += new System.EventHandler(this.timerUpdateValue_Tick);
            // 
            // panelBack
            // 
            this.panelBack.Controls.Add(this.panelFill);
            this.panelBack.Controls.Add(this.panelTop);
            this.panelBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBack.Location = new System.Drawing.Point(0, 0);
            this.panelBack.Name = "panelBack";
            this.panelBack.Size = new System.Drawing.Size(309, 134);
            this.panelBack.TabIndex = 0;
            // 
            // panelFill
            // 
            this.panelFill.Appearance.BackColor = System.Drawing.Color.DimGray;
            this.panelFill.Appearance.Options.UseBackColor = true;
            this.panelFill.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelFill.Controls.Add(this.lbSizeCheck);
            this.panelFill.Controls.Add(this.lbValue);
            this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFill.Location = new System.Drawing.Point(2, 69);
            this.panelFill.Name = "panelFill";
            this.panelFill.Size = new System.Drawing.Size(305, 63);
            this.panelFill.TabIndex = 3;
            // 
            // lbValue
            // 
            this.lbValue.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbValue.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lbValue.Appearance.Options.UseFont = true;
            this.lbValue.Appearance.Options.UseForeColor = true;
            this.lbValue.Appearance.Options.UseTextOptions = true;
            this.lbValue.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.lbValue.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lbValue.Location = new System.Drawing.Point(284, 9);
            this.lbValue.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbValue.Name = "lbValue";
            this.lbValue.Size = new System.Drawing.Size(12, 40);
            this.lbValue.TabIndex = 20;
            this.lbValue.Text = "-";
            // 
            // panelTop
            // 
            this.panelTop.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.panelTop.Appearance.Options.UseBackColor = true;
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.lbName);
            this.panelTop.Controls.Add(this.lbUnit);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(2, 2);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(305, 67);
            this.panelTop.TabIndex = 2;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Appearance.Options.UseFont = true;
            this.lbName.Appearance.Options.UseTextOptions = true;
            this.lbName.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.lbName.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lbName.Location = new System.Drawing.Point(17, 24);
            this.lbName.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(41, 20);
            this.lbName.TabIndex = 18;
            this.lbName.Text = "Name";
            // 
            // lbUnit
            // 
            this.lbUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbUnit.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnit.Appearance.Options.UseFont = true;
            this.lbUnit.Appearance.Options.UseTextOptions = true;
            this.lbUnit.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.lbUnit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbUnit.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lbUnit.Location = new System.Drawing.Point(249, 24);
            this.lbUnit.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbUnit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbUnit.Name = "lbUnit";
            this.lbUnit.Size = new System.Drawing.Size(29, 20);
            this.lbUnit.TabIndex = 19;
            this.lbUnit.Text = "Unit";
            // 
            // lbSizeCheck
            // 
            this.lbSizeCheck.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbSizeCheck.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSizeCheck.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lbSizeCheck.Appearance.Options.UseFont = true;
            this.lbSizeCheck.Appearance.Options.UseForeColor = true;
            this.lbSizeCheck.Appearance.Options.UseTextOptions = true;
            this.lbSizeCheck.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.lbSizeCheck.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbSizeCheck.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lbSizeCheck.Location = new System.Drawing.Point(284, 9);
            this.lbSizeCheck.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbSizeCheck.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSizeCheck.Name = "lbSizeCheck";
            this.lbSizeCheck.Size = new System.Drawing.Size(12, 40);
            this.lbSizeCheck.TabIndex = 21;
            this.lbSizeCheck.Text = "-";
            this.lbSizeCheck.Visible = false;
            // 
            // Digital
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelBack);
            this.Name = "Digital";
            this.Size = new System.Drawing.Size(309, 134);
            this.SizeChanged += new System.EventHandler(this.Digital_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).EndInit();
            this.panelBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).EndInit();
            this.panelFill.ResumeLayout(false);
            this.panelFill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerUpdateValue;
        private DevExpress.XtraEditors.PanelControl panelBack;
        private DevExpress.XtraEditors.PanelControl panelFill;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.LabelControl lbName;
        private DevExpress.XtraEditors.LabelControl lbUnit;
        private DevExpress.XtraEditors.LabelControl lbValue;
        private DevExpress.XtraEditors.LabelControl lbSizeCheck;
    }
}
