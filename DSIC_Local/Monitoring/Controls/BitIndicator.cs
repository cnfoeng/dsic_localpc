﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace DSIC_Local.Monitoring.Controls
{
    public partial class BitIndicator : UserControl
    {
        public string unit;
        public string value;

        MonitorItem searchedMI = new MonitorItem();

        public BitIndicator(MonitorItem searchedMI)
        {
            InitializeComponent();

            if (searchedMI == null)
            {
                return;
            }

            this.searchedMI = searchedMI;

            timerUpdateValue.Enabled = true;

            if (searchedMI.bi.assignedVar == null)
            {
                return;
            }

            UpdateAppearance();
        }

        private void timerUpdateValue_Tick(object sender, EventArgs e)
        {
            if (searchedMI.bi.assignedVar == null)
            {
                return;
            }
            if (searchedMI.Request_Update)
            {
                UpdateAppearance();
                searchedMI.Request_Update = false;
            }
            UpdateValue();
        }

        private void Digital_SizeChanged(object sender, EventArgs e)
        {
            ChangeLocationAndSize();
        }
        private void ChangeLocationAndSize()
        {
            panelTop.Height = panelBack.Height / 2 - 10;

            int LocationX;
            int LocationY;

            LocationX = lbName.Location.X;
            LocationY = panelTop.Height / 2 - (lbName.Height / 2);
            lbName.Location = new Point(LocationX, LocationY);

            pnBit8.Width = pnBit7.Width = pnBit6.Width = pnBit5.Width = pnBit4.Width = pnBit3.Width = pnBit2.Width = pnBit1.Width = panelFill.Width / 8;
        }
        private void UpdateValue()
        {
            if (searchedMI.si.assignedVar.RealValue == 0)
            {
                stateIndicatorComponent1.StateIndex = 1;
            }
            else if (searchedMI.si.assignedVar.RealValue == 1)
            {
                stateIndicatorComponent1.StateIndex = 3;
            }
            else
            {
                stateIndicatorComponent1.StateIndex = 2;
            }
        }
        private void UpdateAppearance()
        {
            if (searchedMI.si.NameFontSize < 1)
            {
                searchedMI.si.NameFontSize = 1;
            }

            lbName.Font = new Font("Segoe UI", searchedMI.si.NameFontSize);
            lbName.ForeColor = searchedMI.si.NameColor;

            ChangeLocationAndSize();

            panelTop.BackColor = searchedMI.si.BackgroundColor_Top;

            if (searchedMI.si.assignedVar.alarmState == AlarmState.Normal)
            {
                panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }
            else if (searchedMI.si.assignedVar.alarmState == AlarmState.Warning)
            {
                if (panelFill.BackColor == searchedMI.si.BackgroundColor_Bottom)
                    panelFill.BackColor = Color.Yellow;
                else
                    panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }
            else if (searchedMI.si.assignedVar.alarmState == AlarmState.Alarm)
            {
                if (panelFill.BackColor == searchedMI.si.BackgroundColor_Bottom)
                    panelFill.BackColor = Color.Red;
                else
                    panelFill.BackColor = searchedMI.si.BackgroundColor_Bottom;
            }

            lbName.Text = searchedMI.si.assignedVar.VariableName;

            string str = searchedMI.documentName;

            searchedMI.Document.AppearanceCaption.BackColor = searchedMI.si.DocumentColor;
            searchedMI.Document.Caption = searchedMI.si.DocumentCaption;
            searchedMI.Document.AppearanceCaption.ForeColor = searchedMI.si.DocumentCaptionColor;
        }
        
    }
}
