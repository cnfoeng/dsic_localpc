﻿namespace DSIC_Local.Monitoring.Controls
{
    partial class BitIndicator
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState1 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState2 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState3 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState4 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState5 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState6 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState7 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState8 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState9 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState10 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState11 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState12 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState13 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState14 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState15 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState16 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState17 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState18 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState19 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState20 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState21 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState22 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState23 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState24 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState25 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState26 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState27 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState28 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState29 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState30 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState31 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState32 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            this.timerUpdateValue = new System.Windows.Forms.Timer(this.components);
            this.panelBack = new DevExpress.XtraEditors.PanelControl();
            this.panelFill = new DevExpress.XtraEditors.PanelControl();
            this.pnBit1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl8 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge8 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent8 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit1 = new System.Windows.Forms.Label();
            this.pnBit2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl7 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge7 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent7 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit2 = new System.Windows.Forms.Label();
            this.pnBit3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl6 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge6 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent6 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit3 = new System.Windows.Forms.Label();
            this.pnBit4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl5 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge5 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent5 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit4 = new System.Windows.Forms.Label();
            this.pnBit5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl4 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge4 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent4 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit5 = new System.Windows.Forms.Label();
            this.pnBit6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl3 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge3 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent3 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit6 = new System.Windows.Forms.Label();
            this.pnBit7 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl2 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent2 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit7 = new System.Windows.Forms.Label();
            this.pnBit8 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lbBit8 = new System.Windows.Forms.Label();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.lbName = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).BeginInit();
            this.panelBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).BeginInit();
            this.panelFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit1)).BeginInit();
            this.pnBit1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit2)).BeginInit();
            this.pnBit2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit3)).BeginInit();
            this.pnBit3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit4)).BeginInit();
            this.pnBit4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit5)).BeginInit();
            this.pnBit5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit6)).BeginInit();
            this.pnBit6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit7)).BeginInit();
            this.pnBit7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBit8)).BeginInit();
            this.pnBit8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerUpdateValue
            // 
            this.timerUpdateValue.Interval = 1000;
            this.timerUpdateValue.Tick += new System.EventHandler(this.timerUpdateValue_Tick);
            // 
            // panelBack
            // 
            this.panelBack.Controls.Add(this.panelFill);
            this.panelBack.Controls.Add(this.panelTop);
            this.panelBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBack.Location = new System.Drawing.Point(0, 0);
            this.panelBack.Name = "panelBack";
            this.panelBack.Size = new System.Drawing.Size(309, 134);
            this.panelBack.TabIndex = 0;
            // 
            // panelFill
            // 
            this.panelFill.Appearance.BackColor = System.Drawing.Color.DimGray;
            this.panelFill.Appearance.Options.UseBackColor = true;
            this.panelFill.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelFill.Controls.Add(this.pnBit1);
            this.panelFill.Controls.Add(this.pnBit2);
            this.panelFill.Controls.Add(this.pnBit3);
            this.panelFill.Controls.Add(this.pnBit4);
            this.panelFill.Controls.Add(this.pnBit5);
            this.panelFill.Controls.Add(this.pnBit6);
            this.panelFill.Controls.Add(this.pnBit7);
            this.panelFill.Controls.Add(this.pnBit8);
            this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFill.Location = new System.Drawing.Point(2, 69);
            this.panelFill.Name = "panelFill";
            this.panelFill.Size = new System.Drawing.Size(305, 63);
            this.panelFill.TabIndex = 3;
            // 
            // pnBit1
            // 
            this.pnBit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit1.Controls.Add(this.panelControl16);
            this.pnBit1.Controls.Add(this.panelControl17);
            this.pnBit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit1.Location = new System.Drawing.Point(235, 0);
            this.pnBit1.Name = "pnBit1";
            this.pnBit1.Size = new System.Drawing.Size(34, 63);
            this.pnBit1.TabIndex = 10;
            // 
            // panelControl16
            // 
            this.panelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl16.Controls.Add(this.gaugeControl8);
            this.panelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl16.Location = new System.Drawing.Point(0, 0);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(34, 44);
            this.panelControl16.TabIndex = 2;
            // 
            // gaugeControl8
            // 
            this.gaugeControl8.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl8.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge8});
            this.gaugeControl8.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl8.Name = "gaugeControl8";
            this.gaugeControl8.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl8.TabIndex = 0;
            // 
            // stateIndicatorGauge8
            // 
            this.stateIndicatorGauge8.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge8.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent8});
            this.stateIndicatorGauge8.Name = "stateIndicatorGauge8";
            // 
            // stateIndicatorComponent8
            // 
            this.stateIndicatorComponent8.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent8.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent8.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent8.StateIndex = 0;
            indicatorState1.Name = "State1";
            indicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState2.Name = "State2";
            indicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState3.Name = "State3";
            indicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState4.Name = "State4";
            indicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent8.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState1,
            indicatorState2,
            indicatorState3,
            indicatorState4});
            // 
            // panelControl17
            // 
            this.panelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl17.Controls.Add(this.lbBit1);
            this.panelControl17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl17.Location = new System.Drawing.Point(0, 44);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(34, 19);
            this.panelControl17.TabIndex = 3;
            // 
            // lbBit1
            // 
            this.lbBit1.BackColor = System.Drawing.Color.Transparent;
            this.lbBit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit1.ForeColor = System.Drawing.Color.White;
            this.lbBit1.Location = new System.Drawing.Point(0, 0);
            this.lbBit1.Name = "lbBit1";
            this.lbBit1.Size = new System.Drawing.Size(34, 19);
            this.lbBit1.TabIndex = 1;
            this.lbBit1.Text = "1";
            this.lbBit1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit2
            // 
            this.pnBit2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit2.Controls.Add(this.panelControl14);
            this.pnBit2.Controls.Add(this.panelControl15);
            this.pnBit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit2.Location = new System.Drawing.Point(201, 0);
            this.pnBit2.Name = "pnBit2";
            this.pnBit2.Size = new System.Drawing.Size(34, 63);
            this.pnBit2.TabIndex = 9;
            // 
            // panelControl14
            // 
            this.panelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl14.Controls.Add(this.gaugeControl7);
            this.panelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl14.Location = new System.Drawing.Point(0, 0);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(34, 44);
            this.panelControl14.TabIndex = 2;
            // 
            // gaugeControl7
            // 
            this.gaugeControl7.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl7.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge7});
            this.gaugeControl7.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl7.Name = "gaugeControl7";
            this.gaugeControl7.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl7.TabIndex = 0;
            // 
            // stateIndicatorGauge7
            // 
            this.stateIndicatorGauge7.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge7.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent7});
            this.stateIndicatorGauge7.Name = "stateIndicatorGauge7";
            // 
            // stateIndicatorComponent7
            // 
            this.stateIndicatorComponent7.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent7.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent7.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent7.StateIndex = 0;
            indicatorState5.Name = "State1";
            indicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState6.Name = "State2";
            indicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState7.Name = "State3";
            indicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState8.Name = "State4";
            indicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent7.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState5,
            indicatorState6,
            indicatorState7,
            indicatorState8});
            // 
            // panelControl15
            // 
            this.panelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl15.Controls.Add(this.lbBit2);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl15.Location = new System.Drawing.Point(0, 44);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(34, 19);
            this.panelControl15.TabIndex = 3;
            // 
            // lbBit2
            // 
            this.lbBit2.BackColor = System.Drawing.Color.Transparent;
            this.lbBit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit2.ForeColor = System.Drawing.Color.White;
            this.lbBit2.Location = new System.Drawing.Point(0, 0);
            this.lbBit2.Name = "lbBit2";
            this.lbBit2.Size = new System.Drawing.Size(34, 19);
            this.lbBit2.TabIndex = 1;
            this.lbBit2.Text = "2";
            this.lbBit2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit3
            // 
            this.pnBit3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit3.Controls.Add(this.panelControl12);
            this.pnBit3.Controls.Add(this.panelControl13);
            this.pnBit3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit3.Location = new System.Drawing.Point(167, 0);
            this.pnBit3.Name = "pnBit3";
            this.pnBit3.Size = new System.Drawing.Size(34, 63);
            this.pnBit3.TabIndex = 8;
            // 
            // panelControl12
            // 
            this.panelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl12.Controls.Add(this.gaugeControl6);
            this.panelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl12.Location = new System.Drawing.Point(0, 0);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(34, 44);
            this.panelControl12.TabIndex = 2;
            // 
            // gaugeControl6
            // 
            this.gaugeControl6.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl6.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge6});
            this.gaugeControl6.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl6.Name = "gaugeControl6";
            this.gaugeControl6.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl6.TabIndex = 0;
            // 
            // stateIndicatorGauge6
            // 
            this.stateIndicatorGauge6.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge6.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent6});
            this.stateIndicatorGauge6.Name = "stateIndicatorGauge6";
            // 
            // stateIndicatorComponent6
            // 
            this.stateIndicatorComponent6.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent6.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent6.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent6.StateIndex = 0;
            indicatorState9.Name = "State1";
            indicatorState9.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState10.Name = "State2";
            indicatorState10.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState11.Name = "State3";
            indicatorState11.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState12.Name = "State4";
            indicatorState12.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent6.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState9,
            indicatorState10,
            indicatorState11,
            indicatorState12});
            // 
            // panelControl13
            // 
            this.panelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl13.Controls.Add(this.lbBit3);
            this.panelControl13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl13.Location = new System.Drawing.Point(0, 44);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(34, 19);
            this.panelControl13.TabIndex = 3;
            // 
            // lbBit3
            // 
            this.lbBit3.BackColor = System.Drawing.Color.Transparent;
            this.lbBit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit3.ForeColor = System.Drawing.Color.White;
            this.lbBit3.Location = new System.Drawing.Point(0, 0);
            this.lbBit3.Name = "lbBit3";
            this.lbBit3.Size = new System.Drawing.Size(34, 19);
            this.lbBit3.TabIndex = 1;
            this.lbBit3.Text = "3";
            this.lbBit3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit4
            // 
            this.pnBit4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit4.Controls.Add(this.panelControl10);
            this.pnBit4.Controls.Add(this.panelControl11);
            this.pnBit4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit4.Location = new System.Drawing.Point(133, 0);
            this.pnBit4.Name = "pnBit4";
            this.pnBit4.Size = new System.Drawing.Size(34, 63);
            this.pnBit4.TabIndex = 7;
            // 
            // panelControl10
            // 
            this.panelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl10.Controls.Add(this.gaugeControl5);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(34, 44);
            this.panelControl10.TabIndex = 2;
            // 
            // gaugeControl5
            // 
            this.gaugeControl5.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl5.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge5});
            this.gaugeControl5.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl5.Name = "gaugeControl5";
            this.gaugeControl5.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl5.TabIndex = 0;
            // 
            // stateIndicatorGauge5
            // 
            this.stateIndicatorGauge5.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge5.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent5});
            this.stateIndicatorGauge5.Name = "stateIndicatorGauge5";
            // 
            // stateIndicatorComponent5
            // 
            this.stateIndicatorComponent5.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent5.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent5.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent5.StateIndex = 0;
            indicatorState13.Name = "State1";
            indicatorState13.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState14.Name = "State2";
            indicatorState14.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState15.Name = "State3";
            indicatorState15.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState16.Name = "State4";
            indicatorState16.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent5.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState13,
            indicatorState14,
            indicatorState15,
            indicatorState16});
            // 
            // panelControl11
            // 
            this.panelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl11.Controls.Add(this.lbBit4);
            this.panelControl11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl11.Location = new System.Drawing.Point(0, 44);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(34, 19);
            this.panelControl11.TabIndex = 3;
            // 
            // lbBit4
            // 
            this.lbBit4.BackColor = System.Drawing.Color.Transparent;
            this.lbBit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit4.ForeColor = System.Drawing.Color.White;
            this.lbBit4.Location = new System.Drawing.Point(0, 0);
            this.lbBit4.Name = "lbBit4";
            this.lbBit4.Size = new System.Drawing.Size(34, 19);
            this.lbBit4.TabIndex = 1;
            this.lbBit4.Text = "4";
            this.lbBit4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit5
            // 
            this.pnBit5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit5.Controls.Add(this.panelControl8);
            this.pnBit5.Controls.Add(this.panelControl9);
            this.pnBit5.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit5.Location = new System.Drawing.Point(99, 0);
            this.pnBit5.Name = "pnBit5";
            this.pnBit5.Size = new System.Drawing.Size(34, 63);
            this.pnBit5.TabIndex = 6;
            // 
            // panelControl8
            // 
            this.panelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl8.Controls.Add(this.gaugeControl4);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(34, 44);
            this.panelControl8.TabIndex = 2;
            // 
            // gaugeControl4
            // 
            this.gaugeControl4.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl4.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge4});
            this.gaugeControl4.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl4.Name = "gaugeControl4";
            this.gaugeControl4.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl4.TabIndex = 0;
            // 
            // stateIndicatorGauge4
            // 
            this.stateIndicatorGauge4.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge4.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent4});
            this.stateIndicatorGauge4.Name = "stateIndicatorGauge4";
            // 
            // stateIndicatorComponent4
            // 
            this.stateIndicatorComponent4.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent4.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent4.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent4.StateIndex = 0;
            indicatorState17.Name = "State1";
            indicatorState17.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState18.Name = "State2";
            indicatorState18.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState19.Name = "State3";
            indicatorState19.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState20.Name = "State4";
            indicatorState20.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent4.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState17,
            indicatorState18,
            indicatorState19,
            indicatorState20});
            // 
            // panelControl9
            // 
            this.panelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl9.Controls.Add(this.lbBit5);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl9.Location = new System.Drawing.Point(0, 44);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(34, 19);
            this.panelControl9.TabIndex = 3;
            // 
            // lbBit5
            // 
            this.lbBit5.BackColor = System.Drawing.Color.Transparent;
            this.lbBit5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit5.ForeColor = System.Drawing.Color.White;
            this.lbBit5.Location = new System.Drawing.Point(0, 0);
            this.lbBit5.Name = "lbBit5";
            this.lbBit5.Size = new System.Drawing.Size(34, 19);
            this.lbBit5.TabIndex = 1;
            this.lbBit5.Text = "5";
            this.lbBit5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit6
            // 
            this.pnBit6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit6.Controls.Add(this.panelControl6);
            this.pnBit6.Controls.Add(this.panelControl7);
            this.pnBit6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit6.Location = new System.Drawing.Point(65, 0);
            this.pnBit6.Name = "pnBit6";
            this.pnBit6.Size = new System.Drawing.Size(34, 63);
            this.pnBit6.TabIndex = 5;
            // 
            // panelControl6
            // 
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Controls.Add(this.gaugeControl3);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(34, 44);
            this.panelControl6.TabIndex = 2;
            // 
            // gaugeControl3
            // 
            this.gaugeControl3.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl3.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge3});
            this.gaugeControl3.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl3.Name = "gaugeControl3";
            this.gaugeControl3.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl3.TabIndex = 0;
            // 
            // stateIndicatorGauge3
            // 
            this.stateIndicatorGauge3.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge3.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent3});
            this.stateIndicatorGauge3.Name = "stateIndicatorGauge3";
            // 
            // stateIndicatorComponent3
            // 
            this.stateIndicatorComponent3.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent3.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent3.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent3.StateIndex = 0;
            indicatorState21.Name = "State1";
            indicatorState21.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState22.Name = "State2";
            indicatorState22.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState23.Name = "State3";
            indicatorState23.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState24.Name = "State4";
            indicatorState24.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent3.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState21,
            indicatorState22,
            indicatorState23,
            indicatorState24});
            // 
            // panelControl7
            // 
            this.panelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl7.Controls.Add(this.lbBit6);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl7.Location = new System.Drawing.Point(0, 44);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(34, 19);
            this.panelControl7.TabIndex = 3;
            // 
            // lbBit6
            // 
            this.lbBit6.BackColor = System.Drawing.Color.Transparent;
            this.lbBit6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit6.ForeColor = System.Drawing.Color.White;
            this.lbBit6.Location = new System.Drawing.Point(0, 0);
            this.lbBit6.Name = "lbBit6";
            this.lbBit6.Size = new System.Drawing.Size(34, 19);
            this.lbBit6.TabIndex = 1;
            this.lbBit6.Text = "6";
            this.lbBit6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit7
            // 
            this.pnBit7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit7.Controls.Add(this.panelControl4);
            this.pnBit7.Controls.Add(this.panelControl5);
            this.pnBit7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit7.Location = new System.Drawing.Point(31, 0);
            this.pnBit7.Name = "pnBit7";
            this.pnBit7.Size = new System.Drawing.Size(34, 63);
            this.pnBit7.TabIndex = 4;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.gaugeControl2);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(34, 44);
            this.panelControl4.TabIndex = 2;
            // 
            // gaugeControl2
            // 
            this.gaugeControl2.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl2.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge2});
            this.gaugeControl2.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl2.Name = "gaugeControl2";
            this.gaugeControl2.Size = new System.Drawing.Size(34, 44);
            this.gaugeControl2.TabIndex = 0;
            // 
            // stateIndicatorGauge2
            // 
            this.stateIndicatorGauge2.Bounds = new System.Drawing.Rectangle(6, 6, 22, 32);
            this.stateIndicatorGauge2.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent2});
            this.stateIndicatorGauge2.Name = "stateIndicatorGauge2";
            // 
            // stateIndicatorComponent2
            // 
            this.stateIndicatorComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent2.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent2.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent2.StateIndex = 0;
            indicatorState25.Name = "State1";
            indicatorState25.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState26.Name = "State2";
            indicatorState26.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState27.Name = "State3";
            indicatorState27.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState28.Name = "State4";
            indicatorState28.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent2.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState25,
            indicatorState26,
            indicatorState27,
            indicatorState28});
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.lbBit7);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(0, 44);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(34, 19);
            this.panelControl5.TabIndex = 3;
            // 
            // lbBit7
            // 
            this.lbBit7.BackColor = System.Drawing.Color.Transparent;
            this.lbBit7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit7.ForeColor = System.Drawing.Color.White;
            this.lbBit7.Location = new System.Drawing.Point(0, 0);
            this.lbBit7.Name = "lbBit7";
            this.lbBit7.Size = new System.Drawing.Size(34, 19);
            this.lbBit7.TabIndex = 1;
            this.lbBit7.Text = "7";
            this.lbBit7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnBit8
            // 
            this.pnBit8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBit8.Controls.Add(this.panelControl2);
            this.pnBit8.Controls.Add(this.panelControl1);
            this.pnBit8.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBit8.Location = new System.Drawing.Point(0, 0);
            this.pnBit8.Name = "pnBit8";
            this.pnBit8.Size = new System.Drawing.Size(31, 63);
            this.pnBit8.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.gaugeControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(31, 44);
            this.panelControl2.TabIndex = 2;
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(0, 0);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(31, 44);
            this.gaugeControl1.TabIndex = 0;
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 20, 32);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 0;
            indicatorState29.Name = "State1";
            indicatorState29.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState30.Name = "State2";
            indicatorState30.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState31.Name = "State3";
            indicatorState31.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState32.Name = "State4";
            indicatorState32.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState29,
            indicatorState30,
            indicatorState31,
            indicatorState32});
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.lbBit8);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 44);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(31, 19);
            this.panelControl1.TabIndex = 3;
            // 
            // lbBit8
            // 
            this.lbBit8.BackColor = System.Drawing.Color.Transparent;
            this.lbBit8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBit8.ForeColor = System.Drawing.Color.White;
            this.lbBit8.Location = new System.Drawing.Point(0, 0);
            this.lbBit8.Name = "lbBit8";
            this.lbBit8.Size = new System.Drawing.Size(31, 19);
            this.lbBit8.TabIndex = 1;
            this.lbBit8.Text = "8";
            this.lbBit8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.panelTop.Appearance.Options.UseBackColor = true;
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.lbName);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(2, 2);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(305, 67);
            this.panelTop.TabIndex = 2;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Appearance.Options.UseFont = true;
            this.lbName.Appearance.Options.UseTextOptions = true;
            this.lbName.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.lbName.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lbName.Location = new System.Drawing.Point(17, 24);
            this.lbName.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lbName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(41, 20);
            this.lbName.TabIndex = 18;
            this.lbName.Text = "Name";
            // 
            // BitIndicator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelBack);
            this.Name = "BitIndicator";
            this.Size = new System.Drawing.Size(309, 134);
            this.SizeChanged += new System.EventHandler(this.Digital_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).EndInit();
            this.panelBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).EndInit();
            this.panelFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit1)).EndInit();
            this.pnBit1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit2)).EndInit();
            this.pnBit2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit3)).EndInit();
            this.pnBit3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit4)).EndInit();
            this.pnBit4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit5)).EndInit();
            this.pnBit5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit6)).EndInit();
            this.pnBit6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit7)).EndInit();
            this.pnBit7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBit8)).EndInit();
            this.pnBit8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerUpdateValue;
        private DevExpress.XtraEditors.PanelControl panelBack;
        private DevExpress.XtraEditors.PanelControl panelFill;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.LabelControl lbName;
        private DevExpress.XtraEditors.PanelControl pnBit8;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraEditors.PanelControl pnBit1;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl8;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge8;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent8;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private System.Windows.Forms.Label lbBit1;
        private DevExpress.XtraEditors.PanelControl pnBit2;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl7;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge7;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent7;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private System.Windows.Forms.Label lbBit2;
        private DevExpress.XtraEditors.PanelControl pnBit3;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl6;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge6;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent6;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private System.Windows.Forms.Label lbBit3;
        private DevExpress.XtraEditors.PanelControl pnBit4;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl5;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge5;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent5;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private System.Windows.Forms.Label lbBit4;
        private DevExpress.XtraEditors.PanelControl pnBit5;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent4;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.Label lbBit5;
        private DevExpress.XtraEditors.PanelControl pnBit6;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent3;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private System.Windows.Forms.Label lbBit6;
        private DevExpress.XtraEditors.PanelControl pnBit7;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl2;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge2;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent2;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private System.Windows.Forms.Label lbBit7;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label lbBit8;
    }
}
