﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Arction.WinForms.Charting;
using Arction.WinForms.Charting.Axes;
using Arction.WinForms.Charting.SeriesXY;

namespace DSIC_Local.Monitoring.Controls
{
    public partial class TrackingChart : UserControl
    {
        private double _previousX;
        public MonitorItem deepCopyMI;
        public MonitorItem originalMI;
        public TrackingChart(MonitorItem mi)
        {
            InitializeComponent();
            originalMI = mi;
            deepCopyMI = mi.Clone();
            CreateChart(lightningChartUltimate1, mi.ci.chartColorTheme);
        }

        private void CreateChart(LightningChartUltimate _chart, ChartColorTheme colorTheme)
        {
            //Create new _chart

            //Disable rendering, strongly recommended before updating chart properties
            _chart.BeginUpdate();

            switch (colorTheme)
            {
                case ChartColorTheme.Black:
                    _chart.ColorTheme = Arction.WinForms.Charting.ColorTheme.Dark;
                    _chart.ViewXY.GraphBackground.Color = Color.FromArgb(64, 64, 64);
                    _chart.ViewXY.GraphBackground.GradientColor = Color.Black;
                    _chart.ViewXY.GraphBackground.GradientFill = GradientFill.Linear;
                    _chart.ViewXY.XAxes[0].MajorGrid.Color = Color.DimGray;
                    if (_chart.ViewXY.YAxes.Count > 0)
                    {
                        _chart.ViewXY.YAxes[0].MajorGrid.Color = Color.Gray;
                    }
                    break;
                case ChartColorTheme.Silver:
                    _chart.ColorTheme = Arction.WinForms.Charting.ColorTheme.LightGray;
                    _chart.ViewXY.GraphBackground.Color = Color.GhostWhite;
                    _chart.ViewXY.GraphBackground.GradientColor = Color.LightGray;
                    _chart.ViewXY.GraphBackground.GradientFill = GradientFill.Cylindrical;
                    _chart.ViewXY.XAxes[0].MajorGrid.Color = Color.Silver;
                    if (_chart.ViewXY.YAxes.Count > 0)
                    {
                        _chart.ViewXY.YAxes[0].MajorGrid.Color = Color.Silver;
                    }
                    break;
                case ChartColorTheme.White:
                    _chart.ColorTheme = Arction.WinForms.Charting.ColorTheme.LightGray;
                    _chart.ViewXY.GraphBackground.Color = Color.FromArgb(254, 254, 254);
                    _chart.ViewXY.GraphBackground.GradientColor = Color.FromArgb(250, 250, 250);
                    _chart.ViewXY.GraphBackground.GradientFill = GradientFill.Cylindrical;
                    _chart.ViewXY.XAxes[0].MajorGrid.Color = Color.FromArgb(50, 0, 0, 0);
                    if (_chart.ViewXY.YAxes.Count > 0)
                    {
                        _chart.ViewXY.YAxes[0].MajorGrid.Color = Color.FromArgb(50, 0, 0, 0);
                    }
                    break;
            }

            _chart.ViewXY.XAxes[0].MajorGrid.Visible = originalMI.ci.GridLine_Visibility;

            _chart.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;
            _chart.ViewXY.ZoomPanOptions.RightToLeftZoomAction = RightToLeftZoomActionXY.Off;
            _chart.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.Pan;

            _chart.Background.Color = SystemColors.Control;
            _chart.Background.GradientFill = GradientFill.Solid;
            
            //Don't show legendbox
            _chart.ViewXY.LegendBoxes[0].Visible = originalMI.ci.LegendBoxVisibility;

            _chart.ViewXY.LegendBoxes[0].AllowMouseResize = false;
            _chart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.SegmentTopLeft;
            //Reduce memory usage and increase performance. Destroys out-scrolled data.
            _chart.ViewXY.DropOldSeriesData = originalMI.ci.DropOldData;

            _chart.Name = "Temperature measurement _chart";
            _chart.Title.Text = "Real-time Temperature Graph";
            _chart.Title.Visible = false;
            _chart.RenderOptions.FontsQuality = FontsRenderingQuality.High;
            _chart.ViewXY.AxisLayout.YAxisAutoPlacement = YAxisAutoPlacement.AllRight;

            //Configure x-axis
            AxisX xAxis = _chart.ViewXY.XAxes[0];
            xAxis.ValueType = AxisValueType.DateTime;
            xAxis.AxisColor = Color.DarkGray;
            xAxis.Title.Text = "Time";
            xAxis.Title.Color = Color.Black;
            xAxis.Title.Shadow.Style = TextShadowStyle.Off;
            xAxis.AutoFormatLabels = false;
            xAxis.LabelsColor = Color.Black;
            xAxis.LabelsTimeFormat = "dd/MM/yyyy\nHH:mm.ss";
            xAxis.LabelsAngle = 90;
            xAxis.ScaleNibs.Color = Color.Gray;
            xAxis.ScrollMode = XAxisScrollMode.Stepping;
            
            //Convert DateTime values to axis values
            DateTime now = DateTime.Now;
            double minX = xAxis.DateTimeToAxisValue(now);
            double maxX = xAxis.DateTimeToAxisValue(now) + 30;
            xAxis.SetRange(minX, maxX);

            List<AxisY> listAxisY = new List<AxisY>();
            _chart.ViewXY.YAxes.Clear();

            int index = 0;
            foreach (YAxis yAxis in originalMI.ci.yAxes)
            {
                int a = _chart.ViewXY.YAxes.Count;
                _chart.ViewXY.YAxes.Add(new AxisY());
                listAxisY.Add(MakeDefaultAxisY(_chart.ViewXY.YAxes[index], yAxis, originalMI.ci.GridLine_Visibility , index));
                index++;
            }

            _chart.ViewXY.PointLineSeries.Clear();

            for (int i = 0; i < listAxisY.Count; i++)
            {
                PointLineSeries series = new PointLineSeries(_chart.ViewXY, xAxis, listAxisY[i]);
                series.Title.Text = listAxisY[i].Title.Text;
                series.LineStyle.Color = listAxisY[i].Title.Color;
                series.MouseInteraction = false;
                _chart.ViewXY.PointLineSeries.Add(series);
            }

            //Allow chart rendering
            _chart.EndUpdate();
        }
        private AxisY MakeDefaultAxisY(AxisY yAxis, DSIC_Local.Monitoring.YAxis definedyAxis, bool bMajorGrid, int yAxisIndex)
        {
            yAxis.Title.Text = definedyAxis.Variable_Name + " / " + definedyAxis.Unit;
            yAxis.Title.Color = definedyAxis.Line_Color;
            yAxis.Title.Shadow.Style = TextShadowStyle.Off;
            yAxis.Units.Text = definedyAxis.Unit;
            yAxis.Units.Color = definedyAxis.Line_Color;
            yAxis.AxisColor = definedyAxis.Line_Color;
            yAxis.LabelsColor = definedyAxis.Line_Color;
            yAxis.SetRange(definedyAxis.MinRange, definedyAxis.MaxRange);
            yAxis.AllowAutoYFit = definedyAxis.Range_Auto;
            yAxis.ScaleNibs.Color = Color.Gray;
            if (yAxisIndex == 0)
            {
                yAxis.MajorGrid.Visible = bMajorGrid;
            }
            else
            {
                yAxis.MajorGrid.Visible = false;
            }
            return yAxis;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (originalMI.Request_Update)
            {
                UpdateChart(lightningChartUltimate1);
                originalMI.Request_Update = false;

                if (originalMI.ci.Request_YAxesUpdate)
                {
                    this.deepCopyMI = originalMI.Clone();
                    CreateChart(lightningChartUltimate1, originalMI.ci.chartColorTheme);
                    originalMI.ci.Request_YAxesUpdate = false;
                }
            }
            AddPoint(lightningChartUltimate1);
        }

        private void UpdateChart(LightningChartUltimate _chart)
        {

            _chart.ViewXY.XAxes[0].MajorGrid.Visible = originalMI.ci.GridLine_Visibility;
            if (_chart.ViewXY.YAxes.Count > 0)
            {
                _chart.ViewXY.YAxes[0].MajorGrid.Visible = originalMI.ci.GridLine_Visibility;
            }
            _chart.ViewXY.LegendBoxes[0].Visible = originalMI.ci.LegendBoxVisibility;
            _chart.ViewXY.DropOldSeriesData = originalMI.ci.DropOldData;

            deepCopyMI.Document.AppearanceCaption.BackColor = originalMI.ci.DocumentColor;
            deepCopyMI.Document.Caption = originalMI.ci.DocumentCaption;
            deepCopyMI.Document.AppearanceCaption.ForeColor = originalMI.ci.DocumentCaptionColor;
        }

        private void AddPoint(LightningChartUltimate _chart)
        {
            if (_chart == null)
                return;

            //Disable updates, to prevent several extra refreshes
            _chart.BeginUpdate();

            //Convert 'Now' to X value 
            _previousX = _chart.ViewXY.XAxes[0].DateTimeToAxisValue(DateTime.Now);

            for (int i = 0; i < deepCopyMI.ci.yAxes.Count; i++)
            {
                //Array for 1 point
                SeriesPoint[] points = new SeriesPoint[1];
               
                //Store the X value
                points[0].X = _previousX;
                //Randomize and store Y value 
                if (Main.project.sysVarInfo.FindVariable(deepCopyMI.ci.yAxes[i].Variable_Name) != null)
                {
                    points[0].Y = Main.project.sysVarInfo.FindVariable(deepCopyMI.ci.yAxes[i].Variable_Name).RealValue;
                    // Add the new point into end of first PointLineSeries
                    _chart.ViewXY.PointLineSeries[i].AddPoints(points, false);
                }
            }

            //Set real-time monitoring scroll position, to latest X point. 
            //ScrollPosition indicates the position where monitoring is currently progressing. 
            if (deepCopyMI.ci.XAxis_AutoScroll)
            {
                _chart.ViewXY.XAxes[0].ScrollPosition = _previousX;
            }
            else
            {

            }

            //Allow updates again, and update
            _chart.EndUpdate();
        }
    }
}
