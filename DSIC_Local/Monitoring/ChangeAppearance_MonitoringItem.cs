﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using DSIC_Local.Monitoring.Controls;
using System.Reflection;

namespace DSIC_Local.Monitoring
{
    public partial class ChangeAppearance_MonitoringItem : DevExpress.XtraEditors.XtraForm
    {
        MonitorItem searchedMI;
        public ChangeAppearance_MonitoringItem(MonitorItem searchedMI)
        {
            InitializeComponent();

            this.searchedMI = searchedMI;
            panelLeftFill.BackColor = searchedMI.si.DocumentColor;
            lbDocumentCaptionEg.Text = searchedMI.si.DocumentCaption;
            lbDocumentCaptionEg.ForeColor = searchedMI.si.DocumentCaptionColor;

            propertyGridControl1.SelectedObject = searchedMI.si;

            switch (searchedMI.MonitorType)
            {
                case MonitorType.Digital:
                    Digital digital = new Digital(searchedMI);
                    panelLeftFill.Controls.Add(digital);

                    digital.Left = (digital.Parent.Width - digital.Width) / 2;
                    digital.Top = (digital.Parent.Height - digital.Height) / 2;

                    searchedMI.si.setBrowsableProperty("ValueColor", false);

                    break;
            }
        }
        private void propertyGridControl1_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            string rowName = propertyGridControl1.FocusedRow.Name;
            
            switch (rowName)
            {
                case "rowDocumentColor":
                    panelLeftFill.BackColor = (Color)e.Value;
                    break;
                case "rowDocumentCaption":
                    lbDocumentCaptionEg.Text = (string)e.Value;
                    break;
                case "rowDocumentCaptionColor":
                    lbDocumentCaptionEg.ForeColor = (Color)e.Value;
                    break;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }
    }
}