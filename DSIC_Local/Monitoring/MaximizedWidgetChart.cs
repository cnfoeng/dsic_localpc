﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
namespace DSIC_Local.Monitoring
{
    public partial class MaximizedWidgetChart : UserControl
    {
        List<VariableInfo> Variables = null;
        MonitorItem mi;
        public MaximizedWidgetChart(MonitorItem mi)
        {
            InitializeComponent();
            this.mi = mi;
            gridControlAssigned.DataSource = mi.ci.yAxes;
            propertyGridXaxisAndChart.SelectedObject = mi.ci;

            cbChannelList.SelectedIndex = 0;
        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variables = Main.project.sysVarInfo.getVariables(cbChannelList.SelectedIndex);

            gridControlVariableList.BeginUpdate();
            gridControlVariableList.DataSource = Variables;
            gridControlVariableList.EndUpdate();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AssignYAxis();

            gridControlAssigned.BeginUpdate();
            gridControlAssigned.EndUpdate();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mi.ci.yAxes.RemoveAt(gridViewAssigned.FocusedRowHandle);

            gridControlAssigned.BeginUpdate();
            gridControlAssigned.EndUpdate();
        }

        private void AssignYAxis()
        {
            if(gridViewVariableList.FocusedRowHandle < 0)
            {
                return;
            }

            mi.ci.yAxes.Add(new YAxis(Variables[gridViewVariableList.FocusedRowHandle], mi.ci.yAxes.Count));
        }

        private void gridViewAssigned_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            propertyGridYaxis.SelectedObject = mi.ci.yAxes[gridViewAssigned.FocusedRowHandle];
            propertyGridYaxis.Refresh();
        }

        private void gridControlVariableList_DoubleClick(object sender, EventArgs e)
        {
            AssignYAxis();

            gridControlAssigned.BeginUpdate();
            gridControlAssigned.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult result = XtraMessageBox.Show("<size=14>차트의 Y축에 할당된 변수 및 설정 변경을 적용합니다. 확인 또는 프로그램 재실행 시 \n차트 초기화 후 변경이 적용됩니다. 바로 적용하시겠습니까?</size>", "<size=14>에러</size>", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (result == DialogResult.OK)
            {
                mi.ci.Request_YAxesUpdate = true;
            }
        }

        private void propertyGridYaxis_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            gridControlAssigned.BeginUpdate();
            gridControlAssigned.EndUpdate();
        }
    }
}
