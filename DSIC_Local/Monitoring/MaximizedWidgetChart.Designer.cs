﻿namespace DSIC_Local.Monitoring
{
    partial class MaximizedWidgetChart
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlAssigned = new DevExpress.XtraGrid.GridControl();
            this.gridViewAssigned = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColAssignedName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAssginedDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAssginedLineColor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlVariableList = new DevExpress.XtraGrid.GridControl();
            this.gridViewVariableList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbChannelList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.propertyGridXaxisAndChart = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.pnLeft = new DevExpress.XtraEditors.PanelControl();
            this.pnLeftUp = new DevExpress.XtraEditors.PanelControl();
            this.pnLeftDown = new DevExpress.XtraEditors.PanelControl();
            this.pnLeftDownFill = new DevExpress.XtraEditors.PanelControl();
            this.pnLeftDownUp = new DevExpress.XtraEditors.PanelControl();
            this.pnRight = new DevExpress.XtraEditors.PanelControl();
            this.pnRightFill = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.propertyGridYaxis = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnRightDown = new DevExpress.XtraEditors.PanelControl();
            this.pnRightSpace = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVariableList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVariableList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridXaxisAndChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).BeginInit();
            this.pnLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftUp)).BeginInit();
            this.pnLeftUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDown)).BeginInit();
            this.pnLeftDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDownFill)).BeginInit();
            this.pnLeftDownFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDownUp)).BeginInit();
            this.pnLeftDownUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).BeginInit();
            this.pnRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnRightFill)).BeginInit();
            this.pnRightFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridYaxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRightDown)).BeginInit();
            this.pnRightDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnRightSpace)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(539, 20);
            this.btnDelete.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnDelete.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(128, 32);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "제거";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.Location = new System.Drawing.Point(394, 20);
            this.btnAdd.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(128, 32);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "추가";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.gridControlAssigned);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(0, 0);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(685, 170);
            this.groupControl4.TabIndex = 8;
            this.groupControl4.Text = "Assigned Variables";
            // 
            // gridControlAssigned
            // 
            this.gridControlAssigned.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAssigned.Location = new System.Drawing.Point(2, 27);
            this.gridControlAssigned.MainView = this.gridViewAssigned;
            this.gridControlAssigned.Name = "gridControlAssigned";
            this.gridControlAssigned.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit1});
            this.gridControlAssigned.Size = new System.Drawing.Size(681, 141);
            this.gridControlAssigned.TabIndex = 3;
            this.gridControlAssigned.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAssigned});
            // 
            // gridViewAssigned
            // 
            this.gridViewAssigned.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAssigned.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewAssigned.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAssigned.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewAssigned.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewAssigned.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewAssigned.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAssigned.Appearance.Row.Options.UseFont = true;
            this.gridViewAssigned.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColAssignedName,
            this.ColAssginedDescription,
            this.ColAssginedLineColor});
            this.gridViewAssigned.GridControl = this.gridControlAssigned;
            this.gridViewAssigned.Name = "gridViewAssigned";
            this.gridViewAssigned.OptionsBehavior.Editable = false;
            this.gridViewAssigned.OptionsCustomization.AllowSort = false;
            this.gridViewAssigned.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAssigned.OptionsSelection.MultiSelect = true;
            this.gridViewAssigned.OptionsView.ShowGroupPanel = false;
            this.gridViewAssigned.OptionsView.ShowIndicator = false;
            this.gridViewAssigned.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewAssigned_FocusedRowChanged);
            // 
            // ColAssignedName
            // 
            this.ColAssignedName.Caption = "변수 이름";
            this.ColAssignedName.FieldName = "Variable_Name";
            this.ColAssignedName.Name = "ColAssignedName";
            this.ColAssignedName.OptionsFilter.AllowAutoFilter = false;
            this.ColAssignedName.OptionsFilter.AllowFilter = false;
            this.ColAssignedName.Visible = true;
            this.ColAssignedName.VisibleIndex = 0;
            this.ColAssignedName.Width = 209;
            // 
            // ColAssginedDescription
            // 
            this.ColAssginedDescription.Caption = "설명";
            this.ColAssginedDescription.FieldName = "Description";
            this.ColAssginedDescription.Name = "ColAssginedDescription";
            this.ColAssginedDescription.OptionsFilter.AllowAutoFilter = false;
            this.ColAssginedDescription.OptionsFilter.AllowFilter = false;
            this.ColAssginedDescription.Visible = true;
            this.ColAssginedDescription.VisibleIndex = 1;
            this.ColAssginedDescription.Width = 343;
            // 
            // ColAssginedLineColor
            // 
            this.ColAssginedLineColor.Caption = "선 색상";
            this.ColAssginedLineColor.ColumnEdit = this.repositoryItemColorEdit1;
            this.ColAssginedLineColor.FieldName = "Line_Color";
            this.ColAssginedLineColor.Name = "ColAssginedLineColor";
            this.ColAssginedLineColor.Visible = true;
            this.ColAssginedLineColor.VisibleIndex = 2;
            this.ColAssginedLineColor.Width = 129;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(685, 310);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Variable List";
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.gridControlVariableList);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 67);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(681, 241);
            this.panelControl2.TabIndex = 91;
            // 
            // gridControlVariableList
            // 
            this.gridControlVariableList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVariableList.Location = new System.Drawing.Point(0, 0);
            this.gridControlVariableList.MainView = this.gridViewVariableList;
            this.gridControlVariableList.Name = "gridControlVariableList";
            this.gridControlVariableList.Size = new System.Drawing.Size(681, 241);
            this.gridControlVariableList.TabIndex = 2;
            this.gridControlVariableList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVariableList});
            this.gridControlVariableList.DoubleClick += new System.EventHandler(this.gridControlVariableList_DoubleClick);
            // 
            // gridViewVariableList
            // 
            this.gridViewVariableList.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewVariableList.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewVariableList.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewVariableList.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewVariableList.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewVariableList.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewVariableList.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewVariableList.Appearance.Row.Options.UseFont = true;
            this.gridViewVariableList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColName,
            this.ColDescription});
            this.gridViewVariableList.GridControl = this.gridControlVariableList;
            this.gridViewVariableList.Name = "gridViewVariableList";
            this.gridViewVariableList.OptionsBehavior.Editable = false;
            this.gridViewVariableList.OptionsCustomization.AllowSort = false;
            this.gridViewVariableList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewVariableList.OptionsSelection.MultiSelect = true;
            this.gridViewVariableList.OptionsView.ShowGroupPanel = false;
            this.gridViewVariableList.OptionsView.ShowIndicator = false;
            // 
            // ColName
            // 
            this.ColName.Caption = "변수 이름";
            this.ColName.FieldName = "VariableName";
            this.ColName.Name = "ColName";
            this.ColName.OptionsFilter.AllowAutoFilter = false;
            this.ColName.OptionsFilter.AllowFilter = false;
            this.ColName.Visible = true;
            this.ColName.VisibleIndex = 0;
            this.ColName.Width = 291;
            // 
            // ColDescription
            // 
            this.ColDescription.Caption = "설명";
            this.ColDescription.FieldName = "NorminalDescription";
            this.ColDescription.Name = "ColDescription";
            this.ColDescription.OptionsFilter.AllowAutoFilter = false;
            this.ColDescription.OptionsFilter.AllowFilter = false;
            this.ColDescription.Visible = true;
            this.ColDescription.VisibleIndex = 1;
            this.ColDescription.Width = 581;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cbChannelList);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 27);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(681, 40);
            this.panelControl1.TabIndex = 90;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(399, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(57, 19);
            this.labelControl2.TabIndex = 91;
            this.labelControl2.Text = "Channel";
            // 
            // cbChannelList
            // 
            this.cbChannelList.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cbChannelList.Location = new System.Drawing.Point(462, 7);
            this.cbChannelList.Name = "cbChannelList";
            this.cbChannelList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannelList.Properties.Appearance.Options.UseFont = true;
            this.cbChannelList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChannelList.Properties.Items.AddRange(new object[] {
            "MITSUBISHI",
            "CALCULATION",
            "ENGINE",
            "RGV"});
            this.cbChannelList.Size = new System.Drawing.Size(215, 26);
            this.cbChannelList.TabIndex = 90;
            this.cbChannelList.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 23);
            this.label1.TabIndex = 89;
            this.label1.Text = "변수 목록";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.Location = new System.Drawing.Point(237, 16);
            this.btnOK.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnOK.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(115, 50);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "적용";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.propertyGridXaxisAndChart);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(369, 217);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "X axis and Chart option";
            // 
            // propertyGridXaxisAndChart
            // 
            this.propertyGridXaxisAndChart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.propertyGridXaxisAndChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridXaxisAndChart.Location = new System.Drawing.Point(2, 27);
            this.propertyGridXaxisAndChart.Name = "propertyGridXaxisAndChart";
            this.propertyGridXaxisAndChart.Padding = new System.Windows.Forms.Padding(5);
            this.propertyGridXaxisAndChart.SelectedObject = "(none)";
            this.propertyGridXaxisAndChart.Size = new System.Drawing.Size(365, 188);
            this.propertyGridXaxisAndChart.TabIndex = 0;
            // 
            // pnLeft
            // 
            this.pnLeft.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeft.Controls.Add(this.pnLeftUp);
            this.pnLeft.Controls.Add(this.pnLeftDown);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(685, 553);
            this.pnLeft.TabIndex = 13;
            // 
            // pnLeftUp
            // 
            this.pnLeftUp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeftUp.Controls.Add(this.groupControl1);
            this.pnLeftUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeftUp.Location = new System.Drawing.Point(0, 0);
            this.pnLeftUp.Name = "pnLeftUp";
            this.pnLeftUp.Size = new System.Drawing.Size(685, 310);
            this.pnLeftUp.TabIndex = 11;
            // 
            // pnLeftDown
            // 
            this.pnLeftDown.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeftDown.Controls.Add(this.pnLeftDownFill);
            this.pnLeftDown.Controls.Add(this.pnLeftDownUp);
            this.pnLeftDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnLeftDown.Location = new System.Drawing.Point(0, 310);
            this.pnLeftDown.Name = "pnLeftDown";
            this.pnLeftDown.Size = new System.Drawing.Size(685, 243);
            this.pnLeftDown.TabIndex = 12;
            // 
            // pnLeftDownFill
            // 
            this.pnLeftDownFill.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeftDownFill.Controls.Add(this.groupControl4);
            this.pnLeftDownFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeftDownFill.Location = new System.Drawing.Point(0, 73);
            this.pnLeftDownFill.Name = "pnLeftDownFill";
            this.pnLeftDownFill.Size = new System.Drawing.Size(685, 170);
            this.pnLeftDownFill.TabIndex = 11;
            // 
            // pnLeftDownUp
            // 
            this.pnLeftDownUp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnLeftDownUp.Controls.Add(this.btnAdd);
            this.pnLeftDownUp.Controls.Add(this.btnDelete);
            this.pnLeftDownUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLeftDownUp.Location = new System.Drawing.Point(0, 0);
            this.pnLeftDownUp.Name = "pnLeftDownUp";
            this.pnLeftDownUp.Size = new System.Drawing.Size(685, 73);
            this.pnLeftDownUp.TabIndex = 12;
            // 
            // pnRight
            // 
            this.pnRight.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRight.Controls.Add(this.pnRightFill);
            this.pnRight.Controls.Add(this.pnRightDown);
            this.pnRight.Controls.Add(this.pnRightSpace);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnRight.Location = new System.Drawing.Point(685, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(399, 553);
            this.pnRight.TabIndex = 14;
            // 
            // pnRightFill
            // 
            this.pnRightFill.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRightFill.Controls.Add(this.groupControl3);
            this.pnRightFill.Controls.Add(this.panel1);
            this.pnRightFill.Controls.Add(this.groupControl2);
            this.pnRightFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRightFill.Location = new System.Drawing.Point(30, 0);
            this.pnRightFill.Name = "pnRightFill";
            this.pnRightFill.Size = new System.Drawing.Size(369, 473);
            this.pnRightFill.TabIndex = 14;
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.propertyGridYaxis);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 237);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(369, 236);
            this.groupControl3.TabIndex = 12;
            this.groupControl3.Text = "Y axis";
            // 
            // propertyGridYaxis
            // 
            this.propertyGridYaxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridYaxis.Location = new System.Drawing.Point(2, 27);
            this.propertyGridYaxis.Name = "propertyGridYaxis";
            this.propertyGridYaxis.Padding = new System.Windows.Forms.Padding(5);
            this.propertyGridYaxis.SelectedObject = "(none)";
            this.propertyGridYaxis.Size = new System.Drawing.Size(365, 207);
            this.propertyGridYaxis.TabIndex = 0;
            this.propertyGridYaxis.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.propertyGridYaxis_CellValueChanged);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 217);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(369, 20);
            this.panel1.TabIndex = 12;
            // 
            // pnRightDown
            // 
            this.pnRightDown.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRightDown.Controls.Add(this.btnOK);
            this.pnRightDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnRightDown.Location = new System.Drawing.Point(30, 473);
            this.pnRightDown.Name = "pnRightDown";
            this.pnRightDown.Size = new System.Drawing.Size(369, 80);
            this.pnRightDown.TabIndex = 15;
            // 
            // pnRightSpace
            // 
            this.pnRightSpace.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRightSpace.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnRightSpace.Location = new System.Drawing.Point(0, 0);
            this.pnRightSpace.Name = "pnRightSpace";
            this.pnRightSpace.Size = new System.Drawing.Size(30, 553);
            this.pnRightSpace.TabIndex = 13;
            // 
            // MaximizedWidgetChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnLeft);
            this.Controls.Add(this.pnRight);
            this.Name = "MaximizedWidgetChart";
            this.Size = new System.Drawing.Size(1084, 553);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVariableList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVariableList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridXaxisAndChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).EndInit();
            this.pnLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftUp)).EndInit();
            this.pnLeftUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDown)).EndInit();
            this.pnLeftDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDownFill)).EndInit();
            this.pnLeftDownFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnLeftDownUp)).EndInit();
            this.pnLeftDownUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).EndInit();
            this.pnRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnRightFill)).EndInit();
            this.pnRightFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridYaxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnRightDown)).EndInit();
            this.pnRightDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnRightSpace)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridXaxisAndChart;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbChannelList;
        private DevExpress.XtraGrid.GridControl gridControlAssigned;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAssigned;
        private DevExpress.XtraGrid.Columns.GridColumn ColAssignedName;
        private DevExpress.XtraGrid.Columns.GridColumn ColAssginedDescription;
        private DevExpress.XtraGrid.Columns.GridColumn ColAssginedLineColor;
        private DevExpress.XtraGrid.GridControl gridControlVariableList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVariableList;
        private DevExpress.XtraGrid.Columns.GridColumn ColName;
        private DevExpress.XtraGrid.Columns.GridColumn ColDescription;
        private DevExpress.XtraEditors.PanelControl pnLeft;
        private DevExpress.XtraEditors.PanelControl pnLeftUp;
        private DevExpress.XtraEditors.PanelControl pnLeftDown;
        private DevExpress.XtraEditors.PanelControl pnLeftDownFill;
        private DevExpress.XtraEditors.PanelControl pnLeftDownUp;
        private DevExpress.XtraEditors.PanelControl pnRight;
        private DevExpress.XtraEditors.PanelControl pnRightFill;
        private DevExpress.XtraEditors.PanelControl pnRightDown;
        private DevExpress.XtraEditors.PanelControl pnRightSpace;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridYaxis;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
    }
}
