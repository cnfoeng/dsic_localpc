﻿namespace DSIC_Local.Monitoring
{
    partial class AssignVarForMonitoringItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssignVarForMonitoringItem));
            this.panelBack = new DevExpress.XtraEditors.PanelControl();
            this.panelBackFill = new DevExpress.XtraEditors.PanelControl();
            this.panelFill = new DevExpress.XtraEditors.PanelControl();
            this.gridList = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.ColVariableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDiscription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cbChannelList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.teAssignedVarName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).BeginInit();
            this.panelBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelBackFill)).BeginInit();
            this.panelBackFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).BeginInit();
            this.panelFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAssignedVarName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBack
            // 
            this.panelBack.Controls.Add(this.panelBackFill);
            this.panelBack.Controls.Add(this.panelControl1);
            this.panelBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBack.Location = new System.Drawing.Point(0, 0);
            this.panelBack.Name = "panelBack";
            this.panelBack.Size = new System.Drawing.Size(947, 613);
            this.panelBack.TabIndex = 0;
            // 
            // panelBackFill
            // 
            this.panelBackFill.Controls.Add(this.panelFill);
            this.panelBackFill.Controls.Add(this.panelTop);
            this.panelBackFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBackFill.Location = new System.Drawing.Point(2, 2);
            this.panelBackFill.Name = "panelBackFill";
            this.panelBackFill.Size = new System.Drawing.Size(943, 544);
            this.panelBackFill.TabIndex = 1;
            // 
            // panelFill
            // 
            this.panelFill.Controls.Add(this.gridList);
            this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFill.Location = new System.Drawing.Point(2, 55);
            this.panelFill.Name = "panelFill";
            this.panelFill.Size = new System.Drawing.Size(939, 487);
            this.panelFill.TabIndex = 3;
            // 
            // gridList
            // 
            this.gridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridList.Location = new System.Drawing.Point(2, 2);
            this.gridList.MainView = this.gridView1;
            this.gridList.Name = "gridList";
            this.gridList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridList.Size = new System.Drawing.Size(935, 483);
            this.gridList.TabIndex = 3;
            this.gridList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColChecked,
            this.ColVariableName,
            this.ColDiscription});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridList;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // ColChecked
            // 
            this.ColChecked.Caption = " ";
            this.ColChecked.ColumnEdit = this.repositoryItemCheckEdit1;
            this.ColChecked.FieldName = "Checked";
            this.ColChecked.ImageOptions.Alignment = System.Drawing.StringAlignment.Center;
            this.ColChecked.Name = "ColChecked";
            this.ColChecked.OptionsFilter.AllowAutoFilter = false;
            this.ColChecked.OptionsFilter.AllowFilter = false;
            this.ColChecked.Visible = true;
            this.ColChecked.VisibleIndex = 0;
            this.ColChecked.Width = 34;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom;
            this.repositoryItemCheckEdit1.CheckBoxOptions.SvgColorChecked = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit1.CheckBoxOptions.SvgColorUnchecked = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit1.ImageOptions.SvgImageChecked = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("repositoryItemCheckEdit1.ImageOptions.SvgImageChecked")));
            this.repositoryItemCheckEdit1.ImageOptions.SvgImageSize = new System.Drawing.Size(18, 18);
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            // 
            // ColVariableName
            // 
            this.ColVariableName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColVariableName.AppearanceHeader.Options.UseFont = true;
            this.ColVariableName.AppearanceHeader.Options.UseTextOptions = true;
            this.ColVariableName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColVariableName.Caption = "변수 이름";
            this.ColVariableName.FieldName = "VariableName";
            this.ColVariableName.Name = "ColVariableName";
            this.ColVariableName.OptionsColumn.AllowEdit = false;
            this.ColVariableName.OptionsFilter.AllowAutoFilter = false;
            this.ColVariableName.OptionsFilter.AllowFilter = false;
            this.ColVariableName.Visible = true;
            this.ColVariableName.VisibleIndex = 1;
            this.ColVariableName.Width = 325;
            // 
            // ColDiscription
            // 
            this.ColDiscription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColDiscription.AppearanceHeader.Options.UseFont = true;
            this.ColDiscription.AppearanceHeader.Options.UseTextOptions = true;
            this.ColDiscription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColDiscription.Caption = "설명";
            this.ColDiscription.FieldName = "NorminalDescription";
            this.ColDiscription.Name = "ColDiscription";
            this.ColDiscription.OptionsColumn.AllowEdit = false;
            this.ColDiscription.OptionsFilter.AllowAutoFilter = false;
            this.ColDiscription.OptionsFilter.AllowFilter = false;
            this.ColDiscription.Visible = true;
            this.ColDiscription.VisibleIndex = 2;
            this.ColDiscription.Width = 577;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.labelControl2);
            this.panelTop.Controls.Add(this.cbChannelList);
            this.panelTop.Controls.Add(this.teAssignedVarName);
            this.panelTop.Controls.Add(this.labelControl1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(2, 2);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(939, 53);
            this.panelTop.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(604, 17);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(57, 19);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Channel";
            // 
            // cbChannelList
            // 
            this.cbChannelList.Location = new System.Drawing.Point(667, 14);
            this.cbChannelList.Name = "cbChannelList";
            this.cbChannelList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannelList.Properties.Appearance.Options.UseFont = true;
            this.cbChannelList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChannelList.Properties.Items.AddRange(new object[] {
            "MITSUBISHI",
            "CALCULATION",
            "ENGINE",
            "RGV"});
            this.cbChannelList.Size = new System.Drawing.Size(254, 26);
            this.cbChannelList.TabIndex = 2;
            this.cbChannelList.SelectedIndexChanged += new System.EventHandler(this.cbChannelList_SelectedIndexChanged);
            // 
            // teAssignedVarName
            // 
            this.teAssignedVarName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.teAssignedVarName.EditValue = "Test";
            this.teAssignedVarName.Location = new System.Drawing.Point(144, 14);
            this.teAssignedVarName.Name = "teAssignedVarName";
            this.teAssignedVarName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.teAssignedVarName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teAssignedVarName.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.teAssignedVarName.Properties.Appearance.Options.UseBackColor = true;
            this.teAssignedVarName.Properties.Appearance.Options.UseFont = true;
            this.teAssignedVarName.Properties.Appearance.Options.UseForeColor = true;
            this.teAssignedVarName.Properties.Appearance.Options.UseTextOptions = true;
            this.teAssignedVarName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teAssignedVarName.Properties.ReadOnly = true;
            this.teAssignedVarName.Size = new System.Drawing.Size(344, 26);
            this.teAssignedVarName.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(126, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Assigned Variable";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnOK);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(2, 546);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(943, 65);
            this.panelControl1.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(803, 11);
            this.btnOK.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(120, 43);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "닫기";
            // 
            // AssignVarForMonitoringItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 613);
            this.Controls.Add(this.panelBack);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AssignVarForMonitoringItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign Variable For Monitoring Item";
            ((System.ComponentModel.ISupportInitialize)(this.panelBack)).EndInit();
            this.panelBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelBackFill)).EndInit();
            this.panelBackFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelFill)).EndInit();
            this.panelFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbChannelList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAssignedVarName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelBack;
        private DevExpress.XtraEditors.PanelControl panelBackFill;
        private DevExpress.XtraEditors.PanelControl panelFill;
        private DevExpress.XtraGrid.GridControl gridList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColChecked;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn ColVariableName;
        private DevExpress.XtraGrid.Columns.GridColumn ColDiscription;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cbChannelList;
        private DevExpress.XtraEditors.TextEdit teAssignedVarName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
    }
}