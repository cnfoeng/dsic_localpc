﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using System.ComponentModel;
using System.Reflection;

namespace DSIC_Local.Monitoring
{
    public enum ChartColorTheme
    {
        Black,
        Silver,
        White,
    }
    [Serializable]
    [DefaultPropertyAttribute("MonitorItem")]
    public class MonitorItem : ICloneable
    {
        string firstName;//맨 처음생성될 때 mi구분용으로만 사용

        public string documentName;

        [Browsable(false)]
        public string FirstName { get => firstName; set => firstName = value; }

        [Browsable(false)]
        public bool Request_Update { get; set; }

        [NonSerialized]
        public Document Document;

        public MonitorType MonitorType;

        public SingleMonitorItem si = new SingleMonitorItem();
        public ChartMonitorItem ci = new ChartMonitorItem();
        public BitIndicatorMonitorItem bi = new BitIndicatorMonitorItem();

        public MonitorItem Clone()
        {
            return DeepCopy();
        }

        object ICloneable.Clone()
        {
            return DeepCopy();
        }

        protected virtual MonitorItem DeepCopy()
        {
            MonitorItem mi = (MonitorItem)this.MemberwiseClone();

            mi.ci = new ChartMonitorItem();

            mi.ci.yAxes = this.ci.yAxes.ConvertAll(o => new YAxis(o.Variable_Name, o.Unit, o.Range_Auto, o.MinRange, o.MaxRange, o.Line_Color, o.Description));

            return mi;
        }
    }

    [Serializable]
    public class SingleMonitorItem
    {
        float nameFontSize = 30;
        Color nameColor = Color.FromArgb(80, 80, 80);

        float unitFontSize = 20;
        bool unitVisible = true;
        Color unitColor = Color.FromArgb(80, 80, 80);

        float valueFontSize = 30;
        Color valueColor = Color.FromArgb(192, 255, 192);

        Color backgroundColor_Top = Color.FromArgb(222, 222, 222);
        Color backgroundColor_Bottom = Color.FromArgb(71, 71, 71);

        Color documentColor = Color.Transparent;
        string documentCaption = "";
        Color documentCaptionColor = Color.FromArgb(80, 80, 80);

        public VariableInfo assignedVar = null; //단일 모니터링 용

        public string Name
        {
            get
            {
                if (assignedVar == null)
                {
                    return null;
                }

                return assignedVar.VariableName;
            }

        }

        public string Unit { get => assignedVar.Unit; }
        public double Value { get => assignedVar.RealValue; }

        [CategoryAttribute("Name")]
        [DisplayName("Name Font Size")]
        public float NameFontSize { get => nameFontSize; set => nameFontSize = value; }
        [CategoryAttribute("Name")]
        [DisplayName("Name Color")]
        public Color NameColor { get => nameColor; set => nameColor = value; }
        [CategoryAttribute("Unit")]
        [DisplayName("Unit Font Size")]
        public float UnitFontSize { get => unitFontSize; set => unitFontSize = value; }
        [CategoryAttribute("Unit")]
        [DisplayName("Unit Visible")]
        public bool UnitVisible { get => unitVisible; set => unitVisible = value; }
        [CategoryAttribute("Unit")]
        [DisplayName("Unit Color")]
        public Color UnitColor { get => unitColor; set => unitColor = value; }
        [CategoryAttribute("Value")]
        [DisplayName("Value Font Size")]
        public float ValueFontSize { get => valueFontSize; set => valueFontSize = value; }
        [CategoryAttribute("Value")]
        [DisplayName("Value Color")]
        public Color ValueColor { get => valueColor; set => valueColor = value; }

        [CategoryAttribute("Background")]
        [DisplayName("Background Color - Top")]
        public Color BackgroundColor_Top { get => backgroundColor_Top; set => backgroundColor_Top = value; }
        [CategoryAttribute("Background")]
        [DisplayName("Background Color - Bottom")]
        public Color BackgroundColor_Bottom { get => backgroundColor_Bottom; set => backgroundColor_Bottom = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Color")]
        public Color DocumentColor { get => documentColor; set => documentColor = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption")]
        public string DocumentCaption { get => documentCaption; set => documentCaption = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption Color")]
        public Color DocumentCaptionColor { get => documentCaptionColor; set => documentCaptionColor = value; }


        public void setBrowsableProperty(string strPropertyName, bool bIsBrowsable)
        {
            // Get the Descriptor's Properties
            PropertyDescriptor theDescriptor = TypeDescriptor.GetProperties(this.GetType())[strPropertyName];

            // Get the Descriptor's "Browsable" Attribute
            BrowsableAttribute theDescriptorBrowsableAttribute = (BrowsableAttribute)theDescriptor.Attributes[typeof(BrowsableAttribute)];
            FieldInfo isBrowsable = theDescriptorBrowsableAttribute.GetType().GetField("Browsable", BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Instance);

            // Set the Descriptor's "Browsable" Attribute
            isBrowsable.SetValue(theDescriptorBrowsableAttribute, bIsBrowsable);
        }
    }



    [Serializable]
    public class ChartMonitorItem : ICloneable
    {
        public List<YAxis> yAxes = new List<YAxis>();

        private bool gridLine_Visibility = false;
        private bool title_Visibility = true;
        private bool dropOldData = false;
        private bool legendBoxVisibility = true;
        private bool xAxis_AutoScroll = true;

        Color documentColor = Color.Transparent;
        string documentCaption = "";
        Color documentCaptionColor = Color.FromArgb(80, 80, 80);

        [Browsable(false)]
        public bool Request_YAxesUpdate { get; set; }

        [CategoryAttribute("Document")]
        [DisplayName("Document Color")]
        public Color DocumentColor { get => documentColor; set => documentColor = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption")]
        public string DocumentCaption { get => documentCaption; set => documentCaption = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption Color")]
        public Color DocumentCaptionColor { get => documentCaptionColor; set => documentCaptionColor = value; }


        [CategoryAttribute("Chart")]
        [DisplayName("Color Theme")]
        public ChartColorTheme chartColorTheme { get; set; } = ChartColorTheme.Black;

        [CategoryAttribute("Chart")]
        [DisplayName("GridLine Visibility")]
        public bool GridLine_Visibility
        {
            get
            {
                return gridLine_Visibility;
            }
            set
            {
                gridLine_Visibility = value;
            }
        }
        [Browsable(false)]
        [CategoryAttribute("Chart")]
        [DisplayName("Title Visibility")]
        public bool Title_Visibility
        {
            get
            {
                return title_Visibility;
            }
            set
            {
                title_Visibility = value;
            }
        }
        [CategoryAttribute("Chart")]
        [DisplayName("Drop Old Data")]
        public bool DropOldData { get => dropOldData; set => dropOldData = value; }
        [CategoryAttribute("Chart")]
        [DisplayName("Legendbox Visibility")]
        public bool LegendBoxVisibility { get => legendBoxVisibility; set => legendBoxVisibility = value; }
        [CategoryAttribute("Chart")]
        [DisplayName("X axis Auto Scroll")]
        public bool XAxis_AutoScroll { get => xAxis_AutoScroll; set => xAxis_AutoScroll = value; }

        public ChartMonitorItem Clone()
        {
            return DeepCopy();
        }

        object ICloneable.Clone()
        {
            return DeepCopy();
        }

        protected virtual ChartMonitorItem DeepCopy()
        {
            ChartMonitorItem chartMonitorItem = (ChartMonitorItem)this.MemberwiseClone();

            return chartMonitorItem;
        }
    }

    [Serializable]
    public class YAxis
    {
        Color[] colors = new Color[7] { Color.DarkCyan, Color.DarkMagenta, Color.DarkGreen, Color.DarkSlateBlue, Color.DarkOrange, Color.DarkRed, Color.DarkSlateGray };

        private string name;
        private string unit;
        private bool range_Auto = true;
        private double minRange = 0;
        private double maxRange = 100;
        private Color lineColor = Color.Black;
        private string description = string.Empty;

        public YAxis(VariableInfo var, int listCount)
        {
            name = var.VariableName;
            unit = var.Unit;
            if (var.CalType == CalibrationType.CalByValue)
            {
                minRange = var.Range_Low;
                maxRange = var.Range_High;
            }
            else if (var.CalType == CalibrationType.CalByTable)
            {
                if (var.CalibrationTable.Count == 2)
                {
                    minRange = var.CalibrationTable[0].RealValue;
                    maxRange = var.CalibrationTable[1].RealValue;
                }
            }
            else
            {
                minRange = 0;
                maxRange = 100;
            }
            Description = var.NorminalDescription;

            if (listCount < 6)
            {
                lineColor = colors[listCount];
            }
        }

        public YAxis(string name, string unit, bool range_Auto, double minRange, double maxRange, Color lineColor, string description)
        {
            this.name = name;
            this.unit = unit;
            this.range_Auto = range_Auto;
            this.minRange = minRange;
            this.maxRange = maxRange;
            this.lineColor = lineColor;
            this.description = description;
        }

        [CategoryAttribute("YAxis"), ReadOnly(true)]
        [DisplayName("Variable Name")]
        public string Variable_Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        [CategoryAttribute("YAxis"), ReadOnly(true)]
        public string Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }
        [CategoryAttribute("YAxis")]
        [DisplayName("Line Color")]
        public Color Line_Color
        {
            get
            {
                return lineColor;
            }
            set
            {
                lineColor = value;
            }
        }
        [CategoryAttribute("YAxis"),
        DescriptionAttribute("Check if you want to change range of Y axis automately.")]
        [DisplayName("Range Auto")]
        public bool Range_Auto
        {
            get
            {
                return range_Auto;
            }
            set
            {
                range_Auto = value;
            }
        }
        [CategoryAttribute("YAxis"),
        DescriptionAttribute("Minimum of Y axis value")]
        [DisplayName("Minimum Range")]
        public double MinRange
        {
            get
            {
                return minRange;
            }
            set
            {
                minRange = value;
            }
        }
        [CategoryAttribute("YAxis"),
        DescriptionAttribute("Visibility of the Yaxis grid line.")]
        [DisplayName("Maximum Range")]
        public double MaxRange
        {
            get
            {
                return maxRange;
            }
            set
            {
                maxRange = value;
            }
        }
        [Browsable(false)]
        public string Description { get => description; set => description = value; }
    }
    public enum BitRange
    {
        Bit1_8,
        Bit9_16,
    }
    [Serializable]
    public class BitIndicatorMonitorItem
    {
        float nameFontSize = 30;
        Color nameColor = Color.FromArgb(80, 80, 80);

        Color backgroundColor_Top = Color.FromArgb(222, 222, 222);
        Color backgroundColor_Bottom = Color.FromArgb(71, 71, 71);

        Color documentColor = Color.Transparent;
        string documentCaption = "";
        Color documentCaptionColor = Color.FromArgb(80, 80, 80);

        bool useDefaultNumber = true;
        bool isLittleEndian = true;


        public VariableInfo assignedVar = null;

        [CategoryAttribute("Name")]
        [DisplayName("Name Font Size")]
        public float NameFontSize { get => nameFontSize; set => nameFontSize = value; }
        [CategoryAttribute("Name")]
        [DisplayName("Name Color")]
        public Color NameColor { get => nameColor; set => nameColor = value; }

        [CategoryAttribute("Background")]
        [DisplayName("Background Color - Top")]
        public Color BackgroundColor_Top { get => backgroundColor_Top; set => backgroundColor_Top = value; }
        [CategoryAttribute("Background")]
        [DisplayName("Background Color - Bottom")]
        public Color BackgroundColor_Bottom { get => backgroundColor_Bottom; set => backgroundColor_Bottom = value; }

        [CategoryAttribute("Document")]
        [DisplayName("Document Color")]
        public Color DocumentColor { get => documentColor; set => documentColor = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption")]
        public string DocumentCaption { get => documentCaption; set => documentCaption = value; }
        [CategoryAttribute("Document")]
        [DisplayName("Document Caption Color")]
        public Color DocumentCaptionColor { get => documentCaptionColor; set => documentCaptionColor = value; }

        [CategoryAttribute("Bit Name")]
        public string Bit1 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit2 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit3 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit4 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit5 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit6 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit7 { get; set; }
        [CategoryAttribute("Bit Name")]
        public string Bit8 { get; set; }

        [CategoryAttribute("Bit Option")]
        [DisplayName("Use Default Number")]
        public bool UseDefaultNumber { get => useDefaultNumber; set => useDefaultNumber = value; }
        [CategoryAttribute("Bit Option")]
        [DisplayName("Is Little Endian")]
        public bool IsLittleEndian { get => isLittleEndian; set => isLittleEndian = value; }
        [CategoryAttribute("Bit Option")]
        public BitRange BitRange = BitRange.Bit1_8;
    }
}

