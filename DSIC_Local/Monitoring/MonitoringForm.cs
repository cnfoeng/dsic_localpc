﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using DSIC_Local.Monitoring.Controls;
using System.Diagnostics;
using System.Threading;

namespace DSIC_Local.Monitoring
{
    public enum MonitorType
    {
        Digital = 0,
        Speedo = 1,
        Chart = 2,
        Ramp = 3,
        BitIndicator = 4,
    }
    public partial class MonitoringForm : UserControl
    {
        public List<MonitorItem> monitorItems = new List<MonitorItem>();
        public static Size CurrentMonitorItemSize;
        string MonitorName = string.Empty;
        public MonitoringForm(string monitorName, List<MonitorItem> monitorItems)
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            this.monitorItems = monitorItems;
            MonitorName = monitorName;

            RestoreDocuments();
            LoadXMLLayout();
        }
        public void RestoreDocuments()
        {
            widgetView1.Documents.Clear();

            foreach (MonitorItem mi in monitorItems)
            {
                Document doc = widgetView1.AddDocument("", mi.documentName) as Document;
                doc.Restored += MonitoringForm_Restored;
                mi.Document = doc;//Document는 Serialize 불가능함. 내가 만든 클래스가 아니라 접근이 불가능. 그래서 모니터아이템 정보로 새로 만들어 줌.
            }
        }
        public void LoadXMLLayout()
        {
            try
            {
                documentManager1.BeginUpdate();
                widgetView1.RestoreLayoutFromXml(Application.StartupPath + "\\" + MonitorName + ".xml");
                documentManager1.EndUpdate();
            }
            catch (System.IO.FileNotFoundException)
            {
                Debug.Print(MonitorName + "XML Not Found");
            }
        }
        public void SaveXMLLayout()
        {
            widgetView1.SaveLayoutToXml(Application.StartupPath + "\\" + MonitorName + ".xml");
        }
        private void widgetView1_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {

            MonitorItem searchedMI = null;

            foreach (MonitorItem mi in monitorItems)
            {
                if (mi.FirstName == e.Document.ControlName)
                    searchedMI = mi;
            }


            if (searchedMI == null)//일단 에러 방지용
            {
                Empty empty = new Empty();
                e.Control = empty;
                return;
            }

            Document doc = e.Document as Document;

            switch (searchedMI.MonitorType)
            {
                case MonitorType.Digital:
                    Digital digital = new Digital(searchedMI);
                    e.Control = digital;
                    doc.MaximizedControl = new MaximizedWidgetSingleControl(searchedMI);
                    break;
                case MonitorType.Speedo:
                    Speedo speedo = new Speedo(searchedMI);
                    e.Control = speedo;
                    doc.MaximizedControl = new MaximizedWidgetSingleControl(searchedMI);
                    break;
                case MonitorType.Chart:
                    TrackingChart trackingChart = new TrackingChart(searchedMI);
                    e.Control = trackingChart;
                    doc.MaximizedControl = new MaximizedWidgetChart(searchedMI);
                    break;
                case MonitorType.Ramp:
                    Ramp ramp = new Ramp(searchedMI);
                    e.Control = ramp;
                    doc.MaximizedControl = new MaximizedWidgetSingleControl(searchedMI);
                    break;
                case MonitorType.BitIndicator:
                    BitIndicator bitIndicator = new BitIndicator(searchedMI);
                    e.Control = bitIndicator;
                    doc.MaximizedControl = new MaximizedWidgetSingleControl(searchedMI);
                    break;
            }

            System.Threading.Thread.Sleep(100);
        }

        private void widgetView1_PopupMenuShowing(object sender, DevExpress.XtraBars.Docking2010.Views.PopupMenuShowingEventArgs e)
        {
            e.Menu.Items.Clear();

            DXMenuItem item = new DXMenuItem();
            item.Caption = "Add Digital";
            item.Click += ItemAddDigital_Click;
            e.Menu.Items.Add(item);

            item = new DXMenuItem();
            item.Caption = "Add Speedo";
            item.Click += ItemAddSpeedo_Click;
            e.Menu.Items.Add(item);

            item = new DXMenuItem();
            item.Caption = "Add Chart";
            item.Click += ItemAddChart_Click;
            e.Menu.Items.Add(item);

            item = new DXMenuItem();
            item.Caption = "Add Ramp";
            item.Click += ItemAddRamp_Click;
            e.Menu.Items.Add(item);

            //item = new DXMenuItem();
            //item.Caption = "Add Bit Indicator";
            //item.Click += ItemAddBitIndicator_Click;
            //e.Menu.Items.Add(item);
        }

        

        private void widgetView1_DocumentRemoved(object sender, DocumentEventArgs e)
        {
            foreach (MonitorItem mi in monitorItems)
            {
                if (mi.documentName == e.Document.ControlName)
                {
                    monitorItems.Remove(mi);
                    break;
                }
            }
        }

        private void ItemAddDigital_Click(object sender, EventArgs e)
        {
            CreateMonitorItemDocument(MonitorType.Digital);
        }
        private void ItemAddSpeedo_Click(object sender, EventArgs e)
        {
            CreateMonitorItemDocument(MonitorType.Speedo);
        }


        private void ItemAddChart_Click(object sender, EventArgs e)
        {
            CreateMonitorItemDocument(MonitorType.Chart);
        }


        private void ItemAddRamp_Click(object sender, EventArgs e)
        {
            CreateMonitorItemDocument(MonitorType.Ramp);
        }
        private void ItemAddBitIndicator_Click(object sender, EventArgs e)
        {
            CreateMonitorItemDocument(MonitorType.BitIndicator);
        }
        private void MonitoringForm_Restored(object sender, EventArgs e)
        {
            Document doc = sender as Document;
            foreach (MonitorItem mi in monitorItems)
            {
                if (mi.Document == doc)
                {
                    mi.Request_Update = true;
                }
            }

        }

        private void CreateMonitorItemDocument(MonitorType monitorType)
        {
            int docCount = widgetView1.Documents.Count;
            string createdMonitorItemName = string.Empty;
            bool checkOK = true;
            int index = 0;

            if (docCount > 0)
            {
                while (true)
                {
                    checkOK = true;

                    for (int i = 0; i < docCount; i++)
                    {
                        createdMonitorItemName = "document" + (index + 1).ToString();

                        if (widgetView1.Documents[i].ControlName == createdMonitorItemName)
                        {
                            checkOK = false;
                            continue;
                        }
                    }

                    index++;

                    if (checkOK)
                    {
                        break;
                    }
                }
            }
            else
            {
                createdMonitorItemName = "document" + (index + 1).ToString();
            }

            Document doc = widgetView1.AddDocument("", createdMonitorItemName) as Document;
            doc.Restored += MonitoringForm_Restored;

            MonitorItem monitorItem = new MonitorItem();
            monitorItem.documentName = (widgetView1.Documents[docCount] as Document).ControlName;
            monitorItem.FirstName = createdMonitorItemName;
            monitorItem.documentName = createdMonitorItemName;
            monitorItem.MonitorType = monitorType;
            monitorItem.Document = doc;
            monitorItems.Add(monitorItem);
        }
        public void RefreshAllDocuments()
        {
            widgetView1.ReleaseDeferredLoadControls(false);

        }

    }


}
