﻿namespace DSIC_Local.Monitoring
{
    partial class MaximizedWidgetSingleControl
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAppearanceChange = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditVar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAssignVar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnAppearanceChange);
            this.panelControl1.Controls.Add(this.btnEditVar);
            this.panelControl1.Controls.Add(this.btnAssignVar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(730, 428);
            this.panelControl1.TabIndex = 3;
            // 
            // btnAppearanceChange
            // 
            this.btnAppearanceChange.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAppearanceChange.Appearance.Options.UseFont = true;
            this.btnAppearanceChange.Enabled = false;
            this.btnAppearanceChange.Location = new System.Drawing.Point(275, 40);
            this.btnAppearanceChange.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAppearanceChange.Name = "btnAppearanceChange";
            this.btnAppearanceChange.Size = new System.Drawing.Size(180, 180);
            this.btnAppearanceChange.TabIndex = 5;
            this.btnAppearanceChange.Text = "Change Appearance";
            this.btnAppearanceChange.Click += new System.EventHandler(this.btnAppearanceChange_Click);
            // 
            // btnEditVar
            // 
            this.btnEditVar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditVar.Appearance.Options.UseFont = true;
            this.btnEditVar.Location = new System.Drawing.Point(511, 40);
            this.btnEditVar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEditVar.Name = "btnEditVar";
            this.btnEditVar.Size = new System.Drawing.Size(180, 180);
            this.btnEditVar.TabIndex = 4;
            this.btnEditVar.Text = "Edit Variable Information";
            // 
            // btnAssignVar
            // 
            this.btnAssignVar.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssignVar.Appearance.Options.UseFont = true;
            this.btnAssignVar.Location = new System.Drawing.Point(39, 40);
            this.btnAssignVar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAssignVar.Name = "btnAssignVar";
            this.btnAssignVar.Size = new System.Drawing.Size(180, 180);
            this.btnAssignVar.TabIndex = 3;
            this.btnAssignVar.Text = "Assign Variable";
            this.btnAssignVar.Click += new System.EventHandler(this.btnAssignVar_Click);
            // 
            // MaximizedWidgetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "MaximizedWidgetControl";
            this.Size = new System.Drawing.Size(730, 428);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnAppearanceChange;
        private DevExpress.XtraEditors.SimpleButton btnEditVar;
        private DevExpress.XtraEditors.SimpleButton btnAssignVar;
    }
}
