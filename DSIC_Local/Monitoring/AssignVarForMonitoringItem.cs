﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local.Monitoring
{
    public partial class AssignVarForMonitoringItem : DevExpress.XtraEditors.XtraForm
    {
        List<VariableInfo> Variables = null;
        MonitorItem searchedMI = null;
        public AssignVarForMonitoringItem(MonitorItem searchedMI)
        {
            InitializeComponent();
            this.searchedMI = searchedMI;
            teAssignedVarName.Text = searchedMI.si.Name;

            

            if (searchedMI.si.assignedVar == null)
            {
                teAssignedVarName.Text = "Unassigned";
                Variables = Main.project.sysVarInfo.CNFODAQ_Variables;
                ClearCheck(Variables);
                gridList.DataSource = Variables;
                return;
            }


            VariableType predefinedLabel = Main.project.sysVarInfo.getPredefineLabel(searchedMI.si.assignedVar);

            switch (predefinedLabel)
            {
                case VariableType.CNFODAQ:
                    Variables = Main.project.sysVarInfo.CNFODAQ_Variables;
                    cbChannelList.Text = "MITSUBISHI";
                    break;
                case VariableType.CALCULATION:
                    Variables = Main.project.sysVarInfo.Calculation_Variables;
                    cbChannelList.Text = "CALCULATION";
                    break;
                case VariableType.ENGINE:
                    Variables = Main.project.sysVarInfo.EngineCom_Variables;
                    cbChannelList.Text = "ENGINE";
                    break;
                case VariableType.RGV:
                    Variables = Main.project.sysVarInfo.RGV_Variables;
                    cbChannelList.Text = "RGV";
                    break;
                case VariableType.NONE:
                    Variables = Main.project.sysVarInfo.CNFODAQ_Variables;
                    teAssignedVarName.Text = "Unassigned";
                    cbChannelList.Text = "MITSUBISHI";
                    break;

            }

            ClearCheck(Variables);

            CheckAssignedVariable(Variables, searchedMI.si.assignedVar);

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }

        private void CheckAssignedVariable(List<VariableInfo> Variables, VariableInfo AssingedVariable)
        {
            foreach (VariableInfo var in Variables)
            {
                if (var == AssingedVariable)
                    var.Checked = true;
            }

            foreach (VariableInfo var in Variables)
            {
                if (var.Checked)
                {

                }
            }
        }
        private void ClearCheck(List<VariableInfo> Variables)
        {
            foreach(VariableInfo var in Variables)
            {
                var.Checked = false;
            }

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            ClearCheck(Variables);

            Variables[gridView1.FocusedRowHandle].Checked = true;
            
            searchedMI.si.assignedVar = (VariableInfo)Variables[gridView1.FocusedRowHandle];
            teAssignedVarName.Text = searchedMI.si.assignedVar.VariableName;

            gridList.BeginUpdate();
            gridList.EndUpdate();
        }

        private void cbChannelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variables = Main.project.sysVarInfo.getVariables(cbChannelList.SelectedIndex);

            ClearCheck(Variables);

            CheckAssignedVariable(Variables, searchedMI.si.assignedVar);

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }
    }
}