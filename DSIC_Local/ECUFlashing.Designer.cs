﻿namespace DSIC_Local
{
    partial class ECUFlashing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ECUFlashing));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.reLog = new DevExpress.XtraRichEdit.RichEditControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.gridQSetting = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColUsage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.cbtnECUPower = new DevExpress.XtraEditors.CheckButton();
            this.cbtnIG_On = new DevExpress.XtraEditors.CheckButton();
            this.btnFlash = new DevExpress.XtraEditors.SimpleButton();
            this.cbFlashType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridQSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFlashType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.Options.UseTextOptions = true;
            this.xtraTabControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.xtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Size = new System.Drawing.Size(1072, 585);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            this.xtraTabControl1.TabPageWidth = 200;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.reLog);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1070, 550);
            this.xtraTabPage1.TabPageWidth = 200;
            this.xtraTabPage1.Text = "ECU Download";
            // 
            // reLog
            // 
            this.reLog.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.reLog.Appearance.Text.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reLog.Appearance.Text.Options.UseFont = true;
            this.reLog.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.reLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reLog.LayoutUnit = DevExpress.XtraRichEdit.DocumentLayoutUnit.Pixel;
            this.reLog.Location = new System.Drawing.Point(0, 0);
            this.reLog.Name = "reLog";
            this.reLog.Size = new System.Drawing.Size(1070, 550);
            this.reLog.TabIndex = 1;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage3.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage3.Controls.Add(this.panelControl5);
            this.xtraTabPage3.Controls.Add(this.panelControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1070, 550);
            this.xtraTabPage3.Text = "Q Setting";
            // 
            // panelControl5
            // 
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Controls.Add(this.gridQSetting);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl5.Location = new System.Drawing.Point(0, 44);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1070, 506);
            this.panelControl5.TabIndex = 3;
            // 
            // gridQSetting
            // 
            this.gridQSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridQSetting.Location = new System.Drawing.Point(0, 0);
            this.gridQSetting.MainView = this.gridView1;
            this.gridQSetting.Name = "gridQSetting";
            this.gridQSetting.Size = new System.Drawing.Size(1070, 506);
            this.gridQSetting.TabIndex = 0;
            this.gridQSetting.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColName,
            this.ColDescription,
            this.ColType,
            this.ColValue,
            this.ColUnit,
            this.ColUsage});
            this.gridView1.GridControl = this.gridQSetting;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // ColName
            // 
            this.ColName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColName.AppearanceHeader.Options.UseFont = true;
            this.ColName.Caption = "이름";
            this.ColName.FieldName = "Name";
            this.ColName.Name = "ColName";
            this.ColName.OptionsFilter.AllowAutoFilter = false;
            this.ColName.OptionsFilter.AllowFilter = false;
            this.ColName.Visible = true;
            this.ColName.VisibleIndex = 0;
            this.ColName.Width = 221;
            // 
            // ColDescription
            // 
            this.ColDescription.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColDescription.AppearanceHeader.Options.UseFont = true;
            this.ColDescription.Caption = "설명";
            this.ColDescription.FieldName = "Description";
            this.ColDescription.Name = "ColDescription";
            this.ColDescription.OptionsFilter.AllowAutoFilter = false;
            this.ColDescription.OptionsFilter.AllowFilter = false;
            this.ColDescription.Visible = true;
            this.ColDescription.VisibleIndex = 1;
            this.ColDescription.Width = 331;
            // 
            // ColType
            // 
            this.ColType.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColType.AppearanceHeader.Options.UseFont = true;
            this.ColType.Caption = "타입";
            this.ColType.FieldName = "strQsettingType";
            this.ColType.Name = "ColType";
            this.ColType.OptionsFilter.AllowAutoFilter = false;
            this.ColType.OptionsFilter.AllowFilter = false;
            this.ColType.Visible = true;
            this.ColType.VisibleIndex = 2;
            this.ColType.Width = 114;
            // 
            // ColValue
            // 
            this.ColValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColValue.AppearanceHeader.Options.UseFont = true;
            this.ColValue.Caption = "값";
            this.ColValue.FieldName = "DisplayValue";
            this.ColValue.Name = "ColValue";
            this.ColValue.OptionsFilter.AllowAutoFilter = false;
            this.ColValue.OptionsFilter.AllowFilter = false;
            this.ColValue.Visible = true;
            this.ColValue.VisibleIndex = 3;
            this.ColValue.Width = 252;
            // 
            // ColUnit
            // 
            this.ColUnit.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColUnit.AppearanceHeader.Options.UseFont = true;
            this.ColUnit.Caption = "단위";
            this.ColUnit.FieldName = "Unit";
            this.ColUnit.Name = "ColUnit";
            this.ColUnit.OptionsFilter.AllowAutoFilter = false;
            this.ColUnit.OptionsFilter.AllowFilter = false;
            this.ColUnit.Visible = true;
            this.ColUnit.VisibleIndex = 4;
            this.ColUnit.Width = 76;
            // 
            // ColUsage
            // 
            this.ColUsage.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColUsage.AppearanceHeader.Options.UseFont = true;
            this.ColUsage.Caption = "사용";
            this.ColUsage.FieldName = "Usage";
            this.ColUsage.Name = "ColUsage";
            this.ColUsage.OptionsFilter.AllowAutoFilter = false;
            this.ColUsage.OptionsFilter.AllowFilter = false;
            this.ColUsage.Visible = true;
            this.ColUsage.VisibleIndex = 5;
            this.ColUsage.Width = 74;
            // 
            // panelControl4
            // 
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.labelControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1070, 44);
            this.panelControl4.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(7, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(240, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Q Setting Parameters Setting";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 589);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1192, 43);
            this.panelControl1.TabIndex = 1;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarControl1.Location = new System.Drawing.Point(0, 0);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(1192, 43);
            this.progressBarControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.cbtnECUPower);
            this.panelControl2.Controls.Add(this.cbtnIG_On);
            this.panelControl2.Controls.Add(this.btnFlash);
            this.panelControl2.Controls.Add(this.cbFlashType);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl2.Location = new System.Drawing.Point(1076, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(116, 589);
            this.panelControl2.TabIndex = 2;
            // 
            // cbtnECUPower
            // 
            this.cbtnECUPower.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cbtnECUPower.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnECUPower.Appearance.Options.UseBackColor = true;
            this.cbtnECUPower.Appearance.Options.UseFont = true;
            this.cbtnECUPower.Location = new System.Drawing.Point(9, 45);
            this.cbtnECUPower.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnECUPower.Name = "cbtnECUPower";
            this.cbtnECUPower.Size = new System.Drawing.Size(100, 36);
            this.cbtnECUPower.TabIndex = 24;
            this.cbtnECUPower.Text = "ECU Power";
            this.cbtnECUPower.Click += new System.EventHandler(this.cbtnECUPower_Click);
            // 
            // cbtnIG_On
            // 
            this.cbtnIG_On.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cbtnIG_On.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbtnIG_On.Appearance.Options.UseBackColor = true;
            this.cbtnIG_On.Appearance.Options.UseFont = true;
            this.cbtnIG_On.Location = new System.Drawing.Point(9, 118);
            this.cbtnIG_On.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cbtnIG_On.Name = "cbtnIG_On";
            this.cbtnIG_On.Size = new System.Drawing.Size(100, 36);
            this.cbtnIG_On.TabIndex = 23;
            this.cbtnIG_On.Text = "Ignition";
            this.cbtnIG_On.Click += new System.EventHandler(this.cbtnIG_On_Click);
            // 
            // btnFlash
            // 
            this.btnFlash.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFlash.Appearance.Options.UseFont = true;
            this.btnFlash.Location = new System.Drawing.Point(9, 467);
            this.btnFlash.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnFlash.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnFlash.Name = "btnFlash";
            this.btnFlash.Size = new System.Drawing.Size(99, 100);
            this.btnFlash.TabIndex = 1;
            this.btnFlash.Text = "Flash";
            this.btnFlash.Click += new System.EventHandler(this.btnFlash_Click);
            // 
            // cbFlashType
            // 
            this.cbFlashType.Location = new System.Drawing.Point(9, 425);
            this.cbFlashType.Name = "cbFlashType";
            this.cbFlashType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFlashType.Properties.Appearance.Options.UseFont = true;
            this.cbFlashType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFlashType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbFlashType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbFlashType.Properties.Items.AddRange(new object[] {
            "시운전용",
            "출하용"});
            this.cbFlashType.Size = new System.Drawing.Size(100, 26);
            this.cbFlashType.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.xtraTabControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1076, 589);
            this.panelControl3.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ECUFlashing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 632);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ECUFlashing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ECU Flashing";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ECUFlashing_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridQSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbFlashType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gridQSetting;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColName;
        private DevExpress.XtraGrid.Columns.GridColumn ColDescription;
        private DevExpress.XtraGrid.Columns.GridColumn ColType;
        private DevExpress.XtraGrid.Columns.GridColumn ColValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColUnit;
        private DevExpress.XtraGrid.Columns.GridColumn ColUsage;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraRichEdit.RichEditControl reLog;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btnFlash;
        private DevExpress.XtraEditors.ComboBoxEdit cbFlashType;
        private DevExpress.XtraEditors.CheckButton cbtnECUPower;
        private DevExpress.XtraEditors.CheckButton cbtnIG_On;
    }
}