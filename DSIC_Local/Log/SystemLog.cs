﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using DevExpress.Images;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Svg;

namespace DSIC_Local.Log
{
    public enum LogType
    {
        None=0,
        Information =1,
        Warning = 2,
        Error =3,
        OK = 4,
    }
    
    public class SystemLog
    {
        DateTime time;
        string message;
        SvgImage iconImage;

        public DateTime Time { get => time; set => time = value; }
        public string Message { get => message; set => message = value; }
        public SvgImage IconImage {
            get
            {
                SvgImage img = null;

                switch (Logtype)
                {
                    case LogType.Error:
                        img = Properties.Resources.Security_WarningCircled1;
                        break;
                    case LogType.Warning:
                        img = Properties.Resources.Warning;
                        break;
                    case LogType.Information:
                        img = Properties.Resources.About;
                        break;
                    case LogType.OK:
                        img = Properties.Resources.Actions_CheckCircled;
                        break;
                    case LogType.None:
                        break;
                }

                return img;
            }
            set => iconImage = value; }
        public LogType Logtype { get; set; }

        public SystemLog(string message, LogType logType, DateTime time)
        {
            this.message = message;
            this.Logtype = logType;
            this.time = time;
            
        }
    }
}
