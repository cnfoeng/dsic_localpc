﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using DSIC_Local.CAN;
using System.Threading;
using System.IO;
using DSIC_Local.Panels;

namespace DSIC_Local
{
    public enum FlashType
    {
        Dyno,
        Shipment,
        None
    }
    public partial class ECUFlashing : DevExpress.XtraEditors.XtraForm
    {
        string ECUPowerStatusName = Properties.Settings.Default.PLC_ECU_Power_STATUS;
        string ECUPowerOnName = Properties.Settings.Default.PLC_ECU_Power_ON;
        string IGStatusName = Properties.Settings.Default.PLC_IG_STATUS;
        string IGOnName = Properties.Settings.Default.PLC_IG_ON;

        static VariableInfo ECUPowerStatusVar;
        static VariableInfo ECUPowerOnVar;
        static VariableInfo IGStatusVar;
        static VariableInfo IGOnVar;

        ASAP2AdjustData aSAP2AdjustData = new ASAP2AdjustData();

        ISOTP_UDS isotp_uds;

        Thread thFlash;

        public string checksum_Kline;

        public static List<string> FlashLogs = new List<string>();

        FlashType flashType;

        public ECUFlashing(FlashType flashType)
        {
            
            InitializeComponent();
            gridQSetting.DataSource = Main.project.currentEngine.qsettingParameters;

            ECUPowerStatusVar = Main.project.sysVarInfo.FindVariable(ECUPowerStatusName);
            ECUPowerOnVar = Main.project.sysVarInfo.FindVariable(ECUPowerOnName);
            IGStatusVar = Main.project.sysVarInfo.FindVariable(IGStatusName);
            IGOnVar = Main.project.sysVarInfo.FindVariable(IGOnName);

            isotp_uds = Main.isotp_uds;

            cbFlashType.SelectedIndex = 0;

            if (Main.project.currentEngine.Engine == Engine.DV11)
            {
                xtraTabPage3.PageVisible = true;
            }
            else if(Main.project.currentEngine.Engine == Engine.DX22)
            {
                xtraTabPage3.PageVisible = false;
            }

            this.flashType = flashType;

            if (flashType == FlashType.Dyno)
            {
                cbFlashType.SelectedIndex = 0;
            }
            else if (flashType == FlashType.Shipment)
            {
                cbFlashType.SelectedIndex = 1;
            }

            
        }
        

        void StopFlashThread()
        {
            if (thFlash != null)
            {
                thFlash.Abort();
            }
        }

        void ECUFullFlash_CAN()
        {
            FlashLogs.Add("ECU 전원조작 중...");
            ReadyFlash(4000);

            isotp_uds.ECUFlash(ISOTP_UDS.FlashType.FullFlash);

            if (flashType == FlashType.Shipment)
            {
                ECUPowerAndIGOff();
                FlashLogs.Add("ECU Power, IG Off");
            }
        }
        void ECUDataFlash_CAN()
        {
            FlashLogs.Add("ECU 전원조작 중...");
            ReadyFlash(4000);

            isotp_uds.ECUFlash(ISOTP_UDS.FlashType.DataOnlyFlash);

            if (flashType == FlashType.Shipment)
            {
                ECUPowerAndIGOff();
                FlashLogs.Add("ECU Power, IG Off");
            }
        }
        void ECUDataFlash_KLine()
        {
            FlashLogs.Add("ECU 전원조작 중...");
            ReadyFlash(10000);
            Main.kline.OBDCommunicationStop();
            Main.kline.OBDCommunicationStart();
            Main.kline.OBDCommunicationFlash(checksum_Kline);
            Main.project.currentEngine.ClearMemorySegment();

            if (flashType == FlashType.Shipment)
            {
                ECUPowerAndIGOff();
                FlashLogs.Add("ECU Power, IG Off");
            }
        }
        public static void ReadyFlash(int msec)
        {
            
            if (ECUPowerStatusVar.RealValue == 1 && IGStatusVar.RealValue == 1)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();


                Thread.Sleep(msec);

                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

            }
            else if (ECUPowerStatusVar.RealValue == 1 && IGStatusVar.RealValue == 0)
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();
            }
            else if (ECUPowerStatusVar.RealValue == 0 && IGStatusVar.RealValue == 1)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();
            }
            else if (ECUPowerStatusVar.RealValue == 0 && IGStatusVar.RealValue == 0)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                Thread.Sleep(msec);

                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();
            }

            Thread.Sleep(msec);
        }
        public static void ECUPowerAndIGOff()
        {
            //상태를 하나만 두는 이유는 ECUPowerAndIGOff는 자동모드 - 출하맵 플래시 후에만 실행되는데 무조건 상태가 ECUPower On, IG On인 상태일 것이다.

            if (ECUPowerStatusVar.RealValue == 1 && IGStatusVar.RealValue == 1)
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                Thread.Sleep(2000);
                
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();
            }
        }
        private void AddLog(string msg)
        {
            DocumentPosition pos = reLog.Document.Range.End;
            SubDocument doc = pos.BeginUpdateDocument();
            
            doc.InsertText(pos, msg + "\n");
            pos.EndUpdateDocument(doc);

            reLog.Document.CaretPosition = reLog.Document.Range.End;
            reLog.ScrollToCaret();
        }

        private void ECUFlash()
        {
            if (Main.localMapFilePath == "")
            {
                XtraMessageBox.Show("<size=14>Map 파일 경로가 지정되지 않았습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!File.Exists(Main.localMapFilePath))
            {
                XtraMessageBox.Show("<size=14>Map 파일이 경로에 존재하지 않습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (cbFlashType.SelectedIndex == 0) //시운전맵
            {
                if (Main.project.currentEngine.Engine == Engine.DV11)
                {
                    Main.kline.StopGetData();
                    StopFlashThread();

                    string destFile = aSAP2AdjustData.MakeNewHexFile(Main.project.currentEngine, FlashType.Dyno);//시운전맵 생성

                    if (destFile != null)
                    {
                        Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, destFile);
                        //Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, Application.StartupPath+ "\\" + Main.project.currentEngine.DynoHexPath);
                        FlashLogs.Add("시운전맵 로드 성공");
                    }
                    else
                    {
                        FlashLogs.Add("시운전맵 생성 실패");
                        return;
                    }

                    StringBuilder dataForCalculateChecksum = new StringBuilder();

                    foreach (ExtractedHexFileData extractedHexFile in Main.project.currentEngine.extractedHexFiles)
                    {
                        if (extractedHexFile.prgType == jnsoft.ASAP2.MEMORYPRG_TYPE.DATA || extractedHexFile.prgType == jnsoft.ASAP2.MEMORYPRG_TYPE.CODE)
                        {
                            dataForCalculateChecksum.Append(extractedHexFile.Data);
                        }
                    }

                    checksum_Kline = aSAP2AdjustData.CalculateChecksum(dataForCalculateChecksum.ToString());

                    thFlash = new Thread(new ThreadStart(ECUDataFlash_KLine));
                    thFlash.Priority = ThreadPriority.Highest;
                    thFlash.Start();
                }
                else if (Main.project.currentEngine.Engine == Engine.DX22)
                {
                    Main.canCom.StopGetData();
                    StopFlashThread();

                    Main.AddLog("Flash 시작", Log.LogType.OK);

                    isotp_uds.status = ISOTP_UDS.CANStatus.Flash;

                    string shipmentFileNameOnly = Path.GetFileNameWithoutExtension(Main.localMapFilePath);
                    string extension = Path.GetExtension(Main.localMapFilePath);
                    string dynoFileNameOnly = shipmentFileNameOnly + "_D";
                    string directoryPath = Path.GetDirectoryName(Main.localMapFilePath);
                    string dynoLocalFilePath = directoryPath + "\\" + dynoFileNameOnly + extension;
                    try
                    {
                        Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, dynoLocalFilePath);

                        thFlash = new Thread(new ThreadStart(ECUFullFlash_CAN));
                        thFlash.Priority = ThreadPriority.Highest;
                        thFlash.Start();
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        XtraMessageBox.Show("<size=14>시운전 Map 파일을 찾을 수 없습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            else if (cbFlashType.SelectedIndex == 1) //출하맵
            {
                if (Main.project.currentEngine.Engine == Engine.DV11)
                {
                    Main.kline.StopGetData();
                    StopFlashThread();

                    string destFile = aSAP2AdjustData.MakeNewHexFile(Main.project.currentEngine, FlashType.Shipment);//시운전맵 생성

                    if (destFile != null)
                    {
                        Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, destFile);
                        //Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, Application.StartupPath+ "\\" + Main.project.currentEngine.DynoHexPath);
                        FlashLogs.Add("출하맵 정보 입력 성공");
                    }
                    else
                    {
                        FlashLogs.Add("출하맵 정보 입력 실패");
                        return;
                    }

                    Main.AddLog("Flash 시작", Log.LogType.OK);

                    Main.kline.status = KWP2000.KlineStatus.Flash;

                    //Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, Main.localMapFilePath);//생각해봐야함. 유로 버전 어떻게 판단할지. 그냥 불러오면 무슨버전인지 모른다. 사람이 직접 설정해주던가 해야함
                    //Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, Application.StartupPath+ "\\" + Main.project.currentEngine.ShipmentHexPath);

                    StringBuilder dataForCalculateChecksum = new StringBuilder();

                    foreach (ExtractedHexFileData extractedHexFile in Main.project.currentEngine.extractedHexFiles)
                    {
                        if (extractedHexFile.prgType == jnsoft.ASAP2.MEMORYPRG_TYPE.DATA || extractedHexFile.prgType == jnsoft.ASAP2.MEMORYPRG_TYPE.CODE)
                        {
                            dataForCalculateChecksum.Append(extractedHexFile.Data);
                        }
                    }

                    checksum_Kline = aSAP2AdjustData.CalculateChecksum(dataForCalculateChecksum.ToString());

                    thFlash = new Thread(new ThreadStart(ECUDataFlash_KLine));
                    thFlash.Priority = ThreadPriority.Highest;
                    thFlash.Start();
                }
                else if (Main.project.currentEngine.Engine == Engine.DX22)
                {
                    Main.canCom.StopGetData();
                    StopFlashThread();

                    Main.AddLog("Flash 시작", Log.LogType.OK);

                    isotp_uds.status = ISOTP_UDS.CANStatus.Flash;

                    Main.project.currentEngine.extractedHexFiles = aSAP2AdjustData.ExtractDataFromHexFile(Main.project.currentEngine, Main.localMapFilePath);

                    thFlash = new Thread(new ThreadStart(ECUFullFlash_CAN));
                    thFlash.Priority = ThreadPriority.Highest;
                    thFlash.Start();

                }
            }
        }

        private void ECUFlashing_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            BasicScreen.ecuFlashingForm = null;

            thFlash?.Abort();
            thFlash?.Join();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (FlashLogs.Count > 0)
            {
                AddLog(FlashLogs[0]);
                FlashLogs.RemoveAt(0);
                Thread.Sleep(10);
            }

            if (Main.project.currentEngine.Engine == Engine.DV11)
            {
                progressBarControl1.Properties.Maximum = Main.kline.progressbarMaximum;
                progressBarControl1.EditValue = Main.kline.progressbarValue;
            }
            else if(Main.project.currentEngine.Engine == Engine.DX22)
            {
                progressBarControl1.Properties.Maximum = Main.isotp_uds.progressbarMaximum;
                progressBarControl1.EditValue = Main.isotp_uds.progressbarValue;
            }

            //if(Main.project.currentEngine.Engine == Engine.DV11 && btnStart.Enabled)
            //{
            //    btnStart.Enabled = false;
            //}
            //Todo Flash끝나고 쓰레드 종료하는거 넣어주기.


            if (ECUPowerStatusVar.RealValue == 1)
            {
                cbtnECUPower.Checked = true;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnECUPower.Checked = false;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (IGStatusVar.RealValue == 1)
            {
                cbtnIG_On.Checked = true;
                cbtnIG_On.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnIG_On.Checked = false;
                cbtnIG_On.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

        }

        private void btnFlash_Click(object sender, EventArgs e)
        {
            ECUFlash();
        }

        private void cbtnECUPower_Click(object sender, EventArgs e)
        {
            if (cbtnECUPower.Checked)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }

        private void cbtnIG_On_Click(object sender, EventArgs e)
        {
            if (cbtnIG_On.Checked)
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnIG_On.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnIG_On.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }
    }
}