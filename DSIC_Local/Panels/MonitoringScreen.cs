﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DSIC_Local.Monitoring;

namespace DSIC_Local.Panels
{
    public partial class MonitoringScreen : UserControl
    {
        public MonitoringForm monitoringForm;
        public MonitoringScreen(List<MonitorItem> monitorItems)
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            monitoringForm = new MonitoringForm("monitoring_Monitor", monitorItems);
            panelControl1.Controls.Add(monitoringForm);
        }

        public void RefreshDocument()
        {
            monitoringForm.RefreshAllDocuments();
        }
    }
}
