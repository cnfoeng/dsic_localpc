﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using DSIC_Local.Step;

namespace DSIC_Local.Panels
{
    public partial class BasicScreen : UserControl
    {
        string ECUPowerStatusName = Properties.Settings.Default.PLC_ECU_Power_STATUS;
        string ECUPowerOnName = Properties.Settings.Default.PLC_ECU_Power_ON;
        string IGStatusName = Properties.Settings.Default.PLC_IG_STATUS;
        string IGOnName = Properties.Settings.Default.PLC_IG_ON;
        string EngineStartStatusName = Properties.Settings.Default.PLC_Engine_Start_STATUS;
        string EngineStartOnName = Properties.Settings.Default.PLC_Engine_Start_ON;


        VariableInfo ECUPowerStatusVar;
        VariableInfo ECUPowerOnVar;
        VariableInfo IGStatusVar;
        VariableInfo IGOnVar;
        VariableInfo EngineStartStatusVar;
        VariableInfo EngineStartOnVar;
        VariableInfo modeSpeedVar;
        VariableInfo modeTorqueVar;
        VariableInfo dynoControlVar;
        VariableInfo engineControlVar;

        public BasicScreen()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            ECUPowerStatusVar = Main.project.sysVarInfo.FindVariable(ECUPowerStatusName);
            ECUPowerOnVar = Main.project.sysVarInfo.FindVariable(ECUPowerOnName);
            IGStatusVar = Main.project.sysVarInfo.FindVariable(IGStatusName);
            IGOnVar = Main.project.sysVarInfo.FindVariable(IGOnName);
            EngineStartStatusVar = Main.project.sysVarInfo.FindVariable(EngineStartStatusName);
            EngineStartOnVar = Main.project.sysVarInfo.FindVariable(EngineStartOnName);

            gridControl1.DataSource = Main.logs;

            tbEngineMeasuringFilePath.Text = Main.localEngineMeasuringFilePath;
            tbEngineFaultListFilePath.Text = Main.localEngineFaultListFilePath;
        }
        public static ECUFlashing ecuFlashingForm = null;
        private void btnECUDownload_Click(object sender, EventArgs e)
        {
            if (ecuFlashingForm == null)
            {
                ecuFlashingForm = new ECUFlashing(FlashType.None);
                ecuFlashingForm.Show();
            }
            else
            {
                ecuFlashingForm.BringToFront();
            }
        }
        int prevCount = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            int count = Main.logs.Count;

            if (prevCount != count)
            {
                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }

            if (ECUPowerStatusVar.RealValue == 1)
            {
                cbtnECUPower.Checked = true;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnECUPower.Checked = false;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (IGStatusVar.RealValue == 1)
            {
                cbtnIG_On.Checked = true;
                cbtnIG_On.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnIG_On.Checked = false;
                cbtnIG_On.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (EngineStartStatusVar.RealValue == 1)
            {
                cbtnEngineStart.Checked = true;
                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnEngineStart.Checked = false;
                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }


            tbEngineNumber.Text = Main.resultData.engineNumber;
        }

        private void cbtnECUPower_Click(object sender, EventArgs e)
        {
            if (cbtnECUPower.Checked)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }

        private void cbtnIG_On_Click(object sender, EventArgs e)
        {

            if (cbtnIG_On.Checked)
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnIG_On.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.IgOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.IgOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnIG_On.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }

        private void cbtnEngineStart_Click(object sender, EventArgs e)
        {
            Idle();
            System.Threading.Thread.Sleep(500);

            if (cbtnEngineStart.Checked)
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();



                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }
        public void Idle()
        {
            string modeSpeedVariableName = Properties.Settings.Default.BME_Mode_Speed;
            string modeTorqueVariableName = Properties.Settings.Default.BME_Mode_Torque;
            string speedControlVariableName = Properties.Settings.Default.BME_Speed_Demand;
            string torqueControlVariableName = Properties.Settings.Default.BME_Torque_Demand;
            string throttleControlVariableName = Properties.Settings.Default.PLC_Throttle_Demand;

            modeSpeedVar = Main.project.sysVarInfo.FindVariable(modeSpeedVariableName);
            modeTorqueVar = Main.project.sysVarInfo.FindVariable(modeTorqueVariableName);

            if (modeSpeedVar.RealValue == 1 && modeTorqueVar.RealValue == 0)
            {
                dynoControlVar = Main.project.sysVarInfo.FindVariable(speedControlVariableName);
                engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                double CurrentDyno = StepMode.GetIOValue(3000, dynoControlVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, engineControlVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(dynoControlVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(engineControlVar.NorminalDescription, (int)CurrentEngine);
            }
            else if (modeSpeedVar.RealValue == 0 && modeTorqueVar.RealValue == 1)
            {
                dynoControlVar = Main.project.sysVarInfo.FindVariable(torqueControlVariableName);
                engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                double CurrentDyno = StepMode.GetIOValue(0, dynoControlVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, engineControlVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(dynoControlVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(engineControlVar.NorminalDescription, (int)CurrentEngine);
            }
        }
        private void btnOpenMapFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialogMap.ShowDialog();
            //xtraOpenFileDialogMap.FileName = "";
            string fileName = xtraOpenFileDialogMap.FileName;
            if (fileName != "")
            {
                string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);

                string last2 = fileNameOnly.Substring(fileNameOnly.Length - 2, 2);
                bool isDynoMapCondition1 = (last2 == "_D") ? true : false;
                bool isDynoMapCondition2 = fileNameOnly.Contains("dyno");
                bool isDynoMapCondition3 = fileNameOnly.Contains("Dyno");
                bool isDynoMapCondition4 = fileNameOnly.Contains("DYNO");

                if (isDynoMapCondition1 || isDynoMapCondition2 || isDynoMapCondition3 || isDynoMapCondition4)
                {
                    XtraMessageBox.Show("<size=14>출하용 맵을 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                tbMapFilePath.Text = fileName;
                Main.localMapFilePath = fileName;
                Main.resultData.mapFilePath = fileNameOnly + ".hex";
            }
        }

        private void btnOpenScheduleFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialogMap.ShowDialog();
            string fileName = xtraOpenFileDialogMap.FileName;
            if (fileName != "")
            {
                string fileNameOnly = Path.GetFileNameWithoutExtension(fileName);

                tbScheduleFilePath.Text = fileName;
                Main.localScheduleFilePath = fileName;
                Main.resultData.schduleFilePath = fileNameOnly + ".csv";
            }
        }

        private void btnOpenMeasuringFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialogMap.ShowDialog();
            string fileName = xtraOpenFileDialogMap.FileName;
            if (fileName != "")
            {
                tbEngineMeasuringFilePath.Text = fileName;
                Main.localEngineMeasuringFilePath = fileName;
            }
        }

        private void btnOpenFaultListFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialogMap.ShowDialog();
            string fileName = xtraOpenFileDialogMap.FileName;
            if (fileName != "")
            {
                tbEngineFaultListFilePath.Text = fileName;
                Main.localEngineFaultListFilePath = fileName;
            }
        }
        public string BarcodeRead(string barcode)
        {
            Tuple<DataSet, string> tuple = Main.database.SelectDataFromEngineNumber(barcode);
            //위 검색 결과는 1개만 존재한다고 가정한다. 반드시 그래야 함.

            if (tuple == null || tuple.Item1.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            object[] results = tuple.Item1.Tables[0].Rows[0].ItemArray;

            tbEngineNumber.Text = Main.resultData.engineNumber = barcode;
            string turboCharger = string.Empty;
            if (results[6].ToString() != "")
            {
                turboCharger = (bool)results[6] ? "YES" : "NO";
            }
            else
            {
                turboCharger = "NO";
            }
            cbTurboCharger.Text = Main.resultData.turboCharger = turboCharger;
            tbSuffix.Text = Main.resultData.suffix = results[0].ToString();
            tbModel.Text = Main.resultData.model = results[1].ToString();
            tbPurpose.Text = Main.resultData.forWhat = results[5].ToString();
            tbScheduleFilePath.Text = Main.localScheduleFilePath = Main.resultData.schduleFilePath = results[10].ToString();
            tbMapFilePath.Text = Main.localMapFilePath = Main.resultData.mapFilePath = results[9].ToString();

            tbEngineMeasuringFilePath.Text = Main.localEngineMeasuringFilePath = results[7].ToString();
            tbEngineFaultListFilePath.Text = Main.localEngineFaultListFilePath = results[8].ToString();

            Main.resultData.engineCategory = results[2].ToString();
            Main.resultData.engineType = results[3].ToString();

            return Main.resultData.suffix = results[0].ToString();//Suffix
        }
        

        private void tbEngineNumber_EditValueChanged(object sender, EventArgs e)
        {
            Main.resultData.engineNumber = tbEngineNumber.Text;
        }

    }
}
