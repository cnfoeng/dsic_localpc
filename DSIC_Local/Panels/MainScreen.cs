﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DSIC_Local.Monitoring;
using DSIC_Local.Step;


namespace DSIC_Local.Panels
{
    public partial class MainScreen : UserControl
    {
        public MonitoringForm monitoringForm;


        public MainScreen(List<MonitorItem> monitorItems)
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            monitoringForm = new MonitoringForm("main_Monitor", monitorItems);
            panelMonitoring.Controls.Add(monitoringForm);

            ContextMenuStrip cmGridview1 = new ContextMenuStrip();
            cmGridview1.Items.Add("결과 파일화", null, btnMakeCSV_Click);
            gridMeasurement.ContextMenuStrip = cmGridview1;
        }
        public void UpdateGridStepDataSource()
        {
            gridStep.DataSource = Main.stepMode.steps;

            gridMeasurement.DataSource = Main.stepmodeOutput.outputs;

            gridStep.BeginUpdate();
            gridStep.EndUpdate();
        }
        public void RefreshDocument()
        {
            monitoringForm.RefreshAllDocuments();
        }
        private void btnMakeCSV_Click(object sender, EventArgs e)
        {
            System.IO.Stream stream = new System.IO.FileStream(Application.StartupPath + "\\ResultFile_Local\\" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".pdf", System.IO.FileMode.Create);
            gridMeasurement.ExportToPdf(stream);
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Main.stepMode.steps.Count != 0)
            {
                bandedGridView1.FocusedRowHandle = Main.stepMode.currentStep;
            }

            if (Main.stepmodeOutput.isUpdated)
            {
                gridMeasurement.BeginUpdate();
                gridMeasurement.EndUpdate();
                Main.stepmodeOutput.isUpdated = false;
            }

            

            gridMeasurement.BeginUpdate();
            gridMeasurement.EndUpdate();

            if (Main.isotp_uds.isConnected || Main.kline.bConnected)
            {
                btnECUFaultCheck.Enabled = true;
                btnECUFaultClear.Enabled = true;

                bool havingFault = false;

                if(Main.project.currentEngine.ECUFaults == null)
                {
                    return;
                }

                foreach(ECUFault fault in Main.project.currentEngine.ECUFaults)
                {
                    if (fault.Checked)
                    {
                        havingFault = true;
                        break;
                    }
                }

                if (havingFault)
                {
                    tbDTCStatus.Text = "진단 중 - Fault 발견";
                    tbDTCStatus.BackColor = Color.Red;
                }
                else
                {
                    tbDTCStatus.Text = "진단 중 - OK";
                    tbDTCStatus.BackColor = Color.LightGreen;
                }
            }
            else
            {
                btnECUFaultCheck.Enabled = false;
                btnECUFaultClear.Enabled = false;

                tbDTCStatus.Text = "진단 대기";
                tbDTCStatus.BackColor = Color.FromArgb(48, 56, 65);
            }

            if (Main.stepMode.pause)
            {
                btnStepFirst.Enabled = true;
                btnStepPrev.Enabled = true;
                btnStepNext.Enabled = true;
            }
            else
            {
                btnStepFirst.Enabled = false;
                btnStepPrev.Enabled = false;
                btnStepNext.Enabled = false;
            }
            
            tbEngineNumber.Text = Main.resultData.engineNumber;
            tbWorker.Text = Main.project.worker;
            tbStartDate.Text = Main.resultData.startDate;
            tbStartTime.Text = Main.resultData.startTime;
            tbCurrentTime.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        private void btnECUFaultCheck_Click(object sender, EventArgs e)
        {
            Main.isotp_uds.doReadDTC = true;

            System.Threading.Thread.Sleep(1000);

            ECUFaultCheck form = new ECUFaultCheck();
            form.Show();
        }

        private void btnECUFaultClear_Click(object sender, EventArgs e)
        {
            if (Main.project.currentEngine.Engine == Engine.DX22)
            {
                Main.isotp_uds.doClear = true;
            }
            else if(Main.project.currentEngine.Engine == Engine.DV11)
            {
                Main.kline.doClear = true;
            }
        }

        private void btnStepPrev_Click(object sender, EventArgs e)
        {
            Main.stepMode.GoPrevStep();
        }

        private void btnStepNext_Click(object sender, EventArgs e)
        {
            Main.stepMode.GoNextStep();
        }

        private void btnStepFirst_Click(object sender, EventArgs e)
        {
            Main.stepMode.GoFirstStep();
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            var row = (Output)gridView1.GetRow(e.RowHandle);

            if(row == null)
            {
                return;
            }

            if (e.RowHandle >= 0 && row.StepNumber == Main.stepMode.currentStep + 1)
            {
                e.Appearance.BackColor = Color.LightGray;
            }
        }
    }
}
