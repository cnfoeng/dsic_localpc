﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Local.Panels
{
    public partial class ResultScreen : UserControl
    {
        int flag;

        public ResultScreen()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        public void PassOrNot()
        {
            ChangePassOrNotUI();
        }

        private int PassOrNotLogic()
        {
            flag = 0;

            if(Main.stepMode.steps == null || Main.stepMode.steps.Count == 0)
            {
                flag = 0;//스텝모드 시작 X
                return flag;
            }

            foreach (Step.Step step in Main.stepMode.steps)
            {
                foreach (Step.MeasuringChannel mc in step.measuringChannels)
                {
                    if (mc.CompleteMeasuring != true)
                    {
                        flag = 0;//메저링이 다 끝나지 않음
                        return flag;
                    }
                }
            }

            foreach (Step.Output output in Main.stepmodeOutput.outputs)
            {

                if (output.Ok != "O")
                {
                    flag = 2;//메저링은 모두 했으나 불합격 판정 존재
                    return flag;
                }
            }

            flag = 1;//모두 메저링했고 합격
            return flag;
        }
        public void ChangePassOrNotUI()
        {
            flag = PassOrNotLogic();
            if (flag == 0)//미판정
            {
                lbPassOrNot.Text = "미      판      정";
                lbPassOrNot.BackColor = Color.White;
            }
            else if (flag == 1)//합격
            {
                lbPassOrNot.Text = "합      격";
                lbPassOrNot.BackColor = Color.LightGreen;
            }
            else if (flag == 2)//불합격
            {
                lbPassOrNot.Text = "불      합      격";
                lbPassOrNot.BackColor = Color.LightCoral;
            }
        }
        public void MakeGridLeft()
        {
            DataTable dt = new DataTable("Left");
            dt.Columns.Add("Item", typeof(string));
            dt.Columns.Add("Content", typeof(string));

            dt.Rows.Add(new object[] { "엔진번호", Main.resultData.engineNumber });
            dt.Rows.Add(new object[] { "SUFFIX", Main.resultData.suffix });
            dt.Rows.Add(new object[] { "MODEL", Main.resultData.model });
            dt.Rows.Add(new object[] { "용도", Main.resultData.forWhat });
            dt.Rows.Add(new object[] { "Schedule File", Main.resultData.schduleFilePath });
            dt.Rows.Add(new object[] { "터보차저", Main.resultData.turboCharger });
            dt.Rows.Add(new object[] { "연료비중", Main.resultData.fuelSpecificGravity });
            dt.Rows.Add(new object[] { "ECU Map File", Main.resultData.mapFilePath });
            dt.Rows.Add(new object[] { "인젝터정보", Main.resultData.injectorInformation });

            gridLeft.DataSource = dt;
        }
        public void MakeGridRight()
        {
            DataTable dt = new DataTable("Right");
            dt.Columns.Add("Item", typeof(string));
            dt.Columns.Add("Content", typeof(string));

            dt.Rows.Add(new object[] { "시험시작일", Main.resultData.startDate });
            dt.Rows.Add(new object[] { "시험시작시간", Main.resultData.startTime });
            dt.Rows.Add(new object[] { "시험종료시간", Main.resultData.endTime });
            dt.Rows.Add(new object[] { "시험장소", Main.project.cellNumber });
            dt.Rows.Add(new object[] { "작업자", Main.project.worker });
            dt.Rows.Add(new object[] { "검사자", Main.project.inspector });

            gridRight.DataSource = dt;
        }
        DataTable ResultDT;
        public void MakeGridMeasurementTable()
        {
            //기존 Class만들어서 하는방식보다 DataTable만들어서 적용하는게 나을듯.

            ResultDT = new DataTable("Result");//string이 TableName이다.
            ResultDT.Columns.Add("Step", typeof(string));
            ResultDT.Columns.Add("StepName", typeof(string));
            ResultDT.Columns.Add("StepTime", typeof(string));
            ResultDT.Columns.Add("RPM", typeof(string));
            ResultDT.Columns.Add("OutputItem", typeof(string));
            ResultDT.Columns.Add("MeasuringChannel", typeof(string));
            ResultDT.Columns.Add("Value", typeof(string));
            ResultDT.Columns.Add("Unit", typeof(string));
            ResultDT.Columns.Add("Condition", typeof(string));
            ResultDT.Columns.Add("Ok", typeof(string));

            bool firstSmoke = true;
            bool firstLoadSmoke = true;
            bool firstFASmoke = true;

            int rowNumSmoke = -1;
            int rowNumLoadSmoke = -1;
            int rowNumFASmoke = -1;

            double sumValueSmoke = 0;
            double sumValueLoadSmoke = 0;
            double sumValueFASmoke = 0;

            int countSmoke = 0;
            int countLoadSmoke = 0;
            int countFASmoke = 0;

            foreach (Step.Output output in Main.stepmodeOutput.outputs)
            {
                if (output.enumOutputItem == Step.OutputItem.SMOKE)
                {
                    if (firstSmoke)
                    {
                        ResultDT.Rows.Add(new object[] { output.StepNumber, output.StepName, output.StepTotalTime, output.DynoSetValue, output.OutputItem, output.MeasuringChannel, output.Value, output.Unit, output.Condition, output.Ok });
                        firstSmoke = false;
                        rowNumSmoke = ResultDT.Rows.Count - 1;
                        sumValueSmoke += output.Value;
                        countSmoke++;
                    }
                    else
                    {
                        sumValueSmoke += output.Value;
                        countSmoke++;
                        double averageValue = Math.Round(sumValueSmoke / countSmoke, 1);
                        ResultDT.Rows[rowNumSmoke]["Value"] = averageValue;

                        if (averageValue <= output.HighValue && averageValue >= output.LowValue)
                        {
                            ResultDT.Rows[rowNumSmoke]["Ok"] = "O";
                        }
                        else
                        {
                            ResultDT.Rows[rowNumSmoke]["Ok"] = "X";
                        }
                    }
                }
                else if (output.enumOutputItem == Step.OutputItem.LOADSMOKE)
                {
                    if (firstLoadSmoke)
                    {
                        ResultDT.Rows.Add(new object[] { output.StepNumber, output.StepName, output.StepTotalTime, output.DynoSetValue, output.OutputItem, output.MeasuringChannel, output.Value, output.Unit, output.Condition, output.Ok });
                        firstLoadSmoke = false;
                        rowNumLoadSmoke = ResultDT.Rows.Count - 1;
                        sumValueLoadSmoke += output.Value;
                        countLoadSmoke++;
                    }
                    else
                    {
                        sumValueLoadSmoke += output.Value;
                        countLoadSmoke++;
                        double averageValue = Math.Round(sumValueLoadSmoke / countLoadSmoke, 1);
                        ResultDT.Rows[rowNumLoadSmoke]["Value"] = averageValue;

                        if (averageValue <= output.HighValue && averageValue >= output.LowValue)
                        {
                            ResultDT.Rows[rowNumLoadSmoke]["Ok"] = "O";
                        }
                        else
                        {
                            ResultDT.Rows[rowNumLoadSmoke]["Ok"] = "X";
                        }
                    }
                }
                else if (output.enumOutputItem == Step.OutputItem.FASMOKE)
                {
                    if (firstFASmoke)
                    {
                        ResultDT.Rows.Add(new object[] { output.StepNumber, output.StepName, output.StepTotalTime, output.DynoSetValue, output.OutputItem, output.MeasuringChannel, output.Value, output.Unit, output.Condition, output.Ok });
                        firstFASmoke = false;
                        rowNumFASmoke = ResultDT.Rows.Count - 1;
                        sumValueFASmoke += output.Value;
                        countFASmoke++;
                    }
                    else
                    {
                        sumValueFASmoke += output.Value;
                        countFASmoke++;
                        double averageValue = Math.Round(sumValueFASmoke / countFASmoke, 1);
                        ResultDT.Rows[rowNumFASmoke]["Value"] = averageValue;

                        if (averageValue <= output.HighValue && averageValue >= output.LowValue)
                        {
                            ResultDT.Rows[rowNumFASmoke]["Ok"] = "O";
                        }
                        else
                        {
                            ResultDT.Rows[rowNumFASmoke]["Ok"] = "X";
                        }

                    }
                }
                else
                {
                    ResultDT.Rows.Add(new object[] { output.StepNumber, output.StepName, output.StepTotalTime, output.DynoSetValue, output.OutputItem, output.MeasuringChannel, output.Value, output.Unit, output.Condition, output.Ok });
                }
            }

            gridMeasurement.DataSource = ResultDT;
        }

        private void ResultScreen_Load(object sender, EventArgs e)
        {

        }

        private void btnSendDataToServer_Click(object sender, EventArgs e)
        {
            if(Main.resultData.engineNumber.Length < 10)
            {
                XtraMessageBox.Show("<size=14>엔진 번호가 공백이거나 너무 짧은 상태로 DB 입력이 불가능합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Main.resultData.endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string APS_rpm = "0";
            string APS_min = "0";
            string APS_max = "0";
            string APS = "0";

            string ATQ_rpm = "0";
            string ATQ_min = "0";
            string ATQ_max = "0";
            string ATQ = "0";

            string ASMOK_rpm = "0";
            string ASMOK_max = "0";
            string ASMOK = "0";

            string AFASMOK_rpm = "0";
            string AFASMOK_max = "0";
            string AFASMOK = "0";

            string ALOILP_min = "0";
            string ALOILP_max = "0";
            string ALOILP = "0";

            string AFOILP_min = "0";
            string AFOILP_max = "0";
            string AFOILP = "0";

            string AJOILP_rpm = "0";
            string AJOILP_min = "0";
            string AJOILP_max = "0";
            string AJOILP = "0";

            string ALRPM_rpm = "0";
            string ALRPM_min = "0";
            string ALRPM_max = "0";
            string ALRPM = "0";

            string AFRPM_rpm = "0";
            string AFRPM_min = "0";
            string AFRPM_max = "0";
            string AFRPM = "0";

            string ABSFC_rpm = "0";
            string ABSFC_min = "0";
            string ABSFC_max = "0";
            string ABSFC = "0";


            foreach (DataRow dr in ResultDT.Rows)
            {
                if (dr["OutputItem"].ToString() == "MAXPOWER")
                {
                    APS_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    APS_min = conditions[0];
                    APS_max = conditions[2];
                    APS = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "MAXTORQUE")
                {
                    ATQ_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    ATQ_min = conditions[0];
                    ATQ_max = conditions[2];
                    ATQ = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "LOADSMOKE" || dr["OutputItem"].ToString() == "SMOKE")
                {
                    ASMOK_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    ASMOK_max = conditions[2];
                    ASMOK = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "FASMOKE")
                {
                    AFASMOK_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    AFASMOK_max = conditions[2];
                    AFASMOK = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "IDLEOILPRESSURE")
                {
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    ALOILP_min = conditions[0];
                    ALOILP_max = conditions[2];
                    ALOILP = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "FULLOILPRESSURE")
                {
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    AFOILP_min = conditions[0];
                    AFOILP_max = conditions[2];
                    AFOILP = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "JBOILPRESSURE")
                {
                    AJOILP_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    AJOILP_min = conditions[0];
                    AJOILP_max = conditions[2];
                    AJOILP = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "IDLERPM")
                {
                    ALRPM_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    ALRPM_min = conditions[0];
                    ALRPM_max = conditions[2];
                    ALRPM = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "MAXRPM")
                {
                    AFRPM_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    AFRPM_min = conditions[0];
                    AFRPM_max = conditions[2];
                    AFRPM = dr["Value"].ToString();
                }
                else if (dr["OutputItem"].ToString() == "FUELCONSUMPTION")
                {
                    ABSFC_rpm = dr["RPM"].ToString();
                    string[] seperateString = { "<=" };
                    string[] conditions = dr["Condition"].ToString().Split(seperateString, System.StringSplitOptions.RemoveEmptyEntries);
                    ABSFC_min = conditions[0];
                    ABSFC_max = conditions[2];

                    ABSFC = dr["Value"].ToString();
                    if (ABSFC == "NaN")
                    {
                        ABSFC = "0";
                    }

                }

            }

            bool ok = Main.database.InsertResultData(Main.resultData.engineNumber,
                Main.resultData.suffix,
                Main.resultData.model,
                Main.resultData.engineType,
                Main.resultData.engineCategory,
                Main.resultData.schduleFilePath,
                Main.resultData.mapFilePath,
                Main.resultData.turboCharger,
                Main.resultData.endTime,
                Main.resultData.AirTemp.ToString(),
                Main.resultData.RelativeHumidity.ToString(),
                Main.resultData.AtmosphericPressure.ToString(),
                Main.resultData.KNum.ToString(),
                Main.resultData.FuelTemp.ToString(),
                Main.resultData.CoolantTemp.ToString(),
                Main.resultData.FuelDensity.ToString(),
                Main.resultData.ExhaustPressure.ToString(),
                Main.project.cellNumber,
                Main.project.worker,
                Main.project.inspector,
                APS_rpm,
                APS_min,
                APS_max,
                APS,
                ATQ_rpm,
                ATQ_min,
                ATQ_max,
                ATQ,
                ASMOK_rpm,
                ASMOK_max,
                ASMOK,
                AFASMOK_rpm,
                AFASMOK_max,
                AFASMOK,
                ALOILP_min,
                ALOILP_max,
                ALOILP,
                AFOILP_min,
                AFOILP_max,
                AFOILP,
                AJOILP_rpm,
                AJOILP_min,
                AJOILP_max,
                AJOILP,
                ALRPM_rpm,
                ALRPM_min,
                ALRPM_max,
                ALRPM,
                AFRPM_rpm,
                AFRPM_min,
                AFRPM_max,
                AFRPM,
                ABSFC_rpm,
                ABSFC_min,
                ABSFC_max,
                ABSFC,
                "1"
                );

            if (ok)
            {
                if (!Main.database.UpdateWorkOrder(Main.resultData.engineNumber, Main.resultData.worker))
                {
                    XtraMessageBox.Show("<size=14>작업 지시서 완료 UPDATE DB 쿼리 에러 발생</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                XtraMessageBox.Show("<size=14>결과 DB 전송 성공</size>", "<size=14>성공</size>", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (flag == 1)
                {
                    ECUFlashing form = new ECUFlashing(FlashType.Shipment);
                    form.Show();
                }
                else
                {
                    XtraMessageBox.Show("<size=14>합격 판정 상태가 아니기 때문에 자동으로 출하맵 플래시가 진행되지 않습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {

                int TIMENO = Main.database.UpdateEngineNumberonResultData(Main.resultData.engineNumber);//TIMENO = -1이면 DB업데이트 에러

                ok = Main.database.InsertResultData(Main.resultData.engineNumber,
                Main.resultData.suffix,
                Main.resultData.model,
                Main.resultData.engineType,
                Main.resultData.engineCategory,
                Main.resultData.schduleFilePath,
                Main.resultData.mapFilePath,
                Main.resultData.turboCharger,
                Main.resultData.endTime,
                Main.resultData.AirTemp.ToString(),
                Main.resultData.RelativeHumidity.ToString(),
                Main.resultData.AtmosphericPressure.ToString(),
                Main.resultData.KNum.ToString(),
                Main.resultData.FuelTemp.ToString(),
                Main.resultData.CoolantTemp.ToString(),
                Main.resultData.FuelDensity.ToString(),
                Main.resultData.ExhaustPressure.ToString(),
                Main.project.cellNumber,
                Main.project.worker,
                Main.project.inspector,
                APS_rpm,
                APS_min,
                APS_max,
                APS,
                ATQ_rpm,
                ATQ_min,
                ATQ_max,
                ATQ,
                ASMOK_rpm,
                ASMOK_max,
                ASMOK,
                AFASMOK_rpm,
                AFASMOK_max,
                AFASMOK,
                ALOILP_min,
                ALOILP_max,
                ALOILP,
                AFOILP_min,
                AFOILP_max,
                AFOILP,
                AJOILP_rpm,
                AJOILP_min,
                AJOILP_max,
                AJOILP,
                ALRPM_rpm,
                ALRPM_min,
                ALRPM_max,
                ALRPM,
                AFRPM_rpm,
                AFRPM_min,
                AFRPM_max,
                AFRPM,
                ABSFC_rpm,
                ABSFC_min,
                ABSFC_max,
                ABSFC,
                TIMENO.ToString()
                );

                if (ok)
                {
                    if (!Main.database.UpdateWorkOrder(Main.resultData.engineNumber, Main.resultData.worker))
                    {
                        XtraMessageBox.Show("<size=14>작업 지시서 완료 UPDATE DB 쿼리 에러 발생</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    XtraMessageBox.Show("<size=14>결과 DB 전송 성공</size>", "<size=14>성공</size>", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (flag == 1)
                    {
                        ECUFlashing form = new ECUFlashing(FlashType.Shipment);
                        form.Show();
                    }
                    else
                    {
                        XtraMessageBox.Show("<size=14>합격 판정 상태가 아니기 때문에 자동으로 출하맵 플래시가 진행되지 않습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
    }
}
