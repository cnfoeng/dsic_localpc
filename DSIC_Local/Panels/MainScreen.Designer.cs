﻿namespace DSIC_Local.Panels
{
    partial class MainScreen
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.panelStep = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridStep = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ColScheduleStep = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColScheduleStepName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ColDynoMode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColDynoSetValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColEngineMode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColEngineSetValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColScheduleRamp = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColScheduleTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ColScheduleMeasStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ColScheduleMeasEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridMeasurement = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColStep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColOutput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColMeasChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColMeasValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColOk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnStepFirst = new DevExpress.XtraEditors.SimpleButton();
            this.btnStepNext = new DevExpress.XtraEditors.SimpleButton();
            this.btnStepPrev = new DevExpress.XtraEditors.SimpleButton();
            this.tbDTCStatus = new DevExpress.XtraEditors.TextEdit();
            this.btnECUFaultClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnECUFaultCheck = new DevExpress.XtraEditors.SimpleButton();
            this.tbCurrentTime = new DevExpress.XtraEditors.TextEdit();
            this.tbStartTime = new DevExpress.XtraEditors.TextEdit();
            this.tbStartDate = new DevExpress.XtraEditors.TextEdit();
            this.tbWorker = new DevExpress.XtraEditors.TextEdit();
            this.tbEngineNumber = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelMonitoring = new DevExpress.XtraEditors.PanelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelStep)).BeginInit();
            this.panelStep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMeasurement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDTCStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurrentTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEngineNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMonitoring)).BeginInit();
            this.SuspendLayout();
            // 
            // panelStep
            // 
            this.panelStep.Controls.Add(this.panelControl3);
            this.panelStep.Controls.Add(this.panelControl2);
            this.panelStep.Controls.Add(this.panelControl1);
            this.panelStep.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStep.Location = new System.Drawing.Point(0, 646);
            this.panelStep.Name = "panelStep";
            this.panelStep.Size = new System.Drawing.Size(1529, 256);
            this.panelStep.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.gridStep);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(311, 102);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1216, 152);
            this.panelControl3.TabIndex = 2;
            // 
            // gridStep
            // 
            this.gridStep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridStep.Location = new System.Drawing.Point(0, 0);
            this.gridStep.MainView = this.bandedGridView1;
            this.gridStep.Name = "gridStep";
            this.gridStep.Size = new System.Drawing.Size(1216, 152);
            this.gridStep.TabIndex = 1;
            this.gridStep.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Options.UseTextOptions = true;
            this.bandedGridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridView1.BandPanelRowHeight = 30;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
            this.bandedGridView1.ColumnPanelRowHeight = 30;
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ColScheduleStep,
            this.ColScheduleStepName,
            this.ColDynoSetValue,
            this.ColEngineSetValue,
            this.ColScheduleRamp,
            this.ColScheduleTime,
            this.ColScheduleMeasStart,
            this.ColScheduleMeasEnd,
            this.ColDynoMode,
            this.ColEngineMode});
            this.bandedGridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.bandedGridView1.GridControl = this.gridStep;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsCustomization.AllowBandMoving = false;
            this.bandedGridView1.OptionsCustomization.AllowColumnMoving = false;
            this.bandedGridView1.OptionsCustomization.AllowSort = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "스케쥴";
            this.gridBand1.Columns.Add(this.ColScheduleStep);
            this.gridBand1.Columns.Add(this.ColScheduleStepName);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 136;
            // 
            // ColScheduleStep
            // 
            this.ColScheduleStep.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleStep.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleStep.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleStep.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleStep.Caption = "스텝";
            this.ColScheduleStep.FieldName = "StepNumber";
            this.ColScheduleStep.Name = "ColScheduleStep";
            this.ColScheduleStep.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleStep.OptionsFilter.AllowFilter = false;
            this.ColScheduleStep.Visible = true;
            this.ColScheduleStep.Width = 44;
            // 
            // ColScheduleStepName
            // 
            this.ColScheduleStepName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleStepName.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleStepName.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleStepName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleStepName.Caption = "스텝 이름";
            this.ColScheduleStepName.FieldName = "StepName";
            this.ColScheduleStepName.Name = "ColScheduleStepName";
            this.ColScheduleStepName.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleStepName.OptionsFilter.AllowFilter = false;
            this.ColScheduleStepName.Visible = true;
            this.ColScheduleStepName.Width = 92;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "테스트 모드";
            this.gridBand2.Columns.Add(this.ColDynoMode);
            this.gridBand2.Columns.Add(this.ColDynoSetValue);
            this.gridBand2.Columns.Add(this.ColEngineMode);
            this.gridBand2.Columns.Add(this.ColEngineSetValue);
            this.gridBand2.Columns.Add(this.ColScheduleRamp);
            this.gridBand2.Columns.Add(this.ColScheduleTime);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 534;
            // 
            // ColDynoMode
            // 
            this.ColDynoMode.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColDynoMode.AppearanceHeader.Options.UseFont = true;
            this.ColDynoMode.AppearanceHeader.Options.UseTextOptions = true;
            this.ColDynoMode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColDynoMode.Caption = "동력계 모드";
            this.ColDynoMode.FieldName = "DynoMode";
            this.ColDynoMode.Name = "ColDynoMode";
            this.ColDynoMode.OptionsFilter.AllowAutoFilter = false;
            this.ColDynoMode.OptionsFilter.AllowFilter = false;
            this.ColDynoMode.Visible = true;
            this.ColDynoMode.Width = 91;
            // 
            // ColDynoSetValue
            // 
            this.ColDynoSetValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColDynoSetValue.AppearanceHeader.Options.UseFont = true;
            this.ColDynoSetValue.AppearanceHeader.Options.UseTextOptions = true;
            this.ColDynoSetValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColDynoSetValue.Caption = "동력계 셋값";
            this.ColDynoSetValue.FieldName = "DynoSetValue";
            this.ColDynoSetValue.Name = "ColDynoSetValue";
            this.ColDynoSetValue.OptionsFilter.AllowAutoFilter = false;
            this.ColDynoSetValue.OptionsFilter.AllowFilter = false;
            this.ColDynoSetValue.Visible = true;
            this.ColDynoSetValue.Width = 104;
            // 
            // ColEngineMode
            // 
            this.ColEngineMode.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColEngineMode.AppearanceHeader.Options.UseFont = true;
            this.ColEngineMode.AppearanceHeader.Options.UseTextOptions = true;
            this.ColEngineMode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColEngineMode.Caption = "엔진 모드";
            this.ColEngineMode.FieldName = "EngineMode";
            this.ColEngineMode.Name = "ColEngineMode";
            this.ColEngineMode.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineMode.OptionsFilter.AllowFilter = false;
            this.ColEngineMode.Visible = true;
            this.ColEngineMode.Width = 76;
            // 
            // ColEngineSetValue
            // 
            this.ColEngineSetValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColEngineSetValue.AppearanceHeader.Options.UseFont = true;
            this.ColEngineSetValue.AppearanceHeader.Options.UseTextOptions = true;
            this.ColEngineSetValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColEngineSetValue.Caption = "엔진 셋값";
            this.ColEngineSetValue.FieldName = "EngineSetValue";
            this.ColEngineSetValue.Name = "ColEngineSetValue";
            this.ColEngineSetValue.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineSetValue.OptionsFilter.AllowFilter = false;
            this.ColEngineSetValue.Visible = true;
            this.ColEngineSetValue.Width = 94;
            // 
            // ColScheduleRamp
            // 
            this.ColScheduleRamp.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleRamp.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleRamp.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleRamp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleRamp.Caption = "가감시간";
            this.ColScheduleRamp.FieldName = "Ramp";
            this.ColScheduleRamp.Name = "ColScheduleRamp";
            this.ColScheduleRamp.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleRamp.OptionsFilter.AllowFilter = false;
            this.ColScheduleRamp.Visible = true;
            this.ColScheduleRamp.Width = 67;
            // 
            // ColScheduleTime
            // 
            this.ColScheduleTime.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleTime.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleTime.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleTime.Caption = "유지시간";
            this.ColScheduleTime.FieldName = "Time";
            this.ColScheduleTime.Name = "ColScheduleTime";
            this.ColScheduleTime.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleTime.OptionsFilter.AllowFilter = false;
            this.ColScheduleTime.Visible = true;
            this.ColScheduleTime.Width = 102;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "측정";
            this.gridBand3.Columns.Add(this.ColScheduleMeasStart);
            this.gridBand3.Columns.Add(this.ColScheduleMeasEnd);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 162;
            // 
            // ColScheduleMeasStart
            // 
            this.ColScheduleMeasStart.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleMeasStart.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleMeasStart.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleMeasStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleMeasStart.Caption = "시작";
            this.ColScheduleMeasStart.FieldName = "MeasureStartTime";
            this.ColScheduleMeasStart.Name = "ColScheduleMeasStart";
            this.ColScheduleMeasStart.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleMeasStart.OptionsFilter.AllowFilter = false;
            this.ColScheduleMeasStart.Visible = true;
            // 
            // ColScheduleMeasEnd
            // 
            this.ColScheduleMeasEnd.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColScheduleMeasEnd.AppearanceHeader.Options.UseFont = true;
            this.ColScheduleMeasEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.ColScheduleMeasEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColScheduleMeasEnd.Caption = "종료";
            this.ColScheduleMeasEnd.FieldName = "MeasureEndTime";
            this.ColScheduleMeasEnd.Name = "ColScheduleMeasEnd";
            this.ColScheduleMeasEnd.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleMeasEnd.OptionsFilter.AllowFilter = false;
            this.ColScheduleMeasEnd.Visible = true;
            this.ColScheduleMeasEnd.Width = 87;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.gridMeasurement);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(311, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1216, 100);
            this.panelControl2.TabIndex = 1;
            // 
            // gridMeasurement
            // 
            this.gridMeasurement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMeasurement.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridMeasurement.Location = new System.Drawing.Point(0, 0);
            this.gridMeasurement.MainView = this.gridView1;
            this.gridMeasurement.Name = "gridMeasurement";
            this.gridMeasurement.Size = new System.Drawing.Size(1216, 100);
            this.gridMeasurement.TabIndex = 0;
            this.gridMeasurement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.gridView1.AppearancePrint.Row.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.AppearancePrint.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 30;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColStep,
            this.ColOutput,
            this.ColMeasChannel,
            this.ColMeasValue,
            this.ColCondition,
            this.ColOk});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GridControl = this.gridMeasurement;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // ColStep
            // 
            this.ColStep.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.ColStep.AppearanceHeader.Options.UseFont = true;
            this.ColStep.AppearanceHeader.Options.UseTextOptions = true;
            this.ColStep.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColStep.Caption = "스텝";
            this.ColStep.FieldName = "StepNumber";
            this.ColStep.Name = "ColStep";
            this.ColStep.OptionsFilter.AllowAutoFilter = false;
            this.ColStep.OptionsFilter.AllowFilter = false;
            this.ColStep.Visible = true;
            this.ColStep.VisibleIndex = 0;
            // 
            // ColOutput
            // 
            this.ColOutput.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.ColOutput.AppearanceHeader.Options.UseFont = true;
            this.ColOutput.AppearanceHeader.Options.UseTextOptions = true;
            this.ColOutput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColOutput.Caption = "출력 항목";
            this.ColOutput.FieldName = "OutputItem";
            this.ColOutput.Name = "ColOutput";
            this.ColOutput.OptionsFilter.AllowAutoFilter = false;
            this.ColOutput.OptionsFilter.AllowFilter = false;
            this.ColOutput.Visible = true;
            this.ColOutput.VisibleIndex = 1;
            this.ColOutput.Width = 157;
            // 
            // ColMeasChannel
            // 
            this.ColMeasChannel.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.ColMeasChannel.AppearanceHeader.Options.UseFont = true;
            this.ColMeasChannel.AppearanceHeader.Options.UseTextOptions = true;
            this.ColMeasChannel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColMeasChannel.Caption = "측정 채널";
            this.ColMeasChannel.FieldName = "MeasuringChannel";
            this.ColMeasChannel.Name = "ColMeasChannel";
            this.ColMeasChannel.OptionsFilter.AllowAutoFilter = false;
            this.ColMeasChannel.OptionsFilter.AllowFilter = false;
            this.ColMeasChannel.Visible = true;
            this.ColMeasChannel.VisibleIndex = 2;
            this.ColMeasChannel.Width = 229;
            // 
            // ColMeasValue
            // 
            this.ColMeasValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.ColMeasValue.AppearanceHeader.Options.UseFont = true;
            this.ColMeasValue.AppearanceHeader.Options.UseTextOptions = true;
            this.ColMeasValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColMeasValue.Caption = "측정 값";
            this.ColMeasValue.FieldName = "Value";
            this.ColMeasValue.Name = "ColMeasValue";
            this.ColMeasValue.OptionsFilter.AllowAutoFilter = false;
            this.ColMeasValue.OptionsFilter.AllowFilter = false;
            this.ColMeasValue.Visible = true;
            this.ColMeasValue.VisibleIndex = 3;
            this.ColMeasValue.Width = 100;
            // 
            // ColCondition
            // 
            this.ColCondition.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.ColCondition.AppearanceHeader.Options.UseFont = true;
            this.ColCondition.AppearanceHeader.Options.UseTextOptions = true;
            this.ColCondition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColCondition.Caption = "합격 조건";
            this.ColCondition.FieldName = "Condition";
            this.ColCondition.Name = "ColCondition";
            this.ColCondition.OptionsFilter.AllowAutoFilter = false;
            this.ColCondition.OptionsFilter.AllowFilter = false;
            this.ColCondition.Visible = true;
            this.ColCondition.VisibleIndex = 5;
            this.ColCondition.Width = 193;
            // 
            // ColOk
            // 
            this.ColOk.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ColOk.AppearanceHeader.Options.UseFont = true;
            this.ColOk.AppearanceHeader.Options.UseTextOptions = true;
            this.ColOk.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColOk.Caption = "합격여부";
            this.ColOk.FieldName = "Ok";
            this.ColOk.Name = "ColOk";
            this.ColOk.Visible = true;
            this.ColOk.VisibleIndex = 4;
            this.ColOk.Width = 76;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(309, 252);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.dataLayoutControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(309, 252);
            this.panelControl4.TabIndex = 0;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.btnStepFirst);
            this.dataLayoutControl1.Controls.Add(this.btnStepNext);
            this.dataLayoutControl1.Controls.Add(this.btnStepPrev);
            this.dataLayoutControl1.Controls.Add(this.tbDTCStatus);
            this.dataLayoutControl1.Controls.Add(this.btnECUFaultClear);
            this.dataLayoutControl1.Controls.Add(this.btnECUFaultCheck);
            this.dataLayoutControl1.Controls.Add(this.tbCurrentTime);
            this.dataLayoutControl1.Controls.Add(this.tbStartTime);
            this.dataLayoutControl1.Controls.Add(this.tbStartDate);
            this.dataLayoutControl1.Controls.Add(this.tbWorker);
            this.dataLayoutControl1.Controls.Add(this.tbEngineNumber);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(2, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.ShareLookAndFeelWithChildren = false;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(305, 248);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // btnStepFirst
            // 
            this.btnStepFirst.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepFirst.Appearance.Options.UseFont = true;
            this.btnStepFirst.Appearance.Options.UseTextOptions = true;
            this.btnStepFirst.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnStepFirst.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnStepFirst.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnStepFirst.Location = new System.Drawing.Point(104, 240);
            this.btnStepFirst.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnStepFirst.Name = "btnStepFirst";
            this.btnStepFirst.Size = new System.Drawing.Size(88, 30);
            this.btnStepFirst.TabIndex = 14;
            this.btnStepFirst.Text = "처음";
            this.btnStepFirst.Click += new System.EventHandler(this.btnStepFirst_Click);
            // 
            // btnStepNext
            // 
            this.btnStepNext.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepNext.Appearance.Options.UseFont = true;
            this.btnStepNext.Appearance.Options.UseTextOptions = true;
            this.btnStepNext.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnStepNext.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.btnStepNext.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnStepNext.ImageOptions.SvgImage")));
            this.btnStepNext.ImageOptions.SvgImageSize = new System.Drawing.Size(18, 18);
            this.btnStepNext.Location = new System.Drawing.Point(196, 240);
            this.btnStepNext.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnStepNext.Name = "btnStepNext";
            this.btnStepNext.Size = new System.Drawing.Size(80, 30);
            this.btnStepNext.TabIndex = 13;
            this.btnStepNext.Text = "Next";
            this.btnStepNext.Click += new System.EventHandler(this.btnStepNext_Click);
            // 
            // btnStepPrev
            // 
            this.btnStepPrev.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepPrev.Appearance.Options.UseFont = true;
            this.btnStepPrev.Appearance.Options.UseTextOptions = true;
            this.btnStepPrev.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnStepPrev.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnStepPrev.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnStepPrev.ImageOptions.SvgImage")));
            this.btnStepPrev.ImageOptions.SvgImageSize = new System.Drawing.Size(18, 18);
            this.btnStepPrev.Location = new System.Drawing.Point(12, 240);
            this.btnStepPrev.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnStepPrev.Name = "btnStepPrev";
            this.btnStepPrev.Size = new System.Drawing.Size(88, 30);
            this.btnStepPrev.TabIndex = 12;
            this.btnStepPrev.Text = "Prev";
            this.btnStepPrev.Click += new System.EventHandler(this.btnStepPrev_Click);
            // 
            // tbDTCStatus
            // 
            this.tbDTCStatus.EditValue = "진단대기";
            this.tbDTCStatus.Location = new System.Drawing.Point(12, 206);
            this.tbDTCStatus.Name = "tbDTCStatus";
            this.tbDTCStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(56)))), ((int)(((byte)(65)))));
            this.tbDTCStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDTCStatus.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.tbDTCStatus.Properties.Appearance.Options.UseBackColor = true;
            this.tbDTCStatus.Properties.Appearance.Options.UseFont = true;
            this.tbDTCStatus.Properties.Appearance.Options.UseForeColor = true;
            this.tbDTCStatus.Properties.Appearance.Options.UseTextOptions = true;
            this.tbDTCStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbDTCStatus.Properties.ReadOnly = true;
            this.tbDTCStatus.Size = new System.Drawing.Size(264, 30);
            this.tbDTCStatus.TabIndex = 11;
            // 
            // btnECUFaultClear
            // 
            this.btnECUFaultClear.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnECUFaultClear.Appearance.Options.UseFont = true;
            this.btnECUFaultClear.Appearance.Options.UseTextOptions = true;
            this.btnECUFaultClear.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnECUFaultClear.Location = new System.Drawing.Point(146, 172);
            this.btnECUFaultClear.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnECUFaultClear.Name = "btnECUFaultClear";
            this.btnECUFaultClear.Size = new System.Drawing.Size(130, 30);
            this.btnECUFaultClear.TabIndex = 10;
            this.btnECUFaultClear.Text = "경보해제";
            this.btnECUFaultClear.Click += new System.EventHandler(this.btnECUFaultClear_Click);
            // 
            // btnECUFaultCheck
            // 
            this.btnECUFaultCheck.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnECUFaultCheck.Appearance.Options.UseFont = true;
            this.btnECUFaultCheck.Appearance.Options.UseTextOptions = true;
            this.btnECUFaultCheck.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnECUFaultCheck.Location = new System.Drawing.Point(12, 172);
            this.btnECUFaultCheck.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnECUFaultCheck.Name = "btnECUFaultCheck";
            this.btnECUFaultCheck.Size = new System.Drawing.Size(130, 30);
            this.btnECUFaultCheck.TabIndex = 9;
            this.btnECUFaultCheck.Text = "경보화면";
            this.btnECUFaultCheck.Click += new System.EventHandler(this.btnECUFaultCheck_Click);
            // 
            // tbCurrentTime
            // 
            this.tbCurrentTime.Location = new System.Drawing.Point(89, 132);
            this.tbCurrentTime.Name = "tbCurrentTime";
            this.tbCurrentTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCurrentTime.Properties.Appearance.Options.UseFont = true;
            this.tbCurrentTime.Properties.Appearance.Options.UseTextOptions = true;
            this.tbCurrentTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbCurrentTime.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.tbCurrentTime.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.tbCurrentTime.Properties.ReadOnly = true;
            this.tbCurrentTime.Size = new System.Drawing.Size(187, 26);
            this.tbCurrentTime.TabIndex = 8;
            // 
            // tbStartTime
            // 
            this.tbStartTime.Location = new System.Drawing.Point(89, 102);
            this.tbStartTime.Name = "tbStartTime";
            this.tbStartTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbStartTime.Properties.Appearance.Options.UseFont = true;
            this.tbStartTime.Properties.Appearance.Options.UseTextOptions = true;
            this.tbStartTime.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbStartTime.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.tbStartTime.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.tbStartTime.Properties.ReadOnly = true;
            this.tbStartTime.Size = new System.Drawing.Size(187, 26);
            this.tbStartTime.TabIndex = 7;
            // 
            // tbStartDate
            // 
            this.tbStartDate.Location = new System.Drawing.Point(89, 72);
            this.tbStartDate.Name = "tbStartDate";
            this.tbStartDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbStartDate.Properties.Appearance.Options.UseFont = true;
            this.tbStartDate.Properties.Appearance.Options.UseTextOptions = true;
            this.tbStartDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbStartDate.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.tbStartDate.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.tbStartDate.Properties.ReadOnly = true;
            this.tbStartDate.Size = new System.Drawing.Size(187, 26);
            this.tbStartDate.TabIndex = 6;
            // 
            // tbWorker
            // 
            this.tbWorker.Location = new System.Drawing.Point(89, 42);
            this.tbWorker.Name = "tbWorker";
            this.tbWorker.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWorker.Properties.Appearance.Options.UseFont = true;
            this.tbWorker.Properties.Appearance.Options.UseTextOptions = true;
            this.tbWorker.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbWorker.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.tbWorker.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.tbWorker.Properties.ReadOnly = true;
            this.tbWorker.Size = new System.Drawing.Size(187, 26);
            this.tbWorker.TabIndex = 5;
            // 
            // tbEngineNumber
            // 
            this.tbEngineNumber.Location = new System.Drawing.Point(89, 12);
            this.tbEngineNumber.Name = "tbEngineNumber";
            this.tbEngineNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEngineNumber.Properties.Appearance.Options.UseFont = true;
            this.tbEngineNumber.Properties.Appearance.Options.UseTextOptions = true;
            this.tbEngineNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbEngineNumber.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.tbEngineNumber.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.tbEngineNumber.Properties.ReadOnly = true;
            this.tbEngineNumber.Size = new System.Drawing.Size(187, 26);
            this.tbEngineNumber.TabIndex = 4;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(288, 282);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.tbEngineNumber;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(268, 30);
            this.layoutControlItem1.Text = "엔진 번호";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(74, 23);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.tbWorker;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(268, 30);
            this.layoutControlItem2.Text = "작업자";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(74, 23);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.tbStartDate;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(268, 30);
            this.layoutControlItem3.Text = "시작일";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(74, 23);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.tbStartTime;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(268, 30);
            this.layoutControlItem4.Text = "시작 시간";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(74, 23);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.tbCurrentTime;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(268, 30);
            this.layoutControlItem5.Text = "현재 시간";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(74, 23);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnECUFaultCheck;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(134, 34);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnECUFaultClear;
            this.layoutControlItem7.Location = new System.Drawing.Point(134, 160);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(134, 34);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.tbDTCStatus;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 194);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(268, 34);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnStepPrev;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 228);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(92, 34);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnStepNext;
            this.layoutControlItem10.Location = new System.Drawing.Point(184, 228);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(84, 34);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnStepFirst;
            this.layoutControlItem11.Location = new System.Drawing.Point(92, 228);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(92, 34);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(268, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelMonitoring
            // 
            this.panelMonitoring.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMonitoring.Location = new System.Drawing.Point(0, 0);
            this.panelMonitoring.Name = "panelMonitoring";
            this.panelMonitoring.Size = new System.Drawing.Size(1529, 646);
            this.panelMonitoring.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelMonitoring);
            this.Controls.Add(this.panelStep);
            this.Name = "MainScreen";
            this.Size = new System.Drawing.Size(1529, 902);
            ((System.ComponentModel.ISupportInitialize)(this.panelStep)).EndInit();
            this.panelStep.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMeasurement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbDTCStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurrentTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbWorker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEngineNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMonitoring)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelStep;
        private DevExpress.XtraEditors.PanelControl panelMonitoring;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit tbCurrentTime;
        private DevExpress.XtraEditors.TextEdit tbStartTime;
        private DevExpress.XtraEditors.TextEdit tbStartDate;
        private DevExpress.XtraEditors.TextEdit tbWorker;
        private DevExpress.XtraEditors.TextEdit tbEngineNumber;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnStepFirst;
        private DevExpress.XtraEditors.SimpleButton btnStepNext;
        private DevExpress.XtraEditors.SimpleButton btnStepPrev;
        private DevExpress.XtraEditors.TextEdit tbDTCStatus;
        private DevExpress.XtraEditors.SimpleButton btnECUFaultClear;
        private DevExpress.XtraEditors.SimpleButton btnECUFaultCheck;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.GridControl gridStep;
        private DevExpress.XtraGrid.GridControl gridMeasurement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColStep;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleStep;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleStepName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColDynoSetValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColEngineSetValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleRamp;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleMeasStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColScheduleMeasEnd;
        private DevExpress.XtraGrid.Columns.GridColumn ColOutput;
        private DevExpress.XtraGrid.Columns.GridColumn ColMeasChannel;
        private DevExpress.XtraGrid.Columns.GridColumn ColMeasValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColCondition;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColDynoMode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ColEngineMode;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraGrid.Columns.GridColumn ColOk;
    }
}
