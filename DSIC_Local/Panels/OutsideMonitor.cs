﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DSIC_Local.Monitoring;

namespace DSIC_Local.Panels
{
    public partial class OutsideMonitor : DevExpress.XtraEditors.XtraForm
    {
        public MonitoringForm monitoringForm;
        public int screenNumber = 0;
        public bool visibility;
        public OutsideMonitor(List<MonitorItem> monitorItems)
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.Manual;
            //this.WindowState = FormWindowState.Maximized;//여기서 최대화시키면 Location 이동하는게 안 먹힘.
            
            monitoringForm = new MonitoringForm("outside_Monitor", monitorItems);
            panel1.Controls.Add(monitoringForm);

            Screen[] screens = Screen.AllScreens;
            
            
        }

        private void OutsideMonitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            this.Visible = false;
            visibility = false;
        }
        

        private void OutsideMonitor_VisibleChanged(object sender, EventArgs e)
        {

            if(this.Visible == false)
            {
                return;
            }

            visibility = true;
            Screen[] screens = Screen.AllScreens;

            if (screens.Length == 1)
            {
                this.Location = Screen.AllScreens[0].WorkingArea.Location;
                this.Size = new Size(Screen.AllScreens[0].WorkingArea.Width, Screen.AllScreens[0].WorkingArea.Height);
                return;
            }

            if (screenNumber == 0)//주 화면
            {
                this.Location = Screen.AllScreens[0].WorkingArea.Location;
                this.Size = new Size(Screen.AllScreens[0].WorkingArea.Width, Screen.AllScreens[0].WorkingArea.Height);
            }
            else if (screenNumber == 1)//서브 화면
            {
                this.Location = Screen.AllScreens[1].WorkingArea.Location;
                this.Size = new Size(Screen.AllScreens[1].WorkingArea.Width, Screen.AllScreens[1].WorkingArea.Height);
            }
        }
        
    }
}