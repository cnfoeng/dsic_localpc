﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace DSIC_Local.Database
{
    public class MSSQL
    {
        static SqlCommand sqlCmd = new SqlCommand();
        string connectionString;
        public bool Connect(string ip, string port, string userid, string pwd, string dbName)
        {
            connectionString = $"server = {ip},{port}; uid = {userid}; pwd = {pwd}; database = {dbName};";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }

        //DB Table Name
        string suffixTableName = Properties.Settings.Default.EngineInfoTableName;
        string cnfoTableName = Properties.Settings.Default.CnfoTableName;
        string workOrderTableName = Properties.Settings.Default.WorkOrderTableName;
        string resultTableName = Properties.Settings.Default.ResultTableName;
        //Column Name 
        string suffix = Properties.Settings.Default.ColSuffix;
        string model = Properties.Settings.Default.ColModel;
        string engineCategory = Properties.Settings.Default.ColEngineCatergory;
        string engineType = Properties.Settings.Default.ColEngineType;
        string generationDate = Properties.Settings.Default.ColGenerationDate;
        string engineConfigFilePath = Properties.Settings.Default.ColEngineConfigFilePath;
        string diagnosticsFilePath = Properties.Settings.Default.ColDiagnosticsFilePath;
        string flashConfigFilePath = Properties.Settings.Default.ColFlashConfigFilePath;
        string workDorderflashConfigFilePath = Properties.Settings.Default.ColWorkOrderFlashConfigFilePath;
        string scheduleFilePath = Properties.Settings.Default.ColScheduleFilePath;
        string purpose = Properties.Settings.Default.ColForWhat;
        string turboCharger = Properties.Settings.Default.ColTurboCharger;
        string usage = Properties.Settings.Default.ColUsage;
        string engineSerial = Properties.Settings.Default.ColEngineSerial;

        public Tuple<DataSet, string> SelectDataFromEngineNumber(string engineNumber)
        {
            string workOrderSuffix = string.Empty;
            string workOrderHexFileName = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();
                }
                catch (SqlException)
                {
                    Debug.Print("DB Connection Error.");
                    return null;
                }
                sqlCmd.CommandText = $"select " +
                    $"{workOrderTableName}.{suffix}," +
                $"{workOrderTableName}.EGNO," +
                 $"{workOrderTableName}.{workDorderflashConfigFilePath}" +
                $" from {workOrderTableName} where {workOrderTableName}.EGNO = @engineNumber";
                //$" from {workOrderTableName} where {workOrderTableName}.{suffix} = 'OOG00'";

                sqlCmd.Parameters.AddWithValue("@engineNumber", engineNumber);

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                DataSet ds = new DataSet();

                try
                {
                    da.Fill(ds, workOrderTableName);
                }
                catch (SqlException)
                {
                    Debug.Print("Select DB 과정에서 SQL 에러 발생.");
                    return null;
                }

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                workOrderSuffix = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                workOrderHexFileName = ds.Tables[0].Rows[0].ItemArray[2].ToString();

            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                sqlCmd = new SqlCommand();
                sqlCmd.Connection = connection;
                connection.Open();

                sqlCmd.CommandText = $"select " +
                $"{suffixTableName}.{suffix}," +
                $"{suffixTableName}.{model}," +
                $"{suffixTableName}.{engineCategory}," +
                $"{suffixTableName}.{engineType}," +
                $"{suffixTableName}.{generationDate}," +
                $"{suffixTableName}.{purpose}," +
                $"{suffixTableName}.{turboCharger}," +
                $"{cnfoTableName}.{engineConfigFilePath}," +
                $"{cnfoTableName}.{diagnosticsFilePath}," +
                $"{cnfoTableName}.{flashConfigFilePath}," +
                $"{cnfoTableName}.{scheduleFilePath}" +
                $" from {suffixTableName} inner join {cnfoTableName} on {suffixTableName}.{suffix} = {cnfoTableName}.{suffix} where {suffixTableName}.{suffix} = @workOrderSuffix and {cnfoTableName}.{flashConfigFilePath} = @workOrderHexFileName";
                //select 이름 안맞으면 에러남. where 쓰려면 param붙여서 사용.

                sqlCmd.Parameters.AddWithValue("@workOrderSuffix", workOrderSuffix);
                sqlCmd.Parameters.AddWithValue("@workOrderHexFileName", workOrderHexFileName);

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                DataSet ds = new DataSet();

                try
                {
                    da.Fill(ds, suffixTableName);
                }
                catch (SqlException)
                {
                    Debug.Print("SelectDataFromEngineNumber DB 과정에서 SQL 에러 발생.");
                    return null;
                }
                return Tuple.Create<DataSet, string>(ds, suffixTableName);

                //sqlCmd.CommandText = $"select result.test, result.test2, sub.test10" +
                //    $" from Result inner join sub on Result.test = sub.test";

                ////select 이름 안맞으면 에러남.
                //SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                //DataSet ds = new DataSet();
                //da.Fill(ds, tableName);


                //return Tuple.Create<DataSet, string>(ds, tableName);
            }

        }

        public bool InsertResultData(string egno_content, string suffix_content, string model_content, string engineType_content, string engineCategory_content, string scheduleFilePath_content, string ecuFilePath_content, string turboCharger_content, string insDate_content, string AirTemp, string RH, string AtmosPre, string Knum, string FuelTemp, string CoolantTemp, string FuelDensity, string ExhaustPre, string cellNumber,string worker, string inspector, string APS_rpm, string APS_min, string APS_max, string APS, string ATQ_rpm, string ATQ_min, string ATQ_max, string ATQ, string ASMOK_rpm, string ASMOK_max, string ASMOK, string AFASMOK_rpm, string AFASMOK_max, string AFASMOK, string ALOILP_min, string ALOILP_max, string ALOILP, string AFOILP_min, string AFOILP_max, string AFOILP, string AJOILP_rpm, string AJOILP_min, string AJOILP_max, string AJOILP,string ALRPM_rpm, string ALRPM_min, string ALRPM_max, string ALRPM,string AFRPM_rpm, string AFRPM_min, string AFRPM_max, string AFRPM, string ABSFC_rpm, string ABSFC_min, string ABSFC_max, string ABSFC,string TIMENO)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    //sqlCmd.CommandText = $"insert into {resultTableName} (EGNO, {suffix} ,{model},{engineType},{engineCategory},Sch_Name,ECUFileName,{turboCharger},INSDATE)" +
                    //   $"values (@EGNO,@suffix,@model,@engineType,@engineCategory,@scheduleFilePath,@ecuFilePath,@turboCharger,@insDate)";
                    //sqlCmd.Parameters.AddWithValue("@EGNO", egno_content);
                    //sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    //sqlCmd.Parameters.AddWithValue("@model", model_content);
                    //sqlCmd.Parameters.AddWithValue("@engineType", engineType_content);
                    //sqlCmd.Parameters.AddWithValue("@engineCategory", engineCategory_content);
                    //sqlCmd.Parameters.AddWithValue("@scheduleFilePath", scheduleFilePath_content);
                    //sqlCmd.Parameters.AddWithValue("@ecuFilePath", ecuFilePath_content);
                    //sqlCmd.Parameters.AddWithValue("@turboCharger", turboCharger_content);
                    //sqlCmd.Parameters.AddWithValue("@insDate", insDate_content);
                    //int rowNum = sqlCmd.ExecuteNonQuery();

                    sqlCmd.CommandText = $"insert into {resultTableName} (EGNO, {suffix} ,{model},{engineType},{engineCategory},Sch_Name,ECUFileName,{turboCharger},INSDATE,AIRTEMP,DAMPTEMP,RELHUMID,AIRPRE,KNUM,FTEMPIN,WTEMP,FuelDensity,Coeff_fm,DY,CELLNAME,WORKM,INSPECT,PSPOINT,APS_rpm,APS_min,APS_max,APS,TQPOINT,ATQ_rpm,ATQ_min,ATQ_max,ATQ,ASMOK_rpm,ASMOK_min,ASMOK_max,ASMOK,ASMOK_jdg,AFASMOK_rpm,AFASMOK_min,AFASMOK_max,AFASMOK,AFASMOK_jdg,ALOILP_min,ALOILP_max,ALOILP,ALOILP_jdg,AFOILP_min,AFOILP_max,AFOILP,AFOILP_jdg,AJOILP_rpm,AJOILP_min,AJOILP_max,AJOILP,AJOILP_jdg,ALRPM_rpm,ALRPM_min,ALRPM_max,ALRPM,AFRPM_rpm,AFRPM_min,AFRPM_max,AFRPM,AFRPM_jdg,ABSFC_rpm,ABSFC_min,ABSFC_max,ABSFC,ABSFC_jdg,IsTested,BLOCK,CRS,INJECTION,ITEMCHECK1,ITEMCHECK2,ITEMCHECK3,ITEMCHECK4,ITEMCHECK5,ITEMCHECKETC,FormulaType,ALSMOK_jdg,TIMENO)" +
                       $"values (@EGNO,@suffix,@model,@engineType,@engineCategory,@scheduleFilePath,@ecuFilePath,@turboCharger,@insDate,@airTemp,@airTemp,@RH,@AtmosPre,@Knum,@FuelTemp,@CoolantTemp,'0.83',@ExhaustPre,@DY,@cellName,@worker,@inspector,@APS_rpm,@APS_rpm,@APS_min,@APS_max,@APS,@ATQ_rpm,@ATQ_rpm,@ATQ_min,@ATQ_max,@ATQ,@ASMOK_rpm,'1',@ASMOK_max,@ASMOK,'0',@AFASMOK_rpm,'1',@AFASMOK_max,@AFASMOK,'0',@ALOILP_min,@ALOILP_max,@ALOILP,'0',@AFOILP_min,@AFOILP_max,@AFOILP,'0',@AJOILP_rpm,@AJOILP_min,@AJOILP_max,@AJOILP,'0',@ALRPM_rpm,@ALRPM_min,@ALRPM_max,@ALRPM,@AFRPM_rpm,@AFRPM_min,@AFRPM_max,@AFRPM,'0',@ABSFC_rpm,@ABSFC_min,@ABSFC_max,@ABSFC,'0','True','','','','0','0','0','0','0','','1','0',@TIMENO)";
                    sqlCmd.Parameters.AddWithValue("@EGNO", egno_content);
                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@model", model_content);
                    sqlCmd.Parameters.AddWithValue("@engineType", engineType_content);
                    sqlCmd.Parameters.AddWithValue("@engineCategory", engineCategory_content);
                    sqlCmd.Parameters.AddWithValue("@scheduleFilePath", scheduleFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@ecuFilePath", ecuFilePath_content);
                    turboCharger_content = (turboCharger_content == "YES") ? "true" : "false";
                    sqlCmd.Parameters.AddWithValue("@turboCharger", turboCharger_content);
                    sqlCmd.Parameters.AddWithValue("@insDate", insDate_content);
                    sqlCmd.Parameters.AddWithValue("@airTemp", AirTemp);
                    sqlCmd.Parameters.AddWithValue("@RH", RH);
                    sqlCmd.Parameters.AddWithValue("@AtmosPre", AtmosPre);
                    sqlCmd.Parameters.AddWithValue("@Knum", Knum);
                    sqlCmd.Parameters.AddWithValue("@FuelTemp", FuelTemp);
                    sqlCmd.Parameters.AddWithValue("@CoolantTemp", CoolantTemp);
                    sqlCmd.Parameters.AddWithValue("@FuelDensity", FuelDensity);
                    sqlCmd.Parameters.AddWithValue("@ExhaustPre", ExhaustPre);
                    sqlCmd.Parameters.AddWithValue("@DY", $"#{cellNumber} CELL");
                    sqlCmd.Parameters.AddWithValue("@cellName", $"대형#{cellNumber}라인");
                    sqlCmd.Parameters.AddWithValue("@worker", worker);
                    sqlCmd.Parameters.AddWithValue("@inspector", inspector);
                    sqlCmd.Parameters.AddWithValue("@APS_rpm", APS_rpm);
                    sqlCmd.Parameters.AddWithValue("@APS_min", APS_min);
                    sqlCmd.Parameters.AddWithValue("@APS_max", APS_max);
                    sqlCmd.Parameters.AddWithValue("@APS", APS);
                    sqlCmd.Parameters.AddWithValue("@ATQ_rpm", ATQ_rpm);
                    sqlCmd.Parameters.AddWithValue("@ATQ_min", ATQ_min);
                    sqlCmd.Parameters.AddWithValue("@ATQ_max", ATQ_max);
                    sqlCmd.Parameters.AddWithValue("@ATQ", ATQ);
                    sqlCmd.Parameters.AddWithValue("@ASMOK_rpm", ASMOK_rpm);
                    sqlCmd.Parameters.AddWithValue("@ASMOK_max", ASMOK_max);
                    sqlCmd.Parameters.AddWithValue("@ASMOK", ASMOK);
                    sqlCmd.Parameters.AddWithValue("@AFASMOK_rpm", AFASMOK_rpm);
                    sqlCmd.Parameters.AddWithValue("@AFASMOK_max", AFASMOK_max);
                    sqlCmd.Parameters.AddWithValue("@AFASMOK", AFASMOK);
                    sqlCmd.Parameters.AddWithValue("@ALOILP_min", ALOILP_min);
                    sqlCmd.Parameters.AddWithValue("@ALOILP_max", ALOILP_max);
                    sqlCmd.Parameters.AddWithValue("@ALOILP", ALOILP);
                    sqlCmd.Parameters.AddWithValue("@AFOILP_min", AFOILP_min);
                    sqlCmd.Parameters.AddWithValue("@AFOILP_max", AFOILP_max);
                    sqlCmd.Parameters.AddWithValue("@AFOILP", AFOILP);
                    sqlCmd.Parameters.AddWithValue("@AJOILP_rpm", AJOILP_rpm);
                    sqlCmd.Parameters.AddWithValue("@AJOILP_min", AJOILP_min);
                    sqlCmd.Parameters.AddWithValue("@AJOILP_max", AJOILP_max);
                    sqlCmd.Parameters.AddWithValue("@AJOILP", AJOILP);
                    sqlCmd.Parameters.AddWithValue("@ALRPM_rpm", ALRPM_rpm);
                    sqlCmd.Parameters.AddWithValue("@ALRPM_min", ALRPM_min);
                    sqlCmd.Parameters.AddWithValue("@ALRPM_max", ALRPM_max);
                    sqlCmd.Parameters.AddWithValue("@ALRPM", ALRPM);
                    sqlCmd.Parameters.AddWithValue("@AFRPM_rpm", AFRPM_rpm);
                    sqlCmd.Parameters.AddWithValue("@AFRPM_min", AFRPM_min);
                    sqlCmd.Parameters.AddWithValue("@AFRPM_max", AFRPM_max);
                    sqlCmd.Parameters.AddWithValue("@AFRPM", AFRPM);
                    sqlCmd.Parameters.AddWithValue("@ABSFC_rpm", ABSFC_rpm);
                    sqlCmd.Parameters.AddWithValue("@ABSFC_min", ABSFC_min);
                    sqlCmd.Parameters.AddWithValue("@ABSFC_max", ABSFC_max);
                    sqlCmd.Parameters.AddWithValue("@ABSFC", ABSFC);
                    sqlCmd.Parameters.AddWithValue("@TIMENO", TIMENO);
                    int rowNum = sqlCmd.ExecuteNonQuery();
                }

                return true;
            }

            catch (SqlException)
            {
                Debug.Print("InsertResultData DB 과정에서 SQL 에러 발생.");
                return false;
            }
        }

        public int UpdateEngineNumberonResultData(string egno_content)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"IF EXISTS ( SELECT EGNO FROM {resultTableName} WHERE EGNO = @EGNO ) " +
                        $"BEGIN " +
                        $"WITH temp AS" +
                        $"(" +
                        $"SELECT EGNO, CAST(ROW_NUMBER() OVER (ORDER BY INSDATE DESC) AS VARCHAR) number FROM {resultTableName} WHERE EGNO LIKE @EGNOForLike AND (EGNO = @EGNO OR LEN(EGNO) = LEN(@EGNO)+2)" +
                        $") " +
                        $"UPDATE temp SET EGNO = @EGNO + '_' +number " +
                        $"END ";

                    sqlCmd.Parameters.AddWithValue("@EGNO", egno_content);
                    sqlCmd.Parameters.AddWithValue("@EGNOForLike", egno_content + "%");
                    int rowNum = sqlCmd.ExecuteNonQuery();

                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"SELECT EGNO, CAST(ROW_NUMBER() OVER (ORDER BY INSDATE DESC) AS VARCHAR) number FROM {resultTableName} WHERE EGNO LIKE @EGNOForLike AND (EGNO = @EGNO OR LEN(EGNO) = LEN(@EGNO)+2)";
                    sqlCmd.Parameters.AddWithValue("@EGNO", egno_content);
                    sqlCmd.Parameters.AddWithValue("@EGNOForLike", egno_content + "%");

                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                    DataSet ds = new DataSet();

                    da.Fill(ds, suffixTableName);

                    int result = ds.Tables[0].Rows.Count + 1;
                    return result;
                }
            }

            catch (SqlException)
            {
                Debug.Print("UpdateEngineNumberonResultData DB 과정에서 SQL 에러 발생.");
                return -1;
            }
        }

        public bool UpdateWorkOrder(string egno_content, string worker_content)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"UPDATE {workOrderTableName} SET ECUUsedYN = 'Y', UPDUser = @worker, UPDDTTM =@now WHERE EGNO = @EGNO";
                    sqlCmd.Parameters.AddWithValue("@EGNO", egno_content);
                    sqlCmd.Parameters.AddWithValue("@worker", worker_content);
                    sqlCmd.Parameters.AddWithValue("@now", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    int rowNum = sqlCmd.ExecuteNonQuery();
                }

                return true;
            }
            catch (SqlException)
            { 
                Debug.Print("UpdateWorkOrder DB 과정에서 SQL 에러 발생.");
                return false;
            }
        }
    }
}