﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;

namespace DSIC_Local.Step
{
    enum enumPhase
    {
        None = 0,
        Ramp = 1,
        Common = 2,
    }

    enum enumMode
    {
        ModeIdle = 0x60,
        ModeIdleControl = 0x61,
        ModeTA = 0x62,
        ModeNA = 0x63,
        ModeTN = 0x64,
        ModeNT = 0x65
    }

    public class StepMode
    {
        public List<Step> steps = new List<Step>();
        List<SetValue> profile = new List<SetValue>();
        private Timer timerStepMode;
        private Timer timerIdle;
        int mATTimer = 500;
        Stopwatch stopWatch = new Stopwatch(); // 실제 경과시간 체크용
        public bool isFinished = false;// 다른 UI에서 스텝모드 종료 체크용
        string remoteFeedbackVariableName = Properties.Settings.Default.Remote_Feedback;

        string dynoFeedbackSpeedVariableName = Properties.Settings.Default.BME_Speed_FeedBack;
        string dynoFeedbackTorqueVariableName = Properties.Settings.Default.BME_Torque_Feedback;

        string engineFeedbackSpeedVariableName = Properties.Settings.Default.BME_Speed_FeedBack;
        string engineFeedbackTorqueVariableName = Properties.Settings.Default.BME_Torque_Feedback;
        string engineFeedbackThrottleVariableName = Properties.Settings.Default.PLC_Throttle_Demand;

        string speedControlVariableName = Properties.Settings.Default.BME_Speed_Demand;
        string torqueControlVariableName = Properties.Settings.Default.BME_Torque_Demand;
        string throttleControlVariableName = Properties.Settings.Default.PLC_Throttle_Demand;

        string modeSpeedVariableName = Properties.Settings.Default.BME_Mode_Speed;
        string modeTorqueVariableName = Properties.Settings.Default.BME_Mode_Torque;
        //string modeThrottleVariableName = "PLC_Mode_Throttle";

        VariableInfo modeSpeedVar;
        VariableInfo modeTorqueVar;

        public StepMode()
        {
            timerStepMode = new Timer(mATTimer);
            timerStepMode.Elapsed += OnRunStepMode;
            //timerStepMode.Enabled = true;

            timerIdle = new Timer(mATTimer);
            timerIdle.Elapsed += OnRunIdle;
            //timerIdle.Enabled = true;

            modeSpeedVar = Main.project.sysVarInfo.FindVariable(modeSpeedVariableName);
            modeTorqueVar = Main.project.sysVarInfo.FindVariable(modeTorqueVariableName);
            //modeThrottleVar = Main.project.sysVarInfo.FindVariable(modeThrottleVariableName);
        }

        public bool run = false;
        public bool pause = false;

        public double totalTime = 0;
        public double currentTime;

        int currentIndex = 0;
        public int currentStep = 0;
        int currentPhase = 0;
        enumMode currentMode;

        double phaseTotalTime = 0;//Phase 전체 시간. Phase는 Ramp, Common, Settling등을 의미
        double phaseCurrentTime = 0;//Phase 현재 시간
        public double stepCurrentTime = 0;//Step(모든 Phase를 합한) 현재 시간

        int currentCycle = 0;//현재 반복한 수
        int totalCycle = 0;//전체 반복 수. 전체 반복하고싶으면 이 수를 늘리면 된다.

        int[] arrIterationCount;

        private void OnRunStepMode(Object source, ElapsedEventArgs e)
        {
            if (run & !pause)
            {
                Running();
                currentIndex++;
                phaseCurrentTime += (double)timerStepMode.Interval / 1000;
                stepCurrentTime += (double)timerStepMode.Interval / 1000;

                if (currentIndex > profile.Count - 1)
                {
                    currentIndex = 0;
                    currentCycle++;

                    for (int i = 0; i < arrIterationCount.Length; i++)
                    {
                        arrIterationCount[i] = 0;
                    }
                    if (currentCycle > totalCycle)
                    {
                        //종료
                        //Stop();
                        //isFinished = true;
                        pause = true;

                        for (int i = 0; i < steps[steps.Count - 1].measuringChannels.Count; i++)
                        {
                            if (steps[steps.Count - 1].measuringChannels[i].CompleteMeasuring)
                            {
                                Main.stepmodeOutput.AddOutput(new Output(steps.Count, steps[steps.Count - 1].measuringChannels[i]));
                            }

                        }
                    }
                    else
                    {
                        currentTime = 0;
                        currentIndex = 0;
                        currentStep = 0;
                        stepCurrentTime = 0;
                        currentPhase = 0;
                        phaseTotalTime = 0;
                        phaseCurrentTime = 0;
                    }
                }
            }
            else
            {

            }
        }

        double dynoSendData = 0;
        double engineSendData = 0;
        double dynoTargetValue = 0;
        double engineTargetValue = 0;
        double dynoFirstValue = 0;
        double engineFirstValue = 0;
        double ControlDuration = 0;
        double ControlElapsed = 0;

        VariableInfo dynoDemandVar = null;
        VariableInfo engineDemandVar = null;
        public bool CheckMeasuringVariableName()
        {
            for(int i=0; i< steps.Count; i++)
            {
                for(int j=0; j<steps[i].measuringChannels.Count; j++)
                {
                    if (steps[i].measuringChannels[j].assignedVar == null)
                    {
                        return false;
                    }
                }
            }

                return true;
        }
        public void Idle()
        {
            if (ReadyIdle())
            {
                timerIdle.Start();
            }
        }
        private bool ReadyIdle()
        {
            ControlDuration = 5;//중지시 idle로 돌아가는 RampTime sec

            if (modeSpeedVar.RealValue == 1 && modeTorqueVar.RealValue == 0)
            {
                VariableInfo dynoFeedbackVar = Main.project.sysVarInfo.FindVariable(dynoFeedbackSpeedVariableName);
                VariableInfo engineFeedbackVar = Main.project.sysVarInfo.FindVariable(engineFeedbackThrottleVariableName);
                dynoDemandVar = Main.project.sysVarInfo.FindVariable(speedControlVariableName);
                engineDemandVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                dynoTargetValue = 3000;
                engineTargetValue = 0;
                dynoFirstValue = dynoFeedbackVar.RealValue;
                engineFirstValue = engineFeedbackVar.RealValue;

                return true;
            }
            else if (modeSpeedVar.RealValue == 0 && modeTorqueVar.RealValue == 1)
            {
                VariableInfo dynoFeedbackVar = Main.project.sysVarInfo.FindVariable(dynoFeedbackTorqueVariableName);
                VariableInfo engineFeedbackVar = Main.project.sysVarInfo.FindVariable(engineFeedbackThrottleVariableName);
                dynoDemandVar = Main.project.sysVarInfo.FindVariable(torqueControlVariableName);
                engineDemandVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                dynoTargetValue = 0;
                engineTargetValue = 0;
                dynoFirstValue = dynoFeedbackVar.RealValue;
                engineFirstValue = engineFeedbackVar.RealValue;

                return true;
            }
            else
            {
                return false;
            }

        }
        private void OnRunIdle(Object source, ElapsedEventArgs e)
        {

            dynoSendData = (dynoTargetValue - dynoFirstValue) / ControlDuration * ControlElapsed + dynoFirstValue;
            dynoSendData = StepMode.GetIOValue(dynoSendData, dynoDemandVar.CalibrationTable);
            engineSendData = (engineTargetValue - engineFirstValue) / ControlDuration * ControlElapsed + engineFirstValue;
            engineSendData = StepMode.GetIOValue(engineSendData, engineDemandVar.CalibrationTable);

            if (ControlDuration > ControlElapsed)
            {
                Main.CNFODAQCom.SetMelsecData(dynoDemandVar.NorminalDescription, (int)dynoSendData);
                Main.CNFODAQCom.SetMelsecData(engineDemandVar.NorminalDescription, (int)engineSendData);
                ControlElapsed = ControlElapsed + timerIdle.Interval / 1000.0;
            }
            else
            {
                dynoSendData = dynoTargetValue;
                engineSendData = engineTargetValue;

                dynoSendData = StepMode.GetIOValue(dynoSendData, dynoDemandVar.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(dynoDemandVar.NorminalDescription, (int)dynoSendData);
                dynoSendData = StepMode.GetIOValue(engineSendData, engineDemandVar.CalibrationTable);
                Main.CNFODAQCom.SetMelsecData(engineDemandVar.NorminalDescription, (int)engineSendData);
                timerIdle.Stop();
            }
        }
        int EngineRampTime = 0;
        int DynoRampTime = 0;
        string PrevDynoMode = string.Empty;
        string PrevEngineMode = string.Empty;
        double PrevDyno = 0;
        double PrevEngine = 0;

        private void Running()
        {
            //TODO
            // 리모트 신호 정하면 아래 True를 Main.project.sysVarInfo.FindVariable(remoteVariableName).RealValue == 1 로 변경
            if (true)
            {
                currentTime = ((double)currentIndex) / 2;

                if (currentStep != profile[currentIndex].Step)
                {
                    stepCurrentTime = 0;
                }
                // 기존 Measuring 추가 방식. 스텝 지나면 이전 완료된 메저링들이 추가됨.
                //if (currentStep != profile[currentIndex].Step)
                //{
                //    //스텝 전환
                //    stepCurrentTime = 0;

                //    if (profile[currentIndex].Step >= 1)
                //    {
                //        int prevStep = profile[currentIndex].Step - 1;
                //        for (int i = 0; i < steps[prevStep].measuringChannels.Count; i++)
                //        {
                //            if (steps[prevStep].measuringChannels[i].CompleteMeasuring)
                //                Main.stepmodeOutput.AddOutput(new Output(prevStep + 1, steps[prevStep].measuringChannels[i]));
                //        }
                //    }
                //}

                //새로운 방식
                for (int i = 0; i < steps[currentStep].measuringChannels.Count; i++)
                {
                    if (currentTime > steps[currentStep].measuringChannels[i].MeasuringStartTime)
                    {
                        Main.stepmodeOutput.AddOutput(new Output(currentStep + 1, steps[currentStep].measuringChannels[i]));
                    }
                }


                currentStep = profile[currentIndex].Step;

                if (currentPhase != (int)profile[currentIndex].Phase)
                {
                    currentPhase = (int)profile[currentIndex].Phase;
                    phaseTotalTime = (int)profile[currentIndex].PhaseTime;
                    phaseCurrentTime = 0;

                    if (currentPhase == 1)//Ramp
                    {
                        DynoRampTime = EngineRampTime = (int)profile[currentIndex].PhaseTime;

                        if (profile[currentIndex].dynoMode == "N")
                            PrevDyno = (double)(Main.project.sysVarInfo.FindVariable(dynoFeedbackSpeedVariableName).RealValue);
                        else
                            PrevDyno = (double)(Main.project.sysVarInfo.FindVariable(dynoFeedbackTorqueVariableName).RealValue);


                        if (profile[currentIndex].engineMode == "N")
                            PrevEngine = (double)(Main.project.sysVarInfo.FindVariable(engineFeedbackSpeedVariableName).RealValue);
                        else if (profile[currentIndex].engineMode == "T")
                            PrevEngine = (double)(Main.project.sysVarInfo.FindVariable(engineFeedbackTorqueVariableName).RealValue);
                        else
                            PrevEngine = (double)(Main.project.sysVarInfo.FindVariable(engineFeedbackThrottleVariableName).RealValue);
                    }
                }

                currentMode = getMode(profile[currentIndex].dynoMode, profile[currentIndex].engineMode);
                PrevDynoMode = profile[currentIndex].dynoMode;
                PrevEngineMode = profile[currentIndex].engineMode;

                double CurrentDyno = 0;
                double CurrentEngine = 0;

                if (currentPhase == 1 && DynoRampTime > phaseCurrentTime)
                {
                    CurrentDyno = (profile[currentIndex].SetValueDyno - PrevDyno) * phaseCurrentTime / phaseTotalTime + PrevDyno; //PrevDyno = 현재 Dyno 밸류, SetValueList[CurrentIndex].SetValueDyno = 스텝모드에 있는 타겟값
                }
                else
                {
                    CurrentDyno = profile[currentIndex].SetValueDyno;
                }

                if (currentPhase == 1 && EngineRampTime > phaseCurrentTime)
                {
                    CurrentEngine = (profile[currentIndex].SetValueEngine - PrevEngine) * phaseCurrentTime / phaseTotalTime + PrevEngine;
                }
                else if (profile[currentIndex].Goto != -1 && currentIndex == profile.Count - 1)
                {
                    if (arrIterationCount[profile[currentIndex].Step] < profile[currentIndex].Iteration)
                    {
                        arrIterationCount[profile[currentIndex].Step]++;
                        currentIndex = FindGotoCurrentIndex(profile[currentIndex].Goto);
                    }
                }
                else if (profile[currentIndex].Goto != -1 && currentPhase == 5 && profile[currentIndex].Step != profile[currentIndex + 1].Step)//Rest고 고투, 이터레이션 존재하고 다음 스텝과 다른경우.
                {
                    if (arrIterationCount[profile[currentIndex].Step] < profile[currentIndex].Iteration)
                    {
                        arrIterationCount[profile[currentIndex].Step]++;
                        currentIndex = FindGotoCurrentIndex(profile[currentIndex].Goto);
                    }
                }
                else
                {
                    CurrentEngine = profile[currentIndex].SetValueEngine;
                }


                //Measuring
                Measuring(currentStep);

                VariableInfo dynoControlVar = null;
                VariableInfo engineControlVar = null;

                if (currentMode == enumMode.ModeNA)
                {
                    if (modeSpeedVar.RealValue != 1 ||
                        modeTorqueVar.RealValue != 0)
                    {
                        Main.statusForDAQ.ModeDynoSpeed = true;
                        Main.statusForDAQ.ModeDynoTorque = false;
                        Main.statusForDAQ.SetPLCStatus();
                    }
                    dynoControlVar = Main.project.sysVarInfo.FindVariable(speedControlVariableName);
                    engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);
                }
                else if (currentMode == enumMode.ModeNT)
                {

                }
                else if (currentMode == enumMode.ModeTA)
                {

                    if (modeSpeedVar.RealValue != 0 ||
                        modeTorqueVar.RealValue != 1)
                    {
                        Main.statusForDAQ.ModeDynoSpeed = false;
                        Main.statusForDAQ.ModeDynoTorque = true;
                        Main.statusForDAQ.SetPLCStatus();
                    }
                    dynoControlVar = Main.project.sysVarInfo.FindVariable(torqueControlVariableName);
                    engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                }
                else if (currentMode == enumMode.ModeTN)
                {

                }

                CurrentDyno = GetIOValue(CurrentDyno, dynoControlVar.CalibrationTable);
                CurrentEngine = GetIOValue(CurrentEngine, engineControlVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(dynoControlVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(engineControlVar.NorminalDescription, (int)CurrentEngine);
            }
        }
        private void Measuring(int currentStep)
        {
            if (steps[currentStep].measuringChannels.Count == 0)
            {
                return;
            }

            foreach (MeasuringChannel mc in steps[currentStep].measuringChannels)
            {
                if (mc.assignedVar == null)//해당하는 변수 없는 경우
                {
                    return;
                }
                if (mc.OutputItem == OutputItem.SMOKE || mc.OutputItem == OutputItem.LOADSMOKE || mc.OutputItem == OutputItem.FASMOKE)
                {
                    if (stepCurrentTime < 1 && mc.Tested)
                    {
                        mc.Tested = false;
                        mc.Measured = false;
                    }
                    if (stepCurrentTime > mc.MeasuringStartTime && !mc.Tested)
                    {
                        System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(TestSmokeMeter));

                        mc.Tested = true;
                        th.Start();
                    }

                    if (stepCurrentTime > mc.MeasuringEndTime && !mc.Measured)
                    {
                        mc.Measured = true;

                        Main.smokeMeter.ReadMeasure();
                        System.Threading.Thread.Sleep(1000);
                        if (Main.smokeMeter.Sooth > 120)
                        {
                            Main.project.sysVarInfo.FindVariable("매연").IOValue = 1;
                        }
                        else
                        {
                            Main.project.sysVarInfo.FindVariable("매연").IOValue = Main.smokeMeter.Sooth + 1;
                            //Main.project.sysVarInfo.FindVariable("매연").IOValue = i;
                            //i++;
                        }

                        mc.sumValue = Main.project.sysVarInfo.FindVariable("매연").IOValue;
                        mc.countTick = 1;
                        mc.CompleteMeasuring = true;
                    }
                }
                
                else
                {
                    

                    if (stepCurrentTime > mc.MeasuringStartTime && stepCurrentTime <= mc.MeasuringEndTime)
                    {
                        if (stepCurrentTime >= mc.MeasuringStartTime && stepCurrentTime <= mc.MeasuringStartTime + (double)timerStepMode.Interval / 1000)//메저링 첫 진입
                        {
                            if (mc.countTick != 0)//재측정하거나 로깅 중간에 일시정지하고 넘어갔다가 다시 돌아오는 경우. 기존 데이터가 있기때문에 지워준다.
                            {
                                mc.sumValue = 0;
                                mc.countTick = 0;
                            }
                        }

                        mc.sumValue += mc.assignedVar.RealValue;
                        mc.countTick++;

                        double countPerSec = 1000 / (double)mATTimer;
                        if ((mc.MeasuringEndTime - mc.MeasuringStartTime) * countPerSec <= mc.countTick)
                        {
                            mc.CompleteMeasuring = true;
                        }

                        if (mc.OutputItem == OutputItem.MAXPOWER)
                        {
                            Main.resultData.AirTemp = Main.project.sysVarInfo.FindVariable("건구온도").RealValue;
                            Main.resultData.RelativeHumidity = Main.project.sysVarInfo.FindVariable("습도(RH)").RealValue;
                            Main.resultData.AtmosphericPressure = Main.project.sysVarInfo.FindVariable("대기압").RealValue;
                            Main.resultData.KNum = Main.project.sysVarInfo.FindVariable("수정계수").RealValue;
                            Main.resultData.FuelTemp = Main.project.sysVarInfo.FindVariable("연료온도").RealValue;
                            Main.resultData.CoolantTemp = Main.project.sysVarInfo.FindVariable("부동액온도").RealValue;
                            Main.resultData.FuelDensity = Main.project.sysVarInfo.FindVariable("연료비중").RealValue;
                            Main.resultData.ExhaustPressure = Math.Round(Main.project.sysVarInfo.FindVariable("배기가스압력").RealValue * 4.97673, 1); //mbar -> mmaq
                        }
                    }
                    else if (stepCurrentTime > mc.MeasuringEndTime)
                    {

                    }
                }

            }


        }

        private void TestSmokeMeter()
        {
            Main.smokeMeter.ClickMode();
            System.Threading.Thread.Sleep(1000);
            Main.smokeMeter.ClickTest();
        }
        private int FindGotoCurrentIndex(int Goto)
        {
            int _goto = Goto - 1;

            for (int i = 0; i < profile.Count; i++)
            {
                if (profile[i].Step == _goto)
                    return i;
            }

            return -1;
        }
        public void Stop()
        {
            run = false;
            pause = false;
            stopWatch.Stop();

            Debug.Print("Step mode completed.\n\r" + stopWatch.ElapsedMilliseconds.ToString() + "mSec Elapsed.", "Information");
            stopWatch.Reset();

            timerStepMode.Stop();

            currentTime = 0;
            currentIndex = 0;
            currentStep = 0;
            stepCurrentTime = 0;
            currentPhase = 0;
            phaseTotalTime = 0;
            phaseCurrentTime = 0;
        }
        public void Pause()
        {
            if (pause)
                pause = false;
            else
                pause = true;
        }
        public void GoPrevStep()
        {
            //현재스텝 -1로. 첫스텝이면 처음으로.
            //if (run && !pause)
            if (pause)
            {
                if (currentStep != 0)
                {
                    currentIndex = FindGotoCurrentIndex(currentStep);
                }
                else
                {
                    currentIndex = 0;
                }

                phaseCurrentTime = 0;
                currentPhase = 0;
                currentStep = profile[currentIndex].Step;
                stepCurrentTime = 0;

            }
        }

        public void GoNextStep()
        {
            //현재스텝 +1로. 마지막스텝이면 종료.
            //if (run && !pause)
            if (pause)
            {
                if (profile[currentIndex].Goto == -1)
                {
                    if (currentStep != steps.Count - 1)
                    {
                        currentIndex = FindGotoCurrentIndex(currentStep + 2);
                    }
                    else
                    {
                        currentIndex = profile.Count - 1;
                    }
                }
                else
                {
                    if (arrIterationCount[profile[currentIndex].Step] < profile[currentIndex].Iteration)
                    {
                        arrIterationCount[profile[currentIndex].Step]++;
                        currentIndex = FindGotoCurrentIndex(profile[currentIndex].Goto);
                    }
                    else
                    {
                        if (currentStep != steps.Count - 1)
                        {
                            currentIndex = FindGotoCurrentIndex(currentStep + 2);
                        }
                        else
                        {
                            currentIndex = profile.Count - 1;
                        }
                    }
                }

                phaseCurrentTime = 0;
                currentPhase = 0;
                try
                {
                    currentStep = profile[currentIndex].Step;
                }
                catch (ArgumentOutOfRangeException)
                {
                    currentStep = currentStep;
                }
                stepCurrentTime = 0;

            }
        }
        public void GoFirstStep()
        {
            if (pause)
            {
                currentIndex = 0;

                phaseCurrentTime = 0;
                currentPhase = 0;
                currentStep = profile[currentIndex].Step;
                stepCurrentTime = 0;

            }
        }
        public void Run()
        {
            timerStepMode.Start();

            ProfileMake();

            currentIndex = 0;
            currentMode = getMode(profile[currentIndex].dynoMode, profile[currentIndex].engineMode);
            currentTime = 0;
            currentStep = 0;
            currentPhase = 0;
            phaseTotalTime = 0;
            phaseCurrentTime = 0;
            currentMode = 0;

            totalTime = 0;
            currentCycle = 1;


            for (int i = 0; i < steps.Count; i++)
            {
                totalTime = totalTime + steps[i].Ramp + steps[i].Time;
            }

            run = true;
            pause = false;

            stopWatch.Start();
        }
        private void ProfileMake()
        {
            double prevDynoValue = 0;//현재 다이노 값. 램프 계산위해서 현재값이 필요
            double prevEngineValue = 0;//현재 엔진 값. 램프 계산위해서 현재값이 필요
            double currentDynoValue = 0;//스텝모드의 다이노 셋값
            double currentEngineValue = 0;//스텝모드의 다이노 셋값

            arrIterationCount = new int[steps.Count];

            int Goto = 0;
            int Iteration = 0;

            int countPerSec = 0;


            enumPhase Phase = enumPhase.None;

            if (steps.Count > 0)
            {
                int time = 0;
                string dynoMode = steps[0].DynoMode;
                string engineMode = steps[0].EngineMode;

                countPerSec = 1000 / (int)timerStepMode.Interval;

                try
                {
                    if (dynoMode == "N")
                        prevDynoValue = Main.project.sysVarInfo.FindVariable(dynoFeedbackSpeedVariableName).RealValue;
                    else
                        prevDynoValue = Main.project.sysVarInfo.FindVariable(dynoFeedbackTorqueVariableName).RealValue;

                    if (engineMode == "N")
                        prevEngineValue = Main.project.sysVarInfo.FindVariable(engineFeedbackSpeedVariableName).RealValue;
                    else if (engineMode == "T")
                        prevEngineValue = Main.project.sysVarInfo.FindVariable(engineFeedbackTorqueVariableName).RealValue;
                    else
                        prevEngineValue = Main.project.sysVarInfo.FindVariable(engineFeedbackThrottleVariableName).RealValue;
                }
                catch (NullReferenceException)
                {
                    Main.AddLog("컨트롤 밸류 맵핑에 문제가 있습니다.", Log.LogType.Error);
                }

                Goto = steps[0].Goto;
                Iteration = steps[0].Iteration;



                if (Goto != -1)//Goto = -1은 Goto 사용 안한다는 의미
                {
                    Iteration = steps[0].Iteration;
                }
                else
                {
                    Iteration = -1;
                }

                Phase = enumPhase.Ramp;


                //profile.Add(new SetValue(dynoMode, engineMode, 0, Phase, time, prevDynoValue, prevEngineValue, Goto, Iteration));

                for (int i = 0; i < steps.Count; i++)
                {
                    dynoMode = steps[i].DynoMode;
                    engineMode = steps[i].EngineMode;
                    double dynoSetValue = steps[i].DynoSetValue;
                    double engineSetValue = steps[i].EngineSetValue;

                    Goto = steps[i].Goto;

                    if (Goto != -1)//Goto = -1은 Goto 사용 안한다는 의미
                    {
                        Iteration = steps[i].Iteration;
                    }
                    else
                    {
                        Iteration = -1;
                    }

                    Phase = enumPhase.Ramp;
                    time = steps[i].Ramp;

                    //engineRampTime = (int)DT[i][6]; //두산인프라코어 프로젝트의 경우 동력계와 엔진의 Ramptime을 동일하게 잡는다.

                    //if (dynoRampTime > engineRampTime)// 둘 중 더 긴 시간을 ramp time로 잡음
                    //    time = dynoRampTime;
                    //else
                    //    time = engineRampTime;

                    for (int ind = 0; ind < time * countPerSec; ind++) //Ramp time profile make
                    {
                        //Ramp 사용 O
                        //currentDynoValue = (int)((dynoSetValue - prevDynoValue) * ind / (time * countPerSec) + prevDynoValue);
                        //currentEngineValue = (int)((engineSetValue - prevEngineValue) * ind / (time * countPerSec) + prevEngineValue);

                        //Ramp 사용 X
                        currentDynoValue = dynoSetValue;
                        currentEngineValue = engineSetValue;

                        profile.Add(new SetValue(dynoMode, engineMode, i, Phase, time, currentDynoValue, currentEngineValue, Goto, Iteration));
                    }



                    time = steps[i].Time; // Common time profile Make
                    Phase = enumPhase.Common;

                    for (int ind = 0; ind < time * countPerSec; ind++)
                    {
                        currentDynoValue = dynoSetValue;
                        currentEngineValue = engineSetValue;
                        profile.Add(new SetValue(dynoMode, engineMode, i, Phase, time, currentDynoValue, currentEngineValue, Goto, Iteration));
                    }

                    prevDynoValue = currentDynoValue;
                    prevEngineValue = currentEngineValue;
                }
            }
        }
        public void CSVToStep(string path)
        {

            string[] Lines = System.IO.File.ReadAllLines(path, Encoding.Default);
            List<string> stepList = Lines.ToList<string>();
            stepList.RemoveRange(0, 3);

            Step step = null;
            bool isNormalStep = true;

            foreach (string str in stepList)
            {
                string[] arrStep = str.Split(',');

                try
                {
                    if (step != null & isNormalStep)//Measuring Step의 경우 List에 Add안하고 Step내부 Measuring 변수 List에만 추가해야 하기에 그 기능을 위한 
                    {
                        steps.Add(step); // 첫번째 방문은 null일테니 그냥 건너 뛰고 아래에서 step 정보를 넣고 그 다음 방문에 리스트에 더해주는 식.
                    }
                    int.Parse(arrStep[0]);
                    step = new Step();
                    isNormalStep = true;
                }
                catch (FormatException)
                {
                    isNormalStep = false;
                }

                if (isNormalStep)
                {
                    if (arrStep[0] == "" && arrStep[1] == "" && arrStep[2] == "")
                    {
                        break;
                    }

                    step.StepNumber = int.Parse(arrStep[0]);
                    step.StepName = arrStep[1];
                    step.DynoMode = arrStep[2];
                    step.DynoSetValue = double.Parse(arrStep[3]);
                    step.EngineMode = arrStep[4];
                    step.EngineSetValue = double.Parse(arrStep[5]);
                    step.Ramp = int.Parse(arrStep[6]);
                    step.Time = int.Parse(arrStep[7]);
                    //step.Exhaust = int.Parse(arrStep[15]);
                }
                string outputItem = arrStep[8];

                if (outputItem != "-")
                {
                    MeasuringChannel mc = new MeasuringChannel();
                    OutputItem oi;
                    Enum.TryParse(outputItem, out oi);
                    mc.OutputItem = oi;
                    mc.assignedVar = Main.project.sysVarInfo.FindVariable(arrStep[9]);
                    mc.MeasuringStartTime = int.Parse(arrStep[10]);
                    mc.MeasuringEndTime = int.Parse(arrStep[11]);
                    mc.Unit = arrStep[12];
                    mc.LowValue = double.Parse(arrStep[13]);
                    mc.HighValue = double.Parse(arrStep[14]);
                    mc.StepName = step.StepName;
                    mc.DynoSetValue = step.DynoSetValue.ToString();
                    mc.StepTotalTime = step.Ramp + step.Time;
                    step.measuringChannels.Add(mc);
                }
            }
            if (isNormalStep)
                steps.Add(step); // 그 다음 방문때 리스트에 집어넣는 방식이기에 마지막 반복의 스텝은 추가가 안된다. 끝나고 여기서 따로 추가해줌.
        }
        public object ShallowCopy()
        {
            return (StepMode)this.MemberwiseClone();
        }
        private enumMode getMode(string dynoMode, string engineMode)
        {
            enumMode CurrentMode;
            if (dynoMode == "N")
            {
                if (engineMode == "α")
                {
                    CurrentMode = enumMode.ModeNA;
                }
                else
                {
                    CurrentMode = enumMode.ModeNT;
                }
            }
            else
            {
                if (engineMode == "α")
                {
                    CurrentMode = enumMode.ModeTA;
                }
                else
                {
                    CurrentMode = enumMode.ModeTN;
                }
            }

            return CurrentMode;
        }
        public static double GetIOValue(double RealValue, List<ValuePair> CalibrationTable)
        {
            double result = 0;

            if (CalibrationTable.Count == 0)
            {
                result = RealValue;
            }
            else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
            {
                result = (double)CalibrationTable[0].IOValue / (double)CalibrationTable[0].RealValue * (double)RealValue; // physical value = d / c * Rawdata.
            }
            else if (CalibrationTable.Count == 2)//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
            {
                result = ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) / ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) * ((double)RealValue - (double)CalibrationTable[1].RealValue) + (double)CalibrationTable[1].IOValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
            }
            else if (CalibrationTable.Count > 2)//2개 인것과 똑같은데 처음에 적정 구간 찾는 부분이 추가됨
            {
                int i = 0;

                for (i = 0; i < CalibrationTable.Count - 1; i++)
                {
                    if (CalibrationTable[i].RealValue <= RealValue && CalibrationTable[i + 1].RealValue >= RealValue)
                    {
                        break;
                    }
                }

                result = ((double)CalibrationTable[i].IOValue - (double)CalibrationTable[i + 1].IOValue) / ((double)CalibrationTable[i].RealValue - (double)CalibrationTable[i + 1].RealValue) * ((double)RealValue - (double)CalibrationTable[i + 1].RealValue) + (double)CalibrationTable[i + 1].IOValue;
            }

            return result;
        }


    }

    public class Step
    {
        private int stepNumber;
        private string stepName;
        private string dynoMode;
        private double dynoSetValue;
        private string engineMode;
        private double engineSetValue;
        private int ramp;
        private int time;
        private int exhaust;
        private int _goto = -1;
        private int iteration = 1;
        private int stepTotalTime = 0;

        public List<MeasuringChannel> measuringChannels = new List<MeasuringChannel>();

        public int StepNumber { get => stepNumber; set => stepNumber = value; }
        public string StepName { get => stepName; set => stepName = value; }

        public string DynoMode { get => dynoMode; set => dynoMode = value; }
        public double DynoSetValue { get => dynoSetValue; set => dynoSetValue = value; }
        public string EngineMode { get => engineMode; set => engineMode = value; }
        public double EngineSetValue { get => engineSetValue; set => engineSetValue = value; }
        public int Ramp { get => ramp; set => ramp = value; }
        public int Time { get => time; set => time = value; }
        public int Exhaust { get => exhaust; set => exhaust = value; }
        public int Goto { get => _goto; set => _goto = value; }
        public int Iteration { get => iteration; set => iteration = value; }
        public int StepTotalTime
        {
            get
            {
                return Ramp + Time;
            }
            set => stepTotalTime = value;
        }
        public string MeasureStartTime
        {
            get
            {
                if (measuringChannels.Count > 0)
                {
                    return measuringChannels[0].MeasuringStartTime.ToString();
                }

                return null;
            }
        }
        public string MeasureEndTime
        {
            get
            {
                if (measuringChannels.Count > 0)
                {
                    return measuringChannels[0].MeasuringEndTime.ToString();
                }

                return null;
            }
        }
    }

    public class MeasuringChannel
    {
        public OutputItem OutputItem;

        public VariableInfo assignedVar;

        private int measuringStartTime;
        private int measuringEndTime;
        private string unit;
        private double lowValue;
        private double highValue;
        private bool completeMeasuring = false;
        private bool tested = false;
        private bool measured = false;
        private string stepName;
        private int stepTotalTime;
        private string dynoSetValue;
        public double sumValue;
        public double countTick;

        public string VarName
        {
            get
            {
                return assignedVar?.VariableName;
            }
        }
        public int MeasuringStartTime { get => measuringStartTime; set => measuringStartTime = value; }
        public int MeasuringEndTime { get => measuringEndTime; set => measuringEndTime = value; }
        public string Unit { get => unit; set => unit = value; }
        public double LowValue { get => lowValue; set => lowValue = value; }
        public double HighValue { get => highValue; set => highValue = value; }
        public double AverageValue
        {

            get
            {
                double value = Math.Round(sumValue / countTick, assignedVar.Round);
                if (Double.IsNaN(value))
                {
                    return 0;
                }

                return value;
            }
        }
        public bool Ok
        {
            get
            {
                if ((AverageValue >= lowValue) && (AverageValue <= HighValue))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool CompleteMeasuring { get => completeMeasuring; set => completeMeasuring = value; }
        public bool Tested { get => tested; set => tested = value; }
        public bool Measured { get => measured; set => measured = value; }
        public string StepName { get => stepName; set => stepName = value; }
        public int StepTotalTime { get => stepTotalTime; set => stepTotalTime = value; }
        public string DynoSetValue { get => dynoSetValue; set => dynoSetValue = value; }
    }

    class SetValue
    {
        public string dynoMode;
        public string engineMode;
        public enumPhase Phase;
        public int Step;
        public double SetValueEngine;
        public double SetValueDyno;
        public int PhaseTime;
        public int Goto;
        public int Iteration;
        public SetValue(string dynoMode, string engineMode, int Step, enumPhase Phase, int PhaseTime, double SetValueDyno, double SetValueEngine, int Goto, int Iteration)
        {
            this.dynoMode = dynoMode;
            this.engineMode = engineMode;
            this.Step = Step;
            this.Phase = Phase;
            this.SetValueDyno = SetValueDyno;
            this.SetValueEngine = SetValueEngine;
            this.PhaseTime = PhaseTime;
            this.Goto = Goto;
            this.Iteration = Iteration;
        }
    }
}