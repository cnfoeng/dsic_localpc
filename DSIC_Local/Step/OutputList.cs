﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSIC_Local.Step
{
    public enum OutputItem
    {
        MAXRPM,
        IDLERPM,
        MAXPOWER,
        IDLEOILPRESSURE,
        FULLOILPRESSURE,
        SMOKE,
        FUELCONSUMPTION,
        JBOILPRESSURE,
        LOADSMOKE,
        FASMOKE,
        MAXTORQUE,
    }
    public class StepmodeOutput
    {
        public bool isUpdated = false;

        public List<Output> outputs = new List<Output>();

        public void AddOutput(Output output)
        {

            for(int i=0; i<outputs.Count; i++)
            {
                //이미 존재하면 덮어 씀
                if(outputs[i].StepNumber == output.StepNumber && outputs[i].OutputItem == output.OutputItem)
                {
                    outputs[i] = output;
                    isUpdated = true;
                    return;
                }
            }

            outputs.Add(output);
            isUpdated = true;
        }
    }
    public class Output
    {
        int stepNumber;
        string stepName;
        string outputItem = string.Empty;
        string measuringChannel = string.Empty;
        double value =0;
        bool ok = false;
        string condition = string.Empty;
        int stepTotalTime =0;
        string dynoSetValue = string.Empty;
        string unit;
        double lowValue;
        double highValue;

        public int StepNumber { get => stepNumber; set => stepNumber = value; }
        public string StepName { get => stepName; set => stepName = value; }
        public string OutputItem { get => outputItem; set => outputItem = value; }
        public string MeasuringChannel { get => measuringChannel; set => measuringChannel = value; }
        public double Value { get => value; set => this.value = value; }
        public string Ok
        {
            get
            {
                if (ok)
                {
                    return "O";
                }
                else
                {
                    return "X";
                }
            }
        }
        public string Condition { get => condition; set => condition = value; }
        public int StepTotalTime { get => stepTotalTime; set => stepTotalTime = value; }
        public string DynoSetValue { get => dynoSetValue; set => dynoSetValue = value; }
        public string Unit { get => unit; set => unit = value; }
        public double LowValue { get => lowValue; set => lowValue = value; }
        public double HighValue { get => highValue; set => highValue = value; }

        public OutputItem enumOutputItem;

        public Output(int stepNumber, MeasuringChannel measuringChannel)
        {
            this.stepNumber = stepNumber;
            this.outputItem = measuringChannel.OutputItem.ToString();
            this.measuringChannel = measuringChannel.VarName;
            this.value = measuringChannel.AverageValue;
            this.ok = measuringChannel.Ok;
            this.condition = $"{measuringChannel.LowValue} <= 측정값 <= {measuringChannel.HighValue}";
            this.enumOutputItem = measuringChannel.OutputItem;
            this.stepName = measuringChannel.StepName;
            this.stepTotalTime = measuringChannel.StepTotalTime;
            this.dynoSetValue = measuringChannel.DynoSetValue;
            this.unit = measuringChannel.Unit;
            this.lowValue = measuringChannel.LowValue;
            this.highValue = measuringChannel.HighValue;
        }
    }

    //class OutputForDatabase//데이터베이스 전송용 클래스. 결과화면용. 스모크미터 에버리징.
    //{
    //    int stepNumber;
    //    string outputItem = string.Empty;
    //    string measuringChannel = string.Empty;
    //    double value = 0;
    //    bool ok = false;
    //    string condition = string.Empty;


    //    public int StepNumber { get => stepNumber; set => stepNumber = value; }
    //    public string OutputItem { get => outputItem; set => outputItem = value; }
    //    public string MeasuringChannel { get => measuringChannel; set => measuringChannel = value; }
    //    public double Value { get => value; set => this.value = value; }
    //    public string Ok
    //    {
    //        get
    //        {
    //            if (ok)
    //            {
    //                return "O";
    //            }
    //            else
    //            {
    //                return "X";
    //            }
    //        }
    //    }
    //    public string Condition { get => condition; set => condition = value; }

    //    public OutputForDatabase(List<Output> outputs)
    //    {
    //      foreach(Output output in outputs)
    //        {
    //            if(output.
    //        }   
    //    }
    //}
}
