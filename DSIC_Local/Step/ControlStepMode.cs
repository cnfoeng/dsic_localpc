﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DSIC_Local.Log;

namespace DSIC_Local.Step
{
    public partial class ControlStepMode : DevExpress.XtraEditors.XtraForm
    {
        Main main;

        string ECUPowerStatusName = Properties.Settings.Default.PLC_ECU_Power_STATUS;
        string ECUPowerOnName = Properties.Settings.Default.PLC_ECU_Power_ON;
        string IGStatusName = Properties.Settings.Default.PLC_IG_STATUS;
        string IGOnName = Properties.Settings.Default.PLC_IG_ON;
        string EngineStartStatusName = Properties.Settings.Default.PLC_Engine_Start_STATUS;
        string EngineStartOnName = Properties.Settings.Default.PLC_Engine_Start_ON;


        VariableInfo ECUPowerStatusVar;
        VariableInfo ECUPowerOnVar;
        VariableInfo IGStatusVar;
        VariableInfo IGOnVar;
        VariableInfo EngineStartStatusVar;
        VariableInfo EngineStartOnVar;
        VariableInfo modeSpeedVar;
        VariableInfo modeTorqueVar;
        VariableInfo dynoControlVar;
        VariableInfo engineControlVar;

        string RemoteDemandName = Properties.Settings.Default.Remote_Demand;
        string RemoteFeedbackName = Properties.Settings.Default.Remote_Feedback;

        VariableInfo RemoteDemandVar;
        VariableInfo RemoteFeedbackVar;
        public ControlStepMode(Main main)
        {
            InitializeComponent();

            this.main = main;
            ECUPowerStatusVar = Main.project.sysVarInfo.FindVariable(ECUPowerStatusName);
            ECUPowerOnVar = Main.project.sysVarInfo.FindVariable(ECUPowerOnName);
            IGStatusVar = Main.project.sysVarInfo.FindVariable(IGStatusName);
            IGOnVar = Main.project.sysVarInfo.FindVariable(IGOnName);
            EngineStartStatusVar = Main.project.sysVarInfo.FindVariable(EngineStartStatusName);
            EngineStartOnVar = Main.project.sysVarInfo.FindVariable(EngineStartOnName);

            RemoteDemandVar = Main.project.sysVarInfo.FindVariable(RemoteDemandName);
            RemoteFeedbackVar = Main.project.sysVarInfo.FindVariable(RemoteFeedbackName);
        }

        private void ControlStepMode_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (Main.stepMode == null)
            {
                return;
            }


            if (Main.stepMode.run)
            {
                btnStop.Enabled = true;
                btnPause.Enabled = true;
                btnRun.Enabled = false;

                if (Main.stepMode.pause)
                {

                    if (btnPause.Text == "일시정지")
                    {
                        btnPause.Text = "다시시작";
                        Main.AddLog("스텝모드 일시정지", LogType.Information);
                        btnPrev.Enabled = true;
                        
                        if (Main.stepMode.currentStep != Main.stepMode.steps.Count-1)
                            btnNext.Enabled = true;
                    }
                }
                else
                {
                    if (btnPause.Text == "다시시작")
                    {
                        btnPause.Text = "일시정지";
                        Main.AddLog("스텝모드 다시시작", LogType.Information);
                        btnPrev.Enabled = false;
                        btnNext.Enabled = false;
                    }
                }
            }
            else
            {
                btnPrev.Enabled = false;
                btnStop.Enabled = false;
                btnPause.Enabled = false;
                btnNext.Enabled = false;
                btnRun.Enabled = true;
            }

            try
            {
                lbCurrentStepNumber.Text = "현재 스텝 : " + (Main.stepMode.currentStep).ToString() + ". " + Main.stepMode.steps[Main.stepMode.currentStep].StepName;
                lbCurrentStepTime.Text = "현재 스텝 시간 : " + Main.stepMode.stepCurrentTime.ToString("F0") + " / " + Main.stepMode.steps[Main.stepMode.currentStep].StepTotalTime.ToString("F0"); //sec to hh mm ss 으로 변경필요.
                progressBarControl1.Properties.Maximum = Main.stepMode.steps[Main.stepMode.currentStep].StepTotalTime;
                progressBarControl1.EditValue = Main.stepMode.stepCurrentTime;
            }
            catch (System.ArgumentOutOfRangeException)
            {

            }

            if (Main.stepMode.isFinished)
            {
                Main.AddLog("스텝모드 완료", LogType.Information);

                btnPrev.Enabled = false;
                btnStop.Enabled = false;
                btnPause.Enabled = false;
                btnNext.Enabled = false;

                btnRun.Enabled = true;

                Main.stepMode.isFinished = false;
            }
            //Todo 스텝모드
            if (ECUPowerStatusVar.RealValue == 1)
            {
                cbtnECUPower.Checked = true;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnECUPower.Checked = false;
                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }

            if (EngineStartStatusVar.RealValue == 1)
            {
                cbtnEngineStart.Checked = true;
                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                cbtnEngineStart.Checked = false;
                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (RemoteFeedbackVar.RealValue != 1)
            {
                Main.statusForDAQ.RemoteDemand = true;
                Main.statusForDAQ.SetPLCStatus();
            }

            Main.stepMode = new StepMode();
            Main.stepmodeOutput.outputs.Clear();

            if (Main.localScheduleFilePath != "")
            {
                Main.stepMode.CSVToStep(Main.localScheduleFilePath);
            }
            else
            {
                Main.stepMode.CSVToStep(Application.StartupPath + "\\ttt.csv");
            }

            if (!Main.stepMode.CheckMeasuringVariableName())
            {
                XtraMessageBox.Show("<size=14>스텝모드의 측정항목 변수가 프로그램상에 존재하지 않습니다. 스텝모드가 정상적으로 진행되지 않습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            main.mainScreen.UpdateGridStepDataSource();
            tbScheduleFileName.Text = Main.localScheduleFilePath;
            Main.stepMode.Run();

            Main.AddLog("스텝모드 시작", LogType.Information);

            Main.resultData.startDate = DateTime.Now.ToString("yyyy.MM.dd");
            Main.resultData.startTime = DateTime.Now.ToString("hh:mm:ss");

            Main.resultData.AirTemp = Main.project.sysVarInfo.FindVariable("건구온도").RealValue;
            Main.resultData.RelativeHumidity = Main.project.sysVarInfo.FindVariable("습도(RH)").RealValue;
            Main.resultData.AtmosphericPressure = Main.project.sysVarInfo.FindVariable("대기압").RealValue;
            Main.resultData.KNum = Main.project.sysVarInfo.FindVariable("수정계수").RealValue;
            Main.resultData.FuelTemp = Main.project.sysVarInfo.FindVariable("연료온도").RealValue;
            Main.resultData.CoolantTemp = Main.project.sysVarInfo.FindVariable("부동액온도").RealValue;
            Main.resultData.FuelDensity = Main.project.sysVarInfo.FindVariable("연료비중").RealValue;
            Main.resultData.ExhaustPressure = Math.Round(Main.project.sysVarInfo.FindVariable("배기가스압력").RealValue * 4.97673 , 1); //mbar -> mmaq
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Main.stepMode.Stop();

            Main.AddLog("스텝모드 중단", LogType.Information);

            btnPrev.Enabled = false;
            btnStop.Enabled = false;
            btnPause.Enabled = false;
            btnNext.Enabled = false;

            btnRun.Enabled = true;

            Main.stepMode.Idle();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            Main.stepMode.Pause();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            Main.stepMode.GoPrevStep();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Main.stepMode.GoNextStep();
        }

        private void ControlStepMode_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.controlStepModeForm = null;
        }

        private void cbtnECUPower_Click(object sender, EventArgs e)
        {
            if (cbtnECUPower.Checked)
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.EcuPowerOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.EcuPowerOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnECUPower.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }

        private void cbtnEngineStart_Click(object sender, EventArgs e)
        {
            Idle();
            System.Threading.Thread.Sleep(500);

            if (cbtnEngineStart.Checked)
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();



                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            }
            else
            {
                Main.statusForDAQ.StOnDemand = true;
                Main.statusForDAQ.SetPLCStatus();
                System.Threading.Thread.Sleep(500);
                Main.statusForDAQ.StOnDemand = false;
                Main.statusForDAQ.SetPLCStatus();

                cbtnEngineStart.Appearance.BackColor = Color.FromArgb(255, 192, 192);
            }
        }
        public void Idle()
        {
            string modeSpeedVariableName = Properties.Settings.Default.BME_Mode_Speed;
            string modeTorqueVariableName = Properties.Settings.Default.BME_Mode_Torque;
            string speedControlVariableName = Properties.Settings.Default.BME_Speed_Demand;
            string torqueControlVariableName = Properties.Settings.Default.BME_Torque_Demand;
            string throttleControlVariableName = Properties.Settings.Default.PLC_Throttle_Demand;

            modeSpeedVar = Main.project.sysVarInfo.FindVariable(modeSpeedVariableName);
            modeTorqueVar = Main.project.sysVarInfo.FindVariable(modeTorqueVariableName);

            if (modeSpeedVar.RealValue == 1 && modeTorqueVar.RealValue == 0)
            {
                dynoControlVar = Main.project.sysVarInfo.FindVariable(speedControlVariableName);
                engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                double CurrentDyno = StepMode.GetIOValue(3000, dynoControlVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, engineControlVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(dynoControlVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(engineControlVar.NorminalDescription, (int)CurrentEngine);
            }
            else if (modeSpeedVar.RealValue == 0 && modeTorqueVar.RealValue == 1)
            {
                dynoControlVar = Main.project.sysVarInfo.FindVariable(torqueControlVariableName);
                engineControlVar = Main.project.sysVarInfo.FindVariable(throttleControlVariableName);

                double CurrentDyno = StepMode.GetIOValue(0, dynoControlVar.CalibrationTable);
                double CurrentEngine = StepMode.GetIOValue(0, engineControlVar.CalibrationTable);

                Main.CNFODAQCom.SetMelsecData(dynoControlVar.NorminalDescription, (int)CurrentDyno);
                Main.CNFODAQCom.SetMelsecData(engineControlVar.NorminalDescription, (int)CurrentEngine);
            }
        }
        
    }
}