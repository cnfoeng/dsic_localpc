﻿/*!
 * @file    
 * @brief   Contains the XCP helper stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Comm.XCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;

namespace jnsoft.Comm
{
  /// <summary>
  /// Event arguments used on receiving acquisition data.
  /// </summary>
  public sealed class ValuesReceivedEventArgs : EventArgs
  {
    /// <summary>The DAQ list number</summary>
    public int DAQListNo { get; private set; }
    /// <summary>timestamp</summary>
    public double Seconds { get; private set; }
    /// <summary>received data</summary>
    public byte[] Data;
    public ValuesReceivedEventArgs(int daqListNo, double seconds, ref byte[] data)
    { DAQListNo = daqListNo; Seconds = seconds; Data = data; }
  }
  /// <summary>
  /// Maps a XCP DAQList number to a specific XCP Event.
  /// </summary>
  public sealed class DAQToEvtMapDict : Dictionary<UInt16, RespGetDAQEventInfo> { }
  /// <summary>
  /// Implements a buffer to store data points to.
  /// 
  /// - Uses a memory mapped file (and therefore requires a .NET Framework V4)
  /// </summary>
  public sealed class DataPointBuffer : IDisposable
  {
    #region members
    static uint mNextId;
    const double mOverSize = 0.2;
    static readonly int mDataItemSize = Marshal.SizeOf(typeof(DataPoint));
    MemoryMappedFile mMMFile;
    string mName;
    long mMaxDataItems;
    long mMaxItems;
    long mCurrentIndex;
    MemoryMappedViewStream mWriteStream;
    MemoryMappedViewStream mReadStream;
    BinaryWriter mBW;
    BinaryReader mBR;
    readonly object mWriteLock = new object();
    #endregion
    #region ctor
    /// <summary>
    /// Creates a new instance of DataPointBuffer.
    /// </summary>
    /// <param name="name">A user defined identifier</param>
    /// <param name="maxDataItems">The maximum count of DataPoints to store</param>
    public DataPointBuffer(string name, int maxDataItems)
    {
      mMaxDataItems = maxDataItems;
      mName = string.Format("{0}_{1}", name, mNextId++);

      mMaxItems = mMaxDataItems + (long)(mMaxDataItems * mOverSize);
      // create the memory mapped file
      mMMFile = MemoryMappedFile.CreateNew(mName, mMaxItems * mDataItemSize);
      // create a write stream and a binary writer
      mWriteStream = mMMFile.CreateViewStream(0, mMaxItems * mDataItemSize, MemoryMappedFileAccess.Write);
      mBW = new BinaryWriter(mWriteStream);
      // create a read stream and a binary reader
      mReadStream = mMMFile.CreateViewStream(0, mMaxItems * mDataItemSize, MemoryMappedFileAccess.Read);
      mBR = new BinaryReader(mReadStream);
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the name of the corresponding measurement.
    /// </summary>
    public string Name { get { return mName.Substring(0, mName.LastIndexOf('_') - 1); } }
    /// <summary>
    /// Gets the current count of available data points.
    /// </summary>
    public long Count { get { return mCurrentIndex; } }
    #endregion
    #region methods
    /// <summary>
    /// Add a data point to the data buffer.
    /// </summary>
    /// <param name="p">The data point to add</param>
    public void add(DataPoint p)
    {
      if (mCurrentIndex == mMaxItems)
        // delete
        removeOldestItems();
      p.serialize(mBW);
      ++mCurrentIndex;
    }
    /// <summary>
    /// Removes the oldest data points from the DataPointBuffer.
    /// </summary>
    void removeOldestItems()
    {
      lock (mWriteLock)
      {
        mWriteStream.Position = 0;
        mReadStream.Position = (long)(mMaxDataItems * mDataItemSize * mOverSize);
        long diff = mReadStream.Length - mReadStream.Position;
        var buffer = new byte[0x10000];
        while (diff > 0)
        {
          int len = Math.Min(buffer.Length, (int)diff);
          mReadStream.Read(buffer, 0, len);
          mWriteStream.Write(buffer, 0, len);
          diff = mReadStream.Length - mReadStream.Position;
        }
        mCurrentIndex = mMaxDataItems;
      }
    }
    /// <summary>
    /// Gets the data at the specified index.
    /// </summary>
    /// <param name="index">The index of the data point to read</param>
    /// <param name="dp">The data point</param>
    /// <returns>true if returned data point is valid</returns>
    public bool getValue(int index, ref DataPoint dp)
    {
      lock (mWriteLock)
      {
        if (index < 0 || index >= mCurrentIndex)
          return false;
        mReadStream.Position = (long)(index * mDataItemSize);
        dp = DataPoint.deserialize(mBR);
        return true;
      }
    }
    /// <summary>
    /// Finds the first valid index to be within the specified time.
    /// </summary>
    /// <param name="time">The timestamp to search for</param>
    /// <returns>The first valid index to be within the specified time</returns>
    public int findTimeIndex(long time)
    {
      lock (mWriteLock)
      {
        var dp = new DataPoint();
        int nLow = 0;
        int nHigh = (int)mCurrentIndex;
        while (nLow < nHigh)
        {
          int nMid = nLow + (nHigh - nLow) / 2; // workaround for sum overflow
          getValue(nMid, ref dp);
          double compareTime = dp.X;
          if (compareTime <= time)
            nLow = nMid + 1;
          else
            nHigh = nMid;
        }
        return nLow > 0 ? nLow - 1 : 0;
      }
    }
    /// <summary>
    /// Finds the first valid index to be within the specified start time.
    /// </summary>
    /// <param name="endTime">The end timestamp to search for</param>
    /// <param name="startTime">The start timestamp to search for</param>
    /// <param name="endIndex">The resulting end index</param>
    /// <param name="startIndex">The resulting start index</param>
    public void findTimeIndex(long endTime, long startTime, out int endIndex, out int startIndex)
    {
      lock (mWriteLock)
      {
        var dp = new DataPoint();
        endIndex = findTimeIndex(endTime);
        int nLow = 0;
        int nHigh = endIndex + 1;
        while (nLow < nHigh)
        {
          int nMid = nLow + (nHigh - nLow) / 2; // workaround for sum overflow
          getValue(nMid, ref dp);
          double compareTime = dp.X;
          if (compareTime <= startTime)
            nLow = nMid + 1;
          else
            nHigh = nMid;
        }
        startIndex = nLow > 0 ? nLow - 1 : 0;
      }
    }
    /// <summary>
    /// Writes the specified DataPoint values into the specified buffer.
    /// </summary>
    /// <param name="points">The data point memory to write to</param>
    /// <param name="firstIndex">The index to read from</param>
    /// <param name="count">The count of data points to read</param>
    public void getValues(IntPtr points, int firstIndex, int count)
    {
      lock (mWriteLock)
      {
        mReadStream.Position = firstIndex * mDataItemSize;
        int size = count * mDataItemSize;
        Marshal.Copy(mBR.ReadBytes(size), 0, points, size);
      }
    }
    /// <summary>
    /// Frees used resources.
    /// </summary>
    public void Dispose()
    {
      lock (mWriteLock)
      {
        mBR.Dispose();
        mReadStream.Dispose();
        mBW.Dispose();
        mWriteStream.Dispose();
        mMMFile.Dispose();
      }
    }
    #endregion
  }
}