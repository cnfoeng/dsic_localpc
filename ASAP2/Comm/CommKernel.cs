﻿/*!
 * @file    
 * @brief   Implements the Communication kernel.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2.CCP;
using jnsoft.ASAP2.Properties;
using jnsoft.ASAP2.XCP;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace jnsoft.Comm
{
  /// <summary>
  /// Implements the Communication kernel.
  /// 
  /// - Steering of communication of several (XCP/CCP) Master's with their slaves
  /// - (XCP/CCP) Master instances get registered for communication with XCPKernel.RegisterClient
  /// - (XCP/CCP) Master instances created with ConnectBehaviourType.Automatic get connected and 
  /// kept alive (by XCPMaster.GetStatus calls) automatically .
  /// </summary>
  public static class CommKernel
  {
    #region types
    /// <summary>
    /// Used to store the a XCP command and its corresponding timeout.
    /// </summary>
    struct CommandOptions
    {
      /// <summary>
      /// The index into the timeout list.
      /// </summary>
      internal int TimeoutIndex;
      /// <summary>
      /// true, if command is optional.
      /// </summary>
      internal bool Optional;
      internal CommandOptions(int timeoutIndex, bool optional)
      { TimeoutIndex = timeoutIndex; Optional = optional; }
    }
    #endregion
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(CommKernel));
    static readonly Dictionary<XCP.CommandCode, CommandOptions> mOptionsMap;
    static readonly SortedList<CommMaster, Communicator> mClients = new SortedList<CommMaster, Communicator>();
    static readonly ManualResetEvent mCancelEvent = new ManualResetEvent(false);
    #endregion
    #region constructor
    /// <summary>
    /// Type initializer.
    /// 
    /// Creates the background ping thread.
    /// </summary>
    static CommKernel()
    {
      // map commands to timeouts (see spec)
      mOptionsMap = new Dictionary<XCP.CommandCode, CommandOptions>()
      {
          {XCP.CommandCode.Connect, new CommandOptions(0, false)}
        , {XCP.CommandCode.Disconnect, new CommandOptions(0, false)}
        , {XCP.CommandCode.GetStatus, new CommandOptions(0, false)}
        , {XCP.CommandCode.Synch, new CommandOptions(0, false)}
        , {XCP.CommandCode.GetCommModeInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetId, new CommandOptions(0, true)}
        , {XCP.CommandCode.SetRequest, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetSeed, new CommandOptions(0, true)}
        , {XCP.CommandCode.Unlock, new CommandOptions(0, true)}
        , {XCP.CommandCode.SetMTA, new CommandOptions(0, true)}
        , {XCP.CommandCode.Upload, new CommandOptions(0, true)}
        , {XCP.CommandCode.ShortUpload, new CommandOptions(0, true)}
        , {XCP.CommandCode.BuildChecksum, new CommandOptions(1, true)}
        , {XCP.CommandCode.TransportLayerCmd, new CommandOptions(0, true)}
        , {XCP.CommandCode.UserCmd, new CommandOptions(0, true)}
        , {XCP.CommandCode.Download, new CommandOptions(0, false)}
        , {XCP.CommandCode.DownloadNext, new CommandOptions(0, true)}
        , {XCP.CommandCode.DownloadMax, new CommandOptions(0, true)}
        , {XCP.CommandCode.ShortDownload, new CommandOptions(0, true)}
        , {XCP.CommandCode.ModifyBits, new CommandOptions(0, true)}
        , {XCP.CommandCode.SetCalPage, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetCalPage, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetPAGProcessorInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetSegmentInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetPageInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.SetSegmentMode, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetSegmentMode, new CommandOptions(0, true)}
        , {XCP.CommandCode.CopyCALPage, new CommandOptions(0, true)}
        , {XCP.CommandCode.SetDAQPtr, new CommandOptions(0, false)}
        , {XCP.CommandCode.WriteDAQ, new CommandOptions(0, false)}
        , {XCP.CommandCode.SetDAQListMode, new CommandOptions(0, false)}
        , {XCP.CommandCode.StartStopDAQList, new CommandOptions(0, false)}
        , {XCP.CommandCode.StartStopSynch, new CommandOptions(0, false)}
        , {XCP.CommandCode.ClearDaqList, new CommandOptions(0, false)}
        , {XCP.CommandCode.GetDAQListInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.WriteDAQMultiple, new CommandOptions(0, true)}
        , {XCP.CommandCode.ReadDAQ, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetDAQClock, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetDAQProcessorInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetDAQResolutionInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetDAQListMode, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetDAQEventInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.DtoCtrPproperties, new CommandOptions(0, true)}
        , {XCP.CommandCode.FreeDAQ, new CommandOptions(0, false)}
        , {XCP.CommandCode.AllocDAQ, new CommandOptions(0, false)}
        , {XCP.CommandCode.AllocODT, new CommandOptions(0, false)}
        , {XCP.CommandCode.AllocODTEntry, new CommandOptions(0, false)}
        , {XCP.CommandCode.ProgramStart, new CommandOptions(2, false)}
        , {XCP.CommandCode.ProgramClear, new CommandOptions(3, false)}
        , {XCP.CommandCode.Program, new CommandOptions(4, false)}
        , {XCP.CommandCode.ProgramReset, new CommandOptions(4, false)}
        , {XCP.CommandCode.GetPGMProcessorInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.GetSectorInfo, new CommandOptions(0, true)}
        , {XCP.CommandCode.ProgramPrepare, new CommandOptions(2, true)}
        , {XCP.CommandCode.ProgramFormat, new CommandOptions(0, true)}
        , {XCP.CommandCode.ProgramNext, new CommandOptions(4, true)}
        , {XCP.CommandCode.ProgramMax, new CommandOptions(4, true)}
        , {XCP.CommandCode.ProgramVerify, new CommandOptions(2, true)}
        , {XCP.CommandCode.TimeCorrelationProperties, new CommandOptions(0, true)}
      };

      // create and start the one and only xcp thread
      ThreadPool.QueueUserWorkItem(kernelThread);
    }
    #endregion
    #region methods
    /// <summary>
    /// Registers a new XCPMaster for XCPonEth communication.
    /// </summary>
    /// <param name="xcpMaster">The instance to register for communication</param>
    /// <exception cref="NotSupportedException">Thrown if called with types other than ethernet (UDP/TCP)</exception>
    public static void RegisterClient(XCP.XCPMaster xcpMaster)
    {
      lock (mClients)
      {
        if (mClients.ContainsKey(xcpMaster))
        { // already registered
          mLogger.Warn($"{xcpMaster.SlaveAddress} is already registered as a master");
          return;
        }

        mLogger.InfoFormat($"New XCP({xcpMaster.Type}) master to {xcpMaster.SlaveAddress}");
        XCP.XCPCommunicator state = null;
        var target = new IPEndPoint(xcpMaster.Address, xcpMaster.Port);
        switch (xcpMaster.Type)
        {
          case XCP.XCPType.UDP: state = new XCP.XCPonUDP(xcpMaster, target); break;
          case XCP.XCPType.TCP: state = new XCP.XCPonTCP(xcpMaster, target); break;
          default: throw new NotSupportedException($"{xcpMaster.Type} not supported in this constructor");
        }
        mClients[xcpMaster] = state;
      }
    }
    /// <summary>
    /// Registers a new XCPMaster for XCPonCAN communication.
    /// </summary>
    /// <param name="xcpMaster">The instance to register for communication</param>
    /// <param name="canProvider">The CAN provider to use</param>
    /// <param name="xcpCAN">The A2L XCPonCAN definition</param>
    /// <exception cref="NotSupportedException">Thrown if called with types other than CAN</exception>
    public static void RegisterClient(XCP.XCPMaster xcpMaster, CAN.ICANProvider canProvider, A2LXCP_ON_CAN xcpCAN)
    {
      lock (mClients)
      {
        if (mClients.ContainsKey(xcpMaster))
        { // already registered
          mLogger.Warn($"{xcpMaster.SlaveAddress} is already registered as a master");
          return;
        }
        mLogger.Info($"New XCP({xcpMaster.Type}) master to {xcpMaster.SlaveAddress}");
        mClients[xcpMaster] = new XCP.XCPOnCAN(xcpMaster, canProvider, xcpCAN);
      }
    }
    /// <summary>
    /// Registers a new CCPMaster for CCP communication.
    /// </summary>
    /// <param name="ccpMaster">The instance to register for communication</param>
    /// <param name="canProvider">The CAN provider to use</param>
    /// <param name="ccpIf">The A2L ASAP1B CCP interface</param>
    /// <exception cref="NotSupportedException">Thrown if called with types other than CAN</exception>
    public static void RegisterClient(CCP.CCPMaster ccpMaster, CAN.ICANProvider canProvider, A2LCCP_TP_BLOB ccpIf)
    {
      lock (mClients)
      {
        if (mClients.ContainsKey(ccpMaster))
        { // already registered
          mLogger.Warn($"{ccpMaster.SlaveAddress} is already registered as a master");
          return;
        }
        mLogger.Info($"New CCP master to {ccpMaster.SlaveAddress}");
        mClients[ccpMaster] = new CCP.CCPonCAN(ccpMaster, canProvider, ccpIf);
      }
    }
    /// <summary>
    /// Deregisters a Master from communication.
    /// 
    /// If the master is connected, a disconnect will be sent before closing communication.
    /// </summary>
    /// <param name="master">The instance to remove from communication</param>
    public static void DeregisterClient(CommMaster master)
    {
      lock (mClients)
      { // synchronize the dictionary
        Communicator state;
        if (!mClients.TryGetValue(master, out state))
          return;
        if (master.Connected)
        { // client connected

          // disconnect client
          master.Disconnect();
        }
        state.Dispose();
        mClients.Remove(master);
      }
    }
    /// <summary>
    /// Gets a value indicating if the specified master 
    /// is currently registered for communication.
    /// </summary>
    /// <param name="commMaster">The XCP/CCP master instance</param>
    /// <returns>true if the specified master is currently registered for communication.</returns>
    public static bool IsRegistered(CommMaster commMaster)
    {
      return mClients.ContainsKey(commMaster);
    }
    /// <summary>
    /// The one and only always active ping thread.
    /// 
    /// Iterates through the dictionary with host addresses to ping,
    /// pings them and sets its connectionstate.
    /// </summary>
    /// <param name="threadState">unused</param>
    static void kernelThread(object threadState)
    {
      int currIndex = 0;
      while (true)
      {
        if (mCancelEvent.WaitOne(100))
          break;
        if (!Monitor.TryEnter(mClients))
          continue;

        // Get the next master
        CommMaster master = null;
        Communicator state = null;
        try
        {
          if (currIndex >= mClients.Count)
          { // index on end, restart
            // let others work
            currIndex = 0;
            continue;
          }
          else
          { // master found
            master = mClients.Keys[currIndex];
            state = mClients[master];
          }
        }
        finally
        { // force to unlock master
          Monitor.Exit(mClients);
        }

        try
        {
          master.onIdle();
          DateTime lastReceived = master.LastReceivedTime;
          if (master.ConnectBehaviour != ConnectBehaviourType.Automatic
            || !master.ReadyToSend
            || lastReceived != DateTime.MinValue
            && DateTime.Now.Subtract(lastReceived).TotalMilliseconds < master.getAliveCycleTime()
            )
          { // not signaled (already waiting for response)
            ++currIndex;
            continue;
          }

          if (!master.SlaveConnected)
          {
            state.reset();
            int maxDTO;
            if (master.internalConnect(XCP.ConnectMode.Normal, out maxDTO))
              state.setMaxDTO(maxDTO);
          }
          else
          { // do an alive packet
            master.internalGetStatus();
          }
        }
#if DEBUG
        catch (Exception ex)
        {
          mLogger.Warn("caught exception...", ex);
        }
#else
		    catch
		    {
		    }
#endif
        ++currIndex;
      }
    }
    /// <summary>
    /// Send a XCP frame and maybe wait for a response.
    /// </summary>
    /// <typeparam name="T">The response type</typeparam>
    /// <param name="xcpMaster">The XCP master</param>
    /// <param name="data">The data to send</param>
    /// <param name="response">The received response (only valid if noResponseExpected is true)</param>
    /// <param name="useTimeout">
    /// - -1 - for standard timeout and wait for response
    /// - 0 - don't wait for any response (used for Master Block Mode)
    /// - greater than 0 - timeout in [ms] to wait for an response
    /// </param>
    /// <returns>The result of the send operation</returns>
    internal static XCP.CmdResult sendFrame<T>(XCP.XCPMasterBase xcpMaster
      , byte[] data
      , out T response
      , int useTimeout = -1
      ) where T : XCP.RespBase, new()
    {
      byte[] dummy;
      return sendFrame(xcpMaster, data, out response, out dummy, useTimeout);
    }
    /// <summary>
    /// Send a xcp frame and maybe wait for a response.
    /// </summary>
    /// <typeparam name="T">The response type</typeparam>
    /// <param name="xcpMaster">The XCP master</param>
    /// <param name="data">The data to send</param>
    /// <param name="response">The received response (only valid if noResponseExpected is true)</param>
    /// <param name="additionalData">available additional data or null (only valid if noResponseExpected is true)</param>
    /// <param name="useTimeout">
    /// - -1 - for standard timeout and wait for response
    /// - 0 - don't wait for any response (used for Master Block Mode)
    /// - greater than 0 - timeout in [ms] to wait for an response
    /// </param>
    /// <param name="cntBytesToWait">
    /// Count of bytes to wait for; greater than 0 if Slave Block Mode (for upload) is used.
    /// </param>
    /// <returns>The result of the send operation</returns>
    internal static XCP.CmdResult sendFrame<T>(XCP.XCPMasterBase xcpMaster
      , byte[] data
      , out T response
      , out byte[] additionalData
      , int useTimeout = -1
      , byte cntBytesToWait = 0
      ) where T : XCP.RespBase, new()
    {
      lock (mClients)
      {
        response = null;
        additionalData = null;
        xcpMaster.LastResponse = null;

        if (mClients.Count == 0)
          return XCP.CmdResult.ERR_PROTOCOL_FAILURE;

        var state = (XCP.XCPCommunicator)mClients[xcpMaster];

        //if (!xcpMaster.SlaveConnected && typeof(T) != typeof(RespConnect))
        //  // if already disconnected return the error
        //  return CmdResult.ERR_TIMEOUT;

        xcpMaster.ReadyToSend = false;
        try
        { // send the frame and wait for response

          var sendFrame = new XCP.XCPFrame(xcpMaster.Type, state.SourceAddress, ref data, true);

          // wait for response signal
          CommandOptions cmdOptions;
          XCP.CommandCode cmdCode = (XCP.CommandCode)sendFrame.Data[XCP.XCPFrame.CmdIndex];
          mOptionsMap.TryGetValue(cmdCode, out cmdOptions);
          int timeout = useTimeout < 0
            ? xcpMaster.Timings[cmdOptions.TimeoutIndex]
            : useTimeout;

          // reset signal to wait for
          state.WaitForResponse.Reset();

          // send request
          int sendLen = state.send(data);

          if (data.Length != sendLen)
          { // send failed
            setDisconnect(xcpMaster);
            return XCP.CmdResult.ERR_TIMEOUT;
          }

          // send succesful, keep the sent frame
          XCP.XCPFrameQueue.Instance.enqueueFrame(sendFrame);
          // increase the XCP masters counter
          xcpMaster.increaseCtr(sendFrame.getRawFrameLength());

          List<byte> frameBytes = new List<byte>();

          ResponsePending:
          state.RespPending = false;

          if (useTimeout >= 0)
          { // successfully sent and no response expected
            // wait the defined timeout and return success.
            if (useTimeout > 0)
              state.WaitForResponse.WaitOne(useTimeout);
            return XCP.CmdResult.OK;
          }

          WaitForMore:
          // Wait for response
          if (!state.WaitForResponse.WaitOne(timeout))
          { // timeout
            if (xcpMaster.SlaveConnected)
            { // master is connected
              setDisconnect(xcpMaster);
              mLogger.Warn($"Timeout, disconnected from {xcpMaster.SlaveAddress}");
            }
            return XCP.CmdResult.ERR_TIMEOUT;
          }

          if (state.RespPending)
            // rewait, if slave is pending
            goto ResponsePending;

          // signalled, get the data
          var recFrame = xcpMaster.LastResponse;
          if (recFrame != null)
          {
            switch (recFrame.Data[XCP.XCPFrame.ResponseIndex])
            {
              case (byte)XCP.PIDSlaveMaster.ERR:
                // error response
                var errCode = (XCP.CmdResult)recFrame.Data[1];
                state.WaitForResponse.Reset();
                state.ResponseProcessed.Set();
                xcpMaster.onErrorReceived(errCode, cmdCode);
                return errCode;

              case (byte)XCP.PIDSlaveMaster.RES:
                var currIdx = frameBytes.Count;
                frameBytes.AddRange(recFrame.Data);
                state.WaitForResponse.Reset();
                state.ResponseProcessed.Set();

                if (currIdx > 0)
                  // delete the FF byte from subsequent received upload responses
                  frameBytes.RemoveAt(currIdx);

                if (frameBytes.Count < cntBytesToWait)
                  // awaiting more frames (slave block mode)
                  goto WaitForMore;

                if (cntBytesToWait > 0 && frameBytes.Count > cntBytesToWait)
                  // remove DLC fill bytes in slave block mode
                  frameBytes.RemoveRange(cntBytesToWait, frameBytes.Count - cntBytesToWait);

                // response is complete, valid response?
                int expectedSize = Marshal.SizeOf(typeof(T));
                if (expectedSize <= frameBytes.Count)
                { // Anything fine with received data
                  Extensions.convertToReference(frameBytes, out response, out additionalData);
                  if (xcpMaster.ChangeEndianess)
                    // Byte order differs, adjust byte order
                    response.changeEndianness();
                  xcpMaster.onValidResponseReceived(cmdCode, response);
                  return XCP.CmdResult.OK;
                }
                // Usually this is a protocol error (the answer is too short).
                // expexted data len is > received data
                // create default response data.
                response = new T();
                return XCP.CmdResult.ERR_PROTOCOL_FAILURE;
            }
          }
        }
        catch (SocketException) { }
        catch (ObjectDisposedException) { }
        catch (Exception ex)
        { // some exception catch all...
          mLogger.Fatal("sendFrame (XCP) caught exception", ex);
        }
        finally
        { // force always to be ready to send the next frame
          xcpMaster.ReadyToSend = true;
        }
        setDisconnect(xcpMaster);
        return XCP.CmdResult.ERR_TIMEOUT;
      }
    }
    /// <summary>
    /// Send a CCP frame and wait for a response.
    /// </summary>
    /// <typeparam name="T">The response type</typeparam>
    /// <param name="ccpMaster">The CCP master</param>
    /// <param name="data">The data to send</param>
    /// <param name="response">The received response</param>
    /// <returns>The result of the send operation</returns>
    internal static CCP.CmdResult sendFrame<T>(CCP.CCPMasterBase ccpMaster
      , byte[] data
      , out T response
      ) where T : CCP.RespBase, new()
    {
      byte[] dummy;
      return sendFrame(ccpMaster, data, out response, out dummy);
    }
    /// <summary>
    /// Gets the standard CCP timeout modified with the variable CCPTimeoutFactor.
    /// </summary>
    /// <param name="cmd">CCP command code</param>
    /// <returns>timeout in [ms]</returns>
    static int getCCPTimeout(CCP.CommandCode cmd)
    {
      switch (cmd)
      {
        case CCP.CommandCode.BuildChksum:
        case CCP.CommandCode.Move:
        case CCP.CommandCode.ClearMemory: return 30000;
        case CCP.CommandCode.ActionService: return 5000;
        case CCP.CommandCode.DiagService: return 500;
        case CCP.CommandCode.Program:
        case CCP.CommandCode.Program6: return 100;
        default: return 25 * Settings.Default.CCPTimeoutFact;
      }
    }
    /// <summary>
    /// Send a CCP frame and wait for a response.
    /// </summary>
    /// <typeparam name="T">The response type</typeparam>
    /// <param name="ccpMaster">The CCP master</param>
    /// <param name="data">The data to send</param>
    /// <param name="response">The received response</param>
    /// <param name="additionalData">available additional data or null</param>
    /// <returns>The result of the send operation</returns>
    internal static CCP.CmdResult sendFrame<T>(CCP.CCPMasterBase ccpMaster
      , byte[] data
      , out T response
      , out byte[] additionalData
      ) where T : CCP.RespBase, new()
    {
      lock (mClients)
      {
        response = null;
        additionalData = null;
        ccpMaster.LastResponse = null;

        if (mClients.Count == 0)
          return CCP.CmdResult.ProtocolFailure;

        var state = (CCP.CCPonCAN)mClients[ccpMaster];

        //if (!xcpMaster.SlaveConnected && typeof(T) != typeof(RespConnect))
        //  // if already disconnected return the error
        //  return CmdResult.ERR_TIMEOUT;

        ccpMaster.ReadyToSend = false;
        try
        { // send the frame and wait for response

          data[1] = ccpMaster.Ctr++;

          // preserve the CCP 8 byte in length command
          byte[] sendData = new byte[8];
          Array.Copy(data, sendData, data.Length);
          data = sendData;

          var sendFrame = new CCP.CCPFrame(ref state.SourceAddress, state.CmdID, ref data, true);

          // send request
          int sendLen = state.send(data);
          // reset signal to wait for
          state.WaitForResponse.Reset();
          if (data.Length != sendLen)
          { // send failed
            setDisconnect(ccpMaster);
            return CCP.CmdResult.Timeout;
          }
          else
          { // send succesful, keep the sent frame
            CCP.CCPFrameQueue.Instance.enqueueFrame(sendFrame);
            // increase he XCP masters counter
            ccpMaster.increaseCtr(sendFrame.getRawFrameLength());
          }

          var cmd = (CCP.CommandCode)data[0];

          ResponsePending:
          state.RespPending = false;

          // Wait for response
          if (!state.WaitForResponse.WaitOne(getCCPTimeout(cmd)))
          { // timeout
            if (ccpMaster.SlaveConnected)
            { // master is connected
              mLogger.Warn($"Timeout, disconnected from {ccpMaster.SlaveAddress}");
              setDisconnect(ccpMaster);
            }
            return CCP.CmdResult.Timeout;
          }

          // signalled, get the data
          var recFrame = ccpMaster.LastResponse;

          if (recFrame != null)
          {
            switch (recFrame.Data[CCP.CCPFrame.ResponseIndex])
            {
              case (byte)CCP.PIDSlaveMaster.RES:
                var result = (CCP.CmdResult)recFrame.Data[CCP.CCPFrame.ResponseIndex + 1];
                if (CCP.CmdResult.OK != result)
                { // error code received
                  ccpMaster.onErrorReceived(result, cmd);
                  return result;
                }

                if (data[1] != recFrame.Data[2])
                { // rewait, if slave is pending
                  ccpMaster.LastResponse = null;
                  goto ResponsePending;
                }

                // valid response?
                if (data[1] == recFrame.Data[2])
                { // CTR ok
                  int expectedSize = Marshal.SizeOf(typeof(T));
                  if (expectedSize <= recFrame.Data.Length)
                  { // Anything fine with received data
                    CCP.CCPFrame.convertToReference<T>(recFrame.Data, out response, out additionalData);
                    if (ccpMaster.ChangeEndianess)
                      // Byte order differs, adjust byte order
                      response.changeEndianness();
                    ccpMaster.onValidResponseReceived(cmd, response);
                    return result;
                  }
                  // Usually this is a protocol error (the answer is too short).
                  // expexted data len is > received data
                  // create default response data.
                  response = new T();
                  return CCP.CmdResult.ProtocolFailure;
                }
                break;
            }
          }
        }
        catch (SocketException) { }
        catch (ObjectDisposedException) { }
        catch (Exception ex)
        { // some exception catch all...
          mLogger.Fatal("caught exception", ex);
        }
        finally
        { // force always to be ready to send the next frame
          ccpMaster.ReadyToSend = true;
        }
        setDisconnect(ccpMaster);
        return CCP.CmdResult.Timeout;
      }
    }
    internal static void setDisconnect(CommMaster master)
    {
      master.setConnectionState<ResponseBase>(null);
    }
    #endregion
  }
}
