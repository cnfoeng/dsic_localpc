﻿/*!
 * @file    
 * @brief   Implements the XCP Master to communicate with a XCP Slave.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2;
using jnsoft.ASAP2.Properties;
using jnsoft.ASAP2.XCP;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Implements the XCP Master to communicate with a XCP Slave.
  /// 
  /// Derived from XCPMasterBase, this is the class providing some 
  /// convenience funtions for a XCP communication.
  /// 
  /// - All commands are implemented as synchronous calls to the slave
  /// - All commands are thread-safe
  /// - The endianess of any command, response or event is converted 
  /// automatically according the XCP protocol
  /// - Responses (out parameter) of commands are only valid if the 
  /// CmdResult of the call is CmdResult.OK
  /// </summary>
  /// <example>Example to connect to a XCP slave device:
  /// <code>
  /// using System;
  /// using System.Text;
  /// using System.Threading;
  /// using jnsoft.ASAP2;
  /// using jnsoft.XCP;
  /// 
  /// class Program
  /// {
  ///   static readonly string fTime = "HH:mm:ss,fff";
  ///   static void Main(string[] args)
  ///   {
  ///     using (var a2lParser = new A2LParser())
  ///     {
  ///       a2lParser.parse(args[0]);
  ///   
  ///       var modPar = a2lParser.Project.getNode<A2LMODULE>(false).getNode<A2LMODPAR>(false);
  ///       var a2lEPK = modPar.EPK.Trim(new char[] { '\0', ' ' });
  ///   
  ///       ConsoleColor prevColor = Console.ForegroundColor;
  ///       try
  ///       {
  ///         // Create a XCP communication master to connect to a slave
  ///         // using XCP over TCP to slave address 127.0.0.1:1800
  ///         var timings = new UInt16[] { 100, 100, 100, 0, 0, 0 };
  ///         using (var xcpMaster = new XCPMaster(ConnectBehaviourType.Manual
  ///           , XCPType.TCP
  ///           , "127.0.0.1", 1800
  ///           , timings
  ///           , onConnectionStateChanged, onXCPEventReceived, onXCPServiceRequestReceived, onXCPErrorReceived
  ///           ))
  ///         { // try to connect to ECU
  ///           Console.WriteLine("{0}: Connecting...EPK should be {1}... (press any key to quit)"
  ///             , DateTime.Now.ToString(fTime)
  ///             , a2lEPK);
  ///           RespConnect respConnect;
  ///           CmdResult result;
  ///           while (CmdResult.OK != (result = xcpMaster.Connect(ConnectMode.Normal, out respConnect)))
  ///           { // wait until connected
  ///             Thread.Sleep(1000);
  ///             if (Console.KeyAvailable)
  ///               // exit process after keypress
  ///               return;
  ///           }
  ///     
  ///           // connect sucessful -> query GetStatus
  ///           RespGetStatus respState;
  ///           if (CmdResult.OK != (result = xcpMaster.GetStatus(out respState)))
  ///             // exit process after failed query
  ///             return;
  ///     
  ///           Console.ForegroundColor = ConsoleColor.Yellow;
  ///           Console.WriteLine("{0}: Session state={1}"
  ///             , DateTime.Now.ToString(fTime)
  ///             , respState.SessionState);
  ///       
  ///           // connect successful -> query EPK on Address 0, length 30
  ///           byte[] respData;
  ///           if (CmdResult.OK == xcpMaster.ShortUpload(30, 0, 0, out respData))
  ///           { // upload successful
  ///             var deviceEPK = Encoding.Default.GetString(respData).Trim(new char[] { '\0', ' ', '\xff' });
  ///             Console.WriteLine("{0}: EPK from device is: '{1}'"
  ///               , DateTime.Now.ToString(fTime)
  ///               , deviceEPK);
  ///           }
  ///         }
  ///       }
  ///     }
  ///     finally
  ///     { // reset starting color
  ///       Console.ForegroundColor = prevColor;
  ///     }
  ///   }
  ///   // Callback receiving XCP service requests
  ///   static void onXCPServiceRequestReceived(object sender, XCPServiceRequestArgs e)
  ///   {
  ///     Console.ForegroundColor = ConsoleColor.White;
  ///     Console.WriteLine("{0}: Received service request: {1}"
  ///       , DateTime.Now.ToString(fTime)
  ///       , e.ReceivedServiceRequest.ServiceRequestCode);
  ///   }
  ///   // Callback receiving XCP events
  ///   static void onXCPEventReceived(object sender, XCPEventArgs e)
  ///   {
  ///     Console.ForegroundColor = ConsoleColor.Green;
  ///     Console.WriteLine("{0}: Received error: {1}"
  ///       , DateTime.Now.ToString(fTime)
  ///       , e.ReceivedEvent.EventCode);
  ///   }
  ///   // Callback receiving XCP errors
  ///   static void onXCPErrorReceived(object sender, XCPErrorArgs e)
  ///   {
  ///     Console.ForegroundColor = ConsoleColor.Red;
  ///     Console.WriteLine("{0}: Received error: {1}"
  ///       , DateTime.Now.ToString(fTime)
  ///       , e.ErrorCode);
  ///   }
  ///   // Callback receiving XCP connection state changes
  ///   static void onConnectionStateChanged(object sender, EventArgs e)
  ///   {
  ///     Console.ForegroundColor = ConsoleColor.Red;
  ///     XCPMaster master = (XCPMaster)sender;
  ///     Console.WriteLine("{0}: State: {1}"
  ///       , DateTime.Now.ToString(fTime)
  ///       , master.Connected ? "connected" : "disconnected"
  ///       );
  ///   }
  /// }
  /// </code></example>
  public class XCPMaster : XCPMasterBase
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(XCPMaster));
    /// <summary>
    /// Object used to synchronize read/write accesses on ECU.
    /// 
    /// Important to protect the MTA pointer on the slave.
    /// </summary>
    readonly object mReadWriteLock = new object();
    bool mConnected;
    volatile bool mIsDAQRunning;
    bool mConfigureMeasurements;
    UInt32 mDAQClock;
    int mDAQDataOffsetFirstPid, mDAQDataOffset;
    XCP_TIMESTAMP_SIZE mTimestampSize;
    /// <summary>
    /// Holds always the last valid responses.
    /// 
    /// Maps the commandcode as key to it's valid response as value.
    /// </summary>
    Dictionary<CommandCode, RespBase> mResponseDict = new Dictionary<CommandCode, RespBase>();
    /// <summary>The XCP Protocol layer from the A2L definition</summary>
    A2LXCP_PROTOCOL_LAYER mProtocolLayer;
    /// <summary>A2L XCP DAQ definition</summary>
    A2LXCP_DAQ mA2LDAQ;
    /// <summary>A2L XCP Timestamp definition (may be null)</summary>
    A2LXCP_TIMESTAMP_SUPPORTED mA2LTS;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a XCP on Ethernet communication master.
    /// </summary>
    /// <param name="connectBehavior">Client's connect behaviour</param>
    /// <param name="type">Client's type</param>
    /// <param name="remoteAddress">The target address</param>
    /// <param name="remotePort">The target port</param>
    /// <param name="protocolLayer">XCP ProtocolLayer (may be null)</param>
    /// <param name="daq">DAQ List definition (may be null)</param>
    /// <param name="onConnectionStateChanged">The callback to receive connection state changes (may be null)</param>
    /// <param name="onXCPEventReceived">The callback to receive XCP events (may be null)</param>
    /// <param name="onXCPServiceRequestReceived">The callback to receive XCP service requests (may be null)</param>
    /// <param name="onXCPErrorReceived">The callback to receive XCP command errors (may be null)</param>
    /// <exception cref="ArgumentException">Thrown, if the remoteAddress is not resolvable</exception>
    public XCPMaster(ConnectBehaviourType connectBehavior
      , XCPType type
      , string remoteAddress
      , int remotePort
      , A2LXCP_PROTOCOL_LAYER protocolLayer
      , A2LXCP_DAQ daq
      , EventHandler onConnectionStateChanged
      , EventHandler<XCPEventArgs> onXCPEventReceived
      , EventHandler<XCPServiceRequestArgs> onXCPServiceRequestReceived
      , EventHandler<XCPErrorArgs> onXCPErrorReceived
      )
      : base(connectBehavior, type, remoteAddress, remotePort, protocolLayer)
    {
      DAQs = new DAQDictXCP();
      mProtocolLayer = protocolLayer;
      mA2LDAQ = daq;
      mA2LTS = mA2LDAQ.getNode<A2LXCP_TIMESTAMP_SUPPORTED>(false);

      initialize();

      // add the handlers
      if (null != onConnectionStateChanged)
        ConnectionStateChanged += onConnectionStateChanged;
      if (null != onXCPEventReceived)
        XCPEventReceived += onXCPEventReceived;
      if (null != onXCPServiceRequestReceived)
        XCPServiceRequestReceived += onXCPServiceRequestReceived;
      if (null != onXCPErrorReceived)
        XCPErrorReceived += onXCPErrorReceived;
      CommKernel.RegisterClient(this);
    }
    /// <summary>
    /// Creates a XCP on CAN communication master.
    /// </summary>
    /// <param name="connectBahviour">Client's connect behaviour</param>
    /// <param name="type">Client's type</param>
    /// <param name="canProvider">The CAN provider to use</param>
    /// <param name="xcpCAN">XCPonCAN definition</param>
    /// <param name="protocolLayer">XCP ProtocolLayer (may be null)</param>
    /// <param name="daq">DAQ List definition (may be null)</param>
    /// <param name="onConnectionStateChanged">The callback to receive connection state changes (may be null)</param>
    /// <param name="onXCPEventReceived">The callback to receive XCP events (may be null)</param>
    /// <param name="onXCPServiceRequestReceived">The callback to receive XCP service requests (may be null)</param>
    /// <param name="onXCPErrorReceived">The callback to receive XCP command errors (may be null)</param>
    /// <exception cref="ArgumentException">Thrown, if the remote address is not resolvable</exception>
    public XCPMaster(ConnectBehaviourType connectBahviour
      , XCPType type
      , ICANProvider canProvider
      , A2LXCP_ON_CAN xcpCAN
      , A2LXCP_PROTOCOL_LAYER protocolLayer
      , A2LXCP_DAQ daq
      , EventHandler onConnectionStateChanged
      , EventHandler<XCPEventArgs> onXCPEventReceived
      , EventHandler<XCPServiceRequestArgs> onXCPServiceRequestReceived
      , EventHandler<XCPErrorArgs> onXCPErrorReceived
      )
      : base(connectBahviour, xcpCAN, protocolLayer, canProvider.ToString())
    {
      DAQs = new DAQDictXCP();
      mProtocolLayer = protocolLayer;
      mA2LDAQ = daq;

      initialize();

      // add the handlers
      if (null != onConnectionStateChanged)
        ConnectionStateChanged += onConnectionStateChanged;
      if (null != onXCPEventReceived)
        XCPEventReceived += onXCPEventReceived;
      if (null != onXCPServiceRequestReceived)
        XCPServiceRequestReceived += onXCPServiceRequestReceived;
      if (null != onXCPErrorReceived)
        XCPErrorReceived += onXCPErrorReceived;
      CommKernel.RegisterClient(this, canProvider, xcpCAN);
    }
    #endregion
    #region events
    /// <summary>
    /// Called if a XCP event is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<XCPEventArgs> XCPEventReceived;
    /// <summary>
    /// Called if a XCP service request is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<XCPServiceRequestArgs> XCPServiceRequestReceived;
    /// <summary>
    /// Called if a XCP command error is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<XCPErrorArgs> XCPErrorReceived;
    #endregion
    #region properties
    public override bool Connected { get { return mConnected; } }
    /// <summary>Indicates if data acquisition is running.</summary>
    public bool IsDAQRunning { get { return mIsDAQRunning; } }
    /// <summary>
    /// The last received Connect response.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public RespConnect ConnectResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.Connect, out response))
            return (RespConnect)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetStatus response.
    /// 
    /// Only valid in connected state and marked as optional in the A2L!
    /// </summary>
    public RespGetStatus StatusResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetStatus, out response))
            return (RespGetStatus)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received CommModeInfo response.
    /// 
    /// Only valid in connected state and marked as optional in the A2L!
    /// </summary>
    public RespGetCommModeInfo CommModeInfoResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetCommModeInfo, out response))
            return (RespGetCommModeInfo)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetPagProcessorInfo response.
    /// 
    /// Only valid in connected state and CAL_PAG is supported!
    /// </summary>
    public RespGetPAGProcessorInfo PAGProcInfo
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetPAGProcessorInfo, out response))
            return (RespGetPAGProcessorInfo)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetDAQProcessorInfo response.
    /// 
    /// Only valid in connected state and DAQ is supported!
    /// </summary>
    public RespGetDAQProcessorInfo DAQProcInfo
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetDAQProcessorInfo, out response))
            return (RespGetDAQProcessorInfo)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetDAQResolutionInfo response.
    /// 
    /// Only valid in connected state and marked as optional in the A2L and DAQ is supported!
    /// </summary>
    public RespGetDAQResolutionInfo DAQResolutionInfo
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetDAQResolutionInfo, out response))
            return (RespGetDAQResolutionInfo)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetDAQProcessorInfo response.
    /// 
    /// Only valid in connected state and DAQ with Timestamp is supported!
    /// </summary>
    public UInt32 DAQClock { get { return mDAQClock; } }
    /// <summary>
    /// The last received GetDAQProcessorInfo response.
    /// 
    /// Only valid in connected state and PGM is supported!
    /// </summary>
    public RespGetPGMProcessorInfo PGMProcInfo
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetPGMProcessorInfo, out response))
            return (RespGetPGMProcessorInfo)response;
          return null;
        }
      }
    }
    public override ECUPage ActivePage
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (!mResponseDict.TryGetValue(CommandCode.GetCalPage, out response))
            return ECUPage.NotSet;
          return (ECUPage)((RespGetCALPage)response).PageNo;
        }
      }
    }
    /// <summary>
    /// Gets the byte order of the slave device.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public BYTEORDER_TYPE ByteOrder
    {
      get
      {
        return !mConnected
          ? BYTEORDER_TYPE.NotSet
          : (ConnectResponse.CommModeBasic & CommModeBasic.BigEndian) > 0
            ? BYTEORDER_TYPE.BIG_ENDIAN
            : BYTEORDER_TYPE.LITTLE_ENDIAN;
      }
    }
    /// <summary>
    /// Gets the XCP version string.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public string Version
    {
      get
      {
        RespConnect resp = ConnectResponse;
        return !mConnected
          ? string.Empty
          : $"{(resp.Version >> 8)}.{(resp.Version & 0xff)}";
      }
    }
    /// <summary>
    /// Gets the address granularity as a string.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public string StrAddressGranularity
    {
      get
      {
        RespConnect resp = ConnectResponse;
        return !mConnected
          ? string.Empty
          : (resp.CommModeBasic & CommModeBasic.AddressGranularityDWORD) > 0
            ? "DWORD"
            : (resp.CommModeBasic & CommModeBasic.AddressGranularityWORD) > 0
              ? "WORD"
              : "BYTE";
      }
    }
    public override bool CanWrite
    {
      get
      {
        return mConnected
          && (ConnectResponse.Resource & ResourceType.CAL_PAG) > 0;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Initialize members.
    /// 
    /// - Sets all available DAQ Lists to be usable.
    /// - Sets timestamp size and data offsets.
    /// </summary>
    void initialize()
    {
      mDAQDataOffsetFirstPid = mDAQDataOffset = mA2LDAQ.getIDFieldSize();
      mTimestampSize = XCP_TIMESTAMP_SIZE.NotSet;
      if (!mProtocolLayer.OptionalCmds.Contains("GET_DAQ_CLOCK") || mA2LTS == null)
        return;
      mTimestampSize = mA2LTS.Size;
      if (mTimestampSize == XCP_TIMESTAMP_SIZE.NotSet)
        return;
      // timestamp is supported
      mDAQDataOffsetFirstPid += (int)mTimestampSize;
      if (mA2LTS.IsFixed)
        // timestamp is sent in all ODTs
        mDAQDataOffset = mDAQDataOffsetFirstPid;
    }
    /// <summary>Indicates if Master Block Mode is supported.</summary>
    /// <param name="maxCTO">Max CTO</param>
    /// <returns>Master Block Mode is supported</returns>
    bool isMasterBlockMode(byte maxCTO)
    {
      return !Settings.Default.XCPPreventBlockMode
        && maxCTO < byte.MaxValue
        && (mProtocolLayer?.CommModesSupported.CommMode & XCP_BLOCK_MODE.MASTER) > 0;
    }
    /// <summary>Indicates if Slave Block Mode is supported.</summary>
    /// <returns>Slave Block Mode is supported</returns>
    public bool isSlaveBlockMode()
    {
      return !Settings.Default.XCPPreventBlockMode
        && MaxCTO < byte.MaxValue
        && (mProtocolLayer?.CommModesSupported.CommMode & XCP_BLOCK_MODE.SLAVE) > 0;
    }
    /// <summary>
    /// Returns true if the command is allowed to request.
    /// </summary>
    /// <param name="cmd">The XCP command in the same form as in the A2LDescription (e.g. "GET_ID")</param>
    /// <returns>true if the optional command is allowed to execute</returns>
    public bool isAllowedRequest(string cmd)
    {
      if (!Settings.Default.RespectOptionalCmds)
        return true;
      return mProtocolLayer.OptionalCmds.Contains(cmd);
    }
    public override void onEventReceived(XCPFrame xcpFrame)
    {
      base.onEventReceived(xcpFrame);
      EventBase xcpEvent;
      if (!Extensions.convertToReference(xcpFrame.Data, out xcpEvent))
      {
        mLogger.Error("Failed to convert to xcp event");
        return;
      }
      if (xcpEvent.EventCode == EventCodes.TimeSync)
      { // update DAQ clock from time sync event
        EventTimeSync evTimeSync;
        if (!Extensions.convertToReference(xcpFrame.Data, out evTimeSync))
          return;
        if (ChangeEndianess) evTimeSync.changeEndianness();
        mDAQClock = evTimeSync.Timestamp;
      }
      XCPEventReceived?.Invoke(this, new XCPEventArgs(xcpEvent));
    }
    public override void onErrorReceived(CmdResult result, CommandCode code)
    {
      base.onErrorReceived(result, code);
      XCPErrorReceived?.Invoke(this, new XCPErrorArgs(result, code));
    }
    public override void onServiceReceived(XCPFrame xcpFrame)
    {
      base.onServiceReceived(xcpFrame);
      RespService srvReq;
      if (!Extensions.convertToReference<RespService>(xcpFrame.Data, out srvReq))
      {
        mLogger.Error("Failed to convert to xcp service request");
        return;
      }
      XCPServiceRequestReceived?.Invoke(this, new XCPServiceRequestArgs(srvReq));
    }
    /// <summary>
    /// Disconnect from slave.
    /// 
    /// Brings the slave to the “DISCONNECTED”state. The DISCONNECTED”
    /// state is described in Part 1, chapter “state machine”.
    /// </summary>
    /// <returns>true if successful</returns>
    public override bool Disconnect()
    {
      if (mIsDAQRunning)
        // stop data acquisition
        stopMeasurements(true);
      bool result = base.Disconnect();
      lock (DAQs)
        DAQs.clearData();
      return result;
    }
    public override void Dispose()
    {
      XCPEventReceived = null;
      XCPServiceRequestReceived = null;
      XCPErrorReceived = null;
      base.Dispose();
      lock (DAQs)
        DAQs.Clear();
    }
    internal sealed override void onValidResponseReceived(CommandCode cmdCode, RespBase response)
    {
      base.onValidResponseReceived(cmdCode, response);
      switch (cmdCode)
      {
        case CommandCode.Connect:
        case CommandCode.GetStatus:
        case CommandCode.GetCommModeInfo:
        case CommandCode.GetCalPage:
        case CommandCode.GetPAGProcessorInfo:
        case CommandCode.GetDAQProcessorInfo:
        case CommandCode.GetDAQResolutionInfo:
        case CommandCode.GetPGMProcessorInfo:
          lock (mResponseDict)
            mResponseDict[cmdCode] = response;
          break;
      }
    }
    internal override void setConnectionState<T>(T respConnect)
    {
      var connect = respConnect as RespConnect;
      base.setConnectionState(connect);
      bool newConnected = connect != null;
      if (newConnected == mConnected)
        // no state change
        return;
      try
      {
        if (newConnected)
        { // connected
          // request the standard telegrams
          RespGetStatus status;
          if (CmdResult.ERR_TIMEOUT != GetStatus(out status))
          {
            if ((connect.CommModeBasic & CommModeBasic.Optional) > 0)
            { // request additional comm mode info if optional flag is set
              RespGetCommModeInfo commModeInfo;
              GetCommModeInfo(out commModeInfo);
            }

            // unlock required
            if (isAllowedRequest("GET_SEED") && status.ResourceProtectionState != ResourceType.None && mSeedAndKeyDLL != null)
              // try to unlock all resources
              unlockECU(mSeedAndKeyDLL);

            status = StatusResponse;
            if (isAllowedRequest("GET_CAL_PAGE") && (connect.Resource & ResourceType.CAL_PAG) > 0)
            { // calibration and pages available and unlocked
              RespGetCALPage respGetCALPage;
              if (CmdResult.OK == GetCalPage(CalPageMode.XCP, 0, out respGetCALPage)
                && isAllowedRequest("GET_PAG_PROCESSOR_INFO")
                )
              {
                RespGetPAGProcessorInfo respGetPagProcessorInfo;
                GetPAGProcessorInfo(out respGetPagProcessorInfo);
              }
            }

            if (isAllowedRequest("GET_PGM_PROCESSOR_INFO") && (connect.Resource & ResourceType.PGM) > 0)
            { // PGM available and unlocked
              RespGetPGMProcessorInfo respGetProgramProcessorInfo;
              GetPGMProcessorInfo(out respGetProgramProcessorInfo);
            }

            if ((connect.Resource & ResourceType.DAQ) > 0)
            { // DAQ available and unlocked
              if (isAllowedRequest("GET_DAQ_RESOLUTION_INFO"))
              {
                RespGetDAQResolutionInfo respGetDAQResolutionInfo;
                GetDAQResolutionInfo(out respGetDAQResolutionInfo);
              }

              if (isAllowedRequest("GET_DAQ_PROCESSOR_INFO"))
              {
                RespGetDAQProcessorInfo respGetDAQProcessorInfo;
                if (CmdResult.OK == GetDAQProcessorInfo(out respGetDAQProcessorInfo))
                {
                  var events = new Dictionary<UInt16, RespGetDAQEventInfo>();
                  if (isAllowedRequest("GET_DAQ_EVENT_INFO"))
                    for (UInt16 i = 0; i < respGetDAQProcessorInfo.MaxEventChannel; ++i)
                    {
                      RespGetDAQEventInfo respGetDAQEventInfo;
                      if (CmdResult.OK == GetDAQEventInfo(i, out respGetDAQEventInfo))
                        events[i] = respGetDAQEventInfo;
                    }

                  var lists = new Dictionary<UInt16, RespGetDAQListInfo>();
                  if (isAllowedRequest("GET_DAQ_LIST_INFO"))
                    for (UInt16 i = 0; i < respGetDAQProcessorInfo.MaxDAQ; ++i)
                    {
                      RespGetDAQListInfo respGetDAQListInfo;
                      if (CmdResult.OK == GetDAQListInfo(i, out respGetDAQListInfo))
                        lists[i] = respGetDAQListInfo;
                    }
                }
              }
            }
            if ((connect.Resource & ResourceType.STIM) > 0 && (status.ResourceProtectionState & ResourceType.STIM) == 0)
            { // STIM available and unlocked
            }
          }
          else
          { // timeout occured
            newConnected = false;
          }
        }
      }
      finally
      {
        bool raise = mConnected != newConnected;
        mConnected = newConnected;
        if (!mConnected)
        { // disconnected, remove all responses
          Disconnect();
          mDAQClock = 0;
          mIsDAQRunning = false;
          lock (mResponseDict)
            mResponseDict.Clear();
        }
        if (raise)
          // notify event
          RaiseConnectionStateChanged();
      }
    }
    #region extended xcp methods
    public override bool readSync(int len, byte addressExtension, UInt32 address
      , out byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      data = null;
      if (!SlaveConnected)
        return false;
      lock (mReadWriteLock)
      {
        try
        {
#if DEBUG
          mLogger.Info($"Uploading from 0x{address:X8}(0x{len:X8} bytes)");
#endif
          var maxFrameBytes = MaxCTO - 1;
          var result = CmdResult.OK;
          if (address != UInt32.MaxValue && len <= maxFrameBytes
            && isAllowedRequest("SHORT_UPLOAD")
            )
            // short upload if address is given and length fits into one response
            result = ShortUpload((byte)len, addressExtension, address, out data);
          else
          { // upload

            // Set MTA if a valid address is specified
            if (address != uint.MaxValue && CmdResult.OK != SetMTA(addressExtension, address))
              return false;

            bool isSlaveBM = isSlaveBlockMode();
            if (isSlaveBM)
              maxFrameBytes = byte.MaxValue - 1;

            // do the read loop
            int updateStep = Math.Max(1, len / 100);
            int currIndex = 0;
            data = new byte[len];
            while (currIndex < len)
            {
              var transLen = len - currIndex > maxFrameBytes
                ? maxFrameBytes
                : len - currIndex;
              byte[] transferData;

              if (CmdResult.OK != (result = Upload((byte)transLen, out transferData)))
                break;

              Array.Copy(transferData, 0, data, currIndex, transLen);
              currIndex += transLen;
              if (null != progressHandler && currIndex > updateStep)
              {
                var args = new ProgressArgs((int)(currIndex * 100.0 / (double)len));
                progressHandler(this, args);
                if (args.Cancel)
                  // cancelled
                  return false;
                updateStep += Math.Max(1, len / 100);
              }
            }
          }
          return result == CmdResult.OK;
        }
        catch { return false; }
        finally
        {
          progressHandler?.Invoke(this, new ProgressArgs(100));
#if DEBUG
          mLogger.Info("Upload successful");
#endif
        }
      }
    }

    /// <summary>
    /// Download or Program data synchronously to the ECU.
    /// 
    /// Depending on the data length to be written 
    /// - SetMTA/Download or ShortDownload is used.
    /// 
    /// ProgramMax/DownloadMax is used if these commands are allowed.
    /// 
    /// Depending on the Master Block Mode supported by the slave
    /// - SetMTA/Download/DownloadNext or SetMTA/Program/ProgamNext is used
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="psResponse">Response from ProgramStart command (if null, download mode is used)</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    bool writeSyncImpl(byte addressExtension, UInt32 address, byte[] data
      , RespProgramStart psResponse
      , EventHandler<ProgressArgs> progressHandler
      )
    {
      if (!SlaveConnected)
        return false;
      lock (mReadWriteLock)
      {
        var result = CmdResult.OK;
#if DEBUG
        mLogger.Info($"{(psResponse != null ? "Programming" : "Downloading")} to 0x{address:X8}(0x{data.Length:X8} bytes)");
#endif
        try
        {
          if (psResponse == null
            && address != UInt32.MaxValue
            && data.Length < MaxCTO - 8
            && isAllowedRequest("SHORT_DOWNLOAD")
            )
            // short download if address is given and length fits into one response
            result = ShortDownload(data, addressExtension, address);
          else
          { // download/program

            // Set MTA if a valid address is specified
            if (address != uint.MaxValue && CmdResult.OK != (result = SetMTA(addressExtension, address)))
              return false;

            // do the write loop
            var maxFrameBytes = (psResponse != null ? psResponse.MaxCTO : MaxCTO) - 2;
            var maxBS = maxFrameBytes;
            int stMin = psResponse != null
              ? psResponse.MinST
              : CommModeInfoResponse != null
                ? CommModeInfoResponse.MinST
                : 0;

            bool isMasterBM = isMasterBlockMode(psResponse == null ? MaxCTO : psResponse.MaxCTO);
            CommandCode cmd = psResponse != null ? CommandCode.Program : CommandCode.Download;
            bool useMaxCommands = !isMasterBM
              && data.Length > maxFrameBytes
              && (psResponse != null ? isAllowedRequest("PROGRAM_MAX") : isAllowedRequest("DOWNLOAD_MAX"));
            if (useMaxCommands)
            { // allowed to use the max commands
              maxBS = ++maxFrameBytes;
              cmd = psResponse != null ? CommandCode.ProgramMax : CommandCode.DownloadMax;
            }

            if (isMasterBM)
              maxBS = (byte)Math.Min((int)byte.MaxValue - 2
                , Math.Max(1, (int)(psResponse != null ? psResponse.MaxBS : mProtocolLayer.CommModesSupported.MaxBS)) * maxFrameBytes
                );
            //maxBS = (byte)(Math.Max(1, (int)0) * maxFrameBytes);

            var currBS = Math.Min(data.Length, maxBS);
            int updateStep = Math.Max(1, data.Length / 100);
            int currIndex = 0;

            while (currIndex < data.Length)
            { // data available
              var transLen = data.Length - currIndex > maxFrameBytes
                ? maxFrameBytes
                : data.Length - currIndex;

              var transferData = new byte[transLen];
              Array.Copy(data, currIndex, transferData, 0, transLen);

              if (stMin > 0 && cmd == CommandCode.ProgramNext || cmd == CommandCode.DownloadNext)
              { // wait the defined stMin value
                Task.Factory.StartNew(() => { Thread.Sleep(stMin); }).Wait(stMin);
              }

              result = WriteBytes(cmd, ref transferData, (byte)currBS, currBS > transLen ? 0 : -1);

              if (CmdResult.OK != result)
                break;

              currIndex += transLen;

              currBS -= transLen;
              if (currBS <= 0)
              { // next sequence
                currBS = Math.Min(data.Length - currIndex, maxBS);
                bool useMaxCmd = useMaxCommands && data.Length - currIndex >= maxFrameBytes;
                cmd = psResponse != null
                  ? useMaxCmd ? CommandCode.ProgramMax : CommandCode.Program
                  : useMaxCmd ? CommandCode.DownloadMax : CommandCode.Download;
              }
              else
                cmd = psResponse != null ? CommandCode.ProgramNext : CommandCode.DownloadNext;

              if (null != progressHandler && currIndex > updateStep)
              {
                var args = new ProgressArgs((int)(currIndex * 100.0 / data.Length));
                progressHandler(this, args);
                if (args.Cancel)
                  // cancelled
                  return false;
                updateStep += Math.Max(1, data.Length / 100);
              }
            }
          }
          return result == CmdResult.OK;
        }
        catch { return false; }
        finally
        {
#if DEBUG
          mLogger.Info($"{(psResponse != null ? "Program" : "Download")} {result}");
#endif
          progressHandler?.Invoke(this, new ProgressArgs(100));
        }
      }
    }
    public override bool writeSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      return writeSyncImpl(addressExtension, address, data, null, progressHandler);
    }
    public override bool programSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      , XCPPrgParams modes = null
      )
    {
      bool result = false;
      try
      {
        RespProgramStart psResponse;
        if (CmdResult.OK == ProgramStart(out psResponse))
        {
          if (!isAllowedRequest("SET_MTA")
            || !(!isMasterBlockMode(psResponse.MaxCTO) || isMasterBlockMode(psResponse.MaxCTO) && isAllowedRequest("PROGRAM_NEXT"))
            )
            return false;

          if (modes == null || CmdResult.OK == ProgramClear(modes.ClearMode, (UInt32)data.Length))
          {
            result = writeSyncImpl(addressExtension, address, data, psResponse, progressHandler);
            if (result && modes != null && modes.VerifyMode != ProgramVerifyMode.None)
              result = CmdResult.OK == ProgramVerify(modes.VerifyMode, modes.VerifyType, modes.VerifyValue);
          }
        }
      }
      finally
      {
        result = CmdResult.OK == ProgramReset();
      }
      return result;
    }
    /// <summary>
    /// Modify Bits on the specific address.
    /// 
    /// - Calls XCPMasterBase.SetMTA with the specified address.
    /// - Calls XCPMasterBase.ModifyBits with the given parameters.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to modify</param>
    /// <param name="s">Shift Value (S)</param>
    /// <param name="ma">AND Mask (MA)</param>
    /// <param name="mx">XOR Mask (MX)</param>
    /// <returns>true if successful</returns>
    public bool modifyBits(byte addressExtension, UInt32 address, byte s, UInt16 ma, UInt16 mx)
    {
      if (!isAllowedRequest("SET_MTA") || !isAllowedRequest("MODIFY_BITS"))
        return false;

      if (CmdResult.OK != SetMTA(addressExtension, address))
        return false;

      if (CmdResult.OK != ModifyBits(s, ma, mx))
        return false;

      return true;
    }
    /// <summary>
    /// Get identification from slave.
    /// 
    /// Automatically uploads the requested data if required.
    /// </summary>
    /// <param name="idType">The request type</param>
    /// <param name="data">Contains the ID data if return value is true</param>
    /// <returns>true if successful</returns>
    public bool getID(GetIDType idType, out byte[] data)
    {
      data = null;
      if (!isAllowedRequest("GET_ID"))
        return false;
      lock (mReadWriteLock)
      {
        RespGetID response;
        bool result = CmdResult.OK == GetID(idType, out response, out data);
        if (result && (response.Mode & GetIDRespType.TRANSFER_MODE) == 0)
          // do a the required manual upload
          result = readSync((int)response.Length, 0, UInt32.MaxValue, out data);
        return result;
      }
    }
    /// <summary>
    /// Tries to unlock a specific protected ECU resource using the specified Seed&Key DLL.
    /// </summary>
    /// <param name="status">The current status</param>
    /// <param name="seedAndKeyDLL">The Seed&Key DLL</param>
    /// <param name="resource">Resource to unlock</param>
    /// <returns>A CmdResult value</returns>
    public CmdResult unlockECU(RespGetStatus status, NativeSKDLL seedAndKeyDLL, ResourceType resource)
    {
      if (!SlaveConnected)
        return CmdResult.ERR_TIMEOUT;

      if ((status.ResourceProtectionState & resource) == 0)
        // not protected
        return CmdResult.OK;

      CmdResult result;

      // Get the seed key from the device
      RespGetSeed respGetSeed;
      byte[] seed1;
      if (CmdResult.OK != (result = GetSeed(SeedModeType.FirstPart, resource, out respGetSeed, out seed1))
        || respGetSeed.Length == 0
         )
        // failed or resource unprotected
        return result;

      // copy first part of seed
      var seed = new List<byte>(seed1);

      byte totalLen = respGetSeed.Length;
      if (totalLen > MaxCTO - 2)
      { // request remaining part of seed if the total length of the seed 
        // is longer than MaxDTO-2 (see XCP specification)
        byte[] seed2;
        do
        { // get the remaining part
          if (CmdResult.OK != (result = GetSeed(SeedModeType.RemainingPart, resource, out respGetSeed, out seed2)))
            return result;
          seed.AddRange(seed2);
        } while (seed2.Length < respGetSeed.Length);
      }

      // Compute the key from the Seed&Key DLL
      byte[] resultingKey;
      if (NativeSKDLL.ResultSk.FncAck != seedAndKeyDLL.ComputeKeyFromSeed((byte)resource, seed.ToArray(), out resultingKey))
        return CmdResult.ERR_GENERIC;

      // Unlock with the computed key
      RespUnlock respUnlock = null;
      int offset = 0;
      while (offset != resultingKey.Length)
        if (CmdResult.OK != (result = Unlock(resultingKey, ref offset, out respUnlock)))
          return result;

      return (respUnlock.ProtectionState & resource) == 0
        ? CmdResult.OK
        : CmdResult.ERR_ACCESS_LOCKED;
    }
    /// <summary>
    /// Tries to unlock all protected ECU resources using the specified Seed&Key DLL.
    /// </summary>
    /// <param name="seedAndKeyDLL">The Seed&Key DLL</param>
    /// <returns>A CmdResult value</returns>
    public CmdResult unlockECU(NativeSKDLL seedAndKeyDLL)
    {
      if ((seedAndKeyDLL.Type & SKType.XCP) == 0)
        return CmdResult.ERR_GENERIC;

      if (!SlaveConnected)
        return CmdResult.ERR_TIMEOUT;

      var status = StatusResponse;
      if (status == null)
        return CmdResult.ERR_PROTOCOL_FAILURE;

      CmdResult result = CmdResult.OK;
      CmdResult localResult = CmdResult.OK;
      if ((status.ResourceProtectionState & ResourceType.CAL_PAG) > 0)
        localResult = unlockECU(status, seedAndKeyDLL, ResourceType.CAL_PAG);
      if (localResult != CmdResult.OK)
        result = localResult;
      if ((status.ResourceProtectionState & ResourceType.DAQ) > 0)
        localResult = unlockECU(status, seedAndKeyDLL, ResourceType.DAQ);
      if (localResult != CmdResult.OK)
        result = localResult;
      if ((status.ResourceProtectionState & ResourceType.PGM) > 0)
        localResult = unlockECU(status, seedAndKeyDLL, ResourceType.PGM);
      if (localResult != CmdResult.OK)
        result = localResult;
      if ((status.ResourceProtectionState & ResourceType.STIM) > 0)
        localResult = unlockECU(status, seedAndKeyDLL, ResourceType.STIM);
      if (localResult != CmdResult.OK)
        result = localResult;

      RespGetStatus respGetStatus;
      GetStatus(out respGetStatus);

      return result;
    }
    public override bool setPage(ECUPage page)
    {
      if (!SlaveConnected)
        return false;
      if (ActivePage == page)
        // page is already set
        return true;

      // remove the local active page
      mResponseDict.Remove(CommandCode.GetCalPage);

      CmdResult result;
      if (CmdResult.OK != (result = SetCalPage(CalPageMode.ALL | CalPageMode.XCP | CalPageMode.ECU, (byte)0, (byte)page)))
        return false;

      RespGetCALPage response;
      return CmdResult.OK == GetCalPage(CalPageMode.XCP, (byte)0, out response);
    }
    /// <summary>
    /// Persists the current active page (after calibration data is downloaded) into flash memory.
    /// 
    /// This is only successful on devices supporting freeze page (see PAGProperties.FreezeSupported).
    /// </summary>
    /// <param name="segment">segment number (usually 0)</param>
    /// <param name="timeout">timeout in ms</param>
    /// <returns>A CmdResult value</returns>
    public CmdResult freezePage(byte segment, uint timeout)
    {
      if (!SlaveConnected)
        return CmdResult.ERR_TIMEOUT;

      lock (this)
      {
        stopMeasurements(true);
        var result = CmdResult.OK;

        if (CmdResult.OK != (result = SetSegmentMode(SegmentMode.Freeze, segment)))
          return result;

        try
        {
          UInt16 sessionID = (UInt16)(new Random((int)DateTime.Now.Ticks).NextDouble() * UInt16.MaxValue);

          if (CmdResult.OK != (result = SetRequest(SetRequestMode.StoreCALRequest, sessionID)))
            return result;

          var startRequest = DateTime.Now;
          bool stored = false;

          while (!stored)
          {
            Thread.Sleep(mProtocolLayer.Timings[0]);
            RespGetStatus respGetStatus;
            if (CmdResult.OK != (result = GetStatus(out respGetStatus)))
              return result;
            if ((respGetStatus.SessionState & SessionState.StoreCALRequest) == 0 && sessionID == respGetStatus.SessionConfigurationId)
            { // ready
              stored = true;
              break;
            }
            var end = DateTime.Now;
            if (end.Subtract(startRequest).TotalMilliseconds > timeout)
              // timeout occured
              return CmdResult.ERR_TIMEOUT;
          }
        }
        finally
        { // force always to reset the segment mode
          SetSegmentMode(SegmentMode.None, segment);
        }
        return result;
      }
    }
    public override bool copyPage2Page(ECUPage srcPage, ECUPage dstPage)
    {
      if (srcPage == dstPage)
        throw new ArgumentException("copy must be called with different pages");
      switch (dstPage)
      {
        case ECUPage.Flash:
          if ((PAGProcInfo.Properties & PAGProperties.FreezeSupported) > 0)
            return CmdResult.OK == freezePage(0, 30000);
          else
            goto case ECUPage.RAM;
        case ECUPage.RAM:
          return CmdResult.OK == CopyCalPage(0, (byte)srcPage, 0, (byte)dstPage);
        default: throw new NotSupportedException(dstPage.ToString());
      }
    }
    public override bool getChecksum(byte addressExtension, UInt32 address, UInt32 size, out UInt32 checksum)
    {
      checksum = 0;
      if (CmdResult.OK != SetMTA(addressExtension, address))
        return false;
      RespBuildChecksum respBuildChecksum;
      if (CmdResult.OK != BuildChecksum(size, out respBuildChecksum))
        return false;
      checksum = respBuildChecksum.Checksum;
      return true;
    }
    #region DAQ support
    public override void onDAQFrameReceived(XCPFrame xcpFrame)
    {
      if (!Monitor.TryEnter(DAQs))
        // DAQ dict in use, ignore daq frame!
        return;
      try
      {
        if (!Connected || !mIsDAQRunning)
          return;

        int offset;
        var daqList = ((DAQDictXCP)DAQs).findDAQList(ref xcpFrame.Data, mA2LDAQ, ChangeEndianess, out offset);
        if (daqList == null)
          // DAQ List not found
          return;

        ODTList odt;
        if (!daqList.ODTs.TryGetValue((byte)(xcpFrame.Data[0] - daqList.FirstPID), out odt))
          // ODT not found
          return;

        if (odt.PID == 0)
        { // The first ODT set is timestamp master
          double timestamp = daqList.getTimeDiffInSecs(ref xcpFrame.Data, offset, mTimestampSize, mA2LTS, ChangeEndianess);

          if (double.IsNaN(timestamp))
            // no timestamp value supplied by the DAQ
            TimeBase.DAQLastTimestamp = daqList.LastReceivedRelSecs = TimeBase.ElapsedDAQSeconds;
          else
          { // DAQ timestamp is valid
            TimeBase.DAQLastTimestamp = daqList.LastReceivedRelSecs = double.IsNaN(daqList.LastReceivedRelSecs)
              ? TimeBase.ElapsedDAQSeconds
              : daqList.LastReceivedRelSecs + timestamp;
          }
          byte[] data = daqList.DAQRecordCache.getData();
          if (data != null)
          { // All records of a DAQList are completely received
            // Data complete event to registered 
            RaiseOnValuesReceived(daqList.DAQNo, TimeBase.DAQLastTimestamp, ref data);
            daqList.onValuesComplete(ref data, ChangeEndianess);
          }
        }

        if (double.IsNaN(daqList.LastReceivedRelSecs))
          // first ODT is not received already, ignore this record data
          return;
        int expectedDataLen = odt.DataLength + (odt.PID == 0 ? mDAQDataOffsetFirstPid : mDAQDataOffset);
        int unusedBytes = Math.Max(xcpFrame.Data.Length - expectedDataLen, 0);
        // store received values in DAQ record cache
        daqList.DAQRecordCache.Add(ref xcpFrame.Data, odt.PID == 0 ? mDAQDataOffsetFirstPid : mDAQDataOffset, unusedBytes);
      }
      finally
      { // force release lock
        Monitor.Exit(DAQs);
      }
    }
    public override void configureMeasurements(List<A2LMEASUREMENT> measurements)
    {
      lock (DAQs)
      { // clear local DAQ configuration
        DAQs.Clear();
        mConfigureMeasurements = true;

        if (mA2LDAQ.DAQAndEvtMap.Count == 0)
          // no DAQ lists at all defined by device
          return;

        measurements.Sort(SortBySizeAscending);

        byte maxODTEntrySize = DAQResolutionInfo == null
          ? mA2LDAQ.MaxODTEntrySize
          : Math.Min(mA2LDAQ.MaxODTEntrySize, DAQResolutionInfo.MaxODTEntrySizeDAQ);

        ((DAQDictXCP)DAQs).configure(MaxDTO, maxODTEntrySize
          , mDAQDataOffsetFirstPid, mDAQDataOffset
          , mA2LDAQ.IDField == XCP_ID_FIELD_TYPE.ABSOLUTE
          , mA2LDAQ.DAQAndEvtMap
          , measurements
          );
      }
    }
    /// <summary>
    /// Allocate DAQLists, ODTs and ODTEntries.
    /// </summary>
    /// <returns>true if successful</returns>
    bool AllocDAQs()
    {
      CmdResult result;
      bool dynamicAllocation = mA2LDAQ.Mode == XCP_DAQ_MODE.DYNAMIC && DAQs.Count - mA2LDAQ.MinDAQ > 0;
      if (dynamicAllocation)
      {
        if (CmdResult.OK != (result = FreeDAQ()))
          return false;

        if (CmdResult.OK != (result = AllocDAQ((UInt16)(DAQs.Count - mA2LDAQ.MinDAQ))))
          return false;
      }

      var enDAQ = DAQs.GetEnumerator();
      while (enDAQ.MoveNext())
      { // iterate through DAQ Lists
        UInt16 daqListNo = enDAQ.Current.Key;
        if (dynamicAllocation && daqListNo >= mA2LDAQ.MinDAQ)
        { // Alloc ODT Counts on configurable DAQ List
          DAQList daqList = enDAQ.Current.Value;
          if (daqList.ODTs.Count > 0)
            if (CmdResult.OK != (result = AllocODT(daqListNo, (byte)(daqList.ODTs.Count))))
              return false;
        }
      }

      if (dynamicAllocation)
      { // dynamic DAQ configuration
        enDAQ = DAQs.GetEnumerator();
        while (enDAQ.MoveNext())
        { // iterate through DAQ Lists
          UInt16 daqListNo = enDAQ.Current.Key;
          if (daqListNo >= mA2LDAQ.MinDAQ)
          { // Alloc ODT Entries on configurable DAQ List
            DAQList daqList = enDAQ.Current.Value;
            for (byte i = 0; i < daqList.ODTs.Count; ++i)
              if (CmdResult.OK != (result = AllocODTEntry(daqListNo, i, (byte)(daqList.ODTs[i].Count))))
                return false;
          }
        }
      }

      enDAQ = DAQs.GetEnumerator();
      while (enDAQ.MoveNext())
      { // iterate through DAQ Lists
        UInt16 daqListNo = enDAQ.Current.Key;
        if (daqListNo < mA2LDAQ.MinDAQ)
          // clear only the static DAQs
          if (CmdResult.OK != (result = ClearDAQList(enDAQ.Current.Key)))
            return false;
      }
      return true;
    }
    /// <summary>
    /// Configure the DAQList on the ECU.
    /// 
    /// This is done by writing the configured ODT Entries 
    /// with WriteDAQ and SetDAQPtr.
    /// </summary>
    /// <param name="daqListNo">The DAQ List number to write</param>
    /// <param name="daqList">The DAQ List itself</param>
    /// <param name="useWriteDAQMultiple">true if writeDAQMultiple should be used (currently not implemented)</param>
    /// <returns>true if successful</returns>
    bool configureDAQList(UInt16 daqListNo, DAQList daqList, bool useWriteDAQMultiple)
    {
      CmdResult result;
      var enODT = daqList.ODTs.GetEnumerator();
      while (enODT.MoveNext())
      {
        var odts = enODT.Current.Value;

        // Call SetDAQPtr when a new ODT is configured
        if (CmdResult.OK != (result = SetDAQPtr(daqListNo, odts.PID, 0)))
          return false;

        foreach (var entry in odts)
        { // Write DAQ for every ODT entry
          if (CmdResult.OK != (result = WriteDAQ(entry.BitOffset
            , entry.Size
            , (byte)entry.Measurement.AddressExtension
            , entry.Address
            )))
            // failed
            return false;
        }
      }
      return true;
    }
    public override bool startMeasurements(bool doSynchronized)
    {
      lock (DAQs)
      {
        if (!Connected)
          return false;

        if (DAQs.Count == 0)
          return true;

        if (mIsDAQRunning)
          // already running
          return false;

        DAQs.clearData();
        TimeBase.resetDAQTime();

        CmdResult result; DAQDict.Enumerator enDAQ;

        if (mConfigureMeasurements)
        { // write configuration to ECU is required

          if (!AllocDAQs())
            return false;

          enDAQ = DAQs.GetEnumerator();
          while (enDAQ.MoveNext())
          {
            UInt16 daqListNo = enDAQ.Current.Key;
            var daqList = enDAQ.Current.Value;

            if (!configureDAQList(daqListNo, daqList, false))
              // failed to configure DAQ lists
              return false;

            DAQListMode daqListMode = DAQListMode.None;
            if (mTimestampSize != XCP_TIMESTAMP_SIZE.NotSet)
              daqListMode |= DAQListMode.Timestamp;

            var evnt = mA2LDAQ.DAQAndEvtMap[daqListNo].Evt;
            if (null != evnt)
            { // connection from DAQ List to event
              if (CmdResult.OK != (result = SetDAQListMode(daqListMode, daqListNo, evnt.Id, 1, evnt.Priority)))
                return false;
            }
          }
        }

        if (mProtocolLayer.OptionalCmds.Contains("GET_DAQ_CLOCK")
          && DAQProcInfo != null && (DAQProcInfo.Properties & DAQProperties.TimestampSupported) > 0
           )
        { // Request the current DAQ Clock
          RespGetDAQClock respGetDAQClock;
          GetDAQClock(out respGetDAQClock);
          mDAQClock = respGetDAQClock.Timestamp;
        }
        else
          mDAQClock = 0;

        // Do the Start/Stop commands
        enDAQ = DAQs.GetEnumerator();
        while (enDAQ.MoveNext())
        {
          UInt16 daqListNo = enDAQ.Current.Key;
          var daqList = enDAQ.Current.Value;
          if (daqList.ODTs.Count > 0)
          {
            StartStopMode startCmd = doSynchronized ? StartStopMode.Select : StartStopMode.Start;
            RespStartStopDAQList respStartStop;
            result = StartStopDAQList(startCmd, daqListNo, out respStartStop);
            if (result == CmdResult.OK && mA2LDAQ.IDField == XCP_ID_FIELD_TYPE.ABSOLUTE)
              daqList.FirstPID = respStartStop.FirstPID;
          }
        }

        // Start in synchronized mode
        if (doSynchronized && CmdResult.OK != (result = StartStopSynch(StartStopMode.Start)))
          return false;

        mLogger.Debug($"{DAQs.Count} DAQLists running");
        mConfigureMeasurements = false;
        mIsDAQRunning = true;
        return true;
      }
    }
    public override bool stopMeasurements(bool doSynchronized)
    {
      try
      {
        lock (DAQs)
        {
          if (!mIsDAQRunning || DAQs.Count == 0)
            // already stopped
            return true;

          // stop any configured DAQ list
          var enDAQ = DAQs.GetEnumerator();
          CmdResult result;
          while (enDAQ.MoveNext())
          {
            UInt16 daqListNo = enDAQ.Current.Key;
            var daqList = enDAQ.Current.Value;
            if (daqList.ODTs.Count > 0)
            {
              RespStartStopDAQList respStartStop;
              StartStopMode stopCmd = doSynchronized ? StartStopMode.Select : StartStopMode.Stop;
              StartStopDAQList(stopCmd, daqListNo, out respStartStop);
            }
          }

          if (doSynchronized)
            // stop all DAQ lists together
            if (CmdResult.OK != (result = StartStopSynch(StartStopMode.Stop)))
              return false;

          mLogger.Debug($"{DAQs.Count} DAQ lists stopped");
          return true;
        }
      }
      finally
      {
        mIsDAQRunning = false;
        mDAQClock = 0;
      }
    }
    #endregion
    #endregion
    #endregion
  }
}