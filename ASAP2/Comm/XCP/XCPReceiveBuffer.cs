﻿/*!
 * @file    
 * @brief   Implements a receive buffer for XCP on Ethernet frames.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 *  
 */
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Implements a receive buffer for XCP frames.
  /// 
  /// On calling XCPReceiveBuffer.getFrameFromData a valid frame from received data may be decoded.
  /// </summary>
  public sealed class XCPReceiveBuffer
  {
    static ILog mLogger = LogManager.GetLogger(typeof(XCPReceiveBuffer));
    /// <summary>
    /// Size of length and counter value.
    /// </summary>
    int mLenCtrSize;
    int mMaxDTO = 255;
    int mMinBytes;
    List<byte> mBuffer = new List<byte>();
    /// <summary>
    /// Creates a new instance of XCPReceiveBuffer.
    /// </summary>
    /// <param name="lenCtrSize">The size of Len and Ctr byte (may be zero)</param>
    public XCPReceiveBuffer(int lenCtrSize)
    {
      mLenCtrSize = lenCtrSize;
      mMinBytes = mLenCtrSize + 1;
    }
    /// <summary>
    /// Gets or sets a value for the MaxDTO.
    /// 
    /// This value usually is received from a connect response.
    /// </summary>
    public int MaxDTO
    {
      get { return mMaxDTO; }
      set { mMaxDTO = value; }
    }
    /// <summary>
    /// Resets the buffer (clean).
    /// 
    /// Maybe this is appropriate if connection is lost.
    /// </summary>
    public void reset()
    {
      lock (mBuffer)
      {
        mBuffer.Clear();
        mMaxDTO = 255;
      }
    }
    /// <summary>
    /// Delivers a frame as long as enough data is in buffer.
    /// 
    /// - If a frame could not be built (missing data) the supplied 
    /// data is taken into an internal buffer for the next call.
    /// </summary>
    /// <param name="type">The XCP type</param>
    /// <param name="source">The source from where the frame is received (may be null)</param>
    /// <param name="data">The data to build the frame from</param>
    /// <param name="frame">The built XCP frame</param>
    /// <returns>true if a valid frame is built</returns>
    public bool getFrameFromData(XCPType type, string source, ref byte[] data, out XCPFrame frame)
    {
      try
      {
        lock (mBuffer)
        {
          frame = null;
          if (data != null)
            // copy data
            mBuffer.AddRange(data);

          // Copy existing data and new data together
          byte[] completeData = mBuffer.ToArray();
          if (completeData.Length < mMinBytes)
            // minimum data to decode a frame not reached
            return false;

          // first byte is the length
          UInt16 len = BitConverter.ToUInt16(completeData, 0);

          if (!BitConverter.IsLittleEndian)
            len = Extensions.changeEndianess(len);

          int requiredLen = len + mLenCtrSize;
          if (requiredLen < mMinBytes || requiredLen - mLenCtrSize > mMaxDTO)
          { // protocol failure, this should never occur!
            mBuffer.Clear();
            return false;
          }
          if (completeData.Length < requiredLen)
            // required data length for a frame not reached
            return false;

          // at least one complete frame!
          var frameData = new byte[requiredLen];
          Array.Copy(completeData, frameData, requiredLen);
          mBuffer.RemoveRange(0, requiredLen);
          frame = new XCPFrame(type, source, ref frameData, false);
          return true;
        }
      }
      catch (Exception ex)
      { // caught exception
        mLogger.Error("caught receive buffer exception", ex);
        frame = null;
        lock(mBuffer)
          mBuffer.Clear();
        return false;
      }
    }
  }
}