﻿/*!
 * @file    
 * @brief   Implements the UDP and TCP drivers for supporting XCP.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using jnsoft.ASAP2.Properties;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Base class implementing the XCP on ethernet based communication instance.
  /// </summary>
  public abstract class XCPonEth : XCPCommunicator
  {
    #region members
    protected XCPReceiveBuffer mReceivedBuffer;
    protected IPEndPoint Slave;
    Thread mReceiveThread;
    protected ManualResetEvent mCancelEvent = new ManualResetEvent(false);
    #endregion
    #region constructors
    /// <summary>
    /// Creates a new instance of XCPonEth.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="slave">The XCP slave</param>
    internal protected XCPonEth(XCPMaster master, IPEndPoint slave)
      : base(master)
    {
        mReceivedBuffer = new XCPReceiveBuffer(4);
        Slave = slave;
      mReceiveThread = new Thread(new ThreadStart(receiveThread));
      mReceiveThread.IsBackground = true;
      mReceiveThread.Start();
    }
    #endregion
    #region methods
    /// <summary>
    /// The receive thread.
    /// 
    /// - This thread should only end, if mCancelReceiving is set to true.
    /// - Catch all exceptions to keep this thread alive
    /// </summary>
    protected abstract void receiveThread();
    internal override void setMaxDTO(int maxDTO) { mReceivedBuffer.MaxDTO = maxDTO; }
    internal override void reset() { mReceivedBuffer.reset(); }
    protected sealed override bool getFrameFromData(XCPType type, string source, ref byte[] data, out XCPFrame frame)
    {
      return mReceivedBuffer.getFrameFromData(type, source, ref data, out frame);
    }
    #endregion
    #region IDisposable Members
    /// <summary>
    /// Disposes all used resources.
    /// 
    /// Inparticular:
    /// - The receive thread
    /// - The communication channel 
    /// </summary>
    public sealed override void Dispose()
    {
      mCancelEvent.Set();
      Thread.Sleep(10);
      mCancelEvent.Dispose();
      base.Dispose();
    }
    #endregion
  }
  /// <summary>
  /// Implements the UDP based XCP communication.
  /// </summary>
  public sealed class XCPonUDP : XCPonEth
  {
    UdpClient mClient;
    UdpClient getClient()
    {
      if (mClient == null)
      { // create Udp client
        mClient = new UdpClient(new IPEndPoint(IPAddress.Any, 0));
        SourceAddress = mClient.Client.LocalEndPoint.ToString();
        // Set receive buffer size large enough to reduce lost packets!
        mClient.Client.ReceiveBufferSize = 4 * UInt16.MaxValue;
        mClient.Ttl = Settings.Default.TTL;
      }
      return mClient;
    }
    /// <summary>
    /// Creates a new instance of XCPonUDP.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="slave">The XCP slave</param>
    public XCPonUDP(XCPMaster master, IPEndPoint slave) : base(master, slave) { }
    protected override void close()
    {
        if (mClient == null)
          return;
        try { mClient.Close(); }
        catch { }
        mClient = null;
    }
    internal override int send(byte[] data)
    {
      try
      {
        UdpClient client = getClient();
        int len = client.Send(data, data.Length, Slave);
        if (len == data.Length)
          return len;
      }
      catch { }
      // failed to send all data
      close();
      return 0;
    }
    protected override void receiveThread()
    {
      while (true)
      {
        if (mCancelEvent.WaitOne(1))
          break;
        try
        {
          while (mClient!=null && mClient.Available > 0)
          { // minimum data is available
            var data = mClient.Receive(ref Slave);
            dataReceived(this, mMaster.SlaveAddress, ref data);
          }
        }
        catch { }
      }
    }
  }
  /// <summary>
  /// Implements the UDP based XCP communication.
  /// </summary>
  public sealed class XCPonTCP : XCPonEth
  {
    Socket mClient;
    Socket getClient()
    {
      if (mClient == null)
      { // create Tcp client
        try
        {
          mClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
          mClient.Ttl = Settings.Default.TTL;
          // Don't use the nagle algo
          mClient.NoDelay = true;
          mClient.Connect(Slave);
          SourceAddress = mClient.LocalEndPoint.ToString();
        }
        catch
        {
          mClient.Close();
          mClient = null;
        }
      }
      return mClient;
    }
    /// <summary>
    /// Creates a new instance of XCPonTCP.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="slave">The XCP slave</param>
    internal XCPonTCP(XCPMaster master, IPEndPoint slave) : base(master, slave) { }
    protected override void close()
    {
      if (mClient != null)
      {
        try
        {
          mClient.Shutdown(SocketShutdown.Both);
          mClient.Close();
        }
        catch { }
        mClient = null;
      }
    }
    internal override int send(byte[] data)
    {
      try
      {
        var client = getClient();
        if (client != null)
        {
          if (data.Length == client.Send(data))
            return data.Length;
        }
      }
      catch { }
      // failed to send all data
      close();
      return 0;
    }
    protected override void receiveThread()
    {
      while (true)
      {
        if (mCancelEvent.WaitOne(1))
          break;
        try
        {
          if (mClient == null)
            continue;
          while (mClient.Connected && mClient.Available > 0)
          { // data is available
            var data = new byte[mClient.Available];
            int bytesRead = mClient.Receive(data);
            dataReceived(this, mMaster.SlaveAddress, ref data);
          }
        }
        catch { }
      }
    }
  }
}