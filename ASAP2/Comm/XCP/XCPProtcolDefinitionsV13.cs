﻿/*!
 * @file    
 * @brief   Defines the XCP protocol definitions (XCP version v1.3 extension)
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    February 2018
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Runtime.InteropServices;

namespace jnsoft.Comm.XCP
{
  #region enums
  /// <summary>
  /// DtoCtrProperties MODIFIER parameter bit mask coding.
  /// </summary>
  [Flags]
  public enum DTOCtrModifier : byte
  {
    None = 0x00,
    /// <summary>
    /// 0 = Keep the current mode setting<para/>
    /// 1 = Take the mode setting from the command
    /// </summary>
    STIM = 0x01,
    /// <summary>
    /// 0 = Keep the current mode setting<para/>
    /// 1 = Take the mode setting from the command
    /// </summary>
    DAQ = STIM << 1,
    /// <summary>
    /// 0 = Keep the current number<para/>
    /// 1 = Take the new number from the command
    /// </summary>
    RELATED_EVENT = STIM << 2,
  }
  /// <summary>
  /// DtoCtrProperties MODE parameter bit mask coding.
  /// </summary>
  [Flags]
  public enum DTOCtrMode : byte
  {
    None = 0x00,
    /// <summary>
    /// When receiving DTOs with CTR field:<para/>
    /// 0 = DO_NOT_CHECK_COUNTER<para/>
    /// 1 = CHECK_COUNTER
    /// </summary>
    DAQ = 0x01,
    /// <summary>
    /// When inserting the DTO CTR field:<para/>
    /// 0 = INSERT_COUNTER - use CTR of the related event<para/>
    /// 1 = INSERT_STIM_COUNTER_COPY - use STIM CTR copy of the related event
    /// </summary>
    STIM = DAQ << 1,
  }
  /// <summary>
  /// DtoCtrProperties PROPERTIES parameter bit mask coding.
  /// </summary>
  [Flags]
  public enum DTOCtrProperties : byte
  {
    /// <summary>
    /// Related event channel number of this event channel.<para/>
    /// 0 = can be modified<para/>
    /// 1 = cannot be modified
    /// </summary>
    RelatedEventFixed = 0x01,
    /// <summary>
    /// DTO CTR DAQ mode of this event channel.<para/> 
    /// 0 = can be modified<para/>
    /// 1 = cannot be modified
    /// </summary>
    DAQModeFixed = 0x02,
    /// <summary>
    /// DTO CTR STIM mode of this event channel.<para/>
    /// 0 = can be modified<para/>
    /// 1 = cannot be modified
    /// </summary>
    STIMModeFixed = 0x04,
    /// <summary>
    /// This event channel<para/>
    /// 0 = does not have a related event channel<para/>
    /// 1 = has a related event channel
    /// </summary>
    RelatedEventPresent = 0x08,
    /// <summary>
    /// DTO CTR DAQ mode: for direction DAQ this event channel<para/>
    /// 0 = does not support DTO CTR handling<para/>
    /// 1 = supports DTO CTR handling
    /// </summary>
    DAQModePresent = 0x10,
    /// <summary>
    /// DTO CTR STIM mode: for direction STIM this event channel<para/>
    /// 0 = does not support DTO CTR handling<para/>
    /// 1 = supports DTO CTR handling
    /// </summary>
    STIMModePresent = 0x20,
    /// <summary>
    /// STIM DTO CTR copy: on successful stimulation, this event channel<para/>
    /// 0 = can save the DTO CTR for later reference<para/>
    /// 1 = cannot save the DTO CTR for later reference
    /// </summary>
    STIMCtrCpyPresent = 0x40,
    /// <summary>
    /// Event counter<para/>
    /// 0 = This event channel has no cycle counter<para/>
    /// 1 = This event channel has a cycle counter
    /// </summary>
    EctCtrPresent = 0x80,
  }
  /// <summary>
  /// Time Correlation SET_PROPERTIES parameter bit mask structure.
  /// </summary>
  [Flags]
  public enum TimeCorrSetProps : byte
  {
    None = 0x00,
    /// <summary>Send EV_TIME_SYNC event packet as response to TRIGGER_INITIATOR 0, 2 and 3 only</summary>
    ResponseFmtSendOnInitTrigger = 0x01,
    /// <summary>Send EV_TIME_SYNC event packet for all trigger conditions</summary>
    ResponseFmtSendOnAllTrigger = ResponseFmtSendOnInitTrigger << 1,
    /// <summary>Enable time synchronization bridging functionality</summary>
    TimeSyncBridgeEnable = ResponseFmtSendOnInitTrigger << 2,
    /// <summary>Disable time synchronization bridging functionality</summary>
    TimeSyncBridgeDisable = ResponseFmtSendOnInitTrigger << 3,
    /// <summary>Assign XCP slave to the logical time correlation cluster given by CLUSTER_ID parameter</summary>
    SetClusterId = ResponseFmtSendOnInitTrigger << 4,
  }
  /// <summary>
  /// Time Correlation GET_PROPERTIES_REQUEST parameter bit mask structure.
  /// </summary>
  [Flags]
  public enum TimeCorrGetPropsReq : byte
  {
    None = 0x00,
    /// <summary>
    /// The GET_CLK_INFO bit allows the XCP master to obtain detailed information of the
    /// observable clocks. Typically, the XCP master sets this bit when sending the first
    /// TIME_CORRELATION_PROPERTIES command. Based on the responses of its XCP slaves, the 
    /// XCP master is able to derive a tree structure representing the relationship among the 
    /// clocks in the system. In addition, when the XCP master detects that a clock has been 
    /// syntonized or synchronized to a grandmaster clock – based on the information sent as 
    /// part of the event – the XCP master will again send a TIME_CORRELATION_PROPERTIES 
    /// command with this bit set to update its view on the relationship among the clocks 
    /// in the system.<para/>
    /// 0 = No upload of information of the clocks requested<para/>
    /// 1 = Set MTA to start of clocks’ information data block
    /// </summary>
    GET_CLK_INFO = 0x01,
  }
  /// <summary>
  /// Time Correlation SLAVE_CONFIG parameter bit mask coding.
  /// </summary>
  [Flags]
  public enum SlaveConfig : byte
  {
    None = 0x00,
    /// <summary>Send EV_TIME_SYNC event packet as response to TRIGGER_INITIATOR 0, 2 and 3 only</summary>
    ResponseFmtSendOnInitTrigger = 0x01,
    /// <summary>Send EV_TIME_SYNC event packet for all trigger conditions</summary>
    ResponseFmtSendOnAllTrigger = ResponseFmtSendOnInitTrigger << 1,
    /// <summary>
    /// DAQ timestamp relation<para/>
    /// 0 = DAQ timestamps are related to XCP slave clock<para/>
    /// 1 = DAQ timestamps are related to ECU clock
    /// </summary>
    DAQTsRelation = ResponseFmtSendOnInitTrigger << 2,
    /// <summary>XCP slave offers a Time Sync Bridge but Time Sync Bridge is disabled</summary>
    TimeSyncBridgeDisabled = ResponseFmtSendOnInitTrigger << 3,
    /// <summary>XCP slave offers a Time Sync Bridge with Time Sync Bridge enabled</summary>
    TimeSyncBridgeEnabled = ResponseFmtSendOnInitTrigger << 4,
  }
  /// <summary>
  /// Time Correlation OBSERVABLE_CLOCKS parameter bit mask coding.
  /// </summary>
  [Flags]
  public enum ObservableClocks : byte
  {
    None = 0x00,
    /// <summary>XCP slave clock might be syntonized or synchronized to a grandmaster clock and can be read randomly</summary>
    XCP_SLV_CLK_AVAIL = 0x01,
    /// <summary>There is no XCP slave clock. Nevertheless, DAQ timestamps might be related to a synchronized clock.</summary>
    XCP_SLV_CLK_NOT_AVAIL = XCP_SLV_CLK_AVAIL << 1,
    /// <summary>The XCP slave offers a dedicated clock that might be synchronized to a grandmaster clock and can be read randomly</summary>
    GRANDM_CLK_AVAIL = XCP_SLV_CLK_AVAIL << 2,
    /// <summary>
    /// The XCP slave offers a dedicated clock that might be synchronized to a grandmaster clock. 
    /// The clock cannot be read randomly. However, the XCP slave autonomously generates EV_TIME_SYNC events 
    /// containing timestamps related to the XCP slave’s clock and the clock which is synchronized to a 
    /// grandmaster clock. Thereby, these timestamps have been sampled simultaneously.
    /// </summary>
    GRANDM_CLK_NOT_AVAIL = XCP_SLV_CLK_AVAIL << 3,
    /// <summary>The XCP slave has access to the ECU clock and can read the clock randomly</summary>
    ECU_CLK_CAN_READ_RANDOM = XCP_SLV_CLK_AVAIL << 4,
    /// <summary>
    /// The XCP slave has access to the ECU clock but cannot read the clock randomly. However, the 
    /// XCP slave autonomously generates EV_TIME_SYNC events containing timestamps related to the 
    /// XCP slave’s clock and the ECU clock. Thereby, these timestamps have been sampled simultaneously.
    /// </summary>
    ECU_CLK_CANNOT_READ_RANDOM = XCP_SLV_CLK_AVAIL << 5,
    /// <summary>The XCP slave reports ECU clock based timestamps whereas the XCP slave cannot read the ECU clock</summary>
    ECU_CLK_CANNOT_READ = ECU_CLK_CAN_READ_RANDOM | ECU_CLK_CANNOT_READ_RANDOM,
  }
  /// <summary>
  /// Time Correlation SYNC_STATE parameter bit mask coding.
  /// 
  /// - SLV_CLK_ parameter is only relevant if XCP_SLV_CLK parameter of OBSERVABLE_CLOCKS parameter is equal 1
  /// </summary>
  [Flags]
  public enum SyncState : byte
  {
    None = 0x00,
    /// <summary>XCP slave’s clock is synchronized to a grandmaster clock</summary>
    SLV_CLK_IS_SYNCRONIZED = 0x01,
    /// <summary>XCP slave’s clock is syntonized to a grandmaster clock</summary>
    SLV_CLK_IS_SYNTONIZED = SLV_CLK_IS_SYNCRONIZED | 0x02,
    /// <summary>XCP slave’s clock does not support synchronization/syntonization to a grandmaster clock</summary>
    SLV_CLK_IS_NOT_SUPPORTED = 0x04,
    /// <summary>Dedicated clock is synchronized to a grandmaster clock</summary>
    GRANDM_CLK = SLV_CLK_IS_NOT_SUPPORTED << 1,
    /// <summary>ECU clock is synchronized to a grandmaster clock</summary>
    ECU_CLK = SLV_CLK_IS_NOT_SUPPORTED << 2,
    /// <summary>The synchronization state of the ECU is unknown</summary>
    ECU_CLK_UNKNOWN = SLV_CLK_IS_NOT_SUPPORTED << 3,
  }
  /// <summary>
  /// Time Correlation CLOCK_INFO parameter bit mask structure.
  /// 
  /// - SLV_CLK_ parameter is only relevant if XCP_SLV_CLK parameter of OBSERVABLE_CLOCKS parameter is equal 1
  /// </summary>
  [Flags]
  public enum ClockInfo : byte
  {
    None = 0x00,
    /// <summary>
    /// 0 = XCP slave’s clock information not part of data block<para/>
    /// 1 = set MTA to start of XCP slave’s clock information data block
    /// </summary>
    SLV_CLK_INFO = 0x01,
    /// <summary>
    /// 0 = Grandmaster’s clock information not part of data block<para/>
    /// 1 = if there is no predecessor data block, MTA set to start of grandmaster’s clock information data block, else append grandmaster’s clock information block to predecessor data block
    /// </summary>
    GRANDM_CLK_INFO = SLV_CLK_INFO << 1,
    /// <summary>
    /// 0 = clock relation not part of data block<para/>
    /// 1 = if there is no predecessor data block, MTA set to start of clock relation data block, else append clock relation data block to predecessor data block
    /// </summary>
    CLK_RELATION = SLV_CLK_INFO << 2,
    /// <summary>
    /// 0 = ECU’s clock information not part of data block<para/>
    /// 1 = if there is no predecessor data block, MTA set to start of ECU’s clock information data block, else append ECU’s clock information block to predecessor data block
    /// </summary>
    ECU_CLK_INFO = SLV_CLK_INFO << 3,
    /// <summary>
    /// 0 = ECU’s grandmaster clock information not part of data block<para/>
    /// 1 = append ECU’s grandmaster clock information block to predecessor data block (there must be at least the ECU’s clock information block)
    /// </summary>
    ECU_GRANDM_CLK_INFO = SLV_CLK_INFO << 4,
  }
  /// <summary>
  /// GetDAQClock Trigger Initiator.
  /// </summary>
  public enum TriggerInitiator : byte
  {
    /// <summary>Hardware trigger, i.e. Vector Syncline</summary>
    HWTrigger,
    /// <summary>Event derived from XCP-independent time synchronization event – e.g. globally synchronized pulse per second signal</summary>
    EventDerived,
    /// <summary>GET_DAQ_CLOCK_MULTICAST</summary>
    MultiCast,
    /// <summary>GET_DAQ_CLOCK_MULTICAST via Time Sync Bridge</summary>
    MultiCastViaTimeSync,
    /// <summary>State change in syntonization/synchronization to grandmaster clock (either established or lost, additional information is provided by the SYNC_STATE field - see Table 236)</summary>
    StateChangeInSynSync,
    /// <summary>Leap second occurred on grandmaster clock</summary>
    LeapSecondOccured,
    /// <summary>Release of ECU reset</summary>
    ReleaseECUReset,
    reserved,
  }
  /// <summary>
  /// GetDAQClock Point in time when the XCP slave’s timestamp was sampled.
  /// 
  /// For GET_DAQ_CLOCK_MULTICAST it is strongly recommended to implement variant 3
  /// </summary>
  public enum TimeOfTSSampling : byte
  {
    /// <summary>during command processing at the protocol layer command processor</summary>
    DuringCmdProcessing,
    /// <summary>low jitter, measured in high-priority interrupt</summary>
    LowJitter,
    /// <summary>upon physical transmission to XCP master</summary>
    PhysicalTransmission,
    /// <summary>upon physical reception of command</summary>
    PhysicalReception,
  }

  /// <summary>
  /// GetDAQClock PAYLOAD_FMT parameter bit mask structure
  /// </summary>
  [Flags]
  public enum PayloadFmt : byte
  {
    None = 0x00,
    /// <summary>XCP slave’s clock: size of payload containing timestamp: DWORD</summary>
    XCP_SLV_DWORD = 0x01,
    /// <summary>XCP slave’s clock: size of payload containing timestamp: DLONG (unsigned interpretation)</summary>
    XCP_SLV_DLONG = XCP_SLV_DWORD << 1,
    /// <summary>Grandmaster’s clock: size of payload containing timestamp: DWORD</summary>
    GRANDM_DWORD = XCP_SLV_DWORD << 2,
    /// <summary>Grandmaster’s clock: size of payload containing timestamp: DLONG (unsigned interpretation)</summary>
    GRANDM_DLONG = XCP_SLV_DWORD << 3,
    /// <summary>ECU’s clock: size of payload containing timestamp: DWORD</summary>
    ECU_DWORD = XCP_SLV_DWORD << 4,
    /// <summary>ECU’s clock: size of payload containing timestamp: DLONG (unsigned interpretation)</summary>
    ECU_DLONG = XCP_SLV_DWORD << 5,
    /// <summary>When event is sent as response to TRIGGER_INITIATOR 2 or 3; Cluster Identifier and Counter are added to event payload</summary>
    CLUSTER_IDENTIFIER = XCP_SLV_DWORD << 6,
  }
  #endregion
  #region command/response
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdDTOCtrProps : CmdBase
  {
    public DTOCtrModifier Modifier;
    public UInt16 EventChannelNo;
    public UInt16 RelatedEventChannelNo;
    public DTOCtrMode Mode;
    internal CmdDTOCtrProps(DTOCtrModifier modifier
      , UInt16 eventChannelNo
      , UInt16 relatedEventChannelNo
      , DTOCtrMode mode
      ) : base(CommandCode.DtoCtrPproperties)
    { Modifier = modifier; EventChannelNo = eventChannelNo; RelatedEventChannelNo = relatedEventChannelNo; Mode = mode; }
    public override void changeEndianness()
    {
      EventChannelNo = Extensions.changeEndianess(EventChannelNo);
      RelatedEventChannelNo = Extensions.changeEndianess(RelatedEventChannelNo);
    }
  }
  /// <summary>
  /// Response from DTO CTR Properties command.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespDTOCtrResp : RespBase
  {
    public DTOCtrProperties Properties;
    public UInt16 CurrentRelatedEventChannelNo;
    public DTOCtrMode Mode;
    public RespDTOCtrResp() { }
    public override void changeEndianness()
    {
      CurrentRelatedEventChannelNo = Extensions.changeEndianess(CurrentRelatedEventChannelNo);
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdTimeCorrelation : CmdBase
  {
    public TimeCorrSetProps SetProperties;
    public TimeCorrGetPropsReq GetPropertiesReq;
    byte reserved;
    public UInt16 ClusterID;
    public CmdTimeCorrelation(TimeCorrSetProps setProperties, TimeCorrGetPropsReq getPropertiesReq, UInt16 clusterID)
      : base(CommandCode.TimeCorrelationProperties)
    { SetProperties = setProperties; GetPropertiesReq = getPropertiesReq; ClusterID = clusterID; }
    public override void changeEndianness()
    {
      ClusterID = Extensions.changeEndianess(ClusterID);
    }
  }
  /// <summary>
  /// Response from Time Correlation command.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespTimeCorrelation : RespBase
  {
    public SlaveConfig SlaveConfig;
    public ObservableClocks ObservableClocks;
    public SyncState SyncState;
    public ClockInfo ClockInfo;
    byte reserved;
    public UInt16 ClusterID;
    public RespTimeCorrelation() { }
    public override void changeEndianness()
    {
      ClusterID = Extensions.changeEndianess(ClusterID);
    }
  }
  #endregion
}