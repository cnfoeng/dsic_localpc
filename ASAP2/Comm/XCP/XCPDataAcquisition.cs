﻿/*!
 * @file    
 * @brief   Implements the XCP data acquisition stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

#define SIMULATOR_SUPPORT
using jnsoft.ASAP2;
using jnsoft.ASAP2.XCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// DAQ List representation.
  /// 
  /// Holds a dictionary of configured ODTs.
  /// </summary>
  public sealed class DAQListXCP : DAQList
  {
    #region members
    UInt32 mLastDAQCounterValue;
    #endregion
#if SIMULATOR_SUPPORT
    StartStopMode mState;
    ODTDict mODTDict = new ODTDict();
    /// <summary>
    /// Gets the configured ODTs in the DAQ list.
    /// </summary>
    public ODTDict ODTDict { get { return mODTDict; } }
    /// <summary>
    /// Gets or sets the current state of the DAQ list.
    /// </summary>
    public StartStopMode State { get { return mState; } set { mState = value; } }
#endif
    #region contsructors
    /// <summary>
    /// Initializes a new DAQ List with the specified parameters.
    /// </summary>
    /// <param name="daqNo">DAQ Number.</param>
    public DAQListXCP(UInt16 daqNo) { DAQNo = daqNo; }
    /// <summary>
    /// Initializes a new DAQ List with the specified parameters.
    /// 
    /// Algorithm:
    /// 
    /// </summary>
    /// <param name="daqNo">DAQ list number</param>
    /// <param name="maxDTO">Maximum DTO size</param>
    /// <param name="maxODT">Maximum ODTs</param>
    /// <param name="maxODTEntries">Maximum ODT entries</param>
    /// <param name="maxODTEntrySize">Maximum ODT entry size</param>
    /// <param name="dataOffsetFirstPid">Computed offset to in receive values in a DAQ record (first PID)</param>
    /// <param name="dataOffset">Computed offset to receive values in a DAQ record</param>
    /// <param name="absolutePID">Current absolute PID</param>
    /// <param name="measurements">[out] configured measurements will be removed from this list</param>
    /// <param name="odtEntries">[out] configured measurement entries will be added</param>
    public DAQListXCP(UInt16 daqNo
      , UInt16 maxDTO, byte maxODT, byte maxODTEntries, byte maxODTEntrySize
      , int dataOffsetFirstPid, int dataOffset
      , byte absolutePID
      , List<A2LMEASUREMENT> measurements
      , ODTEntryDictType odtEntries
      ) : this(daqNo)
    {
      int maxBytesPerODT = maxDTO - dataOffsetFirstPid;
      int currMeasIdx = measurements.Count - 1; // start from the back of the list

      while (currMeasIdx >= 0)
      { // still measurements to configure
        var meas = measurements[currMeasIdx];
        byte measDataLen = (byte)meas.DataType.getSizeInByte();
        int arraySize = meas.getArraySize();
        byte odtEntrySize = Math.Min(measDataLen, maxODTEntrySize);
        if (!fitsIn(meas, measDataLen, odtEntrySize, arraySize, maxODT, maxODTEntries, maxDTO, dataOffsetFirstPid, dataOffset))
        {
          currMeasIdx--;
          continue;
        }

        uint arrayIdxOffset = 0;
        ODTEntry writtenEntry = null;
        for (int i = 0; i < arraySize; ++i, arrayIdxOffset += measDataLen)
        { // Iterate through the whole measurement block (A2L Arraysize may be > 1)

          uint measAdr = meas.Address + arrayIdxOffset;
          if (odtEntries.TryGetValue(measAdr, out writtenEntry))
            // measurement is already configured, remove it
            break;

          byte partDataOffset = 0;
          for (; partDataOffset < measDataLen; partDataOffset += odtEntrySize)
          { // iterate through the measurement by the maximum ODT entry size
            writtenEntry = null;
            var odtEntry = new ODTEntry(daqNo, meas, (uint)arrayIdxOffset, partDataOffset, odtEntrySize);
            ODTList currODT = null;
            while (null != (currODT = getNextODT(currODT, maxODT, maxODTEntries, maxDTO, dataOffsetFirstPid, dataOffset, odtEntrySize != measDataLen)))
            {
              if (!currODT.fitsIn(odtEntry))
                continue;
              writtenEntry = odtEntry;
              currODT.Add(odtEntry);
              if (partDataOffset == 0)
                // store only complete measurement in the fast access dictionary
                odtEntries[measAdr] = odtEntry;
              break;
            }
          }
        }

        if (null != writtenEntry)
          // measurement completely configured, remove from list
          measurements.RemoveAt(currMeasIdx);
        currMeasIdx--;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Gets or creates the next ODT within this DAQ List.
    /// </summary>
    /// <param name="currODT">The current ODT (maybe null)</param>
    /// <param name="maxODTs">The maximum number of ODTs</param>
    /// <param name="maxBytesPerODT">The maximum number of bytes in an ODT</param>
    /// <param name="maxODTEntries">The maximum number of ODT entries in an ODT</param>
    /// <param name="maxDTO">Maximum DTO size</param>
    /// <param name="dataOffsetFirstPid">Computed offset to in receive values in a DAQ record (first PID)</param>
    /// <param name="dataOffset">Computed offset to receive values in a DAQ record</param>
    /// <param name="appendOnly">true if only the last existing ODT or a new one is valid</param>
    /// <returns>An ODT List instance or null if no more ODTs are available</returns>
    internal ODTList getNextODT(ODTList currODT, byte maxODTs, int maxODTEntries
      , int maxDTO, int dataOffsetFirstPid, int dataOffset
      , bool appendOnly
      )
    {
      int initPID = appendOnly && ODTs.Count > 0 ? ODTs.Keys.Last() : 0;
      byte nextPID = (byte)(currODT == null ? initPID : currODT.PID + 1);
      ODTList result = null;
      if (!ODTs.TryGetValue(nextPID, out result) && ODTs.Count < maxODTs)
      {
        int maxBytesPerODT = maxDTO - (nextPID == 0 ? dataOffsetFirstPid : dataOffset);
        result = ODTs[nextPID] = new ODTList(nextPID, maxBytesPerODT, maxODTEntries);
      }
      return result;
    }
    /// <summary>
    /// Test if the specified measurement fits completely into the current DAQ list.
    /// </summary>
    /// <param name="meas">The measurement</param>
    /// <param name="size">Its single data size</param>
    /// <param name="odtEntrySize">Maybe a smaller ODT entry size (Data type splitting on XCP)</param>
    /// <param name="arraySize">The array size of the measurement</param>
    /// <param name="maxODT">Maximum allowed ODTs to configure</param>
    /// <param name="maxODTEntries">Maximum allowed ODT entries</param>
    /// <param name="maxDTO">Maximum DTO size</param>
    /// <param name="dataOffsetFirstPid">Computed offset to in receive values in a DAQ record (first PID)</param>
    /// <param name="dataOffset">Computed offset to receive values in a DAQ record</param>
    /// <returns>True if the specified measurement fits completely into the current DAQ list.</returns>
    internal bool fitsIn(A2LMEASUREMENT meas, int size, byte odtEntrySize, int arraySize, byte maxODT, int maxODTEntries, int maxDTO, int dataOffsetFirstPid, int dataOffset)
    {
      int freeEntries = 0;
      int freeBytes = 0;
      int factor = size / odtEntrySize;
      foreach (var list in ODTs.Values)
      {
        freeEntries += list.FreeEntries / odtEntrySize;
        freeBytes += list.FreeBytes / odtEntrySize;
      }

      freeEntries += (maxODT - ODTs.Count) * (maxODTEntries / odtEntrySize);

      int freeBytesInEmptyODTs = ODTs.Count == 0
        ? (maxODT - 1) * ((maxDTO - dataOffset) / odtEntrySize) + (maxDTO - dataOffsetFirstPid) / odtEntrySize
        : (maxODT - ODTs.Count) * ((maxDTO - dataOffset) / odtEntrySize);
      freeBytes += freeBytesInEmptyODTs;
      return Math.Min(freeEntries, freeBytes) >= arraySize;
    }
    /// <summary>
    /// Gets the difference time between the last received DAQ ODT frame and the specified frame in [seconds].
    /// 
    /// If the first DAQ ODT is received the difference is returned as zero.
    /// </summary>
    /// <param name="data">The data received</param>
    /// <param name="tsOffset">The byte offset to the time stamp data</param>
    /// <param name="tsSize">Current timestamp size</param>
    /// <param name="tsSupport">A2L XCP timestamp definition</param>
    /// <param name="changeEndianess">true if byte order should be adjusted</param>
    /// <returns>The timestamp in seconds (or double.NaN if no timestamp is supplied)</returns>
    internal double getTimeDiffInSecs(ref byte[] data
      , int tsOffset
      , XCP_TIMESTAMP_SIZE tsSize
      , A2LXCP_TIMESTAMP_SUPPORTED tsSupport
      , bool changeEndianess
      )
    {
      if (tsSize == XCP_TIMESTAMP_SIZE.NotSet || tsSupport == null)
        return double.NaN;

      UInt32 orgValue = 0;
      UInt64 ts = 0;
      switch (tsSize)
      {
        case XCP_TIMESTAMP_SIZE.BYTE: // DAQ counter size is 8 Bit
          orgValue = data[tsOffset];
          ts = orgValue < mLastDAQCounterValue
            ? orgValue + byte.MaxValue - mLastDAQCounterValue // ECU DAQ counter value is overflown -> compute the new difference
            : orgValue - mLastDAQCounterValue;
          break;
        case XCP_TIMESTAMP_SIZE.WORD: // DAQ counter size is 16 Bit
          orgValue = changeEndianess
            ? Extensions.changeEndianess(BitConverter.ToUInt16(data, tsOffset))
            : BitConverter.ToUInt16(data, tsOffset);
          ts = orgValue < mLastDAQCounterValue
            ? orgValue + UInt16.MaxValue - mLastDAQCounterValue // ECU DAQ counter value is overflown -> compute the new difference
            : orgValue - mLastDAQCounterValue;
          break;
        case XCP_TIMESTAMP_SIZE.DWORD: // DAQ counter size is 32 Bit
          orgValue = changeEndianess
            ? Extensions.changeEndianess(BitConverter.ToUInt32(data, tsOffset))
            : BitConverter.ToUInt32(data, tsOffset);
          ts = orgValue < mLastDAQCounterValue
            ? orgValue + UInt32.MaxValue - mLastDAQCounterValue // ECU DAQ counter value is overflown -> compute the new difference
            : orgValue - mLastDAQCounterValue;
          break;
        default:
          return double.NaN;
      }
      ts *= tsSupport.Ticks;
      switch (tsSupport.Resolution)
      {
        case XCP_TIMESTAMP_RESOLUTION._1PS: ts /= 1000000000; break;
        case XCP_TIMESTAMP_RESOLUTION._10PS: ts /= 100000000; break;
        case XCP_TIMESTAMP_RESOLUTION._100PS: ts /= 10000000; break;
        case XCP_TIMESTAMP_RESOLUTION._1NS: ts /= 1000000; break;
        case XCP_TIMESTAMP_RESOLUTION._10NS: ts /= 100000; break;
        case XCP_TIMESTAMP_RESOLUTION._100NS: ts /= 10000; break;
        case XCP_TIMESTAMP_RESOLUTION._1US: ts /= 1000; break;
        case XCP_TIMESTAMP_RESOLUTION._10US: ts /= 100; break;
        case XCP_TIMESTAMP_RESOLUTION._100US: ts /= 10; break;
        case XCP_TIMESTAMP_RESOLUTION._1MS: break;
        case XCP_TIMESTAMP_RESOLUTION._10MS: ts *= 10; break;
        case XCP_TIMESTAMP_RESOLUTION._100MS: ts *= 100; break;
        case XCP_TIMESTAMP_RESOLUTION._1S: ts *= 1000; break;
        default: throw new NotSupportedException(tsSupport.Resolution.ToString());
      }
      if (mLastDAQCounterValue == 0)
      { // first time call, return no difference
        mLastDAQCounterValue = orgValue;
        return 0;
      }
      mLastDAQCounterValue = orgValue;
      return (double)ts / 1000.0;
    }
    /// <summary>
    /// Clears only the contained data, not the DAQ configuration.
    /// </summary>
    public override void clearData()
    {
      mLastDAQCounterValue = 0;
      base.clearData();
    }
    #endregion
  }
  /// <summary>
  /// Holds a dictionary of configured DAQ Lists.
  /// 
  /// Maps a specific DAQ number to the corresponding DAQList.
  /// </summary>
  public sealed class DAQDictXCP : DAQDict
  {
    /// <summary>
    /// Tries to find the corresponding DAQ List from the received DAQ record.
    /// </summary>
    /// <param name="data">The data received</param>
    /// <param name="a2lDAQ">DAQ definition from A2L</param>
    /// <param name="changeEndianess">Indicates if endianess must be changed</param>
    /// <param name="tsOffset">Offset to timestamp</param>
    /// <returns>The DAQ List instance or null if no DAQ List is found</returns>
    internal DAQListXCP findDAQList(ref byte[] data, A2LXCP_DAQ a2lDAQ, bool changeEndianess, out int tsOffset)
    {
      byte pid = data[0];
      tsOffset = 1;
      UInt16 daq = UInt16.MaxValue;
      switch (a2lDAQ.IDField)
      {
        case XCP_ID_FIELD_TYPE.ABSOLUTE:
          var en = GetEnumerator();
          while (en.MoveNext())
          {
            var localDaqList = en.Current.Value;
            int firstAbsPIDNextDAQ = localDaqList.FirstPID + localDaqList.ODTs.Count;
            if (pid >= localDaqList.FirstPID && pid < firstAbsPIDNextDAQ)
              // DAQ List for given PID found
              return (DAQListXCP)localDaqList;
          }
          // not found
          return null;

        case XCP_ID_FIELD_TYPE.BYTE:
          daq = data[1];
          tsOffset = 2;
          break;

        case XCP_ID_FIELD_TYPE.WORD:
          daq = BitConverter.ToUInt16(data, 1);
          if (changeEndianess)
            // adjust byte order, if required
            daq = Extensions.changeEndianess(daq);
          tsOffset = 3;
          break;

        case XCP_ID_FIELD_TYPE.ALIGNED:
          daq = BitConverter.ToUInt16(data, 2);
          if (changeEndianess)
            // adjust byte order, if required
            daq = Extensions.changeEndianess(daq);
          tsOffset = 4;
          break;
      }

      DAQList daqList = null;
      TryGetValue(daq, out daqList);
      return (DAQListXCP)daqList;
    }
    /// <summary>
    /// Tries to configure all specified measurements into the available DAQ Lists.
    /// </summary>
    /// <param name="maxDTO">Maximum DTO size</param>
    /// <param name="maxODTEntrySize">Maximum ODT entry size</param>
    /// <param name="dataOffsetFirstPid">DAQ data offset in the first ODT</param>
    /// <param name="dataOffset">DAQ data offset in the remaining ODTs</param>
    /// <param name="absolutePIDMode">true if the absolute PID mode is required</param>
    /// <param name="daqListInfoDict">DAQ List dictionary from A2L description</param>
    /// <param name="measurements">Measurements to configure in DAQ Lists</param>
    /// <returns>0 if successful, else the count of measurements which could not be configured</returns>
    internal int configure(UInt16 maxDTO, byte maxODTEntrySize
      , int dataOffsetFirstPid, int dataOffset
      , bool absolutePIDMode
      , SortedDictionary<UInt16, DAQAndEvt> daqAndEvtDict
      , List<A2LMEASUREMENT> measurements
      )
    {
      int absolutePID = 0;
      var en = daqAndEvtDict.GetEnumerator();
      while (en.MoveNext())
      {
        if (absolutePIDMode && absolutePID >= (byte)PIDSlaveMaster.SERV)
          // ran out of PIDs in absolute PIDs mode
          break;
        UInt16 daqListNo = en.Current.Key;
        var daqListInfo = en.Current.Value.DAQList;

        // Get a value if the DAQ List is activated
        if (daqListInfo.Active)
        { // DAQ enabled to configure
          var daqList = new DAQListXCP(daqListNo
            , maxDTO, daqListInfo.MaxODT, daqListInfo.MaxODTEntries, maxODTEntrySize
            , dataOffsetFirstPid, dataOffset
            , (byte)(absolutePIDMode ? absolutePID : 0)
            , measurements, mODTEntries
            );
          this[daqListNo] = daqList;
          absolutePID += daqList.ODTs.Count;
        }
        else
          this[daqListNo] = new DAQListXCP(daqListNo);
      }
      return measurements.Count;
    }
#if SIMULATOR_SUPPORT
    /// <summary>
    /// Gets the first PID for the specified DAQ list.
    /// 
    /// This method is only used by the simulator.
    /// </summary>
    /// <param name="daqList">The DAQ list to compute the FirstPID for</param>
    /// <returns>The computed FirstPID</returns>
    public byte getFirstPID(DAQList daqList)
    {
      byte firstPID = 0;
      var en = GetEnumerator();
      while (en.MoveNext())
      {
        var localDAQList = (DAQListXCP)en.Current.Value;
        if (localDAQList.DAQNo == daqList.DAQNo)
          break;
        firstPID += (byte)(localDAQList.ODTDict.Count);
      }
      return firstPID;
    }
#endif
  }

#if SIMULATOR_SUPPORT
  /// <summary>
  /// ECU Simulator: ODT representation.
  /// 
  /// - Holds the list of configured ODTEntries.
  /// </summary>
  public sealed class ODT
  {
    #region members
    byte mPID;
    bool mIsFirstPID;
    List<ODTEntry> mODTEntries = new List<ODTEntry>();
    #endregion
    #region constructors
    /// <summary>
    /// Creates a new ODT instamce with the specified parameters.
    /// </summary>
    /// <param name="pid">The corresponding PID</param>
    /// <param name="isFirstPID">Indicates if the PID is the first PID</param>
    public ODT(byte pid, bool isFirstPID)
    {
      mPID = pid;
      mIsFirstPID = isFirstPID;
    }
    #endregion
    #region properties
    /// <summary>
    /// Indicates if this ODT is the first within the DAQ List.
    /// </summary>
    public bool IsFirstPID { get { return mIsFirstPID; } }
    /// <summary>
    /// Gets the PID.
    /// </summary>
    public byte PID { get { return mPID; } }
    /// <summary>
    /// Gets the configured measurements.
    /// </summary>
    public List<ODTEntry> ODTEntries { get { return mODTEntries; } }
    #endregion
    #region methods
    /// <summary>
    /// Adds the specified measurement to the ODT.
    /// </summary>
    /// <param name="measurement">The measurement to configure in the ODT</param>
    /// <param name="length">The length of the measurement</param>
    public void addMeasurement(A2LMEASUREMENT measurement, int length)
    {
      mODTEntries.Add(new ODTEntry(0, measurement, 0, 0, (byte)measurement.DataType.getSizeInByte()));
    }
    #endregion
  }
  /// <summary>
  /// ECUSimulator: Dictionary mapping PIDs to ODTs.
  /// </summary>
  public sealed class ODTDict : SortedDictionary<byte, ODT> { }
#endif
}