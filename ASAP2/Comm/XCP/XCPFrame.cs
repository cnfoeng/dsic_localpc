﻿/*!
 * @file    
 * @brief   Implements the XCP frame.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Runtime.InteropServices;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Represents an XCP frame.
  /// 
  /// Implements some static helper methds to convert 
  /// - object references into byte arrays 
  /// - byte array into object references
  /// </summary>
  public sealed class XCPFrame : Frame
  {
    #region members
    public const int ResponseIndex = 0;
    public const int CmdIndex = 0;
    public const int EventCodeIndex = 1;
    const string mCSVFormat = "{0};\"{1}\";{2};\"{3}\";\"{4}\";\"{5}\";\"{6}\"";
    const string mClpFormat = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}";
    string mSource = string.Empty;
    #endregion
    #region constructors
    /// <summary>
    /// Initialises a new instance of XCPFrame with the specified parameters.
    /// </summary>
    /// <param name="type">The XCP type</param>
    /// <param name="source">The source from where the frame is sent/received</param>
    /// <param name="data">The complete frame data as byte array</param>
    /// <param name="isMasterFrame">true if the frame is sent by the master, else false</param>
    public XCPFrame(XCPType type, string source, ref byte[] data, bool isMasterFrame)
    {
      Type = type;
      mSource = source;
      IsMasterFrame = isMasterFrame;

      switch (Type)
      {
        case XCPType.CAN:
          // XCP on CAN length and Ctr = 0 bytes
          Data = data;
          Len = (ushort)data.Length;
          break;
        case XCPType.UDP:
        case XCPType.TCP:
          // XCP on Ethernet length and Ctr = 4 bytes
          int lenCtrBytes = 4;
          // Copy payload
          int dataLen = data.Length - lenCtrBytes;
          Data = new byte[dataLen];
          Array.Copy(data, lenCtrBytes, Data, 0, dataLen);
          // set length and counter
          Len = BitConverter.ToUInt16(data, 0);
          Ctr = BitConverter.ToUInt16(data, 2);
          if (!BitConverter.IsLittleEndian)
          { // change endianess if host is not a little endian machine
            Len = Extensions.changeEndianess(Len);
            Ctr = Extensions.changeEndianess(Ctr);
          }
          break;
      }
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the XCP type of the frame.
    /// </summary>
    public XCPType Type { get; private set; }
    /// <summary>
    /// The Length value of the frame
    /// </summary>
    public UInt16 Len { get; private set; }
    /// <summary>
    /// The Counter value of the frame
    /// </summary>
    public UInt16 Ctr { get; private set; }
    /// <summary>
    /// The address sent or received from.
    /// </summary>
    public string Address
    {
      get
      {
        return string.IsNullOrEmpty(mSource)
          ? Type.ToString()
          : $"{(IsMasterFrame ? "\u2192" : "\u2190")} {Type} {mSource}";
      }
    }
    /// <summary>
    /// Indicates, if the frame is a data acquisition frame.
    /// </summary>
    public bool IsDAQ { get { return !IsMasterFrame && Data.Length > 0 && Data[0] < (byte)PIDSlaveMaster.SERV; } }
    /// <summary>
    /// Indicates if the frame is an error response frame.
    /// </summary>
    public bool IsError { get { return !IsMasterFrame && Data.Length > 0 && Data[0] == (byte)PIDSlaveMaster.ERR; } }
    #endregion
    #region methods
    /// <summary>
    /// Gets the frame's type.
    /// </summary>
    /// <returns>returns a type string representation</returns>
    public string getTypeStr()
    {
      string result = string.Empty;
      if (IsMasterFrame)
        result = ((CommandCode)Data[0]).ToString();
      else if (IsDAQ)
        result = "DAQ";
      else if (Data.Length > 0)
      {
        PIDSlaveMaster response = (PIDSlaveMaster)Data[0];
        switch (response)
        {
          case PIDSlaveMaster.ERR:
            if (Data.Length > 1)
            {
              result = $"{response}({(CmdResult)Data[1]})";
              break;
            }
            else
              goto default;
          case PIDSlaveMaster.EV:
            if (Data.Length > 1)
            {
              result = $"{response}({(EventCodes)Data[1]})";
              break;
            }
            else
              goto default;
          default:
            result = response.ToString();
            break;
        }
      }
      else
        result = "Unknown";
      return result;
    }
    public override string ToString()
    {
      string frameInd;
      byte first = (byte)Data[0];
      if (IsMasterFrame)
        // commands
        frameInd = ((CommandCode)first).ToString();
      else
      { // responses
        if (first >= (byte)PIDSlaveMaster.SERV)
          frameInd = ((PIDSlaveMaster)first).ToString();
        else
          frameInd = $"DAQ {first:X2}";
      }
      return $"{getTimeStr()} - Len:{Len}, Ctr:{Ctr}, {frameInd} from {Address}";
    }
    public override string toCSV()
    {
      return string.Format(mCSVFormat
        , getTimeStr()
        , Address
        , Data.Length
        , Type != XCPType.CAN ? Ctr.ToString() : "-"
        , getTypeStr()
        , getDataStr()
        , getDataASCIIStr('"')
        );
    }
    public override string toClipboard()
    {
      return string.Format(mClpFormat
        , getTimeStr()
        , Address
        , Data.Length
        , Type != XCPType.CAN ? Ctr.ToString() : "-"
        , getTypeStr()
        , getDataStr()
        , getDataASCIIStr('\t')
        );
    }
    /// <summary>
    /// Gets the frame length in Bits on the Bus depending on the used XCP media type.
    /// </summary>
    /// <returns>The frame length in Bits on the bus</returns>
    public int getRawFrameLength()
    {
      int len = 0;
      if (IsMasterFrame)
        len = Data.Length;
      else
      {
        switch (Type)
        {
          case XCPType.UDP:
          case XCPType.TCP:
            // Add uint16 CTR+ uint16 LEN
            len = 4 + Data.Length;
            break;
          case XCPType.CAN:
            len = Data.Length;
            break;
          default:
            throw new NotImplementedException();
        }
      }
      return len * 8;
    }
    /// <summary>
    /// Creates the byte array from the specified object to send over the network.
    /// </summary>
    /// <typeparam name="T">The sending object type</typeparam>
    /// <param name="type">The XCP type</param>
    /// <param name="Ctr">The XCP counter</param>
    /// <param name="objectToSend">The object to send</param>
    /// <param name="additionalData">Aditional dynamic data to send (may be null)</param>
    /// <returns>The supplied object as a byte array</returns>
    public static byte[] toByteArray<T>(XCPType type, UInt16 Ctr, T objectToSend, byte[] additionalData = null) where T : class
    {
      var mem = IntPtr.Zero;
      int marshalSize = (UInt16)Marshal.SizeOf(typeof(T));
      int additionalDataLen = (additionalData != null ? additionalData.Length : 0);
      int lenCtrSize = 0;
      switch (type)
      {
        case XCPType.TCP:
        case XCPType.UDP:
          // add length and ctr
          lenCtrSize = 4;
          break;
      }
      int fullSize = lenCtrSize + marshalSize + additionalDataLen;
      var frame = new byte[fullSize];
      switch (type)
      {
        case XCPType.UDP:
        case XCPType.TCP:
          // Set the size bytes
          var bytesSize = BitConverter.GetBytes((UInt16)(marshalSize + additionalDataLen));
          var bytesCtr = BitConverter.GetBytes(Ctr);
          if (BitConverter.IsLittleEndian)
          { // host byte order is little endian
            frame[0] = bytesSize[0];
            frame[1] = bytesSize[1];
            // Set the ctr bytes
            frame[2] = bytesCtr[0];
            frame[3] = bytesCtr[1];
          }
          else
          { // host byte order is big endian
            frame[0] = bytesSize[1];
            frame[1] = bytesSize[0];
            // Set the ctr bytes
            frame[2] = bytesCtr[1];
            frame[3] = bytesCtr[0];
          }
          break;
      }

      try
      {
        mem = Marshal.AllocHGlobal(marshalSize);
        Marshal.StructureToPtr(objectToSend, mem, true);
        if (lenCtrSize > 0)
        {
          byte[] result = new byte[marshalSize];
          Marshal.Copy(mem, result, 0, marshalSize);
          // copy result into the frame
          Array.Copy(result, 0, frame, lenCtrSize, marshalSize);
        }
        else
          Marshal.Copy(mem, frame, 0, marshalSize);

        // copy additional data into the frame
        if (additionalData != null)
          Array.Copy(additionalData, 0, frame, lenCtrSize + marshalSize, additionalDataLen);
      }
      finally
      { // force to free the global memory
        if (mem != IntPtr.Zero)
          Marshal.FreeHGlobal(mem);
      }
      return frame;
    }
    #endregion
  }
}