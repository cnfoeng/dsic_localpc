﻿/*!
 * @file    
 * @brief   Implements the XCP Master base to communicate with a XCP Slave.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2;
using jnsoft.ASAP2.XCP;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// EventArgs class delivering received events.
  /// 
  /// To access data For specific events, 
  /// simply cast the ReceivedEvent according the corresponding EventCodes.
  /// </summary>
  public sealed class XCPEventArgs : EventArgs
  {
    /// <summary>Gets the received XCP event.</summary>
    public EventBase ReceivedEvent { get; private set; }
    internal XCPEventArgs(EventBase receivedEvent) { ReceivedEvent = receivedEvent; }
  }
  /// <summary>
  /// ErrorArgs class delivering received command errors.
  /// </summary>
  public sealed class XCPErrorArgs : EventArgs
  {
    /// <summary>Gets the corresponding XCP command.</summary>
    public CommandCode Command { get; private set; }
    /// <summary>Gets the received XCP result code.</summary>
    public CmdResult ErrorCode { get; private set; }
    internal XCPErrorArgs(CmdResult errorCode, CommandCode command) { ErrorCode = errorCode; Command = command; }
  }
  /// <summary>
  /// EventArgs class delivering received events.
  /// 
  /// To access data For specific events, 
  /// simply cast the ReceivedEvent according the corresponding EventCodes.
  /// </summary>
  public sealed class XCPServiceRequestArgs : EventArgs
  {
    /// <summary>Gets the received XCP event.</summary>
    public RespService ReceivedServiceRequest { get; private set; }
    internal XCPServiceRequestArgs(RespService receivedSrvReq) { ReceivedServiceRequest = receivedSrvReq; }
  }
  /// <summary>
  /// Implements the XCP Master base communication with a XCP Slave.
  /// 
  /// - All commands are implemented as synchronous calls to the slave
  /// - All commands are thread-safe
  /// - The endianess of any command, response or event is converted 
  /// automatically according the XCP protocol
  /// - Responses (out parameter) of commands are only valid if the 
  /// CmdResult of the call is CmdResult.OK
  /// </summary>
  public abstract class XCPMasterBase : CommMaster
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(XCPMasterBase));
    A2LXCP_PROTOCOL_LAYER mProtocolLayer;
    internal UInt16 Ctr;
    /// <summary>
    /// Address granularity (only valid if connected).
    /// </summary>
    protected int mAG;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a XCPonEth (TCP pr UDP) communication master.
    /// </summary>
    /// <param name="connectBehavior">Client's connect behaviour</param>
    /// <param name="type">Client's type</param>
    /// <param name="remoteAddress">The target address</param>
    /// <param name="remotePort">The target port</param>
    /// <param name="protocolLayer">XCP ProtocolLayer</param>
    /// <exception cref="ArgumentException">Thrown, if the remoteAddress is not resolvable</exception>
    public XCPMasterBase(ConnectBehaviourType connectBehavior
      , XCPType type
      , string remoteAddress
      , int remotePort
      , A2LXCP_PROTOCOL_LAYER protocolLayer
      )
      : base(connectBehavior, (A2LIF_DATA)protocolLayer.Parent)
    {
      Type = type;
      Port = remotePort;
      mProtocolLayer = protocolLayer;
      try
      {
        Address = IPAddress.Parse(remoteAddress);
      }
      catch (FormatException)
      { // occurs if specified address isn't an IP Address
      }

      if (null == Address)
      { // try to get IP addresses from the dns server
        try
        {
          var addresses = Dns.GetHostAddresses(remoteAddress);
          foreach (var address in addresses)
            if (address.AddressFamily == AddressFamily.InterNetwork)
            { // IP address found
              Address = address;
              break;
            }
        }
        catch { }
      }

      if (Address == null)
        throw new ArgumentException($"Unknown host {remoteAddress}:{remotePort}");
      SlaveAddress = $"{Address}:{remotePort}";
    }
    /// <summary>
    /// Creates a XCPonCAN communication master.
    /// </summary>
    /// <param name="connectBehavior">Client's connect behaviour</param>
    /// <param name="xcpCAN">XCPonCAN definition</param>
    /// <param name="protocolLayer">XCP ProtocolLayer</param>
    /// <param name="canProviderName">The CAN provider name</param>
    public XCPMasterBase(ConnectBehaviourType connectBehavior
      , A2LXCP_ON_CAN xcpCAN
      , A2LXCP_PROTOCOL_LAYER protocolLayer
      , string canProviderName
      )
      : base(connectBehavior, (A2LIF_DATA)protocolLayer.Parent)
    {
      Type = XCPType.CAN;
      mProtocolLayer = protocolLayer;
      SlaveAddress = $"{canProviderName} {CANProvider.toCANIDString(xcpCAN.CANIDResp)}";
    }
    #endregion
    #region properties
    /// <summary>Gets the type of the XCP instance.</summary>
    public XCPType Type { get; private set; }
    /// <summary>Gets the remote port used to connect to the XCP slave device.</summary>
    public int Port { get; private set; }
    /// <summary>Gets the remote address used to connect to the XCP slave device.</summary>
    public IPAddress Address { get; private set; }
    /// <summary>Gets the timing settings used to connect to the XCP slave device.</summary>
    public UInt16[] Timings { get { return mProtocolLayer.Timings; } }
    /// <summary>MaxCTO (only valid if connected).</summary>
    public byte MaxCTO { get; private set; }
    /// <summary>MaxDTO (only valid if connected).</summary>
    public UInt16 MaxDTO { get; private set; }
    /// <summary>Last received response.</summary>
    public XCPFrame LastResponse { get; internal set; }
    /// <summary>The last error occured.</summary>
    public CmdResult LastErrorResponse { get; private set; } = CmdResult.OK;
    public sealed override string LastErrorText { get { return LastErrorResponse.ToString(); } }
    public sealed override Enum LastErrorEnum { get { return LastErrorResponse; } }
    #endregion
    #region methods
    protected internal sealed override void increaseCtr(int bitLength)
    {
      base.increaseCtr(bitLength);
      ++Ctr;
    }
    /// <summary>
    /// Called from the XCPKernel if the connection state gets changed.
    /// </summary>
    /// <param name="respConnect">A valid connect response instance if the device gets connected, null if the device gets disconnected.</param>
    internal override void setConnectionState<T>(T respConnect)
    {
      var connect = respConnect as RespConnect;
      LastStateChange = DateTime.Now;
      if (connect != null)
      { // connect received
        SlaveConnected = true;
        ChangeEndianess = BitConverter.IsLittleEndian != ((connect.CommModeBasic & CommModeBasic.BigEndian) == 0);
        if (ChangeEndianess)
          // turn endianess if required
          respConnect.changeEndianness();
        mAG = (connect.CommModeBasic & CommModeBasic.AddressGranularityDWORD) > 0
          ? 4
          : (connect.CommModeBasic & CommModeBasic.AddressGranularityWORD) > 0
            ? 2
            : 1;
        // minimum between A2L and ECU response, at least 8 Bytes
        MaxDTO = Math.Max((UInt16)8, Math.Min(mProtocolLayer.MaxDTO, connect.MaxDTO));
        // minimum between A2L and ECU response, at least 8 Bytes
        MaxCTO = Math.Max((byte)8, Math.Min(mProtocolLayer.MaxCTO, connect.MaxCTO));

        ServicesReceived =
        EventsReceived =
        FramesSent =
        FramesReceived = 
        ErrorsReceived = 0;
      }
      else
      { // disconnected
        SlaveConnected = false;
        LastReceivedTime = DateTime.MinValue;
        LastErrorResponse = CmdResult.ERR_TIMEOUT;
        mAG = 0;
        MaxCTO = 0;
        MaxDTO = 0;
        ChangeEndianess = false;
        LastResponse = null;
        ReadyToSend = true;
      }
    }
    /// <summary>
    /// Called, if an error response is received from the slave.
    /// </summary>
    /// <param name="result">The command result</param>
    /// <param name="code">The corresponding command code</param>
    public virtual void onErrorReceived(CmdResult result, CommandCode code)
    {
      ++ErrorsReceived;
      LastErrorResponse = result;
    }
    /// <summary>
    /// Called if an event is received from the slave.
    /// </summary>
    /// <param name="xcpFrame">The corresponding received frame</param>
    public virtual void onEventReceived(XCPFrame xcpFrame)
    {
      ++EventsReceived;
    }
    /// <summary>
    /// Called if an service request is received from the slave.
    /// </summary>
    /// <param name="xcpFrame">The corresponding received frame</param>
    public virtual void onServiceReceived(XCPFrame xcpFrame)
    {
      ++ServicesReceived;
    }
    /// <summary>
    /// Called if a DAQ frame is received from the slave.
    /// </summary>
    /// <param name="xcpFrame">The corresponding received frame</param>
    public virtual void onDAQFrameReceived(XCPFrame xcpFrame)
    {
    }
    internal virtual void onValidResponseReceived(CommandCode cmdCode, RespBase response)
    {
    }
    /// <summary>
    /// Gets the count of required padding bytes.
    /// </summary>
    /// <param name="ag">The address granularity</param>
    /// <returns>the count of required padding bytes.</returns>
    public static int getPaddingSize(int ag)
    {
      return ag == 4 ? 3 : ag == 2 ? 1 : 0;
    }
    internal override int getAliveCycleTime()
    {
      return mProtocolLayer.Timings[0] + mProtocolLayer.Timings[1];
    }
    public sealed override void resetLastError() { LastErrorResponse = CmdResult.OK; }
    #region xcp commands
    /// <summary>
    /// Set up connection with slave.
    /// 
    /// This command establishes a continuous, logical, point-to-point connection with a slave device. 
    /// During a running XCP session (CONNECTED) this command has no influence on any 
    /// configuration of the XCP slave driver. 
    /// 
    /// A slave device does not respond to any other commands (except auto detection) unless it is 
    /// in the state CONNECTED. 
    /// </summary>
    /// <param name="mode">The connect mode</param>
    /// <param name="response">The connect response</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Connect(ConnectMode mode, out RespConnect response)
    {
      var cmd = new CmdConnect(mode);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      var result = CommKernel.sendFrame(this, data, out response);
      setConnectionState(response);
      if (CmdResult.OK == result)
        LastReceivedTime = DateTime.Now;
      return result;
    }
    internal sealed override bool internalConnect(ConnectMode mode, out int maxDTO)
    {
      RespConnect response = null;
      maxDTO = 0;
      var result = Connect(mode, out response);
      if (CmdResult.OK == result)
        maxDTO = response.MaxDTO;
      return CmdResult.OK == result;
    }
    internal sealed override bool internalGetStatus()
    {
      RespGetStatus respGetStatus;
      return CmdResult.OK == GetStatus(out respGetStatus);
    }
    /// <summary>
    /// Disconnect from slave.
    /// 
    /// Brings the slave to the “DISCONNECTED”state. The DISCONNECTED”
    /// state is described in Part 1, chapter “state machine”.
    /// </summary>
    /// <returns>true if successful</returns>
    public override bool Disconnect()
    {
      CmdResult result = CmdResult.OK;
      if (SlaveConnected)
      {
        var cmd = new CmdDisconnect();
        var data = XCPFrame.toByteArray(Type, Ctr, cmd);
        RespBase respDisconnect;
        result = CommKernel.sendFrame(this, data, out respDisconnect);
      }
      CommKernel.setDisconnect(this);
      return result == CmdResult.OK;
    }
    /// <summary>
    /// Get current session status from slave.
    /// 
    /// This command returns all current status 
    /// information of the slave device. This includes the
    /// status of the resource protection, pending store requests 
    /// and the general status of data acquisition and stimulation.
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetStatus(out RespGetStatus response)
    {
      var cmd = new CmdBase(CommandCode.GetStatus);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      var result = CommKernel.sendFrame(this, data, out response);
      if (CmdResult.OK == result)
        LastReceivedTime = DateTime.Now;
      return result;
    }
    /// <summary>
    /// Synchronize command execution after time-out.
    /// 
    /// This command is used to synchronize command execution after timeout conditions. The 
    /// SYNCH command will always have a negative response with the error code 
    /// ERR_CMD_SYNCH. There is no other command using this error code, therefore the 
    /// response to a SYNCH command may be distinguished from the response to any other 
    /// command. 
    /// </summary>
    /// <returns>The corresponding command result</returns>
    public CmdResult Synch()
    {
      var cmd = new CmdBase(CommandCode.Synch);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespError response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get current session status from slave.
    /// 
    /// This command returns optional information on different 
    /// Communication Modes supported by the slave.
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetCommModeInfo(out RespGetCommModeInfo response)
    {
      var cmd = new CmdBase(CommandCode.GetCommModeInfo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get identification from slave.
    /// 
    /// This command is used for automatic session configuration and for slave device identification.
    /// </summary>
    /// <param name="idType">The request type</param>
    /// <param name="response">may be null if mode is not supported!</param>
    /// <param name="respData">The uploaded data (only if the TRANSFER_MODE bit in RespGetID.Mode is set)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetID(GetIDType idType, out RespGetID response, out byte[] respData)
    {
      var cmd = new CmdGetID(idType);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response, out respData);
    }
    /// <summary>
    /// Request to save to non-volatile memory
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="sessionId">Session configuration id </param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetRequest(SetRequestMode mode, UInt16 sessionId)
    {
      var cmd = new CmdSetRequest(mode, sessionId);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get seed for unlocking a protected resource
    /// </summary>
    /// <param name="seedMode">see XCP specification</param>
    /// <param name="resource">see XCP specification</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <param name="seed">The seed bytes (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSeed(SeedModeType seedMode, ResourceType resource, out RespGetSeed response, out byte[] seed)
    {
      var cmd = new CmdGetSeed(seedMode, resource);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response, out seed);
    }
    /// <summary>
    /// Send key for unlocking a protected resource.
    /// 
    /// Call this method in consecutive calls until offset == key.Length!
    /// </summary>
    /// <param name="key">The computed key from the seed & key DLL</param>
    /// <param name="offset">The current offset (must be 0 on the first call!)</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Unlock(byte[] key, ref int offset, out RespUnlock response)
    {
      byte maxSend = (byte)Math.Min(key.Length - offset, MaxCTO - 2);
      var sendKey = new byte[maxSend];
      Array.Copy(key, offset, sendKey, 0, maxSend);
      var cmd = new CmdUnlock((byte)(key.Length - offset));
      offset += maxSend;
      var data = XCPFrame.toByteArray(Type, Ctr, cmd, sendKey);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set Memory Transfer Address in slave.
    /// 
    /// This command will initialize a pointer (32Bit address + 8Bit extension) 
    /// for following memory transfer commands.
    /// </summary>
    /// <param name="addressExtension"></param>
    /// <param name="address">The </param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetMTA(byte addressExtension, UInt32 address)
    {
      var cmd = new CmdSetMTA(addressExtension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Upload from slave to master.
    /// 
    /// A data block of the specified length, starting at the 
    /// current MTA, will be returned. The MTA will be post-incremented 
    /// by the given number of data elements.
    /// 
    /// The use of the proper address granularity is done automatically.
    /// </summary>
    /// <param name="numberOfElements">The count of elements to upload</param>
    /// <param name="respData">The uploaded data</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Upload(byte numberOfElements, out byte[] respData)
    {
      respData = null;
      if (numberOfElements == 0)
        return CmdResult.ERR_INVALID_ARGUMENT;

      var cmd = new CmdUpload(numberOfElements);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      var result = CommKernel.sendFrame(this, data, out response, out respData
        , -1, (byte)Math.Min(byte.MaxValue, numberOfElements + 1)
        );
      if (CmdResult.OK != result)
        return result;

      int padding = getPaddingSize(mAG);
      if (padding > 0)
      { // support for address granularity WORD/DWORD
        var resultingData = new byte[numberOfElements * mAG];
        Array.Copy(respData, padding, resultingData, 0, resultingData.Length);
        respData = resultingData;
      }
      return result;
    }
    /// <summary>
    /// Upload from slave to master (short version).
    /// 
    /// A data block of the specified length, starting at address 
    /// will be returned. The MTA pointer is set to the first data 
    /// byte behind the uploaded data block. The error handling and 
    /// the response structure is identical to the UPLOAD command. 
    /// 
    /// The use of the proper address granularity is done automatically.
    /// </summary>
    /// <param name="numberOfElements">The count of elements to upload</param>
    /// <param name="addressExtension"></param>
    /// <param name="address">The address to upload from</param>
    /// <param name="respData">The uploaded data</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ShortUpload(byte numberOfElements, byte addressExtension, UInt32 address, out byte[] respData)
    {
      respData = null;
      if (numberOfElements == 0)
        return CmdResult.ERR_INVALID_ARGUMENT;

      var cmd = new CmdShortUpload(numberOfElements, addressExtension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      var result = CommKernel.sendFrame(this, data, out response, out respData);
      int padding = getPaddingSize(mAG);
      if (result != CmdResult.OK || padding == 0)
        return result;
      // support for address granularity WORD/DWORD
      var resultingData = new byte[numberOfElements * mAG];
      Array.Copy(respData, padding, resultingData, 0, resultingData.Length);
      respData = resultingData;
      return result;
    }
    /// <summary>
    /// Modify Bits on slave.
    /// 
    /// The 32 Bit memory location A referred by the MTA will be modified using the formula below:
    /// <code>A = ((A) & ((~((UInt32) (((UInt16)~MA) << S)))) ^ ((UInt32)(MX << S)))</code>
    /// 
    /// The AND Mask(MA) specifies all the bits of A which have to be set to “0” by setting the
    /// corresponding bit in MA to “0” and all untouched bits to “1”.
    /// The XOR Mask(MX) specifies all bits of A which has to be toggled by setting the
    /// corresponding bit in MX to “1” and all untouched bits to “0”.
    /// To set bit 0 to “0”, use MA = 0xFFFE and MX = 0x0000.
    /// To set bit 0 to “1” first set it to “0” and then toggle it, so MA = 0xFFFE and MX = 0x0001.
    /// Via the masks MA and MX it is only possible to access a 16 bit wide memory location.
    /// Thus the shift parameter S is used to move both masks together with the specified
    /// number of bits into the more significant direction.
    /// </summary>
    /// <param name="s">Shift Value (S)</param>
    /// <param name="ma">AND Mask (MA)</param>
    /// <param name="mx">XOR Mask (MX)</param>
    /// <returns></returns>
    public CmdResult ModifyBits(byte s, UInt16 ma, UInt16 mx)
    {
      var cmd = new CmdModifyBits(s, ma, mx);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Build checksum over memory range.
    /// 
    /// Returns a checksum result of the memory block that is 
    /// defined by the MTA and Block size. The MTA will be 
    /// post-incremented by the block size
    /// </summary>
    /// <param name="blockSize">The block size</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult BuildChecksum(UInt32 blockSize, out RespBuildChecksum response)
    {
      var cmd = new CmdBuildChecksum(blockSize);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Download from master to slave.
    /// 
    /// The data block of the specified length (size) contained 
    /// in the CMD will be copied into memory, starting at the MTA. 
    /// The MTA will be post-incremented by the number of data elements. 
    /// </summary>
    /// <param name="noOfDataElements">The number of data elements</param>
    /// <param name="bytes">The data bytes to download</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Download(byte noOfDataElements, byte[] bytes)
    {
      return WriteBytes(CommandCode.Download, ref bytes, noOfDataElements);
    }
    /// <summary>
    /// Download from master to slave.
    /// 
    /// This command is used to transmit consecutive data elements for 
    /// the DOWNLOAD command in block transfer mode.
    /// </summary>
    /// <param name="noOfDataElements">The number of data elements</param>
    /// <param name="bytes">The data bytes to download</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult DownloadNext(byte noOfDataElements, byte[] bytes)
    {
      return WriteBytes(CommandCode.DownloadNext, ref bytes, noOfDataElements);
    }
    /// <summary>
    /// Download from master to slave (fixed size).
    /// </summary>
    /// <param name="bytes">The data bytes to download</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult DownloadMax(byte[] bytes)
    {
      return WriteBytes(CommandCode.DownloadMax, ref bytes);
    }
    /// <summary>
    /// Download from master to slave.
    /// </summary>
    /// <param name="bytes">The data bytes to download</param>
    /// <param name="addressExtension"></param>
    /// <param name="address"></param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of bytes does not fit with the current address granularity</exception>
    public CmdResult ShortDownload(byte[] bytes, byte addressExtension, UInt32 address)
    {
      var cmd = new CmdShortDownload((byte)bytes.Length, addressExtension, address);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd, bytes);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set calibration page
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="segmentNo">Logical data segment number</param>
    /// <param name="pageNo">Logical data page number</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetCalPage(CalPageMode mode, byte segmentNo, byte pageNo)
    {
      var cmd = new CmdSetCALPage(mode, segmentNo, pageNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase respError;
      return CommKernel.sendFrame(this, data, out respError);
    }
    /// <summary>
    /// Get calibration page
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="segmentNo">Logical data segment number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetCalPage(CalPageMode mode, byte segmentNo, out RespGetCALPage response)
    {
      var cmd = new CmdGetCALPage(mode, segmentNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get general information on PAG processor
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetPAGProcessorInfo(out RespGetPAGProcessorInfo response)
    {
      var cmd = new CmdBase(CommandCode.GetPAGProcessorInfo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a SEGMENT
    /// 
    /// Mode: get standard info for this SEGMENT
    /// </summary>
    /// <param name="mappingMode">The mapping mode</param>
    /// <param name="segmentNo">The segment number</param>
    /// <param name="mappingIndex">The mapping index</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSegmentInfoMapping(MappingInfoModeType mappingMode, byte segmentNo, byte mappingIndex, out RespGetSegmentInfoAddress response)
    {
      var cmd = new CmdGetSegmentInfo(mappingMode, segmentNo, mappingIndex);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a SEGMENT
    /// 
    /// Mode: get basic address info for this SEGMENT.
    /// </summary>
    /// <param name="basicAddressMode">see XCP specification</param>
    /// <param name="segmentNo">The segment number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSegmentInfoAddress(BasicAddressModeType basicAddressMode, byte segmentNo, out RespGetSegmentInfoAddress response)
    {
      var cmd = new CmdGetSegmentInfo(basicAddressMode, segmentNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a SEGMENT
    /// 
    /// Mode: get address mapping info for this SEGMENT
    /// </summary>
    /// <param name="segmentNo">The segment number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSegmentInfo(byte segmentNo, out RespGetSegmentInfo response)
    {
      var cmd = new CmdGetSegmentInfo(segmentNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a PAGE
    /// </summary>
    /// <param name="segmentNo">The segment number</param>
    /// <param name="pageNo">The page number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetPageInfo(byte segmentNo, byte pageNo, out RespGetPageInfo response)
    {
      var cmd = new CmdGetPageInfo(segmentNo, pageNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set mode for a SEGMENT
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="segmentNo">The segment number</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetSegmentMode(SegmentMode mode, byte segmentNo)
    {
      var cmd = new CmdSetSegmentMode(mode, segmentNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get mode for a SEGMENT
    /// </summary>
    /// <param name="segmentNo">The segment number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSegmentMode(byte segmentNo, out RespGetSegmentMode response)
    {
      var cmd = new CmdGetSegmentMode(segmentNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Copy page
    /// </summary>
    /// <param name="srcSegmentNo">Logical data segment number source</param>
    /// <param name="srcPageNo">Logical data page number source</param>
    /// <param name="dstSegmentNo">Logical data segment number destination</param>
    /// <param name="dstPageNo">Logical data page number destination</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult CopyCalPage(byte srcSegmentNo, byte srcPageNo, byte dstSegmentNo, byte dstPageNo)
    {
      var cmd = new CmdCopyCalPage(srcSegmentNo, srcPageNo, dstSegmentNo, dstPageNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set pointer to ODT entry
    /// </summary>
    /// <param name="DAQListNo">DAQ list number</param>
    /// <param name="ODTNo">ODT number</param>
    /// <param name="ODTEntryNo">ODT entry number</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetDAQPtr(UInt16 DAQListNo, byte ODTNo, byte ODTEntryNo)
    {
      var cmd = new CmdSetDAQPtr(DAQListNo, ODTNo, ODTEntryNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Write element in ODT entry
    /// </summary>
    /// <param name="bitOffset">see XCP specification</param>
    /// <param name="elementSize">see XCP specification</param>
    /// <param name="addressExtension">see XCP specification</param>
    /// <param name="address">see XCP specification</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult WriteDAQ(byte bitOffset, byte elementSize, byte addressExtension, UInt32 address)
    {
      var cmd = new CmdWriteDAQ(bitOffset, elementSize, addressExtension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Write multiple elements in ODT.
    /// 
    /// This command is used to write consecutive 
    /// ODT entries to a DAQ list defined by the 
    /// DAQ list pointer (see SET_DAQ_PTR).
    /// </summary>
    /// <param name="entries">The list of ODT ntries</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult WriteDAQMultiple(ODTEntry[] entries)
    {
      var localEntries = new List<ODTEntry>(entries);
      const int recordSize = 8;
      int maxPayload = MaxCTO - 2 - recordSize;
      if (maxPayload < 0)
        // MaxCTO is too small to use this command
        return CmdResult.ERR_CMD_UNKNOWN;
      var payLoad = new List<byte>();

      payLoad.Add((byte)entries.Length);

      int i = 0;
      CmdResult result = CmdResult.OK;
      while (i < localEntries.Count)
      {
        var entry = localEntries[i];

        // write DAQ record
        payLoad.Add(entry.BitOffset);
        payLoad.Add((byte)entry.Measurement.DataType.getSizeInByte());
        payLoad.AddRange(BitConverter.GetBytes(ChangeEndianess
          ? Extensions.changeEndianess(entry.Address)
          : entry.Address
          ));
        payLoad.Add((byte)entry.Measurement.AddressExtension);

        // Add dummy byte
        payLoad.Add((byte)0);

        ++i;
        if (payLoad.Count >= maxPayload || i == localEntries.Count)
        {
          payLoad[0] = (byte)i;
          // Create and send the WriteDAQMultiple cmd
          var cmd = new CmdWriteDAQMultiple();
          var data = XCPFrame.toByteArray(Type, Ctr, cmd, payLoad.ToArray());
          RespBase response;
          result = CommKernel.sendFrame(this, data, out response);
          if (result != CmdResult.OK)
            return result;
          payLoad.RemoveRange(1, payLoad.Count - 1);
          localEntries.RemoveRange(0, i);
          i = 0;
        }
      }
      return result;
    }
    /// <summary>
    /// Set mode for DAQ list
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="daqListNo">DAQ list number</param>
    /// <param name="eventChannelNo">Event channel number</param>
    /// <param name="prescaler">see XCP specification</param>
    /// <param name="priority">see XCP specification</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetDAQListMode(DAQListMode mode, UInt16 daqListNo, UInt16 eventChannelNo, byte prescaler, byte priority)
    {
      var cmd = new CmdSetDAQListMode(mode, daqListNo, eventChannelNo, prescaler, priority);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Start/stop/select DAQ list
    /// 
    /// - According to the specification, the FirstPID paramter in the response could be ignored in some cases.
    /// - On some ECUs the FirstPID is parameter seems to be completely missing in the response...
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult StartStopDAQList(StartStopMode mode, UInt16 dAQListNo, out RespStartStopDAQList response)
    {
      var cmd = new CmdStartStopDAQList(mode, dAQListNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      byte[] additionalRespData;
      return CommKernel.sendFrame(this, data, out response, out additionalRespData);
    }
    /// <summary>
    /// Start/stop DAQ lists (synchronously)
    /// </summary>
    /// <param name="mode"></param>
    /// <returns>The corresponding command result</returns>
    public CmdResult StartStopSynch(StartStopMode mode)
    {
      var cmd = new CmdStartStopSynch(mode);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Read element from ODT entry
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ReadDAQ(out RespReadDAQ response)
    {
      var cmd = new CmdBase(CommandCode.ReadDAQ);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get DAQ clock from slave
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQClock(out RespGetDAQClock response)
    {
      var cmd = new CmdBase(CommandCode.GetDAQClock);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get general information on DAQ processor
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQProcessorInfo(out RespGetDAQProcessorInfo response)
    {
      var cmd = new CmdBase(CommandCode.GetDAQProcessorInfo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get general information on DAQ processing resolution
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQResolutionInfo(out RespGetDAQResolutionInfo response)
    {
      var cmd = new CmdBase(CommandCode.GetDAQResolutionInfo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get mode from DAQ list
    /// </summary>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQListMode(UInt16 dAQListNo, out RespGetDAQListMode response)
    {
      var cmd = new CmdGetDAQListMode(dAQListNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for an event channel
    /// </summary>
    /// <param name="eventChannelNo">Event channel number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQEventInfo(UInt16 eventChannelNo, out RespGetDAQEventInfo response)
    {
      var cmd = new CmdList(CommandCode.GetDAQEventInfo, eventChannelNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Clear DAQ list configuration
    /// </summary>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ClearDAQList(UInt16 dAQListNo)
    {
      var cmd = new CmdList(CommandCode.ClearDaqList, dAQListNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a DAQ list
    /// </summary>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQListInfo(UInt16 dAQListNo, out RespGetDAQListInfo response)
    {
      var cmd = new CmdList(CommandCode.GetDAQListInfo, dAQListNo);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Clear dynamic DAQ configuration
    /// </summary>
    /// <returns>The corresponding command result</returns>
    public CmdResult FreeDAQ()
    {
      var cmd = new CmdBase(CommandCode.FreeDAQ);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Allocate DAQ lists
    /// </summary>
    /// <param name="dAQCount">number of DAQ lists to be allocated</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult AllocDAQ(UInt16 dAQCount)
    {
      var cmd = new CmdAllocateDAQ(dAQCount);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Allocate ODTs to a DAQ list
    /// </summary>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <param name="oDTCount">number of ODTs to be assigned to DAQ list</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult AllocODT(UInt16 dAQListNo, byte oDTCount)
    {
      var cmd = new CmdAllocateODT(dAQListNo, oDTCount);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Allocate ODT entries to an ODT
    /// </summary>
    /// <param name="dAQListNo">DAQ list number</param>
    /// <param name="oDTNo">ODT number</param>
    /// <param name="oDTEntriesCount">number of ODT entries to be assigned to ODT</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult AllocODTEntry(UInt16 dAQListNo, byte oDTNo, byte oDTEntriesCount)
    {
      var cmd = new CmdAllocateODTEntry(dAQListNo, oDTNo, oDTEntriesCount);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>DTO CTR Properties
    /// Indicate the beginning of a programming sequence
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramStart(out RespProgramStart response)
    {
      var cmd = new CmdBase(CommandCode.ProgramStart);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Clear a part of non-volatile memory
    /// </summary>
    /// <param name="mode">Access mode</param>
    /// <param name="clearRange">The Clear Range indicates the length of the memory part to be cleared. 
    /// 
    /// The PROGRAM_CLEAR service clears a complete sector or multiple sectors at once.</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramClear(ProgramClearMode mode, UInt32 clearRange)
    {
      var cmd = new CmdProgramClear(mode, clearRange);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Write the specified byte array to the slave device with the specified command code.
    /// </summary>
    /// <param name="cmdCode">CommandCode to use</param>
    /// <param name="bytes">The byte array to write (This is the payload of the write command)</param>
    /// <param name="noOfDataElements">
    /// For CommandCode.DownloadMax, CommandCode.ProgramMax ignored
    /// For CommandCode.Program,CommandCode.Download,CommandCode.ProgramNext,CommandCode.DownloadNext 
    /// the no of elements to write (may be greater than bytes.Length).
    /// </param>
    /// <param name="useTimeout">
    /// - -1 - for standard timeout and wait for response
    /// - 0 - don't wait for any response (used for Master Block Mode)
    /// - greater than 0 - timeout in [ms] to wait for an response
    /// </param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentException">Thrown if cmdCode isn't one of the Program or Download commands</exception>
    protected CmdResult WriteBytes(CommandCode cmdCode
      , ref byte[] bytes
      , byte noOfDataElements = 0
      , int useTimeout = -1
      )
    {
      byte[] data;
      switch (cmdCode)
      { // parameters check
        case CommandCode.Program:
        case CommandCode.Download:
        case CommandCode.ProgramNext:
        case CommandCode.DownloadNext:
          data = XCPFrame.toByteArray(Type, Ctr, new CmdDownload(cmdCode, noOfDataElements), bytes);
          break;
        case CommandCode.DownloadMax:
        case CommandCode.ProgramMax:
          data = XCPFrame.toByteArray(Type, Ctr, new CmdBase(cmdCode), bytes);
          break;
        default:
          throw new ArgumentException("Command code not supported", "cmdCode");
      }
      RespBase response;
      return CommKernel.sendFrame(this, data, out response, useTimeout);
    }
    /// <summary>
    /// Program a non-volatile memory segment.
    /// 
    /// This command is used to program data inside the slave.
    /// </summary>
    /// <param name="noOfDataElements">The number of data elements</param>
    /// <param name="bytes">The bytes to program</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Program(byte noOfDataElements, byte[] bytes)
    {
      return WriteBytes(CommandCode.Program, ref bytes, noOfDataElements);
    }
    /// <summary>
    /// Program a non-volatile memory segment.
    /// 
    /// This command is used to transmit consecutive data bytes for the 
    /// PROGRAM command in block transfer mode.
    /// </summary>
    /// <param name="noOfDataElements">The number of data elements</param>
    /// <param name="bytes">The bytes to program</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramNext(byte noOfDataElements, byte[] bytes)
    {
      return WriteBytes(CommandCode.ProgramNext, ref bytes, noOfDataElements);
    }
    /// <summary>
    /// Program a non-volatile memory segment (fixed size)
    /// </summary>
    /// <param name="bytes">The bytes to program</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramMax(byte[] bytes)
    {
      return WriteBytes(CommandCode.ProgramMax, ref bytes);
    }
    /// <summary>
    /// Indicate the end of a programming sequence
    /// </summary>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramReset()
    {
      var cmd = new CmdBase(CommandCode.ProgramReset);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      var result = CommKernel.sendFrame(this, data, out response);
      CommKernel.setDisconnect(this);
      return CmdResult.OK;
    }
    /// <summary>
    /// Get general information on PGM processor
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetPGMProcessorInfo(out RespGetPGMProcessorInfo response)
    {
      var cmd = new CmdBase(CommandCode.GetPGMProcessorInfo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a SECTOR
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="sectorNo">The sector number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentException">Thrown if called with GetSectorInfoMode.NameLength</exception>
    public CmdResult GetSectorInfo(GetSectorInfoMode mode, byte sectorNo, out RespGetSectorInfoModeAddressOrLen response)
    {
      if (mode == GetSectorInfoMode.NameLength)
        throw new ArgumentException("Wrong call", "response");
      var cmd = new CmdGetSectorInfo(mode, sectorNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get specific information for a SECTOR
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="sectorNo">The sector number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentException">Thrown if not called with GetSectorInfoMode.NameLength</exception>
    public CmdResult GetSectorInfo(GetSectorInfoMode mode, byte sectorNo, out RespGetSectorInfoModeSectorNameLen response)
    {
      if (mode != GetSectorInfoMode.NameLength)
        throw new ArgumentException("Wrong call", "response");
      var cmd = new CmdGetSectorInfo(mode, sectorNo);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Prepare non-volatile memory programming
    /// </summary>
    /// <param name="codeSize"></param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramPrepare(UInt16 codeSize)
    {
      var cmd = new CmdProgramPrepare(codeSize);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set data format before programming
    /// </summary>
    /// <param name="compressionMethod">see XCP specification</param>
    /// <param name="encryptionMethod">see XCP specification</param>
    /// <param name="programmingMethod">see XCP specification</param>
    /// <param name="accessMethod">see XCP specification</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramFormat(byte compressionMethod, byte encryptionMethod, byte programmingMethod, byte accessMethod)
    {
      var cmd = new CmdProgramFormat(compressionMethod, encryptionMethod, programmingMethod, accessMethod);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Clear a part of non-volatile memory
    /// </summary>
    /// <param name="mode">Verification mode</param>
    /// <param name="verificationType">see XCP specification</param>
    /// <param name="verificationValue">see XCP specification</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ProgramVerify(ProgramVerifyMode mode, UInt16 verificationType, UInt32 verificationValue)
    {
      var cmd = new CmdProgramVerify(mode, verificationType, verificationValue);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Refer to user defined command.
    /// 
    /// This command is user defined. It mustn’t be used to implement functionalities 
    /// done by other services.
    /// </summary>
    /// <param name="subCommand">The subcommand</param>
    /// <param name="bytes">Additional bytes to send</param>
    /// <param name="respData">The uploaded data</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult UserCmd(byte subCommand, byte[] bytes, out byte[] respData)
    {
      var cmd = new CmdUserTransCmd(CommandCode.UserCmd, subCommand);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd, bytes);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response, out respData);
    }
    #region XCP v1.3 extension
    /// <summary>
    /// DTO CTR Properties
    /// 
    /// Returns the properties of a specific event channel addressed by a number in the 
    /// range from 0 to MAX_EVENT_CHANNEL-1. The command can optionally also 
    /// modify DTO CTR properties of this event channel.
    /// </summary>
    /// <param name="modifier">For each modifier which is set, the corresponding property
    /// (DTO CTR STIM mode, DTO CTR DAQ mode or related event channel number)of the event 
    /// channel is overwritten with the appropriate command parameter, else it is kept at 
    /// its current value. Command parameters which are unused, as their modifier is not set, 
    /// should be written as 0.</param>
    /// <param name="eventChannelNo">Event channel number [0,1,..MAX_EVENT_CHANNEL-1]</param>
    /// <param name="relatedEventChannelNo">RELATED_EVENT_CHANNEL_NUMBER [0,1,..MAX_EVENT_CHANNEL-1]</param>
    /// <param name="mode"></param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult DtoCtrProperties(DTOCtrModifier modifier
      , UInt16 eventChannelNo
      , UInt16 relatedEventChannelNo
      , DTOCtrMode mode
      , out RespDTOCtrResp response
      )
    {
      var cmd = new CmdDTOCtrProps(modifier, eventChannelNo, relatedEventChannelNo, mode);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Time Correlation
    /// 
    /// This command allows obtaining general information regarding the synchronization status of the clocks 
    /// available in the XCP slave as well as their characteristics.Also, the command provides all the 
    /// functionality needed by the XCP master for configuration of XCP slave’s behaviour targeting advanced 
    /// time correlation features.
    /// </summary>
    /// <param name="setProperties">Set properties</param>
    /// <param name="getPropertiesReq">Get properties request</param>
    /// <param name="clusterID">Cluster ID</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult TimeCorrelationProperties(TimeCorrSetProps setProperties, TimeCorrGetPropsReq getPropertiesReq, UInt16 clusterID, out RespTimeCorrelation response)
    {
      var cmd = new CmdTimeCorrelation(setProperties, getPropertiesReq, clusterID);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = XCPFrame.toByteArray(Type, Ctr, cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    #endregion
    #region XCPoverCAN specific
    /// <summary>
    /// Refer to transport layer specific command.
    /// 
    /// - This command is defined in the Transport Layer specification. 
    /// - It is used to perform Transport Layer specific actions.
    /// - It is usually used on XCPoverCAN.
    /// </summary>
    /// <param name="subCommand">The subcommand</param>
    /// <param name="bytes">Additional bytes to send</param>
    /// <param name="respData">The uploaded data</param>
    /// <param name="useTimeout">
    /// - -1 - for standard timeout and wait for response
    /// - 0 - don't wait for any response (used for Master Block Mode)
    /// - greater than 0 - timeout in [ms] to wait for an response
    /// </param>
    /// <returns>The corresponding command result</returns>
    public CmdResult TransportLayerCmd(byte subCommand, byte[] bytes, out byte[] respData
      , int useTimeout = -1
      )
    {
      respData = null;
      CmdUserTransCmd cmd = new CmdUserTransCmd(CommandCode.TransportLayerCmd, subCommand);
      var data = XCPFrame.toByteArray(Type, Ctr, cmd, bytes);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response, out respData, useTimeout);
    }
    /// <summary>
    /// Possible XCPOnCAN GetSlaveID modes.
    /// </summary>
    public enum GetSlaveIDMode : byte
    {
      /// <summary>The response is sent as echo (should return "XCP")</summary>
      IdentifyByEcho,
      /// <summary>The response is sent as inversed echo (should return ~"XCP")</summary>
      ConfirmByInverseEcho,
    }
    /// <summary>
    /// The master can use GetSlaveID to detect all XCP slaves within a CAN network.
    /// 
    /// - This is XCPoverCAN specific.
    /// </summary>
    /// <param name="mode">The GetSlaveID mode</param>
    /// <param name="respCANIDForCmdStim">
    /// The CAN identifier for Command and Stim; 
    /// The CAN identifier for CMD/STIM is coded in Intel format (MSB on higher position)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSlaveID(GetSlaveIDMode mode, out UInt32 respCANIDForCmdStim)
    {
      respCANIDForCmdStim = UInt32.MaxValue;
      var data = new byte[] { (byte)'X', (byte)'C', (byte)'P', (byte)mode };
      byte[] respData;
      CmdResult result = TransportLayerCmd((byte)XCPonCAN.GetSlaveID, data, out respData);
      if (result != CmdResult.OK)
        // cmd failure
        return result;

      if (respData.Length != 8)
        // response length failure
        return CmdResult.ERR_PROTOCOL_FAILURE;

      // check result
      switch (mode)
      {
        case GetSlaveIDMode.IdentifyByEcho:
          if (respData[1] != 'X' || respData[2] != 'C' || respData[3] != 'P')
            // response not ok
            return CmdResult.ERR_GENERIC;
          break;
        case GetSlaveIDMode.ConfirmByInverseEcho:
          if (~respData[1] != 'P' || ~respData[2] != 'C' || ~respData[3] != 'X')
            // response not ok
            return CmdResult.ERR_GENERIC;
          break;
      }
      respCANIDForCmdStim = BitConverter.ToUInt32(respData, 4);
      if (!BitConverter.IsLittleEndian)
        // Convert to little endian, if the executing machine is big endian
        respCANIDForCmdStim = Extensions.changeEndianess(respCANIDForCmdStim);
      return CmdResult.OK;
    }
    /// <summary>
    /// Gets the CAN identifier used for the specified DAQ list.
    /// 
    /// - With GetDAQID, the master can detect whether a DAQ list uses an individual CAN 
    /// identifier and whether this Identifier is fixed or configurable.
    /// - This is XCPoverCAN specific.
    /// </summary>
    /// <param name="daqListNo">The DAQ list number</param>
    /// <param name="isFixed">true if the DAQ identifier is fixed to the DAQ list</param>
    /// <param name="respCANID">The configured CAN identifier</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQID(UInt16 daqListNo, out bool isFixed, out UInt32 respCANID)
    {
      isFixed = false;
      respCANID = UInt32.MaxValue;
      var data = BitConverter.GetBytes(daqListNo);
      byte[] respData;
      CmdResult result = TransportLayerCmd((byte)XCPonCAN.GetDAQID, data, out respData);
      if (result != CmdResult.OK || respData.Length != 8)
        // cmd faileure or response not ok
        return result;
      isFixed = respData[1] == 1;
      respCANID = BitConverter.ToUInt32(respData, 4);
      if (ChangeEndianess)
        respCANID = Extensions.changeEndianess(respCANID);
      return CmdResult.OK;
    }
    /// <summary>
    /// Sets the CAN identifier to use for the specified DAQ list.
    /// 
    /// - If the given identifier isn’t possible, the slave returns an ERR_OUT_OF_RANGE.
    /// - This is XCPoverCAN specific.
    /// </summary>
    /// <param name="daqListNo">The DAQ list number</param>
    /// <param name="CANID">The CAN identifier to configure</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetDAQID(UInt16 daqListNo, UInt32 CANID)
    {
      var cmdData = new List<byte>();
      cmdData.AddRange(BitConverter.GetBytes(ChangeEndianess ? Extensions.changeEndianess(daqListNo) : daqListNo));
      cmdData.AddRange(BitConverter.GetBytes(ChangeEndianess ? Extensions.changeEndianess(CANID) : CANID));
      byte[] respData;
      return TransportLayerCmd((byte)XCPonCAN.SetDAQID, cmdData.ToArray(), out respData);
    }
    /// <summary>
    /// DAQ Clock Multicast on CAN.
    /// 
    /// When an XCP master makes use of GET_DAQ_CLOCK_MULTICAST command on
    /// transport layer CAN, a GET_DAQ_CLOCK_MULTICAST command must be sent every two
    /// seconds at the latest.
    /// 
    /// The command shall be sent on the CAN-ID “CAN_ID_GET_DAQ_CLOCK_MULTICAST”. 
    /// This CAN-Id may be the same as the CAN-ID "CAN_ID_BROADCAST".
    /// 
    /// Upon reception of GET_DAQ_CLOCK_MULTICAST command, the XCP slave will respond
    /// an EV_TIME_SYNC event packet as defined in XCP Protocol Layer.
    /// </summary>
    /// <param name="clusterID">Cluster Identifier (Intel byte order = little endian)</param>
    /// <param name="counter">Counter (allows for consistency checks at XCP master)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQClockMulitcast(UInt16 clusterID, byte counter)
    {
      var cmdData = new List<byte>();
      cmdData.AddRange(BitConverter.GetBytes(BitConverter.IsLittleEndian ? clusterID : Extensions.changeEndianess(clusterID)));
      cmdData.Add(counter);
      byte[] respData;
      return TransportLayerCmd((byte)XCPonCAN.GetDAQClockMulticast, cmdData.ToArray(), out respData, 0);
    }
    #endregion
    #endregion
    #endregion
    #region IDisposable Members
    /// <summary>
    /// frees used resources.
    /// </summary>
    public override void Dispose()
    {
      CommKernel.DeregisterClient(this);
      base.Dispose();
    }
    #endregion
  }
}