﻿/*!
 * @file    
 * @brief   Implements the UDP and TCP drivers for supporting XCP.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using log4net;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Base class supporting any XCP based communication instance.
  /// 
  /// This is the class to derive other XCP communication types from (XCP on CAN, FlexRay, USB, ...)
  /// </summary>
  public abstract class XCPCommunicator : Communicator
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(XCPCommunicator));
    /// <summary>
    /// Holds the corresponding XCP master instance.
    /// </summary>
    protected XCPMaster mMaster;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance of XCPCommunicator.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="lenCtrSize">The size of Len and Ctr byte (may be zero)</param>
    internal XCPCommunicator(XCPMaster master)
    {
      mMaster = master;
    }
    #endregion
    #region methods
    /// <summary>
    /// Gets an XCP frame from the specified data.
    /// </summary>
    /// <param name="type">The XCP type</param>
    /// <param name="source">The data source</param>
    /// <param name="data">The data itself</param>
    /// <param name="frame">The resulting frame (only valid if return value is true)</param>
    /// <returns>true if a valid frame is built</returns>
    protected abstract bool getFrameFromData(XCPType type, string source, ref byte[] data, out XCPFrame frame);
    /// <summary>
    /// Called from the receive thread, when data is received.
    /// </summary>
    /// <param name="comm">The communication source</param>
    /// <param name="source">The data source</param>
    /// <param name="buffer">The data received</param>
    protected void dataReceived(XCPCommunicator comm, string source, ref byte[] buffer)
    {
      XCPFrame recFrame;
      UInt16 len = (UInt16)buffer.Length;
      while (getFrameFromData(mMaster.Type, source, ref buffer, out recFrame))
      { // frame rceived
        XCPFrameQueue.Instance.enqueueFrame(recFrame);
        mMaster.increaseReceivedFrames(recFrame.getRawFrameLength());
        buffer = null;
        byte respCode = recFrame.Data[XCPFrame.ResponseIndex];
        switch (respCode)
        {
          case (byte)PIDSlaveMaster.EV:
            EventCodes eventCode = (EventCodes)recFrame.Data[XCPFrame.EventCodeIndex];
            mMaster.onEventReceived(recFrame);
            comm.RespPending = eventCode == EventCodes.CmdPending;
            if (comm.RespPending)
            {
              comm.WaitForResponse.Set();
#if DEBUG
              mLogger.Info("Pending received -> waiting");
#endif
            }
            break;

          case (byte)PIDSlaveMaster.SERV:
            ServiceRequestCode serviceCode = (ServiceRequestCode)recFrame.Data[XCPFrame.EventCodeIndex];
            mMaster.onServiceReceived(recFrame);
            break;

          case (byte)PIDSlaveMaster.ERR:
          case (byte)PIDSlaveMaster.RES:
            mMaster.LastResponse = recFrame;
            ResponseProcessed.Reset();
            // signal the received frame
            comm.WaitForResponse.Set();
            // wait for the response receiver to process frame
            var result = ResponseProcessed.WaitOne(comm.mMaster.Timings[0] / 2);
            break;

          default:
            // These are DAQ events
            mMaster.onDAQFrameReceived(recFrame);
            break;
        }
      }
    }
    #endregion
  }
}