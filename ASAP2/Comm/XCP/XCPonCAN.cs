﻿/*!
 * @file    
 * @brief   Implements the CAN communicator driver for supporting XCP.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using jnsoft.ASAP2.XCP;
using jnsoft.Comm.CAN;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Base class implementing the XCP on CAN based communication instance.
  /// </summary>
  public sealed class XCPOnCAN : XCPCommunicator
  {
    ICANProvider mCANProvider;
    A2LXCP_ON_CAN mXCPCAN;
    /// <summary>
    /// Creates a new instance of XCPOnCAN.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="canProvider">The corresponding CAN provider</param>
    /// <param name="xcpCAN">The ASAP2 XCP on CAN interface</param>
    internal XCPOnCAN(XCPMaster master, ICANProvider canProvider, A2LXCP_ON_CAN xcpCAN)
      : base(master)
    {
      mXCPCAN = xcpCAN;
      SourceAddress = CANProvider.toCANIDString(xcpCAN.CANIDCmd);
      mCANProvider = canProvider;
      var canIDs = new List<UInt32>();
      canIDs.Add(mXCPCAN.CANIDResp);
      foreach (var daqCANId in mXCPCAN.enumChildNodes<A2LXCP_DAQ_LIST_CAN_ID>())
        if (!canIDs.Contains(daqCANId.CANId))
          canIDs.Add(daqCANId.CANId);
      mCANProvider.registerClient(canIDs.ToArray(), receiveThread);
    }
    protected override void close()
    {
    }
    internal override int send(byte[] data)
    {
      var fullData = data;
      var msgLen = CANProvider.getSendMsgLen(data.Length, mXCPCAN.MaxDLCRequired);
      if (msgLen > data.Length)
      { // add data to required DLC length
        fullData = new byte[msgLen];
        Array.Copy(data, fullData, data.Length);
      }

      return fullData.Length == mCANProvider.send(mXCPCAN.CANIDCmd, ref fullData)
        ? data.Length
        : 0;
    }
    protected sealed override bool getFrameFromData(XCPType type, string source, ref byte[] data, out XCPFrame frame)
    {
      if (data == null)
      {
        frame = null;
        return false;
      }
      frame = new XCPFrame(type, source, ref data, false);
      return true;
    }
    void receiveThread(object sender, CANEventArgs e)
    {
      byte[] data = e.Frame.Data;
      if (e.Frame.ID == mXCPCAN.CANIDResp && data[0] >= (byte)PIDSlaveMaster.SERV)
        // response received
        dataReceived(this, mMaster.SlaveAddress, ref data);
      else
        // now this must be a DAQ mesage
        dataReceived(this, CANProvider.toCANIDString(e.Frame.ID), ref data);
    }
    public override void Dispose()
    {
      mCANProvider.unregisterClient(receiveThread);
      base.Dispose();
    }
  }
}