﻿/*!
 * @file    
 * @brief   Defines the XCP protocol definitions
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2;
using jnsoft.ASAP2.XCP;
using jnsoft.Helpers;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

/// <summary>
/// Implements the XCP Protocol Version 1.3 (ASAM MCD-1 XCP V1.3.0)
/// 
/// - supported is XCPType.UDP, XCPType.TCP and XCPType.CAN
/// </summary>
namespace jnsoft.Comm.XCP
{
  #region enums
  /// <summary>
  /// Supported types of XCP communication.
  /// </summary>
  public enum XCPType
  {
    /// <summary>
    /// Not known or not set...
    /// </summary>
    Unknown,
    /// <summary>
    /// XCP on UDP (Ethernet)
    /// </summary>
    UDP,
    /// <summary>
    /// XCP on UDP (Ethernet)
    /// </summary>
    TCP,
    /// <summary>
    /// XCP on CAN
    /// </summary>
    CAN,
    /// <summary>
    /// XCP on USB (not supported by this library)
    /// </summary>
    USB,
    /// <summary>
    /// XCP on FlexRay (not supported by this library)
    /// </summary>
    FlexRay,
    /// <summary>
    /// XCP on SXL (not supported by this library)
    /// </summary>
    SXL,
  }
  /// <summary>
  /// Supported types of XCP communication.
  /// </summary>
  public enum PIDSlaveMaster : byte
  {
    /// <summary>
    /// Positive response
    /// </summary>
    RES = 0xff,
    /// <summary>
    /// Error response
    /// </summary>
    ERR = 0xfe,
    /// <summary>
    /// Event
    /// </summary>
    EV = 0xfd,
    /// <summary>
    /// Service request from slave
    /// </summary>
    SERV = 0xfc,
  }
  /// <summary>
  /// Possible XCP event codes.
  /// </summary>
  public enum EventCodes : byte
  {
    /// <summary>
    /// Slave starting in RESUME mode
    /// </summary>
    [Description("Slave starting in RESUME mode")]
    ResumeMode = 0x00,
    /// <summary>
    /// The DAQ configuration in non-volatile memory has been cleared. 
    /// </summary>
    [Description("The DAQ configuration in non-volatile memory has been cleared. ")]
    ClearDAQ = 0x01,
    /// <summary>
    /// The DAQ configuration has been stored into non-volatile memory.
    /// </summary>
    [Description("The DAQ configuration has been stored into non-volatile memory.")]
    StoreDAQ = 0x02,
    /// <summary>
    /// The calibration data has been stored into non-volatile memory.
    /// </summary>
    [Description("The calibration data has been stored into non-volatile memory.")]
    StoreCAL = 0x03,
    /// <summary>
    /// Slave requesting to restart time-out
    /// </summary>
    [Description("Slave requesting to restart time-out")]
    CmdPending = 0x05,
    /// <summary>
    /// DAQ processor overload.
    /// </summary>
    [Description("DAQ processor overload.")]
    DAQOverload = 0x06,
    /// <summary>
    /// Session terminated by slave device.
    /// </summary>
    [Description("Session terminated by slave device.")]
    SessionTerminated = 0x07,
    /// <summary>
    /// Transfer of externally triggered timestamp
    /// </summary>
    [Description("Transfer of externally triggered timestamp")]
    TimeSync = 0x08,
    /// <summary>
    /// Indication of a STIM timeout
    /// </summary>
    [Description("Indication of a STIM timeout")]
    STIMTimeout = 0x09,
    /// <summary>
    /// Slave entering SLEEP mode
    /// </summary>
    [Description("Slave entering SLEEP mode")]
    Sleep = 0x0a,
    /// <summary>
    /// Slave leaving SLEEP mode
    /// </summary>
    [Description("Slave leaving SLEEP mode")]
    WakeUp = 0x0b,
    /// <summary>
    /// Raised from the master transport layer.
    /// 
    /// if data acquisition data may be lost (This should only appear on UDP).
    /// </summary>
    [Description("Lost DAQ data")]
    DAQDataLost = 0xfd,
    /// <summary>
    /// User-defined event
    /// </summary>
    [Description("User-defined event")]
    User = 0xfe,
    /// <summary>
    /// Transport layer specific event
    /// </summary>
    [Description("Transport layer specific event")]
    Transport = 0xff,
  }
  /// <summary>
  /// Possible service requests
  /// 
  /// Used in RespService.
  /// </summary>
  public enum ServiceRequestCode : byte
  {
    /// <summary>
    /// Slave requesting to be reset
    /// </summary>
    [Description("Slave requesting to be reset")]
    Reset = 0x00,
    /// <summary>
    /// Slave transferring a byte stream of plain ASCII text. 
    /// 
    /// - The line separator is LF or CR/LF. 
    /// - The text can be transferred in consecutive packets. 
    /// - The end of the overall text is indicated by the last 
    /// packet containing a Null terminated string.
    /// </summary>
    [Description("Slave transferring a byte stream of plain ASCII text.")]
    Text = 0x01,
  }
  /// <summary>
  /// XCP command codes defined by the XCP specification.
  /// </summary>
  public enum CommandCode : byte
  {
    // STD
    Connect = 0xff,
    Disconnect = 0xfe,
    GetStatus = 0xfd,
    Synch = 0xfc,
    GetCommModeInfo = 0xfb,
    GetId = 0xfa,
    SetRequest = 0xf9,
    GetSeed = 0xf8,
    Unlock = 0xf7,
    SetMTA = 0xf6,
    Upload = 0xf5,
    ShortUpload = 0xf4,
    BuildChecksum = 0xf3,
    TransportLayerCmd = 0xf2,
    UserCmd = 0xf1,
    // CAL
    Download = 0xf0,
    DownloadNext = 0xef,
    DownloadMax = 0xee,
    ShortDownload = 0xed,
    ModifyBits = 0xec,
    // PAG
    SetCalPage = 0xeb,
    GetCalPage = 0xea,
    GetPAGProcessorInfo = 0xe9,
    GetSegmentInfo = 0xe8,
    GetPageInfo = 0xe7,
    SetSegmentMode = 0xe6,
    GetSegmentMode = 0xe5,
    CopyCALPage = 0xe4,
    // DAQ
    SetDAQPtr = 0xe2,
    WriteDAQ = 0xe1,
    SetDAQListMode = 0xe0,
    StartStopDAQList = 0xde,
    StartStopSynch = 0xdd,
    WriteDAQMultiple = 0xc7,
    ReadDAQ = 0xdb,
    GetDAQClock = 0xdc,
    GetDAQProcessorInfo = 0xda,
    GetDAQResolutionInfo = 0xd9,
    GetDAQListMode = 0xdf,
    GetDAQEventInfo = 0xd7,
    DtoCtrPproperties = 0xc5,
    ClearDaqList = 0xe3,
    GetDAQListInfo = 0xd8,
    FreeDAQ = 0xd6,
    AllocDAQ = 0xd5,
    AllocODT = 0xd4,
    AllocODTEntry = 0xd3,
    // PGM
    ProgramStart = 0xd2,
    ProgramClear = 0xd1,
    Program = 0xd0,
    ProgramReset = 0xcf,
    GetPGMProcessorInfo = 0xce,
    GetSectorInfo = 0xcd,
    ProgramPrepare = 0xcc,
    ProgramFormat = 0xcb,
    ProgramNext = 0xca,
    ProgramMax = 0xc9,
    ProgramVerify = 0xc8,
    // Time sync commands
    TimeCorrelationProperties = 0xc6,
  }
  /// <summary>
  /// Possible resource types
  /// 
  /// Used in RespConnect, RespGetStatus, CmdGetSeed, RespUnlock.
  /// </summary>
  [Flags]
  public enum ResourceType : byte
  {
    None = 0x00,
    /// <summary>
    /// Calibration and paging.
    /// </summary>
    CAL_PAG = 0x01,
    /// <summary>
    /// Data acquisition.
    /// </summary>
    DAQ = 0x04,
    /// <summary>
    /// Stimulation.
    /// </summary>
    STIM = 0x08,
    /// <summary>
    /// Programming.
    /// </summary>
    PGM = 0x10,
  }
  /// <summary>
  /// Optional communication mode flags.
  /// </summary>
  [Flags]
  public enum CommModeOptional : byte
  {
    /// <summary>None of the other 2 modes is available.</summary>
    None = 0x00,
    /// <summary>Master block mode is available.</summary>
    MasterBlockMode = 0x01,
    /// <summary>Interleaved mode is available.</summary>
    InterleavedMode = 0x02,
  }
  /// <summary>
  /// Communication modes for programming.
  /// </summary>
  [Flags]
  public enum CommModeProgram : byte
  {
    /// <summary>Only standard mode is available.</summary>
    None = 0x00,
    /// <summary>Master block mode is available.</summary>
    MasterBlockMode = 0x01,
    /// <summary>Interleaved mode is available.</summary>
    InterleavedMode = 0x02,
    /// <summary>Slave block mode is available.</summary>
    SlaveBlockMode = 0x40,
  }
  [Flags]
  public enum Mode : byte
  {
    TransferMode = 0x01,
    CompressedEncrypted = 0x02,
  }
  /// <summary>
  /// Posoible Set Request modes.
  /// 
  /// Used in CmdSetRequest.
  /// </summary>
  [Flags]
  public enum SetRequestMode : byte
  {
    /// <summary>
    /// REQuest to STORE CALibration data.
    /// </summary>
    StoreCALRequest = 0x01,
    /// <summary>
    /// REQuest to STORE  DAQ list, no RESUME.
    /// </summary>
    StoreDAQRequestNoResume = 0x02,
    /// <summary>
    /// REQuest to STORE  DAQ list, RESUME enabled.
    /// </summary>
    StoreDAQRequestResume = 0x04,
    /// <summary>
    /// REQuest to CLEAR DAQ configuratio.
    /// </summary>
    ClearDAQRequest = 0x08,
  }
  /// <summary>
  /// Possible session states.
  /// 
  /// Used in RespGetStatus.
  /// </summary>
  [Flags]
  public enum SessionState : byte
  {
    None = 0x00,
    /// <summary>
    /// ReqQuest to STORE CALibration data.
    /// </summary>
    StoreCALRequest = 0x01,
    /// <summary>
    /// Request to STORE DAQ list.
    /// </summary>
    StoreDAQRequest = 0x04,
    /// <summary>
    /// Request to CLEAR DAQ configuration.
    /// </summary>
    ClearDAQRequest = 0x08,
    /// <summary>
    /// Indication of running Data Transfer.
    /// </summary>
    DAQRunning = 0x40,
    /// <summary>
    /// RESUME Mode.
    /// </summary>
    Resume = 0x80,
  }
  /// <summary>
  /// Data acquisition key byte flags.
  /// 
  /// Used in RespGetDAQProcessorInfo.
  /// 
  /// IDFieldType:
  /// 
  /// The Identification_Field_Type flags indicate the Type of Identification Field the slave will use 
  /// when transferring DAQ Packets to the master. The master has to use the same Type of 
  /// Identification Field when transferring STIM Packets to the slave.
  /// 
  /// - Not set: Absolute ODT number
  /// - IDFieldType0 set: Relative ODT number, absolute DAQ list number (BYTE)
  /// - IDFieldType1 set: Relative ODT number, absolute DAQ list number (WORD)
  /// - IDFieldType0, IDFieldType1 set: Relative ODT number, absolute DAQ list 
  /// number (WORD, aligned)
  /// </summary>
  [Flags]
  public enum DAQKeyByte : byte
  {
    /// <summary>
    /// OM_ODT_TYPE_16
    /// </summary>
    OptimisationType0 = 0x01,
    /// <summary>
    /// OM_ODT_TYPE_32
    /// </summary>
    OptimisationType1 = 0x02,
    /// <summary>
    /// OM_ODT_TYPE_64
    /// </summary>
    OptimisationType01 = OptimisationType0 | OptimisationType1,
    /// <summary>
    /// OM_ODT_TYPE_ALIGNMENT
    /// </summary>
    OptimisationType2 = 0x04,
    /// <summary>
    /// OM_MAX_ENTRY_SIZE
    /// </summary>
    OptimisationType02 = OptimisationType0 | OptimisationType2,
    OptimisationType3 = 0x08,
    /// <summary>
    /// address extension to be the same for all entries within one ODT.
    /// </summary>
    AdrExtensionODT = 0x10,
    /// <summary>
    /// Not allowed.
    /// </summary>
    AdrExtensionDAQ = 0x20,
    /// <summary>
    /// address extension to be the same for all entries within one DAQ.
    /// </summary>
    AdrExtensionODTDAQ = AdrExtensionODT | AdrExtensionDAQ,
    /// <summary>
    /// Relative ODT number, absolute DAQ list number (BYTE).
    /// </summary>
    IDFieldType0 = 0x40,
    /// <summary>
    /// Relative ODT number, absolute DAQ list number (WORD).
    /// </summary>
    IDFieldType1 = 0x80,
    /// <summary>
    /// Relative ODT number, absolute DAQ list number (WORD, aligned).
    /// </summary>
    IDFieldType11 = IDFieldType0 | IDFieldType1,
  }
  /// <summary>
  /// The DAQ_PROPERTIES parameter is a bit mask
  /// </summary>
  [Flags]
  public enum DAQProperties : byte
  {
    Dynamic = 0x01,
    /// <summary>
    /// The PRESCALER_SUPPORTED flag indicates that all DAQ lists support the 
    /// prescaler for reducing the transmission period.
    /// </summary>
    PrescalerSupported = 0x02,
    /// <summary>
    /// The RESUME_SUPPORTED flag indicates that all DAQ lists can be put in RESUME mode.
    /// </summary>
    ResumeSupported = 0x04,
    /// <summary>
    /// The BIT_STIM_SUPPORTED flag indicates whether bitwise data stimulation through 
    /// BIT_OFFSET in WRITE_DAQ is supported.
    /// </summary>
    BitSTIMSupported = 0x08,
    /// <summary>
    /// The TIMESTAMP_SUPPORTED flag indicates whether the slave supports time stamped data 
    /// acquisition and stimulation. If the slave doesn’t support a time stamped mode, the parameters 
    /// TIMESTAMP_MODE and TIMESTAMP_TICKS (GET_DAQ_RESOLUTION_INFO) are invalid.
    /// </summary>
    TimestampSupported = 0x10,
    /// <summary>
    /// The PID_OFF_SUPPORTED flag in DAQ_PROPERTIES indicates that transfer of DTO 
    /// Packets without Identification Field is possible. 
    /// </summary>
    PIDOffSupported = 0x20,
    /// <summary>
    /// Overload indication in MSB of PID
    /// </summary>
    OverloadMSB = 0x40,
    /// <summary>
    /// overload indication by Event Packet
    /// </summary>
    OverloadEvent = 0x80,
  }
  /// <summary>
  /// Possible DAQ list modes.
  /// 
  /// Valid for reading and setting modes.
  /// 
  /// Used in CmdSetDAQListMode.
  /// </summary>
  [Flags]
  public enum DAQListMode : byte
  {
    /// <summary>
    /// Nothing is set.
    /// </summary>
    None = 0x00,
    /// <summary>
    /// enable alternating display mode (only when set the mode).
    /// </summary>
    Alternating = 0x01,
    /// <summary>
    /// (only when read the mode).
    /// </summary>
    Selected = Alternating,
    /// <summary>
    /// STIM set to Data Stimulation Mode (Master -> Slave).
    /// </summary>
    Direction = 0x02,
    /// <summary>
    /// enable timestamp mode.
    /// </summary>
    Timestamp = 0x10,
    /// <summary>
    /// transmit DTO without Identification Field.
    /// </summary>
    PIDOff = 0x20,
    /// <summary>
    /// DAQ list is active.
    /// </summary>
    Running = 0x40,
    /// <summary>
    /// DAQ list is part of a configuration used in RESUME mode.
    /// </summary>
    Resume = 0x80,
  }
  /// <summary>
  /// Properties for a DAQ list.
  /// 
  /// DAQ == STIM == 0 is not allowed!
  /// </summary>
  [Flags]
  public enum DAQListProperties : byte
  {
    /// <summary>
    /// indicates that the configuration of this DAQ list cannot be changed.<para/>
    /// The DAQ list is predefined and fixed in the slave device’s memory.<para/>
    /// 0 = DAQ list configuration can be changed<para/>
    /// 1 = DAQ list configuration is fixed
    /// </summary>
    Predefined = 0x01,
    /// <summary>
    /// Indicates that the Event Channel for this DAQ list cannot be changed.<para/>
    /// 0 = Event Channel can be changed<para/>
    /// 1 = Event Channel is fixed
    /// </summary>
    EventFixed = Predefined << 1,
    /// <summary>DAQ direction supported</summary>
    DAQ = Predefined << 2,
    /// <summary>STIM direction supported</summary>
    STIM = Predefined << 3,
  }
  /// <summary>
  /// Properties for an event channel<para/>
  /// DAQ == STIM == 0 is not allowed!
  /// </summary>
  [Flags]
  public enum DAQEventProperties : byte
  {
    /// <summary>DAQ direction supported</summary>
    DAQ = 0x04,
    /// <summary>STIM direction supported</summary>
    STIM = DAQ << 1,
    /// <summary>Indicates that for this Event Channel all data that belong to one and the same DAQ list are processed consistently.</summary>
    ConsistencyDAQ = 0x40,
    /// <summary>Indicates that for this Event Channel all data are processed consistently./// </summary>
    ConsistencyEvent = ConsistencyDAQ << 1,
  }
  /// <summary>
  /// The TIMESTAMP_MODE parameter is a bit mask.
  /// 
  /// Used in RespGetDAQResolutionInfo.
  /// </summary>
  [Flags]
  public enum DAQTimestampMode : byte
  {
    /// <summary>
    /// Timestamp size is a Byte (8 Bit).
    /// </summary>
    Size0 = 0x01,
    /// <summary>
    /// Timestamp size is a Word (16 Bit).
    /// </summary>
    Size1 = 0x02,
    /// <summary>
    /// Timestamp size is a DWord (32 Bit).
    /// </summary>
    Size2 = 0x04,
    /// <summary>
    /// Indicates that the Slave always will send DTO Packets in time.
    /// </summary>
    TimestampFixed = 0x08,
    Uint0 = 0x10,
    Uint1 = 0x20,
    Uint2 = 0x40,
    Uint3 = 0x80,
  }
  /// <summary>
  /// Possible segment modes
  /// 
  /// Used in RespGetSegmentMode.
  /// </summary>
  [Flags]
  public enum SegmentMode : byte
  {
    /// <summary>
    /// Mode none or undefined.
    /// </summary>
    None = 0x00,
    /// <summary>
    /// Freeze mode.
    /// </summary>
    Freeze = 0x01,
  }
  /// <summary>
  /// Possible page properties.
  /// 
  /// The ECUAccessX flags indicate whether and how the ECU can access this page.
  /// If the ECU can access this PAGE, the ECU_ACCESS_x flags indicate whether the ECU can 
  /// access this PAGE only if the XCP master does NOT access this PAGE at the same time, 
  /// only if the XCP master accesses this page at the same time, or the ECU doesn’t care 
  /// whether the XCP master accesses this page at the same time or not.
  /// 
  /// The XCPReadAccessX XCPWriteAccessX flags indicate whether and how the XCP master can access this 
  /// page. The flags make a distinction for the XCP_ACCESS_TYPE depending on the kind of 
  /// access the XCP master can have on this page (READABLE and/or WRITEABLE).
  /// </summary>
  [Flags]
  public enum PageProperties : byte
  {
    /// <summary>Access Without XCP</summary>
    ECUAccessWithoutXCP = 0x01,
    /// <summary>Access With XCP</summary>
    ECUAccessWithXCP = 0x02,
    /// <summary>Read access without ECU</summary>
    XCPReadAccessWithoutECU = 0x04,
    /// <summary>Read access with ECU</summary>
    XCPReadAccessWithECU = 0x08,
    /// <summary>Write access without ECU</summary>
    XCPWriteAccessWithoutECU = 0x10,
    /// <summary>Wruite access with ECU</summary>
    XCPWriteAccessWithECU = 0x20,
  }
  /// <summary>
  /// Possible cal page modes.
  /// 
  /// Used in CmdSetCALPage.
  /// </summary>
  [Flags]
  public enum CalPageMode : byte
  {
    /// <summary>The given page will be used by the slave device application.</summary>
    ECU = 0x01,
    /// <summary>The slave device XCP driver will access the given page.</summary>
    XCP = 0x02,
    /// <summary>The logical segment number is ignored. The command applies to all segments.</summary>
    ALL = 0x80,
  }
  [Flags]
  public enum ProgramProperties : byte
  {
    None = 0x00,
    AbsoluteMode = 0x01,
    FunctionalMode = 0x02,
    CompressionSupported = 0x04,
    CompressionRequired = 0x08,
    EncryptionSupported = 0x10,
    EncryptionRequired = 0x20,
    NonSeqProgramSupported = 0x40,
    NonSeqProgramRequired = 0x80,
  }
  /// <summary>
  /// Possible command results.
  /// </summary>
  public enum CmdResult : byte
  {
    /// <summary>
    /// Command processor synchronization
    /// </summary>
    [Description("Command processor synchronization")]
    ERR_CMD_SYNCH = 0x00,
    /// <summary>
    /// Command was not executed
    /// </summary>
    [Description("Command was not executed")]
    ERR_CMD_BUSY = 0x10,
    /// <summary>
    /// Command rejected because DAQ is running
    /// </summary>
    [Description("Command rejected because DAQ is running")]
    ERR_DAQ_ACTIVE = 0x11,
    /// <summary>
    /// Command rejected because PGM is running
    /// </summary>
    [Description("Command rejected because PGM is running")]
    ERR_PGM_ACTIVE = 0x12,
    /// <summary>
    /// Unknown command or not implemented optional command
    /// </summary>
    [Description("Unknown command or not implemented optional command")]
    ERR_CMD_UNKNOWN = 0x20,
    /// <summary>
    /// Command syntax invalid
    /// </summary>
    [Description("Command syntax invalid")]
    ERR_CMD_SYNTAX = 0x21,
    /// <summary>
    /// Command syntax valid but command parameter(s) out of range
    /// </summary>
    [Description("Command syntax valid but command parameter(s) out of range")]
    ERR_OUT_OF_RANGE = 0x22,
    /// <summary>
    /// The memory location is write protected
    /// </summary>
    [Description("The memory location is write protected")]
    ERR_WRITE_PROTECTED = 0x23,
    /// <summary>
    /// The memory location is not accessible
    /// </summary>
    [Description("The memory location is not accessible")]
    ERR_ACCESS_DENIED = 0x24,
    /// <summary>
    /// Access denied,Seed andKey is required
    /// </summary>
    [Description("Access denied, Seed & Key is required")]
    ERR_ACCESS_LOCKED = 0x25,
    /// <summary>
    /// Selected page not available
    /// </summary>
    [Description("Selected page not available")]
    ERR_PAGE_NOT_VALID = 0x26,
    /// <summary>
    /// Selected page mode not available
    /// </summary>
    [Description("Selected page mode not available")]
    ERR_MODE_NOT_VALID = 0x27,
    /// <summary>
    /// Selected segment not valid
    /// </summary>
    [Description("Selected segment not valid")]
    ERR_SEGMENT_NOT_VALID = 0x28,
    /// <summary>
    /// Sequence error
    /// </summary>
    [Description("Sequence error")]
    ERR_SEQUENCE = 0x29,
    /// <summary>
    /// DAQ configuration not valid
    /// </summary>
    [Description("DAQ configuration not valid")]
    ERR_DAQ_CONFIG = 0x2a,
    /// <summary>
    /// Memory overflow error
    /// </summary>
    [Description("Memory overflow error")]
    ERR_MEMORY_OVERFLOW = 0x30,
    /// <summary>
    /// Generic error
    /// </summary>
    [Description("Generic error")]
    ERR_GENERIC = 0x31,
    /// <summary>
    /// The slave internal program verify routine detects an error
    /// </summary>
    [Description("The slave internal program verify routine detects an error")]
    ERR_VERIFY = 0x32,
    /// <summary>
    /// Access to the requested resource is temporary not possible
    /// </summary>
    [Description("Access to the requested resource is temporary not possible")]
    ERR_RESOURCE_TEMPORARY_NOT_ACCESSIBLE = 0x33,
    /// <summary>
    /// Command cancelled by user
    /// </summary>
    [Description("Command was canelled by user")]
    ERR_CANCELLED = 0xfb,
    /// <summary>
    /// Invalid argument failure (e.g. a call to upload with 0 bytes length)
    /// </summary>
    [Description("Invalid argument failure (e.g. a call to upload with 0 bytes length)")]
    ERR_INVALID_ARGUMENT = 0xfc,
    /// <summary>
    /// Timeout (disconnection?)
    /// </summary>
    [Description("Timeout (disconnected?)")]
    ERR_TIMEOUT = 0xfd,
    /// <summary>
    /// Protocol failure (unexpected response length)
    /// </summary>
    [Description("Protocol failure (unexpected response length)")]
    ERR_PROTOCOL_FAILURE = 0xfe,
    /// <summary>
    /// OK (Command successful)
    /// </summary>
    [Description("Successful")]
    OK = 0xff,
  }
  /// <summary>
  /// Granularity for size of ODT entry (STIM or DAQ).
  /// </summary>
  public enum GranODTEntrySize : byte
  {
    Byte = 0x01,
    Word = 0x02,
    DWord = 0x04,
    QWord = 0x08,
  }
  /// <summary>
  /// Possible subcommands defined in XCPonCAN for the TRANSPORT_LAYER_CMD.
  /// </summary>
  public enum XCPonCAN : byte
  {
    /// <summary>Get Slave ID</summary>
    GetSlaveID = 0xff,
    /// <summary>Get DAQ ID</summary>
    GetDAQID = 0xfe,
    /// <summary>Set DAQ ID</summary>
    SetDAQID = 0xfd,
    /// <summary>Get DAQ Clock Multicast</summary>
    GetDAQClockMulticast = 0xfa,
  }
  /// <summary>
  /// Possible XCPOnCAN GetSlaveID modes.
  /// </summary>
  public enum GetSlaveIDMode : byte
  {
    /// <summary>The response is sent as echo (should return "XCP")</summary>
    IdentifyByEcho,
    /// <summary>The response is sent as inversed echo (should return ~"XCP")</summary>
    ConfirmByInverseEcho,
  }
  #endregion
  #region command/response
  /// <summary>
  /// Base class represents a XCP command.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class CmdBase
  {
    public CommandCode CmdCode;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public CmdBase() { }
    internal CmdBase(CommandCode code) { CmdCode = code; }
    public virtual void changeEndianness() { }
  }
  /// <summary>
  /// Base class represents a XCP response.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class RespBase : ResponseBase
  {
    PIDSlaveMaster RespCode;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public RespBase() { }
    public RespBase(PIDSlaveMaster code) { RespCode = code; }
  }
  /// <summary>
  /// Base class represents a XCP event.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class EventBase : RespBase
  {
    public EventCodes EventCode;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public EventBase() { }
    public EventBase(EventCodes code) : base(PIDSlaveMaster.EV) { EventCode = code; }
  }
  /// <summary>
  /// Base class represents a XCP service request.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespService : RespBase
  {
    public ServiceRequestCode ServiceRequestCode;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public RespService() { }
    public RespService(ServiceRequestCode code) : base(PIDSlaveMaster.SERV) { ServiceRequestCode = code; }
  }
  /// <summary>
  /// Error Response
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespError : RespBase
  {
    public CmdResult ErrorCode;
    public RespError() { }
    public RespError(CmdResult errorCode) : base(PIDSlaveMaster.ERR) { ErrorCode = errorCode; }
  }
  /// <summary>
  /// Event response
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespEvent : RespBase
  {
    public EventCodes EventCode;
    public RespEvent(EventCodes eventCode) : base(PIDSlaveMaster.EV) { EventCode = eventCode; }
  }
  /// <summary>
  /// Possible connect modes.
  /// 
  /// Used in CmdConnect.
  /// </summary>
  public enum ConnectMode : byte
  {
    /// <summary>
    /// With a CONNECT(Mode = Normal), the master can start an XCP communication with the slave.
    /// </summary>
    Normal,
    /// <summary>
    /// With a CONNECT(Mode = user defined), the master can start an XCP communication with 
    /// the slave and at the same time tell the slave that it should go into a special (user defined) mode.
    /// </summary>
    UserDefined,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdConnect : CmdBase
  {
    ConnectMode Mode;
    internal CmdConnect(ConnectMode mode) : base(CommandCode.Connect) { Mode = mode; }
  }
  /// <summary>
  /// Response from Set up connection with slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespConnect : RespBase
  {
    public ResourceType Resource;
    public CommModeBasic CommModeBasic;
    public byte MaxCTO;
    public UInt16 MaxDTO;
    public UInt16 Version;
    public RespConnect() { }
    public RespConnect(ResourceType resource, CommModeBasic commModeBasic, byte maxCTO, UInt16 maxDTO, byte v1, byte v2)
      : base(PIDSlaveMaster.RES)
    {
      Resource = resource;
      CommModeBasic = commModeBasic;
      MaxCTO = maxCTO;
      MaxDTO = maxDTO;
      Version = (UInt16)(((UInt16)v1 << 8) + (UInt16)v2);
    }
    public override void changeEndianness()
    {
      MaxDTO = Extensions.changeEndianess(MaxDTO);
      Version = Extensions.changeEndianess(Version);
    }
    public override string ToString()
    {
      return string.Format("XCPVersion={0}.{1} {2}Endian MaxCTO={3} MaxDTO={4} AddressGranularity={5} Resources={6}{7}{8}{9}{10}"
        , (Version >> 8) & 0xff, Version & 0xff
        , (CommModeBasic & CommModeBasic.BigEndian) > 0 ? "Big" : "Little"
        , MaxCTO, MaxDTO
        , (CommModeBasic & CommModeBasic.AddressGranularityWORD) > 0
          ? "WORD"
          : (CommModeBasic & CommModeBasic.AddressGranularityDWORD) > 0
            ? "DWORD"
            : "BYTE"
        , (Resource & ResourceType.CAL_PAG) > 0 ? "CAL_PAG," : string.Empty
        , (Resource & ResourceType.DAQ) > 0 ? "DAQ," : string.Empty
        , (Resource & ResourceType.PGM) > 0 ? "PGM," : string.Empty
        , (Resource & ResourceType.STIM) > 0 ? "STIM," : string.Empty
        , ((CommModeBasic & CommModeBasic.SlaveBlockMode) > 0) ? "Slave Block mode supported" : string.Empty
        );
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdDisconnect : CmdBase
  {
    internal CmdDisconnect() : base(CommandCode.Disconnect) { }
  }
  /// <summary>
  /// Response from Get current session status from slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetStatus : RespBase
  {
    public SessionState SessionState;
    public ResourceType ResourceProtectionState;
    byte Reserved;
    public UInt16 SessionConfigurationId;
    public RespGetStatus() { }
    public RespGetStatus(SessionState sessionState, ResourceType protectionState, UInt16 sessionId)
      : base(PIDSlaveMaster.RES)
    {
      SessionState = sessionState;
      ResourceProtectionState = protectionState;
      SessionConfigurationId = sessionId;
    }
    public override void changeEndianness() { SessionConfigurationId = Extensions.changeEndianess(SessionConfigurationId); }
    /// <summary>
    /// Indicates if slave is currently storing.
    /// </summary>
    public bool IsStoring
    {
      get
      {
        return (SessionState & SessionState.StoreCALRequest) > 0
          || (SessionState & SessionState.StoreDAQRequest) > 0;
      }
    }
    public override string ToString()
    {
      return string.Format("{0}: SessionState={1}\nResourceProtectionState={2}\nSessionID={3}"
        , GetType().Name
        , SessionState.ToString()
        , ResourceProtectionState.ToString()
        , SessionConfigurationId
        );
    }
  }
  /// <summary>
  /// Response from Get current session status from slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetCommModeInfo : RespBase
  {
    byte Reserved1;
    public CommModeOptional CommMode;
    byte Reserved2;
    public byte MaxBS;
    public byte MinST;
    public byte QueueSize;
    public byte DriverVersion;
    public RespGetCommModeInfo() { }
    public RespGetCommModeInfo(CommModeOptional commMode, byte maxBS, byte minST, byte queueSize, byte driverVersion)
      : base(PIDSlaveMaster.RES)
    { CommMode = commMode; MaxBS = maxBS; MinST = minST; QueueSize = queueSize; DriverVersion = driverVersion; }
    public override string ToString()
    {
      switch (CommMode)
      {
        case CommModeOptional.MasterBlockMode | CommModeOptional.InterleavedMode:
          return string.Format("MasterBlockMode (MaxBS={0} MinST={1}), InterLeavedMode (QueueSize={2})", MaxBS, MinST, QueueSize);
        case CommModeOptional.MasterBlockMode:
          return string.Format("MasterBlockMode (MaxBS={0} MinST={1})", MaxBS, MinST, QueueSize);
        case CommModeOptional.InterleavedMode:
          return string.Format("InterLeavedMode (QueueSize={0})", MaxBS, MinST, QueueSize);
      }
      return string.Empty;
    }
  }
  /// <summary>
  /// Possible GetID request types.
  /// </summary>
  public enum GetIDType : byte
  {
    /// <summary>ASCII text</summary>
    ASCII,
    /// <summary>ASAM-MC2 filename without path and extension</summary>
    ASAP2FileNameWithoutExt,
    /// <summary>ASAM-MC2 filename with path and extension</summary>
    ASAP2FileName,
    /// <summary>URL where the ASAM-MC2 file can be found </summary>
    ASAP2URL,
    /// <summary>ASAM-MC2 file to upload</summary>
    ASAP2File2Upload,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetID : CmdBase
  {
    public GetIDType IDType;
    public CmdGetID() { }
    public CmdGetID(GetIDType idType) : base(CommandCode.GetId) { IDType = idType; }
  }
  /// <summary>
  /// Possible GetID transfer types.
  /// </summary>
  [Flags]
  public enum GetIDRespType : byte
  {
    /// <summary>
    /// - If TRANSFER_MODE is 1, the identification is transferred in the remaining bytes of the response.
    /// - If TRANSFER_MODE is 0, the slave device sets the Memory Transfer Address (MTA) to the location from which 
    /// the master device may upload the requested identification using one or more UPLOAD commands.
    /// </summary>
    TRANSFER_MODE = 1,
    /// <summary>
    /// If COMPRESSED_ENCRYPTED is 1, the transferred data are compressed and/or encrypted. This is only allowed for 
    /// identification type 4, i.e. “ASAM-MC2 file to upload”. The XCP master must decompress and/or decrypt the data 
    /// using an implementation specific algorithm, implemented in an externally calculated function.
    /// </summary>
    COMPRESSED_ENCRYPTED = TRANSFER_MODE << 1,
  }
  /// <summary>
  /// Response from Get identification from slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetID : RespBase
  {
    public GetIDRespType Mode;
    UInt16 Reserved;
    public UInt32 Length;
    public RespGetID() { }
    public RespGetID(GetIDRespType mode, UInt32 length) : base(PIDSlaveMaster.RES) { Mode = mode; Length = length; }
    public override void changeEndianness() { Length = Extensions.changeEndianess(Length); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetRequest : CmdBase
  {
    public SetRequestMode Mode;
    public UInt16 SessionId;
    public CmdSetRequest() { }
    internal CmdSetRequest(SetRequestMode mode, UInt16 sessionId) : base(CommandCode.SetRequest) { Mode = mode; SessionId = sessionId; }
    public override void changeEndianness() { SessionId = Extensions.changeEndianess(SessionId); }
  }
  /// <summary>
  /// Possible types of the GetSeed command.
  /// </summary>
  public enum SeedModeType : byte
  {
    /// <summary>Request first part of seed</summary>
    FirstPart,
    /// <summary>Request remaining part of seed</summary>
    RemainingPart,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetSeed : CmdBase
  {
    public SeedModeType Mode;
    public ResourceType Resource;
    public CmdGetSeed() { }
    internal CmdGetSeed(SeedModeType mode, ResourceType resource) : base(CommandCode.GetSeed) { Mode = mode; Resource = resource; }
  }
  /// <summary>
  /// Response from Get seed for unlocking a protected resource
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSeed : RespBase
  {
    public byte Length;
    public RespGetSeed() { }
    public RespGetSeed(byte length) : base(PIDSlaveMaster.RES) { Length = length; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdUnlock : CmdBase
  {
    byte Length;
    internal CmdUnlock(byte length) : base(CommandCode.Unlock) { Length = length; }
  }
  /// <summary>
  /// Response from Send key for unlocking a protected resource
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespUnlock : RespBase
  {
    public ResourceType ProtectionState;
    public RespUnlock() { }
    public RespUnlock(ResourceType protectionState) : base(PIDSlaveMaster.RES) { ProtectionState = protectionState; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetMTA : CmdBase
  {
    UInt16 Reserved;
    byte AddressExtension;
    public UInt32 Address;
    public CmdSetMTA() { }
    internal CmdSetMTA(byte addressExtension, UInt32 address) : base(CommandCode.SetMTA) { Reserved = 0; AddressExtension = addressExtension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdModifyBits : CmdBase
  {
    public byte ShiftValue;
    public UInt16 ANDMask;
    public UInt16 XORMask;
    public CmdModifyBits() { }
    internal CmdModifyBits(byte shiftValue, UInt16 andMask, UInt16 xorMask) 
      : base(CommandCode.ModifyBits)
    { ShiftValue = shiftValue; ANDMask = andMask;XORMask=xorMask; }
    public override void changeEndianness()
    {
      ANDMask = Extensions.changeEndianess(ANDMask);
      XORMask = Extensions.changeEndianess(XORMask);
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdUpload : CmdBase
  {
    public byte NumberOfElements;
    public CmdUpload() { }
    internal CmdUpload(byte numberOfElements) : base(CommandCode.Upload) { NumberOfElements = numberOfElements; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdShortUpload : CmdBase
  {
    public byte NumberOfElements;
    byte Reserved;
    public byte AddressExtension;
    public UInt32 Address;
    public CmdShortUpload() { }
    public CmdShortUpload(byte numberOfElements, byte addressExtension, UInt32 address)
      : base(CommandCode.ShortUpload)
    { NumberOfElements = numberOfElements; AddressExtension = addressExtension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdBuildChecksum : CmdBase
  {
    byte Reserved1;
    UInt16 Reserved2;
    public UInt32 BlockSize;
    public CmdBuildChecksum() { }
    internal CmdBuildChecksum(UInt32 blockSize) : base(CommandCode.BuildChecksum) { BlockSize = blockSize; }
    public override void changeEndianness() { BlockSize = Extensions.changeEndianess(BlockSize); }
  }
  /// <summary>
  /// Response from Build checksum over memory range
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespBuildChecksum : RespBase
  {
    public ChecksumType Type;
    public UInt16 Reserved;
    public UInt32 Checksum;
    public RespBuildChecksum(ChecksumType type, UInt32 checksum)
      : base(PIDSlaveMaster.RES)
    { Type = type; Checksum = checksum; }
    public RespBuildChecksum() { }
    public override void changeEndianness() { Checksum = Extensions.changeEndianess(Checksum); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdDownload : CmdBase
  {
    public byte NumberOfElements;
    public CmdDownload() { }
    internal CmdDownload(CommandCode code, byte len) : base(code) { NumberOfElements = len; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdShortDownload : CmdBase
  {
    public byte NumberOfElements;
    byte Reserved;
    byte AddressExtension;
    public UInt32 Address;
    public CmdShortDownload() { }
    internal CmdShortDownload(byte len, byte addressExtension, UInt32 address)
      : base(CommandCode.ShortDownload)
    { NumberOfElements = len; AddressExtension = addressExtension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetCALPage : CmdBase
  {
    public CalPageMode Mode;
    public byte SegmentNo;
    public byte PageNo;
    public CmdSetCALPage() { }
    internal CmdSetCALPage(CalPageMode mode, byte segmentNo, byte pageNo)
      : base(CommandCode.SetCalPage)
    { Mode = mode; SegmentNo = segmentNo; PageNo = pageNo; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdGetCALPage : CmdBase
  {
    CalPageMode Mode;
    byte SegmentNo;
    internal CmdGetCALPage(CalPageMode mode, byte segmentNo)
      : base(CommandCode.GetCalPage)
    { Mode = mode; SegmentNo = segmentNo; }
  }
  /// <summary>
  /// Response from Get calibration page
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetCALPage : RespBase
  {
    byte Reserved1;
    byte Reserved2;
    public byte PageNo;
    public RespGetCALPage() { }
    public RespGetCALPage(byte pageNo) : base(PIDSlaveMaster.RES) { PageNo = pageNo; }
  }
  /// <summary>
  /// Response from Get general information on PAG processor
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetPAGProcessorInfo : RespBase
  {
    public byte MaxSegment;
    public PAGProperties Properties;
    public RespGetPAGProcessorInfo() { }
    public RespGetPAGProcessorInfo(byte maxSegment, PAGProperties properties)
      : base(PIDSlaveMaster.RES)
    { MaxSegment = maxSegment; Properties = properties; }
  }
  /// <summary>
  /// Posible segment info modes.
  /// </summary>
  public enum SegmentInfoMode : byte
  {
    /// <summary>
    /// Get basic address info.
    /// </summary>
    GetBasicAddress,
    /// <summary>
    /// Get standard info.
    /// </summary>
    GetStandardInfo,
    /// <summary>
    /// Get address mapping.
    /// </summary>
    GetAddressMapping,
  }
  /// <summary>
  /// Possible segment address mode types.
  /// </summary>
  public enum BasicAddressModeType : byte
  {
    /// <summary>
    /// request segment's address.
    /// </summary>
    Address,
    /// <summary>
    /// request segment's length.
    /// </summary>
    Length,
  }
  /// <summary>
  /// Possible mapping info modes
  /// </summary>
  public enum MappingInfoModeType : byte
  {
    SourceAddress,
    DestinationAddress,
    LengthAddress,
  }
  /// <summary>
  /// Defines request modes.
  /// </summary>
  public enum GetSegmentModeType : byte
  {
    /// <summary>
    /// Request address info.
    /// </summary>
    Address,
    /// <summary>
    /// Request standard info.
    /// </summary>
    Standard,
    /// <summary>
    /// Request address mapping.
    /// </summary>
    Mapping,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetSegmentInfo : CmdBase
  {
    public GetSegmentModeType Mode;
    public byte SegmentNo;
    public byte SegmentInfo;
    public byte MappingIndex;
    public CmdGetSegmentInfo() { }
    CmdGetSegmentInfo(GetSegmentModeType mode, byte segmentNo, byte segmentInfo, byte mappingIndex)
      : base(CommandCode.GetSegmentInfo)
    { Mode = mode; SegmentNo = segmentNo; SegmentInfo = segmentInfo; MappingIndex = mappingIndex; }
    public CmdGetSegmentInfo(byte segmentNo)
      : this(GetSegmentModeType.Standard, segmentNo, 0, 0)
    { }
    internal CmdGetSegmentInfo(BasicAddressModeType basicAddressMode, byte segmentNo)
      : this(GetSegmentModeType.Address, segmentNo, (byte)basicAddressMode, 0)
    { }
    internal CmdGetSegmentInfo(MappingInfoModeType mappingInfoMode, byte segmentNo, byte mappingIndex)
      : this(GetSegmentModeType.Mapping, segmentNo, (byte)mappingInfoMode, mappingIndex)
    { }
  }
  /// <summary>
  /// Response from Get address information for a SEGMENT
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSegmentInfo : RespBase
  {
    public byte MaxPages;
    public byte AddressExtension;
    public byte MaxMapping;
    public byte CompressionMethod;
    public byte EncryptionMethod;
    public RespGetSegmentInfo() { }
    public RespGetSegmentInfo(byte maxPages, byte addressExt, byte maxMap, byte compression, byte encryption)
      : base(PIDSlaveMaster.RES)
    { MaxPages = maxPages; AddressExtension = addressExt; MaxMapping = maxMap; CompressionMethod = compression; EncryptionMethod = encryption; }
  }
  /// <summary>
  /// Response from Get standard information for a SEGMENT
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSegmentInfoAddress : RespBase
  {
    byte Reserved1;
    byte Reserved2;
    public UInt32 Info;
    public RespGetSegmentInfoAddress() { }
    public RespGetSegmentInfoAddress(UInt32 info) : base(PIDSlaveMaster.RES) { Info = info; }
    public override void changeEndianness() { Info = Extensions.changeEndianess(Info); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetPageInfo : CmdBase
  {
    public byte SegmentNo;
    public byte PageNo;
    public CmdGetPageInfo() { }
    internal CmdGetPageInfo(byte segmentNo, byte pageNo)
      : base(CommandCode.GetPageInfo)
    { SegmentNo = segmentNo; PageNo = pageNo; }
  }
  /// <summary>
  /// Response from Get specific information for a PAGE
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetPageInfo : RespBase
  {
    public PageProperties Properties;
    public byte InitSegment;
    public RespGetPageInfo() { }
    public RespGetPageInfo(PageProperties pageProperties, byte initSegment)
      : base(PIDSlaveMaster.RES)
    { Properties = pageProperties; InitSegment = initSegment; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdSetSegmentMode : CmdBase
  {
    SegmentMode Mode;
    byte SegmentNo;
    internal CmdSetSegmentMode(SegmentMode mode, byte segmentNo)
      : base(CommandCode.SetSegmentMode)
    { Mode = mode; SegmentNo = segmentNo; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdGetSegmentMode : CmdBase
  {
    byte Reserved;
    byte SegmentNo;
    internal CmdGetSegmentMode(byte segmentNo)
      : base(CommandCode.GetSegmentMode)
    { SegmentNo = segmentNo; }
  }
  /// <summary>
  /// Response from Set mode for a SEGMENT
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSegmentMode : RespBase
  {
    byte Reserved;
    public SegmentMode Mode;
    public RespGetSegmentMode() { }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdCopyCalPage : CmdBase
  {
    public byte SrcSegmentNo;
    public byte SrcPageNo;
    public byte DstSegmentNo;
    public byte DstPageNo;
    public CmdCopyCalPage() { }
    internal CmdCopyCalPage(byte srcSegmentNo, byte srcPageNo, byte dstSegmentNo, byte dstPageNo)
      : base(CommandCode.CopyCALPage)
    { SrcSegmentNo = srcSegmentNo; SrcPageNo = srcPageNo; DstSegmentNo = dstSegmentNo; DstPageNo = dstPageNo; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetDAQPtr : CmdBase
  {
    public byte Reserved;
    public UInt16 DAQListNo;
    public byte ODTNo;
    public byte ODTEntryNo;
    public CmdSetDAQPtr() { }
    internal CmdSetDAQPtr(UInt16 dAQListNo, byte oDTNo, byte oDTEntryNo)
      : base(CommandCode.SetDAQPtr)
    { DAQListNo = dAQListNo; ODTNo = oDTNo; ODTEntryNo = oDTEntryNo; }
    public override void changeEndianness() { DAQListNo = Extensions.changeEndianess(DAQListNo); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdWriteDAQ : CmdBase
  {
    public byte BitOffset;
    public byte ElementSize;
    public byte AddressExtension;
    public UInt32 Address;
    public CmdWriteDAQ() { }
    internal CmdWriteDAQ(byte bitOffset, byte elementSize, byte addressExtension, UInt32 address)
      : base(CommandCode.WriteDAQ)
    { BitOffset = bitOffset; ElementSize = elementSize; AddressExtension = addressExtension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdWriteDAQMultiple : CmdBase
  {
    internal CmdWriteDAQMultiple() : base(CommandCode.WriteDAQMultiple) { }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetDAQListMode : CmdBase
  {
    public DAQListMode Mode;
    public UInt16 DAQListNo;
    public UInt16 EventChannelNo;
    public byte Prescaler;
    public byte Priority;
    public CmdSetDAQListMode() { }
    internal CmdSetDAQListMode(DAQListMode mode, UInt16 dAQListNo, UInt16 eventChannelNo, byte prescaler, byte priority)
      : base(CommandCode.SetDAQListMode)
    { Mode = mode; DAQListNo = dAQListNo; EventChannelNo = eventChannelNo; Prescaler = prescaler; Priority = priority; }
    public override void changeEndianness()
    {
      DAQListNo = Extensions.changeEndianess(DAQListNo);
      EventChannelNo = Extensions.changeEndianess(EventChannelNo);
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdStartStopDAQList : CmdBase
  {
    public StartStopMode Mode;
    public UInt16 DAQListNo;
    public CmdStartStopDAQList() { }
    internal CmdStartStopDAQList(StartStopMode mode, UInt16 dAQListNo)
      : base(CommandCode.StartStopDAQList)
    { Mode = mode; DAQListNo = dAQListNo; }
    public override void changeEndianness() { DAQListNo = Extensions.changeEndianess(DAQListNo); }
  }
  /// <summary>
  /// Response from Start/stop/select DAQ list
  /// 
  /// - According to the specification, the FirstPID paramter could be ignored in some cases.
  /// - On some ECUs the FirstPID is parameter seems to be completely missing in the response...
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespStartStopDAQList : RespBase
  {
    public byte FirstPID;
    public RespStartStopDAQList() { }
    public RespStartStopDAQList(byte firstPID) : base(PIDSlaveMaster.RES) { FirstPID = firstPID; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdStartStopSynch : CmdBase
  {
    public StartStopMode Mode;
    public CmdStartStopSynch() { }
    internal CmdStartStopSynch(StartStopMode mode)
      : base(CommandCode.StartStopSynch)
    { Mode = mode; }
  }
  /// <summary>
  /// Response from Read element from ODT entry
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespReadDAQ : RespBase
  {
    public byte BitOffset;
    public byte ElementSize;
    public Byte AddressExtension;
    public UInt32 Address;
    public RespReadDAQ() { }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  /// <summary>
  /// Response from Get DAQ clock from slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQClock : RespBase
  {
    byte reserved;
    byte triggerInfo;
    public PayloadFmt PayloadFmt;
    public UInt32 Timestamp;
    public RespGetDAQClock() { }
    public RespGetDAQClock(UInt32 timestamp) : base(PIDSlaveMaster.RES) { Timestamp = timestamp; }
    public TriggerInitiator TriggerInitiator { get { return (TriggerInitiator)(triggerInfo & 0x07); } }
    public TimeOfTSSampling TimeOfTSSampling { get { return (TimeOfTSSampling)((triggerInfo >> 3) & 0x03); } }
    public override void changeEndianness() { Timestamp = Extensions.changeEndianess(Timestamp); }
  }
  /// <summary>
  /// Response from Get general information on DAQ processor
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQProcessorInfo : RespBase
  {
    public DAQProperties Properties;
    public UInt16 MaxDAQ;
    public UInt16 MaxEventChannel;
    public byte MinDAQ;
    public DAQKeyByte DAQKeyByte;
    public RespGetDAQProcessorInfo() { }
    public RespGetDAQProcessorInfo(DAQProperties properties, UInt16 maxDAQ, UInt16 maxEvt, byte minDAQ, DAQKeyByte keyByte)
      : base(PIDSlaveMaster.RES)
    {
      Properties = properties; MaxDAQ = maxDAQ; MaxEventChannel = maxEvt; MinDAQ = minDAQ; DAQKeyByte = keyByte;
    }
    public override void changeEndianness()
    {
      MaxDAQ = Extensions.changeEndianess(MaxDAQ);
      MaxEventChannel = Extensions.changeEndianess(MaxEventChannel);
    }
  }
  /// <summary>
  /// Response from Get general information on DAQ processing resolution
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQResolutionInfo : RespBase
  {
    public XCP_ODT_ENTRY_SIZE GranularityODTEntrySizeDAQ;
    public byte MaxODTEntrySizeDAQ;
    public XCP_ODT_ENTRY_SIZE GranularityODTEntrySizeSTIM;
    public byte MaxODTEntrySizeSTIM;
    public DAQTimestampMode TimestampMode;
    public UInt16 TimestampTicks;
    public RespGetDAQResolutionInfo(XCP_ODT_ENTRY_SIZE granularityODTEntrySizeDAQ
      , byte maxODTEntrySizeDAQ
      , XCP_ODT_ENTRY_SIZE granularityODTEntrySizeSTIM
      , byte maxODTEntrySizeSTIM
      , DAQTimestampMode timestampMode
      , UInt16 timestampTicks)
      : base(PIDSlaveMaster.RES)
    {
      GranularityODTEntrySizeDAQ = granularityODTEntrySizeDAQ;
      MaxODTEntrySizeDAQ = maxODTEntrySizeDAQ;
      GranularityODTEntrySizeSTIM = granularityODTEntrySizeSTIM;
      MaxODTEntrySizeSTIM = maxODTEntrySizeSTIM;
      TimestampMode = timestampMode;
      TimestampTicks = timestampTicks;
    }
    public RespGetDAQResolutionInfo() { }
    public override void changeEndianness() { TimestampTicks = Extensions.changeEndianess(TimestampTicks); }
    /// <summary>
    /// Gets the timestamp size in bytes.
    /// </summary>
    /// <returns></returns>
    public int getTSSize()
    {
      byte size = (byte)TimestampMode;
      return size & 0x07;
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdGetDAQListMode : CmdBase
  {
    byte Reserved;
    UInt16 DAQListNo;
    internal CmdGetDAQListMode(UInt16 dAQListNo)
      : base(CommandCode.GetDAQListMode)
    { DAQListNo = dAQListNo; }
    public override void changeEndianness() { DAQListNo = Extensions.changeEndianess(DAQListNo); }
  }
  /// <summary>
  /// Response from Get mode from DAQ list
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQListMode : RespBase
  {
    public DAQListMode Mode;
    UInt16 Reserved;
    public UInt16 EventChannelNo;
    public byte Prescaler;
    public byte Priority;
    public RespGetDAQListMode() { }
    public override void changeEndianness() { EventChannelNo = Extensions.changeEndianess(EventChannelNo); }
  }
  /// <summary>
  /// Response from Get specific information for an event channel
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQEventInfo : RespBase
  {
    public DAQEventProperties Properties;
    public byte MaxDAQList;
    public byte NameLength;
    public byte TimeCycle;
    public byte TimeUnit;
    public byte Priority;
    public RespGetDAQEventInfo() { }
    public RespGetDAQEventInfo(DAQEventProperties properties, byte maxDAQList, byte nameLength, byte timeCycle, byte timeUnit, byte priority)
      : base(PIDSlaveMaster.RES)
    { Properties = properties; MaxDAQList = maxDAQList; NameLength = nameLength; TimeCycle = timeCycle; TimeUnit = timeUnit; Priority = priority; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdList : CmdBase
  {
    byte Reserved;
    public UInt16 ListNo;
    public CmdList() { }
    internal CmdList(CommandCode code, UInt16 listNo)
      : base(code)
    { ListNo = listNo; }
    public override void changeEndianness() { ListNo = Extensions.changeEndianess(ListNo); }
  }
  /// <summary>
  /// Response from Get specific information for a DAQ list
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQListInfo : RespBase
  {
    public DAQListProperties Properties;
    public byte MaxODT;
    public byte MaxODTEntries;
    public UInt16 FixedEvent;
    public RespGetDAQListInfo() { }
    public RespGetDAQListInfo(DAQListProperties properties, byte maxODT, byte maxODTEntries, UInt16 fixedEvent)
      : base(PIDSlaveMaster.RES)
    { Properties = properties; MaxODT = maxODT; MaxODTEntries = maxODTEntries; FixedEvent = fixedEvent; }
    public override void changeEndianness() { FixedEvent = Extensions.changeEndianess(FixedEvent); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdAllocateDAQ : CmdBase
  {
    byte Reserved;
    public UInt16 DAQCount;
    public CmdAllocateDAQ() { }
    internal CmdAllocateDAQ(UInt16 dAQCount)
      : base(CommandCode.AllocDAQ)
    { DAQCount = dAQCount; }
    public override void changeEndianness() { DAQCount = Extensions.changeEndianess(DAQCount); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdAllocateODT : CmdBase
  {
    byte Reserved;
    public UInt16 DAQListNo;
    public byte ODTCount;
    public CmdAllocateODT() { }
    internal CmdAllocateODT(UInt16 dAQListNo, byte oDTCount)
      : base(CommandCode.AllocODT)
    { DAQListNo = dAQListNo; ODTCount = oDTCount; }
    public override void changeEndianness() { DAQListNo = Extensions.changeEndianess(DAQListNo); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdAllocateODTEntry : CmdBase
  {
    byte Reserved;
    public UInt16 DAQListNo;
    public byte ODTNo;
    public byte ODTEntriesCount;
    public CmdAllocateODTEntry() { }
    internal CmdAllocateODTEntry(UInt16 dAQListNo, byte oDTNo, byte oDTEntriesCount)
      : base(CommandCode.AllocODTEntry)
    { DAQListNo = dAQListNo; ODTNo = oDTNo; ODTEntriesCount = oDTEntriesCount; }
    public override void changeEndianness() { DAQListNo = Extensions.changeEndianess(DAQListNo); }
  }
  /// <summary>
  /// Response from Indicate the beginning of a programming sequence
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespProgramStart : RespBase
  {
    byte Reserved;
    public CommModeProgram CommMode;
    public byte MaxCTO;
    public byte MaxBS;
    public byte MinST;
    public byte QueueSize;
    public RespProgramStart() { }
    public RespProgramStart(CommModeProgram commMode, byte maxCTO, byte maxBS, byte minST, byte queueSize)
      : base(PIDSlaveMaster.RES)
    { CommMode = commMode; MaxCTO = maxCTO; MaxBS = maxBS; MinST = minST; QueueSize = queueSize; }
  }
  /// <summary>
  /// Possible modes to program clear.
  /// </summary>
  public enum ProgramClearMode : byte
  {
    [Description("Absolute")]
    AbsoluteAccess,
    [Description("Functional")]
    FunctionalAccess,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdProgramClear : CmdBase
  {
    ProgramClearMode Mode;
    UInt16 Reserved;
    UInt32 ClearRange;
    internal CmdProgramClear(ProgramClearMode mode, UInt32 clearRange)
      : base(CommandCode.ProgramClear)
    { Mode = mode; ClearRange = clearRange; }
    public override void changeEndianness() { ClearRange = Extensions.changeEndianess(ClearRange); }
  }
  /// <summary>
  /// Response from Get general information on PGM processor
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetPGMProcessorInfo : RespBase
  {
    public ProgramProperties Properties;
    public byte MaxSector;
    public RespGetPGMProcessorInfo() { }
    public RespGetPGMProcessorInfo(ProgramProperties properties, byte maxSector)
      : base(PIDSlaveMaster.RES)
    { Properties = properties; MaxSector = maxSector; }
  }
  /// <summary>
  /// Possible sector info modes.
  /// </summary>
  public enum GetSectorInfoMode : byte
  {
    StartAddress,
    Length,
    NameLength,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdGetSectorInfo : CmdBase
  {
    GetSectorInfoMode Mode;
    byte SectorNo;
    internal CmdGetSectorInfo(GetSectorInfoMode mode, byte sectorNo)
      : base(CommandCode.GetSectorInfo)
    { Mode = mode; SectorNo = sectorNo; }
  }
  /// <summary>
  /// Response from Get specific information for a SECTOR
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSectorInfoModeAddressOrLen : RespBase
  {
    public byte ClearSequenceNo;
    public byte ProgramSequenceNo;
    public byte ProgrammingMethod;
    public UInt32 SectorInfo;
    public RespGetSectorInfoModeAddressOrLen() { }
    public override void changeEndianness() { SectorInfo = Extensions.changeEndianess(SectorInfo); }
  }
  /// <summary>
  /// Response from Get specific information for a SECTOR
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSectorInfoModeSectorNameLen : RespBase
  {
    public byte SectorNameLen;
    public RespGetSectorInfoModeSectorNameLen() { }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdProgramPrepare : CmdBase
  {
    byte Reserved;
    UInt16 CodeSize;
    internal CmdProgramPrepare(UInt16 codeSize)
      : base(CommandCode.ProgramPrepare)
    { CodeSize = codeSize; }
    public override void changeEndianness() { CodeSize = Extensions.changeEndianess(CodeSize); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdProgramFormat : CmdBase
  {
    byte CompressionMethod;
    byte EncryptionMethod;
    byte ProgrammingMethod;
    byte AccessMethod;
    internal CmdProgramFormat(byte compressionMethod, byte encryptionMethod, byte programmingMethod, byte accessMethod)
      : base(CommandCode.ProgramFormat)
    { CompressionMethod = compressionMethod; EncryptionMethod = encryptionMethod; ProgrammingMethod = programmingMethod; AccessMethod = accessMethod; }
  }
  /// <summary>
  /// Possible verification modes.
  /// </summary>
  public enum ProgramVerifyMode : byte
  {
    /// <summary>
    ///  The master can request the slave to start internal test routines to 
    ///  check whether the new flash contents fits to the rest of the flash. 
    ///  Only the result is of interest.
    /// </summary>
    [Description("Request to start internal Routine")]
    RequestToStartInternalRoutine,
    /// <summary>
    /// The Master can tell the slave that he will be sending a Verification 
    /// Value to the slave.
    /// </summary>
    [Description("Sending verification Value")]
    SendingVerificationValue,
    /// <summary>
    /// No verification.
    /// </summary>
    None,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  internal sealed class CmdProgramVerify : CmdBase
  {
    ProgramVerifyMode Mode;
    UInt16 VerificationType;
    UInt32 VerificationValue;
    internal CmdProgramVerify(ProgramVerifyMode mode, UInt16 verificationType, UInt32 verificationValue)
      : base(CommandCode.ProgramVerify)
    { Mode = mode; VerificationType = verificationType; VerificationValue = verificationValue; }
    public override void changeEndianness()
    {
      VerificationType = Extensions.changeEndianess(VerificationType);
      VerificationValue = Extensions.changeEndianess(VerificationValue);
    }
  }
  /// <summary>
  /// Used for CommandCode.UserCmd and CommandCode.TransportLayerCmd.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdUserTransCmd : CmdBase
  {
    public byte SubCommand;
    public CmdUserTransCmd() { }
    internal CmdUserTransCmd(CommandCode code, byte subCommand)
      : base(code)
    { SubCommand = subCommand; }
  }
  #endregion
  #region events
  /// <summary>
  /// Event START IN RESUME MODE
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class EventResumeMode : EventBase
  {
    public UInt16 SessionId;
    public override void changeEndianness() { SessionId = Extensions.changeEndianess(SessionId); }
  }
  /// <summary>
  /// Event START IN RESUME MODE.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class EventResumeModeTS : EventResumeMode
  {
    public UInt32 CurrentTimestamp;
    public override void changeEndianness()
    {
      base.changeEndianness();
      CurrentTimestamp = Extensions.changeEndianess(CurrentTimestamp);
    }
  }
  /// <summary>
  /// Event TRANSFER OF EXTERNALLY TRIGGERED TIMESTAMP.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class EventTimeSync : EventBase
  {
    byte triggerInfo;
    public PayloadFmt PayloadFmt;
    public UInt32 Timestamp;
    public EventTimeSync() { }
    public EventTimeSync(uint timestamp) : base(EventCodes.TimeSync) { Timestamp = timestamp; }
    public TriggerInitiator TriggerInitiator { get { return (TriggerInitiator)(triggerInfo & 0x07); } }
    public TimeOfTSSampling TimeOfTSSampling { get { return (TimeOfTSSampling)((triggerInfo >> 3) & 0x03); } }
    public override void changeEndianness() { Timestamp = Extensions.changeEndianess(Timestamp); }
  }
  /// <summary>
  /// Possible STIM timeout modes.
  /// </summary>
  public enum STIMTimeoutMode : byte
  {
    EventChannelNo,
    DAQListNo,
  }
  /// <summary>
  /// Event INDICATION OF TIME-OUT AT STIM.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class EventStimTimeout : EventBase
  {
    STIMTimeoutMode Mode;
    byte Reserved;
    UInt16 No;
    public override void changeEndianness() { No = Extensions.changeEndianess(No); }
  }
  #endregion
}