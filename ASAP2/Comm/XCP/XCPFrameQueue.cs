﻿/*!
 * @file    
 * @brief   Implements a Queue to store XCPFrame's.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2.Properties;
using log4net;

namespace jnsoft.Comm.XCP
{
  /// <summary>
  /// Queue implementation to store XCPFrame's.
  /// </summary>
  public sealed class XCPFrameQueue : FrameQueue<XCPFrame>
  {
    static ILog mLogger = LogManager.GetLogger(typeof(XCPFrameQueue));
    static XCPFrameQueue mInstance;
    public override string LogFileName { get { return "XCPLog.csv"; } }
    public override string LogFileCSVHeader { get { return "Time[s];Source;Len;Ctr;Type;XCP payload;ASCII"; } }
    /// <summary>
    /// Returns the singleton instance of the queue.
    /// </summary>
    public static XCPFrameQueue Instance { get { return mInstance ?? (mInstance = new XCPFrameQueue()); } }
    /// <summary>
    /// Adds a sent or received frame to the frame queue.
    /// 
    /// This method gets called from various receive threads, 
    /// therefore access to members is synchronized.
    /// 
    /// The frames are filtered according the global user settings.
    /// </summary>
    /// <param name="frame">The frame to add</param>
    public void enqueueFrame(XCPFrame frame)
    {
      // keep only filtered items in memory
      bool isDAQ = frame.IsDAQ;
      if (!Settings.Default.XCPLogShowDAQ && isDAQ)
        return;
      if (!Settings.Default.XCPLogShowCMD && frame.IsMasterFrame)
        return;
      if (!Settings.Default.XCPLogShowRES && !frame.IsMasterFrame && !isDAQ)
        return;

      addFrame(frame);
    }
  }
}