﻿/*!
 * @file    
 * @brief   Implements common XCP/CCP data acquisition stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    January 2016
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;

namespace jnsoft.Comm
{
  /// <summary>
  /// Represents an ODT entry.
  /// 
  /// - Holds configuration data to configure an ODT entry.
  /// - Holds received history data of an A2LMEASUREMENT.
  /// </summary>
  public sealed class ODTEntry
  {
    #region members
    /// <summary>
    /// Maximum data points in memory for a single measurement.
    /// </summary>
    const int MAX_POINTS_IN_MEMORY = 100000;
    DataPointBuffer mDPBuffer;
    readonly object mLockObj = new object();
    #endregion
    #region constructors
    /// <summary>
    /// Creates a new instance of an ODTEntry with the specified parameters.
    /// </summary>
    /// <param name="daqNo">THe parent DAQ Number</param>
    /// <param name="measurement">The measurement</param>
    /// <param name="arrayOffset">the (array) offset to the specified measurement base address (ArraySize > 1)</param>
    /// <param name="dataOffset">The offset within the measurements data</param>
    /// <param name="size">Size in bytes</param>
    public ODTEntry(UInt16 daqNo, A2LMEASUREMENT measurement, UInt32 arrayOffset, UInt32 dataOffset, byte size)
    {
      DAQNo = daqNo;
      Measurement = measurement;
      ArrayOffset = arrayOffset;
      DataOffset = dataOffset;
      Size = size;
      BitOffset = 0xff;
    }
    #endregion
    #region properties
    /// <summary>
    /// Received count of data, The next index to write received data to.
    /// </summary>
    public int Count
    {
      get
      {
        lock (mLockObj)
          return mDPBuffer != null ? (int)mDPBuffer.Count : 0;
      }
    }
    /// <summary>The parent DAQ Number</summary>
    public UInt16 DAQNo { get; }
    /// <summary>
    /// Gets the corresponding measurement.
    /// </summary>
    public A2LMEASUREMENT Measurement { get; }
    /// <summary>
    /// The absolute address of the ODTEntry.
    /// </summary>
    public UInt32 Address { get { return Measurement.Address + ArrayOffset + DataOffset; } }
    /// <summary>
    /// Gets the offset to the specified measurement base address (> 0 if ArraySize of the base measurement is > 1).
    /// </summary>
    public UInt32 ArrayOffset { get; }
    /// <summary>
    /// Gets the offset to the specified measurement address (> 0 is only a part of a measurement).
    /// </summary>
    public UInt32 DataOffset { get; }
    /// <summary>
    /// Gets the size of this ODT entry.
    /// </summary>
    public byte Size { get; }
    /// <summary>
    /// Gets the current value depending on the current time (double.NaN if nothing is received yet).
    /// </summary>
    public double Value
    {
      get
      {
        lock (mLockObj)
        {
          if (mDPBuffer == null)
            return double.NaN;
          var dp = new DataPoint();
          int index = mDPBuffer.findTimeIndex((long)(TimeBase.getDAQTime() * TimeSpan.TicksPerSecond));
          mDPBuffer.getValue(index, ref dp);
          return dp.Y;
        }
      }
    }
    /// <summary>
    /// Gets the bit offset value.
    /// </summary>
    public byte BitOffset { get; }
    #endregion
    #region methods
    /// <summary>
    /// Gets all data points between the start- and end timestamp.
    /// 
    /// - The data is delivered as raw data.
    /// </summary>
    /// <param name="startTime">The start timestamp to search for</param>
    /// <param name="endTime">The end timestamp to search for</param>
    /// <param name="maxPoints">
    /// [in] - The maximum number of points to get from the call
    /// [out] - The number of points returned in points
    /// </param>
    /// <param name="points">
    /// <para>[in/out]An IntPtr (as memory) for each channel to store the data into.
    /// The IntPtr should provide enough memory for DataPoint.Size (8 Bytes * maxPoints[n])</para>
    /// <para><b>Attention: The application memory gets corrupted if the provided size is too small to hold the requested size of DataPoints!</b></para>
    /// </param>
    public void getData(long startTime, long endTime
      , ref int maxPoints
      , IntPtr points
      )
    {
      lock (mLockObj)
      {
        if (mDPBuffer == null)
          return;
        int firstIndex, lastIndex;
        mDPBuffer.findTimeIndex(endTime, startTime, out lastIndex, out firstIndex);
        if (firstIndex > 0)
          --firstIndex;
        if (lastIndex < mDPBuffer.Count - 1)
          ++lastIndex;
        maxPoints = Math.Min(maxPoints, lastIndex - firstIndex + 1);
        mDPBuffer.getValues(points, firstIndex, maxPoints);

        if (maxPoints <= 0 || Measurement.BitMask == UInt64.MaxValue)
          return;

        unsafe
        { // handle bitmasked values
          int shiftCount = Extensions.getShiftCount(Measurement.BitMask);
          IntPtr pPtr = points;
          for (int i = 0; i < maxPoints; ++i, pPtr = IntPtr.Add(pPtr, DataPoint.Size))
          { // iterate over all values read
            var p = (DataPoint*)pPtr;
            UInt64 rawUIValue = Convert.ToUInt64(p->Y);
            rawUIValue &= Measurement.BitMask;
            rawUIValue >>= shiftCount;
            p->Y = rawUIValue;
          }
        }
      }
    }
    /// <summary>
    /// Gets all data points between the start- and end timestamp.
    /// 
    /// - The data is delivered as physical data.
    /// </summary>
    /// <param name="startTime">The start timestamp to search for</param>
    /// <param name="endTime">The end timestamp to search for</param>
    /// <param name="maxPoints">
    /// [in] - The maximum number of points to get from the call
    /// [out] - The number of points returned in points
    /// </param>
    /// <param name="min">The physical minimum within the returned data (only valid if maxPoints > 0)</param>
    /// <param name="max">The physical maximum within the returned data (only valid if maxPoints > 0)</param>
    /// <param name="points">
    /// <para>[in/out]An IntPtr (as memory) for each channel to store the data into.
    /// The IntPtr should provide enough memory for DataPoint.Size (8 Bytes * maxPoints[n])</para>
    /// <para><b>Attention: The application memory gets corrupted if the provided size is too small to hold the requested size of DataPoints!</b></para>
    /// </param>
    public unsafe void getPhysicalData(long startTime, long endTime
      , ref int maxPoints
      , out double min
      , out double max
      , IntPtr points
      )
    {
      min = Measurement.LowerLimit; max = Measurement.UpperLimit;
      getData(startTime, endTime, ref maxPoints, points);
      var compuMethod = Measurement.RefCompuMethod;

      if (maxPoints <= 0)
        return;

      bool compute = compuMethod.ConversionType != CONVERSION_TYPE.IDENTICAL;
      min = double.MaxValue; max = double.MinValue;
      IntPtr pPtr = points;
      for (int i = 0; i < maxPoints; ++i, pPtr = IntPtr.Add(pPtr, DataPoint.Size))
      { // iterate over all values read
        var p = (DataPoint*)pPtr;
        if (compute)
          p->Y = compuMethod.toPhysical(p->Y);
        min = Math.Min(min, p->Y);
        max = Math.Max(max, p->Y);
      }
    }
    /// <summary>
    /// Clears all data received within the ODTEntry.
    /// </summary>
    public void clearData()
    {
      lock (mLockObj)
      {
        if (mDPBuffer != null)
        {
          mDPBuffer.Dispose();
          mDPBuffer = null;
        }
      }
    }
    #region add data methods
    /// <summary>
    /// Adds the received value with the specified timestamp to mHistoryValues.
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="rawValue">The raw value</param>
    void addValue(long time, double rawValue)
    {
      lock (mLockObj)
      {
        if (null == mDPBuffer)
          mDPBuffer = new DataPointBuffer(Measurement.Address.ToString("X8"), MAX_POINTS_IN_MEMORY);
        var dp = new DataPoint(time, rawValue);
        mDPBuffer.add(dp);
      }
    }
    /// <summary>
    /// Sets a new received value from data acquisition.
    /// 
    /// This method may be called from different receive threads!
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="rawValue">The raw value (endianess is already corrected)</param>
    public void setValue(long time, byte rawValue)
    {
      switch (Measurement.DataType)
      {
        case DATA_TYPE.UBYTE: addValue(time, rawValue); break;
        case DATA_TYPE.SBYTE: addValue(time, (sbyte)rawValue); break;
      }
    }
    /// <summary>
    /// Sets a new received value from data acquisition.
    /// 
    /// - This method is used for float and double values.
    /// - This method may be called from different receive threads!
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="data">The received data</param>
    /// <param name="offset">The offset to the value within the received data</param>
    /// <param name="len">The length of the received data</param>
    /// <exception cref="ArgumentException">Thrown, if the len parameter value is not supported</exception>
    public void setValue(long time, byte[] data, int offset, int len)
    {
      switch (len)
      {
        case 4: addValue(time, BitConverter.ToSingle(data, offset)); break;
        case 8: addValue(time, BitConverter.ToDouble(data, offset)); break;
        default: throw new ArgumentException("Only values of 4 or 8 are supported", "len");
      }
    }
    /// <summary>
    /// Sets a new received value from data acquisition.
    /// 
    /// This method may be called from different receive threads!
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="rawValue">The raw value (endianess is already corrected)</param>
    public void setValue(long time, UInt16 rawValue)
    {
      switch (Measurement.DataType)
      {
        case DATA_TYPE.UWORD: addValue(time, rawValue); break;
        case DATA_TYPE.SWORD: addValue(time, (Int16)rawValue); break;
      }
    }
    /// <summary>
    /// Sets a new received value from data acquisition.
    /// 
    /// This method may be called from different receive threads!
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="rawValue">The raw value (endianess is already corrected)</param>
    /// <exception cref="ArgumentException">Thrown, if the data type doesn't match the argument size</exception>
    public void setValue(long time, UInt32 rawValue)
    {
      switch (Measurement.DataType)
      {
        case DATA_TYPE.ULONG: addValue(time, rawValue); break;
        case DATA_TYPE.SLONG: addValue(time, (Int32)rawValue); break;
        default: throw new ArgumentException(Measurement.DataType.ToString(), "rawValue");
      }
    }
    /// <summary>
    /// Sets a new received value from data acquisition.
    /// 
    /// This method may be called from different receive threads!
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <param name="rawValue">The raw value (endianess is already corrected)</param>
    /// <exception cref="ArgumentException">Thrown, if the data type doesn't match the argument size</exception>
    public void setValue(long time, UInt64 rawValue)
    {
      switch (Measurement.DataType)
      {
        case DATA_TYPE.A_UINT64: addValue(time, rawValue); break;
        case DATA_TYPE.A_INT64: addValue(time, (Int64)rawValue); break;
        default: throw new ArgumentException(Measurement.DataType.ToString(), "rawValue");
      }
    }
    #endregion
    #endregion
  }
  /// <summary>
  /// Dictionary mapping physical addresses to its corresponding ODTEntries.
  /// 
  /// This is used to gain fast acces from a measurement's address to its corresponding ODT Entry.
  /// </summary>
  public sealed class ODTEntryDictType : Dictionary<UInt32, ODTEntry> { }

  /// <summary>
  /// Cache entry for received DAQ Records.
  /// </summary>
  struct CacheEntry
  {
    /// <summary>Offset from start of record to start of data</summary>
    public int Offset;
    /// <summary>Count of unused bytes at the end of the record</summary>
    public int UnusedBytes;
    /// <summary>Received DAQ record</summary>
    public byte[] Data;
    /// <summary>
    /// Initialises a new instance of CacheEntry.
    /// </summary>
    /// <param name="data">Received DAQ record</param>
    /// <param name="offset">Offset from start of record to start of data</param>
    /// <param name="unusedBytes">Count of unused bytes at the end of the record</param>
    internal CacheEntry(ref byte[] data, int offset, int unusedBytes)
    { Data = data; Offset = offset; UnusedBytes = unusedBytes; }
  }
  /// <summary>
  /// DAQ Record cache.
  /// 
  /// Used to collect DAQ records until all records of a DAQList are complete.
  /// </summary>
  sealed class DAQRecordCacheType : SortedDictionary<byte, CacheEntry>
  {
    #region members
    byte[] mLinearBytes;
    #endregion
    #region methods
    /// <summary>
    /// Adds data to the cache.
    /// </summary>
    /// <param name="data">The data stream from the received DAQ record. The first byte is always interpreted as PID</param>
    /// <param name="offset">Offset from start of record to start of data</param>
    /// <param name="unusedBytes">Count of unused bytes at the end of the record</param>
    public void Add(ref byte[] data, int offset, int unusedBytes)
    {
      this[data[0]] = new CacheEntry(ref data, offset, unusedBytes);
    }
    /// <summary>
    /// Gets the received data as a flat stream.
    /// </summary>
    /// <returns>The stream instance, null if incomplete</returns>
    public byte[] getData()
    {
      // complete
      int size = 0;
      try
      { // get size
        foreach (var entry in Values)
          size += (entry.Data.Length - entry.Offset - entry.UnusedBytes);

        if (mLinearBytes == null || mLinearBytes.Length < size)
          mLinearBytes = new byte[size];

        // copy bytes
        int offset = 0, currPID = -1;
        foreach (var entry in Values)
        {
          byte[] pidData = entry.Data;

          if (currPID >= 0 && pidData[0] - 1 != currPID)
            // incomplete data
            return null;

          currPID = pidData[0];
          int srcLen = pidData.Length - entry.Offset - entry.UnusedBytes;
          Array.Copy(pidData, entry.Offset, mLinearBytes, offset, srcLen);
          offset += srcLen;
        }
      }
      finally { Clear(); }
      return size > 0 ? mLinearBytes : null;
    }
    #endregion
  }

  /// <summary>
  /// Holds the configured ODTEntries within an ODT.
  /// </summary>
  public sealed class ODTList : List<ODTEntry>
  {
    #region members
    /// <summary>Relative PID number (means PID = 0 is always the first ODT)</summary>
    internal byte PID;
    /// <summary>Remaining free bytes of ODT</summary>
    internal int FreeBytes;
    /// <summary>Remaining free entries of ODT</summary>
    internal int FreeEntries;
    /// <summary>Length of configured ODT Data in bytes</summary>
    internal UInt16 DataLength;
    #endregion
    #region ctor
    /// <summary>Creates a new instance of ODTList with the specified parameters</summary>
    /// <param name="pid">The PID</param>
    /// <param name="maxBytes">Maximum bytes in ODT</param>
    /// <param name="maxEntries">Maximum entries in ODT</param>
    internal ODTList(byte pid, int maxBytes, int maxEntries)
    { PID = pid; FreeBytes = maxBytes; FreeEntries = maxEntries; }
    #endregion
    #region methods
    /// <summary>
    /// Test if the specified entry fits in.
    /// </summary>
    /// <param name="entry">The ODT entry to test</param>
    /// <returns>true if the ODT entry could be added</returns>
    internal bool fitsIn(ODTEntry entry)
    {
      return FreeBytes >= entry.Size && FreeEntries > 0;
    }
    /// <summary>
    /// Adds a new ODT entry.
    /// </summary>
    /// <param name="entry">new ODT entry</param>
    internal new void Add(ODTEntry entry)
    {
      FreeBytes -= entry.Size;
      FreeEntries--;
      DataLength += entry.Size;
      base.Add(entry);
    }
    #endregion
  }
  /// <summary>
  /// DAQ List representation base class for both XCP and CCP.
  /// 
  /// Holds a dictionary of configured ODTs.
  /// </summary>
  public abstract class DAQList
  {
    #region members
    internal double LastReceivedRelSecs = double.NaN;
    /// <summary>List of configured ODTs</summary>
    public SortedDictionary<byte, ODTList> ODTs = new SortedDictionary<byte, ODTList>();
    /// <summary>Received DAQ records cache for this DAQ List</summary>
    internal DAQRecordCacheType DAQRecordCache = new DAQRecordCacheType();
    #endregion
    #region properties
    /// <summary>
    /// The (unique) DAQ number.
    /// </summary>
    public UInt16 DAQNo { get; protected set; }
    /// <summary>
    /// Number of the FirstPID.
    /// 
    /// Usually 0, except in absolute PID mode (XCP_ID_FIELD_TYPE.ABSOLUTE).
    /// </summary>
    public byte FirstPID { get; internal set; }
    #endregion
    #region methods
    /// <summary>
    /// Receives the complete DAQList record.
    /// </summary>
    /// <param name="data">all ODTs, all configured values, consequtively ordered in this byte stream</param>
    /// <param name="changeEndianess">true if byte order should be adjusted</param>
    internal void onValuesComplete(ref byte[] data, bool changeEndianess)
    {
      try
      {
        long time = TimeSpan.FromSeconds(LastReceivedRelSecs).Ticks;
        int offset = 0;
        var enODT = ODTs.GetEnumerator();
        while (enODT.MoveNext())
        {
          for (int i = 0; i < enODT.Current.Value.Count; ++i)
          {
            var entry = enODT.Current.Value[i];
            if (entry.DataOffset > 0)
              // Only data part, goto next
              continue;

            var dataType = entry.Measurement.DataType;
            int length = dataType.getSizeInByte();
            if (offset + length > data.Length)
              break;
            switch (length)
            {
              case 1: entry.setValue(time, data[offset]); break;
              case 2:
                // 16 Bit
                UInt16 value16 = BitConverter.ToUInt16(data, offset);
                if (changeEndianess)
                  value16 = Extensions.changeEndianess(value16);
                entry.setValue(time, value16);
                break;
              case 4:
                // 32 Bit
                switch (dataType)
                {
                  case DATA_TYPE.FLOAT32_IEEE:
                    var result = new byte[4];
                    Array.Copy(data, offset, result, 0, length);
                    if (changeEndianess)
                      result = Extensions.changeEndianess(result, 0, length);
                    entry.setValue(time, result, 0, length);
                    break;
                  default:
                    UInt32 value32 = BitConverter.ToUInt32(data, offset);
                    if (changeEndianess)
                      value32 = Extensions.changeEndianess(value32);
                    entry.setValue(time, value32);
                    break;
                }
                break;
              case 8:
                // 64 Bit
                switch (dataType)
                {
                  case DATA_TYPE.FLOAT64_IEEE:
                    var result = new byte[8];
                    Array.Copy(data, offset, result, 0, length);
                    if (changeEndianess)
                      result = Extensions.changeEndianess(result, 0, length);
                    entry.setValue(time, result, 0, length);
                    break;
                  default:
                    UInt64 value64 = BitConverter.ToUInt64(data, offset);
                    if (changeEndianess)
                      value64 = Extensions.changeEndianess(value64);
                    entry.setValue(time, value64);
                    break;
                }
                break;
            }
            offset += length;
          }
        }
      }
      catch (IndexOutOfRangeException) { }
      catch (ArgumentException) { }
    }
    /// <summary>
    /// Clears only the contained data, not the DAQ configuration.
    /// </summary>
    public virtual void clearData()
    {
      LastReceivedRelSecs = double.NaN;
      DAQRecordCache.Clear();
      var enODT = ODTs.GetEnumerator();
      while (enODT.MoveNext())
        foreach (var entry in enODT.Current.Value)
          entry.clearData();
    }
    #endregion
  }
  /// <summary>
  /// Holds a dictionary of configured DAQ Lists for both XCP and CCP.
  /// 
  /// Maps a specific DAQ number to the corresponding DAQList.
  /// </summary>
  public abstract class DAQDict : SortedDictionary<UInt16, DAQList>
  {
    protected ODTEntryDictType mODTEntries = new ODTEntryDictType();
    /// <summary>
    /// Gets the mapping from measurement's address to the configured ODTEntry and therefore to it's measurement data.
    /// </summary>
    public ODTEntryDictType ODTEntries { get { return mODTEntries; } }
    /// <summary>
    /// Clears all the configured ODTs.
    /// 
    /// If resources (received data) should be released, too,
    /// a call to clearData is required before. 
    /// </summary>
    public new void Clear()
    {
      mODTEntries.Clear();
      clearData();
      base.Clear();
    }
    /// <summary>
    /// Clears all data received within the DAQ list dictionary.
    /// </summary>
    public void clearData()
    {
      lock (this)
      {
        foreach (var daqList in Values)
          daqList.clearData();
      }
    }
    /// <summary>
    /// Gets the ODTEntry corresponding to the specified measurement.
    /// </summary>
    /// <param name="measurement">The measurement to get the ODT entry for</param>
    /// <returns>The ODT entry instance if found, else null</returns>
    public ODTEntry getODTEntry(A2LMEASUREMENT measurement)
    {
      ODTEntry entry;
      return mODTEntries.TryGetValue(measurement.Address, out entry)
        ? entry
        : null;
    }
  }
}