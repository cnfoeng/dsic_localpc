﻿/*!
 * @file    
 * @brief   Defines the CCP protocol definitions
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    June 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

/// <summary>
/// Implements the CAN Calibration Protocol Version 2.1 (ASAM MCD-1 CCP)
/// </summary>
namespace jnsoft.Comm.CCP
{
  #region enums
  /// <summary>
  /// Supported types of CCP communication.
  /// </summary>
  public enum PIDSlaveMaster : byte
  {
    /// <summary>
    /// Response
    /// </summary>
    RES = 0xff,
    /// <summary>
    /// Event
    /// </summary>
    EV = 0xfe,
  }
  /// <summary>
  /// CCP command codes defined by the CCP specification.
  /// </summary>
  public enum CommandCode : byte
  {
    Connect = 0x01,
    GetCCPVersion = 0x1B,
    ExchangeID = 0x17,
    GetSeed = 0x12,
    Unlock = 0x13,
    SetMTA = 0x02,
    Dnload = 0x03,
    Dnload6 = 0x23,
    Upload = 0x04,
    ShortUp = 0x0F,
    SelectCALPage = 0x11,
    GetDAQSize = 0x14,
    SetDAQPtr = 0x15,
    WriteDAQ = 0x16,
    StartStop = 0x06,
    Disconnect = 0x07,
    SetSStatus = 0x0C,
    GetSStatus = 0x0D,
    BuildChksum = 0x0E,
    ClearMemory = 0x10,
    Program = 0x18,
    Program6 = 0x22,
    Move = 0x19,
    Test = 0x05,
    GetActiveCALPage = 0x09,
    StartStopAll = 0x08,
    DiagService = 0x20,
    ActionService = 0x21,
  }
  /// <summary>
  /// Possible command results.
  /// </summary>
  public enum CmdResult : byte
  {
    /// <summary>
    /// acknowledge / no error
    /// </summary>
    [Description("Successful")]
    OK = 0x00,
    /// <summary>
    /// DAQ processor overload
    /// </summary>
    [Description("DAQ processor overload")]
    DAQProcOverload = 0x01,
    /// <summary>
    /// command processor busy
    /// </summary>
    [Description("command processor busy")]
    CMDProcBusy = 0x10,
    /// <summary>
    /// DAQ processor busy
    /// </summary>
    [Description("DAQ processor busy")]
    DAQActive = 0x11,
    /// <summary>
    /// internal timeout
    /// </summary>
    [Description("internal timeout")]
    InternalTimeout = 0x12,
    /// <summary>
    /// key request
    /// </summary>
    [Description("key request")]
    KeyRequest = 0x18,
    /// <summary>
    /// session status request
    /// </summary>
    [Description("session status request")]
    SessionStateRequest = 0x19,
    /// <summary>
    /// cold start request
    /// </summary>
    [Description("cold start request")]
    ColdStartRequest = 0x20,
    /// <summary>
    /// cal. data init. request
    /// </summary>
    [Description("cal. data init. request")]
    CALDataInitRequest = 0x21,
    /// <summary>
    /// DAQ list init. request
    /// </summary>
    [Description("DAQ list init. request")]
    DAQListInitRequest = 0x22,
    /// <summary>
    /// code update request
    /// </summary>
    [Description("code update request")]
    CodeUpdateRequest = 0x23,
    /// <summary>
    /// unknown command
    /// </summary>
    [Description("unknown command")]
    UnknownCommand = 0x30,
    /// <summary>
    /// command syntax
    /// </summary>
    [Description("command syntax")]
    CommandSyntax = 0x31,
    /// <summary>
    /// parameter(s) out of range
    /// </summary>
    [Description("parameter(s) out of range")]
    ParOutOfRange = 0x32,
    /// <summary>
    /// access denied
    /// </summary>
    [Description("access denied")]
    AccessDenied = 0x33,
    /// <summary>
    /// overload
    /// </summary>
    [Description("overload")]
    Overload = 0x34,
    /// <summary>
    /// access locked
    /// </summary>
    [Description("access locked")]
    AccessLocked = 0x35,
    /// <summary>
    /// resource/function not available
    /// </summary>
    [Description("resource/function not available")]
    ResFuncNotAvailable = 0x36,
    /// <summary>
    /// Protocol failure (unexpected response length)
    /// </summary>
    [Description("Protocol failure (unexpected response length)")]
    ProtocolFailure = 0xfd,
    /// <summary>
    /// Generic (unspecified failure)
    /// </summary>
    [Description("Unspecified failure")]
    Generic = 0xfe,
    /// <summary>
    /// Timeout (connection lost)
    /// </summary>
    [Description("Timeout")]
    Timeout = 0xff,
  }
  /// <summary>
  /// Possible session states.
  /// 
  /// Used in RespGetStatus.
  /// </summary>
  [Flags]
  public enum SessionState : byte
  {
    None = 0x00,
    /// <summary>
    /// Calibration data initialized.
    /// </summary>
    CAL = 0x01,
    /// <summary>
    /// DAQ list(s) initialized.
    /// </summary>
    DAQ = 0x02,
    /// <summary>
    /// Request to save DAQ setup during shutdown in ECU.<para/>
    /// ECU automatically restarts DAQ after startup.
    /// </summary>
    RESUME = 0x04,
    /// <summary>
    /// Request to save calibration data during shut-down in ECU.
    /// </summary>
    STORE = 0x40,
    /// <summary>
    /// Session in progress.
    /// </summary>
    RUN = 0x80,
  }
  /// <summary>
  /// Possible resource types
  /// 
  /// Used in RespConnect, RespGetStatus, CmdGetSeed, RespUnlock.
  /// </summary>
  [Flags]
  public enum ResourceType : byte
  {
    None = 0x00,
    /// <summary>
    /// Calibration.
    /// </summary>
    CAL = 0x01,
    /// <summary>
    /// Data acquisition.
    /// </summary>
    DAQ = 0x02,
    /// <summary>
    /// Memory programming.
    /// </summary>
    PGM = 0x40,
  }
  #endregion
  #region command/response
  /// <summary>
  /// Base class represents a CCP command.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class CmdBase
  {
    CommandCode CmdCode;
    internal byte Ctr;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public CmdBase() { }
    internal CmdBase(CommandCode code) { CmdCode = code; }
    public virtual void changeEndianness() { }
  }
  /// <summary>
  /// Base class represents a CCP response.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class RespBase : ResponseBase
  {
    PIDSlaveMaster RespCode;
    CmdResult Result;
    internal byte Ctr;
    /// <summary>
    /// Internal use only!
    /// </summary>
    public RespBase() { }
    public RespBase(PIDSlaveMaster code, CmdResult result, byte ctr) { RespCode = code; Result = result; Ctr = ctr; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdConnect : CmdBase
  {
    public UInt16 StationAddress;
    public CmdConnect() { }
    internal CmdConnect(UInt16 stationAddress)
      : base(CommandCode.Connect)
    { // Stationaddress is defined always little endian
      StationAddress = BitConverter.IsLittleEndian
        ? stationAddress
        : Extensions.changeEndianess(stationAddress);
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdTest : CmdBase
  {
    public UInt16 StationAddress;
    public CmdTest() { }
    internal CmdTest(UInt16 stationAddress)
      : base(CommandCode.Test)
    { // Stationaddress is defined always little endian
      StationAddress = BitConverter.IsLittleEndian
        ? stationAddress
        : Extensions.changeEndianess(stationAddress);
    }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetSStatus : CmdBase
  {
    public SessionState SessionState;
    public CmdSetSStatus() { }
    internal CmdSetSStatus(SessionState sessionState)
      : base(CommandCode.SetSStatus)
    { SessionState = sessionState; }
  }
  /// <summary>
  /// Response from Get current session status from slave
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSStatus : RespBase
  {
    public SessionState SessionState;
    public byte StatusQualifier;
    public RespGetSStatus() { }
    public RespGetSStatus(byte ctr, SessionState sessionState)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    {
      SessionState = sessionState;
    }
    public override string ToString()
    {
      return string.Format("{0}: SessionState={1}"
        , GetType().Name
        , SessionState.ToString()
        );
    }
  }
  /// <summary>
  /// Response from Exchange Station Identifications.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespExchangeID : RespBase
  {
    public byte LengthOfID;
    public byte DataTypeQualifier;
    public ResourceType Availability;
    public ResourceType Protection;
    public RespExchangeID() { }
    public RespExchangeID(byte ctr, byte lengthOdID, byte dataTypeQualifier, ResourceType availability, ResourceType protection)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { LengthOfID = lengthOdID; DataTypeQualifier = dataTypeQualifier; Availability = availability; Protection = protection; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetSeed : CmdBase
  {
    public ResourceType Resource;
    public CmdGetSeed() { }
    internal CmdGetSeed(ResourceType resource)
      : base(CommandCode.GetSeed)
    { Resource = resource; }
  }
  /// <summary>
  /// Response from Get Seed for Key.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetSeed : RespBase
  {
    public byte ProtectionState;
    public RespGetSeed() { }
    public RespGetSeed(byte ctr, byte protetionState) : base(PIDSlaveMaster.RES, CmdResult.OK, ctr) { ProtectionState = protetionState; }
  }
  /// <summary>
  /// Response from Unlock Protection.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespUnlock : RespBase
  {
    public ResourceType PrivilegeState;
    public RespUnlock() { }
    public RespUnlock(byte ctr, ResourceType privilegeState) : base(PIDSlaveMaster.RES, CmdResult.OK, ctr) { PrivilegeState = privilegeState; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetMTA : CmdBase
  {
    public byte Number;
    public byte Extension;
    public UInt32 Address;
    public CmdSetMTA() { }
    internal CmdSetMTA(byte number, byte extension, UInt32 address)
      : base(CommandCode.SetMTA)
    { Number = number; Extension = extension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdUpload : CmdBase
  {
    public byte Size;
    public CmdUpload() { }
    internal CmdUpload(byte size)
      : base(CommandCode.Upload)
    { Size = size; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdShortUp : CmdBase
  {
    public byte Size;
    public byte Extension;
    public UInt32 Address;
    public CmdShortUp() { }
    internal CmdShortUp(byte size, byte extension, UInt32 address)
      : base(CommandCode.ShortUp)
    { Size = size; Extension = extension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetDAQSize : CmdBase
  {
    public byte DAQNo;
    byte res;
    public UInt32 CANId;
    public CmdGetDAQSize() { }
    internal CmdGetDAQSize(byte daqNo, UInt32 canId)
      : base(CommandCode.GetDAQSize)
    { DAQNo = daqNo; CANId = canId; }
    public override void changeEndianness() { CANId = Extensions.changeEndianess(CANId); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdDownload : CmdBase
  {
    public byte Size;
    public CmdDownload() { }
    internal CmdDownload(CommandCode code, byte size)
      : base(code)
    { Size = size; }
  }
  /// <summary>
  /// Supports CCP commands Move, Clear memory, BuildChksum.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdMoveClearBC : CmdBase
  {
    public UInt32 Size;
    public CmdMoveClearBC() { }
    internal CmdMoveClearBC(CommandCode code, UInt32 size)
      : base(code)
    { Size = size; }
    public override void changeEndianness() { Size = Extensions.changeEndianess(Size); }
  }
  /// <summary>
  /// Supports CCP commands Diag and Action service.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdActionDiagService : CmdBase
  {
    public UInt16 No;
    public CmdActionDiagService() { }
    internal CmdActionDiagService(CommandCode code, UInt16 no)
      : base(code)
    { No = no; }
    public override void changeEndianness() { No = Extensions.changeEndianess(No); }
  }
  /// <summary>
  /// Possible disconnect modes.
  /// </summary>
  public enum DisconnectMode : byte
  {
    /// <summary>
    /// 
    /// A temporary disconnect doesn’t stop the transmission of DAQ messages. The MTA values,
    /// the DAQ setup, the session status and the protection status are unaffected by the
    /// temporary disconnect and remain unchanged.
    /// </summary>
    Temporary,
    /// <summary>
    /// Terminates the calibration session.
    /// 
    /// Terminating the session invalidates all state information and resets the slave protection status.
    /// </summary>
    EndOfSession,
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdDisconnect : CmdBase
  {
    public DisconnectMode Mode;
    byte res;
    public UInt16 StationAddress;
    public CmdDisconnect() { }
    internal CmdDisconnect(DisconnectMode mode, UInt16 stationAddress)
      : base(CommandCode.Disconnect)
    { // Stationaddress is defined always little endian
      StationAddress = BitConverter.IsLittleEndian
        ? stationAddress
        : Extensions.changeEndianess(stationAddress);
      Mode = mode;
    }
  }
  /// <summary>
  /// Response from Get Size of DAQ list.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespDownload : RespBase
  {
    byte Extension;
    UInt32 Address;
    public RespDownload() { }
    public RespDownload(byte ctr, byte extension, UInt32 address)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { Extension = extension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  /// <summary>
  /// Response from Get Size of DAQ list.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetDAQSize : RespBase
  {
    public byte DAQListSize;
    public byte FirstPID;
    public RespGetDAQSize() { }
    public RespGetDAQSize(byte ctr, byte daqListSize, byte firstPID)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { DAQListSize = daqListSize; FirstPID = firstPID; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdSetDAQPtr : CmdBase
  {
    public byte DAQNo;
    public byte ODTNo;
    public byte ElementNo;
    public CmdSetDAQPtr() { }
    internal CmdSetDAQPtr(byte daqNo, byte odtNo, byte elementNo)
      : base(CommandCode.SetDAQPtr)
    { DAQNo = daqNo; ODTNo = odtNo; ElementNo = elementNo; }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdWriteDAQ : CmdBase
  {
    public byte Size;
    public byte Extension;
    public UInt32 Address;
    public CmdWriteDAQ() { }
    internal CmdWriteDAQ(byte size, byte extension, UInt32 address)
      : base(CommandCode.WriteDAQ)
    { Size = size; Extension = extension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdStartStop : CmdBase
  {
    public StartStopMode Mode;
    public byte DAQNo;
    public byte LastODTNo;
    public byte EvtChnNo;
    public UInt16 Prescaler;
    public CmdStartStop() { }
    internal CmdStartStop(StartStopMode mode, byte daqNo, byte lastODTNo, byte evtChnNo, UInt16 prescaler)
      : base(CommandCode.StartStop)
    { Mode = mode; DAQNo = daqNo; LastODTNo = lastODTNo; EvtChnNo = evtChnNo; Prescaler = prescaler; }
    public override void changeEndianness() { Prescaler = Extensions.changeEndianess(Prescaler); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdStartStopAll : CmdBase
  {
    public StartStopMode Mode;
    public CmdStartStopAll() { }
    internal CmdStartStopAll(StartStopMode mode)
      : base(CommandCode.StartStopAll)
    { Mode = mode; }
  }
  /// <summary>
  /// Response from Get currently active Calibration Page.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetActiveCALPage : RespBase
  {
    public byte Extension;
    public UInt32 Address;
    public RespGetActiveCALPage() { }
    public RespGetActiveCALPage(byte ctr, byte extension, UInt32 address)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { Extension = extension; Address = address; }
    public override void changeEndianness() { Address = Extensions.changeEndianess(Address); }
  }
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class CmdGetCCPVersion : CmdBase
  {
    byte DesiredMain;
    byte DesiredRelease;
    public CmdGetCCPVersion() { }
    internal CmdGetCCPVersion(byte desiredMain, byte desiredRelease)
      : base(CommandCode.GetCCPVersion)
    { DesiredMain = desiredMain; DesiredRelease = desiredRelease; }
  }
  /// <summary>
  /// Response from Get implemented Version of CCP.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespGetCCPVersion : RespBase
  {
    public byte Main;
    public byte Release;
    public RespGetCCPVersion() { }
    public RespGetCCPVersion(byte ctr, byte main, byte release)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { Main = main; Release = release; }
  }
  /// <summary>
  /// Response from Get Diagnostic or Action service.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespActionDiagService : RespBase
  {
    public byte Length;
    public byte DataTypeQualifier;
    public RespActionDiagService() { }
    public RespActionDiagService(byte ctr, byte length, byte dataTypeQualifier)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { Length = length; DataTypeQualifier = dataTypeQualifier; }
  }
  /// <summary>
  /// Response from Build Checksum.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public sealed class RespBuildChecksum : RespBase
  {
    public byte Size;
    public UInt32 Checksum;
    public RespBuildChecksum() { }
    public RespBuildChecksum(byte ctr, byte size, UInt32 checksum)
      : base(PIDSlaveMaster.RES, CmdResult.OK, ctr)
    { Size = size; Checksum = checksum; }
    public override void changeEndianness()
    {
      switch (Size)
      {
        case 2:
          Checksum = Extensions.changeEndianess((UInt16)Checksum);
          break;
        case 4:
          Checksum = Extensions.changeEndianess(Checksum);
          break;
      }
    }
  }
  #endregion
}