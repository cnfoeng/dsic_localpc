﻿/*!
 * @file    
 * @brief   Implements the CAN communicator driver for supporting CCP.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    June 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using log4net;
using System.Collections.Generic;
using jnsoft.ASAP2.CCP;
using jnsoft.Comm.CAN;

namespace jnsoft.Comm.CCP
{
  /// <summary>
  /// Base class implementing the CCP based communication instance.
  /// </summary>
  public sealed class CCPonCAN : Communicator
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(CCPonCAN));
    /// <summary>
    /// Holds the corresponding XCP master instance.
    /// </summary>
    CCPMaster mMaster;
    ICANProvider mCANProvider;
    A2LCCP_TP_BLOB mCCPIf;
    Dictionary<uint, string> mDAQSources = new Dictionary<uint, string>();
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance of XCPOnCAN.
    /// </summary>
    /// <param name="master">The corresponding master instance</param>
    /// <param name="canProvider">The corresponding CAN provider</param>
    /// <param name="ccpIf">The ASAP1B CCP Interface</param>
    internal CCPonCAN(CCPMaster master, ICANProvider canProvider, A2LCCP_TP_BLOB ccpIf)
    {
      mMaster = master;
      mCANProvider = canProvider;
      mCCPIf = ccpIf;
      SourceAddress = CANProvider.toCANIDString(mCCPIf.CANIDCmd);
      mCANProvider = canProvider;
      var canIDs = new List<UInt32>();
      canIDs.Add(mCCPIf.CANIDResp);
      mDAQSources[mCCPIf.CANIDResp] = CANProvider.toCANIDString(mCCPIf.CANIDResp);
      foreach (var source in ccpIf.Parent.enumChildNodes<A2LCCP_SOURCE>())
      {
        var qpBlob = source.getNode<A2LCCP_QP_BLOB>(false);
        if (qpBlob == null || canIDs.Contains(qpBlob.CANId))
          continue;
        canIDs.Add(qpBlob.CANId);
        mDAQSources[qpBlob.CANId] = CANProvider.toCANIDString(qpBlob.CANId);
      }
        ((ICANProvider)mCANProvider).registerClient(canIDs.ToArray(), receiveThread);
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the master commandmessage Id.
    /// </summary>
    internal uint CmdID { get { return mCCPIf.CANIDCmd; } }
    #endregion
    #region methods
    protected override void close()
    {
    }
    internal override int send(byte[] data)
    {
      return mCANProvider.send(mCCPIf.CANIDCmd, ref data);
    }
    void receiveThread(object sender, CANEventArgs e)
    {
      var data = e.Frame.Data;
      if (e.Frame.ID == mCCPIf.CANIDResp)
        // response received
        dataReceived(ref mMaster.SlaveAddress, e.Frame);
      else
      { // now this must be a DAQ mesage
        string strSource;
        mDAQSources.TryGetValue(e.Frame.ID, out strSource);
        dataReceived(ref strSource, e.Frame);
      }
    }
    /// <summary>
    /// Called from the receive thread, when data is received.
    /// </summary>
    /// <param name="source">The data source</param>
    /// <param name="frame">The data received</param>
    void dataReceived(ref string source, CANFrame frame)
    {
      var recFrame = new CCPFrame(ref source, frame.ID, ref frame.Data, false);
      // frame rceived
      CCPFrameQueue.Instance.enqueueFrame(recFrame);
      mMaster.increaseReceivedFrames(recFrame.getRawFrameLength());
      byte respCode = recFrame.Data[CCPFrame.ResponseIndex];
      switch (respCode)
      {
        case (byte)PIDSlaveMaster.EV:
          mMaster.onEventReceived(recFrame);
          break;
        case (byte)PIDSlaveMaster.RES:
          mMaster.LastResponse = recFrame;
          WaitForResponse.Set();
          break;
        default:
          // These are DAQ events
          mMaster.onDAQFrameReceived(recFrame);
          break;
      }
    }
    public override void Dispose()
    {
      mCANProvider.unregisterClient(receiveThread);
      base.Dispose();
    }
    #endregion
  }
}