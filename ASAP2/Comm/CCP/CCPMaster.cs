﻿/*!
 * @file    
 * @brief   Implements the CCP Master to communicate with a CCP Slave.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    June 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using log4net;

using jnsoft.ASAP2;
using jnsoft.ASAP2.Properties;
using jnsoft.Comm.CAN;
using jnsoft.ASAP2.CCP;
using jnsoft.Helpers;

namespace jnsoft.Comm.CCP
{
  /// <summary>
  /// Implements the CCP Master to communicate with a CCP Slave.
  /// 
  /// Derived from CCPMasterBase, this is the class providing some 
  /// convenience funtions for a CCP communication.
  /// 
  /// - All commands are implemented as synchronous calls to the slave
  /// - All commands are thread-safe
  /// - The endianess of any command, response or event is converted 
  /// automatically according the CCP protocol
  /// - Responses (out parameter) of commands are only valid if the 
  /// CmdResult of the call is CmdResult.OK
  /// </summary>
  /// <example>Example to connect to a CCP slave device:
  /// <code>
  /// </code>
  /// </example>
  public class CCPMaster : CCPMasterBase
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(CCPMaster));
    /// <summary>
    /// Object used to synchronize read/write accesses on ECU.
    /// 
    /// Important to protect the MTA pointer on the slave.
    /// </summary>
    readonly object mReadWriteLock = new object();
    string mExchangeIDStr = string.Empty;
    bool mConnected;
    volatile bool mIsDAQRunning;
    bool mConfigureMeasurements;
    /// <summary>
    /// Holds always the last valid responses.
    /// 
    /// Maps the commandcode as key to it's valid response as value.
    /// </summary>
    Dictionary<CommandCode, RespBase> mResponseDict = new Dictionary<CommandCode, RespBase>();
    #endregion
    #region constructor
    /// <summary>
    /// Creates a CCP communication master.
    /// </summary>
    /// <param name="connectBehavior">Client's connect behaviour</param>
    /// <param name="ccpIf">A2L CCP interface instance (ASAP1B_CCP)</param>
    /// <param name="canProvider"></param>
    /// <param name="onConnectionStateChanged">The callback to receive connection state changes (may be null)</param>
    /// <param name="onCCPEventReceived">The callback to receive CCP events (may be null)</param>
    /// <param name="onCCPErrorReceived">The callback to receive CCP command errors (may be null)</param>
    public CCPMaster(ConnectBehaviourType connectBehavior
      , A2LCCP_TP_BLOB ccpIf
      , ICANProvider canProvider
      , EventHandler onConnectionStateChanged
      , EventHandler<CCPEventArgs> onCCPEventReceived
      , EventHandler<CCPErrorArgs> onCCPErrorReceived
      )
      : base(connectBehavior, ccpIf)
    {
      DAQs = new DAQDictCCP();

      // add the handlers
      if (null != onConnectionStateChanged)
        ConnectionStateChanged += onConnectionStateChanged;
      if (null != onCCPEventReceived)
        CCPEventReceived += onCCPEventReceived;
      if (null != onCCPErrorReceived)
        CCPErrorReceived += onCCPErrorReceived;
      CommKernel.RegisterClient(this, canProvider, ccpIf);
    }
    #endregion
    #region events
    /// <summary>
    /// Called if a CCP event is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<CCPEventArgs> CCPEventReceived;
    /// <summary>
    /// Called if a CCP command error is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<CCPErrorArgs> CCPErrorReceived;
    #endregion
    #region properties
    public override bool Connected { get { return mConnected; } }
    /// <summary>
    /// Gets the exchange ID string.
    /// </summary>
    public string ExchangeIDStr { get { return mExchangeIDStr; } }
    /// <summary>Indicates if data acquisition is running.</summary>
    public bool IsDAQRunning { get { return mIsDAQRunning; } }
    /// <summary>
    /// The last received Connect response.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public RespBase ConnectResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.Connect, out response))
            return (RespBase)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received GetStatus response.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public RespGetSStatus StatusResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetSStatus, out response))
            return (RespGetSStatus)response;
          return null;
        }
      }
    }
    /// <summary>
    /// The last received ExchangeID response.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public RespExchangeID ExchangeIDResponse
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.ExchangeID, out response))
            return (RespExchangeID)response;
          return null;
        }
      }
    }
    /// <summary>
    /// Gets the CCP version string.
    /// 
    /// Only valid in connected state!
    /// </summary>
    public string Version
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (mResponseDict.TryGetValue(CommandCode.GetCCPVersion, out response))
            return $"{((RespGetCCPVersion)response).Main}.{((RespGetCCPVersion)response).Release}";
          return string.Empty;
        }
      }
    }
    public sealed override ECUPage ActivePage
    {
      get
      {
        lock (mResponseDict)
        {
          RespBase response;
          if (!mResponseDict.TryGetValue(CommandCode.GetActiveCALPage, out response))
            return ECUPage.NotSet;
          RespGetActiveCALPage resp = (RespGetActiveCALPage)response;
          A2LCCP_DEFINED_PAGES page = mCCPIf.findPage(resp.Extension, resp.Address);
          if (page == null)
            return ECUPage.NotSet;
          return (page.PageType & CCP_MEMORY_PAGE_TYPE.FLASH) > 0
            ? ECUPage.Flash
            : ECUPage.RAM;
        }
      }
    }
    public override bool CanWrite
    {
      get
      {
        return mConnected
          && (ExchangeIDResponse.Availability & ResourceType.CAL) > 0
          && (ExchangeIDResponse.Protection & ResourceType.CAL) == 0;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Returns true if the command is allowed to request.
    /// </summary>
    /// <param name="cmd">The XCP command in the same form as in the A2LDescription (e.g. "GET_ID")</param>
    /// <returns>true if the optional command is allowed to execute</returns>
    public bool isAllowedRequest(CommandCode cmd)
    {
      if (!Settings.Default.RespectOptionalCmds)
        return true;
      return mCCPIf.OptionalCmds.Contains((byte)cmd);
    }

    public override void onEventReceived(CCPFrame ccpFrame)
    {
      base.onEventReceived(ccpFrame);
      CCPEventReceived?.Invoke(this, new CCPEventArgs((CmdResult)ccpFrame.Data[1]));
    }

    public override void onErrorReceived(CmdResult result, CommandCode code)
    {
      base.onErrorReceived(result, code);
      CCPErrorReceived?.Invoke(this, new CCPErrorArgs(result, code));
    }

    /// <summary>
    /// Disconnect from slave.
    /// 
    /// Brings the slave to the DISCONNECTED state. The DISCONNECTED
    /// state is described in Part 1, chapter “state machine”.
    /// </summary>
    /// <returns>true if successful</returns>
    public override bool Disconnect()
    {
      if (mIsDAQRunning)
        // stop data acquisition
        stopMeasurements(false);
      return base.Disconnect();
    }

    public override void Dispose()
    {
      CCPEventReceived = null;
      CCPErrorReceived = null;
      base.Dispose();
      lock (DAQs)
        DAQs.Clear();
    }

    internal sealed override void onValidResponseReceived(CommandCode cmdCode, RespBase response)
    {
      base.onValidResponseReceived(cmdCode, response);
      switch (cmdCode)
      {
        case CommandCode.Connect:
        case CommandCode.GetSStatus:
        case CommandCode.GetCCPVersion:
        case CommandCode.GetActiveCALPage:
        case CommandCode.ExchangeID:
          lock (mResponseDict)
            mResponseDict[cmdCode] = response;
          break;
      }
    }

    internal override void setConnectionState<T>(T respConnect)
    {
      var connect = respConnect as RespBase;
      base.setConnectionState(connect);
      bool newConnected = connect != null;
      if (newConnected == mConnected)
        // no state change
        return;
      try
      {
        if (newConnected)
        { // connected
          // request the standard telegrams
          RespGetSStatus status;
          if (CmdResult.OK != GetStatus(out status))
          {
            newConnected = false;
            return;
          }

          RespGetCCPVersion ccpVersion;
          if (CmdResult.OK != GetCCPVersions(2, 1, out ccpVersion))
          {
            newConnected = false;
            return;
          }

          RespExchangeID exchangeID;
          if (CmdResult.OK != ExchangeID(out exchangeID))
          {
            newConnected = false;
            return;
          }

          if (exchangeID.LengthOfID > 0)
          {
            byte[] data;
            if (readSync(exchangeID.LengthOfID, 0, uint.MaxValue, out data))
              mExchangeIDStr = Encoding.Default.GetString(data).Trim(new char());
          }

          // unlock required
          if (isAllowedRequest(CommandCode.Unlock) && exchangeID.Protection != ResourceType.None && mSeedAndKeyDLL != null)
            // try to unlock all resources
            unlockECU(mSeedAndKeyDLL, mSeedAndKeyDLL, mSeedAndKeyDLL, exchangeID.Protection);

          if (isAllowedRequest(CommandCode.SetSStatus))
            SetStatus(SessionState.CAL | SessionState.RUN);

          if (isAllowedRequest(CommandCode.GetActiveCALPage))
          { // get active CAL page
            RespGetActiveCALPage respActiveCALPage;
            GetActiveCALPage(out respActiveCALPage);
          }
        }
      }
      finally
      {
        bool raise = mConnected != newConnected;
        mConnected = newConnected;
        if (!mConnected)
        { // disconnected, remove all responses
          Disconnect();
          mIsDAQRunning = false;
          lock (mResponseDict)
            mResponseDict.Clear();
        }
        if (raise)
          // notify event
          RaiseConnectionStateChanged();
      }
    }

    public override void onDAQFrameReceived(CCPFrame ccpFrame)
    {
      if (!Monitor.TryEnter(DAQs))
        // DAQ dict in use, ignore daq frame!
        return;
      try
      {
        if (!Connected || !mIsDAQRunning)
          return;

        DAQList daqList = null;
        var en = DAQs.GetEnumerator();
        while (en.MoveNext())
        { // search the DAQList
          if (((DAQListCCP)en.Current.Value).CANId == ccpFrame.ID)
          {
            daqList = en.Current.Value;
            break;
          }
        }

        if (daqList == null)
          // DAQ List not found
          return;

        ODTList odt;
        if (!daqList.ODTs.TryGetValue((byte)(ccpFrame.Data[0]), out odt))
          // ODT not found
          return;

        if (odt.PID == daqList.FirstPID)
        { // The first ODT set is timestamp master
          TimeBase.DAQLastTimestamp = daqList.LastReceivedRelSecs = TimeBase.ElapsedDAQSeconds;
          byte[] data = daqList.DAQRecordCache.getData();
          if (data != null)
          { // All records of a DAQList are completely received
            // Data complete event to registered 
            RaiseOnValuesReceived(daqList.DAQNo, TimeBase.DAQLastTimestamp, ref data);
            daqList.onValuesComplete(ref data, ChangeEndianess);
          }
        }

        if (double.IsNaN(daqList.LastReceivedRelSecs))
          // first ODT is not received already, ignore this record data
          return;
        int expectedDataLen = odt.DataLength + 1;
        int unusedBytes = ccpFrame.Data.Length - expectedDataLen;
        // store received values in DAQ record cache
        daqList.DAQRecordCache.Add(ref ccpFrame.Data, 1, unusedBytes);
      }
      finally
      { // force release lock
        Monitor.Exit(DAQs);
      }
    }
    #region extended ccp methods
    /// <summary>
    /// Read data synchronously from the ECU.
    /// 
    /// Depending on the data length to be read
    /// CmdUpload or CmdShortUpload is used.
    /// </summary>
    /// <param name="len">The length to read</param>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to read from (If the adress is UInt32.MaxValue, the data should be read from the current MTA location)</param>
    /// <param name="data">The resulting data (only valid, if return value is CmdResult.Ok)</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public override bool readSync(int len, byte addressExtension, UInt32 address
      , out byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      data = null;
      if (!SlaveConnected)
        return false;
      lock (mReadWriteLock)
      {
        if (address != uint.MaxValue)
        {
          var page = ActivePage;
          if (page == ECUPage.NotSet)
            return false;
          UInt32 computedAddress = mCCPIf.computeAddress(addressExtension, address, ActivePage);
          if (computedAddress != uint.MaxValue)
            // computed address is valid
            address = computedAddress;
#if DEBUG
          mLogger.Info($"Uploading from 0x{address:X8}(0x{len:X8} bytes)");
#endif
        }
        data = null;
        var result = CmdResult.OK;
        if (address != uint.MaxValue && len <= 5 && isAllowedRequest(CommandCode.ShortUp))
          // use short upload if address is given set and len fits
          result = ShortUp((byte)len, addressExtension, address, out data);
        else
        {
          if (address != uint.MaxValue)
            result = SetMTA(0, addressExtension, address);
          if (CmdResult.OK == result)
          { // Set MTA ok, read
            var uploaded = new List<byte>();
            int updateStep = Math.Max(1, len / 100);
            int currIndex = 0;
            int remainingLen = len;
            byte[] upData;
            while (remainingLen > 0)
            {
              byte bytesUp = (byte)(remainingLen > 5 ? 5 : remainingLen);
              if (CmdResult.OK != (result = Upload(bytesUp, out upData)))
                // failed to upload
                break;
              uploaded.AddRange(upData);
              remainingLen -= bytesUp;
              currIndex += bytesUp;

              if (null != progressHandler && currIndex > updateStep)
              {
                var args = new ProgressArgs((int)(currIndex * 100.0 / (double)len));
                progressHandler(this, args);
                if (args.Cancel)
                  // cancelled
                  return false;
                updateStep += Math.Max(1, len / 100);
              }
            }
            if (remainingLen == 0)
              // complete, takeover data
              data = uploaded.ToArray();
          }
        }
        progressHandler?.Invoke(this, new ProgressArgs(100));
        return result == CmdResult.OK;
      }
    }

    /// <summary>
    /// Write or program data synchronously to the ECU
    /// 
    /// Depending on the data length to be written 
    /// CmdDownload or CmdShortDownload is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="useProgram">true if the method is used to program bytes, else the bytes are only downloaded</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    bool writeSyncImpl(byte addressExtension, UInt32 address, byte[] data
      , bool useProgram
      , EventHandler<ProgressArgs> progressHandler)
    {
      if (!SlaveConnected)
        return false;
      lock (mReadWriteLock)
      {
        ECUPage page = ActivePage;
        if (!useProgram && page == ECUPage.Flash)
          // never write to flash pages
          return false;

        UInt32 computedAddress = mCCPIf.computeAddress(addressExtension, address, page);
        if (computedAddress != uint.MaxValue)
          // computed address is valid
          address = computedAddress;
#if DEBUG
        mLogger.Info($"{(useProgram?"Programming":"Downloading")} to 0x{address:X8}(0x{data.Length:X8} bytes)");
#endif
        var result = CmdResult.OK;
        try
        { // set MTA ok, write
          RespDownload respDL;
          if (address == uint.MaxValue && data.Length <= 5)
            // use download if write goes to the current MTA and length fits into
            result = useProgram ? Program(data, out respDL) : Download(data, out respDL);
          else
          { // download/program

            // Set MTA if a valid address is specified
            if (address != uint.MaxValue && CmdResult.OK != (result = SetMTA(0, addressExtension, address)))
              return false;

            bool _6BytesAllowed = isAllowedRequest(useProgram ? CommandCode.Program6 : CommandCode.Dnload6);
            var maxFrameBytes = _6BytesAllowed ? 6 : 5;

            int updateStep = Math.Max(1, data.Length / 100);
            int currIndex = 0;

            while (currIndex < data.Length)
            {
              var transLen = data.Length - currIndex > maxFrameBytes
                ? maxFrameBytes
                : data.Length - currIndex;

              var transferData = new byte[transLen];
              Array.Copy(data, currIndex, transferData, 0, transLen);

              if (transferData.Length == 6)
                result = useProgram ? Program6(transferData, out respDL) : Download6(transferData, out respDL);
              else
                result = useProgram ? Program(transferData, out respDL) : Download(transferData, out respDL);

              if (result != CmdResult.OK)
                break;

              currIndex += transferData.Length;
              if (null != progressHandler && currIndex > updateStep)
              {
                var args = new ProgressArgs((int)(currIndex * 100.0 / data.Length));
                progressHandler(this, args);
                if (args.Cancel)
                  // cancelled
                  return false;
                updateStep += Math.Max(1, data.Length / 1000);
              }
            }
          }
          return result == CmdResult.OK;
        }
        catch { return false; }
        finally
        {
#if DEBUG
          mLogger.Info($"{(useProgram ? "Program" : "Download")} {result}");
#endif
          progressHandler?.Invoke(this, new ProgressArgs(100));
        }
      }
    }

    /// <summary>
    /// Write data synchronously to the ECU
    /// 
    /// Depending on the data length to be written 
    /// Download or Download6 is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public override bool writeSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      return writeSyncImpl(addressExtension, address, data, false, progressHandler);
    }

    /// <summary>
    /// Program data synchronously to the ECU
    /// 
    /// Depending on the data length to be written 
    /// Progrma or Program6 is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <param name="modes">Unused here (only importatnt for an XCP connection)</param>
    /// <returns>true if successful</returns>
    public override bool programSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      , XCPPrgParams modes = null
      )
    {
      return writeSyncImpl(addressExtension, address, data, true, progressHandler);
    }

    public override bool copyPage2Page(ECUPage srcPage, ECUPage dstPage)
    {
      if (!SlaveConnected || !isAllowedRequest(CommandCode.Move))
        // device is not connected
        return false;

      var pages = mCCPIf.getPages(srcPage == ECUPage.Flash ? CCP_MEMORY_PAGE_TYPE.FLASH : CCP_MEMORY_PAGE_TYPE.RAM);
      if (pages.Count < 1)
        return false;
      var srcCALPage = pages[0];
      pages = mCCPIf.getPages(dstPage == ECUPage.Flash ? CCP_MEMORY_PAGE_TYPE.FLASH : CCP_MEMORY_PAGE_TYPE.RAM);
      if (pages.Count < 1)
        return false;
      var dstCALPage = pages[0];

      CmdResult result;
      if (CmdResult.OK != (result = SetMTA(0, srcCALPage.AddressExt, srcCALPage.Address)))
        return false;
      if (CmdResult.OK != (result = SetMTA(1, dstCALPage.AddressExt, dstCALPage.Address)))
        return false;
      return CmdResult.OK == (result = Move(Math.Min(srcCALPage.Length, dstCALPage.Length)));
    }

    public override bool setPage(ECUPage page)
    {
      if (!SlaveConnected || !isAllowedRequest(CommandCode.SelectCALPage))
        return false;
      if (ActivePage == page)
        // page is already set
        return true;
      RespBase response;
      if (!mResponseDict.TryGetValue(CommandCode.GetActiveCALPage, out response))
        return false;
      var resp = (RespGetActiveCALPage)response;
      uint offset;
      var calPage = mCCPIf.findCALPage(resp.Extension, resp.Address, page, out offset);

      // remove the local active page
      mResponseDict.Remove(CommandCode.GetActiveCALPage);

      CmdResult result;
      if (CmdResult.OK != (result = SetMTA(0, calPage.AddressExt, calPage.Address)))
        return false;
      if (CmdResult.OK != (result = SelectCALPage()))
        return false;
      RespGetActiveCALPage respGetPage;
      return CmdResult.OK == (result = GetActiveCALPage(out respGetPage));
    }

    /// <summary>
    /// Tries to unlock a specific protected ECU resource using the specified SeedKey DLL.
    /// </summary>
    /// <param name="seedAndKeyDLL">The Seed&Key DLL</param>
    /// <param name="resource">Resource to unlock</param>
    /// <returns>A CmdResult value</returns>
    public CmdResult unlockECU(NativeSKDLL seedAndKeyDLL, ResourceType resource)
    {
      if (seedAndKeyDLL == null || (seedAndKeyDLL.Type & SKType.CCP) == 0 || !isAllowedRequest(CommandCode.GetSeed))
        return CmdResult.Generic;

      if (!SlaveConnected)
        return CmdResult.Timeout;

      if (resource == ResourceType.None)
        // not protected
        return CmdResult.OK;

      CmdResult result;

      // Get the seed key from the device
      RespGetSeed respGetSeed;
      byte[] seed;
      if (CmdResult.OK != (result = GetSeed(resource, out respGetSeed, out seed)))
        return result;
      if (0 == respGetSeed.ProtectionState)
        // unlock not required
        return CmdResult.OK;

      // compute the key from seed with the DLL function
      byte[] key;
      seedAndKeyDLL.ComputeKeyFromSeed(seed, out key);
      if (key == null || key.Length == 0)
        return CmdResult.Generic;

      // unlock ECU
      int maxKeyLen = Math.Min(4, key.Length);
      var ccpKey = new byte[maxKeyLen];
      Array.Copy(key, ccpKey, maxKeyLen);
      RespUnlock respUnlock;
      if (CmdResult.OK != (result = Unlock(ccpKey, out respUnlock)))
        return result;

      // request new protection state
      RespExchangeID respExchangeID;
      ExchangeID(out respExchangeID);

      return (respExchangeID.Protection & resource) == 0
        ? CmdResult.OK
        : CmdResult.Generic;
    }

    /// <summary>
    /// Tries to unlock all protected ECU resources using the specified SeedKey DLLs.
    /// </summary>
    /// <param name="seedAndKeyDAQDLL">The Seed&Key DLL to use for the DAQ resource (may be null, depending on the specified resource)</param>
    /// <param name="seedAndKeyCALDLL">The Seed&Key DLL to use for the CAL resource (may be null, depending on the specified resource)</param>
    /// <param name="seedAndKeyPGMDLL">The Seed&Key DLL to use for the PGM resource (may be null, depending on the specified resource)</param>
    /// <param name="resource">The resource(s) to unlock</param>
    /// <returns>A CmdResult value</returns>
    public CmdResult unlockECU(NativeSKDLL seedAndKeyDAQDLL, NativeSKDLL seedAndKeyCALDLL, NativeSKDLL seedAndKeyPGMDLL, ResourceType resource)
    {
      if (resource == ResourceType.None)
        // not protected
        return CmdResult.OK;

      var result = CmdResult.OK;
      var localResult = CmdResult.OK;
      if ((resource & ResourceType.DAQ) > 0)
        localResult = unlockECU(seedAndKeyDAQDLL, ResourceType.DAQ);
      if (localResult != CmdResult.OK)
        result = localResult;

      if ((resource & ResourceType.CAL) > 0)
        localResult = unlockECU(seedAndKeyCALDLL, ResourceType.CAL);
      if (localResult != CmdResult.OK)
        result = localResult;

      if ((resource & ResourceType.PGM) > 0)
        localResult = unlockECU(seedAndKeyPGMDLL, ResourceType.PGM);
      if (localResult != CmdResult.OK)
        result = localResult;

      if (localResult != CmdResult.OK)
        result = localResult;
      return result;
    }

    public override void configureMeasurements(List<A2LMEASUREMENT> measurements)
    {
      lock (DAQs)
      { // clear local DAQ configuration
        DAQs.Clear();
        mConfigureMeasurements = true;

        if (mCCPIf.SourceAndRasterMap.Count == 0)
          // no DAQ lists at all defined by device
          return;

        measurements.Sort(SortBySizeAscending);

        CmdResult result;
        if (isAllowedRequest(CommandCode.GetSStatus) && isAllowedRequest(CommandCode.SetSStatus))
        { // session states allowed
          RespGetSStatus status;
          if (CmdResult.OK == (result = GetStatus(out status)) && (status.SessionState & SessionState.DAQ) > 0)
            // remove DAQ session state
            SetStatus(status.SessionState & ~SessionState.DAQ);
        }

        // Try use GET_DAQ_SIZE
        var getDAQSizes = new List<RespGetDAQSize>();
        var canIDs = new List<uint>();
        foreach (var srcAndRaster in mCCPIf.SourceAndRasterMap.Values)
        {
          var qbBlob = srcAndRaster.Source.getNode<A2LCCP_QP_BLOB>(false);
          if (qbBlob == null)
          { // GET_DAQ_SIZE not allowed
            getDAQSizes.Add(null);
            canIDs.Add(mCCPIf.CANIDResp);
            continue;
          }

          RespGetDAQSize respDAQSizes;
          if (CmdResult.OK == (result = GetDAQSize((byte)qbBlob.DAQNo, qbBlob.CANId, out respDAQSizes)))
          {
            getDAQSizes.Add(respDAQSizes);
            canIDs.Add(qbBlob.CANId);
          }
          else
          {
            getDAQSizes.Add(null);
            canIDs.Add(mCCPIf.CANIDResp);
          }
        }
        ((DAQDictCCP)DAQs).configure(mCCPIf.SourceAndRasterMap, getDAQSizes, canIDs, measurements);
      }
    }

    /// <summary>
    /// Configure the DAQList on the ECU.
    /// 
    /// This is done by writing the configured ODT Entries 
    /// with WriteDAQ and SetDAQPtr.
    /// </summary>
    /// <param name="daqListNo">The DAQ List number to write</param>
    /// <param name="daqList">The DAQ List itself</param>
    /// <returns>true if successful</returns>
    bool configureDAQList(UInt16 daqListNo, DAQList daqList)
    {
      byte odtNo = 0;
      foreach (var odts in daqList.ODTs.Values)
      {
        if (odts.Count > 0)
        {
          byte elementNo = 0;
          foreach (var entry in odts)
          { // Write DAQ for every ODT entry
            CmdResult result;
            if (CmdResult.OK != (result = SetDAQPtr((byte)daqListNo, odtNo, elementNo++)))
              return false;

            var adrExt = (byte)entry.Measurement.AddressExtension;
            if (CmdResult.OK != (result = WriteDAQ(entry.Size, adrExt, entry.Address)))
              // failed
              return false;
          }
        }
        ++odtNo;
      }
      return true;
    }

    public override bool startMeasurements(bool doSynchronized)
    {
      lock (DAQs)
      {
        if (!Connected)
          return false;

        if (DAQs.Count == 0)
          return true;

        if (mIsDAQRunning)
          // already running
          return false;

        DAQs.clearData();
        TimeBase.resetDAQTime();

        if (mConfigureMeasurements)
        {
          // write configuration to ECU is required
          foreach (var kvp in DAQs)
            if (!configureDAQList(kvp.Key, kvp.Value))
              // failed to configure DAQ lists
              return false;
        }

        if (isAllowedRequest(CommandCode.GetSStatus) && isAllowedRequest(CommandCode.SetSStatus))
        { // session states allowed
          RespGetSStatus status;
          if (CmdResult.OK == GetStatus(out status) && (status.SessionState & SessionState.DAQ) == 0)
            // set DAQ session state
            SetStatus(status.SessionState | SessionState.DAQ);
        }

        // Do the Start/Stop commands
        CmdResult result;
        foreach (var kvp in DAQs)
        {
          UInt16 daqListNo = kvp.Key;
          var daqList = kvp.Value;
          if (daqList.ODTs.Count > 0)
          {
            var startCmd = doSynchronized ? StartStopMode.Select : StartStopMode.Start;
            var raster = mCCPIf.SourceAndRasterMap[daqListNo].Raster;
            if (CmdResult.OK != (result = StartStop(startCmd
              , (byte)daqListNo
              , (byte)(daqList.ODTs.Count - 1)
              , (byte)(raster != null ? raster.EvtChnNo : 0)
              , (UInt16)raster.Rate))
              )
              return false;
          }
        }

        // Start in synchronized mode
        if (doSynchronized && CmdResult.OK != (result = StartStopAll(StartStopMode.Start)))
          return false;

        mLogger.Debug($"{DAQs.Count} DAQLists running");
        mConfigureMeasurements = false;
        mIsDAQRunning = true;
        return true;
      }
    }

    public override bool stopMeasurements(bool doSynchronized)
    {
      try
      {
        lock (DAQs)
        {
          if (!mIsDAQRunning || DAQs.Count == 0)
            // already stopped
            return true;

          CmdResult result;
          if (CmdResult.OK != (result = StartStopAll(StartStopMode.Stop)))
            return false;
          mLogger.Debug($"{DAQs.Count} DAQ lists stopped");
          return true;
        }
      }
      finally { mIsDAQRunning = false; }
    }

    public override bool getChecksum(byte addressExtension, UInt32 address, UInt32 size, out UInt32 checksum)
    {
      checksum = 0;
      if (CmdResult.OK != SetMTA(0, addressExtension, address))
        return false;
      RespBuildChecksum respBuildChecksum;
      if (CmdResult.OK != BuildChecksum(size, out respBuildChecksum))
        return false;
      checksum = respBuildChecksum.Checksum;
      return true;
    }
    #endregion
    #endregion
  }
}