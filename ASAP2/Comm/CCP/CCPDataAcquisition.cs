﻿/*!
 * @file    
 * @brief   Implements the CCP data acquisition stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    January 2016
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2;
using jnsoft.ASAP2.CCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;

namespace jnsoft.Comm.CCP
{
  /// <summary>
  /// DAQ List representation.
  /// 
  /// Holds a dictionary of configured ODTs.
  /// </summary>
  public sealed class DAQListCCP : DAQList
  {
    #region members
    UInt32 mCANId;
    #endregion
    #region contsructors
    /// <summary>
    /// Initializes a new DAQ List with the specified parameters.
    /// </summary>
    /// <param name="daqNo">DAQ Number.</param>
    public DAQListCCP(UInt16 daqNo) { DAQNo = daqNo; }
    /// <summary>
    /// Initializes a new DAQ List with the specified parameters.
    /// 
    /// Algorithm:
    /// 
    /// </summary>
    /// <param name="daqNo">DAQ list number</param>
    /// <param name="canID">CAN ID</param>
    /// <param name="firstPID">Starts with PID</param>
    /// <param name="maxODT">Maximum ODTs</param>
    /// <param name="measurements">[out] configured measurements will be removed from this list</param>
    /// <param name="odtEntries">[out] configured measurement entries will be added</param>
    public DAQListCCP(UInt16 daqNo
      , UInt32 canID
      , byte firstPID
      , byte maxODT
      , List<A2LMEASUREMENT> measurements
      , ODTEntryDictType odtEntries
      )
      : this(daqNo)
    {
      mCANId = canID;
      FirstPID = firstPID;
      int maxBytesPerODT = 7;
      int currMeasIdx = measurements.Count - 1; // start from the back of the list

      while (currMeasIdx >= 0)
      { // still measurements to configure
        var meas = measurements[currMeasIdx];
        byte measDataLen = (byte)meas.DataType.getSizeInByte();
        int arraySize = meas.getArraySize();
        byte maxODTEntrySize = 4; // this is set by the CCP spec
        byte odtEntrySize = Math.Min(measDataLen, maxODTEntrySize);
        if (!fitsIn(meas, measDataLen, odtEntrySize, arraySize, maxODT, maxBytesPerODT, maxBytesPerODT))
        {
          currMeasIdx--;
          continue;
        }

        uint arrayIdxOffset = 0;
        ODTEntry writtenEntry = null;
        for (int i = 0; i < arraySize; ++i, arrayIdxOffset += measDataLen)
        { // Iterate through the whole measurement block (A2L Arraysize may be > 1)

          uint measAdr = meas.Address + arrayIdxOffset;
          if (odtEntries.TryGetValue(measAdr, out writtenEntry))
            // measurement is already configured, remove it
            break;

          byte partDataOffset = 0;
          for (; partDataOffset < measDataLen; partDataOffset += odtEntrySize)
          { // iterate through the measurement by the maximum ODT entry size
            writtenEntry = null;
            var odtEntry = new ODTEntry(daqNo, meas, (uint)arrayIdxOffset, partDataOffset, odtEntrySize);
            ODTList currODT = null;
            while (null != (currODT = getNextODT(currODT, firstPID, maxODT, maxBytesPerODT)))
            {
              if (!currODT.fitsIn(odtEntry))
                continue;
              writtenEntry = odtEntry;
              currODT.Add(odtEntry);
              if (partDataOffset == 0)
                // store only complete measurement in the fast access dictionary
                odtEntries[measAdr] = odtEntry;
              break;
            }
          }
        }

        if (null != writtenEntry)
          // measurement completely configured, remove from list
          measurements.RemoveAt(currMeasIdx);
        currMeasIdx--;
      }
    }
    #endregion
    #region properties
    /// <summary>Gets the corresponding CAN ID</summary>
    public UInt32 CANId { get { return mCANId; } }
    #endregion
    #region methods
    /// <summary>
    /// Gets or creates the next ODT within this DAQ List.
    /// </summary>
    /// <param name="currODT">The current ODT (maybe null)</param>
    /// <param name="firstPID">The first PID of the ODTList</param>
    /// <param name="maxODTs">The maximum number of ODTs</param>
    /// <param name="maxBytesPerODT">The maximum number of bytes in an ODT</param>
    /// <returns>An ODT List instance or null if no more ODTs are available</returns>
    internal ODTList getNextODT(ODTList currODT, byte firstPID, byte maxODTs, int maxBytesPerODT)
    {
      byte nextPID = (byte)(currODT == null ? firstPID : currODT.PID + 1);
      ODTList result = null;
      if (!ODTs.TryGetValue(nextPID, out result) && ODTs.Count < maxODTs)
        result = ODTs[nextPID] = new ODTList(nextPID, maxBytesPerODT, maxBytesPerODT);
      return result;
    }
    /// <summary>
    /// Test if the specified measurement fits completely into the current DAQ list.
    /// </summary>
    /// <param name="meas">The measurement</param>
    /// <param name="size">Its single data size</param>
    /// <param name="odtEntrySize">Maybe a smaller ODT entry size (Data type splitting on XCP)</param>
    /// <param name="arraySize">The array size of the measurement</param>
    /// <param name="maxODT">Maximum allowed ODTs to configure</param>
    /// <param name="maxODTEntries">Maximum allowed ODT entries</param>
    /// <param name="maxBytesPerODT">Maximum allowed bytes per ODT</param>
    /// <returns>True if the specified measurement fits completely into the current DAQ list.</returns>
    internal bool fitsIn(A2LMEASUREMENT meas, int size, byte odtEntrySize, int arraySize, byte maxODT, int maxODTEntries, int maxBytesPerODT)
    {
      int freeEntries = 0;
      int freeBytes = 0;
      int factor = size / odtEntrySize;
      foreach (var list in ODTs.Values)
      {
        freeEntries += list.FreeEntries / factor;
        freeBytes += list.FreeBytes / odtEntrySize;
      }
      freeEntries += (maxODT - ODTs.Count) * (maxODTEntries / factor);
      freeBytes += (maxODT - ODTs.Count) * (maxBytesPerODT / odtEntrySize);
      return Math.Min(freeEntries, freeBytes) >= arraySize;
    }
    #endregion
  }
  /// <summary>
  /// Holds a dictionary of configured DAQ Lists.
  /// 
  /// Maps a specific DAQ number to the corresponding DAQList.
  /// </summary>
  public sealed class DAQDictCCP : DAQDict
  {
    /// <summary>
    /// Tries to configure all specified measurements into the available DAQ Lists.
    /// </summary>
    /// <param name="sourceAndRasterDict">DAQ List dictionary from A2L description</param>
    /// <param name="respGetDAQSizes">Responses from GetDAQSize provided by the ECU</param>
    /// <param name="canIDs">CANIds from A2L description</param>
    /// <param name="measurements">Measurements to configure in DAQ Lists</param>
    /// <returns>0 if successful, else the count of measurements which could not be configured</returns>
    internal int configure(SortedDictionary<UInt16, SourceAndRaster> sourceAndRasterDict
      , List<RespGetDAQSize> respGetDAQSizes
      , List<UInt32> canIDs
      , List<A2LMEASUREMENT> measurements
      )
    {
      int i = 0;
      foreach (var kvp in sourceAndRasterDict)
      {
        var daqListNo = kvp.Key;
        byte firstPID = byte.MinValue, maxODT = byte.MaxValue;

        var respDAQSize = i < respGetDAQSizes.Count ? respGetDAQSizes[i] : null;
        if (respDAQSize != null)
        { // Get limits from ECU
          firstPID = respDAQSize.FirstPID;
          maxODT = respDAQSize.DAQListSize;
        }


        var daqListInfo = kvp.Value.Source;
        A2LCCP_QP_BLOB qbBlob;
        if ((qbBlob = daqListInfo.getNode<A2LCCP_QP_BLOB>(false)) != null)
        { // Get limits from A2L
          if (firstPID == 0 && qbBlob.FirstPID > 0)
            firstPID = qbBlob.FirstPID;
          maxODT = Math.Min((byte)qbBlob.Length, maxODT);
        }

        if (daqListInfo.Active)
        { // DAQ enabled to configure
          this[daqListNo] = new DAQListCCP(daqListNo, canIDs[i], firstPID, maxODT, measurements, mODTEntries);
        }
        else
          this[daqListNo] = new DAQListCCP(daqListNo);
        i++;
      }
      return measurements.Count;
    }
  }
}