﻿/*!
 * @file    
 * @brief   Implements the CCP Master base to communicate with a CCP Slave.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2;
using jnsoft.ASAP2.CCP;
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;

namespace jnsoft.Comm.CCP
{
  /// <summary>
  /// ErrorArgs class delivering received command errors.
  /// </summary>
  public sealed class CCPErrorArgs : EventArgs
  {
    /// <summary>Gets the received CCP result code.</summary>
    public CmdResult ErrorCode { get; private set; }
    /// <summary>Gets the corresponding CCP command.</summary>
    public CommandCode Command { get; private set; }
    internal CCPErrorArgs(CmdResult errorCode, CommandCode code) { ErrorCode = errorCode; Command = code; }
  }
  /// <summary>
  /// EventArgs class delivering received events.
  /// </summary>
  public sealed class CCPEventArgs : EventArgs
  {
    /// <summary>Gets the received CCP result code.</summary>
    public CmdResult ErrorCode { get; private set; }
    internal CCPEventArgs(CmdResult errorCode) { ErrorCode = errorCode; }
  }
  /// <summary>
  /// Implements the CCP Master base communication with a CCP Slave.
  /// 
  /// - All commands are implemented as synchronous calls to the slave
  /// - All commands are thread-safe
  /// - The endianess of any command, response or event is converted 
  /// automatically according the CCP protocol
  /// - Responses (out parameter) of commands are only valid if the 
  /// CmdResult of the call is CmdResult.OK
  /// </summary>
  public abstract class CCPMasterBase : CommMaster
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(CCPMasterBase));
    internal byte Ctr;
    protected A2LCCP_TP_BLOB mCCPIf;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a XCPonCAN communication master.
    /// </summary>
    /// <param name="connectBehavior">Client's connect behaviour</param>
    /// <param name="ccpIf">A2L CCP interface instance (ASAP1B_CCP)</param>
    public CCPMasterBase(ConnectBehaviourType connectBehavior
      , A2LCCP_TP_BLOB ccpIf
      )
      : base(connectBehavior, (A2LIF_DATA)ccpIf.Parent)
    {
      mCCPIf = ccpIf;
      SlaveAddress = $"{ccpIf.StationAddress:X} on CAN IDs ({mCCPIf.CANIDCmd:X}/{mCCPIf.CANIDResp:X})";
      ChangeEndianess = BitConverter.IsLittleEndian != (mCCPIf.ByteOrder == BYTEORDER_TYPE.LITTLE_ENDIAN);
    }
    #endregion
    #region properties
    /// <summary>Last received response.</summary>
    public CCPFrame LastResponse { get; internal set; }
    /// <summary>The last error occured.</summary>
    public CmdResult LastErrorResponse { get; private set; } = CmdResult.OK;
    public sealed override string LastErrorText { get { return LastErrorResponse.ToString(); } }
    public sealed override Enum LastErrorEnum { get { return LastErrorResponse; } }
    #endregion
    #region methods
    internal override int getAliveCycleTime()
    {
      return Settings.Default.CCPConnectionTestCycle;
    }
    /// <summary>
    /// Called from the CommKernel if the connection state gets changed.
    /// </summary>
    /// <param name="respConnect">A valid connect response instance if the device gets connected, null if the device gets disconnected.</param>
    internal override void setConnectionState<T>(T respConnect)
    {
      LastStateChange = DateTime.Now;
      if (respConnect != null)
      { // connect received
        SlaveConnected = true;
      }
      else
      { // disconnected
        SlaveConnected = false;
        LastReceivedTime = DateTime.MinValue;
        LastResponse = null;
        LastErrorResponse = CmdResult.OK;
        EventsReceived =
        FramesSent =
        FramesReceived =
        ErrorsReceived = 0;
        ReadyToSend = true;
      }
    }
    /// <summary>
    /// Called, if an error response is received from the slave.
    /// </summary>
    /// <param name="result">The command result</param>
    /// <param name="code">The corresponding command code</param>
    public virtual void onErrorReceived(CmdResult result, CommandCode code)
    {
      ++ErrorsReceived;
      LastErrorResponse = result;
    }
    /// <summary>
    /// Called if an event is received from the slave.
    /// </summary>
    /// <param name="ccpFrame">The corresponding received frame</param>
    public virtual void onEventReceived(CCPFrame ccpFrame)
    {
      ++EventsReceived;
    }
    /// <summary>
    /// Called if a DAQ frame is received from the slave.
    /// </summary>
    /// <param name="ccpFrame">The corresponding received frame</param>
    public virtual void onDAQFrameReceived(CCPFrame ccpFrame)
    {
    }
    internal virtual void onValidResponseReceived(CommandCode cmdCode, RespBase response)
    {
    }
    public sealed override void resetLastError() { LastErrorResponse = CmdResult.OK; }
    #region ccp commands
    /// <summary>
    /// Set up connection with slave.
    /// 
    /// This command establishes a continuous, logical, point-to-point connection with a slave device. 
    /// During a running XCP session (CONNECTED) this command has no influence on any 
    /// configuration of the XCP slave driver. 
    /// 
    /// A slave device does not respond to any other commands (except auto detection) unless it is 
    /// in the state CONNECTED. 
    /// </summary>
    /// <returns>The corresponding command result</returns>
    public CmdResult Connect()
    {
      // address is always litlle endian
      UInt16 address = BitConverter.IsLittleEndian
        ? mCCPIf.StationAddress
        : Extensions.changeEndianess(mCCPIf.StationAddress);

      var cmd = new CmdConnect(address);
      var data = CCPFrame.toByteArray(cmd);
      RespBase response;
      var result = CommKernel.sendFrame(this, data, out response);
      setConnectionState(response);
      if (CmdResult.OK == result)
        LastReceivedTime = DateTime.Now;
      return result;
    }
    internal sealed override bool internalConnect(XCP.ConnectMode mode, out int maxDTO)
    {
      maxDTO = 0;
      return CmdResult.OK == Connect();
    }
    internal sealed override bool internalGetStatus()
    {
      RespGetSStatus respGetSStatus;
      return CmdResult.OK == GetStatus(out respGetSStatus);
    }
    /// <summary>
    /// Disconnect from slave.
    /// 
    /// Brings the slave to the “DISCONNECTED” state. The DISCONNECTED”
    /// state is described in Part 1, chapter “state machine”.
    /// </summary>
    /// <param name="mode">The disconnect mode</param>
    /// <returns>true if successful</returns>
    public bool Disconnect(DisconnectMode mode)
    {
      CmdResult result = CmdResult.OK;
      if (SlaveConnected)
      {
        var data = CCPFrame.toByteArray(new CmdDisconnect(mode, mCCPIf.StationAddress));
        RespBase response;
        result = CommKernel.sendFrame(this, data, out response);
      }
      CommKernel.setDisconnect(this);
      return result == CmdResult.OK;
    }
    public override bool Disconnect()
    {
      return Disconnect(DisconnectMode.EndOfSession);
    }
    /// <summary>
    /// Get the CCP version from slave.
    /// 
    /// This command performs a mutual identification of the protocol version used in the
    /// master and in the slave device to aggree on a common protocol version. This
    /// command is expected to be executed prior to the EXCHANGE_ID command.
    /// </summary>
    /// <param name="desiredMain">The desired main version</param>
    /// <param name="desiredRelease">The desired release version</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetCCPVersions(byte desiredMain, byte desiredRelease, out RespGetCCPVersion response)
    {
      var cmd = new CmdGetCCPVersion(desiredMain, desiredRelease);
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get the Exchange Station Identifications from slave.
    /// 
    /// The CCP master and slave stations exchange IDs for automatic session
    /// configuration. This might include automatic assignment of a data acquisition
    /// setup file depending on the slave's returned ID (Plug´n´Play).
    /// 
    /// The slave device automatically sets the Memory Transfer Address 0 (MTA0) to
    /// the location from which the CCP master may subsequently upload the requested
    /// ID using UPLOAD. See also the SetMTA and Upload command description.
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ExchangeID(out RespExchangeID response)
    {
      var cmd = new CmdBase(CommandCode.ExchangeID);
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get Seed for Key from slave.
    /// 
    /// See ExchangeID for a description of the resource mask.
    /// Only one resource or function may be requested with one GetSeed
    /// command If more than one resource is requested, the GetSeed command
    /// together with the following Unlock command has to be performed multiple
    /// times.
    /// 
    /// Returns ´seed´ data for a seed&key algorithm for computing the ´key´ to
    /// unlock the requested function for authorized access.
    /// </summary>
    /// <param name="resource">Requested slave resource or function (Resource Mask)</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <param name="seed">The seed bytes (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetSeed(ResourceType resource, out RespGetSeed response, out byte[] seed)
    {
      var cmd = new CmdGetSeed(resource);
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response, out seed);
    }
    /// <summary>
    /// Set Memory Transfer Address.
    /// 
    /// This command will initialize a base pointer (32Bit + extension) for all following memory
    /// transfers. The address extension depends on the slave controller's organization and may
    /// identify a switchable memory bank or a memory segment.
    /// 
    /// The MTA number (handle) is used to identify different transfer address locations (pointers).
    /// MTA0 is used by the commands DNLOAD, UPLOAD, DNLOAD_6 , SELECT_CAL_PAGE,
    /// CLEAR_MEMORY, PROGRAM and PROGRAM_6. MTA1 is used by the MOVE command. See also command ‘MOVE’.
    /// </summary>
    /// <param name="mtaNo">Memory transfer address MTA number (0,1)</param>
    /// <param name="addressExtension">Address extension</param>
    /// <param name="address">The address to set</param>
    /// <returns></returns>
    public CmdResult SetMTA(byte mtaNo, byte addressExtension, UInt32 address)
    {
      var cmd = new CmdSetMTA(mtaNo, addressExtension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      RespBase response;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out response);
    }
    /// <summary>
    /// Move memory block.
    /// 
    /// A data block of the specified length (size) will be copied from the address defined by MTA 0
    /// (source pointer) to the address defined by MTA 1 (destination pointer).
    /// </summary>
    /// <param name="size">Size (number of bytes) of data block to be moved</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Move(UInt32 size)
    {
      var cmd = new CmdMoveClearBC(CommandCode.Move, size);
      if (ChangeEndianess) cmd.changeEndianness();
      RespBase response;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out response);
    }
    /// <summary>
    /// Clear memory.
    /// 
    /// This command may be used to erase FLASH EPROM prior to reprogramming. 
    /// The MTA0 pointer points to the memory location to be erased.
    /// </summary>
    /// <param name="size">Size (number of bytes) of data block to be cleared</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ClearMemory(UInt32 size)
    {
      var cmd = new CmdMoveClearBC(CommandCode.ClearMemory, size);
      if (ChangeEndianess) cmd.changeEndianness();
      RespBase response;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out response);
    }
    /// <summary>
    /// Build checksum over memory range.
    /// 
    /// Returns a checksum result of the memory block that is defined by MTA0
    /// (memory transfer area start address) and block size.The checksum algorithm
    /// may be manufacturer and / or project specific, it is not part of this specification.
    /// </summary>
    /// <param name="blockSize">The block size</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult BuildChecksum(UInt32 blockSize, out RespBuildChecksum response)
    {
      var cmd = new CmdMoveClearBC(CommandCode.BuildChksum, blockSize);
      if (ChangeEndianess) cmd.changeEndianness();
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out response);
    }
    /// <summary>
    /// Get the Unlock Protection from slave.
    /// 
    /// Unlocks the slave devices security protection (if applicable) using a ´key´ computed from ´seed´.
    /// </summary>
    /// <param name="key">Requested slave resource or function (Resource Mask)</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Unlock(byte[] key, out RespUnlock response)
    {
      var cmd = new CmdBase(CommandCode.Unlock);
      var data = new List<byte>(CCPFrame.toByteArray(cmd));
      data.AddRange(key);
      return CommKernel.sendFrame(this, data.ToArray(), out response);
    }
    /// <summary>
    /// Get the Data upload from slave.
    /// 
    /// A data block of the specified length (size), starting at current MTA0, will be copied into the
    /// corresponding DTO data field. The MTA0 pointer will be post-incremented by the value of 'size'.
    /// </summary>
    /// <param name="size">The count of elements to upload</param>
    /// <param name="respData">The uploaded data</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult Upload(byte size, out byte[] respData)
    {
      var cmd = new CmdUpload(size);
      RespBase resp;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out resp, out respData);
    }
    /// <summary>
    /// Get the short upload from slave.
    /// 
    /// A data block of the specified length (size), starting at source address will be copied into the
    /// corresponding DTO data field. The MTA0 pointer remains unchanged
    /// </summary>
    /// <param name="size">The count of elements to upload</param>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address</param>
    /// <param name="respData">The uploaded data</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ShortUp(byte size, byte addressExtension, UInt32 address, out byte[] respData)
    {
      var cmd = new CmdShortUp(size, addressExtension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      RespBase resp;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out resp, out respData);
    }
    /// <summary>
    /// Download from master to slave
    /// 
    /// The data block of the specified length (size) contained in the CRO will be copied into
    /// memory, starting at the current Memory Transfer Address 0 (MTA0). The MTA0 pointer will
    /// be post-incremented by the value of 'size'.
    /// </summary>
    /// <param name="bytes">data to be transferred (up to 5 bytes)</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of bytes exceeds 5</exception>
    public CmdResult Download(byte[] bytes, out RespDownload response)
    {
      if (bytes.Length > 5)
        throw new ArgumentOutOfRangeException("bytes", string.Format(Resources.strLengthNotValid, bytes.Length, 1));
      var data = new List<byte>(CCPFrame.toByteArray(new CmdDownload(CommandCode.Dnload, (byte)bytes.Length)));
      data.AddRange(bytes);
      return CommKernel.sendFrame(this, data.ToArray(), out response);
    }
    /// <summary>
    /// Program from master to slave
    /// 
    /// The data block of the specified length (size) contained in the CRO will be copied into
    /// memory, starting at the current Memory Transfer Address 0 (MTA0). The MTA0 pointer will
    /// be post-incremented by the value of 'size'.
    /// </summary>
    /// <param name="bytes">data to be transferred (up to 5 bytes)</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of bytes exceeds 5</exception>
    public CmdResult Program(byte[] bytes, out RespDownload response)
    {
      if (bytes.Length > 5)
        throw new ArgumentOutOfRangeException("bytes", string.Format(Resources.strLengthNotValid, bytes.Length, 1));
      var data = new List<byte>(CCPFrame.toByteArray(new CmdDownload(CommandCode.Program, (byte)bytes.Length)));
      data.AddRange(bytes);
      return CommKernel.sendFrame(this, data.ToArray(), out response);
    }
    /// <summary>
    /// Download 6 bytes from master to slave
    /// 
    /// The data block of the specified length (size) contained in the CRO will be copied into
    /// memory, starting at the current Memory Transfer Address 0 (MTA0). The MTA0 pointer will
    /// be post-incremented by the value of 'size'.
    /// </summary>
    /// <param name="bytes">6 bytes of data to be transferred</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of bytes is not 6</exception>
    public CmdResult Download6(byte[] bytes, out RespDownload response)
    {
      if (bytes.Length != 6)
        throw new ArgumentOutOfRangeException("bytes", "This must be always 6 bytes to transfer");
      var data = new List<byte>(CCPFrame.toByteArray(new CmdBase(CommandCode.Dnload6)));
      data.AddRange(bytes);
      return CommKernel.sendFrame(this, data.ToArray(), out response);
    }
    /// <summary>
    /// Program 6 bytes from master to slave
    /// 
    /// The data block of the specified length (size) contained in the CRO will be copied into
    /// memory, starting at the current Memory Transfer Address 0 (MTA0). The MTA0 pointer will
    /// be post-incremented by the value of 'size'.
    /// </summary>
    /// <param name="bytes">6 bytes of data to be transferred</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of bytes is not 6</exception>
    public CmdResult Program6(byte[] bytes, out RespDownload response)
    {
      if (bytes.Length != 6)
        throw new ArgumentOutOfRangeException("bytes", "This must be always 6 bytes to transfer");
      var data = new List<byte>(CCPFrame.toByteArray(new CmdBase(CommandCode.Program6)));
      data.AddRange(bytes);
      return CommKernel.sendFrame(this, data.ToArray(), out response);
    }
    /// <summary>
    /// Get current session status from slave.
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetStatus(out RespGetSStatus response)
    {
      var cmd = new CmdBase(CommandCode.GetSStatus);
      var data = CCPFrame.toByteArray(cmd);
      var result = CommKernel.sendFrame(this, data, out response);
      if (CmdResult.OK == result)
        LastReceivedTime = DateTime.Now;
      return result;
    }
    /// <summary>
    /// Set Session Status from slave.
    /// 
    /// Keeps the slave node informed about the current state of the calibration session.
    /// </summary>
    /// <param name="state">The session status</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetStatus(SessionState state)
    {
      var cmd = new CmdSetSStatus(state);
      RespBase response;
      return CommKernel.sendFrame(this, CCPFrame.toByteArray(cmd), out response);
    }
    /// <summary>
    /// Get the DAQ List size from slave.
    /// 
    /// Returns the size of the specified DAQ list as the number of available Object
    /// DescriptorTables (ODTs) and clears the current list. If the specified list number is not
    /// available, size = 0 should be returned. The DAQ list is initialized and data acquisition by this
    /// list is stopped.
    /// An individual CAN Identifier may be assigned to a DAQ list to configure multi ECU data
    /// acquisition. This feature is optional. If the given identifier isn’t possible, an error code is
    /// returned. 29 bit CAN identifiers are marked by the most significant bit set.
    /// </summary>
    /// <param name="daqNo">DAQ list number (0,1,...)</param>
    /// <param name="canId">CAN Identifier of DTO dedicated to list number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetDAQSize(byte daqNo, UInt32 canId, out RespGetDAQSize response)
    {
      var cmd = new CmdGetDAQSize(daqNo, canId);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Action/Diagnostic Service
    /// </summary>
    /// <param name="No">Action service number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult ActionService(UInt16 No, out RespActionDiagService response)
    {
      var cmd = new CmdActionDiagService(CommandCode.ActionService, No);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Diagnostic Service
    /// </summary>
    /// <param name="No">Diagnostic service number</param>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult DiagService(UInt16 No, out RespActionDiagService response)
    {
      var cmd = new CmdActionDiagService(CommandCode.DiagService, No);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Set DAQ list pointer.
    /// 
    /// Initializes the DAQ list pointer for a subsequent write to a DAQ list.
    /// See also 'Organization of Data Acquisition Messages'.
    /// </summary>
    /// <param name="daqListNo">DAQ list number</param>
    /// <param name="ODTNo">Object Descriptor Table ODT number</param>
    /// <param name="elmentNo">Element number within ODT</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult SetDAQPtr(byte daqListNo, byte ODTNo, byte elmentNo)
    {
      var cmd = new CmdSetDAQPtr(daqListNo, ODTNo, elmentNo);
      var data = CCPFrame.toByteArray(cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Write DAQ list entry.
    /// 
    /// Writes one entry (description of single DAQ element) to a DAQ list defined by the DAQ list
    /// pointer(see SET_DAQ_PTR). The following DAQ element sizes are defined: 
    /// 1 byte, 2 bytes(type word), 4 bytes(type long / Float).
    /// 
    /// An ECU may not support individual address extensions for each element and 2 or 4 byte
    /// element sizes.It is the responsibility of the master device to care for the ECU limitations.
    /// The limitations may be defined in the slave device description file(e.g.ASAP2).
    /// It is the responsibility of the slave device, that all bytes of a DAQ element are consistent
    /// upon transmission.
    /// </summary>
    /// <param name="size">Size of DAQ element in bytes { 1, 2, 4 }</param>
    /// <param name="extension">Address extension of DAQ element</param>
    /// <param name="address">Address of DAQ element</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult WriteDAQ(byte size, byte extension, UInt32 address)
    {
      var cmd = new CmdWriteDAQ(size, extension, address);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = CCPFrame.toByteArray(cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Start / Stop Data transmission.
    /// 
    /// This command is used to start or to stop the data acquisition or to prepare a synchronized
    /// start of the specified DAQ list.The Last ODT number specifies which ODTs (From 0 to
    /// Last ODT number) of this DAQ list should be transmitted.The Event Channel No.
    /// specifies the generic signal source that effectively determines the data transmission timing.
    /// To allow reduction of the desired transmission rate, a prescaler may be applied to the
    /// Event Channel.The prescaler value factor must be greater than or equal to 1.
    /// The mode parameter is defined as follows: 0x00 stops specified DAQ list, 0x01 starts
    /// specified DAQ list, 0x02 prepares DAQ list for synchronised start.
    /// The start/stop mode parameter = 0x02(prepare to start data transmission) configures the
    /// DAQ list with the provided parameters but does not start the data acquisition of the
    /// specified list.This parameter is used for a synchronized start of all configured DAQ lists. In
    /// case the slave device is not capable of performing the synchronized start of the data
    /// acquisition, the slave device may then start data transmission if this parameter is TRUE
    /// (not zero).
    /// The ECU specific properties of event channels and DAQ lists may be described in the slave
    /// device description(ASAP2).
    /// </summary>
    /// <param name="mode">Mode : start / stop / prepare data tranmission</param>
    /// <param name="daqNo">DAQ list number</param>
    /// <param name="lastODTNo">Last ODT number</param>
    /// <param name="evtChnNo">Event Channel No.</param>
    /// <param name="prescaler">Transmission rate prescaler</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult StartStop(StartStopMode mode, byte daqNo, byte lastODTNo, byte evtChnNo, UInt16 prescaler)
    {
      var cmd = new CmdStartStop(mode, daqNo, lastODTNo, evtChnNo, prescaler);
      if (ChangeEndianess) cmd.changeEndianness();
      var data = CCPFrame.toByteArray(cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Start / Stop Synchronised Data transmission.
    /// 
    /// This command is used to start the periodic transmission of all DAQ lists
    /// configured with the previously sent START_STOP command(start/stop modus
    /// = 2) as „prepared to start“ in a synchronized manner.The command is used to
    /// stop the periodic transmission of all DAQ lists, including those not started
    /// synchronized.
    /// </summary>
    /// <param name="mode">Stop or Start data transmission (The StartStopSynchMode.StopSelected is not valid on CCP)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult StartStopAll(StartStopMode mode)
    {
      if (mode == StartStopMode.Select)
        throw new ArgumentException("This mode is not valid on CCP");
      var cmd = new CmdStartStopAll(mode);
      var data = CCPFrame.toByteArray(cmd);
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Get currently active Calibration Page from slave.
    /// 
    /// This command returns the start address of the calibration page that is currently active in the slave device.
    /// </summary>
    /// <param name="response">The slave response (only valid if return value is CMDResult.OK, else null)</param>
    /// <returns>The corresponding command result</returns>
    public CmdResult GetActiveCALPage(out RespGetActiveCALPage response)
    {
      var cmd = new CmdBase(CommandCode.GetActiveCALPage);
      var data = CCPFrame.toByteArray(cmd);
      return CommKernel.sendFrame(this, data, out response);
    }
    /// <summary>
    /// Select Calibration Data Page from slave.
    /// 
    /// This command's function depends on the ECU implementation. The previously initialized
    /// MTA0 points to the start of the calibration data page that is selected as the currently active
    /// page by this command.
    /// </summary>
    /// <returns>The corresponding command result</returns>
    public CmdResult SelectCALPage()
    {
      var data = CCPFrame.toByteArray(new CmdBase(CommandCode.SelectCALPage));
      RespBase response;
      return CommKernel.sendFrame(this, data, out response);
    }
    #endregion
    #endregion
    #region IDisposable Members
    /// <summary>
    /// frees used resources.
    /// </summary>
    public override void Dispose()
    {
      CommKernel.DeregisterClient(this);
      base.Dispose();
    }
    #endregion
  }
}