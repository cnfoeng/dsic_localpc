﻿/*!
 * @file    
 * @brief   Implements the CCP frame.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    March 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Comm.CAN;
using System;
using System.Runtime.InteropServices;

namespace jnsoft.Comm.CCP
{
  /// <summary>
  /// Represents an CCP frame.
  /// 
  /// Implements some static helper methds to convert 
  /// - object references into byte arrays 
  /// - byte array into object references
  /// </summary>
  public sealed class CCPFrame : Frame
  {
    #region members
    public const int ResponseIndex = 0;
    const string mCSVFormat = "{0};\"{1}\";\"{2}\";{3};{4};\"{5}\";\"{6}\";\"{7}\"";
    const string mClpFormat = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}";
    /// <summary>
    /// The CAN Bus source identifcation (free text, maybe something like 'Vector XL virtual CAN Bus 1')
    /// </summary>
    public string BusID;
    #endregion
    #region constructors
    /// <summary>
    /// Initialises a new instance of CCPFrame with the specified parameters.
    /// </summary>
    /// <param name="busID">The CAN Bus ID of the message</param>
    /// <param name="canID">The CAN ID of he message</param>
    /// <param name="data">The complete frame data as byte array</param>
    /// <param name="isMasterFrame">true if the frame is sent by the master, else false</param>
    public CCPFrame(ref string busID, UInt32 canID, ref byte[] data, bool isMasterFrame)
    {
      BusID = busID;
      ID = canID;
      Data = data;
      IsMasterFrame = isMasterFrame;
    }
    #endregion
    #region properties
    /// <summary>
    /// The address sent or received from.
    /// </summary>
    public string Address { get { return $"{(IsMasterFrame ? "\u2192" : "\u2190")} {CANProvider.toCANIDString(ID)}"; } }
    /// <summary>
    /// The Length value of the frame
    /// </summary>
    public int Len { get { return Data.Length; } }
    /// <summary>
    /// Gets the CAN IDa sent or received from.
    /// </summary>
    public UInt32 ID { get; private set; }
    /// <summary>
    /// Indicates, if the frame is a data acquisition frame.
    /// </summary>
    public bool IsDAQ { get { return !IsMasterFrame && Data.Length > 0 && Data[0] < (byte)PIDSlaveMaster.EV; } }
    /// <summary>
    /// Indicates if the frame is an error response frame.
    /// </summary>
    public bool IsError { get { return !IsMasterFrame && Data.Length > 1 && Data[0] == (byte)PIDSlaveMaster.RES && Data[1] != (byte)CmdResult.OK; } }
    /// <summary>
    /// Gets the frame CTR.
    /// 
    /// Only valid for Command and Response frames.
    /// </summary>
    public string Ctr
    {
      get
      {
        return IsDAQ ? string.Empty
          : IsMasterFrame
            ? Data.Length > 1 ? Data[1].ToString() : string.Empty
            : Data.Length > 2 ? Data[2].ToString() : string.Empty;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Gets the frame's type.
    /// </summary>
    /// <returns>returns a type string representation</returns>
    public string getTypeStr()
    {
      string result = string.Empty;
      if (IsMasterFrame)
        result = ((CommandCode)Data[0]).ToString();
      else
      {
        PIDSlaveMaster respCode = (PIDSlaveMaster)Data[0];
        switch (respCode)
        {
          case PIDSlaveMaster.RES:
          case PIDSlaveMaster.EV: result = $"{respCode}({(CmdResult)Data[1]})"; break;
          default: result = "DAQ"; break;
        }
      }
      return result;
    }
    public override string toCSV()
    {
      return string.Format(mCSVFormat
        , getTimeStr()
        , BusID
        , Address
        , Data.Length
        , Ctr
        , getTypeStr()
        , getDataStr()
        , getDataASCIIStr('"')
        );
    }
    public override string toClipboard()
    {
      return string.Format(mClpFormat
        , getTimeStr()
        , BusID
        , Address
        , Data.Length
        , Ctr
        , getTypeStr()
        , getDataStr()
        , getDataASCIIStr('\t')
        );
    }
    /// <summary>
    /// Gets the frame length in Bits on the Bus depending on the used CCP media type.
    /// </summary>
    /// <returns>The frame length in Bits on the bus</returns>
    public int getRawFrameLength()
    {
      return CANFrame.getRawFrameLength(ID, ref Data);
    }
    /// <summary>
    /// Converts a byte array into the specified object reference.
    /// </summary>
    /// <typeparam name="T">The resulting object type</typeparam>
    /// <param name="data">The byte array to copy from</param>
    /// <param name="result">The resulting object</param>
    /// <returns>true if successful, false, if the memory size of the supplied data is smaller than the destination type</returns>
    public static bool convertToReference<T>(byte[] data, out T result) where T : class, new()
    {
      byte[] dummy;
      return convertToReference(data, out result, out dummy);
    }
    /// <summary>
    /// Converts a byte array into the specified object reference.
    /// </summary>
    /// <typeparam name="T">The resulting object type</typeparam>
    /// <param name="data">The byte array to copy from</param>
    /// <param name="result">The resulting object</param>
    /// <param name="additionalData">available additional data or null</param>
    /// <returns>true if successful, false, if the memory size of the supplied data is smaller than the destination type</returns>
    public static bool convertToReference<T>(byte[] data, out T result, out byte[] additionalData) where T : class, new()
    {
      result = null;
      additionalData = null;
      var structurePtr = IntPtr.Zero;
      try
      {
        var type = typeof(T);
        int size = Marshal.SizeOf(type);
        structurePtr = Marshal.AllocHGlobal(size);
        Marshal.Copy(data, 0, structurePtr, size);
        // copy data into it
        result = (T)Marshal.PtrToStructure(structurePtr, type);
        int remainingBytes = data.Length - size;
        if (remainingBytes > 0)
        {
          additionalData = new byte[remainingBytes];
          Array.Copy(data, size, additionalData, 0, remainingBytes);
        }
        return true;
      }
      catch { return false; }
      finally
      { // force deleting the global memory
        if (structurePtr != IntPtr.Zero)
          Marshal.FreeHGlobal(structurePtr);
      }
    }
    /// <summary>
    /// Creates the byte array from the specified object to send over the network.
    /// </summary>
    /// <typeparam name="T">The sending object type</typeparam>
    /// <param name="objectToSend">The object to send</param>
    /// <returns>The supplied object as a byte array</returns>
    public static byte[] toByteArray<T>(T objectToSend) where T : class
    {
      return toByteArray(objectToSend, null);
    }
    /// <summary>
    /// Creates the byte array from the specified object to send over the network.
    /// </summary>
    /// <typeparam name="T">The sending object type</typeparam>
    /// <param name="objectToSend">The object to send</param>
    /// <param name="additionalData">Aditional dynamic data to send (may be null)</param>
    /// <returns>The supplied object as a byte array</returns>
    public static byte[] toByteArray<T>(T objectToSend, byte[] additionalData) where T : class
    {
      var mem = IntPtr.Zero;
      int marshalSize = (UInt16)Marshal.SizeOf(typeof(T));
      int additionalDataLen = (additionalData != null ? additionalData.Length : 0);
      int fullSize = marshalSize + additionalDataLen;
      var frame = new byte[fullSize];

      try
      {
        mem = Marshal.AllocHGlobal(marshalSize);
        Marshal.StructureToPtr(objectToSend, mem, true);
        Marshal.Copy(mem, frame, 0, marshalSize);
        // copy additional data into the frame
        if (additionalData != null)
          Array.Copy(additionalData, 0, frame, marshalSize, additionalDataLen);
      }
      finally
      { // force to free the global memory
        if (mem != IntPtr.Zero)
          Marshal.FreeHGlobal(mem);
      }
      return frame;
    }
    #endregion
  }
}