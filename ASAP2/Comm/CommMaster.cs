﻿/*!
 * @file    
 * @brief   Defines and implements stuff for communicating with CCP and XCP enabled devices.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2;
using jnsoft.ASAP2.Helpers;
using jnsoft.ASAP2.Properties;
using jnsoft.Comm.XCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

/// <summary>
/// Contains all the stuff to communicate over XCP, CCP and CAN.
/// </summary>
namespace jnsoft.Comm
{
  /// <summary>
  /// Possible master connect behaviours.
  /// </summary>
  public enum ConnectBehaviourType
  {
    /// <summary>
    /// Master is connected and kept alive by GetStatus calls automatically by the XCPKernel.
    /// </summary>
    Automatic,
    /// <summary>
    /// Master is connected manually by the XCPMaster.
    /// </summary>
    Manual,
  }
  /// <summary>
  /// Possible DAQ Start/Stop modes.
  /// </summary>
  public enum StartStopMode : byte
  {
    /// <summary>
    /// Stops the DAQ.
    /// </summary>
    Stop,
    /// <summary>
    /// Starts the DAQ.
    /// </summary>
    Start,
    /// <summary>
    /// Selects to start the DAQ.
    /// </summary>
    Select,
  }

  /// <summary>
  /// Transfer structure holding (XCP) programming parameters.
  /// </summary>
  public sealed class XCPPrgParams
  {
    public ProgramClearMode ClearMode { get; }
    public ProgramVerifyMode VerifyMode { get; }
    public UInt16 VerifyType { get; }
    public UInt32 VerifyValue { get; }
    public XCPPrgParams(ProgramClearMode clearMode = ProgramClearMode.AbsoluteAccess
      , ProgramVerifyMode verifyMode = ProgramVerifyMode.None
      , UInt16 verifyType = 0
      , UInt32 verifyValue = 0)
    { ClearMode = clearMode; VerifyMode = verifyMode; VerifyType = verifyType; VerifyValue = verifyValue; }
  }

  /// <summary>
  /// Base class for both XCP and CCP master.
  /// 
  /// Provides common XCP/CCP functions.
  /// </summary>
  public abstract class CommMaster : IComparable, IDisposable
  {
    #region members
    /// <summary>
    /// Used to trim EPK or ID strings.
    /// </summary>
    public static readonly char[] mStrTrimmer = new char[] { '\0', '\xFF', ' ' };
    protected long mBitsTransferred;
    long mTransmissionRateQueried = -1;
    internal string SlaveAddress = string.Empty;
    protected internal volatile bool SlaveConnected;
    protected internal volatile bool ReadyToSend = true;
    protected NativeSKDLL mSeedAndKeyDLL;
    /// <summary>
    /// The adaptive characteristics dictionary.
    /// </summary>
    protected AdaptiveCharDict mAdaptiveCharDict = new AdaptiveCharDict();
    /// <summary>
    /// The list of data files to write adaptive characteristics into.
    /// </summary>
    protected List<IDataFile> mDataFiles;
    #endregion
    #region constructor
    /// <summary>Creates a communication master.</summary>
    /// <param name="connectBehaviour">Client's connect behavior</param>
    /// <param name="srcIf">The used A2L interface (may be null)</param>
    public CommMaster(ConnectBehaviourType connectBehaviour, A2LIF_DATA srcIf)
    { ConnectBehaviour = connectBehaviour; LastReceivedTime = DateTime.Now; Interface = srcIf; }
    #endregion
    #region properties
    /// <summary>Gets the connection behavior.</summary>
    public ConnectBehaviourType ConnectBehaviour { get; }
    /// <summary>Indicates, if the master is connected to the slave device.</summary>
    public virtual bool Connected { get { return false; } }
    /// <summary>Indicates the timestamp the last byte was received from a slave.</summary>
    public DateTime LastReceivedTime { get; protected set; }
    /// <summary>Returns the last state change (DateTime.MinValue if never changed).</summary>
    public DateTime LastStateChange { get; protected set; }
    /// <summary>
    /// Indicates if XCP Master and Slave byte orders differ.
    /// 
    /// Gets a value if 16/32/64 Bit values 
    /// have to be turned around in case the host 
    /// and the slave byte order differs.
    /// </summary>
    internal protected bool ChangeEndianess { get; set; }
    /// <summary>The amount of frames sent since connection is established.</summary>
    public UInt64 FramesSent { get; internal protected set; }
    /// <summary>The amount of errors received since connection is established.</summary>
    public UInt64 ErrorsReceived { get; internal protected set; }
    /// <summary>The amount of frames received since connection is established.</summary>
    public UInt64 FramesReceived { get; internal protected set; }
    /// <summary>The amount of events received since connection is established.</summary>
    public UInt64 EventsReceived { get; internal protected set; }
    /// <summary>The amount of service requests received since connection is established.</summary>
    public UInt64 ServicesReceived { get; internal protected set; }
    /// <summary>
    /// Indicates if writing to the XCP/CCP mmaster is allowed.
    /// 
    /// Writing is allowed, if
    /// - The XCP/CCP master is connected
    /// - The XCP/CCP master supports calibration
    /// </summary>
    public abstract bool CanWrite { get; }
    /// <summary>The configured DAQ dictionary.</summary>
    public DAQDict DAQs { get; internal protected set; }
    /// <summary>Gets or sets the Seed&Key DLL.</summary>
    public NativeSKDLL SeedAndKeyDLL
    {
      get { return mSeedAndKeyDLL; }
      set { mSeedAndKeyDLL = value; }
    }
    /// <summary>
    /// The currently active calibration page
    /// 
    /// Only valid in connected state!
    /// </summary>
    public abstract ECUPage ActivePage { get; }
    /// <summary>Gets the corresponding A2L communication interface.</summary>
    public A2LIF_DATA Interface { get; private set; }
    #endregion
    #region events
    /// <summary>
    /// Called if a XCP/CCP connection state changes.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler ConnectionStateChanged;
    internal protected void RaiseConnectionStateChanged()
    {
      ConnectionStateChanged?.Invoke(this, EventArgs.Empty);
    }
    /// <summary>
    /// Called if a XCP/CCP data acquisition data is received.
    /// 
    /// The event is invoked from a receiver thread (this is never the GUI thread)!
    /// </summary>
    public event EventHandler<ValuesReceivedEventArgs> OnValuesReceived;
    internal protected void RaiseOnValuesReceived(int daqListNo, double seconds, ref byte[] data)
    {
      OnValuesReceived?.Invoke(this, new ValuesReceivedEventArgs(daqListNo, seconds, ref data));
    }
    #endregion
    #region methods
    /// <summary>
    /// Sorts the measurments ascending according to it's size (including ArraySize).
    /// </summary>
    /// <param name="x">first measurement</param>
    /// <param name="y">second measurement</param>
    /// <returns></returns>
    protected static int SortBySizeAscending(A2LMEASUREMENT x, A2LMEASUREMENT y)
    {
      if (x == y)
        return 0;

      var sizeX = x.getArraySize() * x.DataType.getSizeInByte();
      var sizeY = y.getArraySize() * y.DataType.getSizeInByte();
      int result = sizeX.CompareTo(sizeY);
      if (result == 0)
        result = x.DataType.CompareTo(y.DataType);
      if (result == 0)
        result = y.Name.CompareTo(x.Name);
      return result;
    }
    /// <summary>
    /// Increases the command counter.
    /// </summary>
    /// <param name="bitLength">currently transferred bits</param>
    protected internal virtual void increaseCtr(int bitLength) { ++FramesSent; mBitsTransferred += bitLength; }
    internal void increaseReceivedFrames(int bitLength) { ++FramesReceived; mBitsTransferred += bitLength; }
    /// <summary>
    /// Gets the current transmission rate.
    /// 
    /// - Needs at least two calls in activated state to get correct values.
    /// </summary>
    /// <param name="bitsPerSec">Gets the count of bits transferred per second</param>
    public void getTransmissionRate(out double bitsPerSec)
    {
      bitsPerSec = 0;
      if (mTransmissionRateQueried == -1)
      { // first call
        mBitsTransferred = 0;
        mTransmissionRateQueried = DateTime.Now.Ticks;
        return;
      }
      long now = DateTime.Now.Ticks;
      double diffSecs = TimeSpan.FromTicks(now - mTransmissionRateQueried).TotalSeconds;
      bitsPerSec = (double)mBitsTransferred / diffSecs;
      mBitsTransferred = 0;
      mTransmissionRateQueried = now;
    }
    #region adaptive characteristics
    /// <summary>
    /// Adds all defined adaptive characteristics to the adaptive characteristic list.
    /// 
    /// - Adaptive characteristics gets cyclically refreshed by the Master.
    /// - The refresh time is defined by the MaxRefreshUnit and MaxRefreshRate members on a characteristic.
    /// </summary>
    /// <param name="project">The project containing refreshable characteristics</param>
    /// <param name="dataFiles">The list of datafiles to write the values read to</param>
    public void addAdaptiveCharacteristics(A2LPROJECT project, List<IDataFile> dataFiles)
    {
      lock (mAdaptiveCharDict)
      { // fill adaptive values
        mAdaptiveCharDict.Clear();

        if (dataFiles == null)
          // nothing to do...
          return;

        mDataFiles = dataFiles;
        // enable, get all refreshable characteristics
        var layoutRefs = new List<A2LRECORD_LAYOUT_REF>(project.CharDict.Values);
        layoutRefs.Sort(A2LNODE.sortByAddress);
        foreach (var layoutRef in layoutRefs)
        {
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          if (characteristic == null)
            continue;
          if (characteristic.MaxRefreshUnit < ScalingUnits.Time_1uSec
            || characteristic.MaxRefreshUnit > ScalingUnits.Time_1Day
             )
            // filter out non-adaptive characteristics
            continue;
          UInt64 key = (((UInt64)characteristic.MaxRefreshUnit) << 32) + (UInt64)characteristic.MaxRefreshRate;
          AdaptiveState refreshState;
          if (!mAdaptiveCharDict.TryGetValue(key, out refreshState))
          {
            refreshState = new AdaptiveState();
            mAdaptiveCharDict[key] = refreshState;
          }
          refreshState.add(characteristic);
        }
      }
    }
#if ASAP2_VALUES
    /// <summary>
    /// Gets or sets the measurement access data.
    /// </summary>
    public ASAP2.Values.MeasurementAccessData MeasurementAccessData { get; set; }
    /// <summary>
    /// Requests all adaptive characteristics.
    /// </summary>
    void requestAdaptiveCharacteristics()
    {
      if (MeasurementAccessData != null)
      {
        for (int i = 0; i < MeasurementAccessData.MemoryRanges.Count; ++i)
        {
          var r = MeasurementAccessData.MemoryRanges[i];
          var len = r.Next - r.Start;
          byte[] data;
          if (!readSync((int)len, 0, r.Start, out data))
            // stop requesting values on any error
            break;
          MeasurementAccessData.DataChunks[i] = data;
        }
      }

      AdaptiveCharDict adaptiveDict = null;
      lock (mAdaptiveCharDict)
      {
        if (mAdaptiveCharDict.Count == 0)
          // nothing to do
          return;
        adaptiveDict = (AdaptiveCharDict)mAdaptiveCharDict.Clone();
      }

      long elpasedTicks = DateTime.Now.Ticks;
      var en = adaptiveDict.GetEnumerator();
      while (en.MoveNext())
      {
        var unit = (ScalingUnits)(en.Current.Key >> 32);
        int rate = (int)(en.Current.Key & UInt32.MaxValue);
        var refreshState = en.Current.Value;
        if (!refreshState.isUpdateRequired(unit, rate, elpasedTicks))
          // no update required
          continue;
        // Now read the required data from the ECU
        foreach (var range in refreshState.MemoryRanges)
        {
          if (!SlaveConnected)
            return;
          int len = (int)(range.Next - range.Start);
          byte[] data = new byte[len];
          if (!readSync(len, 0, range.Start, out data))
            // failed to read
            return;

          for (int i = mDataFiles.Count - 1; i >= 0; --i)
          {
            var dataFile = mDataFiles[i];
            var segment = dataFile.findMemSeg(range.Start);
            if (segment == null)
              continue;
            segment.setDataBytes(range.Start, data);
          }
        }
        refreshState.LastElapsedTicks = DateTime.Now.Ticks;
      }
    }
#endif
    #endregion
    /// <summary>
    /// Disconnects the master from the slave.
    /// </summary>
    /// <returns>true if successful</returns>
    public abstract bool Disconnect();
    /// <summary>
    /// Idle call from the CommKernel.
    /// 
    /// - Will only be called if the ConnectBehaviour is ConnectBehaviour.Automatic.
    /// - Refreshes potentially available adaptive characteristics.
    /// - Override this to handle IDL calls in derived XCP or CCP Masters.
    /// </summary>
    internal virtual void onIdle()
    {
#if ASAP2_VALUES
      if (!Connected || !Settings.Default.RefreshCharacteristics)
        // nothing to do
        return;
      requestAdaptiveCharacteristics();
#endif
    }
    internal abstract int getAliveCycleTime();
    /// <summary>
    /// Tries to connect to a XCP/CCP slave
    /// 
    /// Used by the CommKernel.
    /// </summary>
    /// <param name="mode">The connect mode (only used for xcp protocol</param>
    /// <returns>true if successful</returns>
    internal abstract bool internalConnect(ConnectMode mode, out int maxDTO);
    /// <summary>
    /// Get current session status from slave.
    /// 
    /// Used by the CommKernel.
    /// </summary>
    /// <returns>true if successful</returns>
    internal abstract bool internalGetStatus();
    internal abstract void setConnectionState<T>(T respConnect) where T : ResponseBase;
    /// <summary>
    /// Gets the last error occured as text.
    /// </summary>
    public abstract string LastErrorText { get; }
    /// <summary>
    /// Gets the last error occured as enum.
    /// </summary>
    public abstract Enum LastErrorEnum { get; }
    /// <summary>
    /// Write data synchronously to the ECU.
    /// 
    /// Depending on the data length to be written 
    /// CmdDownload or CmdShortDownload is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public abstract bool writeSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      );

    /// <summary>
    /// Program data synchronously to the ECU.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to program</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <param name="modes">Additional (XCP) programming parameters</param>
    /// <returns>true if successful</returns>
    public abstract bool programSync(byte addressExtension, UInt32 address, byte[] data
      , EventHandler<ProgressArgs> progressHandler = null
      , XCPPrgParams modes = null
      );
#if WIN32_OPTIMIZED
    /// <summary>
    /// Write data synchronously to the ECU.
    /// 
    /// Depending on the data length to be written 
    /// CmdDownload or CmdShortDownload is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="size">The data size to write</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public bool writeSync(byte addressExtension, UInt32 address, IntPtr data, int size
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      var bData = new byte[size];
      Marshal.Copy(data, bData, 0, size);
      return writeSync(addressExtension, address, bData, progressHandler);
    }
    /// <summary>
    /// Program data synchronously to the ECU.
    /// 
    /// Depending on the data length to be written 
    /// CmdDownload or CmdShortDownload is used.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to write to (If the adress is UInt32.MaxValue, the data should be written to the current MTA location)</param>
    /// <param name="data">The data to write</param>
    /// <param name="size">The data size to write</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <param name="modes">Additional (XCP) programming parameters</param>
    /// <returns>true if successful</returns>
    public bool programSync(byte addressExtension, UInt32 address, IntPtr data, int size
      , EventHandler<ProgressArgs> progressHandler = null
      , XCPPrgParams modes = null
      )
    {
      var bData = new byte[size];
      Marshal.Copy(data, bData, 0, size);
      return programSync(addressExtension, address, bData, progressHandler, modes);
    }
#endif
    /// <summary>
    /// Read data synchronously from the ECU.
    /// </summary>
    /// <param name="len">The length to read</param>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to read from (If the adress is UInt32.MaxValue, the data should be read from the current MTA location)</param>
    /// <param name="data">The resulting data (only valid, if return value is CmdResult.OK)</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public abstract bool readSync(int len, byte addressExtension, UInt32 address, out byte[] data, EventHandler<ProgressArgs> progressHandler = null);
    /// <summary>
    /// Copy data from one ECU page to another.
    /// </summary>
    /// <param name="srcPage">The page to copy data from</param>
    /// <param name="dstPage">The page to copy data to</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown, if srcPage == dstPage</exception>
    public abstract bool copyPage2Page(ECUPage srcPage, ECUPage dstPage);
    /// <summary>
    /// Sets the specified page as active page.
    /// 
    /// Pages of all segments got switched!
    /// </summary>
    /// <param name="page">The page to set active</param>
    /// <returns>true if successful</returns>
    public abstract bool setPage(ECUPage page);
    /// <summary>
    /// Write the characteristic value read from the specified datafile to the device.
    /// 
    /// - Contained referenced AxisPts will be written to the ECU, too.
    /// - The ECU get switched to the RAM page if not already done. 
    /// </summary>
    /// <param name="dataFile">The data file to read from</param>
    /// <param name="layoutRef">The characteristic to write to the data file</param>
    /// <returns>true if successful</returns>
    public bool writeCharacteristic(IDataFile dataFile, A2LRECORD_LAYOUT_REF layoutRef)
    {
      return writeCharacteristic(dataFile, layoutRef, true);
    }
    /// <summary>
    /// Write the characteristic value read from the specified datafile to the device.
    /// 
    /// - The ECU get switched to the RAM page if not already done. 
    /// </summary>
    /// <param name="dataFile">The data file to read from</param>
    /// <param name="layoutRef">The characteristic to write to the data file</param>
    /// <param name="writeAxisRefs">true, if referenced AxisPts should be written, too</param>
    /// <returns>true if successful</returns>
    public bool writeCharacteristic(IDataFile dataFile, A2LRECORD_LAYOUT_REF layoutRef, bool writeAxisRefs)
    {
      if (!SlaveConnected)
        // device is not connected
        return false;

      var memSeg = dataFile.findMemSeg(layoutRef.Address);
      if (memSeg == null)
        // segment not found in data file
        return false;

      byte[] data = null;
      try { data = memSeg.getDataBytes(layoutRef.Address, (uint)layoutRef.getSize()); }
      catch (ArgumentOutOfRangeException)
      { // characteristic could not be read from memory segment
        return false;
      }

      bool result = false;
      if (ActivePage != ECUPage.RAM)
      { // change page to RAM if required
        if (!(result = setPage(ECUPage.RAM)))
          // failed to switch to RAM page
          return result;
      }
      // write value to device
      result = writeSync(0, layoutRef.Address, data);

      if (result && writeAxisRefs)
      { // Write all referenced axis pts
        var axisdescs = layoutRef.getNodeList<A2LAXIS_DESCR>(false);
        for (int i = 0; i < axisdescs.Count; ++i)
        { // iterate each axis
          switch (axisdescs[i].AxisType)
          {
            case AXIS_TYPE.COM_AXIS:
              // Recursive call to write the referenced AxisPts, too.
              var axisPts = axisdescs[i].RefAxisPtsRef;
              result = axisPts != null && writeCharacteristic(dataFile, axisPts, false);
              break;
          }
        }
      }
      return result;
    }
    /// <summary>
    /// Read the characteristic value from the ECU into the specified datafile.
    /// 
    /// - Contained referenced AxisPts will be read from the ECU, too.
    /// </summary>
    /// <param name="pageToReadFrom">ECU page to read from</param>
    /// <param name="dataFile">The datafile to write the data into</param>
    /// <param name="layoutRef">The characteristic to read</param>
    /// <returns>true if successful</returns>
    public bool readCharacteristic(ECUPage pageToReadFrom, IDataFile dataFile, A2LRECORD_LAYOUT_REF layoutRef)
    {
      return readCharacteristic(pageToReadFrom, dataFile, layoutRef, true);
    }
    /// <summary>
    /// Read the characteristic value from the ECU into the specified datafile.
    /// </summary>
    /// <param name="pageToReadFrom">ECU page to read from</param>
    /// <param name="dataFile">The datafile to write the data into</param>
    /// <param name="layoutRef">The characteristic to read</param>
    /// <param name="readAxisRefs">true, if referenced AxisPts should be read, too</param>
    /// <returns>true if successful</returns>
    public bool readCharacteristic(ECUPage pageToReadFrom, IDataFile dataFile, A2LRECORD_LAYOUT_REF layoutRef, bool readAxisRefs)
    {
      if (!SlaveConnected)
        // device is not connected
        return false;

      var memSeg = dataFile.findMemSeg(layoutRef.Address);
      if (memSeg == null)
        // segment not found in data file
        return false;

      bool result = false;
      if (ActivePage != pageToReadFrom)
      { // change page if required
        if (!(result = setPage(pageToReadFrom)))
          // failed to switch to read page
          return result;
      }
      // read value from device
      int len = layoutRef.getSize();
      byte[] data;
      result = readSync(len, (byte)layoutRef.AddressExtension, layoutRef.Address, out data);
      if (result)
        // write data to memory segment
        memSeg.setDataBytes(layoutRef.Address, data);

      if (result && readAxisRefs)
      { // Read all referenced axis pts
        var axisdescs = layoutRef.getNodeList<A2LAXIS_DESCR>(false);
        for (int i = 0; i < axisdescs.Count; ++i)
        { // iterate each axis
          switch (axisdescs[i].AxisType)
          {
            case AXIS_TYPE.COM_AXIS:
              // Recursive call to read the referenced AxisPts, too.
              var axisPts = axisdescs[i].RefAxisPtsRef;
              result = axisPts != null && readCharacteristic(pageToReadFrom, dataFile, axisPts, false);
              break;
          }
        }
      }
      return result;
    }
    /// <summary>
    /// Get the checksum from the slave.
    /// </summary>
    /// <param name="addressExtension">The segment's address extension</param>
    /// <param name="address">The start address</param>
    /// <param name="size">The size of the data block to build the checksum over</param>
    /// <param name="checksum">The resulting checksum</param>
    /// <returns>true if successful</returns>
    public abstract bool getChecksum(byte addressExtension, UInt32 address, UInt32 size, out UInt32 checksum);
    /// <summary>Reset the last error</summary>
    public abstract void resetLastError();
    #region measurement access
    /// <summary>
    /// Returns an array of operating points if available.
    /// 
    /// - Current values of axis input measurements are returned
    /// - This means that the axis input measurements must be measured when calling this function
    /// - Only curves, maps and cube's are supported.
    /// </summary>
    /// <param name="characteristic">The characteristic to get the operating point for</param>
    /// <returns>null if not supported or not available (not measuring), else an array of values (length is depending on the axis count)</returns>
    public double[] getOperatingPoint(A2LCHARACTERISTIC characteristic)
    {
      if (!Connected)
        return null;
      var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
      if (axisDescs.Count == 0)
        // not supported
        return null;

      var measurementX = axisDescs[0].RefMeasurement;
      double valueX = double.NaN;
      if (measurementX != null)
      {
        getDAQPhysValue(measurementX, out valueX);
        valueX = Math.Round(valueX, Math.Min(15, measurementX.DecimalCount));
      }
      if (axisDescs.Count < 2)
        // curve
        return new double[] { valueX };
      var measurementY = axisDescs[1].RefMeasurement;
      double valueY = double.NaN;
      if (measurementY != null)
      {
        getDAQPhysValue(measurementY, out valueY);
        valueY = Math.Round(valueY, Math.Min(15, measurementY.DecimalCount));
      }
      if (axisDescs.Count < 3)
        // map
        return new double[] { valueX, valueY };
      var measurementZ = axisDescs[2].RefMeasurement;
      double valueZ = double.NaN;
      if (measurementZ != null)
      {
        getDAQPhysValue(measurementZ, out valueZ);
        valueZ = Math.Round(valueZ, Math.Min(15, measurementZ.DecimalCount));
      }
      if (axisDescs.Count < 4)
        // cuboid
        return new double[] { valueX, valueY, valueZ };
      var measurement4 = axisDescs[3].RefMeasurement;
      double value4 = double.NaN;
      if (measurement4 != null)
      {
        getDAQPhysValue(measurement4, out value4);
        value4 = Math.Round(value4, Math.Min(15, measurement4.DecimalCount));
      }
      if (axisDescs.Count < 5)
        // cube_4
        return new double[] { valueX, valueY, valueZ, value4 };
      var measurement5 = axisDescs[4].RefMeasurement;
      double value5 = double.NaN;
      if (measurement5 != null)
      {
        getDAQPhysValue(measurement5, out value5);
        value5 = Math.Round(value5, Math.Min(15, measurement5.DecimalCount));
      }
      // cube_5
      return new double[] { valueX, valueY, valueZ, value4, value5 };
    }
    /// <summary>
    /// Gets the last received raw value for the specified measurement address.
    /// </summary>
    /// <param name="measurement">The measurement to get the raw value for</param>
    /// <param name="rawValue">[out] the raw value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getDAQRawValue(A2LMEASUREMENT measurement, out double rawValue)
    {
      return getDAQRawValue(measurement, 0, 0, out rawValue);
    }
    /// <summary>
    /// Gets the last received raw value for the specified measurement address.
    /// 
    /// This is the version for an A2LMEASUREMENT with ARRAY_SIZE > 1 or a MATRIX_DIM specification.
    /// </summary>
    /// <param name="measurement">The measurement to get the raw value for</param>
    /// <param name="x">The column</param>
    /// <param name="y">The row</param>
    /// <param name="rawValue">[out] the raw value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getDAQRawValue(A2LMEASUREMENT measurement, int x, int y, out double rawValue)
    {
      if ((x > 0 || y > 0) && measurement.MatrixDim == null)
        // wrong call to a array which is not defined as an array
        throw new ApplicationException();

      int lengthInByte = measurement.DataType.getSizeInByte();
      uint address = measurement.Address + measurement.getAddressOffset(x, y);
      lock (DAQs)
      {
        rawValue = double.NaN;
        ODTEntry entry;
        if (!DAQs.ODTEntries.TryGetValue(address, out entry))
          return false;
        rawValue = entry.Value;

        if (measurement.BitMask != UInt64.MaxValue && !double.IsNaN(rawValue))
        { // handle bitmasked values
          UInt64 rawUIValue = Convert.ToUInt64(rawValue);
          rawUIValue &= measurement.BitMask;
          rawUIValue >>= Helpers.Extensions.getShiftCount(measurement.BitMask);
          rawValue = rawUIValue;
        }
        return true;
      }
    }
    /// <summary>
    /// Gets the last received physical value for the specified measurement address.
    /// </summary>
    /// <param name="measurement">The measurement to get the raw value for</param>
    /// <param name="value">[out] the value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getDAQPhysValue(A2LMEASUREMENT measurement, out double value)
    {
      return getDAQPhysValue(measurement, 0, 0, out value);
    }
    /// <summary>
    /// Gets the last received physical value for the specified measurement address.
    /// 
    /// This is the version for an A2LMEASUREMENT with ARRAY_SIZE > 1 or a MATRIX_DIM specification.
    /// </summary>
    /// <param name="measurement">The measurement to get the physical value for</param>
    /// <param name="x">The column</param>
    /// <param name="y">The row</param>
    /// <param name="value">[out] the value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getDAQPhysValue(A2LMEASUREMENT measurement, int x, int y, out double value)
    {
      if (!getDAQRawValue(measurement, x, y, out value))
        return false;
      value = measurement.RefCompuMethod.toPhysical(value);
      return true;
    }
    /// <summary>
    /// (Re)Configures the specified measurements for data acquisition.
    /// 
    /// A potentially existing configuration will be deleted.
    /// </summary>
    /// <param name="measurements">[in/out] The measurements to be configured,
    /// successfully configured measurements will be removed from this list!
    /// </param>
    public abstract void configureMeasurements(List<A2LMEASUREMENT> measurements);
    /// <summary>
    /// Stops a running data acquisition.
    /// 
    /// The configured data acquisition could be restarted without 
    /// calling configureMeasurements again with startMeasurements.
    /// </summary>
    /// <param name="doSynchronized">true, if the DAQ List should be stopped synchronously (Only supported on XCP connections).</param>
    /// <returns>true if successful</returns>
    public abstract bool stopMeasurements(bool doSynchronized);
    /// <summary>
    /// Starts a (previously configured by configureMeasurements) data acquisition.
    /// </summary>
    /// <param name="doSynchronized">true, if the DAQ List should be started synchronously</param>
    /// <returns>true if successful</returns>
    public abstract bool startMeasurements(bool doSynchronized);
    #endregion
    #endregion
    #region IComparable Members
    /// <summary>
    /// Compare method implementation.
    /// 
    /// This method should do a difference between the 
    /// IPAddresses and ports to support more than 
    /// one ECU simulators on the same machine but with 
    /// different ports.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    int IComparable.CompareTo(object obj)
    {
      return SlaveAddress.CompareTo(((CommMaster)obj).SlaveAddress);
    }
    #endregion
    #region IDisposable Members
    /// <summary>
    /// Frees used resources.
    /// 
    /// Potentially existing Seed and Key DLLs are unloaded here.
    /// </summary>
    public virtual void Dispose()
    {
      ConnectionStateChanged = null;
      OnValuesReceived = null;
      if (mSeedAndKeyDLL != null)
        mSeedAndKeyDLL.Dispose();
    }
    #endregion
  }
  /// <summary>
  /// Base class represents a CCP or XCP response.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public class ResponseBase
  {
    /// <summary>
    /// Internal use only!
    /// </summary>
    public ResponseBase() { }
    /// <summary>
    /// Changes the endianess of the response.
    /// </summary>
    public virtual void changeEndianness() { }
  }
  /// <summary>
  /// Basic communicator implementation.
  /// 
  /// Acts as something like an endpoint for XCP/CCP Master communication.
  /// </summary>
  public abstract class Communicator : IDisposable
  {
    #region members
    /// <summary>
    /// Signals a received response.
    /// </summary>
    internal readonly ManualResetEvent WaitForResponse = new ManualResetEvent(false);
    /// <summary>
    /// Signals a processed response.
    /// </summary>
    internal readonly ManualResetEvent ResponseProcessed = new ManualResetEvent(true);
    internal string SourceAddress = string.Empty;
    /// <summary>Response pending flag</summary>
    internal volatile bool RespPending;
    #endregion
    #region methods
    /// <summary>
    /// Closes the communication (frees all used resources).
    /// 
    /// - Any derived class has to implement this method.
    /// </summary>
    protected abstract void close();
    /// <summary>
    /// Sends data.
    /// </summary>
    /// <param name="data">The data to send</param>
    /// <returns>The count of bytes sent</returns>
    internal abstract int send(byte[] data);
    /// <summary>
    /// Resets the communication source.
    /// 
    /// - Calls reset on the corresponding communicator
    /// - Clears the receive buffer
    /// </summary>
    internal virtual void reset() { }
    /// <summary>
    /// Sets the Max DTO value.
    /// 
    /// Only used on some XCP (TCP/UDP,...).
    /// </summary>
    /// <param name="maxDTO">The maximum DTO value</param>
    internal virtual void setMaxDTO(int maxDTO) { }
    #endregion
    #region IDisposable Members
    /// <summary>
    /// Disposes all used resources.
    /// 
    /// Inparticular:
    /// - The communication channel 
    /// </summary>
    public virtual void Dispose()
    {
      WaitForResponse.Dispose();
      ResponseProcessed.Dispose();
      close();
    }
    #endregion
  }
}