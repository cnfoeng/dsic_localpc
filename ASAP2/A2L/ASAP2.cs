﻿/*!
 * @file    
 * @brief   Implements the ASAP2 object model.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.CCP;
using jnsoft.ASAP2.Formulas;
using jnsoft.ASAP2.Helpers;
using jnsoft.ASAP2.Properties;
using jnsoft.ASAP2.XCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Text;
using System.Xml.Serialization;

namespace jnsoft.ASAP2
{
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LIF_DATA : A2LUNSUPPORTEDNODE
  {
    #region constructor
#if ASAP2_WRITER
    internal A2LIF_DATA() { }
#endif
    internal A2LIF_DATA(int startLine) : base(startLine) { }
    #endregion
#if IS_IMPLEMENTED
    #region properties
    /// <summary>
    /// Version is valid if <> 0, usually on XCPplus.
    /// </summary>
    [Category(Constants.mDataCategory), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt16), "0x0"), Description("Version is valid if <> 0, usually on XCPplus.")]
    public UInt16 Version { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      if (startIndex == parameterList.Count - 2)
        Version = (UInt16)A2LParser.parse2IntVal(parameterList[parameterList.Count - 1]);
      return true;
    }
    #endregion
#endif
  }
  /// <summary>
  /// The one and only root node of an A2L source file.
  /// 
  /// At least this node is required.
  /// </summary>
  public sealed partial class A2LPROJECT : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    internal A2LPROJECT(int startLine) : base(startLine) { }
    #endregion
    #region events
    public event EventHandler<AdditiveDescriptionEventArgs> GetAdditiveDescription;
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    /// <summary>Gets the A2LMEASUREMENT dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific measurement by name
    ///   var gear = a2lParser.Project.MeasDict["gear"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LMEASUREMENT dictionary.")]
    public A2LMeasDict MeasDict { get; internal set; }
    /// <summary>Gets the A2LCHARACTERISTIC dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific characteristic by name
    ///   var characteristic1 = (A2LCHARACTERISTIC)a2lParser.Project.CharDict["characteristic1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LCHARACTERISTIC dictionary.")]
    public A2LCharDict CharDict { get; internal set; }
    /// <summary>Gets the A2LFUNCTION dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific function by name
    ///   var function1 = a2lParser.Project.FuncDict["function1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LFUNCTION dictionary.")]
    public A2LRefBaseDict FuncDict { get; internal set; }
    /// <summary>Gets the A2LGROUP dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific group by name
    ///   var group1 = a2lParser.Project.GroupDict["Group1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LGROUP dictionary.")]
    public A2LRefBaseDict GroupDict { get; internal set; }
    /// <summary>Gets the A2LCOMPU_METHOD dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific compu method by name
    ///   var compuMethod1 = a2lParser.Project.CompDict["CompuMethod1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LCOMPU_METHOD dictionary.")]
    public A2LCompDict CompDict { get; internal set; }
    /// <summary>Gets the A2LRECORD_LAYOUT dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific record layout by name
    ///   var recordLayout1 = a2lParser.Project.RecLayDict["recordLayout1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LRECORD_LAYOUT dictionary.")]
    public A2LRecLayDict RecLayDict { get; internal set; }
    /// <summary>Gets the A2LCOMPU_TAB dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific compu tab by name
    ///   var compuTab1 = a2lParser.Project.CompTabDict["compuTab1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LCOMPU_TAB dictionary.")]
    public A2LCompTabDict CompTabDict { get; internal set; }
    /// <summary>Gets the A2LCOMPU_VTAB dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific compu vtab by name
    ///   var compuVTab1 = a2lParser.Project.CompVTabDict["compuVTab1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LCOMPU_VTAB dictionary.")]
    public A2LCompVTabDict CompVTabDict { get; internal set; }
    /// <summary>Gets the A2LCOMPU_VTABRANGE dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific compu vtab range by name
    ///   var compuVTabRange1 = a2lParser.Project.CompVTabRangeDict["compuVTabRange1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LCOMPU_VTABRANGE dictionary.")]
    public A2LCompVTabRangeDict CompVTabRangeDict { get; internal set; }
    /// <summary>Gets the A2LUNIT dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a specific unit by name
    ///   var mph = a2lParser.Project.UnitDict["mph"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LUNIT dictionary.")]
    public A2LUnitDict UnitDict { get; internal set; }
    /// <summary>Gets the A2LVIRTUAL dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a virtual instance by name
    ///   var virtual_entity = a2lParser.Project.VirtualDict["virtual_entity1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LVIRTUAL dictionary.")]
    public A2LVirtualDict VirtualDict { get; internal set; }
    /// <summary>Gets the A2LBLOB dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a blob instance by name
    ///   var blob = a2lParser.Project.BlobDict["Blob1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LBLOB dictionary.")]
    public A2LBlobDict BlobDict { get; internal set; }
    /// <summary>Gets the A2LTYPEDEF dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a typedef instance by name
    ///   var typedef1 = a2lParser.Project.TypeDefDict["TypeDef1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LTYPEDEF dictionary.")]
    public A2LTypeDefDict TypeDefDict { get; internal set; }
    /// <summary>Gets the A2LINSTANCE dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a typedef instance by name
    ///   var instance1 = a2lParser.Project.InstanceDict["Instance1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LINSTANCE dictionary.")]
    public A2LInstanceDict InstanceDict { get; internal set; }
    /// <summary>Gets the A2LTRANSFORMER dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Access a transformer instance by name
    ///   var transformer1 = a2lParser.Project.TransformerDict["Transformer1"];
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2TRANSFORMER dictionary.")]
    public A2LTransformerDict TransformerDict { get; internal set; }
    /// <summary>Gets the A2LFormula dictionary.</summary>
    /// <example>Usage example:
    /// <code>
    /// using (var a2lParser = new A2LParser())
    /// {
    ///   // parse test.a2l
    ///   a2lParser.parse("test.a2l");
    ///   // Compute a SYSTEM_CONSTANT or a Formula
    ///   double result = a2lParser.Project.FormulaDict["Constant 1"].toPhysical(0);
    /// }
    /// </code></example>
    [XmlIgnore, Category(Constants.CATData), Description("The A2LFormula dictionary (Contains an instance of A2LFormula for each SYSTEM_CONSTANT, FORMULA/FORMULA_INV).")]
    public A2LFormulaDict FormulaDict { get; internal set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      Name = parameterList[startIndex];
      Description = parameterList[startIndex + 1].Replace("\\r\\n", "\n").Replace("\\n", "\n"); ;
      return true;
    }
    /// <summary>
    /// Calls the Additive Description eventhandler. 
    /// 
    /// If set, returns an additive Description from users code for the specified node.
    /// </summary>
    /// <param name="node">The node to get an additive description for.</param>
    /// <returns>returns teh additive description or null if eventhandler is not set</returns>
    internal string RaiseGetAdditiveDescription(A2LNODE node)
    {
      if (GetAdditiveDescription == null)
        return null;
      var args = new AdditiveDescriptionEventArgs();
      GetAdditiveDescription(node, args);
      return args.DescriptionAddition;
    }
    /// <summary>
    /// Gets all symbols defined. 
    /// 
    /// - Symbols are A2LObject's containing an ECU address (A2LADDRESS based classes, ususally measurements and characteristics).
    /// - These objects may define a symbol name by it's SYMBOL_LINK property.
    /// - If no SYMBOL_LINK property is defined, the A2L object's name is used.
    /// </summary>
    /// <returns>Dictionary of symbols</returns>
    public HashSet<string> getSymbols()
    {
      var result = new HashSet<string>();
      foreach (var module in getNodeList<A2LMODULE>(false))
      { // iterate through modules
        string separator = ".";
        var variantCoding = module.getNode<A2LVARIANT_CODING>(false);
        if (variantCoding != null && !string.IsNullOrEmpty(variantCoding.VAR_SEPARATOR))
          separator = variantCoding.VAR_SEPARATOR;

        foreach (var convRef in module.enumChildNodes<A2LADDRESS>())
        { // Iterate over all A2L Instances with Addresses.
          if (!convRef.IsInstanced)
          { // filter objects created by A2LINSTANCEs
            int offset;
            result.Add(convRef.getSymbol(out offset));
          }
        }
      }
      return result;
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LHEADER : A2LNODE
  {
    #region constructor
    internal A2LHEADER(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Comment { get; set; }
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string VERSION { get; set; }
    [Category(Constants.CATData)]
    public string PROJECT_NO { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Comment = parameterList[i++];
      while (i < parameterList.Count - 1)
      {
        string parStr = parameterList[i++];
        string valueStr = parameterList[i++];
        switch (parStr)
        {
          case A2LC_Version: VERSION = valueStr; break;
          case A2LC_ProjectNo: PROJECT_NO = valueStr; break;
          default: --i; break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LMODULE : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    internal A2LMODULE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      Name = parameterList[startIndex];
      Description = parameterList[startIndex + 1].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      return true;
    }
    /// <summary>
    /// Gets all memory ranges used from contained characteristics.
    /// </summary>
    /// <returns>a list of memory ranges</returns>
    public MemoryRangeList getMemoryRanges()
    {
      var characteristics = getNodeList<A2LRECORD_LAYOUT_REF>(false);
      return getMemoryRanges(characteristics);
    }
    /// <summary>
    /// Gets all memory ranges used by the specified characteristics.
    /// </summary>
    /// <param name="characteristics"></param>
    /// <returns>a list of memory ranges</returns>
    public static MemoryRangeList getMemoryRanges(List<A2LRECORD_LAYOUT_REF> characteristics)
    {
      characteristics.Sort(sortByAddress);
      var ranges = new MemoryRangeList();
      for (int j = 0; j < characteristics.Count; ++j)
      { // iterate over all characteristics
        var characteristic = characteristics[j];
        var start = characteristic.Address;
        var next = start + (UInt32)characteristic.getSize();

        if (ranges.Count > 0)
        { // previous ranges available
          var range = ranges[ranges.Count - 1];
          if (start <= range.Next)
          { // used memory at least adjacent to previous range
            range.Start = Math.Min(start, range.Start);
            range.Next = Math.Max(next, range.Next);
            continue;
          }
        }
        // add a new range
        ranges.Add(new MemoryRange(start, next));
      }
      return ranges;
    }
    /// <summary>
    /// Creates initial memory segments from the A2LMEMORY_LAYOUT or A2LMEMORY_SEGMENT nodes.
    /// 
    /// -# If no memory segments exist, the method tries to create 
    /// memory segments from a potential CCP page definition.
    /// -# If no CCP page definition exists, the method tries to create
    /// default segments from all characteristics (default is 64 kBytes).
    /// </summary>
    /// <param name="excludeRAM">true, if RAM segments should be excluded.
    /// Note, that A2LMEMORY_LAYOUTs are always interpreted as Non-RAM segments.</param>
    /// <returns>Initial memory segments or null if no segments are created</returns>
    public MemorySegmentList createInitialMemorySegments(bool excludeRAM)
    {
      var newSegList = new List<MemorySegment>();
      var modPar = getNode<A2LMODPAR>(false);
      var segments = modPar != null ? modPar.getNodeList<A2LMEMORY_SEGMENT>() : null;

      // add segments
      if (segments != null)
      {
        for (int i = segments.Count - 1; i >= 0; --i)
        {
          var segment = segments[i];
          if (excludeRAM && segment.PrgType == MEMORYPRG_TYPE.DATA)
          {
            switch (segment.MemoryType)
            {
              case MEMORY_TYPE.RAM:
              case MEMORY_TYPE.REGISTER:
                // filter out RAM segments
                continue;
            }
          }
          newSegList.Add(new MemorySegment(segment.Address, segment.Size, segment.PrgType));
        }
      }
      // add layouts
      var layouts = modPar != null ? modPar.getNodeList<A2LMEMORY_LAYOUT>() : null;
      if (layouts != null)
      {
        for (int i = layouts.Count - 1; i >= 0; --i)
        {
          var layout = layouts[i];
          newSegList.Add(new MemorySegment(layout.Address, layout.Size, toMemoryPrgType(layout.PrgType)));
        }
      }

      if (newSegList.Count == 0)
      { // at least empty, try ccp add definitions
        var medias = new List<A2LNODE>(getNodeList<A2LIF_DATA>().ToArray());
        foreach (var media in medias)
        {
          foreach (var page in media.enumChildNodes<A2LCCP_DEFINED_PAGES>())
          {
            if (excludeRAM && (page.PageType & CCP_MEMORY_PAGE_TYPE.FLASH) == 0)
              // non flash and exclude ram requested
              continue;
            newSegList.Add(new MemorySegment(page.Address, page.Length, toMemoryPrgType(page.PageType)));
          }
        }
      }

      if (newSegList.Count == 0)
      { // nothing defined...try to create default segments
        var sortCharDict = new SortedDictionary<uint, int>();
        if (modPar != null && !string.IsNullOrEmpty(modPar.EPK))
          // add the EPK memory to sorted mem dict
          sortCharDict[modPar.EPKAddress] = (int)modPar.EPK.Length;

        var project = getParent<A2LPROJECT>();
        foreach (var characteristic in project.CharDict.Values)
          // add all characteristics to the sorted mem dict
          sortCharDict[characteristic.Address] = characteristic.getSize();

        var en = sortCharDict.GetEnumerator();
        UInt32 baseAddress = 0;
        var nextAddress = UInt32.MaxValue;
        while (en.MoveNext())
        { // iterate over sorted mem dict
          var address = en.Current.Key;
          if (baseAddress == 0)
            // init base
            baseAddress = address;

          if (nextAddress != UInt32.MaxValue
            && address / 0x10000 != nextAddress / 0x10000
            )
          { // this is tho 64kByte border split
            var minAddress = (UInt32)jnsoft.Helpers.Extensions.alignDown((int)baseAddress, 2);
            var len = (int)(nextAddress - minAddress);
            len = jnsoft.Helpers.Extensions.alignUp(len, 2);
            newSegList.Add(new MemorySegment(minAddress, (UInt32)len, MEMORYPRG_TYPE.DATA));
            // set new base
            baseAddress = address;
          }
          nextAddress = (UInt32)(address + en.Current.Value);
        }
        if (nextAddress != UInt32.MaxValue)
        {
          var minAddress = (UInt32)jnsoft.Helpers.Extensions.alignDown((int)baseAddress, 2);
          var len = (int)(nextAddress - minAddress);
          len = jnsoft.Helpers.Extensions.alignUp(len, 2);
          newSegList.Add(new MemorySegment(minAddress, (UInt32)len, MEMORYPRG_TYPE.DATA));
        }
      }
      return newSegList.Count == 0
        ? null
        : new MemorySegmentList(newSegList);
    }
    /// <summary>
    /// Converts PRG_TYPE values to MEMORYPRG_TYPE values.
    /// </summary>
    /// <param name="prgType">The source value</param>
    /// <returns>The destination value</returns>
    static MEMORYPRG_TYPE toMemoryPrgType(PRG_TYPE prgType)
    {
      switch (prgType)
      {
        case PRG_TYPE.PRG_CODE: return MEMORYPRG_TYPE.CODE;
        case PRG_TYPE.PRG_DATA: return MEMORYPRG_TYPE.DATA;
        case PRG_TYPE.PRG_RESERVED: return MEMORYPRG_TYPE.RESERVED;
        default: throw new NotSupportedException(prgType.ToString());
      }
    }
    /// <summary>
    /// Converts CCP_MEMORY_PAGE_TYPE values to MEMORYPRG_TYPE values.
    /// </summary>
    /// <param name="pageType">The source value</param>
    /// <returns>The destination value</returns>
    static MEMORYPRG_TYPE toMemoryPrgType(CCP_MEMORY_PAGE_TYPE pageType)
    {

      if ((pageType & CCP_MEMORY_PAGE_TYPE.ROM) > 0)
        return MEMORYPRG_TYPE.CODE;
      return MEMORYPRG_TYPE.DATA;
    }
    /// <summary>
    /// Gets the XCP (UDP/TCP/CAN) related settings from the XCP IF_DATA section.
    /// </summary>
    /// <param name="xcpMediaList">only valid if return value is true</param>
    /// <param name="xcpProtocolLayer">only valid if return value is true</param>
    /// <param name="xcpChecksum">may be null and may only valid if return value is true</param>
    /// <param name="xcpPAG">The XCP PAG section (may be null and may only valid if return value is true)</param>
    /// <param name="xcpDAQ">The XCP DAQ section (may be null and may only valid if return value is true)</param>
    /// <param name="xcpPGM">The XCP PGM section (may be null and may only valid if return value is true)</param>
    /// <returns>false is no XCP (UDP or TCP or CAN) IF_DATA section is found</returns>
    public bool getXCPSettings(ref List<A2LXCP_MEDIA> xcpMediaList
      , out A2LXCP_PROTOCOL_LAYER xcpProtocolLayer
      , out A2LXCP_CHECKSUM xcpChecksum
      , out A2LXCP_PAG xcpPAG
      , out A2LXCP_DAQ xcpDAQ
      , out A2LXCP_PGM xcpPGM
      )
    {
      xcpProtocolLayer = null;
      xcpPAG = null;
      xcpDAQ = null;
      xcpPGM = null;
      var medias = new List<A2LNODE>(getNodeList<A2LIF_DATA>().ToArray());
      foreach (A2LNODE ifData in medias)
      {
        xcpProtocolLayer = ifData.getNode<A2LXCP_PROTOCOL_LAYER>(false);
        if (xcpProtocolLayer == null)
          // not an xcp definition
          continue;
        xcpPAG = ifData.getNode<A2LXCP_PAG>(false);
        xcpDAQ = ifData.getNode<A2LXCP_DAQ>(false);
        xcpPGM = ifData.getNode<A2LXCP_PGM>(false);
        xcpMediaList.AddRange(ifData.getNodeList<A2LXCP_MEDIA>());
        break;
      }
      xcpChecksum = null;
      var modPar = getNode<A2LMODPAR>(false);
      if (null != modPar)
        xcpChecksum = modPar.getNode<A2LXCP_CHECKSUM>(true);
      return xcpMediaList != null && xcpMediaList.Count > 0 && xcpProtocolLayer != null;
    }
    /// <summary>
    /// Gets the ASAP1B CCP IF_DATA section.
    /// 
    /// The returned A2LCCP_TP_BLOB node contains all CCP settings as childs.
    /// </summary>
    /// <param name="ccpIf">The resulting ccp interface (only valid if return value is true)</param>
    /// <returns>false if no ASAP1B CCP A2LCCP_TP_BLOB section is found</returns>
    public bool getCCPSettings(out A2LCCP_TP_BLOB ccpIf)
    {
      ccpIf = null;
      var medias = new List<A2LIF_DATA>(getNodeList<A2LIF_DATA>().ToArray());
      foreach (var ifData in medias)
        if (null != (ccpIf = ifData.getNode<A2LCCP_TP_BLOB>(false)))
          return true;
      return false;
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>
  /// This entity is never parsed as it contains
  /// the A2L supplier specific Transport interfaces (A2LIF_DATA).
  /// </summary>
  public sealed partial class A2LA2ML : A2LUNSUPPORTEDNODE
  {
    #region constructor
    internal A2LA2ML(int startLine) : base(startLine, A2LType.A2ML.ToString()) { }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LMODPAR : A2LNODE, IA2LDescription
  {
    #region constructor
    internal A2LMODPAR(int startLine)
      : base(startLine)
    {
      SystemConstants
        = new Dictionary<string, string>(Settings.Default.StrictParsing
          ? StringComparer.Ordinal
          : StringComparer.OrdinalIgnoreCase
          );
    }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public string Version { get; set; }
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Supplier { get; set; }
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Customer { get; set; }
    [Category(Constants.CATData)]
    public string CustomerNo { get; set; }
    [Category(Constants.CATData)]
    public string User { get; set; }
    [Category(Constants.CATData)]
    public string PhoneNo { get; set; }
    [Category(Constants.CATData)]
    public string ECU { get; set; }
    [Category(Constants.CATData)]
    public string CPU { get; set; }
    [Category(Constants.CATData), DefaultValue(-1)]
    public int NoOfInterfaces { get; set; } = -1;
    [Category(Constants.CATData)]
    public string EPK { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt32), mStrHexDWordMax)]
    public UInt32 EPKAddress { get; set; } = UInt32.MaxValue;
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt32), "0")]
    public UInt32 ECUCalibrationOffset { get; set; }
    [XmlIgnore, Category(Constants.CATData)]
    public Dictionary<string, string> SystemConstants { get; private set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      while (i < parameterList.Count - 1)
      {
        string parStr = parameterList[i++];
        string valueStr = parameterList[i++];
        switch (parStr)
        {
          case A2LC_Version: Version = valueStr; break;
          case A2LC_Supplier: Supplier = valueStr; break;
          case A2LC_Customer: Customer = valueStr; break;
          case A2LC_CustomerNo: CustomerNo = valueStr; break;
          case A2LC_User: User = valueStr; break;
          case A2LC_PhoneNo: PhoneNo = valueStr; break;
          case A2LC_Ecu: ECU = valueStr; break;
          case A2LC_CpuType: CPU = valueStr; break;
          case A2LC_NoOfIFs: NoOfInterfaces = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_Epk: EPK = valueStr; break;
          case A2LC_AddrEpk: EPKAddress = A2LParser.parse2UIntVal(valueStr); break;
          case A2LC_EcuCalOffs: ECUCalibrationOffset = A2LParser.parse2UIntVal(valueStr); break;
          case A2LC_SysConst: SystemConstants[valueStr] = parameterList[i++]; break;
          case "MEMORY_SEGMENT":
            if (!Settings.Default.StrictParsing)
            { // old style MEMORY_SEGMENT
              if (Childs == null)
                Childs = new A2LNodeList();
              ++i;
              var segment = new A2LMEMORY_LAYOUT(parser, StartLine, parameterList, ref i);
              Childs.Add(segment);
              i -= 2;
            }
            break;
          default: --i; break;
        }
      }
      return true;
    }
    #endregion
    #region IA2LDescription Members
    string IA2LDescription.DisplayName { get { return Type.ToString(); } }
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LMEMORY_SEGMENT : A2LNAMEDNODE, IA2LMemory, IA2LDescription
  {
    #region constructor
    internal A2LMEMORY_SEGMENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public MEMORYPRG_TYPE PrgType { get; set; }
    [Category(Constants.CATData)]
    public MEMORY_TYPE MemoryType { get; set; }
    [Category(Constants.CATData)]
    public MEMORY_ATTRIBUTE MemoryAttribute { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Address { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Size { get; set; }
    [Category(Constants.CATData)]
    public int[] Offsets { get; set; } = new int[5];
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      PrgType = parse2Enum<MEMORYPRG_TYPE>(parameterList[i++], parser);
      MemoryType = parse2Enum<MEMORY_TYPE>(parameterList[i++], parser);
      MemoryAttribute = parse2Enum<MEMORY_ATTRIBUTE>(parameterList[i++], parser);
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      Size = A2LParser.parse2UIntVal(parameterList[i++]);
      for (int j = 0; j < Offsets.Length; ++j)
        Offsets[j] = A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    /// <summary>
    /// Gets the referencing A2LCHARACTERISTICs.
    /// </summary>
    /// <returns>List of referenced characateristics.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var characteristic in project.CharDict.Values)
      {
        if (characteristic.Address < Address || characteristic.Address >= Address + Size)
          continue;
        refList.Add(characteristic);
      }
      return refList.ToArray();
    }
    #endregion
    #region IA2LMEMOPRY Members
    string IA2LMemory.toUserDescriptrion()
    {
      return string.Format("Memory segment {0}{1}\n\tType\t{2}\n\tAddress\t0x{3:X}\n\tSize\t0x{4:X}"
        , Name
        , string.IsNullOrEmpty(Description) ? string.Empty : string.Format("\n{0}", Description)
        , PrgType.ToString()
        , Address, Size
        );
    }
    bool IA2LMemory.contains(UInt32 address)
    {
      return address >= Address && address < Address + Size;
    }
    bool IA2LMemory.IsReadOnly
    {
      get
      {
        switch (PrgType)
        {
          case MEMORYPRG_TYPE.DATA:
          case MEMORYPRG_TYPE.VARIABLES:
          case MEMORYPRG_TYPE.OFFLINE_DATA: return false;
          default: return true;
        }
      }
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LMEMORY_LAYOUT : A2LNODE, IA2LMemory
  {
    #region constructor
    internal A2LMEMORY_LAYOUT(int startLine) : base(startLine) { }
    internal A2LMEMORY_LAYOUT(A2LParser parser, int startLine, List<string> parameterList, ref int offset)
      : base(startLine)
    {
      EndLine = startLine;
      Type = A2LType.MEMORY_SEGMENT;
      PrgType = parse2Enum<PRG_TYPE>(parameterList[offset++], parser);
      Address = (uint)A2LParser.parse2IntVal(parameterList[offset++]);
      Size = (uint)A2LParser.parse2IntVal(parameterList[offset++]);
      for (int i = 3; i < 8; ++i)
        Offsets[i - 3] = (int)A2LParser.parse2IntVal(parameterList[offset++]);
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public PRG_TYPE PrgType { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Address { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Size { get; set; }
    [Category(Constants.CATData)]
    public int[] Offsets { get; set; } = new int[5];
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      PrgType = parse2Enum<PRG_TYPE>(parameterList[i++], parser);
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      Size = A2LParser.parse2UIntVal(parameterList[i++]);
      for (int j = 0; j < Offsets.Length; ++j)
        Offsets[j] = A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    /// <summary>
    /// Gets the referencing A2LCHARACTERISTICs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any characateristic.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var characteristic in project.CharDict.Values)
      {
        if (characteristic.Address < Address || characteristic.Address >= Address + Size)
          continue;
        refList.Add(characteristic);
      }
      return refList.ToArray();
    }
    #endregion
    #region IA2LMEMOPRY Members
    string IA2LMemory.toUserDescriptrion()
    {
      return string.Format("Memory layout\n\tType\t{0}\n\tAddress\t0x{1:X8}\n\tSize\t0x{2:X8}"
        , PrgType.ToString()
        , Address, Size
        );
    }
    bool IA2LMemory.contains(UInt32 address)
    {
      return address >= Address && address < Address + Size;
    }
    bool IA2LMemory.IsReadOnly { get { return PrgType != PRG_TYPE.PRG_DATA; } }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LMOD_COMMON : A2LNODE
  {
    #region members
    /// <summary>
    /// ASAP2 standard defined default alignments.
    /// </summary>
    public static int[] DefaultAlignments = new int[] { 1, 2, 4, 4, 4, 8 };
    BYTEORDER_TYPE mByteOrder;
    #endregion
    #region constructor
    internal A2LMOD_COMMON(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Comment { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrder))]
    public BYTEORDER_TYPE BYTE_ORDER
    {
      get { return mByteOrder; }
      set
      {
        if (mByteOrder == value)
          return;
        mByteOrder = value;
        A2LMODULE module = getParent<A2LMODULE>();
        foreach (var convRef in module.getNodeList<A2LCONVERSION_REF>(false))
          convRef.reset();
        foreach (var axisDesc in module.getNodeList<A2LAXIS_DESCR>(false))
          axisDesc.reset();
      }
    }
    [Category(Constants.CATData), DefaultValue(DEPOSIT_TYPE.ABSOLUTE)]
    public DEPOSIT_TYPE DEPOSIT { get; set; } = DEPOSIT_TYPE.ABSOLUTE;
    [Category(Constants.CATData)]
    public int DATA_SIZE { get; set; }
    /// <summary>
    /// The alignments mapped into indizes: BYTE=0, WORD=1, LONG=2, FLOAT32_IEEE=3, FLOAT64_IEEE=4. INT64=5.
    /// </summary>
    [Category(Constants.CATData), Description(mMappedAlignDesc)]
    public int[] Alignments { get; set; } = (int[])DefaultAlignments.Clone();
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Comment = parameterList[i++];
      while (i < parameterList.Count - 1)
      {
        string parStr = parameterList[i++];
        string valueStr = parameterList[i++];
        switch (parStr)
        {
          case A2LC_ByteOrder: mByteOrder = parse2Enum<BYTEORDER_TYPE>(valueStr, parser); break;
          case A2LC_Deposit: DEPOSIT = parse2Enum<DEPOSIT_TYPE>(valueStr, parser); break;
          case A2LC_DataSize: DATA_SIZE = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlByte: Alignments[0] = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlWord: Alignments[1] = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlLong: Alignments[2] = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlFloat32: Alignments[3] = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlFloat64: Alignments[4] = A2LParser.parse2IntVal(valueStr); break;
          case A2LC_AlInt64: Alignments[5] = A2LParser.parse2IntVal(valueStr); break;
          default: --i; break;
        }
      }
      return true;
    }
    /// <summary>
    /// Creates a default MOD_COMMON node.
    /// 
    /// If the A2L file doesn't contain a MOD_COMMON node, this method creates a default one.
    /// </summary>
    /// <param name="byteOrder">The byte order</param>
    /// <returns>A default MOD_COMMON Instance</returns>
    public static A2LMOD_COMMON createDefault(BYTEORDER_TYPE byteOrder)
    {
      var defModCom = new A2LMOD_COMMON(int.MaxValue);
      defModCom.Type = A2LType.MOD_COMMON;
      defModCom.mByteOrder = byteOrder;
      defModCom.Comment = string.Format(Resources.strDefaultInstanceCreated
        , typeof(A2LMOD_COMMON).Name, typeof(A2LParser).Name, A2LParser.Version
        );
      return defModCom;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LFRAME : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    internal A2LFRAME(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public int ScalingUnit { get; set; }
    [Category(Constants.CATData)]
    public long Rate { get; set; }
    [Category(Constants.CATData)]
    public List<string> References { get; private set; }
    /// <summary>
    /// Gets the list of referenced nodes within the ASAP2 hierarchy.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Description("The list of refenced nodes within the ASAP2 hierarchy.")]
    public A2LNODE[] ReferencedNodes
    {
      get
      {
        List<A2LNODE> result = new List<A2LNODE>(References.Count);
        if (References == null)
          return result.ToArray();
        A2LPROJECT project = getParent<A2LPROJECT>();
        A2LMEASUREMENT meas;
        for (int i = References.Count - 1; i >= 0; --i)
        {
          bool found = project.MeasDict.TryGetValue(References[i], out meas);
          if (found)
            result.Add(meas);
          else
          { // Not found
            //if (parser.ParserMessage != null)
            //  parser.ParserMessage(parser, new ParserEventArgs(this, A2LMessageType.Error
            //    , string.Format(Resources.strVerbAlreadyExisting, mParTempList[4 + i])
            //    ));
            References.RemoveAt(i);
          }
        }
        return result.ToArray();
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      ScalingUnit = A2LParser.parse2IntVal(parameterList[i++]);
      Rate = A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "FRAME_MEASUREMENT":
            References = new List<string>();
            for (int j = i; j < parameterList.Count; ++j)
              References.Add(parameterList[i++]);
            break;
        }
      }
      return true;
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LAXIS_DESCR : A2LNODE
  {
    #region local types
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class FIX_AXIS_PAR
    {
      int mOffset;
      int mShift;
      int mNumberapo;
      public FIX_AXIS_PAR(int offset, int shift, int numberapo)
      { mOffset = offset; mShift = shift; mNumberapo = numberapo; }
      public int Offset { get { return mOffset; } }
      public int Shift { get { return mShift; } }
      public int Numberapo { get { return mNumberapo; } }
    }
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class FIX_AXIS_PAR_DIST
    {
      int mOffset;
      int mDistance;
      int mNumberapo;
      public FIX_AXIS_PAR_DIST(int offset, int distance, int numberapo)
      { mOffset = offset; mDistance = distance; mNumberapo = numberapo; }
      public int Offset { get { return mOffset; } }
      public int Distance { get { return mDistance; } }
      public int Numberapo { get { return mNumberapo; } }
    }
    #endregion
    #region members
    string mConversion;
    BYTEORDER_TYPE mByteOrder;
    string mFormat;
    string mInputQuantity;
    string mAxisPtsRef;
    string mCurveAxisRef;
    DEPOSIT_TYPE mDeposit;
    FIX_AXIS_PAR mFixAxisPar;
    FIX_AXIS_PAR_DIST mFixAxisParDist;
    string mPhysUnit;
    A2LCOMPU_METHOD mCompuMethod;
    A2LMEASUREMENT mMeasurement;
    A2LRECORD_LAYOUT_REF mRefAxisPts;
    A2LRECORD_LAYOUT_REF mRefCurveAxis;
    string mUnitCache;
    BYTEORDER_TYPE mByteOrderCache;
    #endregion
    #region constructor
    internal A2LAXIS_DESCR(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCConversionRef))]
    public string Conversion
    {
      get { return mConversion; }
      set
      {
        mCompuMethod = null;
        mUnitCache = null;
        mConversion = value;
      }
    }
    [Category(Constants.CATDataLimit)]
    public double LowerLimit { get; set; }
    [Category(Constants.CATDataLimit)]
    public double UpperLimit { get; set; }
    [Category(Constants.CATData)]
    //[DefaultValue(double.NaN)], disabled in case of .NET XML serialization bug
    public double StepSize { get; set; } = double.NaN;
    [Category(Constants.CATData)]
    //[DefaultValue(double.NaN)], disabled in case of .NET XML serialization bug
    public double MaxGrad { get; set; } = double.NaN;
    /// <summary>
    /// Gets or sets the Byte order (optional).
    /// 
    /// - If the local value is NotSet, the value from A2LMOD_COMMON is taken.
    /// - The setter initialises the byte order cache (e.g. if the MOD_COMMON vbyte order is changed).
    /// - The local member is explicitely set.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrderWithNotSet)), Description("If not explicitly set, the value from A2LMOD_COMMON is taken.")]
    public BYTEORDER_TYPE ByteOrder
    {
      get
      {
        if (mByteOrderCache == BYTEORDER_TYPE.NotSet)
        {
          mByteOrderCache = mByteOrder;
          if (mByteOrderCache == BYTEORDER_TYPE.NotSet)
            mByteOrderCache = getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false).BYTE_ORDER;
        }
        return mByteOrderCache;
      }
      set
      {
        mByteOrder = value;
        mByteOrderCache = BYTEORDER_TYPE.NotSet;
      }
    }
    [Category(Constants.CATData), DefaultValue(MONOTONY_TYPE.NotSet)]
    public MONOTONY_TYPE Monotony { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCFormatRef))]
    public string Format
    {
      get
      {
        if (!string.IsNullOrEmpty(mFormat))
          return mFormat;
        A2LCOMPU_METHOD refCompuMethod = RefCompuMethod;
        return mFormat = refCompuMethod != null
          ? refCompuMethod.Format
          : null;
      }
      set { mFormat = value; }
    }
    [Category(Constants.CATData)]
    public AXIS_TYPE AxisType { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCMeasurementRef))]
    public string InputQuantity { get { return mInputQuantity; } set { mMeasurement = null; mInputQuantity = value; } }
    [Category(Constants.CATData)]
    public int MaxAxisPoints { get; set; }
    [Category(Constants.CATDataLimit)]
    public double LowerLimitEx { get; set; }
    [Category(Constants.CATDataLimit)]
    public double UpperLimitEx { get; set; }
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ReadOnly { get; set; }
    [Category(Constants.CATData)]
    public string AxisPtsRef { get { return mAxisPtsRef; } }
    [Category(Constants.CATData)]
    public string CurveAxisRef { get { return mCurveAxisRef; } }
    [Category(Constants.CATData), ReadOnly(true), DefaultValue(DEPOSIT_TYPE.ABSOLUTE)]
    public DEPOSIT_TYPE Deposit
    {
      get
      {
        switch (mDeposit)
        {
          case DEPOSIT_TYPE.NotSet:
            return getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false).DEPOSIT;
          default:
            return mDeposit;
        }
      }
      set { mDeposit = value; }
    }
    [Category(Constants.CATData)]
    public FIX_AXIS_PAR FixAxisPar { get { return mFixAxisPar; } }
    [Category(Constants.CATData)]
    public FIX_AXIS_PAR_DIST FixAxisParDist { get { return mFixAxisParDist; } }
    [Category(Constants.CATData), TypeConverter(typeof(TCUnitRef))]
    public string Unit
    {
      get
      {
        if (mUnitCache != null)
          return mUnitCache;
        if (!string.IsNullOrEmpty(mPhysUnit))
          mUnitCache = mPhysUnit;
        else
        {
          A2LCOMPU_METHOD refCompuMethod = RefCompuMethod;
          mUnitCache = refCompuMethod != null
            ? refCompuMethod.Unit
            : null;
        }
        return mUnitCache;
      }
      set { mPhysUnit = value; mUnitCache = null; }
    }
    /// <summary>
    /// Gets the direct reference to the input measurement of the axis.
    /// 
    /// May be null if no input measurement is defined.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LMEASUREMENT RefMeasurement
    {
      get
      {
        if (string.IsNullOrEmpty(mInputQuantity) || mInputQuantity == mNoInputQuantity)
          return null;
        if (mMeasurement == null)
          // search only once
          getParent<A2LPROJECT>().MeasDict.TryGetValue(mInputQuantity, out mMeasurement);
        return mMeasurement;
      }
    }
    /// <summary>
    /// Gets the direct reference to the corresponding compu method.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LCOMPU_METHOD RefCompuMethod
    {
      get
      {
        if (null == mCompuMethod)
          // Search only once
          getCompuMethod(getParent<A2LPROJECT>().CompDict, mConversion, out mCompuMethod);
        return mCompuMethod;
      }
    }
    /// <summary>
    /// Gets the direct reference to the AXISPTS instance.
    /// 
    /// May be null if not defined.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LAXIS_PTS RefAxisPtsRef
    {
      get
      {
        if (string.IsNullOrEmpty(mAxisPtsRef))
          return null;
        if (mRefAxisPts == null)
          // search only once
          getParent<A2LPROJECT>().CharDict.TryGetValue(mAxisPtsRef, out mRefAxisPts);
        return (A2LAXIS_PTS)mRefAxisPts;
      }
    }
    /// <summary>
    /// Gets the direct reference to the Curve axis reference instance.
    /// 
    /// May be null if not defined.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LCHARACTERISTIC RefCurveAxisRef
    {
      get
      {
        if (string.IsNullOrEmpty(mCurveAxisRef))
          return null;
        if (mRefCurveAxis == null)
          // search only once
          getParent<A2LPROJECT>().CharDict.TryGetValue(mCurveAxisRef, out mRefCurveAxis);
        return (A2LCHARACTERISTIC)mRefCurveAxis;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      AxisType = parse2Enum<AXIS_TYPE>(parameterList[i++], parser);
      mInputQuantity = parameterList[i++];
      mConversion = parameterList[i++];
      MaxAxisPoints = A2LParser.parse2IntVal(parameterList[i++]);
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimitEx = LowerLimit = Math.Min(lwr, upr);
      UpperLimitEx = UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_ReadOnly: ReadOnly = true; break;
          case A2LC_ByteOrder: mByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_Monotony: Monotony = parse2Enum<MONOTONY_TYPE>(parameterList[i++], parser); break;
          case A2LC_Format: mFormat = parameterList[i++]; break;
          case A2LC_PhysUnit: mPhysUnit = parameterList[i++]; break;
          case A2LC_AxisPtsRef: mAxisPtsRef = parameterList[i++]; break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            break;
          case A2LC_CurveAxisRef: mCurveAxisRef = parameterList[i++]; break;
          case A2LC_StepSize: StepSize = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_MaxGrad: MaxGrad = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_Deposit: mDeposit = parse2Enum<DEPOSIT_TYPE>(parameterList[i++], parser); break;
          case A2LC_FixAxisPar:
            mFixAxisPar = new FIX_AXIS_PAR(A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              );
            break;
          case A2LC_FixAxisParDist:
            mFixAxisParDist = new FIX_AXIS_PAR_DIST(A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              );
            break;
        }
      }
      return true;
    }
    /// <summary>
    /// Resets the byte order cache (used e.g. if the MOD_COMMON byte order is changed).
    /// </summary>
    internal void reset() { mByteOrderCache = BYTEORDER_TYPE.NotSet; }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LUNIT : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    internal A2LUNIT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public string Display { get; set; }
    [Category(Constants.CATData)]
    public UNIT_TYPE UnitType { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCUnitRef))]
    public string RefUnit { get; set; }
    /// <summary>
    /// Gradient ([0]) and Offset ([1]) of the linear relationship between two measurement units: f(x) = gradient * x + offset.
    /// 
    /// Specification of the linear relationship between two measurement units given by describing the conversion from the referenced unit to the derived unit:
    /// <para>derived_unit  =  f(referenced_unit)</para>
    /// <para>The referenced measurement unit had to be specified with parameter REF_UNIT.</para>
    /// </summary>
    [Category(Constants.CATData)]
    public double[] UnitConversion { get; set; }
    /// <summary>
    /// Specification of the seven base dimensions required to define an extended SI unit:
    /// 
    /// - [0]: Length - exponent of the base dimension length with unit metre 
    /// - [1]: Mass - exponent of the base dimension mass with unit kilogram
    /// - [2]: Time - exponent of the base dimension time with unit second
    /// - [3]: ElectricCurrent - exponent of the base dimension electric current with unit ampere
    /// - [4]: Temperature - exponent of the base dimension thermodynamic temperature with unit kelvin
    /// - [5]: AmountOfSubstance - exponent of the base dimension amount of substance with unit mole
    /// - [6]: LuminousIntensity - exponent of the base dimension luminous intensity with unit candela 
    /// </summary>
    [Category(Constants.CATData)]
    public int[] SIExponents { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      Display = parameterList[i++];
      UnitType = parse2Enum<UNIT_TYPE>(parameterList[i++], parser);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_RefUnit: RefUnit = parameterList[i++]; break;
          case A2LC_UnitConv:
            UnitConversion = new double[] { A2LParser.parse2DoubleVal(parameterList[i++])
              , A2LParser.parse2DoubleVal(parameterList[i++])
              };
            break;
          case A2LC_SIExponents:
            SIExponents = new int[] { A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              , A2LParser.parse2IntVal(parameterList[i++])
              };
            break;
        }
      }
      return true;
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LAXIS_PTS : A2LRECORD_LAYOUT_REF
  {
    #region members
    DEPOSIT_TYPE mDeposit;
    A2LMEASUREMENT mMeasurement;
    #endregion
    #region constructor
    internal A2LAXIS_PTS(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdAxis">The axis typedef reference</param>
    internal A2LAXIS_PTS(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_AXIS tdAxis)
      : base(module, instance, tdAxis)
    {
      Monotony = tdAxis.Monotony;
      MaxAxisPoints = tdAxis.MaxAxisPoints;
      Deposit = tdAxis.Deposit;
      InputQuantity = tdAxis.InputQuantity;

      var subNodes = instance.getNodeList<A2LOVERWRITE>(false);
      foreach (var subNode in subNodes)
      {
        if (subNode.AxisNo > 0)
          continue;
        if (!string.IsNullOrEmpty(subNode.InputQuantity))
          InputQuantity = subNode.InputQuantity;
        if (subNode.Monotony != MONOTONY_TYPE.NotSet)
          Monotony = subNode.Monotony;
      }
      addFromInstance(module, A2LType.AXIS_PTS, tdAxis);
      module.getParent<A2LPROJECT>().CharDict[Name] = this;
    }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCMeasurementRef)), DefaultValue(null)]
    public string InputQuantity { get; set; }
    [Category(Constants.CATData)]
    public int MaxAxisPoints { get; set; }
    [Category(Constants.CATData), ReadOnly(true), DefaultValue(DEPOSIT_TYPE.ABSOLUTE)]
    public DEPOSIT_TYPE Deposit
    {
      get
      {
        switch (mDeposit)
        {
          case DEPOSIT_TYPE.NotSet:
            return getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false).DEPOSIT;
          default:
            return mDeposit;
        }
      }
      set { mDeposit = value; }
    }
    [Category(Constants.CATData), DefaultValue(MONOTONY_TYPE.NotSet)]
    public MONOTONY_TYPE Monotony { get; set; }
    /// <summary>
    /// Gets the direct reference to the input measurement of the axis.
    /// 
    /// May be null if no input measurement is defined.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LMEASUREMENT RefMeasurement
    {
      get
      {
        if (string.IsNullOrEmpty(InputQuantity) || InputQuantity == mNoInputQuantity)
          return null;
        if (null == mMeasurement)
          // search only once
          getParent<A2LPROJECT>().MeasDict.TryGetValue(InputQuantity, out mMeasurement);
        return mMeasurement;
      }
    }
    /// <summary>
    /// Gets the referencing A2LCHARACTERISTICs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any characateristic.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var layoutRef in project.CharDict.Values)
      {
        var characteristic = layoutRef as A2LCHARACTERISTIC;
        if (characteristic == null)
          continue;
        switch (characteristic.CharType)
        {
          case CHAR_TYPE.CURVE:
          case CHAR_TYPE.MAP:
          case CHAR_TYPE.CUBOID:
          case CHAR_TYPE.CUBE_4:
          case CHAR_TYPE.CUBE_5:
            foreach (var axisDesc in characteristic.enumChildNodes<A2LAXIS_DESCR>())
            {
              if (axisDesc.RefAxisPtsRef != this)
                continue;
              refList.Add(characteristic);
            }
            break;
        }
      }
      return refList.ToArray();
    }
    #endregion
    #region methods
    public override int getSize()
    {
      return getOffsetToPosition(int.MaxValue, RefRecordLayout, null, MaxAxisPoints);
    }
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      InputQuantity = parameterList[i++];
      mRecordLayout = parameterList[i++];
      MaxDiff = A2LParser.parse2DoubleVal(parameterList[i++]);
      mConversion = parameterList[i++];
      MaxAxisPoints = A2LParser.parse2IntVal(parameterList[i++]);
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimitEx = LowerLimit = Math.Min(lwr, upr);
      UpperLimitEx = UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_ReadOnly: ReadOnly = true; break;
          case A2LC_GuardRails: GuardRails = true; break;
          case A2LC_Format: mFormat = parameterList[i++]; break;
          case A2LC_PhysUnit: mPhysUnit = parameterList[i++]; break;
          case A2LC_Deposit: mDeposit = parse2Enum<DEPOSIT_TYPE>(parameterList[i++], parser); break;
          case A2LC_Monotony: Monotony = parse2Enum<MONOTONY_TYPE>(parameterList[i++], parser); break;
          case A2LC_ByteOrder: mByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_EcuAdrExt: AddressExtension = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_DisplayId: mDisplayName = parameterList[i++]; break;
          case A2LC_StepSize: StepSize = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_MaxRefresh:
            MaxRefreshUnit = (ScalingUnits)A2LParser.parse2IntVal(parameterList[i++]);
            MaxRefreshRate = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            break;
          case A2LC_SymbolLink:
            SymbolLink = parameterList[i++];
            SymbolOffset = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ModelLink: ModelLink = parameterList[i++]; break;
          case A2LC_RefMemSeg: mMemorySegmentRef = parameterList[i++]; break;
          case A2LC_CalAccess: CalibAccess = parse2Enum<CALIBRATION_ACCESS>(parameterList[i++], parser); break;
        }
      }
      return true;
    }
    public override string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      var description = new StringBuilder();
      try
      {
        addMaxText(description, Description);

        if (!string.IsNullOrEmpty(mDisplayName) && mDisplayName != Name)
          description.Insert(0, string.Format("Displayname:\t{0}\n", DisplayName));
        var layout = RefRecordLayout;
        var meas = RefMeasurement;
        //string unit = string.IsNullOrEmpty(Unit) ? string.Empty : Unit;
        description.Append('\n');
#if DEBUG
        var len = getSize();
        description.AppendFormat("\nStart:{0:X} End:{1:X} Len:{2:X}\n", Address, Address + len - 1, len);
#endif
        description.AppendFormat("Address:\t\t0x{0:X}\nElements:\t{1}\n", Address, MaxAxisPoints);
        DATA_TYPE dataTpye = layout.AxisPts[0].DataType;
        string unit = meas != null ? RefMeasurement.Unit : string.Empty;
        description.AppendFormat("Data type:\t{0}\n", dataTpye);
        description.AppendFormat("Increment:\t{0} {1}\n", RefCompuMethod.getMinIncrement(LowerLimit, UpperLimit
          , getDecimalCount(Format)
          , dataTpye)
          , unit
          );
        description.AppendFormat("Limits:\t\t{0}..{1} {2}\n", LowerLimit, UpperLimit, unit);
        if ((LowerLimitEx != LowerLimit || UpperLimitEx != UpperLimit)
          && LowerLimitEx != UpperLimitEx
           )
          description.AppendFormat("Limits(ext):\t{0}..{1} {2}\n", LowerLimitEx, UpperLimitEx, unit);
      }
      catch { }
      return base.getDescriptionAndAnnotation(description.ToString(), addUserObjects, maxLines);
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCHARACTERISTIC : A2LRECORD_LAYOUT_REF
  {
    #region members
    string mComparisonQuantity;
    A2LMEASUREMENT mRefComparisonQuantity;
    #endregion
    #region constructor
    internal A2LCHARACTERISTIC(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdChar">The characteristic typedef reference</param>
    internal A2LCHARACTERISTIC(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_CHARCTERISTIC tdChar)
      : base(module, instance, tdChar)
    {
      CharType = tdChar.CharType;
      Encoding = tdChar.Encoding;
      Bitmask = tdChar.Bitmask;
      Number = tdChar.Number;
      Discrete = tdChar.Discrete;
      if (tdChar.MatrixDim != null)
        MatrixDim = (int[])tdChar.MatrixDim.Clone();

      var subNodes = instance.getNodeList<A2LOVERWRITE>(false);
      var axisDescs = getNodeList<A2LAXIS_DESCR>(false);
      foreach (var subNode in subNodes)
      {
        for (int i = 1; i < axisDescs.Count + 1; ++i)
        {
          if (i != subNode.AxisNo)
            continue;
          if (!string.IsNullOrEmpty(subNode.InputQuantity))
            axisDescs[i - 1].InputQuantity = subNode.InputQuantity;
          if (subNode.Monotony != MONOTONY_TYPE.NotSet)
            axisDescs[i - 1].Monotony = subNode.Monotony;
          break;
        }
      }
      addFromInstance(module, A2LType.CHARACTERISTIC, tdChar);
      module.getParent<A2LPROJECT>().CharDict[Name] = this;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the characteristic type.
    /// </summary>
    [Category(Constants.CATData)]
    public CHAR_TYPE CharType { get; set; }
    /// <summary>
    /// Encoding to use for CHAR_TYPE.ASCII.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(EncodingType.ASCII), Description("Encoding to use for ASCII-typed characteristics.")]
    public EncodingType Encoding { get; set; }
    /// <summary>
    /// Gets or sets the Bitmask.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMax)]
    public UInt64 Bitmask { get; set; } = UInt64.MaxValue;
    /// <summary>
    /// Gets the number of fixed values.
    /// 
    /// For the adjustable object types 'fixed value block' 
    /// (VAL_BLK) and 'string' (ASCII), this keyword specifies 
    /// the number of fixed values and characters respectively.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(0)]
    public int Number { get; set; }
    /// <summary>
    /// Gets or sets the discrete flag.
    /// 
    /// This keyword indicates that the characteristic values 
    /// are discrete values which should not be interpolated – 
    /// e.g. in graphic display windows or further calculations. 
    /// This flag can be used e.g. for integer objects 
    /// describing states. If the keyword is not specified the 
    /// values are interpreted as continuous values which can 
    /// be interpolated.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool Discrete { get; set; }
    /// <summary>
    /// Gets or sets the comparison quantity.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCMeasurementRef)), DefaultValue(null)]
    public string ComparisonQuantity { get { return mComparisonQuantity; } set { mComparisonQuantity = value; mRefComparisonQuantity = null; } }
    /// <summary>
    /// Gets the 3-value (x, y, z) integer matrix dimensions.
    /// 
    /// Shows the size and dimension of a multidimensional 
    /// characteristic (e.g. VAL_BLK). If the MATRIX_DIM 
    /// keyword is used, then the option NUMBER is not needed. 
    /// However, if the keywords NUMBER and MATRIX_DIM are both 
    /// used, the resulting value in NUMBER must be the same as 
    /// xDim * yDim * zDim for MATRIX_DIM. If the keyword is missing 
    /// the array has only one dimension with the size given at NUMBER.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    /// <summary>
    /// Gets the direct reference to the reference comparison quantity of the axis.
    /// 
    /// May be null if not defined.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LMEASUREMENT RefComparisonQuantity
    {
      get
      {
        if (string.IsNullOrEmpty(mComparisonQuantity))
          return null;
        if (mRefComparisonQuantity == null)
          // search only once
          getParent<A2LPROJECT>().MeasDict.TryGetValue(mComparisonQuantity, out mRefComparisonQuantity);
        return mRefComparisonQuantity;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Gets the number of elements depending on the CharType property.
    /// 
    /// - CHAR_TYPE.VALUE: aleays 1
    /// - CHAR_TYPE.ASCII, CHAR_TYPE.VAL_BLK: multiplied with MatrixDim values or Number
    /// - all others: Not supported
    /// </summary>
    /// <returns>The number of elements</returns>
    /// <exception cref="NotSupportedException">Thrown, if accessed by not supported types.</exception>
    public int getNumberOfElements()
    {
      switch (CharType)
      {
        case CHAR_TYPE.VALUE: return 1;
        case CHAR_TYPE.ASCII:
        case CHAR_TYPE.VAL_BLK:
          int count = Number;
          if (MatrixDim != null)
          {
            count = 1;
            foreach (var val in MatrixDim)
              count *= val;
          }
          return count;
        default:
          throw new NotSupportedException(CharType.ToString());
      }
    }
    public override int getSize()
    {
      var axisDesc = getNodeList<A2LAXIS_DESCR>(false);
      var size = getOffsetToPosition(int.MaxValue, RefRecordLayout, axisDesc, 0);
      switch (CharType)
      {
        case CHAR_TYPE.ASCII:
        case CHAR_TYPE.VAL_BLK:
          size *= getNumberOfElements();
          break;
      }
      return size;
    }
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      CharType = parse2Enum<CHAR_TYPE>(parameterList[i++], parser);
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      mRecordLayout = parameterList[i++];
      MaxDiff = A2LParser.parse2DoubleVal(parameterList[i++]);
      mConversion = parameterList[i++];
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimitEx = LowerLimit = Math.Min(lwr, upr);
      UpperLimitEx = UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_ReadOnly: ReadOnly = true; break;
          case A2LC_GuardRails: GuardRails = true; break;
          case A2LC_Discrete: Discrete = true; break;
          case A2LC_Format: mFormat = parameterList[i++]; break;
          case A2LC_PhysUnit: mPhysUnit = parameterList[i++]; break;
          case A2LC_ByteOrder: mByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_StepSize: StepSize = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_ENCODING: Encoding = parse2Enum<EncodingType>(parameterList[i++], parser); break;
          case A2LC_MaxRefresh:
            MaxRefreshUnit = (ScalingUnits)A2LParser.parse2IntVal(parameterList[i++]);
            MaxRefreshRate = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            break;
          case A2LC_SymbolLink:
            SymbolLink = parameterList[i++];
            SymbolOffset = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ModelLink: ModelLink = parameterList[i++]; break;
          case A2LC_EcuAdrExt: AddressExtension = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_Number: Number = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_BitMask: Bitmask = (UInt64)A2LParser.parse2UInt64Val(parameterList[i++]); break;
          case A2LC_CalAccess: CalibAccess = parse2Enum<CALIBRATION_ACCESS>(parameterList[i++], parser); break;
          case A2LC_DisplayId: mDisplayName = parameterList[i++]; break;
          case "COMPARISON_QUANTITY": mComparisonQuantity = parameterList[i++]; break;
          case A2LC_RefMemSeg: mMemorySegmentRef = parameterList[i++]; break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
        }
      }
      return true;
    }
    public override string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      var description = new StringBuilder();
      try
      {
        addMaxText(description, Description);

        if (!string.IsNullOrEmpty(mDisplayName) && mDisplayName != Name)
          description.Insert(0, $"Displayname:\t{DisplayName}\n");
        var layout = RefRecordLayout;
        string unit = string.IsNullOrEmpty(Unit) ? string.Empty : Unit;
        description.Append('\n');
#if DEBUG
        var len = getSize();
        description.Append($"\nStart:{Address:X} End:{Address + len - 1:X} Len:{len:X}\n");
#endif
        switch (CharType)
        {
          case CHAR_TYPE.ASCII:
            if (Encoding != EncodingType.ASCII)
              description.Append($"Encoding:\t\t{Encoding}\n");
            goto case CHAR_TYPE.VAL_BLK;
          case CHAR_TYPE.VALUE:
          case CHAR_TYPE.VAL_BLK:
            var numberElements = getNumberOfElements();
            if (numberElements > 1)
            {
              var matrix = MatrixDim != null ? $"{MatrixDim[0]}x{MatrixDim[1]}x{MatrixDim[2]}" : numberElements.ToString();
              description.Append($"Address:\t\t0x{Address:X}\nElements:\t{matrix}\n");
            }
            else
              description.Append($"Address:\t\t0x{Address:X}\n");
            break;
          default: // axis types
            var axisDescs = getNodeList<A2LAXIS_DESCR>();
            for (int i = 0; i < axisDescs.Count; ++i)
            {
              var desc = axisDescs[i];
              string aUnit = string.IsNullOrEmpty(desc.Unit) ? string.Empty : desc.Unit;
              description.Append($"\n{(i == 0 ? 'X' : i == 1 ? 'Y' : i == 2 ? 'Z' : i == 3 ? '4' : '5')}-Axis:\n");
              if (layout.AxisPts[i] != null)
              {
                description.Append($"Data type:\t{layout.AxisPts[i].DataType}\n");
                switch (desc.RefCompuMethod.ConversionType)
                {
                  case CONVERSION_TYPE.TAB_VERB:
                  case CONVERSION_TYPE.TAB_NOINTP:
                    break;
                  default:
                    description.Append($"Increment:\t{desc.RefCompuMethod.getMinIncrement(desc.LowerLimit, desc.UpperLimit, getDecimalCount(desc.Format), layout.FncValues.DataType)} {aUnit}\n");
                    break;
                }
              }
              description.Append($"Limits:\t\t{desc.LowerLimit}..{desc.UpperLimit} {aUnit}\n");
              if ((desc.LowerLimitEx != desc.LowerLimit || desc.UpperLimitEx != desc.UpperLimit)
                && desc.LowerLimitEx != desc.UpperLimitEx
                 )
                description.Append($"Limits(ext):\t{desc.LowerLimitEx}..{desc.UpperLimitEx} {aUnit}\n");
              if (!string.IsNullOrEmpty(desc.InputQuantity) && desc.InputQuantity != mNoInputQuantity)
                description.Append($"Input quantity:\t{desc.InputQuantity}\n");
              if (desc.Conversion != mNoCompuMethod)
                description.Append($"Conversion:\t{desc.Conversion}\n");
            }
            description.Append("\nFunction values:\n");
            break;
        }
        if (Bitmask != UInt64.MaxValue)
          description.Append($"Bitmask:\t\t0x{Bitmask:X}\n");

        description.Append($"Data type:\t{layout.FncValues.DataType}\n");

        switch (RefCompuMethod.ConversionType)
        {
          case CONVERSION_TYPE.TAB_VERB:
          case CONVERSION_TYPE.TAB_NOINTP:
            break;
          default:
            description.Append($"Increment:\t{RefCompuMethod.getMinIncrement(LowerLimit, UpperLimit, getDecimalCount(Format), layout.FncValues.DataType)} {unit}\n");
            break;
        }
        description.Append($"Limits:\t\t{LowerLimit}..{UpperLimit} {unit}\n");
        if ((LowerLimitEx != LowerLimit || UpperLimitEx != UpperLimit)
          && LowerLimitEx != UpperLimitEx
           )
          description.Append($"Limits(ext):\t{LowerLimitEx}..{UpperLimitEx} {unit}\n");
        if (A2LCOMPU_METHOD.mDefaultCompuMethod != RefCompuMethod && A2LCOMPU_METHOD.mReplaceCompuMethod != RefCompuMethod)
          description.Append($"Conversion:\t{mConversion}\n");
        if (MaxRefreshUnit != ScalingUnits.NotSet)
          description.Append($"Refreshrate:\t{MaxRefreshUnit}{(MaxRefreshRate > 1 ? $"*{MaxRefreshRate}" : string.Empty)}\n");
      }
      catch { }
      return base.getDescriptionAndAnnotation(description.ToString(), addUserObjects, maxLines);
    }
    #endregion
  }
  /// <summary>
  /// Record layout.
  /// 
  /// The 'RECORD_LAYOUT' keyword is used to specify the various data structures of the 
  /// adjustable objects in the memory. The structural buildup of the various adjustable 
  /// object types must be described in such a way that a standard measurement and 
  /// calibration system will be able to process all adjustable object types (reading, writing, 
  /// operating point display etc.). 
  /// In particular, if the ALTERNATE option is used with FNC_VALUES, the position 
  /// parameter determines the order of values and axis points.
  /// 
  /// Note: To describe the record layouts, use is made of a predefined list of parameters 
  /// which may be part of an adjustable object (characteristic) in the emulation 
  /// memory. This list represents the current status of the record layouts. With 
  /// each change or extension of the record layouts contained in this predefined 
  /// list of parameters the ASAM MCD-2MC description file format must be 
  /// modified accordingly. 
  /// 
  /// Note: The keywords describing axis parameters for CUBE_4 and CUBE_5 are 
  /// extended by _X, _Y, _Z, _4, _5. This allows an easier understanding which 
  /// dimension the axis description belongs to. In the textual description the axes 
  /// for CUBE_4 and CUBE_5 are named X, Y, Z, Z4, Z5. This allows textual 
  /// description without a reference to a keyword (Z4-axis describes the axis better 
  /// as 4-axis)  
  /// 
  /// Note: For CUBOID, CUBE_4 and CUBE_5 the RECORD_LAYOUT supports only 
  /// one dedicated way to sort the data in the memory. These objects are always 
  /// stored as array of MAP with incremented or decremented axes. The exchange 
  /// of dimensions (e.g. X, Z, Y, Z5, Z4) in the memory is not supported.
  /// 
  /// see ASAP2 specification, too.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed partial class A2LRECORD_LAYOUT : A2LNAMEDNODE
  {
    #region local classes
    /// <summary>
    /// Base class for record layout items.
    /// </summary>
    public abstract class ItemLayoutDesc
    {
      #region properties
      public string Name { get; set; }
      #endregion
      protected ItemLayoutDesc() { }
      public ItemLayoutDesc(string name, int position, DATA_TYPE dataType, int axisIdx = -1)
      { Name = name; Position = position; DataType = dataType; AxisIdx = axisIdx; }
      /// <summary>
      /// Gets or sets Position of the axis point values in the deposit structure (description of sequence of elements in the data record).
      /// </summary>
      public int Position { get; set; }
      /// <summary>
      /// Gets or sets the data type.
      /// </summary>
      public DATA_TYPE DataType { get; set; }
      /// <summary>
      /// The corresponding axis index (-1 if no axis corresponds).
      /// </summary>
      [XmlIgnore]
      public int AxisIdx { get; private set; }
      internal protected bool equals(ItemLayoutDesc y) { return y != null && Name == y.Name && Position == y.Position && DataType == y.DataType; }
      public abstract override string ToString();
    }
    /// <summary>
    /// Layout description for function values
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class FncValuesLayoutDesc : ItemLayoutDesc
    {
      FncValuesLayoutDesc() { }
      public FncValuesLayoutDesc(int position, DATA_TYPE dataType, INDEX_MODE indexMode, ADDR_TYPE adressType)
        : base("FNC_VALUES", position, dataType)
      { IndexMode = indexMode; AdressType = adressType; }
      /// <summary>
      /// Gets or sets the indexing mode of the values.
      /// </summary>
      public INDEX_MODE IndexMode { get; set; }
      /// <summary>
      /// Gets or sets the addressing type of the table values.
      /// </summary>
      public ADDR_TYPE AdressType { get; set; }
      internal bool equals(FncValuesLayoutDesc y)
      {
        if (!base.equals(y))
          return false;
        return IndexMode == y.IndexMode && AdressType == y.AdressType;
      }
      public override string ToString() { return $"{Name} {Position} {DataType} {IndexMode} {AdressType}"; }
    }
    /// <summary>
    /// Layout description for axis points.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class AxisPtsLayoutDesc : ItemLayoutDesc
    {
      protected AxisPtsLayoutDesc() { }
      /// <summary>Creates a new instance of AxisPtsLayoutDesc</summary>
      /// <param name="position">Position</param>
      /// <param name="dataType">Data type</param>
      /// <param name="indexOrder">Index Order mode</param>
      /// <param name="adressType">Address type</param>
      public AxisPtsLayoutDesc(string name, int position, DATA_TYPE dataType, INDEX_ORDER indexOrder, ADDR_TYPE adressType, int axisIdx)
        : base(name, position, dataType, axisIdx)
      { IndexOrder = indexOrder; AdressType = adressType; }
      /// <summary>
      /// Gets or sets the index order of the values.
      /// </summary>
      public INDEX_ORDER IndexOrder { get; set; }
      /// <summary>
      /// Gets or sets  the addressing type of the table values.
      /// </summary>
      public ADDR_TYPE AdressType { get; set; }
      internal bool equals(AxisPtsLayoutDesc y)
      {
        if (!base.equals(y))
          return false;
        return IndexOrder == y.IndexOrder && AdressType == y.AdressType;
      }
      public override string ToString() { return $"{Name} {Position} {DataType} {IndexOrder} {AdressType}"; }
    }
    /// <summary>
    /// Layout description for axis rescale value pairs.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class AxisRescaleLayoutDesc : AxisPtsLayoutDesc
    {
      AxisRescaleLayoutDesc() { }
      public AxisRescaleLayoutDesc(int position, DATA_TYPE dataType, int maxNoRescalePairs, INDEX_ORDER indexOrder, ADDR_TYPE adressType, int axisIdx)
        : base("AXIS_RESCALE_X", position, dataType, indexOrder, adressType, axisIdx)
      { MaxNoRescalePairs = maxNoRescalePairs; }
      /// <summary>
      /// Gets or sets the maximum number of 
      /// </summary>
      public int MaxNoRescalePairs { get; set; }
      internal bool equals(AxisRescaleLayoutDesc y)
      {
        if (!base.equals(y))
          return false;
        return MaxNoRescalePairs == y.MaxNoRescalePairs;
      }
      public override string ToString() { return $"{Name} {Position} {DataType} {MaxNoRescalePairs} {IndexOrder} {AdressType}"; }
    }
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class NoAxisPtsLayoutDesc : ItemLayoutDesc
    {
      NoAxisPtsLayoutDesc() { }
      public NoAxisPtsLayoutDesc(string name, int position, DATA_TYPE dataType, int axisIdx = -1) : base(name, position, dataType, axisIdx) { }
      public NoAxisPtsLayoutDesc(int position, DATA_SIZE dataSize)
        : base("RESERVED", position, DATA_TYPE.UBYTE)
      {
        switch (dataSize)
        {
          case DATA_SIZE.BYTE: DataType = DATA_TYPE.UBYTE; break;
          case DATA_SIZE.WORD: DataType = DATA_TYPE.UWORD; break;
          case DATA_SIZE.LONG: DataType = DATA_TYPE.ULONG; break;
        }
      }
      /// <summary>
      /// Gets the data size.
      /// </summary>
      public DATA_SIZE DataSize
      {
        get
        {
          switch (DataType)
          {
            case DATA_TYPE.SBYTE:
            case DATA_TYPE.UBYTE: return DATA_SIZE.BYTE;
            case DATA_TYPE.SWORD:
            case DATA_TYPE.UWORD: return DATA_SIZE.WORD;
            case DATA_TYPE.FLOAT32_IEEE:
            case DATA_TYPE.SLONG:
            case DATA_TYPE.ULONG: return DATA_SIZE.LONG;
            default: throw new NotSupportedException(DataType.ToString());
          }
        }
      }
      public override string ToString() { return Name == "RESERVED" ? $"{Name} {Position} {DataSize}" : $"{Name} {Position} {DataType}"; }
    }
    #endregion
    #region members
    const string mMappedIndizesDesc = "Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.";
    int[] mAlignments;
    int[] mAlignmentCache;
    List<ItemLayoutDesc> mLayoutEntries;
    #endregion
    #region constructor
    internal A2LRECORD_LAYOUT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// The alignments mapped into indizes: BYTE=0, WORD=1, LONG=2, FLOAT32_IEEE=3, FLOAT64_IEEE=4, INT64=5.
    /// 
    /// If not explicitely set, the alignments are taken from the A2LMOD_COMMON intance.
    /// </summary>
    [Category(Constants.CATData), Description(mMappedAlignDesc), ReadOnly(true)]
    public int[] Alignments
    {
      get
      {
        if (mAlignmentCache == null)
        { // not cached before
          if (mAlignments != null)
            // take the specific values
            mAlignmentCache = mAlignments;
          else
          { // take the values from MOD_COMMON or default
            A2LMOD_COMMON modCommon = getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false);
            mAlignmentCache = modCommon != null
              ? modCommon.Alignments
              : A2LMOD_COMMON.DefaultAlignments;
          }
        }
        return mAlignmentCache;
      }
      set { mAlignmentCache = null; mAlignments = value; }
    }
    /// <summary>
    /// This keyword describes how the table values 
    /// (function values) of the adjustable object 
    /// are deposited in memory.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public FncValuesLayoutDesc FncValues { get; set; }
    /// <summary>
    /// Description of the X, Y, Z, Z4 or Z5 axis points in the memory.
    /// 
    /// Note: If the Alternate option is used with FNC_VALUES, 
    /// the position parameter determines the order of values and axis points.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public AxisPtsLayoutDesc[] AxisPts { get; set; } = new AxisPtsLayoutDesc[5];
    /// <summary>
    /// Describes in which position the parameter 'number of axis points' is deposited in memory.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] NoAxisPts { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// Indicates that a fixed number of axis points is allocated. 
    /// 
    /// Note: In a RECORD_LAYOUT data record, this keyword may not 
    /// be used simultaneously with the keyword 'NO_AXIS_PTS_X / _Y / _Z / _4 / _5'.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public int[] FixNoAxisPts { get; set; } = new int[] { -1, -1, -1, -1, -1 };
    /// <summary>
    /// Describes at which position the address of the input quantity of the axis is deposited in memory.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] SrcAddress { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// Describes where the rescale mapping for the X axis is deposited in memory.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public AxisRescaleLayoutDesc AxisRescaleX { get; set; }
    /// <summary>
    /// Describes at which position the parameter ‘current number of rescale pairs
    /// for the X axis is deposited.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc NoRescaleX { get; set; }
    /// <summary>
    /// Offset to compute the axis points of fixed Characteristics.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] Offset { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// 'Distance' parameter to compute the axis points of fixed Characteristics.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] DistOp { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// Is used to describe at which position the address of this RIP_X, RIP_Y, RIP_Z, RIP_Z4 or RIP_Z5 
    /// quantity is deposited, which contains the current value of the ECU-internal interpolation. 
    /// (input values).
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] RipAddr { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// Shift operand to compute the axis points of fixed Characteristics.
    /// 
    /// Indizes mapped to: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description(mMappedIndizesDesc)]
    public NoAxisPtsLayoutDesc[] ShiftOp { get; set; } = new NoAxisPtsLayoutDesc[5];
    /// <summary>
    /// This keyword can be used to skip specific elements in the adjustable object 
    /// whose meaning must not be interpreted by the measurement and calibration 
    /// system (e.g. for extensions: new parameters in the adjustable objects).
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public List<NoAxisPtsLayoutDesc> Reserved { get; set; } = new List<NoAxisPtsLayoutDesc>();
    /// <summary>
    /// This keyword is used to describe that an 'identifier' 
    /// is deposited in a specific position in the adjustable object.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public NoAxisPtsLayoutDesc Identification { get; set; }
    /// <summary>
    /// Final result (table value) of the ECU-internal interpolation. (output value).
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public NoAxisPtsLayoutDesc RipAddrW { get; set; }
    /// <summary>
    /// For calibration objects with dynamic number of 
    /// axis points this keyword indicates that the 
    /// calibration object does not compact or expand 
    /// data when removing resp. inserting axis points. All 
    /// record layout elements are stored at the same address 
    /// as for the max. number of axis points specified at 
    /// the calibration object - independent of the actual 
    /// number of axis points.
    /// 
    /// If the parameter STATIC_RECORD_LAYOUT is missing, the 
    /// calibration objects referencing this record layout do 
    /// compact / extend data when removing resp. inserting 
    /// axis points and the addresses of the record layout 
    /// elements depend on the actual number of axis points.
    /// 
    /// This parameter may be used only together with NO_AXIS_PTS_X/_Y/_Z/_4/_5.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool StaticRecordLayout { get; set; }
    /// <summary>
    /// For calibration objects with dynamic number of axis
    /// points this keyword indicates that the start addresses
    /// of axes and function values do not change when
    /// removing or inserting axis points, however the
    /// addresses of the single data cells will change if the
    /// dimension of the map changes
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool StaticAddressOffsets { get; set; }
    /// <summary>
    /// Represents all layout entries sorted in a single list by it's position in ascending order.
    /// </summary>
    [XmlIgnore, Browsable(false)]
    public List<ItemLayoutDesc> LayoutEntries
    {
      get
      {
        if (mLayoutEntries == null)
        {
          mLayoutEntries = new List<ItemLayoutDesc>();
          foreach (var noAxisPt in NoAxisPts)
            if (noAxisPt != null)
              mLayoutEntries.Add(noAxisPt);
          foreach (var axisPt in AxisPts)
            if (axisPt != null)
              mLayoutEntries.Add(axisPt);
          foreach (var srcAddress in SrcAddress)
            if (srcAddress != null)
              mLayoutEntries.Add(srcAddress);
          foreach (var offset in Offset)
            if (offset != null)
              mLayoutEntries.Add(offset);
          foreach (var distOp in DistOp)
            if (distOp != null)
              mLayoutEntries.Add(distOp);
          foreach (var ripAdr in RipAddr)
            if (ripAdr != null)
              mLayoutEntries.Add(ripAdr);
          foreach (var shiftOp in ShiftOp)
            if (shiftOp != null)
              mLayoutEntries.Add(shiftOp);
          foreach (var reserved in Reserved)
              mLayoutEntries.Add(reserved);

          if (AxisRescaleX != null) mLayoutEntries.Add(AxisRescaleX);
          if (NoRescaleX != null) mLayoutEntries.Add(NoRescaleX);
          if (Identification != null) mLayoutEntries.Add(Identification);
          if (RipAddrW != null) mLayoutEntries.Add(RipAddrW);
          if (FncValues != null) mLayoutEntries.Add(FncValues);

          mLayoutEntries.Sort((x, y) => { return x.Position.CompareTo(y.Position); });
        }
        return mLayoutEntries;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Checks record layout position compliance.
    /// </summary>
    /// <param name="positions">Positions already read</param>
    /// <param name="posParameter">current position parameter</param>
    /// <returns>The position</returns>
    int checkPosition(A2LParser parser, List<int> positions, string posParameter)
    {
      var position = A2LParser.parse2IntVal(posParameter);
      if (positions.Count > 0 && positions[positions.Count - 1] >= position)
        parser.onParserMessage(new ParserEventArgs(this, MessageType.Error
          , string.Format(Resources.strInvalidLayoutPos, position, positions[positions.Count - 1]))
          );
      positions.Add(position);
      return position;
    }
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      int position;
      var positions = new List<int>();
      while (i < parameterList.Count)
      {
        string parStr = parameterList[i++];
        switch (parStr.Substring(0, 6))
        {
          case "ALIGNM":  // ALIGNMENT_
            if (mAlignments == null)
              // copy if not set
              mAlignments = (int[])Alignments.Clone();
            switch (parStr)
            {
              case A2LC_AlByte: mAlignments[0] = A2LParser.parse2IntVal(parameterList[i++]); break;
              case A2LC_AlWord: mAlignments[1] = A2LParser.parse2IntVal(parameterList[i++]); break;
              case A2LC_AlLong: mAlignments[2] = A2LParser.parse2IntVal(parameterList[i++]); break;
              case A2LC_AlFloat32: mAlignments[3] = A2LParser.parse2IntVal(parameterList[i++]); break;
              case A2LC_AlFloat64: mAlignments[4] = A2LParser.parse2IntVal(parameterList[i++]); break;
              case A2LC_AlInt64: mAlignments[5] = A2LParser.parse2IntVal(parameterList[i++]); break;
            }
            break;
          case "FNC_VA":  // FNC_VALUES
            position = checkPosition(parser, positions, parameterList[i++]);
            var dataType = parse2Enum<DATA_TYPE>(parameterList[i++], parser);
            var indexMode = parse2Enum<INDEX_MODE>(parameterList[i++], parser);
            if (indexMode == INDEX_MODE.NotSet && !Settings.Default.StrictParsing)
              indexMode = INDEX_MODE.COLUMN_DIR;
            var addrType = parse2Enum<ADDR_TYPE>(parameterList[i++], parser);
            FncValues = new FncValuesLayoutDesc(position, dataType, indexMode, addrType);
            break;
          case "FIX_NO": // FIX_NO_AXIS_PTS_
            FixNoAxisPts[getIndexFromPar(parStr)] = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case "SRC_AD": // SRC_ADDR_
            position = checkPosition(parser, positions, parameterList[i++]);
            SrcAddress[getIndexFromPar(parStr)] = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              );
            break;
          case "AXIS_P":  // AXIS_PTS_
            int axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            AxisPts[axisIdx] = new AxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , parse2Enum<INDEX_ORDER>(parameterList[i++], parser)
              , parse2Enum<ADDR_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "NO_AXI":  // NO_AXIS_PTS_
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            NoAxisPts[axisIdx] = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "AXIS_R":  // AXIS_RESCALE_X
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            AxisRescaleX = new AxisRescaleLayoutDesc(position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , A2LParser.parse2IntVal(parameterList[i++])
              , parse2Enum<INDEX_ORDER>(parameterList[i++], parser)
              , parse2Enum<ADDR_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "NO_RES":  // NO_RESCALE_X
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            NoRescaleX = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "OFFSET":  // OFFSET_
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            Offset[axisIdx] = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "DIST_O":  // DIST_OP_
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            DistOp[axisIdx] = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "RIP_AD":  // RIP_ADDR_
            position = checkPosition(parser, positions, parameterList[i++]);
            if (parStr[parStr.Length - 1] == 'W')
              // RIP_ADDR_W
              RipAddrW = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              );
            else
            {
              axisIdx = getIndexFromPar(parStr);
              RipAddr[axisIdx] = new NoAxisPtsLayoutDesc(parStr, position
                , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
                , axisIdx
                );
            }
            break;
          case "SHIFT_":  // SHIFT_OP_
            axisIdx = getIndexFromPar(parStr);
            position = checkPosition(parser, positions, parameterList[i++]);
            ShiftOp[axisIdx] = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              , axisIdx
              );
            break;
          case "RESERV":  // RESERVED
            position = checkPosition(parser, positions, parameterList[i++]);
            Reserved.Add(new NoAxisPtsLayoutDesc(position, parse2Enum<DATA_SIZE>(parameterList[i++], parser)));
            break;
          case "IDENTI":  // IDENTIFICATION
            position = checkPosition(parser, positions, parameterList[i++]);
            Identification = new NoAxisPtsLayoutDesc(parStr, position
              , parse2Enum<DATA_TYPE>(parameterList[i++], parser)
              );
            break;
          case "STATIC":  // STATIC_RECORD_LAYOUT or STATIC_ADDRESS_OFFSETS
            switch (parStr)
            {
              case A2LC_STATIC_RECORD_LAYOUT: StaticRecordLayout = true; break;
              case A2LC_STATIC_ADDRESS_OFFSETS: StaticAddressOffsets = true; break;
            }
            break;
        }
      }
      return true;
    }
    /// <summary>
    /// Gets the index from the mapping: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    /// <param name="parameter"></param>
    /// <returns>the array index</returns>
    static int getIndexFromPar(string parameter)
    {
      switch (parameter[parameter.Length - 1])
      {
        case 'X': return 0;
        case 'Y': return 1;
        case 'Z': return 2;
        case '4': return 3;
        case '5': return 4;
      }
      throw new ArgumentException($"wrong axis parameter: {parameter}");
    }
    /// <summary>
    /// Gets the alignment value for the specified data type.
    /// </summary>
    /// <param name="dataType">The data type</param>
    /// <returns>The alignment value</returns>
    public int getAlignment(DATA_TYPE dataType)
    {
      int[] alignments = Alignments;
      switch (dataType)
      {
        case DATA_TYPE.SBYTE:
        case DATA_TYPE.UBYTE: return alignments[0];
        case DATA_TYPE.SWORD:
        case DATA_TYPE.UWORD: return alignments[1];
        case DATA_TYPE.SLONG:
        case DATA_TYPE.ULONG: return alignments[2];
        case DATA_TYPE.FLOAT32_IEEE: return alignments[3];
        case DATA_TYPE.FLOAT64_IEEE: return alignments[4];
        case DATA_TYPE.A_INT64:
        case DATA_TYPE.A_UINT64: return alignments[5];
        default:
          throw new NotSupportedException(dataType.ToString());
      }
    }
    /// <summary>
    /// Gets the referencing A2LCHARACTERISTICs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any characateristic.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var characteristic in project.CharDict.Values)
      {
        if (characteristic.RefRecordLayout != this)
          continue;
        refList.Add(characteristic);
      }
      return refList.ToArray();
    }
    public sealed override A2LREFBASE[] getRefFunctions()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LREFBASE>();
      foreach (var refBase in project.FuncDict.Values)
      {
        if (!refBase.ReferencedNodes.Contains(this))
          continue;
        refList.Add(refBase);
      }
      return refList.ToArray();
    }
    #endregion
  }
  /// <summary>
  /// see ASAP2 specification
  /// 
  /// The ARRAY_SIZE parameter is always interpreted as MATRIX_DIM parameter!
  /// </summary>
  public sealed partial class A2LMEASUREMENT : A2LCONVERSION_REF
  {
    #region constructor
    internal A2LMEASUREMENT(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdConv">The conversion ref typedef reference</param>
    internal A2LMEASUREMENT(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_MEASUREMENT tdMeasurement)
      : base(module, instance, tdMeasurement)
    {
      DataType = tdMeasurement.DataType;
      Layout = tdMeasurement.Layout;
      Resolution = tdMeasurement.Resolution;
      Accuracy = tdMeasurement.Accuracy;
      BitMask = tdMeasurement.BitMask;
      ErrorMask = tdMeasurement.ErrorMask;
      Discrete = tdMeasurement.Discrete;
      AddrType = tdMeasurement.AddrType;
      if (tdMeasurement.MatrixDim != null)
        MatrixDim = (int[])tdMeasurement.MatrixDim.Clone();

      addFromInstance(module, A2LType.MEASUREMENT, tdMeasurement);
      module.getParent<A2LPROJECT>().MeasDict[Name] = this;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the data type of the measurement.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public DATA_TYPE DataType { get; set; }
    /// <summary>
    /// Gets or sets the layout.
    /// 
    /// For multi-dimensional measurement arrays this 
    /// keyword can be used to specify the layout of the 
    /// array. If the keyword is missing, multi-dimensional 
    /// measurement arrays are interpreted row by row (ROW_DIR).
    /// </summary>
    [Category(Constants.CATData), DefaultValue(INDEX_MODE.NotSet)]
    public INDEX_MODE Layout { get; set; }
    /// <summary>
    /// Gets or sets the smallest possible change in bits.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(1)]
    public int Resolution { get; set; }
    /// <summary>
    /// Gets or sets the possible variation from exact value in %.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(0.0)]
    public double Accuracy { get; set; }
    /// <summary>
    /// Gets or sets the bitmask.
    /// 
    /// With deviation from the standard value 0xFFFFFFFFFFFFFFFF 
    /// this parameter can be used to mask out bits.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMax)]
    public UInt64 BitMask { get; set; } = UInt64.MaxValue;
    /// <summary>
    /// Gets or sets the errormask.
    /// 
    /// With deviation from the standard value 0x0000000000000000 this parameter can be 
    /// used to mask bits of a MEASUREMENT which indicate that the value is in error.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMin)]
    public UInt64 ErrorMask { get; set; }
    /// <summary>
    /// Gets or sets the discrete flag.
    /// 
    /// This keyword indicates that the measurement 
    /// values are discrete values which should not be 
    /// interpolated – e.g. in graphic display windows or 
    /// further calculations. This flag can be used e.g. for 
    /// integer objects describing states. If the keyword is 
    /// not specified the values are interpreted as 
    /// continuous values which can be interpolated.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool Discrete { get; set; }
    /// <summary>
    /// Gets or sets the READ_WRITE flag.
    /// 
    /// Keyword to mark this measurement object as 'writeable'.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ReadWrite { get; set; }
    /// <summary>
    /// Gets the 3-value (x, y, z) integer matrix dimensions.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    [Category(Constants.CATData), DefaultValue(ADDR_TYPE.DIRECT)]
    public ADDR_TYPE AddrType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      DataType = parse2Enum<DATA_TYPE>(parameterList[i++], parser);
      mConversion = parameterList[i++];
      Resolution = A2LParser.parse2IntVal(parameterList[i++]);
      Accuracy = A2LParser.parse2DoubleVal(parameterList[i++]);
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimit = Math.Min(lwr, upr);
      UpperLimit = Math.Max(lwr, upr);
      int arraySize = 0;
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Format: mFormat = parameterList[i++]; break;
          case A2LC_PhysUnit: mPhysUnit = parameterList[i++]; break;
          case A2LC_ByteOrder: mByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_Layout: Layout = parse2Enum<INDEX_MODE>(parameterList[i++], parser); break;
          case A2LC_MaxRefresh:
            MaxRefreshUnit = (ScalingUnits)A2LParser.parse2IntVal(parameterList[i++]);
            MaxRefreshRate = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_SymbolLink:
            SymbolLink = parameterList[i++];
            SymbolOffset = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ModelLink: ModelLink = parameterList[i++]; break;
          case A2LC_EcuAdr: Address = A2LParser.parse2UIntVal(parameterList[i++]); break;
          case A2LC_EcuAdrExt: AddressExtension = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_BitMask: BitMask = A2LParser.parse2UInt64Val(parameterList[i++]); break;
          case A2LC_ErrorMask: ErrorMask = A2LParser.parse2UInt64Val(parameterList[i++]); break;
          case A2LC_CalAccess: CalibAccess = parse2Enum<CALIBRATION_ACCESS>(parameterList[i++], parser); break;
          case A2LC_Discrete: Discrete = true; break;
          case A2LC_ReadWrite: ReadWrite = true; break;
          case A2LC_DisplayId: mDisplayName = parameterList[i++]; break;
          case A2LC_RefMemSeg: mMemorySegmentRef = parameterList[i++]; break;
          case "ARRAY_SIZE": arraySize = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
          case A2LC_ADDRESS_TYPE: AddrType = parse2Enum<ADDR_TYPE>(parameterList[i++], parser); break;
        }
      }

      if (arraySize > 1 && MatrixDim == null)
        // map array size always to matrix dim
        MatrixDim = new int[] { arraySize, 1, 1 };

      return true;
    }
    /// <summary>
    /// Gets the array size of the measurement, the minimum is 1 (single value).
    /// </summary>
    /// <returns>the array size or 1 if the measurement is a single value</returns>
    public int getArraySize()
    {
      int result = 1;
      if (MatrixDim != null)
        foreach (var val in MatrixDim)
          result *= val;
      return result;
    }
    /// <summary>
    /// Gets the address offset for a specific value if this is a multi value measurement (MATRIX_DIM defined).
    /// 
    /// If MATRIX_DIM is not defined, the return value is 0.
    /// </summary>
    /// <param name="x">Matrix index in X direction</param>
    /// <param name="y">Matrix index in Y direction</param>
    /// <returns>The address offset for the specified matrix indizes</returns>
    public uint getAddressOffset(int x, int y)
    {
      if (MatrixDim == null)
        return 0;
      var cols = MatrixDim[0];
      return (uint)(DataType.getSizeInByte() * (y * cols + x));
    }
    /// <summary>
    /// Gets the referencing A2LCONVERSION_REFs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any node.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var layoutRef in project.CharDict.Values)
      {
        var axisPts = layoutRef as A2LAXIS_PTS;
        if (axisPts != null)
        {
          if (axisPts.RefMeasurement == this)
            refList.Add(axisPts);
        }
        else
        {
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          switch (characteristic.CharType)
          {
            case CHAR_TYPE.CURVE:
            case CHAR_TYPE.MAP:
            case CHAR_TYPE.CUBOID:
            case CHAR_TYPE.CUBE_4:
            case CHAR_TYPE.CUBE_5:
              bool found = false;
              foreach (var axisDesc in characteristic.enumChildNodes<A2LAXIS_DESCR>())
              {
                if (axisDesc.RefMeasurement == this)
                {
                  refList.Add(characteristic);
                  found = true;
                  break;
                }
              }
              if (found)
                break;
              if (characteristic.RefComparisonQuantity == this)
                refList.Add(characteristic);
              break;
          }
        }
      }
      return refList.ToArray();
    }
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="value">The value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <returns>the string representation</returns>
    public string toStringValue(double value
      , ValueObjectFormat format
      )
    {
      return base.toStringValue(value, format, DataType);
    }
    /// <summary>
    /// Gets the bit count of the measurement.
    /// </summary>
    /// <param name="maskOffset">The Offset of the bitmask (in bytes)</param>
    /// <returns>The bit count of the measurement</returns>
    public int getBitCount(out int maskOffset)
    {
      maskOffset = 0;
      if (BitMask == UInt64.MaxValue)
        return DataType.getSizeInBit();
      UInt64 bitmask = BitMask;
      while ((bitmask & 0x01) == 0)
      {
        ++maskOffset;
        bitmask >>= 1;
      }
      maskOffset /= 8;

      var bitCount = 0;
      while ((bitmask & 0x01) == 1)
      {
        ++bitCount;
        bitmask >>= 1;
      }
      return bitCount;
    }
    public override int getMemorySize()
    {
      return DataType.getSizeInByte() * getArraySize();
    }
    #endregion
    #region IA2LDescription Members
    public override string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      var description = new StringBuilder();
      try
      {
        addMaxText(description, Description);
        var unit = string.IsNullOrEmpty(Unit) ? string.Empty : Unit;
        description.AppendFormat("\n\nAddress:\t\t0x{0:X}\n", Address);
        description.AppendFormat("Data type:\t{0}\n", DataType);
        if (BitMask != UInt64.MaxValue)
          description.AppendFormat("Bitmask:\t\t0x{0:X}\n", BitMask);
        if (ErrorMask != 0)
          description.AppendFormat("Errormask:\t{0:X}\n", ErrorMask);
        if (Resolution != 0)
          description.AppendFormat("Resulution:\t{0}\n", Resolution);
        if (Accuracy != 0)
          description.AppendFormat("Accuracy:\t{0}\n", Accuracy);
        description.AppendFormat("Limits:\t\t{0}..{1} {2}\n", LowerLimit, UpperLimit, unit);
        if (A2LCOMPU_METHOD.mDefaultCompuMethod != RefCompuMethod && A2LCOMPU_METHOD.mReplaceCompuMethod != RefCompuMethod)
          description.AppendFormat("Conversion:\t{0}\n", mConversion);
        if (MatrixDim != null)
        {
          description.Append("Dimensions:\t");
          foreach (var val in MatrixDim)
            description.Append($"{val}x");
          description.Replace('x', '\n', description.Length - 1, 1);
        }
      }
      catch { }
      return base.getDescriptionAndAnnotation(description.ToString(), addUserObjects, maxLines);
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LFUNCTION : A2LREFBASE
  {
    #region constructor
    internal A2LFUNCTION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string Version { get; set; }
    /// <summary>
    /// Gets the list of referenced nodes within the ASAP2 hierarchy.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Description("The list of refenced nodes within the ASAP2 hierarchy.")]
    public override List<A2LNAMEDNODE> ReferencedNodes
    {
      get
      {
        if (mReferencedNodes == null)
        {
          mReferencedNodes = base.ReferencedNodes;
          // get the subgroups
          List<A2LSUB_FUNCTION> subFunctions = getNodeList<A2LSUB_FUNCTION>();
          for (int i = 0; i < subFunctions.Count; ++i)
          {
            A2LSUB_FUNCTION subFunction = subFunctions[i];
            foreach (A2LFUNCTION function in subFunction.ReferencedNodes)
            {
              for (int j = 0; j < subFunction.ReferencedNodes.Count; ++j)
              {
                A2LCONVERSION_REF convRef = subFunction.ReferencedNodes[j] as A2LCONVERSION_REF;
                if (convRef == null || mReferencedNodes.Contains(convRef))
                  continue;
                mReferencedNodes.Add(convRef);
              }
            }
          }
        }
        return mReferencedNodes;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "FUNCTION_VERSION": Version = parameterList[i++]; break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LUSER_RIGHTS : A2LNAMEDNODE
  {
    #region constructor
    internal A2LUSER_RIGHTS(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public bool ReadOnly { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "READ_ONLY": ReadOnly = true; break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LGROUP : A2LREFBASE
  {
    #region constructor
    internal A2LGROUP(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), ReadOnly(true)]
    public bool Root { get; set; }
    /// <summary>
    /// Gets the list of referenced nodes within the ASAP2 hierarchy.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Description("The list of refenced nodes within the ASAP2 hierarchy.")]
    public override List<A2LNAMEDNODE> ReferencedNodes
    {
      get
      {
        if (mReferencedNodes == null)
        {
          mReferencedNodes = base.ReferencedNodes;
          // get the subgroups
          List<A2LSUB_GROUP> subGroups = getNodeList<A2LSUB_GROUP>();
          for (int i = 0; i < subGroups.Count; ++i)
          {
            A2LSUB_GROUP subGroup = subGroups[i];
            foreach (A2LGROUP group in subGroup.ReferencedNodes)
              mReferencedNodes.AddRange(group.ReferencedNodes);
          }
        }
        return mReferencedNodes;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      for (; i < parameterList.Count;)
      {
        switch (parameterList[i++])
        {
          case "ROOT": Root = true; break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LDEF_CHARACTERISTIC : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LDEF_CHARACTERISTIC() { }
#endif
    internal A2LDEF_CHARACTERISTIC(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LREF_CHARACTERISTIC : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LREF_CHARACTERISTIC() { }
#endif
    internal A2LREF_CHARACTERISTIC(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LREF_MEASUREMENT : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LREF_MEASUREMENT() { }
#endif
    internal A2LREF_MEASUREMENT(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LREF_GROUP : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LREF_GROUP() { }
#endif
    internal A2LREF_GROUP(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LIN_MEASUREMENT : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LIN_MEASUREMENT() { }
#endif
    internal A2LIN_MEASUREMENT(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LOUT_MEASUREMENT : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LOUT_MEASUREMENT() { }
#endif
    internal A2LOUT_MEASUREMENT(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LLOC_MEASUREMENT : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LLOC_MEASUREMENT() { }
#endif
    internal A2LLOC_MEASUREMENT(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LSUB_FUNCTION : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LSUB_FUNCTION() { }
#endif
    internal A2LSUB_FUNCTION(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LFUNCTION_LIST : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LFUNCTION_LIST() { }
#endif
    internal A2LFUNCTION_LIST(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LSUB_GROUP : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LSUB_GROUP() { }
#endif
    internal A2LSUB_GROUP(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LVIRTUAL : A2LREFERENCE
  {
#if ASAP2_WRITER
    internal A2LVIRTUAL() { }
#endif
    internal A2LVIRTUAL(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  public partial class A2LDEPENDENT_CHARACTERISTIC : A2LREFERENCE
  {
    #region constructor
    internal A2LDEPENDENT_CHARACTERISTIC(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string Formula { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Formula = parameterList[i++];
      References = new List<string>();
      while (i < parameterList.Count)
        References.Add(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed class A2LVIRTUAL_CHARACTERISTIC : A2LDEPENDENT_CHARACTERISTIC
  {
#if ASAP2_WRITER
    internal A2LVIRTUAL_CHARACTERISTIC() { }
#endif
    internal A2LVIRTUAL_CHARACTERISTIC(int startLine) : base(startLine) { }
  }
  /// <summary>see ASAP2 specification</summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed partial class A2LCOMPU_METHOD : A2LNAMEDNODE, IA2LDescription
  {
    #region members
    string mUnit = string.Empty;
    string mRefUnit;
    A2LCOMPU_TAB_BASE mCompuTabBase;
    string mUnitCache;
    A2LFormula mFormCache;
    /// <summary>
    /// Default compu method definition (no conversion from raw to physical values).
    /// </summary>
    internal static A2LCOMPU_METHOD mDefaultCompuMethod = new A2LCOMPU_METHOD(-2);
    /// <summary>
    /// Replacement compu method for undefined/wrongly defined compu mehod references.
    /// </summary>
    internal static A2LCOMPU_METHOD mReplaceCompuMethod = new A2LCOMPU_METHOD(-1);
    #endregion
    #region constructor
    internal A2LCOMPU_METHOD(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; } = "default compuMethod";
    [Category(Constants.CATData), TypeConverter(typeof(TCFormatRef))]
    public string Format { get; set; } = "%4.0";
    [Category(Constants.CATData), TypeConverter(typeof(TCUnitRef))]
    public string Unit
    {
      get
      {
        if (mUnitCache != null)
          return mUnitCache;
        if (!string.IsNullOrEmpty(mRefUnit))
        { // search only once
          A2LUNIT unit;
          if (getParent<A2LPROJECT>().UnitDict.TryGetValue(mRefUnit, out unit))
            mUnitCache = unit.Display;
          else
            mUnitCache = mUnit;
        }
        else
          mUnitCache = mUnit;
        return mUnitCache;
      }
      set
      {
        mUnitCache = null;
        mUnit = value;
      }
    }
    [Category(Constants.CATData), ReadOnly(true)]
    public CONVERSION_TYPE ConversionType { get; set; }
    [Category(Constants.CATData), ReadOnly(true)]
    public RationalCoeffs Coeffs { get; set; } = Helpers.RationalCoeffs.mDefault;
    [Category(Constants.CATData), ReadOnly(true)]
    public string CompuTabRef { get; set; }
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LCOMPU_TAB_BASE RefCompuTab
    {
      get
      {
        if (null == mCompuTabBase)
        { // Search only once
          A2LPROJECT project = getParent<A2LPROJECT>();
          switch (ConversionType)
          {
            case CONVERSION_TYPE.TAB_VERB:
              A2LCOMPU_VTAB vTab;
              if (project.CompVTabDict.TryGetValue(CompuTabRef, out vTab))
                mCompuTabBase = vTab;
              A2LCOMPU_VTAB_RANGE vTabRange;
              if (project.CompVTabRangeDict.TryGetValue(CompuTabRef, out vTabRange))
                mCompuTabBase = vTabRange;
              break;
            case CONVERSION_TYPE.TAB_INTP:
            case CONVERSION_TYPE.TAB_NOINTP:
              A2LCOMPU_TAB tab;
              if (project.CompTabDict.TryGetValue(CompuTabRef, out tab))
                mCompuTabBase = tab;
              break;
          }
        }
        return mCompuTabBase;
      }
    }
    [Category(Constants.CATData)]
    public string StatusStringRef { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      ConversionType = parse2Enum<CONVERSION_TYPE>(parameterList[i++], parser);
      Format = parameterList[i++];
      mUnit = parameterList[i++];
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Coeffs:
            Coeffs = new RationalCoeffs(parameterList, i, 6); i += 6;
            if (Coeffs.Equals(RationalCoeffs.mDefault))
              Coeffs = RationalCoeffs.mDefault;
            break;
          case A2LC_CoeffsLinear: Coeffs = new RationalCoeffs(parameterList, i, 2); i += 2; break;
          case A2LC_CompuTabRef: CompuTabRef = parameterList[i++]; break;
          case A2LC_StatStrRef: StatusStringRef = parameterList[i++]; break;
          case A2LC_RefUnit: mRefUnit = parameterList[i++]; break;
          case "FORMULA":
            if (!Settings.Default.StrictParsing)
            { // old style FORMULA
              if (Childs == null)
                Childs = new A2LNodeList();
              A2LFORMULA formula = new A2LFORMULA(StartLine, parameterList[i++]);
              Childs.Add(formula);
            }
            break;
            //case "FORMULA_INV":
            //  if (!Settings.Default.StrictParsing)
            //  { // old style FORMULA_INV
            //    if (mChilds == null) mChilds = new A2LNodeList();
            //    A2LFORMULA_INV formula = new A2LFORMULA_INV(StartLine, parameterList[i + 1]); ++i;
            //    mChilds.Add(formula);
            //  }
            //  break;
        }
      }
      return true;
    }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>Physical value.</returns>
    public double toPhysical(double rawValue)
    {
      switch (ConversionType)
      {
        case CONVERSION_TYPE.IDENTICAL:
          return rawValue;
        case CONVERSION_TYPE.RAT_FUNC:
        case CONVERSION_TYPE.LINEAR:
          return Coeffs.toPhysical(rawValue);
        case CONVERSION_TYPE.TAB_VERB:
          return rawValue;
        case CONVERSION_TYPE.TAB_INTP:
        case CONVERSION_TYPE.TAB_NOINTP:
          return TabCoeffs.toPhysical(ConversionType, RefCompuTab as A2LCOMPU_TAB, rawValue);
        case CONVERSION_TYPE.FORM:
          if (mFormCache == null)
            getParent<A2LPROJECT>().FormulaDict.TryGetValue(Name, out mFormCache);
          return mFormCache != null ? mFormCache.toPhysical(rawValue) : rawValue;
        default:
          throw new NotSupportedException(ConversionType.ToString());
      }
    }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="decimalCount">The decimal count to round to</param>
    /// <returns>Physical value.</returns>
    public double toPhysical(double rawValue, int decimalCount)
    {
      double physValue = toPhysical(rawValue);
      return Math.Round(physValue, decimalCount);
    }
    /// <summary>
    /// Computes the raw value from a physical value.
    /// </summary>
    /// <param name="dataType">The raw data type of the value</param>
    /// <param name="physicalValue">The raw value</param>
    /// <returns>Raw value.</returns>
    public double toRaw(DATA_TYPE dataType, double physicalValue)
    {
      switch (ConversionType)
      {
        case CONVERSION_TYPE.IDENTICAL:
          return physicalValue;
        case CONVERSION_TYPE.RAT_FUNC:
        case CONVERSION_TYPE.LINEAR:
          return Coeffs.toRaw(dataType, (double)physicalValue);
        case CONVERSION_TYPE.TAB_VERB:
          return physicalValue;
        case CONVERSION_TYPE.TAB_INTP:
        case CONVERSION_TYPE.TAB_NOINTP:
          return TabCoeffs.toRaw(ConversionType, RefCompuTab as A2LCOMPU_TAB, dataType, physicalValue);
        case CONVERSION_TYPE.FORM:
          if (mFormCache == null)
            getParent<A2LPROJECT>().FormulaDict.TryGetValue(Name, out mFormCache);
          return mFormCache != null ? mFormCache.toRaw(dataType, physicalValue) : physicalValue;
        default:
          throw new NotSupportedException(ConversionType.ToString());
      }
    }
    /// <summary>
    /// Gets the minimum increment.
    /// </summary>
    /// <param name="physLower">Physical lower limit</param>
    /// <param name="physUpper">Physical upper limit</param>
    /// <param name="decimalCount">Count of decimal places</param>
    /// <param name="dataType">Data type of the corresponding A2LCONVERSION_REF instance</param>
    /// <returns>the computed increment</returns>
    public double getMinIncrement(double physLower, double physUpper, int decimalCount, DATA_TYPE dataType)
    {
      switch (ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          throw new ArgumentException("Wrong method called for this conversion type", "compuMethod");
      }

      // compute the minimum increment from the corresponding format
      double minIncrement = 1;
      for (int i = decimalCount - 1; i >= 0; --i)
        minIncrement /= 10.0;

      switch (dataType)
      {
        case DATA_TYPE.FLOAT32_IEEE:
        case DATA_TYPE.FLOAT64_IEEE:
          // floats don't have a linear increment by datatype!
          // increment only by the corresponding format
          break;
        default:
          // compute minimum increment by data type and CompuMethod
          minIncrement = Math.Abs(toPhysical(1.0) - toPhysical(0.0));
          break;
      }
      return minIncrement;
    }
    /// <summary>
    /// Gets the referencing A2LCONVERSION_REFs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any node.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var layoutRef in project.CharDict.Values)
      {
        var axisPts = layoutRef as A2LAXIS_PTS;
        if (axisPts != null)
        {
          if (axisPts.RefCompuMethod == this)
            refList.Add(axisPts);
        }
        else
        {
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          switch (characteristic.CharType)
          {
            case CHAR_TYPE.CURVE:
            case CHAR_TYPE.MAP:
            case CHAR_TYPE.CUBOID:
            case CHAR_TYPE.CUBE_4:
            case CHAR_TYPE.CUBE_5:
              bool found = false;
              foreach (var axisDesc in characteristic.enumChildNodes<A2LAXIS_DESCR>())
              {
                if (axisDesc.RefCompuMethod == this)
                {
                  refList.Add(characteristic);
                  found = true;
                  break;
                }
              }
              if (!found)
                goto default;
              break;
            default:
              if (characteristic.RefCompuMethod == this)
                refList.Add(characteristic);
              break;
          }
        }
      }
      foreach (A2LMEASUREMENT measurement in project.MeasDict.Values)
      {
        if (measurement.RefCompuMethod != this)
          continue;
        refList.Add(measurement);
      }
      return refList.ToArray();
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCOMPU_TAB : A2LCOMPU_TAB_BASE
  {
    #region constructor
    internal A2LCOMPU_TAB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [XmlIgnore, Category(Constants.CATData)]
    public SortedList<float, double> Values { get; set; }
    [Category(Constants.CATData)]
    //[DefaultValue(double.NaN)], disabled in case of .NET XML serialization bug
    public double DefaultValueNumeric { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 3;
      int count = A2LParser.parse2IntVal(parameterList[i++]);
      Values = new SortedList<float, double>();

      while (i < parameterList.Count)
      {
        var currentValue = parameterList[i++];
        switch (currentValue)
        {
          case A2LC_DefaultValue: DefaultValue = parameterList[i++]; break;
          case A2LC_DefaultValueNum: DefaultValueNumeric = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          default:
            float key = A2LParser.parse2SingleVal(currentValue);
            double value = A2LParser.parse2DoubleVal(parameterList[i++]);
            if (Values.ContainsKey(key))
            {
              parser.onParserMessage(new ParserEventArgs(this, MessageType.Error, string.Format(Resources.strVerbAlreadyExisting, key)));
              continue;
            }
            Values[key] = value;
            break;
        }
      }

      if (Values.Count != count)
      {
        parser.onParserMessage(new ParserEventArgs(this, MessageType.Warning, string.Format(Resources.strNumberValuePairsInvalid, count, Values.Count)));
      }
      return true;
    }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>The physical value string.</returns>
    public double toPhysical(double rawValue)
    {
      float key = Convert.ToSingle(rawValue);
      double value;
      if (Values.TryGetValue(key, out value))
        return value;
      return DefaultValueNumeric;
    }
    /// <summary>
    /// Computes the raw value from the specified physical (string) value.
    /// </summary>
    /// <param name="strValue">The physical (string) value</param>
    /// <returns>The raw value</returns>
    public double toRaw(string strValue)
    {
      return Values.Keys[Values.IndexOfValue(A2LParser.parse2SingleVal(strValue))];
    }
    /// <summary>
    /// Computes the raw value from the specified physical (string) value.
    /// </summary>
    /// <param name="physicalValue">The physical (string) value</param>
    /// <returns>The raw value</returns>
    public double toRaw(double physicalValue)
    {
      return Values.Keys[Values.IndexOfValue(physicalValue)];
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCOMPU_VTAB : A2LCOMPU_TAB_BASE
  {
    #region constructor
    internal A2LCOMPU_VTAB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [XmlIgnore, Category(Constants.CATData)]
    public SortedList<float, string> Verbs { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 3;
      int count = A2LParser.parse2IntVal(parameterList[i++]);
      Verbs = new SortedList<float, string>();

      while (i < parameterList.Count)
      {
        var currentValue = parameterList[i++];
        switch (currentValue)
        {
          case A2LC_DefaultValue: DefaultValue = parameterList[i++]; break;
          default:
            float key = A2LParser.parse2SingleVal(currentValue);
            string value = parameterList[i++];
            if (Verbs.ContainsKey(key))
            {
              parser.onParserMessage(new ParserEventArgs(this, MessageType.Error, string.Format(Resources.strVerbAlreadyExisting, key)));
              continue;
            }
            Verbs[key] = value;
            break;
        }
      }
      if (Verbs.Count != count)
      {
        parser.onParserMessage(new ParserEventArgs(this, MessageType.Warning, string.Format(Resources.strNumberValuePairsInvalid, count, Verbs.Count)));
      }
      return true;
    }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>The physical value string.</returns>
    public string toPhysical(double rawValue)
    {
      string verb;
      float key = Convert.ToSingle(rawValue);
      if (Verbs.TryGetValue(key, out verb))
        return verb;
      return string.IsNullOrEmpty(DefaultValue)
        ? rawValue.ToString()
        : DefaultValue;
    }
    /// <summary>
    /// Computes the raw value from the specified physical (string) value.
    /// 
    /// Returns
    /// - 0 if the string is null or empty
    /// - 0 if the specified string is not defined
    /// </summary>
    /// <param name="strValue">The physical (string) value</param>
    /// <returns>The raw value</returns>
    public double toRaw(string strValue)
    {
      if (string.IsNullOrEmpty(strValue))
        return 0;
      int index = Verbs.IndexOfValue(strValue);
      if (index < 0)
        return 0;
      return Verbs.Keys[index];
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCOMPU_VTAB_RANGE : A2LCOMPU_TAB_BASE
  {
    #region constructor
    internal A2LCOMPU_VTAB_RANGE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [XmlIgnore, Category(Constants.CATData)]
    public Dictionary<string, sRange> Verbs { get; private set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      int count = A2LParser.parse2IntVal(parameterList[i++]);
      Verbs = new Dictionary<string, sRange>();

      while (i < parameterList.Count)
      {
        var currentValue = parameterList[i++];
        switch (currentValue)
        {
          case A2LC_DefaultValue: DefaultValue = parameterList[i++]; break;
          default:
            double min = A2LParser.parse2DoubleVal(currentValue);
            double max = A2LParser.parse2DoubleVal(parameterList[i++]);
            string key = parameterList[i++];
            if (Verbs.ContainsKey(key))
            {
              parser.onParserMessage(new ParserEventArgs(this, MessageType.Error
                , string.Format(Resources.strVerbAlreadyExisting, key)
                ));
              continue;
            }
            Verbs[key] = new sRange { Min = min, Max = max };
            break;
        }
      }
      if (Verbs.Count != count)
      {
        parser.onParserMessage(new ParserEventArgs(this, MessageType.Warning, string.Format(Resources.strNumberValuePairsInvalid, count, Verbs.Count)));
      }
      return true;
    }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>The physical value string.</returns>
    public string toPhysical(double rawValue)
    {
      var en = Verbs.GetEnumerator();
      while (en.MoveNext())
      {
        sRange range = en.Current.Value;
        if (rawValue >= range.Min && rawValue <= range.Max)
          return en.Current.Key;
      }
      return DefaultValue;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCALIBRATION_METHOD : A2LNODE
  {
    #region constructor
    internal A2LCALIBRATION_METHOD(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string Method { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public int Version { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Method = parameterList[i++];
      Version = A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LCALIBRATION_HANDLE : A2LNODE
  {
    #region constructor
    internal A2LCALIBRATION_HANDLE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public List<uint> Handles { get; private set; }
    [Category(Constants.CATData)]
    public string Text { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        switch (parameterList[i])
        {
          case "CALIBRATION_HANDLE_TEXT": Text = parameterList[++i]; i++; break;
          default:
            if (Handles == null)
              Handles = new List<uint>();
            Handles.Add((uint)A2LParser.parse2IntVal(parameterList[i++]));
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LANNOTATION : A2LNODE
  {
    #region constructor
    internal A2LANNOTATION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Label { get; set; }
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Origin { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      if (startIndex == parameterList.Count)
        // parameters are optional!
        return false;
      int i = startIndex;
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "ANNOTATION_LABEL": Label = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n"); break;
          case "ANNOTATION_ORIGIN": Origin = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n"); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LANNOTATION_TEXT : A2LNODE
  {
    #region constructor
    internal A2LANNOTATION_TEXT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Text { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      if (startIndex == parameterList.Count)
        // parameters are optional!
        return false;

      var sb = new StringBuilder();
      for (int i = startIndex; i < parameterList.Count; ++i)
        sb.Append(parameterList[i].Replace("\\r\\n", "\n").Replace("\\n", "\n"));
      Text = sb.ToString();
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public partial class A2LFORMULA : A2LNODE
  {
    #region constructor
    internal A2LFORMULA(int startLine) : base(startLine) { }
    internal A2LFORMULA(int startLine, string formula)
      : base(startLine)
    { EndLine = startLine; Type = A2LType.FORMULA; Formula = formula; }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string Formula { get; set; }
    [Category(Constants.CATData)]
    public string FormulaInv { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Formula = parameterList[i++];
      if (parameterList.Count > i)
      {
        ++i;
        FormulaInv = parameterList[i++];
      }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// The BIT_OPERATION keyword can be used to perform operation on the masked out value. 
  /// 
  /// First BIT_MASK will be applied on measurement data, then LEFT_SHIFT / RIGHT_SHIFT 
  /// is performed and last the SIGN_EXTEND is carried out.  
  /// 
  /// SIGN_EXTEND means that the sign bit (masked data’s leftmost bit) will be copied to 
  /// all bit positions to the left of the sign  bit. This results in a new datatype with 
  /// the same signed value as the masked data. 
  /// </summary>
  public sealed partial class A2LBIT_OPERATION : A2LNODE
  {
    #region constructor
    internal A2LBIT_OPERATION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public BITOPERATION_TYPE BitOperation { get; set; }
    [Category(Constants.CATData)]
    public int Count { get; set; }
    [Category(Constants.CATData)]
    public bool SignExtend { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "LEFT_SHIFT":
            BitOperation = BITOPERATION_TYPE.LEFT_SHIFT;
            Count = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case "RIGHT_SHIFT":
            BitOperation = BITOPERATION_TYPE.RIGHT_SHIFT;
            Count = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_SignExtend:
            SignExtend = true;
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See ASAP2 Specification > v1.6.0 .
  /// </summary>
  public sealed partial class A2LVARIANT_CODING : A2LNODE
  {
    #region constructor
    internal A2LVARIANT_CODING(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// This parameter defines the separating symbol of variant extension.
    /// </summary>
    public string VAR_SEPARATOR { get; set; }
    /// <summary>
    /// This keyword defines the format of variant extension (index) of adjustable objects name.
    /// The extension is used at MCD to distinguish the different variants of adjustable objects.
    /// </summary>
    public VarNamingType VAR_NAMING { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_VAR_SEPARATOR: VAR_SEPARATOR = parameterList[i++]; break;
          case A2LC_VAR_NAMING: VAR_NAMING = parse2Enum<VarNamingType>(parameterList[i++], parser); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LFIX_AXIS_PAR_LIST : A2LNODE
  {
    #region constructor
    internal A2LFIX_AXIS_PAR_LIST(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public List<double> Values { get; private set; } = new List<double>();
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      for (int i = startIndex; i < parameterList.Count;)
        Values.Add(A2LParser.parse2DoubleVal(parameterList[i++]));
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LVAR_CRITERION : A2LNAMEDNODE, IA2LDescription
  {
    #region members
    string mVarMeasurement;
    string mVarSelectionCharacteristic;
    A2LMEASUREMENT mRefVarMeasurement;
    A2LRECORD_LAYOUT_REF mRefVarSelectionCharacteristic;
    #endregion
    #region constructor
    internal A2LVAR_CRITERION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public List<string> CriterionValues { get; private set; }
    [Category(Constants.CATData)]
    public string VarMeasurement { get { return mVarMeasurement; } }
    [Category(Constants.CATData)]
    public string VarSelectionCharacteristic { get { return mVarSelectionCharacteristic; } }
    /// <summary>
    /// Gets the direct reference to the corresponding compu method.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LMEASUREMENT RefVarMeasurement
    {
      get
      {
        if (null == mRefVarMeasurement && !string.IsNullOrEmpty(mVarMeasurement))
          // Search the method only once
          getParent<A2LPROJECT>().MeasDict.TryGetValue(mVarMeasurement, out mRefVarMeasurement);
        return mRefVarMeasurement;
      }
    }
    /// <summary>
    /// Gets the direct reference to the corresponding Var Selection characteristic.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LCHARACTERISTIC RefVarSelectionCharacteristic
    {
      get
      {
        if (null == mRefVarSelectionCharacteristic && !string.IsNullOrEmpty(mVarSelectionCharacteristic))
          // Search the method only once
          getParent<A2LPROJECT>().CharDict.TryGetValue(mVarSelectionCharacteristic, out mRefVarSelectionCharacteristic);
        return (A2LCHARACTERISTIC)mRefVarSelectionCharacteristic;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      while (i < parameterList.Count)
      {
        switch (parameterList[i])
        {
          case A2LC_VAR_MEASUREMENT: mVarMeasurement = parameterList[i++]; break;
          case A2LC_VAR_SELECTION_CHARACTERISTIC: mVarSelectionCharacteristic = parameterList[i++]; break;
          default:
            if (null == CriterionValues)
              CriterionValues = new List<string>();
            CriterionValues.Add(parameterList[i++]);
            break;
        }
      }
      return true;
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LVAR_CHARACTERISTIC : A2LNAMEDNODE
  {
    #region constructor
    internal A2LVAR_CHARACTERISTIC(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public List<string> CriterionValues { get; private set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      while (i < parameterList.Count)
      {
        if (null == CriterionValues)
          CriterionValues = new List<string>();
        CriterionValues.Add(parameterList[i++]);
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LVAR_ADDRESS : A2LNODE
  {
    #region constructor
    internal A2LVAR_ADDRESS(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public List<uint> Addresses { get; private set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        if (null == Addresses)
          Addresses = new List<uint>();
        Addresses.Add(A2LParser.parse2UIntVal(parameterList[i++]));
      }
      return true;
    }
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public sealed partial class A2LVAR_FORBIDDEN_COMB : A2LNODE
  {
    #region constructor
    internal A2LVAR_FORBIDDEN_COMB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [XmlIgnore, Category(Constants.CATData)]
    public Dictionary<string, string> ForbiddenDict { get; private set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        if (null == ForbiddenDict)
          ForbiddenDict = new Dictionary<string, string>();
        ForbiddenDict[parameterList[i++]] = parameterList[i++];
      }
      return true;
    }
    #endregion
  }
}
