/*!
 * @file    
 * @brief   Implements the memory segment of a Datafile.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace jnsoft.ASAP2
{
    /// <summary>
    /// Implements the representation of a memory segment of a data file.
    /// 
    /// A memory segment may correspond to memory segments defined in an 
    /// A2L source file (from A2LMEMORY_SEGMENT or A2LMEMORY_LAYOUT instances).
    /// </summary>

    [Serializable]
    public sealed class MemorySegment : ICloneable, IDisposable
    {
        #region Private fields
        UInt32 mAddress;
        bool mIsInitialized;
#if WIN32_OPTIMIZED
        IntPtr mData;
#else
    byte[] mData;
#endif
        MEMORYPRG_TYPE mPrgType;
        IDataFile mDataFile;
        int mSize;
        #endregion
        #region constructor
#if WIN32_OPTIMIZED
        /// <summary>
        /// Creates a new instance of MemorySegment with the specified data.
        /// </summary>
        /// <param name="address">The start address of the memory segment</param>
        /// <param name="data">The data to initialize</param>
        /// <param name="isInitialized">Is the specified data initialized?</param>
        internal MemorySegment(uint address, int size, IntPtr data, bool isInitialized)
        {
            mAddress = address;
            mSize = size;
            mData = Marshal.AllocHGlobal(mSize);
            Extensions.copyMemory(mData, data, (uint)size);
            mIsInitialized = isInitialized;
        }
        /// <summary>
        /// Creates a new instance of MemorySegment with the specified data.
        /// </summary>
        /// <param name="address">The start address of the memory segment</param>
        /// <param name="data">The data to initialize</param>
        internal MemorySegment(IDataFile dataFile, uint address, ref List<byte> data)
        {
            mPrgType = MEMORYPRG_TYPE.DATA;
            mDataFile = dataFile;
            mAddress = address;
            mSize = data.Count;
            mData = Marshal.AllocHGlobal(mSize);
            mIsInitialized = true;
            if (mSize > 0)
                Marshal.Copy(data.ToArray(), 0, mData, (int)mSize);
            data.Clear();
        }
        /// <summary>
        /// Creates a new instance of MemorySegment with the specified size.
        /// 
        /// - The complete memory segment data will be initialised by 0xff bytes.
        /// - The initialisation state of a memory segment will be 'not initialized'.
        /// </summary>
        /// <param name="address">The start address of the memory segment</param>
        /// <param name="size">The size to initialize</param>
        /// <param name="prgType">The memory segment type</param>
        public MemorySegment(uint address, uint size, MEMORYPRG_TYPE prgType)
        {
            mAddress = address;
            mPrgType = prgType;
            mSize = (int)size;
            mData = Marshal.AllocHGlobal(mSize);
            setDataBytes(0xff);
        }
#else
    /// <summary>
    /// Creates a new instance of MemorySegment with the specified data.
    /// </summary>
    /// <param name="address">The start address of the memory segment</param>
    /// <param name="size">The data length</param>
    /// <param name="data">The data to initialize</param>
    /// <param name="isInitialized">Is the specified data initialized?</param>
    internal MemorySegment(uint address, int size, byte[] data, bool isInitialized)
    {
      mAddress = address;
      mSize = size;
      mData = data;
      mIsInitialized = isInitialized;
    }
    /// <summary>
    /// Creates a new instance of MemorySegment with the specified data.
    /// </summary>
    /// <param name="address">The start address of the memory segment</param>
    /// <param name="data">The data to initialize</param>
    internal MemorySegment(IDataFile dataFile, uint address, ref List<byte> data)
    {
      mPrgType = MEMORYPRG_TYPE.DATA;
      mIsInitialized = true;
      mDataFile = dataFile;
      mAddress = address;
      mSize = data.Count;
      mData = data.ToArray();
      mIsInitialized = true;
      data.Clear();
    }
    /// <summary>
    /// Creates a new instance of MemorySegment with the specified size.
    /// 
    /// - The complete memory segment data will be initialised by 0xff bytes.
    /// - The initialisation state of a memory segment will be 'not initialized'.
    /// </summary>
    /// <param name="address">The start address of the memory segment</param>
    /// <param name="size">The size to initialize</param>
    /// <param name="prgType">The memory segment type</param>
    public MemorySegment(uint address, uint size, MEMORYPRG_TYPE prgType)
    {
      mAddress = address;
      mPrgType = prgType;
      mSize = (int)size;
      mData = new byte[size];
      setDataBytes(0xff);
    }
#endif
        #endregion
        #region Public properties
        /// <summary>
        /// Gets the segment base address.
        /// </summary>
        public UInt32 Address { get { return mAddress; } }
        /// <summary>
        /// Gets the segment size.
        /// </summary>
        public int Size { get { return mSize; } set { mSize = value; } }
        /// <summary>
        /// Gets a value if the segment is initialized.
        /// </summary>
        public bool IsInitialized { get { return mIsInitialized; } internal set { mIsInitialized = true; } }
#if WIN32_OPTIMIZED
        /// <summary>
        /// Gets the data values for the segment.
        /// </summary>
        public IntPtr Data { get { return mData; } internal set { Data = mData; } }
#else
    /// <summary>
    /// Gets the data values for the segment.
    /// </summary>
    public byte[] Data { get { return mData; } }
#endif
        /// <summary>
        /// Gets the segment type.
        /// </summary>
        public MEMORYPRG_TYPE PrgType { get { return mPrgType; } }
        /// <summary>
        /// Gets or sets the owning data file for the segment (may be null).
        /// </summary>
        public IDataFile DataFile { get { return mDataFile; } set { mDataFile = value; } }
        /// <summary>
        /// Gets a value indicating if the segment is used as a data segment.
        /// </summary>
        public bool IsDataSegment
        {
            get
            {
                switch (mPrgType)
                {
                    case MEMORYPRG_TYPE.UNKNOWN:
                    case MEMORYPRG_TYPE.DATA:
                    case MEMORYPRG_TYPE.SERAM:
                    case MEMORYPRG_TYPE.OFFLINE_DATA:
                    case MEMORYPRG_TYPE.CALIBRATION_VARIABLES:
                    case MEMORYPRG_TYPE.VARIABLES:
                        return true;
                    case MEMORYPRG_TYPE.CODE:
                    case MEMORYPRG_TYPE.EXCLUDE_FROM_FLASH:
                    case MEMORYPRG_TYPE.RESERVED:
                        return false;
                    default: throw new NotSupportedException(mPrgType.ToString());
                }
            }
        }
        #endregion
        #region Public methods
        /// <summary>
        /// Reads data from the memory segment.
        /// </summary>
        /// <param name="address">The (absolute) start address</param>
        /// <param name="len">The data length to read from</param>
        /// <returns>The data bytes read from the memory segment</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if address is not within segment</exception>
        public byte[] getDataBytes(UInt32 address, UInt32 len)
        {
            UInt32 lastAddress = address + len - 1;
            if (address < mAddress || lastAddress >= mAddress + mSize)
                throw new ArgumentOutOfRangeException("address", "address not within memory segment");
            var result = new byte[len];
            int offset = (int)(address - mAddress);
#if WIN32_OPTIMIZED
            Marshal.Copy(IntPtr.Add(mData, offset), result, 0, result.Length);
#else
      Array.Copy(mData, offset, result, 0, len);
#endif
            return result;
        }
        /// <summary>
        /// Fills the memory segment with the specified fillByte.
        /// </summary>
        /// <param name="value">The byte value to fill the memory with</param>
        public void setDataBytes(byte value)
        {
#if WIN32_OPTIMIZED
            Extensions.fillMemory(mData, mSize, value);
#else
      Extensions.fillMemory(ref mData, value);
#endif
        }
        /// <summary>
        /// Copies data to the segment.
        /// 
        /// Only fitting data will be copied, the segment size will not be enhanced.
        /// </summary>
        /// <param name="address">Absolute address</param>
        /// <param name="data">Memory segment data</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if startIndex is &lt; 0</exception>
        /// <exception cref="ArgumentException">Thrown if data is null</exception>
        public void setDataBytes(uint address, byte[] data)
        {
            setDataBytes((int)(address - mAddress), data);
        }
        /// <summary>
        /// Copies data to the segment.
        /// 
        /// Only fitting data will be copied, the segment size will not be enhanced.
        /// </summary>
        /// <param name="offset">Offset to the start of the segment</param>
        /// <param name="data">Memory segment data</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if startIndex is &lt; 0</exception>
        /// <exception cref="ArgumentException">Thrown if data is null</exception>
        public void setDataBytes(int offset, byte[] data)
        {
            if (data == null)
                throw new ArgumentException("may not be null", "data");
            mIsInitialized = true;
            int maxLength = Math.Min(data.Length, (int)(mSize - offset));
#if WIN32_OPTIMIZED
            Marshal.Copy(data, 0, IntPtr.Add(mData, offset), maxLength);
#else
      Array.Copy(data, 0, mData, offset, maxLength);
#endif
            if (mDataFile != null)
                mDataFile.IsDirty = true;
        }
#if WIN32_OPTIMIZED
        /// <summary>
        /// Copies data to the segment.
        /// 
        /// Only fitting data will be copied, the segment size will not be enhanced.
        /// </summary>
        /// <param name="data">Memory segment data</param>
        /// <param name="size">The source data size</param>
        /// <exception cref="ArgumentException">Thrown if data is null</exception>
        public void setDataBytes(IntPtr data, int size)
        {
            if (data == IntPtr.Zero)
                throw new ArgumentException("may not be null", "data");
            mIsInitialized = true;
            int maxLength = Math.Min(size, mSize);
            Extensions.copyMemory(mData, data, (uint)maxLength);
        }
#else
    /// <summary>
    /// Copies data to the segment.
    /// 
    /// Only fitting data will be copied, the segment size will not be enhanced.
    /// </summary>
    /// <param name="data">Memory segment data</param>
    /// <param name="size">The source data size</param>
    /// <exception cref="ArgumentException">Thrown if data is null</exception>
    public void setDataBytes(byte[] data, int size)
    {
      if (data == null)
        throw new ArgumentException("may not be null", "data");
      IsInitialized = true;
      int maxLength = Math.Min(size, mSize);
      Array.Copy(data, mData, maxLength);
    }
#endif
        /// <summary>
        /// Checks the segment to find the specified epk string.
        /// </summary>
        /// <param name="epkAddress">The epk address</param>
        /// <param name="expectedEPK">The epk string to compare to</param>
        /// <returns>true if the specified EPK is found</returns>
        internal bool epkCheck(UInt32 epkAddress, string expectedEPK)
        {
            if (string.IsNullOrEmpty(expectedEPK))
                // nothing expected, that's ok
                return true;
            if (!IsInitialized)
                // segment not initialized
                return false;
            if (epkAddress + expectedEPK.Length >= mAddress + mSize)
                return false;
            try
            {
#if WIN32_OPTIMIZED
                var bytes = new byte[expectedEPK.Length];
                Marshal.Copy(IntPtr.Add(mData, (int)(epkAddress - mAddress)), bytes, 0, bytes.Length);
                return expectedEPK == Encoding.Default.GetString(bytes
                  , 0
                  , expectedEPK.Length
                  );
#else
        return expectedEPK == Encoding.Default.GetString(mData
          , (int)(epkAddress - mAddress)
          , expectedEPK.Length
          );
#endif
            }
            catch (ArgumentOutOfRangeException) { return false; }
        }
        /// <summary>
        /// Checks if data is within the current MemorySegment.
        /// </summary>
        /// <param name="address">The start address</param>
        /// <param name="len">The length</param>
        /// <returns>true if data is within the segment</returns>
        public bool isMemoryIn(UInt32 address, int len)
        {
            var endAddress = mAddress + mSize;
            if (address >= mAddress && address < endAddress)
                return true;
            var lastAddress = address == UInt32.MaxValue ? UInt32.MaxValue : address + len;
            return lastAddress >= mAddress && lastAddress <= endAddress;
        }
        /// <summary>
        /// Gets the used memory in percent of the segment.
        /// </summary>
        /// <param name="ranges">The list of memory ranges</param>
        /// <returns>The used memory in percent of the segment.</returns>
        public double getUsageInPercent(MemoryRangeList ranges)
        {
            if (mSize == 0)
                return 0;
            return (double)getUsedBytes(ranges) * 100.0 / (double)mSize;
        }
        /// <summary>
        /// Gets the count of used bytes in the segment.
        /// 
        /// This algorithm supports:
        /// - Memory shared by or more characterisrtics
        /// - More than one segment used by a single characteristic
        /// </summary>
        /// <param name="ranges">The memory ranges (see A2LMODULE.getMemoryRanges on how to create this list)</param>
        /// <returns>The count of used bytes in the segment.</returns>
        public uint getUsedBytes(MemoryRangeList ranges)
        {
            // Now compute the used bytes from the memory ranges.
            uint usedBytes = 0;
            uint startSeg = mAddress;
            uint endSeg = (uint)(mAddress + mSize);
            for (int j = 0; j < ranges.Count; ++j)
            {
                MemoryRange range = ranges[j];

                if (range.Start > endSeg || range.Next < startSeg)
                    // range is not within this segment
                    continue;

                // memory range occupies at least a part of the memory segment
                if (range.Start < startSeg)
                    // starts into segment
                    usedBytes += range.Next - startSeg;
                else if (range.Next > endSeg)
                    // ends the segment
                    usedBytes += endSeg - range.Start;
                else
                    // fully in segment
                    usedBytes += range.Next - range.Start;
            }
            return usedBytes;
        }
        public override string ToString()
        {
            return string.Format(Resources.strMemorySegmentDesc
              , mPrgType
              , mAddress
              , mSize
              , IsInitialized ? Helpers.Checksum.CRC32(mData, 0, mSize) : 0
              , IsInitialized ? "initialized" : "not initialized"
              , mDataFile != null && mDataFile.IsDirty ? "changed" : "unchanged"
              );
        }
        #endregion
        #region ICloneable Member
        /// <summary>
        /// The clone method does a deep copy of the MemorySegment.
        /// </summary>
        /// <returns>a new instance cloned from the current instance</returns>
        public object Clone()
        {
            MemorySegment clonedMemSeg = new MemorySegment(mAddress, mSize, mData, mIsInitialized);
            clonedMemSeg.mDataFile = mDataFile;
            clonedMemSeg.mPrgType = mPrgType;
            return clonedMemSeg;
        }
        #endregion
        /// <summary>
        /// Frees used resources.
        /// </summary>
        public void Dispose()
        {
#if WIN32_OPTIMIZED
            if (mData != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(mData);
                mData = IntPtr.Zero;
            }
#endif
        }
    }
    /// <summary>
    /// Memory segment list type.
    /// 
    /// Memory segments may be constructed from a set of
    /// A2LMEMORY_SEGMENT or A2LMEMORY_LAYOUT instances 
    /// defined in the A2L model.
    /// <example>
    /// <para>Creating memory segments from a loaded A2L model:</para>
    /// <code>
    /// // Creates all memory segments from the a2l information.
    /// MemorySegmentList createEmptyMemorySegments()
    /// {
    ///   var modPar = mA2lParser.Project.getNode<A2LMODPAR>(true);
    ///   bool filterCodeSegments = !Settings.Default.DataFileLoadAllSegments;
    ///   var memSegList = new MemorySegmentList();
    ///   // add segments
    ///   var segments = modPar.getNodeList<A2LMEMORY_SEGMENT>();
    ///   if (segments.Count > 0)
    ///   {
    ///     for (int i = segments.Count - 1; i >= 0; --i)
    ///     {
    ///       var segment = segments[i];
    ///       if (segment.PrgType == MEMORYPRG_TYPE.CODE && filterCodeSegments)
    ///        // filter code segments
    ///        continue;
    ///       memSegList.Add(new MemorySegment(segment.Address, segment.Size));
    ///     }
    ///   }
    ///   // add layouts
    ///   var layouts = modPar.getNodeList<A2LMEMORY_LAYOUT>();
    ///   if (layouts.Count > 0)
    ///   {
    ///      for (int i = layouts.Count - 1; i >= 0; --i)
    ///      {
    ///        var layout = layouts[i];
    ///        if (layout.PrgType == PRG_TYPE.PRG_CODE && filterCodeSegments)
    ///          // filter code segments
    ///          continue;
    ///        memSegList.Add(new MemorySegment(layout.Address, layout.Size));
    ///     }
    ///   }
    ///   return memSegList;
    /// }
    /// </code>
    /// </example>
    /// </summary>
    /// 

    [Serializable]
    public sealed class MemorySegmentList : List<MemorySegment>, ICloneable
    {
        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MemorySegmentList() { }
        /// <summary>
        /// Creates a new instance with the specified parameters.
        /// 
        /// The specified segments are merged if their MEMORYPRG_TYPE matches and no gap is between merged segments!
        /// </summary>
        /// <param name="list">A list of menmory segments</param>
        public MemorySegmentList(List<MemorySegment> list)
        {
            AddRange(list);
            Sort(sortDataSegsToFront);
        }
        #endregion
        #region methods
        /// <summary>
        /// Find the corresponding memory segment to the address specified.
        /// </summary>
        /// <param name="address">The memory adress to search the memory segment for</param>
        /// <param name="len">The length of the data</param>
        /// <returns>The memory segment found or null if none is available</returns>
        public MemorySegment findMemSeg(UInt32 address, int len)
        {
            for (int i = Count - 1; i >= 0; --i)
            {
                MemorySegment memSeg = this[i];
                if (memSeg.isMemoryIn(address, len))
                    // segment found
                    return memSeg;
            }
            return null;
        }
        /// <summary>
        /// Builds a string representation for the memory segment list.
        /// </summary>
        /// <returns>The instance as a string</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat(Resources.strMemorySegmentCount, Count);
            for (int i = 0; i < Count; ++i)
                sb.AppendFormat("{0}\n", this[i]);
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
        /// <summary>
        /// Sorts data segments to the front of the list.
        /// </summary>
        /// <param name="seg1">First segment to compare</param>
        /// <param name="seg2">Second segment to compare</param>
        /// <returns>Sorting algorithm return value</returns>
        public static int sortDataSegsToFront(MemorySegment seg1, MemorySegment seg2)
        {
            if (seg1 == seg2)
                return 0;
            if (seg1.PrgType != seg2.PrgType)
            {
                if (seg1.PrgType == MEMORYPRG_TYPE.DATA)
                    return -1;
                if (seg2.PrgType == MEMORYPRG_TYPE.DATA)
                    return 1;
            }
            return sortSegmentsByAddress(seg1, seg2);
        }
        /// <summary>
        /// Sorts data segments by address.
        /// </summary>
        /// <param name="seg1">First segment to compare</param>
        /// <param name="seg2">Second segment to compare</param>
        /// <returns>Sorting algorithm return value</returns>
        internal static int sortSegmentsByAddress(MemorySegment x, MemorySegment y)
        {
            return x.Address.CompareTo(y.Address);
        }
        /// <summary>
        /// Indicates if the segments are adjacent.
        /// </summary>
        /// <returns>true if the segments are adjacent</returns>
        public bool areSegmentsAdjacent()
        {
            if (Count == 0)
                return true;
            Sort(sortSegmentsByAddress);
            uint address = (uint)(this[0].Address + this[0].Size);
            for (int i = 1; i < Count; ++i)
            {
                uint newAdress = this[i].Address;
                if (newAdress != address)
                    return false;
                address = (uint)(newAdress + this[i].Size);
            }
            return true;
        }
        #endregion
        #region ICloneable Members
        /// <summary>
        /// The clone method does a deep copy of the memory segment list.
        /// </summary>
        /// <returns>a new instance cloned from the current instance</returns>
        public object Clone()
        {
            List<MemorySegment> newList = new List<MemorySegment>();
            for (int i = 0; i < Count; ++i)
                newList.Add((MemorySegment)this[i].Clone());
            return new MemorySegmentList(newList);
        }
        #endregion
    }
}
