﻿/*!
 * @file    
 * @brief   Defines the Interface for accessing a characteristic value representation.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace jnsoft.ASAP2
{
  /// <summary>
  /// Interface for accessing a characteristic value representation.
  /// 
  /// Provides methods for 
  /// - adjusting values
  /// - presenting values
  /// - copying to the clipboard (standard spreadsheet format)
  /// - pasting from the clipboard (standard spreadsheet format)
  /// </summary>
  public interface ICharValue : IComparable<ICharValue>
  {
    #region properties
    /// <summary>
    /// The corresponding characteristic.
    /// 
    /// Maybe an A2LCHARACTERISTIC or an A2LAXIS_PTS reference.
    /// </summary>
    A2LRECORD_LAYOUT_REF Characteristic { get; set; }
    /// <summary>
    /// Gets the characteristic type of the value (for A2LAXIS_PTS, CHAR_TYPE.VALBLK is delivered).
    /// </summary>
    CHAR_TYPE CharType { get; }
    /// <summary>
    /// Gets or sets the format of the contained values.
    /// </summary>
    ValueObjectFormat ValueFormat { get; set; }
    /// <summary>
    /// The value as an object.
    /// 
    /// The data gets typed according it's implementor.
    /// - double for CHAR_TYPE.VALUE
    /// - string for CHAR_TYPE_ASCII
    /// - double[] for CHAR_TYPE.VAL_BLK
    /// - double[] for CHAR_TYPE.CURVE
    /// - double[,] for CHAR_TYPE.MAP
    /// - double[,,] for CHAR_TYPE.CUBOID
    /// - double[,,,] for CHAR_TYPE.CUBE_4
    /// - double[,,,,] for CHAR_TYPE.CUBE_5
    /// </summary>
    object Value { get; set; }
    /// <summary>
    /// The axis values.
    /// 
    /// The axis values mapped into indizes: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    double[][] AxisValue { get; set; }
    /// <summary>
    /// The axis decimal counts.
    /// 
    /// The axis decimal counts mapped into indizes: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    int[] DecimalCountAxis { get; set; }
    /// <summary>
    /// The axis units.
    /// 
    /// The axis units mapped into indizes: X=0, Y=1, Z=2, _4=3, _5=4.
    /// </summary>
    string[] UnitAxis { get; set; }
    /// <summary>
    /// The values decimal count (function value).
    /// </summary>
    int DecimalCount { get; set; }
    /// <summary>
    /// The values unit.
    /// </summary>
    string Unit { get; set; }
    #endregion
    #region methods
    /// <summary>
    /// Increments a single value.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">Count of increments to do</param>
    /// <returns>true if successful</returns>
    bool increment(IDataFile dataFile, int count);
    /// <summary>
    /// Increments CHAR_TYPE.VAL_BLK, CHAR_TYPE.CURVE or CHAR_TYPE.MAP value by the specified increment count.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">Count of increments to do</param>
    /// <param name="cells">The selection to increment (as DataGridView cells)</param>
    /// <returns>true if successful</returns>
    bool increment(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells);
    /// <summary>
    /// Increments a CHAR_TYPE.CUBOID value by the specified increment count.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">The count of increments to do</param>
    /// <param name="cells">The selection to increment (as DataGridView cells)</param>
    /// <param name="zIndex">The corresponding zIndex to set</param>
    /// <returns>true if successful</returns>
    bool increment(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells, int zIndex);
    /// <summary>
    /// Decrements a single value.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">Count of decrements to do</param>
    /// <returns>true if successful</returns>
    bool decrement(IDataFile dataFile, int count);
    /// <summary>
    /// Decrements CHAR_TYPE.VAL_BLK, CHAR_TYPE.CURVE or CHAR_TYPE.MAP value by the specified decrement count.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">Count of decrements to do</param>
    /// <param name="cells">The selection to decrement (as DataGridView cells)</param>
    /// <returns>true if successful</returns>
    bool decrement(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells);
    /// <summary>
    /// Decrements a CHAR_TYPE.CUBOID value by the specified decrement count.
    /// 
    /// Physical values will be prevented to exceed physical limits
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="count">The count of increments to do</param>
    /// <param name="cells">The selection to decrement (as DataGridView cells)</param>
    /// <param name="zIndex">The corresponding zIndex to set</param>
    /// <returns>true if successful</returns>
    bool decrement(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells, int zIndex);
    /// <summary>
    /// Gets a string representing a single value of the characteristic.
    /// </summary>
    /// <returns>a string representing the single value or string.Empty if the characteristic's type does not support a single value representation (CHAR_TYPE.CURVE, ...)</returns>
    string toSingleValue();
    /// <summary>
    /// Gets a single value string from a multiple value type.
    /// </summary>
    /// <param name="x">The x (column), -1 for the x axis value</param>
    /// <param name="y">The y (row), -1 for the y axis value</param>
    /// <param name="z">The z (cuboid), -1 for the z axis value</param>
    /// <returns>a string representing the single value</returns>
    /// <exception cref="NotSupportedException">Thrown, if the type is not a value block, curve, map or cuboid</exception>
    string toSingleValue(int x, int y, int z);
    /// <summary>
    /// Gets the first function value of the characteristic as double value.
    /// 
    /// For a single value always the one and only.
    /// </summary>
    /// <returns></returns>
    double getFncValue();
    /// <summary>
    /// Gets the function value of the characteristic as double value.
    /// </summary>
    /// <param name="x">The x (column) value</param>
    /// <param name="y">The y (row) value</param>
    /// <param name="z">The z (cuboid) value</param>
    /// <returns></returns>
    double getFncValue(int x, int y, int z);
    /// <summary>
    /// Sets the specified function value of the characteristic as double value.
    /// 
    /// For a single value always the one and only.
    /// </summary>
    /// <param name="value">The new function value</param>
    void setFncValue(double value);
    /// <summary>
    /// Sets the specified function value of the characteristic as double value.
    /// </summary>
    /// <param name="value">The new function value</param>
    /// <param name="x">The x (column) value</param>
    /// <param name="y">The y (row) value</param>
    /// <param name="z">The z (cuboid) value</param>
    void setFncValue(double value, int x, int y, int z);
    /// <summary>
    /// Gets a function values enumerator.
    /// </summary>
    /// <returns>An iterator iterating over all function values</returns>
    IEnumerable<double> GetFncValuesEnumerator();
    /// <summary>
    /// Builds a string in clipboard format.
    /// 
    /// The supported clipboard format is a string with each value separated 
    /// by a tab, and lines separated by CRLF. This format is compatible to 
    /// standard spreadsheet applications (e.g. Excel).
    /// 
    /// Not implemented types are:
    /// - A2LAXIS_PTS (contained in their corresponding characteristics)
    /// - CHAR_TYPE.CUBOID, CHAR_TYPE.CUBE4, CHAR_TYPE.CUBE5
    /// </summary>
    /// <param name="srcCells">A cell selection from a DataGridView (may be null). 
    /// If specified, the bounding rectangle of all cells is 
    /// taken to build the result (This is useful if only a part
    /// of a characteristic map/curve should be written into this format).
    /// 
    /// For single value characteristics, this parameter is ignored.
    /// </param>
    /// <returns>a string instance</returns>
    /// <exception cref="NotImplementedException">Thrown, if type is not supported</exception>
    string toClipboardFormat(DataGridViewSelectedCellCollection srcCells);
    /// <summary>
    /// Tries to paste the clipboard content to the value.
    /// 
    /// The supported clipboard format is a string with each value separated 
    /// by a tab, and lines separated by CRLF. This format is compatible to 
    /// standard spreadsheet applications (e.g. Excel).
    /// 
    /// Not supported types are:
    /// - A2LAXIS_PTS (contained in their corresponding characteristics)
    /// - CHAR_TYPE.CUBOID, CHAR_TYPE.CUBE4, CHAR_TYPE.CUBE5
    /// </summary>
    /// <param name="cbString">The string to paste</param>
    /// <param name="dataFile">The datafile to write to</param>
    /// <param name="targetCells">The selected target cells (as DataGridView cells, 
    /// may be null). If specified, the left top position of the bounding rectangle 
    /// is taken as start position for the paste operation.
    /// </param>
    /// <returns>The result</returns>
    PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells);
    /// <summary>
    /// Tests, if a paste of clipboard content is possible.
    /// </summary>
    /// <param name="cbString">The string to paste</param>
    /// <param name="targetCells">The selected cells (as DataGridView cells, may be null)</param>
    /// <returns>The result</returns>
    PasteResult pasteClipboardStringTest(string cbString, DataGridViewSelectedCellCollection targetCells);
    /// <summary>
    /// Sets the new value to the characteristic.
    /// </summary>
    /// <param name="newValue">A new value to modify with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <param name="modifyFunc">The modifying function</param>
    void modifyValue(double newValue, bool treatValueAsPercent, ModifyFuncByValue modifyFunc);
    /// <summary>
    /// Sets the new value to the characteristic.
    /// </summary>
    /// <param name="newValue">A new value to modify with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <param name="modifyFunc">The modifying function</param>
    /// <param name="targetCells">The selected target cells</param>
    /// <param name="z">The cuboid index</param>
    void modifyValue(double newValue, bool treatValueAsPercent, ModifyFuncByValue modifyFunc, DataGridViewSelectedCellCollection targetCells, int z);
    /// <summary>
    /// Modifies the value with the specified funtion.
    /// </summary>
    /// <param name="modifyFunc"></param>
    void modifyValue(ModifyFunc modifyFunc);
    /// <summary>
    /// Modifies the value with the specified funtion.
    /// </summary>
    /// <param name="modifyFunc"></param>
    /// <param name="targetCells">The selected target cells</param>
    /// <param name="z">The cuboid index</param>
    void modifyValue(ModifyFunc modifyFunc, DataGridViewSelectedCellCollection targetCells, int z);
    #endregion
  }
  /// <summary>
  /// Function delegate for value modifying functions.
  /// </summary>
  /// <param name="originalValue">The original value</param>
  /// <param name="value">The new value to to modify the original value with</param>
  /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
  /// <returns>The resulting value</returns>
  public delegate double ModifyFuncByValue(double originalValue, double value, bool treatValueAsPercent);
  /// <summary>
  /// Function delegate for value modifying functions.
  /// </summary>
  /// <param name="originalValue">The original value</param>
  /// <param name="layoutRef">The corresponding characteristic</param>
  /// <returns>The resulting value</returns>
  public delegate double ModifyFunc(double originalValue, A2LRECORD_LAYOUT_REF layoutRef);
  /// <summary>
  /// Results from pasting values from clipboard.
  /// </summary>
  public enum PasteResult
  {
    /// <summary>
    /// Successful.
    /// </summary>
    Ok,
    /// <summary>
    /// Marks start of the warnings section.
    /// </summary>
    Warn,
    /// <summary>
    /// Values violates destination value limits.
    /// </summary>
    [Description("Values violates destination value limits.")]
    WarnDstLimitViolated,
    /// <summary>
    /// Values does not fit into current object.
    /// </summary>
    [Description("Values does not fit into current object.")]
    WarnDoesNotFit,
    /// <summary>
    /// Marks start of the error section.
    /// 
    /// Any result smaller PasteResult.Err are Warnings
    /// ad my not be pasted into the characteristic value.
    /// </summary>
    Err,
    /// <summary>
    /// Required format is not available.
    /// </summary>
    [Description("Required format is not available.")]
    ErrSrcFormatNotAvailable,
    /// <summary>
    /// Required format is not available.
    /// </summary>
    [Description("Source format is not supported.")]
    ErrNotSupportedByTargetValue,
    /// <summary>
    /// Source value format does not fit.
    /// </summary>
    [Description("Source value format does not fit.")]
    ErrSrcWrongValueFormat,
    /// <summary>
    /// Source value format is a non quadratic array.
    /// </summary>
    [Description("Source is a non quadratic array.")]
    ErrSrcNonQuadratic,
  }
}