﻿/*!
 * @file    
 * @brief   Implements the ASAP2 CCP Interface stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    April 2010
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2.XCP;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

/// <summary>
/// Implements the ASAP2 CCP (IF_DATA) Interface stuff.
/// </summary>
namespace jnsoft.ASAP2.CCP
{
  #region types
  /// <summary>
  /// Page types.
  /// </summary>
  [Flags]
  public enum CCP_MEMORY_PAGE_TYPE
  {
    /// <summary>not set</summary>
    NotSet = 0x00,
    /// <summary>memory page in RAM</summary>
    RAM = 0x01,
    /// <summary>memory page in ROM</summary>
    ROM = 0x02,
    /// <summary>memory page in FLASH</summary>
    FLASH = 0x04,
    /// <summary>memory page in EEPROM</summary>
    EEPROM = 0x08,
    /// <summary>memory page is initialised by ECU start-up</summary>
    RAM_INIT_BY_ECU = 0x10,
    /// <summary>RAM- memory page is initialised by the MCS</summary>
    RAM_INIT_BY_TOOL = 0x20,
    /// <summary>RAM memory page is automatically flashed back</summary>
    AUTO_FLASH_BACK = 0x40,
    /// <summary>feature available to flash back the RAM memory page</summary>
    FLASH_BACK = 0x80,
    /// <summary>memory page is standard (fallback mode)</summary>
    DEFAULT = 0x100,
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public enum CCP_CHECKSUM_CALC_TYPE
  {
    /// <summary>not set</summary>
    NotSet,
    ACTIVE_PAGE,
    BIT_OR_WITH_OPT_PAGE,
  }
  /// <summary>
  /// Modes of cyclic data acquisition.
  /// </summary>
  public enum CCP_DAQ_MODE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>ECU is sending one ODT per cycle</summary>
    ALTERNATING,
    /// <summary>ECU is sending a complete DAQ</summary>
    BURST,
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public enum CCP_ADDRESS_MODE
  {
    /// <summary>not set</summary>
    NotSet,
    DAQ,
    ODT,
  }
  #endregion
  /// <summary>
  /// Base class for all A2L node definitions concerning CCP.
  /// </summary>
  public abstract class A2LCCP_NODE : A2LNODE
  {
    #region constructor
#if ASAP2_WRITER
    protected A2LCCP_NODE() { }
#endif
    internal A2LCCP_NODE(int startLine) : base(startLine) { }
    #endregion
    [ReadOnly(true)]
    public sealed override string Name
    {
      get { return base.Name; }
      set { base.Name = value; }
    }
  }
  /// <summary>
  /// Hold a CCP Source with it's attached CCP Raster.
  /// </summary>
  public struct SourceAndRaster
  {
    /// <summary>DAQ List</summary>
    public A2LCCP_SOURCE Source;
    /// <summary>Event (may be null)</summary>
    public A2LCCP_RASTER Raster;
    /// <summary>
    /// Creates a new instance of DAQAndEvt.
    /// </summary>
    /// <param name="source">A CCP source</param>
    /// <param name="raster">CCP Raster to attach</param>
    public SourceAndRaster(A2LCCP_SOURCE source, A2LCCP_RASTER raster) { Source = source; Raster = raster; }
  }

  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_TP_BLOB : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_TP_BLOB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Version { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 BlobVersion { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIDCmd { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIDResp { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 StationAddress { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrder))]
    public BYTEORDER_TYPE ByteOrder { get; set; }
    [Category(Constants.CATData), DefaultValue(CCP_DAQ_MODE.NotSet)]
    public CCP_DAQ_MODE DAQMode { get; set; }
    [Category(Constants.CATData), DefaultValue(CCP_ADDRESS_MODE.NotSet)]
    public CCP_ADDRESS_MODE Consistency { get; set; }
    [Category(Constants.CATData), DefaultValue(CCP_ADDRESS_MODE.NotSet)]
    public CCP_ADDRESS_MODE AddressExt { get; set; }
    [XmlIgnore, Category(Constants.CATData)]
    public HashSet<byte> OptionalCmds { get; set; } = new HashSet<byte>();
    [Category(Constants.CATData)]
    public UInt32 Baudrate { get; set; }
    /// <summary>Holds CCP Source and corresponding CCP Raster</summary>
    [XmlIgnore, Browsable(false)]
    public SortedDictionary<UInt16, SourceAndRaster> SourceAndRasterMap { get; private set; } = new SortedDictionary<UInt16, SourceAndRaster>();
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      BlobVersion = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      CANIDCmd = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      CANIDResp = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      StationAddress = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      ByteOrder = (BYTEORDER_TYPE)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "DAQ_MODE": DAQMode = parse2Enum<CCP_DAQ_MODE>(parameterList[i++], parser); break;
          case A2LC_CONSISTENCY: Consistency = parse2Enum<CCP_ADDRESS_MODE>(parameterList[i++], parser); break;
          case A2LC_AdrExt: AddressExt = parse2Enum<CCP_ADDRESS_MODE>(parameterList[i++], parser); break;
          case A2LC_OPTIONAL_CMD: OptionalCmds.Add((byte)A2LParser.parse2IntVal(parameterList[i++])); break;
          case A2LC_BAUDRATE: Baudrate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]); break;
        }
      }

      buildSourceAndRasterDict(parser);
      return true;
    }
    /// <summary>
    /// Maps the existing CCP Sources to existing Rasters.
    /// </summary>
    void buildSourceAndRasterDict(A2LParser parser)
    {
      var srcList = Parent.getNodeList<A2LCCP_SOURCE>(false);
      var rasterList = Parent.getNodeList<A2LCCP_RASTER>(false);

      foreach (var src in srcList)
      {
        var qpBlob = src.getNode<A2LCCP_QP_BLOB>(false);
        bool found = false;
        if (null != qpBlob)
        {
          foreach (var raster in rasterList)
          {
            if (raster.EvtChnNo == qpBlob.Raster)
            {
              SourceAndRasterMap[qpBlob.DAQNo] = new SourceAndRaster(src, raster);
              found = true;
              break;
            }

          }
        }
        if (!found)
          SourceAndRasterMap[qpBlob.DAQNo] = new SourceAndRaster(src, null);
      }
    }
    /// <summary>
    /// Gets all pages containing at leat one of the specified page type flags.
    /// </summary>
    /// <param name="pageType">The page type flags to request</param>
    /// <returns>The list of defined pages (may be empty)</returns>
    public List<A2LCCP_DEFINED_PAGES> getPages(CCP_MEMORY_PAGE_TYPE pageType)
    {
      var pages = getNodeList<A2LCCP_DEFINED_PAGES>();
      for (int i = pages.Count - 1; i >= 0; --i)
      {
        var page = pages[i];
        if ((pageType & page.PageType) > 0)
          // page hit
          continue;
        // no hit, delete page
        pages.RemoveAt(i);
      }
      return pages;
    }
    /// <summary>
    /// Finds the defined page the specified address is within.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to find</param>
    /// <returns>The defined page instance or null if not found</returns>
    public A2LCCP_DEFINED_PAGES findPage(byte addressExtension, UInt32 address)
    {
      foreach (var page in enumChildNodes<A2LCCP_DEFINED_PAGES>())
      {
        if (page.AddressExt == addressExtension && page.contains(address))
          // page found
          return page;
      }
      return null;
    }
    /// <summary>
    /// Trie ti find the corresponding CAL page from the specified address offset.
    /// </summary>
    /// <param name="offset">The offset within the CAL page base address</param>
    /// <param name="targetPage">The target Calibration page type</param>
    /// <returns>The calibration page found or null if the page is not found)</returns>
    /// <exception cref="NotSupportedException">Thrown for unsupported page types</exception>
    A2LCCP_DEFINED_PAGES findCorrespondingCALPage(UInt32 offset, ECUPage targetPage)
    {
      foreach (var page in enumChildNodes<A2LCCP_DEFINED_PAGES>())
      {
        switch (targetPage)
        {
          case ECUPage.Flash:
            if ((page.PageType & CCP_MEMORY_PAGE_TYPE.FLASH) > 0 && page.contains(page.Address + offset))
              // corresponding Flash page found
              return page;
            break;
          case ECUPage.RAM:
            if ((page.PageType & CCP_MEMORY_PAGE_TYPE.RAM) > 0 && page.contains(page.Address + offset))
              // corresponding RAM page found
              return page;
            break;
          default: throw new NotSupportedException(page.ToString());
        }
      }
      return null;
    }
    /// <summary>
    /// Gets the calibration page base address for the specified page type.
    /// 
    /// Search Iterates over defined pages
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to compute the calibration page address for</param>
    /// <param name="targetPage">The page type to get the CAL page address for</param>
    /// <param name="offset">The offset of the addres to the target base address</param>
    /// <returns>The calibration page found or null if the page is not found)</returns>
    /// <exception cref="NotSupportedException">Thrown for unsupported page types</exception>
    public A2LCCP_DEFINED_PAGES findCALPage(byte addressExtension, UInt32 address, ECUPage targetPage, out UInt32 offset)
    {
      offset = 0;
      var dstPage = findPage(addressExtension, address);
      if (dstPage == null)
        // address isn't within any calibration page
        return null;

      offset = address - dstPage.Address;
      switch (targetPage)
      {
        case ECUPage.Flash:
          if ((dstPage.PageType & CCP_MEMORY_PAGE_TYPE.FLASH) == 0)
            dstPage = findCorrespondingCALPage(address - dstPage.Address, targetPage);
          break;
        case ECUPage.RAM:
          if ((dstPage.PageType & CCP_MEMORY_PAGE_TYPE.RAM) == 0)
            dstPage = findCorrespondingCALPage(address - dstPage.Address, targetPage);
          break;
        default: throw new NotSupportedException(targetPage.ToString());
      }
      return dstPage;
    }
    /// <summary>
    /// Computes the calibration address for the specified target page type.
    /// </summary>
    /// <param name="addressExtension">The address extension</param>
    /// <param name="address">The address to compute the calibration page address for</param>
    /// <param name="targetPage">The page type to get the calibration page base address for </param>
    /// <returns>The computed address (or uint.MaxValue if the address could not be computed)</returns>
    /// <exception cref="NotSupportedException">Thrown for unsupported page types</exception>
    public UInt32 computeAddress(byte addressExtension, UInt32 address, ECUPage targetPage)
    {
      UInt32 offset;
      var dstPage = findCALPage(addressExtension, address, targetPage, out offset);
      if (dstPage == null)
        // address isn't within any calibration page
        return uint.MaxValue;
      return dstPage.Address + offset;
    }
    /// <summary>
    /// Returns the string representation.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return string.Format("{0}CCP; IDs[{1}]; {2} Bit/s"
        , StartLine == -1 ? "Simulate on " : string.Empty
        , CANProvider.toCANIDPairString(CANIDCmd, CANIDResp)
        , Baudrate
        );
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_DEFINED_PAGES : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_DEFINED_PAGES(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 No { get; set; }
    [Category(Constants.CATData)]
    public string PageName { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Address { get; set; }
    [Category(Constants.CATData)]
    public byte AddressExt { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Length { get; set; }
    [Category(Constants.CATData)]
    public CCP_MEMORY_PAGE_TYPE PageType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      int value = A2LParser.parse2IntVal(parameterList[i++]);
      No = (UInt16)Math.Min(value, UInt16.MaxValue);
      PageName = parameterList[i++];
      value = A2LParser.parse2IntVal(parameterList[i++]);
      AddressExt = (byte)Math.Min(value, byte.MaxValue);
      Address = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      Length = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
        PageType |= parse2Enum<CCP_MEMORY_PAGE_TYPE>(parameterList[i++], parser);
      return true;
    }
    /// <summary>
    /// Returns a value, if the specified address is contained in the defined page.
    /// </summary>
    /// <param name="address">The address to test</param>
    /// <returns>true if the address is contained</returns>
    public bool contains(UInt32 address)
    {
      return address >= Address && address < Address + Length;
    }
    public override string ToString()
    {
      return $"{PageType}: {Address:X8}({Length:X8})";
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_CAN_PARAM : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_CAN_PARAM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Frequency { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public byte BTR0 { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public byte BTR1 { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      int value = A2LParser.parse2IntVal(parameterList[i++]);
      Frequency = (UInt16)Math.Min(value, UInt16.MaxValue);
      value = A2LParser.parse2IntVal(parameterList[i++]);
      BTR0 = (byte)Math.Min(value, byte.MaxValue);
      BTR1 = (byte)Math.Min(value, byte.MaxValue);
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_CHECKSUM_PARAM : A2LCCP_NODE
  {
    #region members
    UInt16 mProcedure;
    #endregion
    #region constructor
    internal A2LCCP_CHECKSUM_PARAM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Procedure { get; set; }
    /// <summary>
    /// Maximum block length used by an ASAP1a-CCP command, for checksum calculation procedure.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Limit { get; set; }
    [Category(Constants.CATData), DefaultValue(CCP_CHECKSUM_CALC_TYPE.NotSet)]
    public CCP_CHECKSUM_CALC_TYPE CalcType { get; set; }
    [Category(Constants.CATData), DefaultValue(ChecksumType.NotSet)]
    public ChecksumType ChecksumType { get { return (ChecksumType)(byte)mProcedure; } }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      mProcedure = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Limit = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      if (parameterList.Count - 1 > i)
      {
        ++i;
        CalcType = parse2Enum<CCP_CHECKSUM_CALC_TYPE>(parameterList[i++], parser);
      }
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_SEED_KEY : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_SEED_KEY(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string CalDll { get; set; }
    [Category(Constants.CATData)]
    public string DaqDll { get; set; }
    [Category(Constants.CATData)]
    public string PgmDll { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      if (startIndex < parameterList.Count)
        CalDll = parameterList[startIndex++];
      if (startIndex < parameterList.Count)
        DaqDll = parameterList[startIndex++];
      if (startIndex < parameterList.Count)
        PgmDll = parameterList[startIndex++];
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_QP_BLOB : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_QP_BLOB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 DAQNo { get; set; }
    [Category(Constants.CATData)]
    public UInt16 Length { get; set; }
    [Category(Constants.CATData)]
    public byte FirstPID { get; set; }
    [Category(Constants.CATData)]
    public byte Raster { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANId { get; set; }
    [Category(Constants.CATData)]
    public XCP_DAQ_LIST_CAN_TYPE DAQListType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "LENGTH": Length = (UInt16)A2LParser.parse2IntVal(parameterList[i++]); break;
          case "RASTER":
            Raster = Math.Min((byte)A2LParser.parse2Int64Val(parameterList[i++]), byte.MaxValue);
            break;
          case A2LC_CAN_ID_VARIABLE:
            DAQListType = XCP_DAQ_LIST_CAN_TYPE.VARIABLE;
            CANId = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_CAN_ID_FIXED:
            DAQListType = XCP_DAQ_LIST_CAN_TYPE.FIXED;
            CANId = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_FIRST_PID:
            FirstPID = Math.Min((byte)A2LParser.parse2Int64Val(parameterList[i++]), byte.MaxValue);
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_SOURCE : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_SOURCE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public Int16 ScalingUnit { get; set; }
    [Category(Constants.CATData)]
    public Int32 Rate { get; set; }
    /// <summary>
    /// Used from the CCPMaster to mark as available for online measuring.
    /// </summary>
    [Category(Constants.CATData), XmlIgnore, DefaultValue(true), Description("Used from the CCPMaster to mark as available for online measuring.")]
    public bool Active { get; set; } = true;
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      NameLong = parameterList[i++];
      ScalingUnit = (Int16)A2LParser.parse2IntVal(parameterList[i++]);
      Rate = A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_RASTER : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_RASTER(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public string NameShort { get; set; }
    [Category(Constants.CATData)]
    public byte EvtChnNo { get; set; }
    [Category(Constants.CATData)]
    public Int16 ScalingUnit { get; set; }
    [Category(Constants.CATData)]
    public Int32 Rate { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      NameLong = parameterList[i++];
      NameShort = parameterList[i++];
      EvtChnNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      ScalingUnit = (Int16)A2LParser.parse2IntVal(parameterList[i++]);
      Rate = A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LCCP_EVENT_GROUP : A2LCCP_NODE
  {
    #region constructor
    internal A2LCCP_EVENT_GROUP(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public string NameShort { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      NameLong = parameterList[i++];
      NameShort = parameterList[i++];
      return true;
    }
    #endregion
  }
}