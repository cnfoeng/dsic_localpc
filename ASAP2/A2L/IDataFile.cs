/*!
 * @file    
 * @brief   Interface for reading and writing data files like S19 and HEX files.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;

/// <summary>
/// Defines and implements stuff for managing data from data files (s19/Hex) files.
/// </summary>
namespace jnsoft.ASAP2
{
    /// <summary>
    /// Interface for reading and writing data files (S19 and HEX) files.
    /// </summary>
    /// <example>
    /// How to use the IDataFile interface:
    /// <code>
    /// class Program
    /// {
    ///   static void Main(string[] args)
    ///   {
    ///     using (var a2lParser = new A2LParser())
    ///     {
    ///       // parse specified A2L file
    ///       if (!a2lParser.parse(args[0]))
    ///         // not an a2l file
    ///         return;
    ///       
    ///       // get project
    ///       var project = a2lParser.Project;
    ///       // get module
    ///       var module = project.getNode<A2LMODULE>(false);
    ///       // create initial memory segments
    ///       var initialSegments = module.createInitialMemorySegments(true);
    ///       if (initialSegments == null)
    ///         return;
    ///       
    ///       // Load the data file
    ///       var dataFile = DataFile.open(args[1], initialSegments);
    ///     
    ///       // Do the EPK check on the file
    ///       var modPar = module.getNode<A2LMODPAR>(false);
    ///       if (!dataFile.checkEPK(modPar.EPKAddress, modPar.EPK))
    ///         // epk check failed
    ///         return;
    ///       
    ///       // Required: pin the datafile to the A2L module instance
    ///       module.Tag = dataFile;
    ///     
    ///       // Print out any single characteristic value
    ///       string templateformatStr = " = {{0:f{0}}}";
    ///       foreach (var characteristic in module.enumChildNodes<A2LCHARACTERISTIC>())
    ///       {
    ///         switch (characteristic.CharType)
    ///         {
    ///           case CHAR_TYPE.VALUE:
    ///             var value = CharacteristicValue.getValue<SingleValue>(characteristic, ValueObjectFormat.Physical);
    ///             var sb = new StringBuilder(characteristic.Name.PadRight(30));
    ///             // build format string according the values decimal count
    ///             string formatStr = string.Format(templateformatStr, value.DecimalCount)+"{1}";
    ///             // append value and unit
    ///             sb.AppendFormat(formatStr
    ///               , (double)value.Value
    ///               , string.IsNullOrEmpty(value.Unit)
    ///                 ? string.Empty
    ///                 : string.Format(" [{0}]", value.Unit)
    ///               );
    ///             // print string
    ///             Console.WriteLine(sb.ToString());
    ///             break;
    ///         }
    ///       }
    ///     }
    ///   }
    /// }
    /// </code>
    /// </example>
    /// 
    public interface IDataFile : ICloneable, IDisposable
  {
    /// <summary>
    /// Gets the fully qualified path to the source filename.
    /// </summary>
    string SourceFilename { get; }
    /// <summary>
    /// Gets a value indicating if the current file is changed in memory.
    /// </summary>
    bool IsDirty { get; set; }
    /// <summary>
    /// Gets the memory segment list.
    /// </summary>
    MemorySegmentList SegmentList { get; }
    /// <summary>
    /// Saves the data file into the filesystem.
    /// 
    /// Converts the memory segments to a certain format (Motorola-S or HEX)
    /// and saves it to a file.
    /// </summary>
    /// <param name="fileName">The fully qualified path of the data file to store to</param>
    /// <param name="onlyDataSegments">true if only data segments should be saved</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown if specified arguments are not supported</exception>
    bool save(string fileName, bool onlyDataSegments);
    /// <summary>
    /// Saves the data file into the filesystem.
    /// 
    /// Converts the memory segments to a certain format (Motorola-S or HEX)
    /// and saves it to a file.
    /// </summary>
    /// <param name="fileName">The fully qualified path of the data file to store to</param>
    /// <param name="onlyDataSegments">true if only data segments should be saved</param>
    /// <param name="dataBytesPerLine">Count of written data bytes per line (maybe 8, 16, 32,...)</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown if specified arguments are not supported</exception>
    bool save(string fileName, bool onlyDataSegments, byte dataBytesPerLine);
    /// <summary>
    /// Finds the memory segment the specified address is within.
    /// </summary>
    /// <param name="address">The address to find</param>
    /// <returns>The segment instance or null if not found</returns>
    MemorySegment findMemSeg(UInt32 address);
		/// <summary>
		/// Checks all segments to find the specified EPK string.
		/// </summary>
		/// <param name="epkAddress">The EPK address</param>
		/// <param name="expectedEPK">The EPK string to compare to</param>
		/// <returns>true if the specified EPK is found</returns>
		bool checkEPK(UInt32 epkAddress, string expectedEPK);
    /// <summary>
    /// Set the EPK to the specified address.
    /// </summary>
    /// <param name="epkAddress">The epk address</param>
    /// <param name="epkToSet">The EPK string to set</param>
    /// <returns>true if the specified EPK is set</returns>
    bool setEPK(UInt32 epkAddress, string epkToSet);
    /// <summary>
    /// Set segments data from the specified segments.
    /// 
    /// - A segment exists if only if the starting addresses and the length is the same
    /// - If a segment is not existing in the target, the data will not be added
    /// - If a segment exists, the specified data will override the data in the target.
    /// </summary>
    /// <param name="segments">The segments to set data from</param>
    void setSegmentsData(MemorySegmentList segments);
    /// <summary>
    /// Imports the specified values into the datafile.
    /// 
    /// May be used e.g from a DCMFile import.
    /// </summary>
    /// <param name="values">The values to import</param>
    void import(ICharValue[] values);
    /// <summary>
    /// Reloads a datafile (if it is based on a valid file in filesystem). 
    /// </summary>
    /// <returns>true if successful</returns>
    bool reLoad();
    /// <summary>
    /// Gets the dictionary of changed characteristic values within the file.
    /// 
    /// The dictionary contains A2L characteristic label names, 
    /// which are changed by a jnsoft.ASAP2.Values.CharacteristicValue.SetValue call.
    /// 
    /// Key is the corresponding A2L instance, the value is not used.
    /// </summary>
    HashSet<A2LRECORD_LAYOUT_REF> ChangedValues { get; }
  }
}
