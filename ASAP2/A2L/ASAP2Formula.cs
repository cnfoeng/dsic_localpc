/*!
 * @file    
 * @brief   Implements the ASAP2 formula evaluator stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    February 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Helpers;
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Contains all the stuff for dynamically compiled formulas.
/// </summary>
namespace jnsoft.ASAP2.Formulas
{
  /// <summary>
  /// Represents a constant or a formula definition.
  /// 
  /// An instance of this class is created from a 
  /// - SYSTEM_CONSTANT
  /// - A2LFORMULA
  /// - A2LFORMULA / A2LFORMULA_INV pair.
  /// 
  /// Provides methods to compute 
  /// - physical from raw values 
  /// - raw values from physical values
  /// </summary>
  public sealed class A2LFormula
  {
    #region members
    /// <summary>
    /// Evaluator instance to the compiled formula).
    /// </summary>
    IEvaluator mEvaluator;
    /// <summary>
    /// Inverse evaluator instance to the compiled formula (may be null).
    /// </summary>
    IEvaluator mEvaluatorInv;
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance with the specified parameters.
    /// </summary>
    /// <param name="evaluator">The formula evaluator instance</param>
    /// <param name="evaluatorInv">The inverse formula evaluator instance (may be null)</param>
    internal A2LFormula(IEvaluator evaluator, IEvaluator evaluatorInv)
    { mEvaluator = evaluator; mEvaluatorInv = evaluatorInv; }
    #endregion
    #region methods
    /// <summary>
    /// Computes the physical value from a raw value.
    /// 
    /// - If this instance is based on a SYSTEM_CONSTANT, the input value is not used.
    /// </summary>
    /// <param name="rawValue">The raw value to transform to a physical value.</param>
    /// <returns>Physical value.</returns>
    public double toPhysical(double rawValue)
    {
      return mEvaluator.eval(rawValue);
    }
    /// <summary>
    /// Computes the raw value from a physical value.
    /// 
    /// - For formulas, the inverse formula evaluator is used.
    /// - If a formula does not define the inverse case, double.NaN is returned.
    /// </summary>
    /// <param name="dataType">The raw data type of the value</param>
    /// <param name="physValue">The physical value to transform to a raw value.</param>
    /// <returns>Raw value.</returns>
    public double toRaw(DATA_TYPE dataType, double physValue)
    {
      if (null == mEvaluatorInv)
        return double.NaN;
      double rawValue = mEvaluatorInv.eval(physValue);
      switch (dataType)
      {
        case DATA_TYPE.FLOAT32_IEEE:
        case DATA_TYPE.FLOAT64_IEEE: break;
        default:
          // round to zero decimals
          rawValue = Math.Round(rawValue); break;
      }
      return rawValue;
    }
    /// <summary>
    /// Provides a string representation for the instance.
    /// </summary>
    /// <returns>The instance value as a string</returns>
    public override string ToString()
    {
      return mEvaluatorInv != null
        ? string.Format("f({0})={1}, finv({2})={3}", 1, toPhysical(1), toPhysical(1), toRaw(DATA_TYPE.A_INT64, toPhysical(1)))
        : string.Format("f({0})={1}", 1, toPhysical(1));
    }
    #endregion
  }
  /// <summary>
  /// A A2LFormula dictionary (key = NodeName, value = A2LFormula).
  /// 
  /// The dictionary is created from SYSTEM_CONSTANT entries in the A2LMODPAR instance
  /// and A2LFORMULA / A2LFORMULA_INV instances.
  /// 
  /// The A2LParser creates this dictionary
  /// - construct instance
  /// - Add all SYSTEM_CONSTANT and A2LCOMPU_METHOD by calling the add methods.
  /// - Call buildAssembly
  /// - Set instance to A2LPROJECT.FormulaDict
  /// </summary>
  /// <example>Usage example:
  /// <code>
  /// using (var a2lParser = new A2LParser())
  /// {
  ///   // load test.a2l
  ///   a2lParser.parse("test.a2l");
  ///   // Get A2LMODPAR instance
  ///   var modpar = a2lParser.Project.getNode<A2LMODULE>(false).getNode<A2LMODPAR>(false);
  ///   // Output all system constants and it's values to console
  ///   var en = modpar.SystemConstants.GetEnumerator();
  ///   while (en.MoveNext())
  ///     Console.WriteLine("SYSTEM_CONSTANT {0}({1}), value={2}"
  ///       , en.Current.Key, en.Current.Value
  ///       , a2lParser.Project.FormulaDict[en.Current.Key].toPhysical(0)
  ///       );
  /// }
  /// </code></example>
  public sealed class A2LFormulaDict : Dictionary<string, A2LFormula>
  {
    #region local types
    /// <summary>
    /// Temporary data store from the add method calls until the buildAssembly method is called.
    /// </summary>
    sealed class TempFormulaData
    {
      internal string Formula;
      internal string FormulaInv;
      internal TempFormulaData(string formula) { Formula = formula; FormulaInv = null; }
      internal TempFormulaData(string formula, string formulaInv) { Formula = formula; FormulaInv = formulaInv; }
    }
    #endregion
    #region members
    /// <summary>
    /// Matches identifiers in math formulas.
    /// </summary>
    internal static Regex mIdentifierMatch = new Regex("([A-Z_]+[A-Z0-9_.]*)", RegexOptions.IgnoreCase);
    static Regex mSysConstMatch = new Regex("\\$\\(([A-Z_]+[A-Z0-9_.]*)\\)", RegexOptions.IgnoreCase);
    /// <summary>
    /// Dictionary mapping formula strings to formulas.
    /// 
    /// Used if an already created formula is defined more than once.
    /// In this case the formula is not created again (only reuded).
    /// </summary>
    Dictionary<string, string> mFormulaToIdent = new Dictionary<string, string>();
    Dictionary<string, TempFormulaData> mTempFormulas = new Dictionary<string, TempFormulaData>();
    #endregion
    #region ctor
    /// <summary>
    /// Creates a new instance of A2LFormulaDict.
    /// </summary>
    public A2LFormulaDict()
      : base(Settings.Default.StrictParsing
        ? StringComparer.OrdinalIgnoreCase
        : StringComparer.Ordinal)
    { }
    #endregion
    #region methods
    #region interface
    /// <summary>
    /// Adds a SYSTEM_CONSTANT to the formula dictionary.
    /// </summary>
    /// <param name="parser">The A2L parser instance (just to create messages, may be null)</param>
    /// <param name="ident">The ident (in this case the SYSTEM_CONSTANT name)</param>
    /// <param name="formula">The formula as text</param>
    public void add(A2LParser parser, string ident, string formula)
    {
      string nFormula = normalize(formula);
      if (string.IsNullOrEmpty(nFormula))
        return;
      if (parser != null && -1 != checkBracketsCount(nFormula))
      { // failed brackets count
        parser.onParserMessage(new ParserEventArgs(parser.Project, MessageType.Error
          , string.Format(Resources.strFailedSCBracketsCount, formula))
          );
        return;
      }

      string tempIdentName;
      if (mFormulaToIdent.TryGetValue(nFormula, out tempIdentName))
        // formula is already created
        mTempFormulas[ident] = mTempFormulas[tempIdentName];
      else
      {
        mTempFormulas[ident] = new TempFormulaData(nFormula);
        mFormulaToIdent[nFormula] = ident;
      }
    }
    /// <summary>
    /// Adds a formula to the formula dictionary.
    /// 
    /// </summary>
    /// <param name="parser">The A2L parser instance (just to create messages)</param>
    /// <param name="method">The parent compu method with CONVERSION_TYPE=CONVERSION_TYPE.FORM</param>
    /// <exception cref="ArgumentException">Thrown, if the compu method is not a CONVERSION_TYPE.FORM</exception>
    public void add(A2LParser parser, A2LCOMPU_METHOD method)
    {
      if (method == null || method.ConversionType != CONVERSION_TYPE.FORM)
        throw new ArgumentException("Must be a compu method with Conversion type form", "method");

      string ident = method.Name;
      // This is a formula compu method
      A2LFORMULA formula = method.getNode<A2LFORMULA>(false);
      if (formula == null)
      { // may never be missing
        parser.onParserMessage(new ParserEventArgs(method, MessageType.Error, Resources.strMissingFormula));
        return;
      }

      string nFormula = normalize(formula.Formula);
      if (string.IsNullOrEmpty(nFormula))
        return;
      if (-1 != checkBracketsCount(nFormula))
      { // failed brackets count
        parser.onParserMessage(new ParserEventArgs(method, MessageType.Error
          , string.Format(Resources.strFailedBracketsCount, string.Empty))
          );
        return;
      }

      // the inversion may be missing (depending on this compu method is only referenced by MEASUREMENTs)
      string nFormulaInv = null;
      if (!string.IsNullOrEmpty(formula.FormulaInv))
      { // inverse formula
        nFormulaInv = normalize(formula.FormulaInv);
        if (string.IsNullOrEmpty(nFormulaInv))
          return;
        if (-1 != checkBracketsCount(nFormulaInv))
        { // failed brackets count
          parser.onParserMessage(new ParserEventArgs(method, MessageType.Error
            , string.Format(Resources.strFailedBracketsCount, "inverse "))
            );
          return;
        }
      }

      string tempIdentName;
      if (mFormulaToIdent.TryGetValue(nFormula, out tempIdentName))
      { // formula is already created
        TempFormulaData tempFormula = mTempFormulas[tempIdentName];
        if (tempFormula.FormulaInv == null)
          // takeover of inverse formula if given
          tempFormula.FormulaInv = nFormulaInv;
        mTempFormulas[ident] = tempFormula;
      }
      else
      {
        mTempFormulas[ident] = new TempFormulaData(nFormula, nFormulaInv);
        mFormulaToIdent[nFormula] = ident;
      }
    }
    /// <summary>
    /// Fills the A2LFormulaDict after all SYSTEM_CONSTANT and A2LCOMPU_METHOD are added.
    /// 
    /// The algorithm:
    /// - Build the string representation of a CSharp code class from the formulas added
    /// - Compile the resulting string by using the .NET compiler
    /// - Create IEvaluator instances of the compiled classes
    /// - Create A2LFormula instances with the IEvaluator instances.
    /// </summary>
    /// <param name="parser">The A2L parser instance (just to create messages, may be null)</param>
    /// <param name="systemConstants">The system constant dictionary</param>
    public void buildAssembly(A2LParser parser, Dictionary<string, string> systemConstants)
    {
      try
      { // building the assembly requires the local path to the asap2if.dll
        var ass = Assembly.GetExecutingAssembly();
        string location = Path.GetDirectoryName(new Uri(ass.EscapedCodeBase).LocalPath);

        var startTime = DateTime.Now;
        var sb = new StringBuilder();
        if (systemConstants != null)
          // build system constants
          buildSystemConstantsClass(sb, systemConstants);

        //buildFormulaClass(parser, sb, systemConstants);

        var en = mTempFormulas.GetEnumerator();
        var createdFormulas = new Dictionary<string, string>();
        int counter = 0;
        while (en.MoveNext())
        {
          string ident = en.Current.Key;
          TempFormulaData fData = en.Current.Value;
          if (createdFormulas.ContainsKey(fData.Formula))
            // do not create all
            continue;

          if (sb.Length == 0)
            sb.AppendLine(Resources.strFormulaAssemblyHeader);

          string codeFormula = fData.Formula;
          var identifiers = getIdentifierList(systemConstants, ref codeFormula);

          if (identifiers.Length == 1 && identifiers[0] == codeFormula)
            // this does nothing but return the same value
            continue;

          sb.AppendFormat(Resources.strFormulaClass, string.Format(Resources.strFormulaName, counter));
          createdFormulas[fData.Formula] = counter.ToString();
          ++counter;
          if (identifiers.Length == 0)
            sb.AppendFormat(Resources.strFormulaFunction, codeFormula);
          else
            sb.AppendFormat(Resources.strFormulaFunctionParm, identifiers[0], codeFormula);

          if (fData.FormulaInv == null || createdFormulas.ContainsKey(fData.FormulaInv))
            continue;

          // create inverse formula
          codeFormula = fData.FormulaInv;
          identifiers = getIdentifierList(systemConstants, ref codeFormula);
          sb.AppendFormat(Resources.strFormulaClass, string.Format(Resources.strFormulaName, counter));
          createdFormulas[fData.FormulaInv] = counter.ToString();
          ++counter;
          if (identifiers.Length == 0)
            sb.AppendFormat(Resources.strFormulaFunction, codeFormula);
          else
            sb.AppendFormat(Resources.strFormulaFunctionParm, identifiers[0], codeFormula);
        }
        if (sb.Length == 0)
          return;
        // add end
        sb.Append("}");

        // build code
        using (var cp = new CSharpCodeProvider())
        {
          var cpar = new CompilerParameters();
          cpar.TreatWarningsAsErrors = cpar.IncludeDebugInformation = false;
          //cpar.CompilerOptions = "/optimize";
          cpar.GenerateInMemory = true;
          cpar.ReferencedAssemblies.AddRange(new string[] { "system.dll", Path.Combine(location, "asap2if.dll") });
#if DEBUG
          using (var sw = new StreamWriter(Path.Combine(Path.GetTempPath(), "ASAP2Formulas.cs")))
            sw.Write(sb.ToString());
#endif
          // build the csharp code
          var cr = cp.CompileAssemblyFromSource(cpar, sb.ToString());
          foreach (CompilerError ce in cr.Errors)
          { // errors occured by compiling
            // print out created code to file
            string formulaCodeFile = Path.Combine(Path.GetTempPath(), "ASAP2Formulas.cs");
            using (var sw = new StreamWriter(formulaCodeFile))
              sw.Write(sb.ToString());

            if (parser != null)
              parser.onParserMessage(new ParserEventArgs(parser.Project
                , MessageType.Error
                , string.Format(Resources.strFormulaCompError, ce.ErrorText
                , ce.Line, ce.Column, formulaCodeFile)
                ));
          }
          if (cr.Errors.Count > 0)
            return;
          if (cr.CompiledAssembly == null)
          { // failed to compile
            if (parser != null)
              parser.onParserMessage(new ParserEventArgs(parser.Project, MessageType.Error
              , Resources.strFailedToCompile
              ));
            return;
          }
          en = mTempFormulas.GetEnumerator();
          while (en.MoveNext())
          { // instantiate created formula classes 
            string ident = en.Current.Key;
            TempFormulaData fData = en.Current.Value;
            string methodName;
            if (!createdFormulas.TryGetValue(fData.Formula, out methodName))
              // not parsed -> not needed
              continue;

            // Get the compiled type
            var evaluatorType = cr.CompiledAssembly.GetType(string.Format(Resources.strFormulaNS + Resources.strFormulaName, methodName));
            // Create an instance from the type
            var evaluator = (IEvaluator)Activator.CreateInstance(evaluatorType);

            IEvaluator evaluatorInv = null;
            if (fData.FormulaInv != null)
            { // get the inverse method
              createdFormulas.TryGetValue(fData.FormulaInv, out methodName);
              Type evaluatorTypeInv = cr.CompiledAssembly.GetType(string.Format(Resources.strFormulaNS + Resources.strFormulaName, methodName));
              // Create an instance from the type
              evaluatorInv = (IEvaluator)Activator.CreateInstance(evaluatorTypeInv);
            }
            // create a new formula dict entry
            this[ident] = new A2LFormula(evaluator, evaluatorInv);
          }
          if (parser != null)
            parser.onParserMessage(new ParserEventArgs(parser.Project
            , MessageType.Info
            , string.Format(Resources.strFormulaCompileMsg
              , (UInt32)DateTime.Now.Subtract(startTime).TotalMilliseconds))
              );
        }
      }
      finally
      { // cleanup the temporary used memory
        mTempFormulas = null;
        mFormulaToIdent = null;
      }
    }
    #endregion
    /// <summary>
    /// Gets the list of identifiers within a formula.
    /// </summary>
    /// <param name="systemConstants">The system constant dictionary</param>
    /// <param name="nFormula">The formula to scan (will be modified)</param>
    /// <returns>The list of contained identifers (may be empty)</returns>
    static string[] getIdentifierList(Dictionary<string, string> systemConstants, ref string nFormula)
    {
      var resultingIdentifiers = new List<string>();
      var sb = new StringBuilder(nFormula);

      // Dictionary to prevent replacing the same identifiers more than once      
      var replacedStrings = new Dictionary<string, object>();

      // Do the system constants
      var syscMatches = mSysConstMatch.Matches(nFormula);
      for (int i = 0; i < syscMatches.Count; ++i)
      {
        string identifier = syscMatches[i].Groups[1].Value;
        string replacedConst = string.Format(Resources.strFormulaConstRef, identifier);
        sb.Replace(string.Format("$({0})", identifier), replacedConst);
        replacedStrings[replacedConst] = null;
      }
      // set result to new formula value
      nFormula = sb.ToString();

      // Do the identifiers and math functions
      var matches = mIdentifierMatch.Matches(nFormula);
      for (int i = 0; i < matches.Count; ++i)
      { // Iterate over all identifier matches
        string identifier = matches[i].Groups[1].Value;
        if (replacedStrings.ContainsKey(identifier))
          // already replaced
          continue;

        string function;
        if (!string.IsNullOrEmpty(function = CToCSharpMath.getFunction(identifier)))
        { // identifier is a math function, replace identifier in formula with .NET Math function
          sb.Replace(identifier, function);
          replacedStrings[identifier] = null;
        }
        else
          if (!identifier.Equals("e", StringComparison.OrdinalIgnoreCase))
            // ignore the euler constant
            resultingIdentifiers.Add(identifier);
      }

      // set changed formula
      nFormula = sb.ToString();
      // return remaining identifiers
      return resultingIdentifiers.ToArray();
    }
    /// <summary>
    /// Normalizezs the formula string
    /// 
    /// - Remove blanks, tabs, commas, CR, LF characters.
    /// - Replace commas with dots
    /// </summary>
    /// <param name="formula"></param>
    /// <returns>The normalized string</returns>
    static string normalize(string formula)
    {
      var sb = new StringBuilder(formula);
      sb.Replace(" ", string.Empty);
      sb.Replace("\t", string.Empty);
      sb.Replace("\r", string.Empty);
      sb.Replace("\n", string.Empty);
      sb.Replace(',', '.');
      return sb.ToString();
    }
    /// <summary>
    /// Checks if the formulas brackets are consistant.
    /// </summary>
    /// <param name="formula">The normalized formula to check</param>
    /// <returns>-1 if the count is correct, otherwise the character index of the error</returns>
    static int checkBracketsCount(string nFormula)
    {
      int bracketsCount = 0;
      for (int i = 0; i < nFormula.Length; ++i)
      {
        if (nFormula[i] == '(')
          ++bracketsCount;
        else if (nFormula[i] == ')')
          --bracketsCount;
        if (bracketsCount < 0)
          // this count may never get positive!
          return i;
      }
      return bracketsCount == 0 ? -1 : nFormula.Length - 1;
    }
#if USED
    /// <summary>
    /// Build the system constants class from the SYSTEM_CONSTANT dictionary
    /// </summary>
    /// <param name="sb">The string to add the result to</param>
    /// <param name="systemConstants">The system constants dictionary</param>
    void buildFormulaClass(A2LParser parser, StringBuilder sb, Dictionary<string, string> systemConstants)
    {
      var en = mTempFormulas.GetEnumerator();
      var createdFormulas = new Dictionary<string, string>();
      int counter = 0;
      while (en.MoveNext())
      {
        string ident = en.Current.Key;
        var fData = en.Current.Value;
        if (createdFormulas.ContainsKey(fData.Formula))
          // do not create all
          continue;

        if (sb.Length == 0)
          sb.AppendLine(Resources.strFormulaAssemblyHeader);

        string codeFormula = fData.Formula;
        var identifiers = getIdentifierList(systemConstants, ref codeFormula);
        sb.AppendFormat(Resources.strFormulaClass, string.Format(Resources.strFormulaName, counter));
        createdFormulas[fData.Formula] = counter.ToString();
        ++counter;
        if (identifiers.Length == 0)
          sb.AppendFormat(Resources.strFormulaFunction, codeFormula);
        else
          sb.AppendFormat(Resources.strFormulaFunctionParm, identifiers[0], codeFormula);

        if (fData.FormulaInv == null || createdFormulas.ContainsKey(fData.FormulaInv))
          continue;

        // create inverse formula
        codeFormula = fData.FormulaInv;
        identifiers = getIdentifierList(systemConstants, ref codeFormula);
        sb.AppendFormat(Resources.strFormulaClass, string.Format(Resources.strFormulaName, counter));
        createdFormulas[fData.FormulaInv] = counter.ToString();
        ++counter;
        if (identifiers.Length == 0)
          sb.AppendFormat(Resources.strFormulaFunction, codeFormula);
        else
          sb.AppendFormat(Resources.strFormulaFunctionParm, identifiers[0], codeFormula);
      }
      if (sb.Length == 0)
        return;
      // add end
      sb.Append("}");

      // build code
      using (var cp = new CSharpCodeProvider())
      {
        var cpar = new CompilerParameters();
        cpar.TreatWarningsAsErrors = cpar.IncludeDebugInformation = false;
        //cpar.CompilerOptions = "/optimize";
        cpar.GenerateInMemory = true;
        cpar.ReferencedAssemblies.AddRange(new string[] { "system.dll"
          , Path.Combine(Application.StartupPath, "asap2if.dll") 
          });

        // build the csharp code
        CompilerResults cr = cp.CompileAssemblyFromSource(cpar, sb.ToString());
        foreach (CompilerError ce in cr.Errors)
        { // errors occured by compiling
          // print out created code to file
          string formulaCodeFile = Path.Combine(Path.GetTempPath(), "ASAP2Formulas.cs");
          using (StreamWriter sw = new StreamWriter(formulaCodeFile))
            sw.Write(sb.ToString());

          parser.onParserMessage(new ParserEventArgs(parser.Project, MessageType.Error
            , string.Format(Resources.strFormulaCompError, ce.ErrorText
            , ce.Line, ce.Column, formulaCodeFile)
            ));
        }
        if (cr.Errors.Count > 0)
          return;
        if (cr.CompiledAssembly == null)
        { // failed to compile
          parser.onParserMessage(new ParserEventArgs(parser.Project, MessageType.Error
            , Resources.strFailedToCompile
            ));
          return;
        }
        en = mTempFormulas.GetEnumerator();
        while (en.MoveNext())
        { // instantiate created formula classes 
          string ident = en.Current.Key;
          TempFormulaData fData = en.Current.Value;
          string methodName;
          createdFormulas.TryGetValue(fData.Formula, out methodName);

          // Get the compiled type
          Type evaluatorType = cr.CompiledAssembly.GetType(string.Format(Resources.strFormulaNS + Resources.strFormulaName, methodName));
          // Create an instance from the type
          IEvaluator evaluator = (IEvaluator)Activator.CreateInstance(evaluatorType);

          IEvaluator evaluatorInv = null;
          if (fData.FormulaInv != null)
          { // get the inverse method
            createdFormulas.TryGetValue(fData.FormulaInv, out methodName);
            Type evaluatorTypeInv = cr.CompiledAssembly.GetType(string.Format(Resources.strFormulaNS + Resources.strFormulaName, methodName));
            // Create an instance from the type
            evaluatorInv = (IEvaluator)Activator.CreateInstance(evaluatorTypeInv);
          }
          // create a new formula dict entry
          this[ident] = new A2LFormula(evaluator, evaluatorInv);
        }
      }
    }
#endif
    /// <summary>
    /// Build the system constants class from the SYSTEM_CONSTANT dictionary
    /// </summary>
    /// <param name="sb">The string to add the result to</param>
    /// <param name="systemConstants">The system constants dictionary</param>
    static void buildSystemConstantsClass(StringBuilder sb
      , Dictionary<string, string> systemConstants
      )
    {
      var headerStr = new StringBuilder();
      headerStr.AppendLine(Resources.strFormulaAssemblyHeader);
      headerStr.AppendLine(Resources.strConstantsAssemblyHeader);

      var enSC = systemConstants.GetEnumerator();
      while (enSC.MoveNext())
      { // add system constants
        var m = mIdentifierMatch.Match(enSC.Current.Key);
        if (!m.Success || m.Length != enSC.Current.Key.Length)
          // system constants name is not an identifier
          continue;
        try
        {
          double scValue = A2LParser.parse2DoubleVal(enSC.Current.Value);
          sb.AppendFormat(CultureInfo.InvariantCulture, Resources.strConstantsAssemblyEntry, enSC.Current.Key, scValue);
        }
        catch (FormatException)
        { // not a constant...in this case a simple formula
          string formula = enSC.Current.Value;
          var identifiers = getIdentifierList(systemConstants, ref formula);
          var localsb = new StringBuilder();
          foreach (string s in identifiers)
            localsb.Append(s);
          if (localsb.Length == 0 || localsb.ToString() != enSC.Current.Value.Replace(" ", string.Empty).Replace("\t", string.Empty))
          {
            string constName = enSC.Current.Key.Trim().Replace(" ", "_0_").Replace("\t", "_0_");
            sb.AppendFormat(Resources.strConstantsAssemblyEntry, constName, formula);
          }
        }
      }
      if (sb.Length > 0)
      {
        sb.Insert(0, headerStr);
        sb.AppendLine("}");
      }
    }
    #endregion
  }
}
