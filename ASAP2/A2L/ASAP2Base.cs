﻿/*!
 * @file    
 * @brief   Implements the ASAP2 object model.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 * 
 * This file contains the base type definitions and base classes.
 */
/*!
 * @mainpage
 * @image html ASAP2Architecture.svg
 * 
 * <b>Feature list</b>
 * 
 * - Is a .NET Framework assembly and therefore usable from any available .NET framework language (C#, VB, J#, F#,...)
 * - Is exclusively written in C# managed code and therefore portable. Available for Windows or Mono (Linux/MacOS).
 * 
 * -# <b>ASAP2 reader module</b>
 *  - Implements the complete <A HREF="https://www.asam.net/standards/detail/mcd-2-mc/">ASAM MCD-2MC</A> Version 1.7.0 (ASAP2) specification
 *  - Creates a fully navigatable and object oriented .NET object model
 *  - Any A2L object instance and it’s properties are mapped to a corresponding .NET object
 *  - A2L syntax checking
 *  - A2L model checking (missing object links)
 *  - approximately 10 MBit/s parsing performance (maybe the fastest available ASAP2 parser)
 * -# <b>ASAP2 Writer module</b>
 *  - Rewriting and optimizing the A2L model as A2L file
 *  - Exporting the A2L model into a XML file
 * -# <b>ASAP2 Values module</b>
 *  - Reading ECU calibration data files into the created A2L model (supported are Intel HEX, Motorola S, INCA DCM, CDFX and MATLAB (.m) file formats)
 *  - A module to convert byte streams from the calibration data file into physical values of corresponding A2L characteristics
 *  - A module to convert physical data into byte streams in order to save modified data into a calibration data file
 * -# <b>ASAP2 Communication module</b>
 *  - <b>XCP module</b>
 *   - Implements the <A HREF="https://www.asam.net/standards/detail/mcd-1-xcp/">ASAM MCD-1 XCP</A> V1.3.0 specification supporting XCP over Ethernet (TCP/UDP) and XCP on CAN (using supported CAN hardware)
 *  - <b>CCP module</b>
 *   - Implements the <A HREF="https://www.asam.net/standards/detail/mcd-1-ccp/">ASAM MCD-1 CCP</A> V2.1.0 specification supporting communication over CAN (using supported CAN Hardware)
 *  - <b>Supports accessing the CAN bus with various CAN/CAN FD Hardware:</b>
 *   - <A HREF="https://www.8devices.com/products/usb2can">8devices</A>
 *   - <A HREF="http://advantech.com/">Advantech</A>
 *   - Eberspächer Electronics (Flexcard USB)
 *   - <A HREF="https://esd.eu/en">ESD</A> (CAN-USB-Micro, CAN-USB/2, CAN-PCI, CAN-PCIe, ...), CAN FD support
 *   - <A HREF="https://www.ixxat.com">IXXAT</A> (CAN-IB, USB-to-CAN V2)
 *   - <A HREF="https://www.kvaser.com">Kvaser</A> (USB, PCI Board, ...), CAN FD support
 *   - <A HREF="http://www.mhs-elektronik.de">MHS Elektronik</A> Tiny-CAN I, Tiny-CAN II XL, Tiny-CAN IV XL, Tiny-CAN M1, ...)
 *   - <A HREF="http://canusb.com">Lawicel</A>
 *   - <A HREF="http://ni.com/can">National Instruments (NI-CAN)</A> (USB 84XX, ...)
 *   - <A HREF="http://ni.com/can">National Instruments (XNET)</A> (PCI-85XX, USB-85XX, PCIE-85XX, PXI-85XX, ...)
 *   - <A HREF="https://www.peak-system.com">Peak</A> (PCAN-USB, PCAN-PCI, PCAN-ISA, PCAN-PCCard, PCAN-Dongle, ...), CAN FD support
 *   - <A HREF="https://industrial.softing.com">Softing</A>
 *   - <A HREF="https://vector.com">Vector</A> (VN1600, VN8900, VN5610, ...), CAN FD support
 * -# <b>Diagnostic Services module</b>
 *  - Implements the <A HREF="https://en.wikipedia.org/wiki/ISO_15765-2">ISO 15765-2 (IsoTp) protocol</A>.
 *  - Implements the <A HREF="https://en.wikipedia.org/wiki/Unified_Diagnostic_Services">ISO 14229 (Unified Diagnostic Service, UDS) protocol</A>.
 *  - Implements a module to read, modify and write ODX (Open Diagnostic data eXchange format, <A HREF="https://wiki.asam.net/display/STANDARDS/ASAM+MCD-2+D">ASAM MCD-2D v2.0.1-v2.2.0)</A> files
 * -# <b>DBC module</b>
 *  - Implements a module to read, modify and write CANdb++ (DBC) files
 *  - Measure signals from any jnsoft.Comm.CAN.CANProvider
 * -# <b>MDF (Measurement data format) module</b>
 *  - Implements a module to read MDF formats up to v4.1 and write MDF format v3.3 and MDF V4.1 (Measurement data format) files.
 *  - Both formats are accessible with the <b>same interface</b> for reading and writig measured data.
 * -# <b>ELF module</b>
 *  - Implements a module to read <A HREF="https://en.wikipedia.org/wiki/Executable_and_Linkable_Format">ELF (Excecutable and Linkable Format)</A> files in order to synchronize A2L object addresses.
 *  - Supports <A HREF="http://dwarfstd.org">DWARF2</A> formatted debug information contained in the ELF file
 *  - Allows to rewrite an A2L with updated addresses from an ELF file
 *  
 * @author Copyright © JNachbaur, 2011-2018
 */
using jnsoft.ASAP2.CCP;
using jnsoft.ASAP2.KWP;
using jnsoft.ASAP2.Properties;
using jnsoft.ASAP2.XCP;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;

namespace jnsoft.ASAP2
{
  #region types
  /// <summary>
  /// Defines if the ASAP2 writer should use tabs or spaces as indentation.
  /// </summary>
  public enum WriterIndent
  {
    /// <summary>
    /// Using the blank character to indent.
    /// </summary>
    Space,
    /// <summary>
    /// Using the tabulator character to indent.
    /// </summary>
    Tab,
  }
  /// <summary>
  /// Supported types of the ASAP2 standard.
  /// 
  /// - Specified for ASAP2 specification version 1.7.0
  /// - Some of the types are required (at least the A2LPROJECT), some are not.
  /// Refer to the ASAP2 specification which types you can rely on.
  /// </summary>
  public enum A2LType
  {
    /// <summary>This type is used for A2L nodes currently not implemented</summary>
    UNSUPPORTED,
    /// <summary>see ASAP2 specification</summary>
    PROJECT,
    /// <summary>see ASAP2 specification</summary>
    HEADER,
    /// <summary>see ASAP2 specification</summary>
    MODULE,
    /// <summary>see ASAP2 specification</summary>
    A2ML,
    /// <summary>see ASAP2 specification</summary>
    MOD_PAR,
    /// <summary>see ASAP2 specification</summary>
    MEMORY_LAYOUT,
    /// <summary>see ASAP2 specification</summary>
    IF_DATA,
    /// <summary>see ASAP2 specification</summary>
    MOD_COMMON,
    /// <summary>see ASAP2 specification</summary>
    AXIS_PTS,
    /// <summary>see ASAP2 specification</summary>
    CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    AXIS_DESCR,
    /// <summary>see ASAP2 specification</summary>
    MEASUREMENT,
    /// <summary>see ASAP2 specification</summary>
    FUNCTION,
    /// <summary>see ASAP2 specification</summary>
    GROUP,
    /// <summary>see ASAP2 specification</summary>
    DEF_CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    REF_CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    REF_MEASUREMENT,
    /// <summary>see ASAP2 specification</summary>
    REF_GROUP,
    /// <summary>see ASAP2 specification</summary>
    IN_MEASUREMENT,
    /// <summary>see ASAP2 specification</summary>
    OUT_MEASUREMENT,
    /// <summary>see ASAP2 specification</summary>
    LOC_MEASUREMENT,
    /// <summary>see ASAP2 specification</summary>
    SUB_FUNCTION,
    /// <summary>see ASAP2 specification</summary>
    FUNCTION_LIST,
    /// <summary>see ASAP2 specification</summary>
    SUB_GROUP,
    /// <summary>see ASAP2 specification</summary>
    COMPU_METHOD,
    /// <summary>see ASAP2 specification</summary>
    COMPU_VTAB_RANGE,
    /// <summary>see ASAP2 specification</summary>
    COMPU_VTAB,
    /// <summary>see ASAP2 specification</summary>
    RECORD_LAYOUT,
    /// <summary>see ASAP2 specification</summary>
    CALIBRATION_METHOD,
    /// <summary>see ASAP2 specification</summary>
    CALIBRATION_HANDLE,
    /// <summary>see ASAP2 specification</summary>
    MEMORY_SEGMENT,
    /// <summary>see ASAP2 specification</summary>
    ANNOTATION,
    /// <summary>see ASAP2 specification</summary>
    ANNOTATION_TEXT,
    /// <summary>see ASAP2 specification</summary>
    COMPU_TAB,
    /// <summary>see ASAP2 specification</summary>
    FORMULA,
    /// <summary>see ASAP2 specification</summary>
    FIX_AXIS_PAR_LIST,
    /// <summary>see ASAP2 specification</summary>
    FRAME,
    /// <summary>see ASAP2 specification</summary>
    DEPENDENT_CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    VIRTUAL_CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    UNIT,
    /// <summary>see ASAP2 specification</summary>
    USER_RIGHTS,
    /// <summary>see ASAP2 specification</summary>
    VIRTUAL,
    /// <summary>see ASAP2 specification</summary>
    VAR_CRITERION,
    /// <summary>see ASAP2 specification</summary>
    VAR_CHARACTERISTIC,
    /// <summary>see ASAP2 specification</summary>
    VAR_ADDRESS,
    /// <summary>see ASAP2 specification</summary>
    VAR_FORBIDDEN_COMB,
    /// <summary>see ASAP2 specification</summary>
    BIT_OPERATION,
    /// <summary>see ASAP2 specification</summary>
    VARIANT_CODING,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    BLOB,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    INSTANCE,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TYPEDEF_STRUCTURE,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TYPEDEF_BLOB,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TYPEDEF_MEASUREMENT,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TYPEDEF_AXIS,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TYPEDEF_CHARACTERISTIC,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    STRUCTURE_COMPONENT,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    OVERWRITE,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TRANSFORMER,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TRANSFORMER_IN_OBJECTS,
    /// <summary>see ASAP2 specification >= 1.7.0</summary>
    TRANSFORMER_OUT_OBJECTS,
    #region CCP support
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    DAQ_LIST_CAN_ID,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    DEFINED_PAGES,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    CAN_PARAM,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    CHECKSUM_PARAM,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    TP_BLOB,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    SEED_KEY,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    SOURCE,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    RASTER,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    EVENT_GROUP,
    /// <summary>See CCP Version 2.1, ASAP1B_CCP (COMMON_PARAMETERS)</summary>
    QP_BLOB,
    #endregion
    #region KWP support
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    K_LINE,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    CAN,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    ADDRESS,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    NETWORK_LIMITS,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    TIME_DEF,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    COPY,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    FLASH_COPY,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    FLASH,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    DIAG_BAUD,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    PAGE_SWITCH,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    ROUTINE_PARA,
    /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
    DATA_ADDRESS,
    #endregion
    #region XCP support
    /// <summary>see ASAP2 specification > 1.6.0</summary>
    EVENT_CAN_ID_LIST,
    /// <summary>see ASAP2 specification > 1.6.0</summary>
    CAN_FD,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    PROTOCOL_LAYER,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    SEGMENT,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    CHECKSUM,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    PAGE,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    ADDRESS_MAPPING,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    DAQ,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    STIM,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    DAQ_LIST,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    EVENT,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    DAQ_EVENT,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
    AVAILABLE_EVENT_LIST,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
    DEFAULT_EVENT_LIST,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
    CONSISTENCY_EVENT_LIST,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    TIMESTAMP_SUPPORTED,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    PAG,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    PGM,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    SECTOR,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    XCP_ON_UDP_IP,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    XCP_ON_TCP_IP,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
    XCP_ON_CAN,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
    TIME_CORRELATION,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Time correlation)</summary>
    CLOCK,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Time correlation, Clock)</summary>
    TIMESTAMP_CHARACTERIZATION,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (DAQ_LIST)</summary>
    PREDEFINED,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (PREDEFINED)</summary>
    ODT,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Protocol Layer)</summary>
    ECU_STATES,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Protocol Layer, ECU States)</summary>
    STATE,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Protocol Layer, ECU States, State)</summary>
    MEMORY_ACCESS,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
    DAQ_MEMORY_CONSUMPTION,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Event channels)</summary>
    MIN_CYCLE_TIME,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, overrules default BUFFER_RESERVE for this EVENT)</summary>
    BUFFER_RESERVE_EVENT,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, DAQ packed mode, applies for all associated DAQ lists)</summary>
    DAQ_PACKED_MODE,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption for DAQ)</summary>
    CPU_LOAD_CONSUMPTION_DAQ,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption for STIM)</summary>
    CPU_LOAD_CONSUMPTION_STIM,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption for QUEUE)</summary>
    CPU_LOAD_CONSUMPTION_QUEUE,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption for QUEUE_STIM)</summary>
    CPU_LOAD_CONSUMPTION_QUEUE_STIM,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, ODT Entry size factor table)</summary>
    ODT_ENTRY_SIZE_FACTOR_TABLE,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load maximum, max load on single core)</summary>
    CORE_LOAD_MAX,
    /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption, max load on single core)</summary>
    CORE_LOAD_EP,
    #region XCP on USB
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    XCP_ON_USB,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    DAQ_LIST_USB_ENDPOINT,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    OUT_EP_CMD_STIM,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    OUT_EP_ONLY_STIM,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    IN_EP_ONLY_DAQ,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    IN_EP_ONLY_EVSERV,
    /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB</summary>
    IN_EP_RESERR_DAQ_EVSERV,
    #endregion
    #endregion
  }
  /// <summary>see ASAP2 specification</summary>
  public enum CONVERSION_TYPE
  {
    /// <summary>
    /// no conversion of the internal source value.
    /// 
    /// The following equation is applied: PHYS = INT 
    /// </summary>
    IDENTICAL,
    /// <summary>conversion based on the formula specified by the FORMULA keyword.</summary>
    FORM,
    /// <summary>
    /// linear function of the following type: f(x)=ax + b
    /// 
    /// for which: PHYS=f(INT)
    /// 
    /// The coefficients a and b have to be specified by the COEFFS_LINEAR keyword.
    /// </summary>
    LINEAR,
    /// <summary>
    /// fractional rational function of the following type f(x)=(axx + bx + c)/(dxx + ex + f)
    /// 
    /// for which:  INT = f(PHYS)
    /// </summary>
    RAT_FUNC,
    /// <summary>table with interpolation</summary>
    TAB_INTP,
    /// <summary>table without interpolation</summary>
    TAB_NOINTP,
    /// <summary>verbal conversion table</summary>
    TAB_VERB,
  }
  /// <summary>The axis points of a characteristic can be deposited in two different ways in the memory</summary>
  public enum DEPOSIT_TYPE
  {
    /// <summary> Used when deposit type is 'not set'.</summary>
    NotSet,
    /// <summary>
    /// absolute axis points
    /// 
    /// The individual axis point values are deposited as absolute values.
    /// </summary>
    ABSOLUTE,
    /// <summary>
    /// difference axis points
    /// 
    /// The individual axis points are deposited as differences. 
    /// Each axis point value is determined on the basis of the 
    /// adjacent axis point (predecessor) and the corresponding 
    /// difference. As reference point for the first axis point
    /// <b>maxvalue</b> is used: 
    /// 
    /// - 1-byte-size: maxvalue = 2^8 (256)
    /// - 2-byte-size: maxvalue = 2^16 (65536) 
    /// - 4-byte-size: maxvalue = 2^32
    /// </summary>
    DIFFERENCE,
  }
  /// <summary>see ASAP2 specification</summary>
  public enum MEMORYPRG_TYPE
  {
    /// <summary>
    /// The kind of data is not known.
    /// </summary>
    UNKNOWN,
    /// <summary>program code</summary>
    CODE,
    /// <summary>program data allowed for online calibration</summary>
    DATA,
    /// <summary>program data allowed only for offline calibration</summary>
    OFFLINE_DATA,
    /// <summary>program variables</summary>
    VARIABLES,
    /// <summary>program data for serial emulation</summary>
    SERAM,
    /// <summary>reserved segments</summary>
    RESERVED,
    /// <summary>
    /// Values which are available in the ECU but do not exist in the Hex-file.
    /// 
    /// There is no upload required to get access to the ECU data. The ECU will never 
    /// be touched by the instrumentation tool except by upload.
    /// </summary>
    CALIBRATION_VARIABLES,
    /// <summary>Values existing in the ECU but not dropped down in the binary file.
    /// 
    /// There should no upload be needed to get access to the ECU data. The ECU will 
    /// never be touched by the instrumentation tool except by upload.
    /// </summary>
    EXCLUDE_FROM_FLASH,
  }
  /// <summary>Description of the type of memory used</summary>
  public enum MEMORY_TYPE
  {
    /// <summary>segment of RAM</summary>
    RAM,
    /// <summary>segment of EEPROM</summary>
    EEPROM,
    /// <summary>segment of EPROM</summary>
    EPROM,
    /// <summary>segment of ROM</summary>
    ROM,
    /// <summary>segment of CPU registers</summary>
    REGISTER,
    /// <summary>segment of FLASH</summary>
    FLASH,
    /// <summary>Not available segemnt in ECU memory (no download/upload to ECU)</summary>
    NOT_IN_ECU,
  }
  /// <summary>Memory attributes</summary>
  public enum MEMORY_ATTRIBUTE
  {
    /// <summary>internal segment</summary>
    INTERN,
    /// <summary>external segment</summary>
    EXTERN,
  }
  /// <summary>Description of the program segments</summary>
  public enum PRG_TYPE
  {
    /// <summary>program code</summary>
    PRG_CODE,
    /// <summary>program data</summary>
    PRG_DATA,
    /// <summary>other</summary>
    PRG_RESERVED,
  }
  /// <summary>see ASAP2 specification</summary>
  public enum UNIT_TYPE
  {
    /// <summary>
    /// Measurement unit derived from another one referenced by the optional parameter REF_UNIT.
    /// </summary>
    DERIVED,
    /// <summary>
    /// Extended SI unit.
    /// 
    /// Recommendation: The principle of describing "real" measurement units is to refer to SI units. 
    /// Therefore, this relationship should be given by using the optional parameter SI_EXPONENTS. 
    /// </summary>
    EXTENDED_SI,
  }
  /// <summary>Enumeration for description of the word lengths in the ECU program</summary>
  public enum DATA_SIZE
  {
    /// <summary>8 Bit size</summary>
    BYTE,
    /// <summary>16 Bit size</summary>
    WORD,
    /// <summary>32 Bit size</summary>
    LONG,
  }
  /// <summary>Possible characteristic types</summary>
  [Flags]
  public enum CHAR_TYPE
  {
    /// <summary>
    /// Unknown or not defined.
    /// </summary>
    NotSet = 0,
    /// <summary>scalar</summary>
    [Description("Single value")]
    VALUE = 1,
    /// <summary>string</summary>
    [Description("ASCII value")]
    ASCII = VALUE << 1,
    /// <summary>array without axes</summary>
    [Description("Value block")]
    VAL_BLK = VALUE << 2,
    /// <summary>1-dimensional array with axes</summary>
    [Description("Curve value")]
    CURVE = VALUE << 3,
    /// <summary>2-dimensional array with axes</summary>
    [Description("Map value")]
    MAP = VALUE << 4,
    /// <summary>3-dimensional array with axes</summary>
    [Description("Cuboid value")]
    CUBOID = VALUE << 5,
    /// <summary>4-dimensional array with axes</summary>
    [Description("Cube (4 axes) value")]
    CUBE_4 = VALUE << 6,
    [Description("Cube (5 axes) value")]
    /// <summary>5-dimensional array with axes</summary>
    CUBE_5 = VALUE << 7,
  }
  /// <summary>Description of the axis points</summary>
  public enum AXIS_TYPE
  {
    /// <summary>
    /// Standard axis.
    /// </summary>
    STD_AXIS,
    /// <summary>
    /// This is a curve or a 
    /// map with virtual axis points that are not 
    /// deposited at EPROM. The axis points can be 
    /// calculated from parameters defined with keywords 
    /// FIX_AXIS_PAR, FIX_AXIS_PAR_DIST and FIX_AXIS_PAR_LIST.
    /// The axis points can‘t be modified.
    /// </summary>
    FIX_AXIS,
    /// <summary>
    /// Group axis points or description of the axis
    /// points for SIEMENS deposit. For this variant 
    /// of the axis points the axis point values are 
    /// separated from the table values of the curve 
    /// or map in the emulation memory and must be 
    /// described by a special AXIS_PTS data record.
    /// The reference to this record occurs with the 
    /// keyword 'AXIS_PTS_REF'. 
    /// </summary>
    COM_AXIS,
    /// <summary>
    /// Rescale axis. For this variant of the axis 
    /// points the axis point values are separated from 
    /// the table values of the curve or map in the 
    /// emulation memory and must be described by a 
    /// special AXIS_PTS data record. The reference 
    /// to this record occurs with the keyword 
    /// 'AXIS_PTS_REF'.
    /// </summary>
    RES_AXIS,
    /// <summary>
    /// Curve axis. This axis type uses a separate CURVE 
    /// CHARACTERISTIC to rescale the ASAM MCD 2MC / ASAP2 
    /// Interface Specification 2002-02-05 Version 1.51
    /// Release Page 47 of 277 axis. The referenced CURVE 
    /// is used to lookup an axis index, and the index value 
    /// is used by the controller to determine the operating 
    /// point in the CURVE or MAP. See Appendix C for more 
    /// details.
    /// </summary>
    CURVE_AXIS,
  }
  /// <summary>
  /// Description of the monotony.
  /// 
  /// This keyword can be  used to specify the monotony of an adjustment object. 
  /// The monotony is always related to an axis (see keyword "AXIS_DESCR"). 
  /// With each adjustment operation the measurement and calibration system 
  /// (user interface) verifies whether the monotony is guaranteed. Changes that 
  /// do not correspond to the monotony are not allowed.
  /// </summary>
  public enum MONOTONY_TYPE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>no monotony required</summary>
    NOT_MON,
    /// <summary>monotonously decreasing</summary>
    MON_DECREASE,
    /// <summary>monotonously increasing</summary>
    MON_INCREASE,
    /// <summary>strict monotonously decreasing</summary>
    STRICT_DECREASE,
    /// <summary>strict monotonously increasing</summary>
    STRICT_INCREASE,
    /// <summary>monotonously in- or decreasing</summary>
    MONOTONOUS,
    /// <summary>strict monotonously in- or decreasing</summary>
    STRICT_MON,
  }
  /// <summary>see ASAP2 specification</summary>
  public enum INDEX_MODE
  {
    /// <summary>
    /// Index mode is not set.
    /// 
    /// This usually may never occur, but in some cases the value is optinal.
    /// </summary>
    NotSet,
    /// <summary>
    /// deposited in rows
    /// </summary>
    ROW_DIR,
    /// <summary>
    /// deposited in columns
    /// </summary>
    COLUMN_DIR,
    /// <summary>
    /// maps: deposited in columns, the columns of table values alternate with the respective X-coordinates. 
    /// curves: table values and X-coordinate values are deposited alternating.
    /// </summary>
    ALTERNATE_WITH_X,
    /// <summary>
    /// maps: deposited in rows, the rows of table values alternate with the respective Y-coordinates (maps only).
    /// </summary>
    ALTERNATE_WITH_Y,
    /// <summary>
    /// curves: curves which share a common axis are deposited in columns; each row of memory contains 
    /// values for all the shared axis curves at a given axis breakpoint. Required in order to represent 
    /// characteristics which correspond to arrays of structures in ECU program code. In the example code 
    /// below, DT10, DT20, etcare treated as separate curves which may have different conversions or limits:
    /// <code>
    /// typedef struct
    /// {
    ///   int DT10;
    ///   int DT20;
    ///   int DT30;
    ///   int DT40;
    /// } VXP_TYPE;
    /// const VXP_TYPE VX_PLUS_DELAY_TIMES[5] = {
    /// { 10, 3, 4, 8 },
    /// { 12, 2, 4, 6 },
    /// { 17, 9, 5, 8 },
    /// { 10, 1, 4, 8 },
    /// { 18, 3, 8, 8 },
    /// };
    /// </code>
    /// </summary>
    ALTERNATE_CURVES,
  }
  /// <summary>
  /// Enumeration for description of the addressing of table values or axis point values.
  /// 
  /// See ASAP2 specification.
  /// </summary>
  public enum ADDR_TYPE
  {
    /// <summary>
    /// The relevant memory location has the first table value or axis point value, all others follow with incrementing address.
    /// </summary>
    DIRECT,
    /// <summary>
    /// The relevant memory location has a 1 byte pointer to this table value or axis point value.
    /// </summary>
    PBYTE,
    /// <summary>
    /// The relevant memory location has a 2 byte pointer to this table value or axis point value.
    /// </summary>
    PWORD,
    /// <summary>
    /// The relevant memory location has a 4 byte pointer to this table value or axis point value.
    /// </summary>
    PLONG,
    /// <summary>
    /// The relevant memory location has a 8 byte pointer to this table value or axis point value.
    /// </summary>
    PLONGLONG,
  }
  /// <summary>Enumeration for description of the axis point sequence in the memory. </summary>
  public enum INDEX_ORDER
  {
    /// <summary>
    /// Increasing index with increasing address.
    /// </summary>
    INDEX_INCR,
    /// <summary>
    /// decreasing index with increasing address.
    /// </summary>
    INDEX_DECR,
  }
  /// <summary>
  /// This keyword specifies the access of a CHARACTERISTIC or AXIS_PTS for calibration. 
  /// 
  /// It substitutes the READ_ONLY attribute since ASAM-MCD-2MC V1.4
  /// </summary>
  public enum CALIBRATION_ACCESS
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>Characteristic or axis points with calibration allowed.</summary>
    CALIBRATION,
    /// <summary>
    /// This keyword can be used to indicate that the axis points cannot be changed (but can be read only). 
    /// 
    /// Note: This optional keyword used at CHARACTERISTIC record indicates the adjustable object 
    /// to be read only at all (table values and axis pts).
    /// </summary>
    NO_CALIBRATION,
    /// <summary>
    /// Internal characteristic or axis points which are not readable or writeable by the MCD-System.
    /// 
    /// If there are references between AXIS_PTS and CHARACTERISTICS with one of them gets this value of 
    /// CALIBRATION_ACCESS, the other one must get this value too.
    /// </summary>
    NOT_IN_MCD_SYSTEM,
    /// <summary>
    /// Variables which can be flashed but not emulated ore calibrated, e.g. values representing 
    /// safety relevant property while driving.
    /// </summary>
    OFFLINE_CALIBRATION,
  }
  /// <summary>
  /// The BIT_OPERATION keyword can be used to perform operation on the masked out value. 
  /// 
  /// First BIT_MASK will be applied on measurement data, then LEFT_SHIFT / RIGHT_SHIFT is 
  /// performed and last the SIGN_EXTEND is carried out. SIGN_EXTEND means that the sign bit 
  /// (masked data’s leftmost bit) will be copied to all bit positions to the left of the sign bit.
  /// This results in a new datatype with the same signed value as the masked data.
  /// </summary>
  public enum BITOPERATION_TYPE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>Number of positions to left shift data, zeros will be shifted in from the right.</summary>
    RIGHT_SHIFT,
    /// <summary>Number of positions to right shift data, zeros will be shifted in from the left.</summary>
    LEFT_SHIFT,
  }
  /// <summary>
  /// Codes for scaling units (CSE).
  /// </summary>
  public enum ScalingUnits
  {
    /// <summary>
    /// 1 µsec
    /// </summary>
    Time_1uSec = 0,
    /// <summary>
    /// 10 µsec
    /// </summary>
    Time_10uSec = 1,
    /// <summary>
    /// 100 µsec
    /// </summary>
    Time_100uSec = 2,
    /// <summary>
    /// 1 msec
    /// </summary>
    Time_1mSec = 3,
    /// <summary>
    /// 10 msec
    /// </summary>
    Time_10mSec = 4,
    /// <summary>
    /// 100 msec
    /// </summary>
    Time_100mSec = 5,
    /// <summary>
    /// 1 sec
    /// </summary>
    Time_1Sec = 6,
    /// <summary>
    /// 10 sec
    /// </summary>
    Time_10Sec = 7,
    /// <summary>
    /// 1 Minute
    /// </summary>
    Time_1Min = 8,
    /// <summary>
    /// 1 Hour
    /// </summary>
    Time_1Hour = 9,
    /// <summary>
    /// 1 day
    /// </summary>
    Time_1Day = 10,
    /// <summary>
    /// Angular degrees
    /// </summary>
    AngularDegrees = 100,
    /// <summary>
    /// Revolutions 360 degrees
    /// </summary>
    Revolutions = 101,
    /// <summary>
    /// Cycle 720 degrees angle, e.g. in case of IC engines
    /// </summary>
    Cycle = 102,
    /// <summary>
    /// Cylinder segment Combustion, e.g. in case of IC engines 
    /// </summary>
    CylinderSegmentCombustion = 103,
    /// <summary>
    /// When frame available Event Source defined in keyword Frame
    /// </summary>
    FrameAvailableEvent = 998,
    /// <summary>
    /// Calculation of a new upper range limit after receiving a 
    /// new partial value, e.g. when calculating a complex trigger 
    /// condition
    /// </summary>
    AlwaysOnNewValue = 999,
    /// <summary>
    /// Without fixed scaling
    /// </summary>
    NonDeternministic = 1000,
    /// <summary>
    /// Refresh rate unit is not defined
    /// </summary>
    NotSet = int.MaxValue,
  }
  /// <summary>
  /// Defined checksum types
  /// 
  /// All checksum algorithms required by ASAP2 and XCP are supported.
  /// </summary>
  public enum ChecksumType : byte
  {
    /// <summary>
    /// not set
    /// </summary>
    NotSet = 0x00,
    /// <summary>
    /// Add BYTE into a BYTE checksum, ignore overflows
    /// </summary>
    ADD_11 = 0x01,
    /// <summary>
    /// Add BYTE into a WORD checksum, ignore overflows
    /// </summary>
    ADD_12 = 0x02,
    /// <summary>
    /// Add BYTE into a DWORD checksum, ignore overflows
    /// </summary>
    ADD_14 = 0x03,
    /// <summary>
    /// See CRC error detection algorithms
    /// 
    /// Only defined by CCP.
    /// </summary>
    CRC_8 = 0x04,
    /// <summary>
    /// See CRC error detection algorithms
    /// 
    /// Only defined by CCP.
    /// </summary>
    CRC_16 = 0x05,
    /// <summary>
    /// See CRC error detection algorithms
    /// </summary>
    CRC_32 = 0x06,
    /// <summary>
    /// See CRC error detection algorithms
    /// 
    /// Only defined by CCP.
    /// </summary>
    CRC_2_16 = 0x07,
    /// <summary>
    /// Add WORD into a WORD checksum, ignore overflows, blocksize must be modulo 2 
    /// </summary>
    ADD_22 = 0x08,
    /// <summary>
    /// Add WORD into DWORD, ignore overflows, blocksize must be modulo 2
    /// </summary>
    ADD_24 = 0x09,
    /// <summary>
    /// Add DWORD into DWORD, ignore overflows, blocksize must be modulo 4
    /// </summary>
    ADD_44 = 0x0A,
    /// <summary>
    /// User defined algorithm, in externally calculated function
    /// </summary>
    USER_DEFINED = 0x0B,
    /// <summary>
    /// See CRC error detection algorithms
    /// </summary>
    CRC_16_CITT = 0x0C,
  }
  /// <summary>
  /// Possible active ECU pages (generic for XCP and CCP).
  /// </summary>
  public enum ECUPage : byte
  {
    /// <summary>
    /// The flash (persistent) page
    /// </summary>
    [Description("FLASH page")]
    Flash = 0,
    /// <summary>
    /// The RAM (writeable) page
    /// </summary>
    [Description("RAM page")]
    RAM = 1,
    /// <summary>
    /// Page is not known.
    /// </summary>
    NotSet = byte.MaxValue,
  }
  /// <summary>
  /// Format of variant extension (index)
  /// </summary>
  public enum VarNamingType
  {
    /// <summary>Not defined value.</summary>
    NotSet,
    /// <summary>
    /// variant extension is a number (integer: 0,1,2,3...).
    /// </summary>
    NUMERIC,
  }
  /// <summary>
  /// Defines the encoding for characteristics of type CHAR_TYPE.ASCII.
  /// 
  /// If no ENCODING is specified, the content of the CHARACTERISTIC is interpreted as array of 8 Bit characters.
  /// </summary>
  public enum EncodingType
  {
    ASCII,
    UTF8,
    UTF16,
    UTF32,
  }
  /// <summary>
  /// Defines values for the A2ÖTRANSFORMER Trigger.
  /// </summary>
  public enum TriggerType
  {
    /// <summary>
    /// Triggers the TRANSFORMER whenever a referenced input object in IN_OBJECTS is changed.
    /// </summary>
    ON_CHANGE,
    /// <summary>
    /// Triggers the TRANSFORMER whenever the user issues the trigger request in the MC tool.
    /// </summary>
    ON_USER_REQUEST,
  }
  #endregion
  #region base classes
  /// <summary>
  /// Base class for any A2L entity.
  /// 
  /// Any A2LNODE based type and therfore ASAP2 entity is
  /// defined as an uppercase'd class starting with A2L.
  /// This is done to reflect the direct reference to the
  /// ASAP2 specification.
  /// </summary>
  /// <remarks>
  /// Any A2LNode derived Type needs to implement a parameterless 
  /// default constructor to support the XML export.
  /// 
  /// Remarks to ASAP2 optional parameters and therefore 
  /// to properties of created A2LNode instances:
  /// 
  /// Many properties of an A2LNode instance are optional
  /// (please refer to the ASAP2 specification). 
  /// As an A2LNode instance contains any defined parameter
  /// as a property the implementation behaves as follows:
  /// 
  /// - for an optional value type property (e.g a property of type <b>int</b>)
  /// if the property is optional, a proper default value according the ASAP2 specification is set
  /// 
  /// - for an optional reference type property (e.g a property of type <b>string</b>)
  /// The property instance is not created by the parser, 
  /// so you have to always check against the null value before accessing them)
  /// </remarks>
  public abstract partial class A2LNODE
  {
    #region members
    #region A2L keywords
    internal protected const string A2LC_ByteOrder = "BYTE_ORDER";
    internal protected const string A2LC_Deposit = "DEPOSIT";
    internal protected const string A2LC_DataSize = "DATA_SIZE";
    internal protected const string A2LC_Format = "FORMAT";
    internal protected const string A2LC_DisplayId = "DISPLAY_IDENTIFIER";
    internal protected const string A2LC_ReadOnly = "READ_ONLY";
    internal protected const string A2LC_PhysUnit = "PHYS_UNIT";
    internal protected const string A2LC_StepSize = "STEP_SIZE";
    internal protected const string A2LC_Monotony = "MONOTONY";
    internal protected const string A2LC_GuardRails = "GUARD_RAILS";
    internal protected const string A2LC_AdrExt = "ADDRESS_EXTENSION";
    internal protected const string A2LC_EcuAdrExt = "ECU_" + A2LC_AdrExt;
    internal protected const string A2LC_BitMask = "BIT_MASK";
    internal protected const string A2LC_MatrixDim = "MATRIX_DIM";
    internal protected const string A2LC_RefMemSeg = "REF_MEMORY_SEGMENT";
    internal protected const string A2LC_SymbolLink = "SYMBOL_LINK";
    internal protected const string A2LC_ModelLink = "MODEL_LINK";
    internal protected const string A2LC_ExLimits = "EXTENDED_LIMITS";
    internal protected const string A2LC_MaxRefresh = "MAX_REFRESH";
    internal protected const string A2LC_Discrete = "DISCRETE";
    internal protected const string A2LC_AlByte = "ALIGNMENT_BYTE";
    internal protected const string A2LC_AlWord = "ALIGNMENT_WORD";
    internal protected const string A2LC_AlLong = "ALIGNMENT_LONG";
    internal protected const string A2LC_AlFloat32 = "ALIGNMENT_FLOAT32_IEEE";
    internal protected const string A2LC_AlFloat64 = "ALIGNMENT_FLOAT64_IEEE";
    internal protected const string A2LC_AlInt64 = "ALIGNMENT_INT64";
    internal protected const string A2LC_RefUnit = "REF_UNIT";
    internal protected const string A2LC_UnitConv = "UNIT_CONVERSION";
    internal protected const string A2LC_SIExponents = "SI_EXPONENTS";
    internal protected const string A2LC_MaxGrad = "MAX_GRAD";
    internal protected const string A2LC_AxisPtsRef = "AXIS_PTS_REF";
    internal protected const string A2LC_CurveAxisRef = "CURVE_AXIS_REF";
    internal protected const string A2LC_FixAxisPar = "FIX_AXIS_PAR";
    internal protected const string A2LC_FixAxisParDist = "FIX_AXIS_PAR_DIST";
    internal protected const string A2LC_Number = "NUMBER";
    internal protected const string A2LC_CalAccess = "CALIBRATION_ACCESS";
    internal protected const string A2LC_CoeffsLinear = "COEFFS_LINEAR";
    internal protected const string A2LC_CompuTabRef = "COMPU_TAB_REF";
    internal protected const string A2LC_Coeffs = "COEFFS";
    internal protected const string A2LC_StatStrRef = "STATUS_STRING_REF";
    internal protected const string A2LC_Layout = "LAYOUT";
    internal protected const string A2LC_EcuAdr = "ECU_ADDRESS";
    internal protected const string A2LC_ErrorMask = "ERROR_MASK";
    internal protected const string A2LC_ReadWrite = "READ_WRITE";
    internal protected const string A2LC_SignExtend = "SIGN_EXTEND";
    internal protected const string A2LC_Version = "VERSION";
    internal protected const string A2LC_Supplier = "SUPPLIER";
    internal protected const string A2LC_Customer = "CUSTOMER";
    internal protected const string A2LC_CustomerNo = "CUSTOMER_NO";
    internal protected const string A2LC_User = "USER";
    internal protected const string A2LC_PhoneNo = "PHONE_NO";
    internal protected const string A2LC_Ecu = "ECU";
    internal protected const string A2LC_CpuType = "CPU_TYPE";
    internal protected const string A2LC_NoOfIFs = "NO_OF_INTERFACES";
    internal protected const string A2LC_Epk = "EPK";
    internal protected const string A2LC_AddrEpk = "ADDR_EPK";
    internal protected const string A2LC_EcuCalOffs = "ECU_CALIBRATION_OFFSET";
    internal protected const string A2LC_SysConst = "SYSTEM_CONSTANT";
    internal protected const string A2LC_FormulaInv = "FORMULA_INV";
    internal protected const string A2LC_DefaultValue = "DEFAULT_VALUE";
    internal protected const string A2LC_DefaultValueNum = "DEFAULT_VALUE_NUMERIC";
    internal protected const string A2LC_ProjectNo = "PROJECT_NO";
    internal protected const string A2LC_PAGE_CODE = "PAGE_CODE";
    internal protected const string A2LC_ESCAPE_CODE_PARA_SET = "ESCAPE_CODE_PARA_SET";
    internal protected const string A2LC_ESCAPE_CODE_PARA_GET = "ESCAPE_CODE_PARA_GET";
    internal protected const string A2LC_RNC_RESULT = "RNC_RESULT";
    internal protected const string A2LC_COPY_FRAME = "COPY_FRAME";
    internal protected const string A2LC_COPY_PARA = "COPY_PARA";
    internal protected const string A2LC_SECURITY_ACCESS = "SECURITY_ACCESS";
    internal protected const string A2LC_DATA_ACCESS = "DATA_ACCESS";
    internal protected const string A2LC_DATA_SERAM = "SERAM";
    internal protected const string A2LC_BAUD_DEF = "BAUD_DEF";
    internal protected const string A2LC_TIME_DEF = "TIME_DEF";
    internal protected const string A2LC_QP_BLOB = "QP_BLOB";
    internal protected const string A2LC_NETWORK_LIMITS = "NETWORK_LIMITS";
    internal protected const string A2LC_START_STOP = "START_STOP";
    internal protected const string A2LC_COMM_MODE_SUPPORTED = "COMMUNICATION_MODE_SUPPORTED";
    internal protected const string A2LC_MAX_BUS_LOAD = "MAX_BUS_LOAD";
    internal protected const string A2LC_MAX_BIT_RATE = "MAX_BIT_RATE";
    internal protected const string A2LC_IPV6 = "IPV6";
    internal protected const string A2LC_ADDRESS = "ADDRESS";
    internal protected const string A2LC_FIXED = "FIXED";
    internal protected const string A2LC_EVENT = "EVENT";
    internal protected const string A2LC_ODT_ENTRY = "ODT_ENTRY";
    internal protected const string A2LC_FIXED_EVENT_LIST = "FIXED_EVENT_LIST";
    internal protected const string A2LC_VARIABLE = "VARIABLE";
    internal protected const string A2LC_HOST_NAME = "HOST_NAME";
    internal protected const string A2LC_TRANSPORT_LAYER_INSTANCE = "TRANSPORT_LAYER_INSTANCE";
    internal protected const string A2LC_PGMMODE_ABS = "PGM_MODE_ABSOLUTE";
    internal protected const string A2LC_PGMMODE_FUNC = "PGM_MODE_FUNCTIONAL";
    internal protected const string A2LC_PGMMODE_ABS_FUNC = "PGM_MODE_ABSOLUTE_AND_FUNCTIONAL";
    internal protected const string A2LC_MTA_BLOCK_SIZE_ALIGN = "MTA_BLOCK_SIZE_ALIGN";
    internal protected const string A2LC_EXTERNAL_FUNCTION = "EXTERNAL_FUNCTION";
    internal protected const string A2LC_SK_EXT_FUNC = "SEED_AND_KEY_" + A2LC_EXTERNAL_FUNCTION;
    internal protected const string A2LC_MSB_FIRST = "BYTE_ORDER_MSB_FIRST";
    internal protected const string A2LC_MSB_LAST = "BYTE_ORDER_MSB_LAST";
    internal protected const string A2LC_ADR_GRAN_WORD = "ADDRESS_GRANULARITY_WORD";
    internal protected const string A2LC_ADR_GRAN_DWORD = "ADDRESS_GRANULARITY_DWORD";
    internal protected const string A2LC_ADR_GRAN_BYTE = "ADDRESS_GRANULARITY_BYTE";
    internal protected const string A2LC_OPTIONAL_CMD = "OPTIONAL_CMD";
    internal protected const string A2LC_OPTIONAL_TL_SUBCMD = "OPTIONAL_TL_SUBCMD";
    internal protected const string A2LC_CAN_ID_MASTER = "CAN_ID_MASTER";
    internal protected const string A2LC_CAN_ID_SLAVE = "CAN_ID_SLAVE";
    internal protected const string A2LC_CAN_ID_BROADCAST = "CAN_ID_BROADCAST";
    internal protected const string A2LC_CAN_ID_GET_DAQ_CLOCK_MULTICAST = "CAN_ID_GET_DAQ_CLOCK_MULTICAST";
    internal protected const string A2LC_BAUDRATE = "BAUDRATE";
    internal protected const string A2LC_CAN_FD_DATA_TRANSFER_BAUDRATE = "CAN_FD_DATA_TRANSFER_BAUDRATE";
    internal protected const string A2LC_SAMPLE_POINT = "SAMPLE_POINT";
    internal protected const string A2LC_SAMPLE_RATE = "SAMPLE_RATE";
    internal protected const string A2LC_SECONDARY_SAMPLE_POINT = "SECONDARY_SAMPLE_POINT";
    internal protected const string A2LC_MAX_DLC = "MAX_DLC";
    internal protected const string A2LC_BTL_CYCLES = "BTL_CYCLES";
    internal protected const string A2LC_SJW = "SJW";
    internal protected const string A2LC_SYNC_EDGE = "SYNC_EDGE";
    internal protected const string A2LC_MAX_DLC_REQUIRED = "MAX_DLC_REQUIRED";
    internal protected const string A2LC_CAN_ID_MASTER_INCREMENTAL = "CAN_ID_MASTER_INCREMENTAL";
    internal protected const string A2LC_TRANSCEIVER_DELAY_COMPENSATION = "TRANSCEIVER_DELAY_COMPENSATION";
    internal protected const string A2LC_MAX_DTO_STIM = "MAX_DTO_STIM";
    internal protected const string A2LC_DAQ_ALTERNATING_SUPPORTED = "DAQ_ALTERNATING_SUPPORTED";
    internal protected const string A2LC_PRESCALER_SUPPORTED = "PRESCALER_SUPPORTED";
    internal protected const string A2LC_RESUME_SUPPORTED = "RESUME_SUPPORTED";
    internal protected const string A2LC_STORE_DAQ_SUPPORTED = "STORE_DAQ_SUPPORTED";
    internal protected const string A2LC_DTO_CTR_FIELD_SUPPORTED = "DTO_CTR_FIELD_SUPPORTED";
    internal protected const string A2LC_PID_OFF_SUPPORTED = "PID_OFF_SUPPORTED";
    internal protected const string A2LC_MAX_DAQ_TOTAL = "MAX_DAQ_TOTAL";
    internal protected const string A2LC_MAX_ODT_TOTAL = "MAX_ODT_TOTAL";
    internal protected const string A2LC_MAX_ODT_DAQ_TOTAL = "MAX_ODT_DAQ_TOTAL";
    internal protected const string A2LC_MAX_ODT_STIM_TOTAL = "MAX_ODT_STIM_TOTAL";
    internal protected const string A2LC_MAX_ODT_ENTRIES_TOTAL = "MAX_ODT_ENTRIES_TOTAL";
    internal protected const string A2LC_MAX_ODT_ENTRIES_DAQ_TOTAL = "MAX_ODT_ENTRIES_DAQ_TOTAL";
    internal protected const string A2LC_MAX_ODT_ENTRIES_STIM_TOTAL = "MAX_ODT_ENTRIES_STIM_TOTAL";
    internal protected const string A2LC_CPU_LOAD_MAX_TOTAL = "CPU_LOAD_MAX_TOTAL";
    internal protected const string A2LC_CORE_LOAD_MAX_TOTAL = "CORE_LOAD_MAX_TOTAL";
    internal protected const string A2LC_VAR_SEPARATOR = "VAR_SEPARATOR";
    internal protected const string A2LC_VAR_NAMING = "VAR_NAMING";
    internal protected const string A2LC_ADDRESS_TYPE = "ADDRESS_TYPE";
    internal protected const string A2LC_CONSISTENT_EXCHANGE = "CONSISTENT_EXCHANGE";
    internal protected const string A2LC_ENCODING = "ENCODING";
    internal protected const string A2LC_VAR_MEASUREMENT = "VAR_MEASUREMENT";
    internal protected const string A2LC_VAR_SELECTION_CHARACTERISTIC = "VAR_SELECTION_CHARACTERISTIC";
    internal protected const string A2LC_CONVERSION = "CONVERSION";
    internal protected const string A2LC_LIMITS = "LIMITS";
    internal protected const string A2LC_INPUT_QUANTITY = "INPUT_QUANTITY";
    internal protected const string A2LC_STATIC_RECORD_LAYOUT = "STATIC_RECORD_LAYOUT";
    internal protected const string A2LC_STATIC_ADDRESS_OFFSETS = "STATIC_ADDRESS_OFFSETS";
    internal protected const string A2LC_PGM_VERIFY = "PGM_VERIFY";
    internal protected const string A2LC_MAX_ODT = "MAX_ODT";
    internal protected const string A2LC_MAX_ODT_ENTRIES = "MAX_ODT_ENTRIES";
    internal protected const string A2LC_DAQ_LIST_TYPE = "DAQ_LIST_TYPE";
    internal protected const string A2LC_EVENT_FIXED = "EVENT_FIXED";
    internal protected const string A2LC_FIRST_PID = "FIRST_PID";
    internal protected const string A2LC_CAN_ID_VARIABLE = "CAN_ID_VARIABLE";
    internal protected const string A2LC_CAN_ID_FIXED = "CAN_ID_FIXED";
    internal protected const string A2LC_INTERFACE_STRING_DESCRIPTOR = "INTERFACE_STRING_DESCRIPTOR";
    internal protected const string A2LC_ALTERNATE_SETTING_NO = "ALTERNATE_SETTING_NO";
    internal protected const string A2LC_ALT_SAMPLE_COUNT = "ALT_SAMPLE_COUNT";
    internal protected const string A2LC_COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER = "COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER";
    internal protected const string A2LC_CONSISTENCY = "CONSISTENCY";
    internal protected const string A2LC_EVENT_COUNTER_PRESENT = "EVENT_COUNTER_PRESENT";
    internal protected const string A2LC_RELATED_EVENT_CHANNEL_NUMBER = "RELATED_EVENT_CHANNEL_NUMBER";
    internal protected const string A2LC_RELATED_EVENT_CHANNEL_NUMBER_FIXED = "RELATED_EVENT_CHANNEL_NUMBER_FIXED";
    internal protected const string A2LC_DTO_CTR_DAQ_MODE = "DTO_CTR_DAQ_MODE";
    internal protected const string A2LC_DTO_CTR_DAQ_MODE_FIXED = A2LC_DTO_CTR_DAQ_MODE + "_FIXED";
    internal protected const string A2LC_DTO_CTR_STIM_MODE = "DTO_CTR_STIM_MODE";
    internal protected const string A2LC_DTO_CTR_STIM_MODE_FIXED = A2LC_DTO_CTR_STIM_MODE + "_FIXED";
    internal protected const string A2LC_STIM_DTO_CTR_COPY_PRESENT = "STIM_DTO_CTR_COPY_PRESENT";
    internal protected const string A2LC_CPU_LOAD_MAX = "CPU_LOAD_MAX";
    internal protected const string A2LC_DEFAULT_PAGE_NUMBER = "DEFAULT_PAGE_NUMBER";
    internal protected const string A2LC_BIT_STIM_SUPPORTED = "BIT_STIM_SUPPORTED";
    internal protected const string A2LC_MIN_ST_STIM = "MIN_ST_STIM";
    internal protected const string A2LC_DAQ_TIMESTAMPS_RELATE_TO = "DAQ_TIMESTAMPS_RELATE_TO";
    internal protected const string A2LC_RECOMMENDED_HOST_BUFSIZE = "RECOMMENDED_HOST_BUFSIZE";
    internal protected const string A2LC_TIMESTAMP_FIXED = "TIMESTAMP_FIXED";
    #endregion
    protected const string mStrHexByteMax = "0xFF";
    protected const string mStrHexWordMax = "0xFFFF";
    protected const string mStrHexDWordMax = "0xFFFFFFFF";
    protected const string mStrHexQWordMax = "0xFFFFFFFFFFFFFFFF";
    protected const string mStrHexQWordMin = "0x0000000000000000";
    protected const string mTrimStr = "...";
    const int mMaxTTText = 0x400;
    public const int mFilterLine = int.MaxValue - 10;
    /// <summary>Mark for no input measurement on axis</summary>
    internal const string mNoInputQuantity = "NO_INPUT_QUANTITY";
    /// <summary>Mark for undefined inverse transformer</summary>
    internal const string mNoInverseTransformer = "NO_INVERSE_TRANSFORMER";
    /// <summary>Mark for default conversion</summary>
    internal const string mNoCompuMethod = "NO_COMPU_METHOD";
    internal protected const string mMappedAlignDesc = "The alignments mapped into indizes: BYTE=0, WORD=1, LONG=2, FLOAT32_IEEE=3, FLOAT64_IEEE=4, INT64=5.";
    bool mChanged;
    #endregion
    #region constructor
    /// <summary>
    /// Initialises a new instance of A2LNODE with the specified parameters.
    /// </summary>
    /// <param name="startLine">The starting line</param>
    protected A2LNODE(int startLine) { StartLine = startLine; }
    #endregion
    #region properties
    /// <summary>
    /// Gets the type of the Node.
    /// </summary>
    [Category(Constants.CATData), Description("The type of the ASAP2 node."), ReadOnly(true)]
    public A2LType Type { get; set; }
    /// <summary>
    /// Gets the starting line of the Node within the source file.
    /// 
    /// If the startline is not valid (no text content reference), the value is int.MaxValue.
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation), DisplayName("First line"), Description("The first line in the original file.")]
    public int StartLine { get; }
    /// <summary>
    /// Gets the ending line of the Node within the source file.
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation), ReadOnly(true), DisplayName("Last line"), Description("The last line in the original file.")]
    public int EndLine { get; internal protected set; }
    /// <summary>
    /// Gets the name of the node.
    /// </summary>
    [XmlIgnore, ReadOnly(true), Category(Constants.CATData)]
    public virtual string Name { get { return Type.ToString(); } set { } }
    /// <summary>
    /// Gets or sets the Tag property.
    /// 
    /// - This property may be used to bind any user reference to this instance.
    /// - XMLExport: Never export this (maybe an unknown type) instance!
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation), Description(Constants.DESCTagProperty), TypeConverter(typeof(ExpandableObjectConverter))]
    public object Tag { get; set; }
    /// <summary>
    /// Indicates if the node is created or changed within the object model.
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation), ReadOnly(true), DefaultValue(false), Description("Indicates if the node is created or changed in the object model.")]
    public bool Changed { get { return mChanged || StartLine == int.MaxValue; } set { mChanged = value; } }
    /// <summary>
    /// The direct parent of the node instance.
    /// 
    /// - For the root node (A2LPROJECT) this member is always null.
    /// - For any other node instances, this value always leads to 
    /// the direct parent instance.
    /// - XMLExport: Never export this instance!
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation), Description("The direct parent node within the ASAP2 hierarchy.")]
    public A2LNODE Parent { get; internal protected set; }
    /// <summary>
    /// Gets or sets the list of (direct) child nodes.
    /// </summary>
    [Category(Constants.CATNavigation), Description("The list of direct child node's within the ASAP2 hierarchy.")]
    public A2LNodeList Childs { get; internal protected set; }
    /// <summary>
    /// Indicates, if the object is instanced by an A2LINSTANCE.
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation)]
    public virtual bool IsInstanced { get { return false; } }
    #endregion
    #region methods
    /// <summary>
    /// Parses a string into a enum value.
    /// </summary>
    /// <typeparam name="T">Enum T to parse into</typeparam>
    /// <param name="par">parameter value to parse</param>
    /// <param name="parser">The parser object for notifiying messages</param>
    /// <param name="preventMsg">Prevents parser from creating a message</param>
    /// <returns>Parsed value or default(EnumType)</returns>
    protected T parse2Enum<T>(string par, A2LParser parser, bool preventMsg = false) where T : struct
    {
      T result;
      if (!Enum.TryParse(par, !Settings.Default.StrictParsing, out result) && !preventMsg)
        parser.onParserMessage(new ParserEventArgs(this, MessageType.Error, string.Format(Resources.strInvalidEnum, par, typeof(T).Name)));
      return result;
    }
    /// <summary>
    /// Adds a child node to the node.
    /// </summary>
    /// <param name="childNode">The node to add</param>
    public void addChild(A2LNODE childNode)
    {
      if (Childs == null)
        Childs = new A2LNodeList();
      Childs.Add(childNode);
      childNode.Parent = this;
    }
    /// <summary>
    /// Creates an unsupported node and a cotrresponding message.
    /// </summary>
    /// <param name="parser">The parser instance</param>
    /// <param name="parent">The parent node</param>
    /// <param name="startLine">The start line in the content</param>
    /// <param name="a2lType">The a2l type string</param>
    /// <returns>The unsupported node instance</returns>
    static A2LNODE createUnsupportedNode(A2LParser parser, A2LNODE parent, int startLine, string a2lType)
    {
      var node = new A2LUNSUPPORTEDNODE(startLine, a2lType);
      if (parent != null && parent.Type == A2LType.UNSUPPORTED)
        return node;
      parser.onParserMessage(new ParserEventArgs(node
        , MessageType.Info
        , string.Format(Resources.strCreateUnsupportedNode, parent == null ? "root" : parent.getContext('/'))
        ));
      return node;
    }
    /// <summary>
    /// The A2L type factory.
    /// </summary>
    /// <param name="parser">The parser object</param>
    /// <param name="parent">The parent of the type instance</param>
    /// <param name="a2lType">The target type</param>
    /// <param name="startLine">The start line within the file</param>
    /// <param name="inIFData">true, if parser state is within an IF_DATA block</param>
    /// <returns>always a new child</returns>
    /// <exception cref="ApplicationException">Thrown, if a known A2L type is not handled</exception>
    internal static A2LNODE create(A2LParser parser
      , A2LNODE parent
      , string a2lType
      , int startLine
      , bool inIFData
      )
    {
      A2LNODE child = null;

      if (inIFData)
      { // if data
        var ifDataParent = parent is A2LIF_DATA
          ? (A2LIF_DATA)parent
          : parent.getParent<A2LIF_DATA>();
        A2LType ifMediaType;
        if (Enum.TryParse(a2lType, !Settings.Default.StrictParsing, out ifMediaType))
        {
          bool xcpChild = ifDataParent.Name == "XCP" || ifDataParent.Name == "XCPplus";
          bool ccpChild = ifDataParent.Name == "ASAP1B_CCP";
          bool kwpChild = ifDataParent.Name == "ASAP1B_KWP2000";
          if (xcpChild || ccpChild)
          {
            switch (ifMediaType)
            {
              #region XCP support
              case A2LType.SEGMENT: child = new A2LXCP_SEGMENT(startLine); child.Type = ifMediaType; break;
              case A2LType.CHECKSUM: child = new A2LXCP_CHECKSUM(startLine); child.Type = ifMediaType; break;
              case A2LType.PAGE: child = new A2LXCP_PAGE(startLine); child.Type = ifMediaType; break;
              case A2LType.ADDRESS_MAPPING: child = new A2LXCP_ADDRESS_MAPPING(startLine); child.Type = ifMediaType; break;
              case A2LType.PAG: child = new A2LXCP_PAG(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ: child = new A2LXCP_DAQ(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_LIST: child = new A2LXCP_DAQ_LIST(startLine); child.Type = ifMediaType; break;
              case A2LType.EVENT: child = new A2LXCP_EVENT(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_EVENT: child = new A2LXCP_DAQ_EVENT(startLine); child.Type = ifMediaType; break;
              case A2LType.AVAILABLE_EVENT_LIST: child = new A2LXCP_EVENT_LIST(startLine); child.Type = ifMediaType; break;
              case A2LType.DEFAULT_EVENT_LIST: child = new A2LXCP_EVENT_LIST(startLine); child.Type = ifMediaType; break;
              case A2LType.CONSISTENCY_EVENT_LIST: child = new A2LXCP_EVENT_LIST(startLine); child.Type = ifMediaType; break;
              case A2LType.STIM: child = new A2LXCP_STIM(startLine); child.Type = ifMediaType; break;
              case A2LType.XCP_ON_TCP_IP: child = new A2LXCP_ON_TCP_IP(startLine); child.Type = ifMediaType; break;
              case A2LType.XCP_ON_UDP_IP: child = new A2LXCP_ON_UDP_IP(startLine); child.Type = ifMediaType; break;
              case A2LType.XCP_ON_CAN: child = new A2LXCP_ON_CAN(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_LIST_CAN_ID: child = new A2LXCP_DAQ_LIST_CAN_ID(startLine); child.Type = ifMediaType; break;
              case A2LType.PROTOCOL_LAYER: child = new A2LXCP_PROTOCOL_LAYER(startLine); child.Type = ifMediaType; break;
              case A2LType.TIMESTAMP_SUPPORTED: child = new A2LXCP_TIMESTAMP_SUPPORTED(startLine); child.Type = ifMediaType; break;
              case A2LType.PGM: child = new A2LXCP_PGM(startLine); child.Type = ifMediaType; break;
              case A2LType.SECTOR: child = new A2LXCP_SECTOR(startLine); child.Type = ifMediaType; break;
              case A2LType.EVENT_CAN_ID_LIST: child = new A2LXCP_EVENT_CAN_ID_LIST(startLine); child.Type = ifMediaType; break;
              case A2LType.CAN_FD: child = new A2LXCP_CAN_FD(startLine); child.Type = ifMediaType; break;
              case A2LType.TIME_CORRELATION: child = new A2LXCP_TIME_CORRELATION(startLine); child.Type = ifMediaType; break;
              case A2LType.CLOCK: child = new A2LXCP_CLOCK(startLine); child.Type = ifMediaType; break;
              case A2LType.TIMESTAMP_CHARACTERIZATION: child = new A2LXCP_TIMESTAMP_CHARACTERIZATION(startLine); child.Type = ifMediaType; break;
              case A2LType.PREDEFINED:
              case A2LType.ECU_STATES: child = new A2LXCP_NODE(startLine); child.Type = ifMediaType; break;
              case A2LType.ODT: child = new A2LXCP_ODT(startLine); child.Type = ifMediaType; break;
              case A2LType.STATE: child = new A2LXCP_ECU_STATES_STATE(startLine); child.Type = ifMediaType; break;
              case A2LType.MEMORY_ACCESS: child = new A2LXCP_ECU_STATES_MEMORY_ACCESS(startLine); child.Type = ifMediaType; break;
              case A2LType.ODT_ENTRY_SIZE_FACTOR_TABLE: child = new A2LXCP_EVENT_ODT_ENTRY_SIZE_FACTOR_TABLE(startLine); child.Type = ifMediaType; break;
              case A2LType.CORE_LOAD_MAX:
              case A2LType.MIN_CYCLE_TIME: child = new A2LXCP_EVENT_MIN_CYCLE_TIME(startLine); child.Type = ifMediaType; break;
              case A2LType.BUFFER_RESERVE_EVENT: child = new A2LXCP_BUFFER_RESERVE(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_MEMORY_CONSUMPTION: child = new A2LXCP_DAQ_MEMORY_CONSUMPTION(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_PACKED_MODE: child = new A2LXCP_EVENT_DAQ_PACKED_MODE(startLine); child.Type = ifMediaType; break;
              case A2LType.CPU_LOAD_CONSUMPTION_DAQ:
              case A2LType.CPU_LOAD_CONSUMPTION_STIM: child = new A2LXCP_EVENT_CPU_LOAD_CONSUMPTION(startLine); child.Type = ifMediaType; break;
              case A2LType.CPU_LOAD_CONSUMPTION_QUEUE:
              case A2LType.CPU_LOAD_CONSUMPTION_QUEUE_STIM: child = new A2LXCP_EVENT_CPU_LOAD_CONSUMPTION_QUEUE(startLine); child.Type = ifMediaType; break;
              case A2LType.CORE_LOAD_EP: child = new A2LXCP_CORE_LOAD(startLine); child.Type = ifMediaType; break;

              #region USB support
              case A2LType.XCP_ON_USB: child = new A2LXCP_ON_USB(startLine); child.Type = ifMediaType; break;
              case A2LType.DAQ_LIST_USB_ENDPOINT: child = new A2LXCP_DAQ_LIST_USB_ENDPOINT(startLine); child.Type = ifMediaType; break;
              case A2LType.OUT_EP_CMD_STIM: child = new A2LXCP_OUT_EP_CMD_STIM(startLine); child.Type = ifMediaType; break;
              case A2LType.OUT_EP_ONLY_STIM: child = new A2LXCP_OUT_EP_ONLY_STIM(startLine); child.Type = ifMediaType; break;
              case A2LType.IN_EP_ONLY_DAQ: child = new A2LXCP_IN_EP_ONLY_DAQ(startLine); child.Type = ifMediaType; break;
              case A2LType.IN_EP_ONLY_EVSERV: child = new A2LXCP_IN_EP_ONLY_EVSERV(startLine); child.Type = ifMediaType; break;
              case A2LType.IN_EP_RESERR_DAQ_EVSERV: child = new A2LXCP_IN_EP_RESERR_DAQ_EVSERV(startLine); child.Type = ifMediaType; break;
              #endregion
              #endregion
              #region ASAP1B_CCP support
              case A2LType.TP_BLOB: child = new A2LCCP_TP_BLOB(startLine); child.Type = ifMediaType; break;
              case A2LType.DEFINED_PAGES: child = new A2LCCP_DEFINED_PAGES(startLine); child.Type = ifMediaType; break;
              case A2LType.CAN_PARAM: child = new A2LCCP_CAN_PARAM(startLine); child.Type = ifMediaType; break;
              case A2LType.CHECKSUM_PARAM: child = new A2LCCP_CHECKSUM_PARAM(startLine); child.Type = ifMediaType; break;
              case A2LType.SEED_KEY: child = new A2LCCP_SEED_KEY(startLine); child.Type = ifMediaType; break;
              case A2LType.SOURCE: child = new A2LCCP_SOURCE(startLine); child.Type = ifMediaType; break;
              case A2LType.RASTER: child = new A2LCCP_RASTER(startLine); child.Type = ifMediaType; break;
              case A2LType.EVENT_GROUP: child = new A2LCCP_EVENT_GROUP(startLine); child.Type = ifMediaType; break;
              case A2LType.QP_BLOB: child = new A2LCCP_QP_BLOB(startLine); child.Type = ifMediaType; break;
              #endregion
              default:
                child = createUnsupportedNode(parser, parent, startLine, a2lType);
                break;
            }
          }
          #region ASAP1B_KWP support
          else if (kwpChild)
          {
            switch (ifMediaType)
            {
              case A2LType.ROUTINE_PARA: child = new A2LKWP_ROUTINE_PARA(startLine); child.Type = ifMediaType; break;
              case A2LType.PAGE_SWITCH: child = new A2LKWP_PAGE_SWITCH(startLine); child.Type = ifMediaType; break;
              case A2LType.DIAG_BAUD: child = new A2LKWP_DIAG_BAUD(startLine); child.Type = ifMediaType; break;
              case A2LType.CHECKSUM: child = new A2LKWP_CHECKSUM(startLine); child.Type = ifMediaType; break;
              case A2LType.FLASH_COPY: child = new A2LKWP_FLASH_COPY(startLine); child.Type = ifMediaType; break;
              case A2LType.FLASH: child = new A2LKWP_FLASH(startLine); child.Type = ifMediaType; break;
              case A2LType.COPY: child = new A2LKWP_COPY(startLine); child.Type = ifMediaType; break;
              case A2LType.TIME_DEF: child = new A2LKWP_TIME_DEF(startLine); child.Type = ifMediaType; break;
              case A2LType.ADDRESS: child = new A2LKWP_ADDRESS(startLine); child.Type = ifMediaType; break;
              case A2LType.CAN: child = new A2LKWP_CAN(startLine); child.Type = ifMediaType; break;
              case A2LType.K_LINE: child = new A2LKWP_K_LINE(startLine); child.Type = ifMediaType; break;
              case A2LType.SOURCE: child = new A2LKWP_SOURCE(startLine); child.Type = ifMediaType; break;
              case A2LType.TP_BLOB: child = new A2LKWP_TP_BLOB(startLine); child.Type = ifMediaType; break;
            }
          }
          #endregion
        }
        else
          // not parsed -> create a standard IF_DATA_CHILD instance
          child = createUnsupportedNode(parser, parent, startLine, a2lType);

        if (child == null)
          child = createUnsupportedNode(parser, parent, startLine, a2lType);
        parent.addChild(child);
        return child;
      }

      // recognize the A2L type
      var type = A2LType.UNSUPPORTED;
      if (Enum.TryParse(a2lType, out type))
      {
        // supported A2L type
        switch (type)
        {
          case A2LType.MEASUREMENT: child = new A2LMEASUREMENT(startLine); break;
          case A2LType.CHARACTERISTIC: child = new A2LCHARACTERISTIC(startLine); break;
          case A2LType.ANNOTATION: child = new A2LANNOTATION(startLine); break;
          case A2LType.ANNOTATION_TEXT: child = new A2LANNOTATION_TEXT(startLine); break;
          case A2LType.AXIS_PTS: child = new A2LAXIS_PTS(startLine); break;
          case A2LType.AXIS_DESCR: child = new A2LAXIS_DESCR(startLine); break;
          case A2LType.RECORD_LAYOUT: child = new A2LRECORD_LAYOUT(startLine); break;
          case A2LType.FUNCTION: child = new A2LFUNCTION(startLine); break;
          case A2LType.UNIT: child = new A2LUNIT(startLine); break;
          case A2LType.DEF_CHARACTERISTIC: child = new A2LDEF_CHARACTERISTIC(startLine); break;
          case A2LType.REF_CHARACTERISTIC: child = new A2LREF_CHARACTERISTIC(startLine); break;
          case A2LType.REF_MEASUREMENT: child = new A2LREF_MEASUREMENT(startLine); break;
          case A2LType.REF_GROUP: child = new A2LREF_GROUP(startLine); break;
          case A2LType.IN_MEASUREMENT: child = new A2LIN_MEASUREMENT(startLine); break;
          case A2LType.OUT_MEASUREMENT: child = new A2LOUT_MEASUREMENT(startLine); break;
          case A2LType.LOC_MEASUREMENT: child = new A2LLOC_MEASUREMENT(startLine); break;
          case A2LType.SUB_FUNCTION: child = new A2LSUB_FUNCTION(startLine); break;
          case A2LType.COMPU_METHOD: child = new A2LCOMPU_METHOD(startLine); break;
          case A2LType.COMPU_VTAB: child = new A2LCOMPU_VTAB(startLine); break;
          case A2LType.COMPU_VTAB_RANGE: child = new A2LCOMPU_VTAB_RANGE(startLine); break;
          case A2LType.COMPU_TAB: child = new A2LCOMPU_TAB(startLine); break;
          case A2LType.MEMORY_LAYOUT: child = new A2LMEMORY_LAYOUT(startLine); break;
          case A2LType.CALIBRATION_METHOD: child = new A2LCALIBRATION_METHOD(startLine); break;
          case A2LType.CALIBRATION_HANDLE: child = new A2LCALIBRATION_HANDLE(startLine); break;
          case A2LType.MEMORY_SEGMENT: child = new A2LMEMORY_SEGMENT(startLine); break;
          case A2LType.PROJECT: child = new A2LPROJECT(startLine); break;
          case A2LType.HEADER: child = new A2LHEADER(startLine); break;
          case A2LType.MODULE: child = new A2LMODULE(startLine); break;
          case A2LType.A2ML: child = new A2LA2ML(startLine); break;
          case A2LType.MOD_PAR: child = new A2LMODPAR(startLine); break;
          case A2LType.MOD_COMMON: child = new A2LMOD_COMMON(startLine); break;
          case A2LType.DEPENDENT_CHARACTERISTIC: child = new A2LDEPENDENT_CHARACTERISTIC(startLine); break;
          case A2LType.VIRTUAL_CHARACTERISTIC: child = new A2LVIRTUAL_CHARACTERISTIC(startLine); break;
          case A2LType.VIRTUAL: child = new A2LVIRTUAL(startLine); break;
          case A2LType.VAR_CRITERION: child = new A2LVAR_CRITERION(startLine); break;
          case A2LType.VAR_CHARACTERISTIC: child = new A2LVAR_CHARACTERISTIC(startLine); break;
          case A2LType.VAR_ADDRESS: child = new A2LVAR_ADDRESS(startLine); break;
          case A2LType.VAR_FORBIDDEN_COMB: child = new A2LVAR_FORBIDDEN_COMB(startLine); break;
          case A2LType.USER_RIGHTS: child = new A2LUSER_RIGHTS(startLine); break;
          case A2LType.GROUP: child = new A2LGROUP(startLine); break;
          case A2LType.SUB_GROUP: child = new A2LSUB_GROUP(startLine); break;
          case A2LType.FUNCTION_LIST: child = new A2LFUNCTION_LIST(startLine); break;
          case A2LType.FORMULA: child = new A2LFORMULA(startLine); break;
          case A2LType.FIX_AXIS_PAR_LIST: child = new A2LFIX_AXIS_PAR_LIST(startLine); break;
          case A2LType.FRAME: child = new A2LFRAME(startLine); break;
          case A2LType.BIT_OPERATION: child = new A2LBIT_OPERATION(startLine); break;
          case A2LType.VARIANT_CODING: child = new A2LVARIANT_CODING(startLine); break;
          case A2LType.IF_DATA: child = new A2LIF_DATA(startLine); break;
          case A2LType.TYPEDEF_STRUCTURE: child = new A2LTYPEDEF_STRUCTURE(startLine); break;
          case A2LType.STRUCTURE_COMPONENT: child = new A2LSTRUCTURE_COMPONENT(startLine); break;
          case A2LType.TYPEDEF_BLOB: child = new A2LTYPEDEF_BLOB(startLine); break;
          case A2LType.TYPEDEF_MEASUREMENT: child = new A2LTYPEDEF_MEASUREMENT(startLine); break;
          case A2LType.TYPEDEF_AXIS: child = new A2LTYPEDEF_AXIS(startLine); break;
          case A2LType.TYPEDEF_CHARACTERISTIC: child = new A2LTYPEDEF_CHARCTERISTIC(startLine); break;
          case A2LType.BLOB: child = new A2LBLOB(startLine); break;
          case A2LType.INSTANCE: child = new A2LINSTANCE(startLine); break;
          case A2LType.OVERWRITE: child = new A2LOVERWRITE(startLine); break;
          case A2LType.TRANSFORMER: child = new A2LTRANSFORMER(startLine); break;
          case A2LType.TRANSFORMER_IN_OBJECTS: child = new A2LTRANSFORMER_IN_OBJECTS(startLine); break;
          case A2LType.TRANSFORMER_OUT_OBJECTS: child = new A2LTRANSFORMER_OUT_OBJECTS(startLine); break;
        }
      }
      else
        // A2L type is unsupported (e.g. a file supports a newer specification)
        child = createUnsupportedNode(parser, parent, startLine, a2lType);
      child.Type = type;
      if (null != parent)
        parent.addChild(child);
      return child;
    }
    /// <summary>
    /// Searches for the specified node name starting from the calling instance.
    /// 
    /// - The search is case sensitive.
    /// - The calling instance is included in the search.
    /// - The search is recursive over all child nodes
    /// </summary>
    /// <typeparam name="T">The A2LNODE type to search to</typeparam>
    /// <param name="name">The node name to search</param>
    /// <returns>The instance found or null</returns>
    public T findNode<T>(string name) where T : A2LNODE
    {
      return findNode<T>(name, StringComparison.Ordinal);
    }
    /// <summary>
    /// Searches for the specified node name starting from the calling instance.
    /// 
    /// - The calling instance is included in the search.
    /// - The search is recursive over all child nodes
    /// </summary>
    /// <typeparam name="T">The A2LNODE type to search to</typeparam>
    /// <param name="name">The node name to search</param>
    /// <param name="comparison">the comparison type to use</param>
    /// <returns>The instance found or null</returns>
    public T findNode<T>(string name, StringComparison comparison) where T : A2LNODE
    {
      if (this is T && Name.Equals(name, comparison))
        // it is the calling instance
        return this as T;

      foreach (var currentNode in enumChildNodes<T>())
      { // enuemrate all childs
        if (!currentNode.Name.Equals(name, comparison))
          continue;
        // node found
        return currentNode;
      }
      // nothing found
      return null;
    }
    /// <summary>
    /// Gets the first node of the specified type found in the underlying childs.
    /// </summary>
    /// <example>
    /// Example code:
    /// <code>
    /// // get the first a2l measurement from the first a2l module
    /// using (A2LParser a2lParser = new A2LParser())
    /// {
    ///   a2lParser.parse("file.a2l");
    ///   A2LMEASUREMENT a2lMeasurement = a2lParser.Project.getNode<A2LMODULE>(false).getNode<A2LMEASUREMENT>(false);
    ///   Console.WriteLine("First measurement: {0}", a2lMeasurement.Name);
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">Type of element to be returned</typeparam>
    /// <param name="recursive">True if the child nodes should be searched recursively</param>
    /// <returns>a node instance of the specified type or null if no one exists</returns>
    public T getNode<T>(bool recursive) where T : A2LNODE
    {
      if (Childs == null)
        // no child at all
        return null;

      for (int i = 0; i < Childs.Count; ++i)
      { // iterate over all childs
        var childNode = Childs[i];
        if (childNode is T)
          return (T)childNode;
        if (!recursive)
          continue;
        T foundNode = childNode.getNode<T>(recursive);
        if (null != foundNode)
          // found recursively
          return foundNode;
      }
      // nothing found
      return null;
    }
    /// <summary>
    /// Returns a list of child nodes of the specified type <b>(non-recursive!)</b>. 
    /// </summary>
    /// <example>
    /// Example code:
    /// <code>
    /// // get all CompuMethods from the first a2l module
    /// using (A2LParser a2lParser = new A2LParser())
    /// {
    ///   a2lParser.parse("file.a2l");
    ///   List<A2LCOMPU_METHOD> a2lCompuMethods = a2lParser.Project.getNode<A2LMODULE>(false).getNodeList<A2LCOMPU_METHOD>();
    ///   foreach ( A2LCOMPU_METHOD method in a2lCompuMethods)
    ///     Console.WriteLine("CompuMethod: {0}", method.Name);
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">Type of elements to be returned.</typeparam>
    /// <returns>List of found elements (may be empty).</returns>
    public List<T> getNodeList<T>() where T : A2LNODE
    {
      return getNodeList<T>(false);
    }
    /// <summary>
    /// Returns a list of child nodes of the specified type <b>(non-recursive!)</b>. 
    /// </summary>
    /// <example>
    /// Example code:
    /// <code>
    /// // get all CompuMethods from the first a2l module
    /// using (A2LParser a2lParser = new A2LParser())
    /// {
    ///   a2lParser.parse("file.a2l");
    ///   List<A2LCOMPU_METHOD> a2lCompuMethods = a2lParser.Project.getNode<A2LMODULE>(false).getNodeList<A2LCOMPU_METHOD>();
    ///   foreach ( A2LCOMPU_METHOD method in a2lCompuMethods)
    ///     Console.WriteLine("CompuMethod: {0}", method.Name);
    /// }
    /// </code>
    /// </example>
    /// <param name="recursive">True if the child nodes should be searched recursively</param>
    /// <typeparam name="T">Type of elements to be returned.</typeparam>
    /// <returns>List of found elements (may be empty).</returns>
    public List<T> getNodeList<T>(bool recursive) where T : A2LNODE
    {
      var nodeList = new List<T>();
      if (null == Childs)
        // no child at all
        return nodeList;

      for (int i = 0; i < Childs.Count; ++i)
      { // recursively over all childs
        var childNode = Childs[i];
        if (childNode is T)
          nodeList.Add((T)childNode);

        if (!recursive)
          // not the searched type
          continue;

        // hit and add
        nodeList.AddRange(childNode.getNodeList<T>(recursive));
      }
      return nodeList;
    }
    /// <summary>
    /// Enumerates over all child nodes corresponding to the specified type from the calling instance.
    /// 
    /// - The search is recursive over all child nodes
    /// </summary>
    /// <example>
    /// Example code:
    /// <code>
    /// // enumerating measurements over the whole project
    /// using (A2LParser a2lParser = new A2LParser())
    /// {
    ///   a2lParser.parse("file.a2l");
    ///   foreach(A2LMEASUREMENT measurement in parser.Project.enumChildNodes&lt;A2LMEASUREMENT&gt;())
    ///     Console.WriteLine("Measurement: {0}", measurement.Name);
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">The node type to search</typeparam>
    /// <returns>An enumerable instance</returns>
    public IEnumerable<T> enumChildNodes<T>() where T : A2LNODE
    {
      if (Childs != null)
      {
        foreach (var child in Childs)
        { // Drill down recursively
          var directChild = child as T;
          if (directChild != null)
            // yield --> found direct child
            yield return directChild;
          // search subchilds
          foreach (var subChild in child.enumChildNodes<T>())
            // yield --> found in subchilds
            yield return subChild;
        }
      }
    }
    /// <summary>
    /// Gets the corresponding parent with the specified type from the current instance.
    /// 
    /// - Steps up the hierarchy until the corresponding parentType is found, else returns null.
    /// </summary>
    /// <example>
    /// Example code:
    /// <code>
    /// using (A2LParser a2lParser = new A2LParser())
    /// {
    ///   a2lParser.parse("file.a2l");
    ///   A2LMEASUREMENT a2lMeasurement = a2lParser.Project.getNode<A2LMODULE>(false).getNode<A2LMEASUREMENT>(false);
    ///   Console.WriteLine("Parent module: {0}", a2lMeasurement.getParent<A2LMODULE>().Name);
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">The node type to search</typeparam>
    /// <returns>a found parent or null</returns>
    public T getParent<T>() where T : A2LNODE
    {
      var parent = Parent;
      while (null != parent && !(parent is T))
        parent = parent.Parent;
      // not found
      return parent as T;
    }
    /// <summary>
    /// Sorts the nodes.
    /// 
    /// - Sort according the node's address
    /// </summary>
    /// <param name="x">first node</param>
    /// <param name="y">second node</param>
    /// <returns>See CompareTo return value</returns>
    public static int sortByAddress(A2LNODE x, A2LNODE y)
    {
      if (x == y)
        return 0;
      // same type
      UInt32 xAddress = UInt32.MaxValue, yAddress = UInt32.MaxValue;
      if (x is A2LCONVERSION_REF)
        xAddress = ((A2LCONVERSION_REF)x).Address;
      if (y is A2LCONVERSION_REF)
        yAddress = ((A2LCONVERSION_REF)y).Address;
      return xAddress.CompareTo(yAddress);
    }
    /// <summary>
    /// Sorts the nodes for output.
    /// 
    /// - Sort according the node's name
    /// </summary>
    /// <param name="x">first node</param>
    /// <param name="y">second node</param>
    /// <returns>See CompareTo return value</returns>
    public static int sortByName(A2LNODE x, A2LNODE y)
    {
      return string.Compare(x.Name, y.Name, true);
    }
    /// <summary>
    /// Sorts the nodes for output.
    /// 
    /// - Sort according the node's type
    /// - Sort A2LCHARACTERISTIC according it's type.
    /// - Sort according the node's name
    /// </summary>
    /// <param name="x">first node</param>
    /// <param name="y">second node</param>
    /// <returns>See CompareTo return value</returns>
    public static int sortByTypeAndName(A2LNODE x, A2LNODE y)
    {
      if (x == y)
        return 0;
      int result = x.Type.CompareTo(y.Type);
      if (result != 0)
        return result;
      if (x.Type == A2LType.CHARACTERISTIC && x.Type == A2LType.CHARACTERISTIC)
      {
        var xc = (A2LCHARACTERISTIC)x;
        var yc = (A2LCHARACTERISTIC)y;
        result = xc.CharType.CompareTo(yc.CharType);
        if (result != 0)
          return result;
      }
      // same type, sort by name
      return sortByName(x, y);
    }
    /// <summary>
    /// Sorts the nodes for output.
    /// 
    /// - Sort according the node's address
    /// - Sort according the node's name
    /// </summary>
    /// <param name="x">first node</param>
    /// <param name="y">second node</param>
    /// <returns>See CompareTo return value</returns>
    public static int sortByAddressAndName(A2LNODE x, A2LNODE y)
    {
      if (x == y)
        return 0;
      int result = sortByAddress(x, y);
      if (result != 0)
        // address difference
        return result;
      // same Address, sort by name
      return sortByName(x, y);
    }
    /// <summary>
    /// Called when the parameter list is complete.
    /// </summary>
    /// <param name="parser">The parser object for notifiying messages</param>
    /// <param name="parameterList">The parameter list</param>
    /// <param name="startIndex">The index into the parameter list to start from</param>
    /// <returns>true if successful</returns>
    internal abstract bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex);
    /// <summary>
    /// Gets available compu methods from the compu method dictionary.
    /// </summary>
    /// <param name="compuMethodDict"></param>
    /// <param name="conversionName">The name of the compu method</param>
    /// <param name="compuMethod">[out] The copu method found</param>
    static protected void getCompuMethod(A2LCompDict compuMethodDict, string conversionName, out A2LCOMPU_METHOD compuMethod)
    {
      if (string.IsNullOrEmpty(conversionName) || conversionName == mNoCompuMethod)
      { // NO_COMPU_METHOD
        compuMethod = A2LCOMPU_METHOD.mDefaultCompuMethod;
        return;
      }
      compuMethodDict.TryGetValue(conversionName, out compuMethod);
      if (compuMethod == null)
        // undefined compuMethod (wrong conversion reference or undefefined)
        compuMethod = A2LCOMPU_METHOD.mReplaceCompuMethod;
    }
    /// <summary>
    /// Adds a mximum of 1024 characters to the specified string builder.
    /// 
    /// appends ... for oversized text.
    /// </summary>
    /// <param name="sb">The string builder to add the text to</param>
    /// <param name="text">The text to add</param>
    protected static void addMaxText(StringBuilder sb, string text)
    {
      if (!string.IsNullOrEmpty(text))
      {
        string addText = text;
        sb.Append(text.Substring(0, Math.Min(addText.Length, mMaxTTText)));
        if (addText.Length > mMaxTTText)
          sb.Append(mTrimStr);
      }
    }
    /// <summary>
    /// Creates a description from the specified description and a potential existing annotation.
    /// </summary>
    /// <param name="description"></param>
    /// <param name="addUserObjects">true if user objects should be added (if available)</param>
    /// <param name="maxLines">The maximum lines the return string may contain</param>
    /// <returns>The combined description/annotation string (may be empty)</returns>
    protected string getDescriptionAndAnnotation(string description, bool addUserObjects, int maxLines)
    {
      var sb = new StringBuilder();
      try
      {
        var trimmedDescription = string.IsNullOrEmpty(description) ? string.Empty : description.Trim();
        if (!string.IsNullOrEmpty(trimmedDescription))
          sb.Append($"\n{trimmedDescription}");

        var project = getParent<A2LPROJECT>();
        if (project != null)
        {
          string s = project.RaiseGetAdditiveDescription(this);
          if (!string.IsNullOrEmpty(s))
            sb.Append($"\n\n{s}");
        }

        var annotation = getNode<A2LANNOTATION>(false);
        if (annotation != null)
        {
          if (!string.IsNullOrEmpty(annotation.Label))
          {
            string label = annotation.Label;
            sb.Append($"\n\n{Resources.strAnnotationLabel}\n");
            addMaxText(sb, label);
          }
          var annotationText = annotation.getNode<A2LANNOTATION_TEXT>(false);
          if (annotationText != null)
          {
            string text = Extensions.getTrimmedText(annotationText.Text, 80);
            if (!string.IsNullOrEmpty(text))
            {
              sb.Append($"\n\n{Resources.strAnnotationText}\n");
              addMaxText(sb, text);
            }
          }
        }
        var layoutRef = this as A2LRECORD_LAYOUT_REF;
        if (layoutRef != null)
        { // try to get the containing memory segment
          var memory = layoutRef.getMemorySegment();
          if (null != memory)
            sb.Append($"\n\nContained in:\n{memory.toUserDescriptrion()}");
        }

        if (addUserObjects && Tag != null)
        { // add user object
          var strValue = Tag.ToString();
          if (string.IsNullOrEmpty(strValue))
            sb.Append("\n\nUserdata: not available");
          else
          {
            sb.Append("\n\nUserdata:\n");
            addMaxText(sb, strValue);
          }
        }

        string result = sb.ToString();
        int indexLF = -1;
        int countLF = 0;
        while (countLF < maxLines)
        {
          int localIndex = result.IndexOf('\n', indexLF + 1);
          if (localIndex == -1)
            // the end
            break;
          indexLF = localIndex;
          ++countLF;
        }
        if (countLF < maxLines)
          return result;
        sb.Length = indexLF + 1;
        sb.Append(mTrimStr);
      }
      catch { }
      return sb.ToString();
    }
    /// <summary>
    /// Gets the depth of the node within the ASAP2 hierarchy.
    /// 
    /// Examples:
    /// - An A2LPROJECT returns 0
    /// - An A2LMEASUREMENT returns 2
    /// </summary>
    /// <returns>the depth of the node</returns>
    public int getNodeDepth()
    {
      A2LNODE parent = Parent;
      int i = 0;
      while (parent != null)
      { // iterate until parent is null
        parent = parent.Parent;
        ++i;
      }
      return i;
    }
    /// <summary>
    /// Gets the referencing nodes.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any node.</returns>
    public virtual A2LNODE[] getRefNodes() { return null; }
    /// <summary>
    /// Gets the referencing functions.
    /// 
    /// Ususally only valid for A2LMEASUREMENT and A2LCHARACTERISTIC.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any node.</returns>
    public virtual A2LREFBASE[] getRefFunctions() { return null; }
    /// <summary>
    /// Gets the context of the node instance up to the root node.
    /// </summary>
    /// <param name="delimiter">The delimiter to use between the nodes</param>
    /// <returns>a context string</returns>
    public string getContext(char delimiter)
    {
      var sb = new StringBuilder();
      var parent = this;
      while (null != parent)
      {
        sb.Insert(0, string.Format("{0}/", parent.Name));
        parent = parent.Parent;
      }
      if (sb.Length > 0)
        sb.Remove(sb.Length - 1, 1);
      return sb.ToString();
    }
    /// <summary>
    /// Gets the used .NET Framework types from the current instance including all childs.
    /// </summary>
    /// <returns>An array of types within the calling instance</returns>
    internal Type[] getTypes()
    {
      var typeSet = new Dictionary<A2LType, Type>();
      foreach (var node in enumChildNodes<A2LNODE>())
      { // iterate over all child nodes
        if (typeSet.ContainsKey(node.Type))
          continue;
        // keep the nodes type in the dictionary
        typeSet[node.Type] = node.GetType();
      }
      var types = new Type[typeSet.Count];
      typeSet.Values.CopyTo(types, 0);
      return types;
    }
    /// <summary>
    /// Gets the indent of the node to write.
    /// </summary>
    /// <param name="node">The A2LNode instance</param>
    /// <param name="forData">true if the data indent should be computed</param>
    /// <returns>the node indent</returns>
    internal static string getIndent(A2LNODE node, bool forData)
    {
      var sb = new StringBuilder();
      string indent = Settings.Default.WriterIndent == WriterIndent.Space ? "  " : "\t";
      if (forData)
        sb.Append(indent);
      for (int i = node.getNodeDepth() - 1; i >= 0; --i)
        sb.Append(indent);
      return sb.ToString();
    }
    /// <summary>
    /// Deletes the specified node from the it's containing model.
    /// </summary>
    /// <param name="node">The (named) node to delete</param>
    /// <returns>false if node is not found nor deleted</returns>
    public static bool deleteFromModel(A2LNAMEDNODE node)
    {
      bool result = false;
      // get the model's root
      var module = node.getParent<A2LMODULE>();
      module.Childs.Remove(node);

      var project = module.getParent<A2LPROJECT>();
      switch (node.Type)
      {
        case A2LType.MEASUREMENT:
          result = project.MeasDict.Remove(node.Name);
          break;
        case A2LType.AXIS_PTS:
        case A2LType.CHARACTERISTIC:
          result = project.CharDict.Remove(node.Name);
          break;
      }

      if (!result)
        return result;

      // node is deleted, delete potential references (Functions and Groups)
      var enFunc = project.FuncDict.GetEnumerator();
      while (enFunc.MoveNext())
      { // Iterate through all functions/groups
        var refBase = enFunc.Current.Value;
        foreach (var reference in refBase.enumChildNodes<A2LREFERENCE>())
          // iterate through all reference instances of the function/group
          reference.deleteNode(node);
      }

      if (node.Type == A2LType.AXIS_PTS)
      { // special case for axis points (if deleted, referencing characteristics must be deleted, too)
        // Important: copy the characteristic in order to do an recursive call on this method
        var layoutRefs = new A2LRECORD_LAYOUT_REF[project.CharDict.Count];
        project.CharDict.Values.CopyTo(layoutRefs, 0);
        foreach (var layoutRef in layoutRefs)
        { // iterate through characteristics
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          if (characteristic == null)
            continue;
          switch (characteristic.CharType)
          {
            case CHAR_TYPE.CURVE:
            case CHAR_TYPE.MAP:
            case CHAR_TYPE.CUBOID:
            case CHAR_TYPE.CUBE_4:
            case CHAR_TYPE.CUBE_5:
              // inspect AxisPts referencing types
              foreach (var axisDesc in characteristic.enumChildNodes<A2LAXIS_DESCR>())
              {
                if (axisDesc.AxisPtsRef != node.Name)
                  continue;
                // AxisPts is referenced by this characteristic -> delete it, too
                deleteFromModel(characteristic);
              }
              break;
          }
        }
      }
      return result;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all A2L entities with specific names.
  /// </summary>
  public abstract partial class A2LNAMEDNODE : A2LNODE
  {
    #region constructor
    /// <summary>
    /// Initialises a new instance of A2LNAMEDNODE with the specified parameters.
    /// </summary>
    /// <param name="startLine">The starting line</param>
    protected A2LNAMEDNODE(int startLine) : base(startLine) { }
    /// <summary>
    /// Initialises a new instance of A2LNAMEDNODE with the specified parameters.
    /// </summary>
    /// <param name="startLine">The starting line</param>
    /// <param name="mA2MLType">The A2ML type</param>
    protected A2LNAMEDNODE(int startLine, string mA2MLType) : base(startLine) { Name = mA2MLType; }
    #endregion
    #region properties
    /// <summary>
    /// Gets the name of the node.
    /// 
    /// Use the setter with care! Changing the name requires to adjust all references to this instance.
    /// </summary>
    [Category(Constants.CATData), Description("The unique name of the A2LNAMEDNODE.")]
    public sealed override string Name { get; set; }
    [Browsable(false), Category(Constants.CATData)]
    public virtual string DisplayName { get { return Name; } set { } }
    #endregion
  }
  /// <summary>
  /// Node type to store unsupported objects.
  /// 
  /// - This type is used whenever an unsupported object is recognized
  /// (e.g with a newer specification as 1.7).
  /// - A basically unsupported node is A2ML.
  /// </summary>
  public partial class A2LUNSUPPORTEDNODE : A2LNAMEDNODE
  {
    #region constructors
    /// <summary>
    /// Creates a new instance of A2LUNSUPPORTEDNODE with the specified parameter.
    /// </summary>
    /// <param name="startLine">The starting line</param>
    protected A2LUNSUPPORTEDNODE(int startLine) : base(startLine) { Type = A2LType.UNSUPPORTED; }
    internal A2LUNSUPPORTEDNODE(int startLine, string name) : base(startLine, name) { Type = A2LType.UNSUPPORTED; }
    #endregion
    #region properties
    /// <summary>
    /// Represents the original content from the ASAP2 file as a string.
    /// 
    /// The XML Serialzer does not export this property.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Content { get; set; }
    #endregion
    #region methods
    internal sealed override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      return true;
    }
    #endregion
  }
  /// <summary>
  /// Base class for A2LFUNCTION and A2LGROUP.
  /// 
  /// This nodes typically reference a set of other nodes by name.
  /// </summary>
  public abstract partial class A2LREFBASE : A2LNAMEDNODE, IA2LDescription
  {
    #region members
    protected List<A2LNAMEDNODE> mReferencedNodes;
    #endregion
    #region constructor
    protected A2LREFBASE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    /// <summary>
    /// Gets the list of referenced nodes within the ASAP2 hierarchy.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Description("The list of refenced nodes within the ASAP2 hierarchy.")]
    public virtual List<A2LNAMEDNODE> ReferencedNodes
    {
      get
      {
        if (mReferencedNodes == null)
        {
          mReferencedNodes = new List<A2LNAMEDNODE>();
          // Get the direct references
          List<A2LREFERENCE> refNodes = getNodeList<A2LREFERENCE>();
          for (int i = refNodes.Count - 1; i >= 0; --i)
          {
            A2LREFERENCE refNode = refNodes[i];
            for (int j = refNode.ReferencedNodes.Count - 1; j >= 0; --j)
            {
              A2LNAMEDNODE node = refNode.ReferencedNodes[j] as A2LNAMEDNODE;
              if (node == null)
                throw new ArgumentException(string.Format("referencing the not named node {0}", refNode.ReferencedNodes[i]));
              mReferencedNodes.Add(node);
            }
          }
          // get the subgroups
          List<A2LSUB_GROUP> subGroups = getNodeList<A2LSUB_GROUP>();
          for (int i = 0; i < subGroups.Count; ++i)
          {
            A2LSUB_GROUP subGroup = subGroups[i];
            foreach (A2LGROUP group in subGroup.ReferencedNodes)
              mReferencedNodes.AddRange(group.ReferencedNodes);
          }
        }
        return mReferencedNodes;
      }
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      var result = base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines);
      return string.Format("{0}{1}{2} references contained.", result, result.Length > 0 ? "\n\n" : "\n", ReferencedNodes.Count);
    }
    #endregion
  }
  /// <summary>
  /// Base class for all unnamed A2L entities referencing A2LMEASUREMENTs or a A2LCHARACTERISTICs.
  /// 
  /// This nodes typically reference a set of other nodes by name.
  /// 
  /// Examples:
  /// A2LREF_CHARACTERISTIC, A2LSUB_FUNCTION,...
  /// </summary>
  public abstract partial class A2LREFERENCE : A2LNODE, IA2LDescription
  {
    #region members
    A2LNodeList mReferencedNodes;
    #endregion
    #region constructor
    internal A2LREFERENCE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), DefaultValue(null)]
    public List<string> References { get; protected set; }
    /// <summary>
    /// Gets the list of referenced nodes within the ASAP2 hierarchy.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), Description("The list of refenced nodes within the ASAP2 hierarchy.")]
    public A2LNodeList ReferencedNodes
    {
      get
      {
        if (null == mReferencedNodes)
        {
          mReferencedNodes = new A2LNodeList();
          if (References != null)
          {
            var project = getParent<A2LPROJECT>();
            A2LMEASUREMENT measurement;
            A2LRECORD_LAYOUT_REF characteristic;
            A2LREFBASE function;
            A2LREFBASE group;
            A2LBLOB blob;
            for (int i = References.Count - 1; i >= 0; --i)
            {
              A2LNAMEDNODE node = null;
              switch (Type)
              {
                case A2LType.SUB_GROUP:
                case A2LType.REF_GROUP:
                  project.GroupDict.TryGetValue(References[i], out group);
                  node = group;
                  break;
                case A2LType.FUNCTION_LIST:
                case A2LType.SUB_FUNCTION:
                  project.FuncDict.TryGetValue(References[i], out function);
                  node = function;
                  break;
                case A2LType.IN_MEASUREMENT:
                case A2LType.OUT_MEASUREMENT:
                case A2LType.LOC_MEASUREMENT:
                case A2LType.REF_MEASUREMENT:
                case A2LType.VIRTUAL:
                  project.MeasDict.TryGetValue(References[i], out measurement);
                  node = measurement;
                  break;
                case A2LType.TRANSFORMER_IN_OBJECTS:
                case A2LType.TRANSFORMER_OUT_OBJECTS:
                  if (project.MeasDict.TryGetValue(References[i], out measurement))
                    node = measurement;
                  else if (project.CharDict.TryGetValue(References[i], out characteristic))
                    node = characteristic;
                  else if (project.BlobDict.TryGetValue(References[i], out blob))
                    node = blob;
                  break;
                case A2LType.DEPENDENT_CHARACTERISTIC:
                case A2LType.DEF_CHARACTERISTIC:
                case A2LType.REF_CHARACTERISTIC:
                  project.CharDict.TryGetValue(References[i], out characteristic);
                  node = characteristic;
                  break;
                case A2LType.VIRTUAL_CHARACTERISTIC:
                  // todo virtual characteristic dictionary
                  break;
                default:
                  throw new NotSupportedException(Type.ToString());
              }
              if (node != null)
                mReferencedNodes.Add(node);
              else
              { // Not found
                //if (parser.ParserMessage != null)
                //  parser.ParserMessage(parser, new ParserEventArgs(this, MessageType.Error
                //    , string.Format("Verb {0} already existing in verbs!", mParTempList[4 + i])
                //    ));
                References.RemoveAt(i);
              }
            }
          }
        }
        return mReferencedNodes;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      if (startIndex == parameterList.Count)
        return false;
      string[] para = new string[parameterList.Count - startIndex];
      parameterList.CopyTo(startIndex, para, 0, parameterList.Count - startIndex);
      References = new List<string>(para);
      return true;
    }
    /// <summary>
    /// Deletes the specified node from the list of references.
    /// </summary>
    /// <param name="node"></param>
    public void deleteNode(A2LNAMEDNODE node)
    {
      if (null == References || !References.Remove(node.Name))
        // empty or not found
        return;
      // invalidate cache
      mReferencedNodes = null;
    }
    public string Description { get { return null; } }

    public string DisplayName { get { return Name; } }

    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      return string.Format("\n{0} references contained.", ReferencedNodes.Count);
    }
    #endregion
  }
  /// <summary>
  /// Base class for all A2L entities containing an ECU address.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public abstract partial class A2LADDRESS : A2LNAMEDNODE, IA2LDescription
  {
    #region members
    protected string mDisplayName;
    #endregion
    #region constructor
    internal A2LADDRESS(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    protected A2LADDRESS(A2LMODULE module, A2LINSTANCE instance) : base(int.MaxValue - 1)
    {
      RefInstance = instance;
      Name = instance.Name;
      Description = instance.Description;
      Address = instance.Address;
      AddressExtension = instance.AddressExtension;
      mDisplayName = instance.mDisplayName;
      MaxRefreshRate = instance.MaxRefreshRate;
      MaxRefreshUnit = instance.MaxRefreshUnit;
      // Dont' copy Symbol Links and Offsets
      //SymbolLink = instance.SymbolLink;
      //SymbolOffset = instance.SymbolOffset;
      //ModelLink = instance.ModelLink;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the comment, description.
    /// </summary>
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    /// <summary>
    /// Gets or sets the address of the adjustable object in the emulation memory.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Address { get; set; } = UInt32.MaxValue;
    /// <summary>
    /// Gets the address extension.
    /// 
    /// This keyword is an additional address information. 
    /// For instance it can be used, to distinguish different 
    /// address spaces of an ECU (multi-micro controller devices).
    /// </summary>
    [Category(Constants.CATData), DefaultValue(0)]
    public int AddressExtension { get; set; }
    [Category(Constants.CATData), DefaultValue(ScalingUnits.NotSet), TypeConverter(typeof(EnumConverter))]
    public ScalingUnits MaxRefreshUnit { get; set; } = ScalingUnits.NotSet;
    [Category(Constants.CATData), DefaultValue(0)]
    public int MaxRefreshRate { get; set; }
    [Category(Constants.CATData), DefaultValue(CALIBRATION_ACCESS.NotSet)]
    public CALIBRATION_ACCESS CalibAccess { get; set; }
    /// <summary>
    /// Name of the symbol within the corresponding linker map file.
    /// </summary>
    [Category(Constants.CATDataLink)]
    public string SymbolLink { get; set; }
    /// <summary>
    /// Offset of the Symbol relative to the symbol’s address in the linker map file.
    /// 
    /// Only Valid if SymbolLink is not null or empty.
    /// </summary>
    [Category(Constants.CATDataLink), DefaultValue(0)]
    public int SymbolOffset { get; set; }
    /// <summary>
    /// Object model's name.
    /// 
    /// This model_link can be used to reference a software model object the
    /// CHARACTERISTIC, AXIS_PTS, BLOB, INSTANCE or MEASUREMENT was derived
    /// from.
    /// </summary>
    [Category(Constants.CATDataLink)]
    public string ModelLink { get; set; }
    /// <summary>
    /// Gets the display name.
    /// 
    /// Can be used as a display name (alternative to the ‘name’ attribute).
    /// </summary>
    [Browsable(true), Description("If not explicitly set, the value from Name is taken.")]
    public sealed override string DisplayName
    {
      get
      {
        return string.IsNullOrEmpty(mDisplayName)
          ? Name
          : mDisplayName;
      }
      set { mDisplayName = value; }
    }
    /// <summary>
    /// A reference to an A2L instance (May occur with since ASAP2 v1.7.0 specification).
    /// </summary>
    [XmlIgnore, Category(Constants.CATNavigation)]
    public A2LINSTANCE RefInstance { get; private set; }
    public sealed override bool IsInstanced { get { return StartLine == int.MaxValue - 1; } }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      return true;
    }
    /// <summary>
    /// Gets the defined symbol link if requested.
    /// 
    /// If no SYMBOL_LINK is defined the Name is delivered.
    /// </summary>
    /// <param name="offset">The offset to be used (defined by a potential symbol link offset)</param>
    /// <returns>An ELF symbol name or the Name if no SYMBOL_LINK is defined</returns>
    internal string getSymbol(out int offset)
    {
      offset = 0;
      var symbolName = Name;
      if (!string.IsNullOrEmpty(SymbolLink))
      {
        offset = SymbolOffset;
        symbolName = SymbolLink;
      }
      return symbolName;
    }
    /// <summary>
    /// Gets the size of used memory in bytes.
    /// </summary>
    /// <returns></returns>
    public abstract int getMemorySize();
    /// <summary>
    /// 
    /// </summary>
    /// <param name="module">The module to add to</param>
    /// <param name="type">The A2L type to set</param>
    /// <param name="typedef">The typedef reference</param>
    protected void addFromInstance(A2LMODULE module, A2LType type, A2LTYPEDEF typedef)
    {
      Type = type;
      Parent = module;

      if (typedef.Childs == null)
        // no childs to add
        return;

      // copy typedef childs to instance
      if (Childs == null)
        Childs = new A2LNodeList();
      Childs.AddRange(typedef.Childs);
    }
    #region IA2LDescription Members
    public virtual string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
    #endregion
  }

  /// <summary>
  /// Base class for all A2L entities referencing an A2L_COMPUMETHOD method.
  /// </summary>
  public abstract partial class A2LCONVERSION_REF : A2LADDRESS
  {
    #region members
    internal static readonly char[] mFormatSplitChars = new char[] { '.', 'd', 'f' };
    protected string mConversion;
    protected BYTEORDER_TYPE mByteOrder;
    protected string mFormat;
    protected string mPhysUnit;
    A2LCOMPU_METHOD mCompuMethod;
    List<A2LREFBASE> mRefFunctions;
    protected string mMemorySegmentRef;
    int mDecimalCount = -1;
    string mUnitCache;
    BYTEORDER_TYPE mByteOrderCache;
    #endregion
    #region constructor
    internal A2LCONVERSION_REF(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdConv">The conversion ref typedef reference</param>
    protected A2LCONVERSION_REF(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_CONVERSION_REF tdConv)
      : base(module, instance)
    {
      mConversion = tdConv.Conversion;
      mByteOrder = tdConv.ByteOrder;
      mFormat = tdConv.Format;
      mPhysUnit = tdConv.PhysUnit;
      mByteOrder = tdConv.ByteOrder;
      LowerLimit = tdConv.LowerLimit;
      UpperLimit = tdConv.UpperLimit;

      var subNodes = instance.getNodeList<A2LOVERWRITE>(false);
      foreach (var subNode in subNodes)
      {
        if (subNode.AxisNo > 0)
          continue;
        if (!string.IsNullOrEmpty(subNode.Conversion))
          mConversion = subNode.Conversion;
        if (!string.IsNullOrEmpty(subNode.PhysUnit))
          mPhysUnit = subNode.PhysUnit;
        if (!string.IsNullOrEmpty(subNode.Format))
          mFormat = subNode.Format;
        if (subNode.LimitsSet)
        {
          LowerLimit = subNode.LowerLimit;
          UpperLimit = subNode.UpperLimit;
        }
      }
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the conversion's name.
    /// 
    /// Reference to the relevant record of the description of 
    /// the conversion method (see A2LCOMPU_METHOD). If there is 
    /// no conversion method, as in the case of CURVE_AXIS, 
    /// the parameter ‘Conversion’ should be set to 
    /// "NO_COMPU_METHOD" (measurement and calibration systems must 
    /// be able to handle this case).
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(Helpers.TCConversionRef))]
    public string Conversion
    {
      get { return mConversion; }
      set
      {
        mCompuMethod = null;
        mUnitCache = null;
        mConversion = value;
      }
    }
    [Category(Constants.CATData)]
    public string MemorySegmentRef { get { return mMemorySegmentRef; } }
    /// <summary>
    /// Gets the decimal count of the corresponding Format value.
    /// </summary>
    [XmlIgnore, Browsable(false)]
    public int DecimalCount
    {
      get
      {
        if (mDecimalCount == -1)
          mDecimalCount = getDecimalCount(Format);
        return mDecimalCount;
      }
    }
    /// <summary>
    /// Gets or sets the Byte order (optional).
    /// 
    /// - If the local value is NotSet, the value from A2LMOD_COMMON is taken.
    /// - The setter initialises the byte order cache (e.g. if the MOD_COMMON vbyte order is changed).
    /// - The local member is explicitely set.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrderWithNotSet)), Description("If not explicitly set, the value from A2LMOD_COMMON is taken.")]
    public BYTEORDER_TYPE ByteOrder
    {
      get
      {
        if (mByteOrderCache == BYTEORDER_TYPE.NotSet)
        {
          mByteOrderCache = mByteOrder;
          if (mByteOrderCache == BYTEORDER_TYPE.NotSet)
          {
            A2LMOD_COMMON modCommon = getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false);
            if (modCommon != null)
              mByteOrderCache = getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false).BYTE_ORDER;
          }
        }
        return mByteOrderCache;
      }
      set
      {
        mByteOrder = value;
        mByteOrderCache = BYTEORDER_TYPE.NotSet;
      }
    }
    /// <summary>
    /// Gets or sets the plausible range of table values, lower limit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double LowerLimit { get; set; }
    /// <summary>
    /// Gets or sets the plausible range of table values, upper limit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double UpperLimit { get; set; }
    /// <summary>
    /// Gets or sets the format.
    /// 
    /// With deviation from the display format specified with 
    /// keyword COMPU_TAB referenced by parameter Conversion
    /// a special display format can be specified to be used 
    /// to display the table values.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(Helpers.TCFormatRef))]
    public string Format
    {
      get
      {
        if (!string.IsNullOrEmpty(mFormat))
          return mFormat;
        A2LCOMPU_METHOD refCompuMethod = RefCompuMethod;
        return mFormat = refCompuMethod != null
          ? refCompuMethod.Format
          : null;
      }
      set { mFormat = value; }
    }
    /// <summary>
    /// Gets or sets the Unit.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(Helpers.TCUnitRef))]
    public string Unit
    {
      get
      {
        if (mUnitCache != null)
          return mUnitCache;
        if (!string.IsNullOrEmpty(mPhysUnit))
          mUnitCache = mPhysUnit;
        else
        {
          A2LCOMPU_METHOD refCompuMethod = RefCompuMethod;
          mUnitCache = refCompuMethod != null
            ? refCompuMethod.Unit
            : null;
        }
        return mUnitCache;
      }
      set { mPhysUnit = value; mUnitCache = null; }
    }
    /// <summary>
    /// Gets the direct reference to the corresponding compu method.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LCOMPU_METHOD RefCompuMethod
    {
      get
      {
        if (null == mCompuMethod)
          // Search only once
          getCompuMethod(getParent<A2LPROJECT>().CompDict, mConversion, out mCompuMethod);
        return mCompuMethod;
      }
    }
    /// <summary>
    /// Gets the list of functions referring this node.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData)]
    public List<A2LREFBASE> RefFunctions
    {
      get
      {
        if (mRefFunctions == null)
        {
          mRefFunctions = new List<A2LREFBASE>();
          var project = getParent<A2LPROJECT>();
          var en = project.FuncDict.GetEnumerator();
          while (en.MoveNext())
          {
            var function = en.Current.Value;
            if (!function.ReferencedNodes.Contains(this))
              continue;
            // function references this node
            if (!mRefFunctions.Contains(function))
              mRefFunctions.Add(function);
          }
          en = project.GroupDict.GetEnumerator();
          while (en.MoveNext())
          {
            var group = en.Current.Value;
            if (!group.ReferencedNodes.Contains(this))
              continue;
            // function references this node
            if (!mRefFunctions.Contains(group))
              mRefFunctions.Add(group);
          }
        }
        return mRefFunctions;
      }
    }
    public override A2LREFBASE[] getRefFunctions()
    {
      return RefFunctions != null ? RefFunctions.ToArray() : null;
    }
    #endregion
    #region methods
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="value">The value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <param name="dataType">The data type (only important if format is raw)</param>
    /// <param name="compuMethod">The corresponding comp method</param>
    /// <param name="decimalCount">The value decimal count (only used if ValueObjectFormt.Physical is set)</param>
    /// <returns>the string representation</returns>
    public static string toStringValue(double value, ValueObjectFormat format, DATA_TYPE dataType, A2LCOMPU_METHOD compuMethod, int decimalCount)
    {
      bool lowerLimitViolated, upperLimitViolated;
      return toStringValue(value, format, dataType, compuMethod, decimalCount
        , double.MinValue, double.MaxValue
        , out lowerLimitViolated, out upperLimitViolated
        );
    }
    /// <summary>
    /// Builds a string representation from the specified physical value.
    /// </summary>
    /// <param name="physValue">The physical value to build the string for</param>
    /// <returns>the string representation</returns>
    public string toStringValue(double physValue)
    {
      bool lowerLimitViolated, upperLimitViolated;
      return toStringValue(physValue, ValueObjectFormat.Physical
        , DATA_TYPE.UBYTE, RefCompuMethod, DecimalCount
        , LowerLimit, UpperLimit
        , out lowerLimitViolated, out upperLimitViolated
        );
    }
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="value">The value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <param name="dataType">The data type (only important if format is raw)</param>
    /// <returns>the string representation</returns>
    public string toStringValue(double value
      , ValueObjectFormat format
      , DATA_TYPE dataType
      )
    {
      bool lowerLimitViolated, upperLimitViolated;
      return toStringValue(value, format, dataType, RefCompuMethod, DecimalCount
        , LowerLimit, UpperLimit
        , out lowerLimitViolated, out upperLimitViolated
        );
    }
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="value">The value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <param name="dataType">The data type (only important if format is raw)</param>
    /// <param name="lowerLimitViolated">true if the lower limit is violated</param>
    /// <param name="upperLimitViolated">true if the upper limit is violated</param>
    /// <returns>the string representation</returns>
    public string toStringValue(double value
      , ValueObjectFormat format
      , DATA_TYPE dataType
      , out bool lowerLimitViolated
      , out bool upperLimitViolated
      )
    {
      return toStringValue(value, format, dataType, RefCompuMethod, DecimalCount
        , LowerLimit, UpperLimit
        , out lowerLimitViolated, out upperLimitViolated
        );
    }
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="value">The value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <param name="dataType">The data type (only used if ValueObjectFormt.Physical is not set)</param>
    /// <param name="compuMethod">The corresponding comp method</param>
    /// <param name="decimalCount">The value decimal count (only used if ValueObjectFormt.Physical is set)</param>
    /// <param name="lowerLimit">The physical lower limit</param>
    /// <param name="upperLimit">The physical upper limit</param>
    /// <param name="lowerLimitViolated">true if the lower limit is violated</param>
    /// <param name="upperLimitViolated">true if the upper limit is violated</param>
    /// <returns>the string representation</returns>
    public static string toStringValue(double value
      , ValueObjectFormat format
      , DATA_TYPE dataType
      , A2LCOMPU_METHOD compuMethod
      , int decimalCount
      , double lowerLimit
      , double upperLimit
      , out bool lowerLimitViolated
      , out bool upperLimitViolated
      )
    {
      decimalCount = Math.Min(15, decimalCount);
      lowerLimitViolated = upperLimitViolated = false;
      switch (compuMethod.ConversionType)
      {
        case CONVERSION_TYPE.TAB_VERB:
          if (format == ValueObjectFormat.Physical)
          { // pyhsical values required
            A2LCOMPU_TAB_BASE baseTab = compuMethod.RefCompuTab;
            A2LCOMPU_VTAB vTab = baseTab as A2LCOMPU_VTAB;
            if (vTab != null)
              // verbal
              return vTab.toPhysical(value);
            A2LCOMPU_VTAB_RANGE vTabRange = baseTab as A2LCOMPU_VTAB_RANGE;
            if (vTabRange != null)
              // verbal range
              return vTabRange.toPhysical(value);
          }
          // forced fall through
          goto default;
        default:
          switch (format)
          {
            case ValueObjectFormat.Physical:
              double physValue = Math.Round(value, decimalCount);
              lowerLimitViolated = physValue < Math.Round(lowerLimit, decimalCount);
              upperLimitViolated = physValue > Math.Round(upperLimit, decimalCount);
              return physValue.ToString(string.Format("f{0}", decimalCount), CultureInfo.InvariantCulture);
            case ValueObjectFormat.Raw:
              return Extensions.toDecimalString(value, dataType);
            case ValueObjectFormat.RawHex:
              return Extensions.toHexString(value, dataType, false);
            case ValueObjectFormat.RawBin:
              return Extensions.toBinaryString(value, dataType, false);
            default: throw new NotSupportedException(format.ToString());
          }
      }
    }
    /// <summary>
    /// Gets the decimal count from an A2L format string.
    /// </summary>
    /// <param name="format"></param>
    /// <returns>The count of decimal places</returns>
    public static int getDecimalCount(string format)
    {
      if (string.IsNullOrEmpty(format) || format.Length < 2)
        return 0;
      var splits = format.Substring(1).ToLower().Split(mFormatSplitChars);
      return splits.Length < 2
        ? 0
        : int.Parse(splits[1]);
    }
    /// <summary>
    /// Resets the byte order cache (used e.g. if the MOD_COMMON byte order is changed).
    /// </summary>
    internal void reset() { mByteOrderCache = BYTEORDER_TYPE.NotSet; }
    public sealed override string ToString()
    {
      return string.IsNullOrEmpty(mDisplayName) ? Name : mDisplayName;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all A2L entities referencing an A2L_COMPUMETHOD method and an A2LRECORD_LAYOUT.
  /// 
  /// These derived classes are:
  /// - A2LCHARACTERISTIC
  /// - A2LAXIS_PTS
  /// </summary>
  public abstract partial class A2LRECORD_LAYOUT_REF : A2LCONVERSION_REF
  {
    /// <summary>
    /// Cache type for readonly computation.
    /// </summary>
    enum ReadOnlyType
    {
      /// <summary>
      /// Not computed before.
      /// </summary>
      Unknown,
      /// <summary>
      /// Instance is readonly.
      /// </summary>
      ReadOnly,
      /// <summary>
      /// Instance is writable.
      /// </summary>
      Writeable,
    }
    #region members
    protected string mRecordLayout;
    /// <summary>
    /// Cache for the reaonly computation.
    /// </summary>
    ReadOnlyType mReadonlyType;
    A2LRECORD_LAYOUT mRefRecordLayout;
    #endregion
    #region constructor
    internal A2LRECORD_LAYOUT_REF(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdRecLayout">The axis typedef reference</param>
    protected A2LRECORD_LAYOUT_REF(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_RECORDLAYOUT_REF tdRecLayout)
      : base(module, instance, tdRecLayout)
    {
      mRecordLayout = tdRecLayout.RecordLayout;
      MaxDiff = tdRecLayout.MaxDiff;
      LowerLimitEx = tdRecLayout.LowerLimitEx;
      UpperLimitEx = tdRecLayout.UpperLimitEx;
      StepSize = tdRecLayout.StepSize;

      var subNodes = instance.getNodeList<A2LOVERWRITE>(false);
      foreach (var subNode in subNodes)
      {
        if (subNode.AxisNo > 0)
          continue;
        if (subNode.LimitsExSet)
        {
          LowerLimitEx = subNode.LowerLimitEx;
          UpperLimitEx = subNode.UpperLimitEx;
        }
      }
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets a reference to the corresponding data record for 
    /// description of the record layout (see A2LRECORD_LAYOUT)
    /// 
    /// For a direct reference use RefRecordLayout!
    /// </summary>
    [Category(Constants.CATData)]
    public string RecordLayout { get { return mRecordLayout; } set { mRefRecordLayout = null; mRecordLayout = value; } }
    /// <summary>
    /// Gets or sets the maximum float with respect to an adjustment of a table value.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(0.0)]
    public double MaxDiff { get; set; }
    /// <summary>
    /// Gets or sets the readonly state.
    /// 
    /// This keyword can be used to indicate that the 
    /// adjustable object cannot be changed (but can be read 
    /// only). This keyword indicates the adjustable object to 
    /// be read only at all (table values and axis points). The 
    /// optional keyword used at AXIS_DESCR record indicates the 
    /// related axis points to be read only.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ReadOnly { get; set; }
    /// <summary>
    /// Gets or sets the guard rails.
    /// 
    /// This keyword is used to indicate that an adjustable 
    /// CURVE or MAP uses guard rails. The Measurement 
    /// and Calibration System does not allow the user to edit 
    /// the outermost values of the adjustable object (see GUARD_RAILS).
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool GuardRails { get; set; }
    /// <summary>
    /// Gets or sets the extended lower limit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double LowerLimitEx { get; set; }
    /// <summary>
    /// Gets or sets the extended upper limit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double UpperLimitEx { get; set; }
    /// <summary>
    /// Gets the direct reference to the corresponding record layout.
    /// 
    /// may be (never should be) null!
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LRECORD_LAYOUT RefRecordLayout
    {
      get
      {
        if (mRefRecordLayout == null)
          // search only once
          getParent<A2LPROJECT>().RecLayDict.TryGetValue(mRecordLayout, out mRefRecordLayout);
        return mRefRecordLayout;
      }
    }
    /// <summary>
    /// Gets or sets the step size.
    /// 
    /// This keyword can be used to define a delta value 
    /// which is added to or subtracted from the current value 
    /// when using up/down keys while calibrating.
    /// </summary>
    [Category(Constants.CATData)]
    //[DefaultValue(double.NaN)], disabled in case of .NET XML serialization bug
    public double StepSize { get; set; } = double.NaN;
    #endregion
    #region methods
    /// <summary>
    /// Returns a value indicating if the characteristic value should not be changed.
    /// 
    /// Additionally to the direct ReadOnly flag, the corresponding ReadOnly state
    /// of the memory segment is taken into account.
    /// </summary>
    /// <returns>true if values should never be written</returns>
    public bool isReadOnly()
    {
      if (mReadonlyType == ReadOnlyType.Unknown)
      { // first access
        mReadonlyType = ReadOnly ? ReadOnlyType.ReadOnly : ReadOnlyType.Writeable;
        if (mReadonlyType == ReadOnlyType.Writeable)
        { // test the memory segment
          var memSeg = getMemorySegment();
          if (null != memSeg && memSeg.IsReadOnly)
            mReadonlyType = ReadOnlyType.ReadOnly;
        }
      }
      return mReadonlyType == ReadOnlyType.ReadOnly;
    }
    /// <summary>
    /// Gets the corresponding memory segment (may be null).
    /// </summary>
    /// <returns>The memory segment or null if not available</returns>
    public IA2LMemory getMemorySegment()
    {
      var module = getParent<A2LMODULE>();
      if (module == null)
        return null;
      var modPar = module.getNode<A2LMODPAR>(false);
      if (modPar == null)
        return null;
      var memSegs = new List<IA2LMemory>(modPar.getNodeList<A2LMEMORY_SEGMENT>().ToArray());
      memSegs.AddRange(modPar.getNodeList<A2LMEMORY_LAYOUT>().ToArray());
      for (int i = memSegs.Count - 1; i >= 0; --i)
      {
        var memSeg = memSegs[i];
        if (memSeg.contains(Address))
          return memSeg;
      }
      return null;
    }
    /// <summary>
    /// Gets the function which holds the current instance as the DEF_CHARACTERISTIC.
    /// </summary>
    /// <returns>The A2LFUNCTION instance or null if not found</returns>
    public A2LFUNCTION getDefCharacteristicFunction()
    {
      foreach (var refBase in RefFunctions)
      {
        var function = refBase as A2LFUNCTION;
        if (function == null)
          continue;
        var defChar = function.getNode<A2LDEF_CHARACTERISTIC>(false);
        if (defChar == null)
          continue;
        if (defChar.ReferencedNodes.Contains(this))
          return function;
      }
      return null;
    }
    /// <summary>
    /// Builds a string representation from the specified value.
    /// </summary>
    /// <param name="rawValue">The raw value to build the string for</param>
    /// <param name="format">The value format</param>
    /// <param name="dt">The data type (only important if format is raw)</param>
    /// <param name="lowerLimitViolated">true if the lower limit is violated</param>
    /// <param name="upperLimitViolated">true if the upper limit is violated</param>
    /// <param name="lowerLimitExViolated">true if the extended lower limit is violated</param>
    /// <param name="upperLimitExViolated">true if the extended upper limit is violated</param>
    /// <returns>the string representation</returns>
    public string toStringValue(double rawValue
      , ValueObjectFormat format
      , DATA_TYPE dt
      , out bool lowerLimitViolated
      , out bool upperLimitViolated
      , out bool lowerLimitExViolated
      , out bool upperLimitExViolated
      )
    {
      int decimalCount = DecimalCount;
      var compuMethod = RefCompuMethod;
      var layout = RefRecordLayout;
      // test the standard limits
      string result = A2LCONVERSION_REF.toStringValue(rawValue, format
        , layout.FncValues.DataType, compuMethod, decimalCount
        , LowerLimitEx, UpperLimitEx
        , out lowerLimitExViolated, out upperLimitExViolated
        );
      // initialize value for extended limit
      lowerLimitViolated = lowerLimitExViolated;
      upperLimitViolated = upperLimitExViolated;
      if (lowerLimitExViolated || upperLimitExViolated
        || LowerLimitEx == LowerLimit && UpperLimitEx == UpperLimit
         )
        // extended limit violated, standard limits must be violated, too
        return result;

      // test for extended limits
      return A2LCONVERSION_REF.toStringValue(rawValue, format
        , layout.FncValues.DataType, compuMethod, decimalCount
        , LowerLimit, UpperLimit
        , out lowerLimitViolated, out upperLimitViolated
        );
    }
    /// <summary>
    /// Gets the size in bytes of the characteristic.
    /// 
    /// The size is calculated from
    /// - the corresponding record layout
    /// - the number of elements from MatrixDim
    /// - external references (like A2LAXIS_PTS) of the characteristic is not taken into account
    /// 
    /// Together with the characteristic's address, the characteristic's complete value could be
    /// read or written directly from/to a MemorySegment or device.
    /// </summary>
    /// <returns>The size in bytes</returns>
    public abstract int getSize();
    /// <summary>
    /// Computes the offset until the specified position of a multiple value characteristic value.
    /// 
    /// This method is also used to determine the size of characteristic's value (see getSize).
    /// </summary>
    /// <param name="position">The position to compute the offset to (0 for computing the complete size)</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="axisDescs">The axis descriptions (may be null)</param>
    /// <param name="maxAxisPts">Maximum no of axis points if axisDesc is not defined(null). This case is usually used for A2LAXIS_PTS</param>
    /// <returns>the computed offset</returns>
    internal static int getOffsetToPosition(int position, A2LRECORD_LAYOUT layout, List<A2LAXIS_DESCR> axisDescs, int maxAxisPts)
    {
      int offset = 0, i = 0;
      var layoutEntries = layout.LayoutEntries;
      A2LRECORD_LAYOUT.ItemLayoutDesc itemLayout;
      while (i++ < layoutEntries.Count && position > (itemLayout = layoutEntries[i - 1]).Position)
      {
        DATA_TYPE dt = itemLayout.DataType;
        var alignment = layout.getAlignment(dt);

        var axisRescalePts = itemLayout as A2LRECORD_LAYOUT.AxisRescaleLayoutDesc;
        if (axisRescalePts != null)
        {
          switch (axisRescalePts.AdressType)
          {
            case ADDR_TYPE.DIRECT:
              offset = Extensions.alignUp(offset, alignment);  // Alignment
              offset += dt.getSizeInByte() * 2 * axisRescalePts.MaxNoRescalePairs;
              continue;
          }
        }

        var axisPts = itemLayout as A2LRECORD_LAYOUT.AxisPtsLayoutDesc;
        if (axisPts != null)
        {
          switch (axisPts.AdressType)
          {
            case ADDR_TYPE.DIRECT:
              offset = Extensions.alignUp(offset, alignment);  // Alignment
              offset += dt.getSizeInByte() * (axisDescs != null ? axisDescs[axisPts.AxisIdx].MaxAxisPoints : maxAxisPts);
              break;
            case ADDR_TYPE.PBYTE: ++offset; break;
            case ADDR_TYPE.PWORD:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.UWORD));  // Alignment
              offset += 2; break;
            case ADDR_TYPE.PLONG:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.ULONG));  // Alignment
              offset += 4; break;
            case ADDR_TYPE.PLONGLONG:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.A_UINT64));  // Alignment
              offset += 8; break;
          }
          continue;
        }

        var noAxisPts = itemLayout as A2LRECORD_LAYOUT.NoAxisPtsLayoutDesc;
        if (noAxisPts != null)
        {
          offset = Extensions.alignUp(offset, alignment);  // Alignment
          offset += dt.getSizeInByte();
          continue;
        }

        var fncValues = itemLayout as A2LRECORD_LAYOUT.FncValuesLayoutDesc;
        if (fncValues != null)
        {
          switch (fncValues.AdressType)
          {
            case ADDR_TYPE.DIRECT:
              offset = Extensions.alignUp(offset, alignment);  // Alignment
              var zSize = dt.getSizeInByte();
              if (axisDescs != null)
              { // axis available
                for (int z = axisDescs.Count - 1; z >= 0; --z)
                { // for each axis definition
                  var countValues = axisDescs[z].MaxAxisPoints;
                  switch (fncValues.IndexMode)
                  {
                    case INDEX_MODE.ALTERNATE_WITH_X:
                      if (z == 0)
                        // add x axis values to the count
                        countValues *= 2;
                      break;
                    case INDEX_MODE.ALTERNATE_WITH_Y:
                      if (z == 1)
                        // add y axis values to the count
                        countValues *= 2;
                      break;
                  }
                  // multiply the value size with the axis values
                  zSize *= countValues;
                }
              }
              offset += zSize;
              break;
            case ADDR_TYPE.PBYTE: ++offset; break;
            case ADDR_TYPE.PWORD:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.UWORD));  // Alignment
              offset += 2; break;
            case ADDR_TYPE.PLONG:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.ULONG));  // Alignment
              offset += 4; break;
            case ADDR_TYPE.PLONGLONG:
              offset = Extensions.alignUp(offset, layout.getAlignment(DATA_TYPE.A_UINT64));  // Alignment
              offset += 8; break;
          }
          continue;
        }
      }
      return offset;
    }
    public sealed override int getMemorySize()
    {
      return getSize();
    }
    #endregion
  }
  /// <summary>
  /// Base class for dictionary based compu methods.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public abstract partial class A2LCOMPU_TAB_BASE : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    internal A2LCOMPU_TAB_BASE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    [Category(Constants.CATData)]
    public CONVERSION_TYPE ConversionType { get; set; }
    [Category(Constants.CATData)]
    public string DefaultValue { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      ConversionType = parse2Enum<CONVERSION_TYPE>(parameterList[i++], parser);
      return true;
    }
    /// <summary>
    /// Gets the referencing A2LCONVERSION_REFs.
    /// </summary>
    /// <returns>May be null or empty if not referenced by any node.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var compuMethod in project.CompDict.Values)
      {
        if (compuMethod.RefCompuTab == this)
          refList.Add(compuMethod);
      }
      return refList.ToArray();
    }
    #endregion
    #region IA2LDescription Members
    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    { return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines); }
    #endregion
  }
  #endregion
}