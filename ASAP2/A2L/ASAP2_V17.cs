﻿/*!
 * @file    
 * @brief   Implements the ASAP2 object model (ASAP2 v1.7.0 definitions).
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2017
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Helpers;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Xml.Serialization;

namespace jnsoft.ASAP2
{
  /// <summary>
  /// Base class for all A2L typedef entities.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public abstract partial class A2LTYPEDEF : A2LNAMEDNODE, IA2LDescription
  {
    #region constructor
    protected A2LTYPEDEF(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
    public string Description { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Description = parameterList[i++].Replace("\\r\\n", "\n").Replace("\\n", "\n");
      return true;
    }

    public string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      return base.getDescriptionAndAnnotation(Description, addUserObjects, maxLines);
    }

    /// <summary>
    /// Gets the referencing A2LTYPEDEFs.
    /// </summary>
    /// <returns>List of referenced TYPEDEFs.</returns>
    public sealed override A2LNODE[] getRefNodes()
    {
      var project = getParent<A2LPROJECT>();
      var refList = new List<A2LNODE>();
      foreach (var instance in project.InstanceDict.Values)
      {
        if (instance.RefTypedef != this)
          continue;
        refList.Add(instance);
      }

      foreach (var module in project.getNodeList<A2LMODULE>(false))
        foreach (var node in module.Childs)
        {
          if (!(node is A2LTYPEDEF_STRUCTURE))
            continue;
          foreach (var subNode in node.Childs)
          {
            if (!(subNode is A2LSTRUCTURE_COMPONENT) || ((A2LSTRUCTURE_COMPONENT)subNode).RefTypedef != this)
              continue;
            refList.Add(subNode);
          }
        }
      return refList.ToArray();
    }
    #endregion
  }
  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public partial class A2LTYPEDEF_BLOB : A2LTYPEDEF
  {
    #region constructor
    internal A2LTYPEDEF_BLOB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the size of the adjustable object in the emulation memory.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public uint Size { get; set; }
    [Category(Constants.CATData), DefaultValue(ADDR_TYPE.DIRECT)]
    public ADDR_TYPE AddrType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      Size = (uint)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_ADDRESS_TYPE: AddrType = parse2Enum<ADDR_TYPE>(parameterList[i++], parser); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTYPEDEF_STRUCTURE : A2LTYPEDEF_BLOB
  {
    #region constructor
    internal A2LTYPEDEF_STRUCTURE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ConsistentExchange { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      while (i < parameterList.Count)
        switch (parameterList[i++])
        {
          case A2LC_CONSISTENT_EXCHANGE: ConsistentExchange = true; break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all A2L typedef entities referencing an A2L_COMPUMETHOD method.
  /// </summary>
  public abstract partial class A2LTYPEDEF_CONVERSION_REF : A2LTYPEDEF
  {
    #region constructor
    protected A2LTYPEDEF_CONVERSION_REF(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// see A2LCONVERSION_REF.Conversion.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCConversionRef))]
    public string Conversion { get; set; }
    /// <summary>
    /// see A2LCONVERSION_REF.PhysUnit.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCUnitRef))]
    public string PhysUnit { get; set; }
    /// <summary>
    /// see A2LCONVERSION_REF.ByteOrder.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrderWithNotSet))]
    public BYTEORDER_TYPE ByteOrder { get; set; }
    /// <summary>
    /// see A2LCONVERSION_REF.LowerLimit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double LowerLimit { get; set; }
    /// <summary>
    /// see A2LCONVERSION_REF.UpperLimit.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double UpperLimit { get; set; }
    /// <summary>
    /// see A2LCONVERSION_REF.Format.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCFormatRef))]
    public string Format { get; set; }
    #endregion
  }
  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTYPEDEF_MEASUREMENT : A2LTYPEDEF_CONVERSION_REF
  {
    #region constructor
    internal A2LTYPEDEF_MEASUREMENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// see A2LMEASUREMENT.DataType.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public DATA_TYPE DataType { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.Resolution.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(1)]
    public int Resolution { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.Accuracy.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(0.0)]
    public double Accuracy { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.BitMask.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMax)]
    public UInt64 BitMask { get; set; } = UInt64.MaxValue;
    /// <summary>
    /// see A2LMEASUREMENT.ErrorMask.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMin)]
    public UInt64 ErrorMask { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.Discrete.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool Discrete { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.MatrixDim.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.Layout.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(INDEX_MODE.NotSet)]
    public INDEX_MODE Layout { get; set; }
    /// <summary>
    /// see A2LMEASUREMENT.AddrType.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(ADDR_TYPE.DIRECT)]
    public ADDR_TYPE AddrType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      DataType = parse2Enum<DATA_TYPE>(parameterList[i++], parser);
      Conversion = parameterList[i++];
      Resolution = A2LParser.parse2IntVal(parameterList[i++]);
      Accuracy = A2LParser.parse2DoubleVal(parameterList[i++]);
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimit = Math.Min(lwr, upr);
      UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Format: Format = parameterList[i++]; break;
          case A2LC_PhysUnit: PhysUnit = parameterList[i++]; break;
          case A2LC_ByteOrder: ByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_BitMask: BitMask = A2LParser.parse2UInt64Val(parameterList[i++]); break;
          case A2LC_ErrorMask: ErrorMask = A2LParser.parse2UInt64Val(parameterList[i++]); break;
          case A2LC_Discrete: Discrete = true; break;
          case A2LC_Layout: Layout = parse2Enum<INDEX_MODE>(parameterList[i++], parser); break;
          case A2LC_ADDRESS_TYPE: AddrType = parse2Enum<ADDR_TYPE>(parameterList[i++], parser); break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// Base class for all A2L typedef entities referencing an A2L_COMPUMETHOD method and an A2LRECORD_LAYOUT.
  /// 
  /// These derived classes are:
  /// - A2LTYPEDEF_CHARACTERISTIC
  /// - A2LTYPEDEF_AXIS
  /// </summary>
  public abstract partial class A2LTYPEDEF_RECORDLAYOUT_REF : A2LTYPEDEF_CONVERSION_REF
  {
    #region constructor
    protected A2LTYPEDEF_RECORDLAYOUT_REF(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// see A2LRECORD_LAYOUT_REF.RecordLayout.
    /// </summary>
    [Category(Constants.CATData)]
    public string RecordLayout { get; set; }
    /// <summary>
    /// see A2LRECORD_LAYOUT_REF.MaxDiff.
    /// </summary>
    [Category(Constants.CATData)]
    public double MaxDiff { get; set; }
    /// <summary>
    /// see A2LRECORD_LAYOUT_REF.LowerLimitEx.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double LowerLimitEx { get; set; }
    /// <summary>
    /// see A2LRECORD_LAYOUT_REF.UpperLimitEx.
    /// </summary>
    [Category(Constants.CATDataLimit)]
    public double UpperLimitEx { get; set; }
    /// <summary>
    /// see A2LRECORD_LAYOUT_REF.StepSize.
    /// </summary>
    [Category(Constants.CATData)]
    //[DefaultValue(double.NaN)], disabled in case of .NET XML serialization bug
    public double StepSize { get; set; } = double.NaN;
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTYPEDEF_AXIS : A2LTYPEDEF_RECORDLAYOUT_REF
  {
    #region constructor
    DEPOSIT_TYPE mDeposit;
    internal A2LTYPEDEF_AXIS(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// see A2LAXIS_PTS.InputQuantity.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCMeasurementRef)), DefaultValue(null)]
    public string InputQuantity { get; set; }
    /// <summary>
    /// see A2LAXIS_PTS.MaxAxisPoints.
    /// </summary>
    [Category(Constants.CATData)]
    public int MaxAxisPoints { get; set; }
    /// <summary>
    /// see A2LAXIS_PTS.Deposit.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), DefaultValue(DEPOSIT_TYPE.ABSOLUTE)]
    public DEPOSIT_TYPE Deposit
    {
      get
      {
        switch (mDeposit)
        {
          case DEPOSIT_TYPE.NotSet:
            return getParent<A2LMODULE>().getNode<A2LMOD_COMMON>(false).DEPOSIT;
          default:
            return mDeposit;
        }
      }
      set { mDeposit = value; }
    }
    /// <summary>
    /// see A2LAXIS_PTS.Monotony.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(MONOTONY_TYPE.NotSet)]
    public MONOTONY_TYPE Monotony { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      InputQuantity = parameterList[i++];
      RecordLayout = parameterList[i++];
      MaxDiff = A2LParser.parse2DoubleVal(parameterList[i++]);
      Conversion = parameterList[i++];
      MaxAxisPoints = A2LParser.parse2IntVal(parameterList[i++]);
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimitEx = LowerLimit = Math.Min(lwr, upr);
      UpperLimitEx = UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Format: Format = parameterList[i++]; break;
          case A2LC_PhysUnit: PhysUnit = parameterList[i++]; break;
          case A2LC_Deposit: mDeposit = parse2Enum<DEPOSIT_TYPE>(parameterList[i++], parser); break;
          case A2LC_Monotony: Monotony = parse2Enum<MONOTONY_TYPE>(parameterList[i++], parser); break;
          case A2LC_ByteOrder: ByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_StepSize: StepSize = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTYPEDEF_CHARCTERISTIC : A2LTYPEDEF_RECORDLAYOUT_REF
  {
    #region constructor
    internal A2LTYPEDEF_CHARCTERISTIC(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// see A2LCHARACTERISTIC.CharType.
    /// </summary>
    [Category(Constants.CATData)]
    public CHAR_TYPE CharType { get; set; }
    /// <summary>
    /// see A2LCHARACTERISTIC.Bitmask.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt64), mStrHexQWordMax)]
    public UInt64 Bitmask { get; set; } = UInt64.MaxValue;
    /// <summary>
    /// see A2LCHARACTERISTIC.Number.
    /// </summary>
    [Category(Constants.CATData)]
    public int Number { get; set; }
    /// <summary>
    /// see A2LCHARACTERISTIC.Discrete.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool Discrete { get; set; }
    /// <summary>
    /// see A2LCHARACTERISTIC.Encoding.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(EncodingType.ASCII), Description("Encoding to use for ASCII-typed characteristics.")]
    public EncodingType Encoding { get; set; }
    /// <summary>
    /// see A2LCHARACTERISTIC.MatrixDim.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      CharType = parse2Enum<CHAR_TYPE>(parameterList[i++], parser);
      RecordLayout = parameterList[i++];
      MaxDiff = A2LParser.parse2DoubleVal(parameterList[i++]);
      Conversion = parameterList[i++];
      double lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
      double upr = A2LParser.parse2DoubleVal(parameterList[i++]);
      LowerLimitEx = LowerLimit = Math.Min(lwr, upr);
      UpperLimitEx = UpperLimit = Math.Max(lwr, upr);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Discrete: Discrete = true; break;
          case A2LC_Format: Format = parameterList[i++]; break;
          case A2LC_PhysUnit: PhysUnit = parameterList[i++]; break;
          case A2LC_ByteOrder: ByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser); break;
          case A2LC_StepSize: StepSize = A2LParser.parse2DoubleVal(parameterList[i++]); break;
          case A2LC_ENCODING: Encoding = parse2Enum<EncodingType>(parameterList[i++], parser); break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            break;
          case A2LC_Number: Number = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_BitMask: Bitmask = (UInt64)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_DisplayId: DisplayName = parameterList[i++]; break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LBLOB : A2LADDRESS
  {
    #region constructor
    internal A2LBLOB(int startLine) : base(startLine) { }
    /// <summary>
    /// Creates instance from an A2LINSTANCE. 
    /// </summary>
    /// <param name="module">The module to add the instance to</param>
    /// <param name="instance">The instance to create from</param>
    /// <param name="tdBlob">The Blob typedef reference</param>
    internal A2LBLOB(A2LMODULE module, A2LINSTANCE instance, A2LTYPEDEF_BLOB tdBlob)
      : base(module, instance)
    {
      Size = tdBlob.Size;
      AddrType = tdBlob.AddrType;

      addFromInstance(module, A2LType.BLOB, tdBlob);
      module.getParent<A2LPROJECT>().BlobDict[Name] = this;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the size of the adjustable object in the emulation memory.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public uint Size { get; set; }
    [Category(Constants.CATData), DefaultValue(ADDR_TYPE.DIRECT)]
    public ADDR_TYPE AddrType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      Size = A2LParser.parse2UIntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_EcuAdrExt: AddressExtension = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_DisplayId: mDisplayName = parameterList[i++]; break;
          case A2LC_MaxRefresh:
            MaxRefreshUnit = (ScalingUnits)A2LParser.parse2IntVal(parameterList[i++]);
            MaxRefreshRate = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_SymbolLink:
            SymbolLink = parameterList[i++];
            SymbolOffset = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ModelLink: ModelLink = parameterList[i++]; break;
          case A2LC_CalAccess: CalibAccess = parse2Enum<CALIBRATION_ACCESS>(parameterList[i++], parser); break;
          case A2LC_ADDRESS_TYPE: AddrType = parse2Enum<ADDR_TYPE>(parameterList[i++], parser); break;
        }
      }
      return true;
    }
    public override int getMemorySize()
    {
      return Convert.ToInt32(Size);
    }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LINSTANCE : A2LADDRESS
  {
    #region members
    string mTypedefName;
    A2LTYPEDEF mTypeDef;
    #endregion
    #region constructor
    internal A2LINSTANCE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCTypedefRef))]
    public string TypedefName { get { return mTypedefName; } set { mTypedefName = value; mTypeDef = null; } }
    /// <summary>
    /// Gets or sets the layout.
    /// 
    /// For multi-dimensional measurement arrays this 
    /// keyword can be used to specify the layout of the 
    /// array. If the keyword is missing, multi-dimensional 
    /// measurement arrays are interpreted row by row (ROW_DIR).
    /// </summary>
    [Category(Constants.CATData), DefaultValue(INDEX_MODE.NotSet)]
    public INDEX_MODE Layout { get; set; }
    /// <summary>
    /// Gets or sets the READ_WRITE flag.
    /// 
    /// Keyword to mark this measurement object as 'writeable'.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ReadWrite { get; set; }
    /// <summary>
    /// Gets the 3-value (x, y, z) integer matrix dimensions.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    /// <summary>
    /// Gets the direct reference to the corresponding typedef.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LTYPEDEF RefTypedef
    {
      get
      {
        if (null == mTypeDef)
          // Search only once
          getParent<A2LPROJECT>().TypeDefDict.TryGetValue(mTypedefName, out mTypeDef);
        return mTypeDef;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      int i = startIndex + 2;
      mTypedefName = parameterList[i++];
      Address = A2LParser.parse2UIntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Layout: Layout = parse2Enum<INDEX_MODE>(parameterList[i++], parser); break;
          case A2LC_EcuAdrExt: AddressExtension = A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_DisplayId: mDisplayName = parameterList[i++]; break;
          case A2LC_MaxRefresh:
            MaxRefreshUnit = (ScalingUnits)A2LParser.parse2IntVal(parameterList[i++]);
            MaxRefreshRate = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_SymbolLink:
            SymbolLink = parameterList[i++];
            SymbolOffset = A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_ModelLink: ModelLink = parameterList[i++]; break;
          case A2LC_CalAccess: CalibAccess = parse2Enum<CALIBRATION_ACCESS>(parameterList[i++], parser); break;
          case A2LC_ReadWrite: ReadWrite = true; break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
        }
      }
      return true;
    }
    public override int getMemorySize()
    {
      return 0;
    }
    string getDescription()
    {
      string desc = Description;
      if (string.IsNullOrEmpty(desc))
        if (RefTypedef != null)
          return RefTypedef.Description;
      return desc;
    }
    /// <summary>
    /// Gets the referencing A2L instanced nodes.
    /// </summary>
    /// <returns>List of referenced instance nodes.</returns>
    public sealed override A2LNODE[] getRefNodes()
    {
      var refList = new List<A2LNODE>();
      var project = getParent<A2LPROJECT>();
      foreach (var module in project.getNodeList<A2LMODULE>(false))
        foreach (var node in module.Childs)
        {
          if (!node.IsInstanced || ((A2LADDRESS)node).RefInstance != this)
            continue;
          refList.Add(node);
        }
      return refList.ToArray();
    }

    #region IA2LDescription Members
    public override string getDescriptionAndAnnotation(bool addUserObjects, int maxLines)
    {
      return base.getDescriptionAndAnnotation(getDescription(), addUserObjects, maxLines);
    }
    #endregion
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LOVERWRITE : A2LNAMEDNODE
  {
    #region constructor
    internal A2LOVERWRITE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 AxisNo { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCConversionRef))]
    public string Conversion { get; set; }
    [Category(Constants.CATData)]
    public string Format { get; set; }
    [Category(Constants.CATData)]
    public string InputQuantity { get; set; }
    [Category(Constants.CATDataLimit)]
    public double LowerLimit { get; set; }
    [Category(Constants.CATDataLimit)]
    public double UpperLimit { get; set; }
    [Category(Constants.CATDataLimit)]
    public double LowerLimitEx { get; set; }
    [Category(Constants.CATDataLimit)]
    public double UpperLimitEx { get; set; }
    /// <summary>Indicates if Limits are set</summary>
    [XmlIgnore, Browsable(false)]
    public bool LimitsSet { get; private set; }
    /// <summary>Indicates if Extended Limits are set</summary>
    [XmlIgnore, Browsable(false)]
    public bool LimitsExSet { get; private set; }
    /// <summary>
    /// see A2LCONVERSION_REF.PhysUnit.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCUnitRef))]
    public string PhysUnit { get; set; }
    /// <summary>
    /// see A2LAXIS_PTS.Monotony.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(MONOTONY_TYPE.NotSet)]
    public MONOTONY_TYPE Monotony { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      AxisNo = (UInt16)A2LParser.parse2UIntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_CONVERSION: Conversion = parameterList[i++]; break;
          case A2LC_Format: Format = parameterList[i++]; break;
          case A2LC_PhysUnit: PhysUnit = parameterList[i++]; break;
          case A2LC_INPUT_QUANTITY: InputQuantity = parameterList[i++]; break;
          case A2LC_Monotony: Monotony = parse2Enum<MONOTONY_TYPE>(parameterList[i++], parser); break;
          case A2LC_LIMITS:
            var lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            var upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimit = Math.Min(lwr, upr);
            UpperLimit = Math.Max(lwr, upr);
            LimitsSet = true;
            break;
          case A2LC_ExLimits:
            lwr = A2LParser.parse2DoubleVal(parameterList[i++]);
            upr = A2LParser.parse2DoubleVal(parameterList[i++]);
            LowerLimitEx = Math.Min(lwr, upr);
            UpperLimitEx = Math.Max(lwr, upr);
            LimitsExSet = true;
            break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LSTRUCTURE_COMPONENT : A2LNAMEDNODE
  {
    #region members
    string mTypedefName;
    A2LTYPEDEF mTypeDef;
    #endregion
    #region constructor
    internal A2LSTRUCTURE_COMPONENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the address of the adjustable object in the emulation memory.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 AddressOffset { get; set; } = UInt32.MaxValue;
    [Category(Constants.CATData), TypeConverter(typeof(TCTypedefRef))]
    public string TypedefName { get { return mTypedefName; } set { mTypedefName = value; mTypeDef = null; } }
    /// <summary>
    /// Gets or sets the layout.
    /// 
    /// For multi-dimensional measurement arrays this 
    /// keyword can be used to specify the layout of the 
    /// array. If the keyword is missing, multi-dimensional 
    /// measurement arrays are interpreted row by row (ROW_DIR).
    /// </summary>
    [Category(Constants.CATData), DefaultValue(INDEX_MODE.NotSet)]
    public INDEX_MODE Layout { get; set; }
    /// <summary>
    /// Gets the 3-value (x, y, z) integer matrix dimensions.
    /// </summary>
    [Category(Constants.CATData)]
    public int[] MatrixDim { get; set; }
    /// <summary>
    /// Gets the direct reference to the corresponding typedef.
    /// </summary>
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LTYPEDEF RefTypedef
    {
      get
      {
        if (null == mTypeDef)
          // Search only once
          getParent<A2LPROJECT>().TypeDefDict.TryGetValue(mTypedefName, out mTypeDef);
        return mTypeDef;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      mTypedefName = parameterList[i++];
      AddressOffset = A2LParser.parse2UIntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_Layout: Layout = parse2Enum<INDEX_MODE>(parameterList[i++], parser); break;
          case A2LC_MatrixDim: MatrixDim = A2LParser.parseMatrixDim(ref i, ref parameterList); break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed partial class A2LTRANSFORMER : A2LNAMEDNODE
  {
    #region member
    string mInverseTransformer;
    A2LTRANSFORMER mRefInverseTransformer;
    #endregion
    #region constructor
    internal A2LTRANSFORMER(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string Version { get; set; }
    [Category(Constants.CATData)]
    public string Executable32 { get; set; }
    [Category(Constants.CATData)]
    public string Executable64 { get; set; }
    [Category(Constants.CATData)]
    public UInt32 Timeout { get; set; }
    [Category(Constants.CATData)]
    public TriggerType Trigger { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCTransformerRef))]
    public string InverseTransformer { get { return mInverseTransformer; } set { mInverseTransformer = value; mRefInverseTransformer = null; } }
    [XmlIgnore, Category(Constants.CATData), ReadOnly(true)]
    public A2LTRANSFORMER RefInverseTransormer
    {
      get
      {
        if (string.IsNullOrEmpty(InverseTransformer) || InverseTransformer == mNoInverseTransformer)
          return null;
        if (mRefInverseTransformer == null)
          // search only once
          getParent<A2LPROJECT>().TransformerDict.TryGetValue(InverseTransformer, out mRefInverseTransformer);
        return mRefInverseTransformer;
      }
    }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Name = parameterList[i++];
      Version = parameterList[i++];
      Executable32 = parameterList[i++];
      Executable64 = parameterList[i++];
      Timeout = A2LParser.parse2UIntVal(parameterList[i++]);
      Trigger = parse2Enum<TriggerType>(parameterList[i++], parser);
      InverseTransformer = parameterList[i++];
      return true;
    }

    /// <summary>
    /// Gets the referencing A2LTRANSFORMER nodes.
    /// </summary>
    /// <returns>List of referenced Transformer node.</returns>
    public override A2LNODE[] getRefNodes()
    {
      var refList = new List<A2LNODE>();
      if (RefInverseTransormer != null)
        refList.Add(RefInverseTransormer);
      return refList.ToArray();
    }

    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTRANSFORMER_IN_OBJECTS : A2LREFERENCE
  {
    #region constructor
#if ASAP2_WRITER
    internal A2LTRANSFORMER_IN_OBJECTS() { }
#endif
    internal A2LTRANSFORMER_IN_OBJECTS(int startLine) : base(startLine) { }
    #endregion
  }

  /// <summary>
  /// see ASAP2 specification
  /// </summary>
  public sealed partial class A2LTRANSFORMER_OUT_OBJECTS : A2LREFERENCE
  {
    #region constructor
#if ASAP2_WRITER
    internal A2LTRANSFORMER_OUT_OBJECTS() { }
#endif
    internal A2LTRANSFORMER_OUT_OBJECTS(int startLine) : base(startLine) { }
    #endregion
  }
}