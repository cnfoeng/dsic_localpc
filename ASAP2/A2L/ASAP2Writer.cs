﻿/*!
 * @file    
 * @brief   Implements the ASAP2 writer stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using System;
using System.IO;
using System.Text;
using System.Globalization;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.ComponentModel;
using System.Linq;
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;

#if ASAP2_WRITER
namespace jnsoft.ASAP2
{
  /// <summary>
  /// Defines the sort mode to use for the ASAP2 writer.
  /// </summary>
  public enum WriterSortMode
  {
    /// <summary>
    /// Content is written in the same order as read from the source.
    /// </summary>
    [Description("Not ordered (as read from source)")]
    None,
    /// <summary>
    /// Content is sorted by name before it is written to the output file.
    /// </summary>
    [Description("Sort by name")]
    ByName,
    /// <summary>
    /// Content is sorted by type and name, before it is written to the output file.
    /// </summary>
    [Description("Sort by object type")]
    ByTypeAndName,
    /// <summary>
    /// Content is sorted by object address and name, before it is written to the output file.
    /// </summary>
    [Description("Sort by object address")]
    ByAddressAndName,
  }
  partial class A2LNODE
  {
    protected A2LNODE() { }
    /// <summary>
    /// Write the specified value as ASAP2 value.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent to do</param>
    /// <param name="quotes">true if the value should be set into quotes</param>
    /// <param name="tag">The tag to set (may be null)</param>
    /// <param name="value">The value to write</param>
    internal protected static void write(StreamWriter wr, string indent, bool quotes, string tag, string value)
    {
      if (null != tag && null != value)
        wr.WriteLine("{0}{1} {2}{3}{2}", indent, tag, quotes ? "\"" : string.Empty, value);
      else if (null != value)
        wr.WriteLine("{0}{1}{2}{1}", indent, quotes ? "\"" : string.Empty, value);
    }
    /// <summary>
    /// Write the specified value as ASAP2 value.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent to do</param>
    /// <param name="tag">The tag to set (may be null)</param>
    /// <param name="value">The value to write</param>
    internal protected static void writeValue(StreamWriter wr, string indent, string tag, string value)
    {
      if (null != tag && null != value)
        wr.WriteLine("{0}{1} {2}", indent, tag, value);
      else if (null != value)
        wr.WriteLine("{0}{1}", indent, value);
    }
    /// <summary>
    /// Writes a list of string references as ASAP2 references.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent to do</param>
    /// <param name="references">The string references to write</param>
    protected static void writeReferences(StreamWriter wr, string indent, IList<string> references)
    {
      if (references == null)
        return;
      var sb = new StringBuilder(indent);
      var lineBreak = false;
      for (int i = 0; i < references.Count; ++i)
      {
        lineBreak = (i + 1) % Settings.Default.WriterColumns == 0;
        if (lineBreak)
        { // line break
          sb.AppendFormat("{0}{1}", references[i], Environment.NewLine);
          sb.Append(indent);
        }
        else
          sb.AppendFormat("{0} ", references[i]);
      }
      if (sb.Length > 0)
      { // Content available
        if (lineBreak)
        { // remove last linebreak
          int size = Environment.NewLine.Length + indent.Length;
          sb.Remove(sb.Length - size, size);
        }
        else
          // remove blank
          sb.Remove(sb.Length - 1, 1);
      }
      // write all
      wr.WriteLine(sb);
    }
    /// <summary>
    /// Write the MATRIX_DIM depending on the ASAP2 version written.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="major">Major ASAP2 version</param>
    /// <param name="minor">Minor ASAP2 version</param>
    /// <param name="indent">The indent to do</param>
    /// <param name="MatrixDim">The object to write</param>
    protected static void writeMatrixDim(StreamWriter wr, int major, int minor, string indent, int[] MatrixDim)
    {
      var sb = new StringBuilder();
      for (int i = 0; i < MatrixDim.Length; ++i)
      {
        sb.Append(MatrixDim[i].ToString());
        sb.Append(' ');
      }
      if (major < 2 && minor < 70)
        for (int i = 0; i < 3 - MatrixDim.Length; ++i)
        {
          sb.Append(1.ToString());
          sb.Append(' ');
        }
      sb.Remove(sb.Length - 1, 1);
      write(wr, indent, false, A2LC_MatrixDim, sb.ToString());
    }
    /// <summary>
    /// Indicates if the node is write supported.
    /// 
    /// Override this in derived types to prevent from being written to the output file.
    /// </summary>
    internal virtual bool IsWriteSupported
    {
      get
      {
        return StartLine < int.MaxValue && !IsInstanced
          || StartLine == int.MaxValue && Settings.Default.WriteCreatedInstances;
      }
    }
    /// <summary>
    /// Write the begin of an ASAP2 node.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    internal virtual void writeBegin(StreamWriter wr)
    {
      wr.Write("{0}/begin {1}", getIndent(this, false), Type.ToString());
    }
    /// <summary>
    /// Writes the end of an ASAP2 node.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    internal virtual void writeEnd(StreamWriter wr)
    {
      wr.WriteLine("{0}/end {1}", getIndent(this, false), Type.ToString());
    }
    /// <summary>
    /// Convert value into hexadecimal representation.
    /// </summary>
    /// <param name="value">The value to convert</param>
    /// <returns>The hexadecimal representation</returns>
    internal protected static string toHex(UInt64 value) { return "0x" + value.ToString("X"); }
    /// <summary>
    /// Convert value into decimal representation.
    /// </summary>
    /// <param name="value">The value to convert</param>
    /// <returns>The decimal representation</returns>
    internal protected static string toDec(double value) { return value.ToString(CultureInfo.InvariantCulture); }
    /// <summary>
    /// Convert value into decimal representation.
    /// </summary>
    /// <param name="value">The value to convert</param>
    /// <returns>The decimal representation</returns>
    internal protected static string toDec(float value) { return value.ToString(CultureInfo.InvariantCulture); }
    /// <summary>
    /// Write the node's content to the specified stream in ASAP2 notation.
    /// 
    /// - Any supported type should implement a derived method to be successfully written
    /// - Calling this base implementation is required in all derived implementations!
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="major">Major ASAP2 version</param>
    /// <param name="minor">Minor ASAP2 version</param>
    internal virtual void writeToStream(StreamWriter wr, int major, int minor)
    {
      if (Childs != null)
      {
        foreach (var node in Childs)
        { // do all childs
          if (!node.IsWriteSupported)
            // filter out unsupported nodes
            continue;
          switch (node.Parent.Type)
          {
            case A2LType.PROJECT:
            case A2LType.MODULE:
            case A2LType.PREDEFINED:
            case A2LType.ECU_STATES:
            case A2LType.MOD_PAR: wr.WriteLine(); break;
          }
          node.writeBegin(wr);
          node.writeToStream(wr, major, minor);
        }
      }
      writeEnd(wr);
    }
    /// <summary>
    /// Writes a byte array with teh specified name.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent string to write</param>
    /// <param name="name">The byte array's name</param>
    /// <param name="data">The byte array to write</param>
    /// <param name="asHex">true if the array should be written as hex values</param>
    internal protected static void writeByteArray(StreamWriter wr, string indent, string name, byte[] data, bool asHex)
    {
      if (data != null && data.Length > 0)
      {
        StringBuilder sb = new StringBuilder(name + " ");
        string format = asHex ? "0x{0:X2} " : "{0} ";
        foreach (byte b in data)
          sb.AppendFormat(format, b);
        sb.Remove(sb.Length - 1, 1);
        writeValue(wr, indent, null, sb.ToString());
      }
    }
  }
  partial class A2LNAMEDNODE
  {
    protected A2LNAMEDNODE() { }
    internal override void writeBegin(StreamWriter wr)
    {
      base.writeBegin(wr);
      wr.Write(" " + Name);
    }
  }
  partial class A2LUNSUPPORTEDNODE
  {
    protected A2LUNSUPPORTEDNODE() { }
    internal override void writeBegin(StreamWriter wr)
    {
    }
    internal override void writeEnd(StreamWriter wr)
    {
      wr.WriteLine("{0}/end {1}", getIndent(this, false)
        , Type != A2LType.UNSUPPORTED ? Type.ToString() : Name
        );
    }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        wr.Write("{0}/begin {1} ", getIndent(this, false)
          , Type != A2LType.UNSUPPORTED ? Type.ToString() : Name
          );
        if (Content != null && Content.Length > 0)
          wr.WriteLine(Content);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LA2ML
  {
    internal A2LA2ML() { }
    internal override bool IsWriteSupported { get { return Settings.Default.WriteA2ML; } }
  }
  partial class A2LPROJECT
  {
    internal A2LPROJECT() { }
    /// <summary>
    /// Start writing the complete model to the specified stream.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="additionalComments">Addition comments (may be null or empty)</param>
    /// <param name="major">Major ASAP2 version</param>
    /// <param name="minor">Minor ASAP2 version</param>
    internal void writeToStream(StreamWriter wr, string additionalComments, int major, int minor)
    {
      try
      {
        var assembly = Assembly.GetExecutingAssembly();
        var product = ((AssemblyProductAttribute)Attribute.GetCustomAttribute(assembly, typeof(AssemblyProductAttribute), false)).Product;
        major = major == -1 ? 1 : major;
        minor = minor == -1 ? 70 : minor;
        wr.WriteLine(Resources.strWriterHeader
          , product, A2LParser.Version
          , DateTime.Now.ToShortDateString()
          , DateTime.Now.ToShortTimeString()
          , string.IsNullOrEmpty(additionalComments)
            ? string.Empty
            : string.Format("\n   {0}", additionalComments)
          , major, minor
          );
        writeBegin(wr);
        string indent = getIndent(this, true);
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
    /// <summary>
    /// Sorts the ASAP2 object model with the specified sort mode.
    /// </summary>
    /// <param name="sortMode">The sort mode to apply</param>
    internal void sortModel(WriterSortMode sortMode)
    {
      switch (sortMode)
      {
        case WriterSortMode.ByName:
          Childs.Sort(A2LNODE.sortByName);
          getNode<A2LMODULE>(false).Childs.Sort(A2LNODE.sortByName);
          break;
        case WriterSortMode.ByTypeAndName:
          Childs.Sort(A2LNODE.sortByTypeAndName);
          getNode<A2LMODULE>(false).Childs.Sort(A2LNODE.sortByTypeAndName);
          break;
        case WriterSortMode.ByAddressAndName:
          Childs.Sort(A2LNODE.sortByAddressAndName);
          getNode<A2LMODULE>(false).Childs.Sort(A2LNODE.sortByAddressAndName);
          break;
      }
    }
    /// <summary>
    /// Optimize the model.
    /// 
    /// - Removes redundant objects
    /// </summary>
    internal void optimizeModel(A2LParser parser, bool optimizeCaseInsensitive)
    {
      StringComparison optimizeComparison = optimizeCaseInsensitive
        ? StringComparison.OrdinalIgnoreCase
        : StringComparison.Ordinal;

      // A2L RecordLayouts
      optimizeRecordLayouts(parser, optimizeComparison);

      // A2L CompuVTab
      optimizeCompuVTABs(parser, optimizeComparison);

      // A2L CompuMethods
      optimizeComputMethods(parser, optimizeComparison);
    }
    /// <summary>
    /// Optimizes the A2LCOMPU_METHOD references.
    /// </summary>
    /// <param name="parser">The corresponding parser instance</param>
    /// <param name="optimizeComparison">The comparison method</param>
    void optimizeComputMethods(A2LParser parser, StringComparison optimizeComparison)
    {
      var currentDict = (A2LCompDict)CompDict.Clone();
      var enComp = CompDict.GetEnumerator();
      while (enComp.MoveNext())
      {
        var compuMethod = enComp.Current.Value;
        var refNodes = compuMethod.getRefNodes();
        if (refNodes == null || refNodes.Length == 0)
        {
          compuMethod.Parent.Childs.Remove(compuMethod);
          currentDict.Remove(compuMethod.Name);
          parser.onParserMessage(new ParserEventArgs(compuMethod
            , MessageType.Info
            , string.Format(Resources.strOptimizeRemoveUnref, compuMethod.Name)
            ));
          continue;
        }

        string replaceInstance;
        if (!compuMethod.isRedundant(currentDict, optimizeComparison, out replaceInstance))
          // non redundant object
          continue;

        parser.onParserMessage(new ParserEventArgs(compuMethod
          , MessageType.Info
          , string.Format(Resources.strOptimizeReplaced, replaceInstance)
          ));

        // adjust the referencing nodes
        foreach (A2LCONVERSION_REF convRef in refNodes)
        {
          if (!(convRef is A2LCHARACTERISTIC))
            convRef.Conversion = replaceInstance;
          else
          { // characteristic
            if (convRef.Conversion == compuMethod.Name)
              convRef.Conversion = replaceInstance;
            foreach (var axisDesc in convRef.enumChildNodes<A2LAXIS_DESCR>())
            {
              if (axisDesc.Conversion == compuMethod.Name)
                axisDesc.Conversion = replaceInstance;
            }
          }
        }
        // remove redundant instance from parent
        compuMethod.Parent.Childs.Remove(compuMethod);
        // remove from dictionary
        currentDict.Remove(compuMethod.Name);
      }

      // Set the new dictionary
      CompDict = currentDict;
    }
    /// <summary>
    /// Optimizes the A2LRECORD_LAYOUT references.
    /// </summary>
    /// <param name="parser">The corresponding parser instance</param>
    /// <param name="optimizeComparison">The comparison method</param>
    void optimizeRecordLayouts(A2LParser parser, StringComparison optimizeComparison)
    {
      var currentDict = (A2LRecLayDict)RecLayDict.Clone();
      var enRL = RecLayDict.GetEnumerator();
      while (enRL.MoveNext())
      {
        var recLay = enRL.Current.Value;
        var refNodes = recLay.getRefNodes();
        if (refNodes == null || refNodes.Length == 0)
        {
          recLay.Parent.Childs.Remove(recLay);
          currentDict.Remove(recLay.Name);
          parser.onParserMessage(new ParserEventArgs(recLay
            , MessageType.Info
            , string.Format(Resources.strOptimizeRemoveUnref, enRL.Current.Key)
            ));
          continue;
        }
        string replaceInstance;
        if (!recLay.isRedundant(currentDict, optimizeComparison, out replaceInstance))
          // non redundant object
          continue;

        parser.onParserMessage(new ParserEventArgs(recLay
          , MessageType.Info
          , string.Format(Resources.strOptimizeReplaced, replaceInstance)
          ));

        // adjust the referencing nodes
        foreach (A2LRECORD_LAYOUT_REF recLayRef in refNodes)
          recLayRef.RecordLayout = replaceInstance;

        // remove redundant instance from parent
        recLay.Parent.Childs.Remove(recLay);
        // remove from dictionary
        currentDict.Remove(recLay.Name);
      }

      // Set the new dictionary
      RecLayDict = currentDict;
    }
    /// <summary>
    /// Optimizes the A2LCOMPU_VTAB references.
    /// </summary>
    /// <param name="parser">The corresponding parser instance</param>
    /// <param name="optimizeComparison">The comparison method</param>
    void optimizeCompuVTABs(A2LParser parser, StringComparison optimizeComparison)
    {
      var currentDict = (A2LCompVTabDict)CompVTabDict.Clone();
      var enVTAB = CompVTabDict.GetEnumerator();
      while (enVTAB.MoveNext())
      {
        var vTab = enVTAB.Current.Value;
        var refNodes = vTab.getRefNodes();
        if (refNodes == null || refNodes.Length == 0)
        {
          vTab.Parent.Childs.Remove(vTab);
          currentDict.Remove(vTab.Name);
          parser.onParserMessage(new ParserEventArgs(vTab
            , MessageType.Info
            , string.Format(Resources.strOptimizeRemoveUnref, vTab.Name)
            ));
          continue;
        }
        string replaceInstance;
        if (!vTab.isRedundant(currentDict, optimizeComparison, out replaceInstance))
          // non redundant object
          continue;

        parser.onParserMessage(new ParserEventArgs(vTab
          , MessageType.Info
          , string.Format(Resources.strOptimizeReplaced, replaceInstance)
          ));

        // adjust the referencing nodes
        foreach (A2LCOMPU_METHOD compuMethod in refNodes)
          compuMethod.CompuTabRef = replaceInstance;

        // remove redundant instance from parent
        vTab.Parent.Childs.Remove(vTab);
        // remove from dictionary
        currentDict.Remove(vTab.Name);
      }
      // Set the new dictionary
      CompVTabDict = currentDict;
    }
  }
  partial class A2LMODULE
  {
    internal A2LMODULE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LHEADER
  {
    internal A2LHEADER() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Comment.Replace("\"", "\\\""));
        if (!string.IsNullOrEmpty(VERSION))
          write(wr, indent, true, A2LC_Version, VERSION);
        if (!string.IsNullOrEmpty(PROJECT_NO))
          write(wr, indent, false, A2LC_ProjectNo, PROJECT_NO);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LMOD_COMMON
  {
    internal A2LMOD_COMMON() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Comment);
        write(wr, indent, false, A2LC_ByteOrder, mByteOrder.ToString());
        if (DEPOSIT != DEPOSIT_TYPE.NotSet)
          write(wr, indent, false, A2LC_Deposit, DEPOSIT.ToString());
        if (DATA_SIZE > 0)
          write(wr, indent, false, A2LC_DataSize, DATA_SIZE.ToString());
        for (int i = 0; i < Alignments.Length; ++i)
          wr.WriteLine($"{indent}ALIGNMENT_{(i == 0 ? "BYTE" : i == 1 ? "WORD" : i == 2 ? "LONG" : i == 3 ? "FLOAT32_IEEE" : i == 4 ? "FLOAT64_IEEE" : "INT64")} {Alignments[i]}");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LMODPAR
  {
    internal A2LMODPAR() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        if (!string.IsNullOrEmpty(Version))
          write(wr, indent, true, A2LC_Version, Version);
        if (!string.IsNullOrEmpty(Supplier))
          write(wr, indent, true, A2LC_Supplier, Supplier);
        if (!string.IsNullOrEmpty(Customer))
          write(wr, indent, true, A2LC_Customer, Customer);
        if (!string.IsNullOrEmpty(CustomerNo))
          write(wr, indent, true, A2LC_CustomerNo, CustomerNo);
        if (!string.IsNullOrEmpty(User))
          write(wr, indent, true, A2LC_User, User);
        if (!string.IsNullOrEmpty(PhoneNo))
          write(wr, indent, true, A2LC_PhoneNo, PhoneNo);
        if (!string.IsNullOrEmpty(ECU))
          write(wr, indent, true, A2LC_Ecu, ECU);
        if (!string.IsNullOrEmpty(CPU))
          write(wr, indent, true, A2LC_CpuType, CPU);
        if (!string.IsNullOrEmpty(EPK))
          write(wr, indent, true, A2LC_Epk, EPK);
        if (EPKAddress != UInt32.MaxValue)
          writeValue(wr, indent, A2LC_AddrEpk, toHex(EPKAddress));
        if (NoOfInterfaces > -1)
          writeValue(wr, indent, A2LC_NoOfIFs, NoOfInterfaces.ToString());
        if (ECUCalibrationOffset > 0)
          writeValue(wr, indent, A2LC_EcuCalOffs, ECUCalibrationOffset.ToString());

        var en = SystemConstants.GetEnumerator();
        while (en.MoveNext())
          wr.WriteLine($"{indent}{A2LC_SysConst} \"{en.Current.Key}\" \"{en.Current.Value}\"");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LMEMORY_LAYOUT
  {
    internal A2LMEMORY_LAYOUT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        var sb = new StringBuilder();
        sb.Append($"{PrgType} {toHex(Address)} {toHex(Size)} ");
        for (int i = 0; i < Offsets.Length; ++i)
          sb.Append($"{Offsets[i]} ");
        sb.Remove(sb.Length - 1, 1);
        writeValue(wr, indent, null, sb.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LREFBASE
  {
    protected A2LREFBASE() { }
  }
  partial class A2LFUNCTION
  {
    internal A2LFUNCTION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        if (!string.IsNullOrEmpty(Version))
          write(wr, indent, true, "FUNCTION_VERSION", Version);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LGROUP
  {
    internal A2LGROUP() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        if (Root)
          write(wr, indent, false, null, "ROOT");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LREFERENCE
  {
    protected A2LREFERENCE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeReferences(wr, indent, References);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LMEASUREMENT
  {
    internal A2LMEASUREMENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, DataType.ToString());
        writeValue(wr, indent, null, mConversion);
        writeValue(wr, indent, null, $"{Resolution} {toDec(Accuracy)}");

        writeBaseValues(wr, indent);
        if (Layout != INDEX_MODE.NotSet)
          write(wr, indent, false, A2LC_Layout, Layout.ToString());
        if (Address != uint.MaxValue)
          write(wr, indent, false, A2LC_EcuAdr, toHex(Address));
        if (BitMask != UInt64.MaxValue)
          write(wr, indent, false, A2LC_BitMask, toHex(BitMask));
        if (ErrorMask != 0)
          write(wr, indent, false, A2LC_ErrorMask, toHex(ErrorMask));
        if (Discrete)
          write(wr, indent, false, null, A2LC_Discrete);
        if (ReadWrite)
          write(wr, indent, false, null, A2LC_ReadWrite);
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
        if (AddrType != ADDR_TYPE.DIRECT)
          write(wr, indent, false, A2LC_ADDRESS_TYPE, AddrType.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LADDRESS
  {
    protected A2LADDRESS() { }
    /// <summary>
    /// Writes the basic values of conversion references to the output stream.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent to do</param>
    internal protected virtual void writeBaseValues(StreamWriter wr, string indent)
    {
      if (AddressExtension != 0)
        write(wr, indent, false, A2LC_EcuAdrExt, AddressExtension.ToString());
      if (!string.IsNullOrEmpty(mDisplayName))
        write(wr, indent, false, A2LC_DisplayId, mDisplayName);
      if (CalibAccess != CALIBRATION_ACCESS.NotSet)
        write(wr, indent, false, A2LC_CalAccess, CalibAccess.ToString());
      if (MaxRefreshUnit != ScalingUnits.NotSet)
        write(wr, indent, false, A2LC_MaxRefresh, $"{((int)MaxRefreshUnit).ToString()} {MaxRefreshRate}");
      if (!string.IsNullOrEmpty(SymbolLink))
        write(wr, indent, false, A2LC_SymbolLink, $"\"{SymbolLink}\" {SymbolOffset}");
      if (!string.IsNullOrEmpty(ModelLink))
        write(wr, indent, true, A2LC_ModelLink, ModelLink);
    }
  }
  partial class A2LCONVERSION_REF
  {
    protected A2LCONVERSION_REF() { }
    /// <summary>
    /// Writes the basic values of conversion references to the output stream.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    /// <param name="indent">The indent to do</param>
    internal protected override void writeBaseValues(StreamWriter wr, string indent)
    {
      writeValue(wr, indent, null, $"{toDec(LowerLimit)} {toDec(UpperLimit)}");
      base.writeBaseValues(wr, indent);
      if (!string.IsNullOrEmpty(mFormat) && !mFormat.Equals(RefCompuMethod.Format, StringComparison.OrdinalIgnoreCase))
        write(wr, indent, true, A2LC_Format, mFormat);
      if (!string.IsNullOrEmpty(mPhysUnit) && !mPhysUnit.Equals(RefCompuMethod.Unit, StringComparison.OrdinalIgnoreCase))
        write(wr, indent, true, A2LC_PhysUnit, mPhysUnit);
      if (mByteOrder != BYTEORDER_TYPE.NotSet)
        write(wr, indent, false, A2LC_ByteOrder, mByteOrder.ToString());
      if (!string.IsNullOrEmpty(mMemorySegmentRef))
        write(wr, indent, false, A2LC_RefMemSeg, mMemorySegmentRef);
    }
  }
  partial class A2LRECORD_LAYOUT_REF
  {
    protected A2LRECORD_LAYOUT_REF() { }
    internal protected sealed override void writeBaseValues(StreamWriter wr, string indent)
    {
      base.writeBaseValues(wr, indent);
      if (ReadOnly)
        writeValue(wr, indent, null, A2LC_ReadOnly);
      if (LowerLimitEx != LowerLimit || UpperLimit != UpperLimitEx)
        writeValue(wr, indent, null, $"{A2LC_ExLimits} {toDec(LowerLimitEx)} {toDec(UpperLimitEx)}");
      if (!double.IsNaN(StepSize))
        writeValue(wr, indent, A2LC_StepSize, toDec(StepSize));
      if (GuardRails)
        writeValue(wr, indent, null, A2LC_GuardRails);
    }
  }
  partial class A2LCHARACTERISTIC
  {
    internal A2LCHARACTERISTIC() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, CharType.ToString());
        writeValue(wr, indent, null, toHex(Address));
        writeValue(wr, indent, null, mRecordLayout);
        writeValue(wr, indent, null, toDec(MaxDiff));
        writeValue(wr, indent, null, mConversion);
        writeBaseValues(wr, indent);
        if (Bitmask != UInt64.MaxValue)
          writeValue(wr, indent, A2LC_BitMask, toHex(Bitmask));
        if (Number != 0)
          write(wr, indent, false, A2LC_Number, Number.ToString());
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
        if (!string.IsNullOrEmpty(mComparisonQuantity))
          write(wr, indent, false, null, mComparisonQuantity);
        if (Discrete)
          write(wr, indent, false, null, A2LC_Discrete);
        if (Encoding != EncodingType.ASCII)
          write(wr, indent, false, A2LC_ENCODING, Encoding.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LAXIS_DESCR
  {
    internal A2LAXIS_DESCR() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, AxisType.ToString());
        writeValue(wr, indent, null, mInputQuantity);
        writeValue(wr, indent, null, mConversion);
        writeValue(wr, indent, null, toDec(MaxAxisPoints));
        writeValue(wr, indent, null, $"{toDec(LowerLimit)} {toDec(UpperLimit)}");
        if (!string.IsNullOrEmpty(mAxisPtsRef))
          writeValue(wr, indent, A2LC_AxisPtsRef, mAxisPtsRef);
        if (!string.IsNullOrEmpty(mCurveAxisRef))
          writeValue(wr, indent, A2LC_CurveAxisRef, mCurveAxisRef);
        if (ReadOnly)
          writeValue(wr, indent, null, A2LC_ReadOnly);
        if (LowerLimitEx != LowerLimit || UpperLimit != UpperLimitEx)
          writeValue(wr, indent, null, $"{A2LC_ExLimits} {toDec(LowerLimitEx)} {toDec(UpperLimitEx)}");
        if (!double.IsNaN(MaxGrad))
          writeValue(wr, indent, A2LC_MaxGrad, toDec(MaxGrad));
        if (!double.IsNaN(StepSize))
          writeValue(wr, indent, A2LC_StepSize, toDec(StepSize));
        if (!string.IsNullOrEmpty(mFormat))
          write(wr, indent, true, A2LC_Format, mFormat);
        if (!string.IsNullOrEmpty(mPhysUnit))
          write(wr, indent, true, A2LC_PhysUnit, mPhysUnit);
        if (mByteOrder != BYTEORDER_TYPE.NotSet)
          write(wr, indent, false, A2LC_ByteOrder, mByteOrder.ToString());
        if (Monotony != MONOTONY_TYPE.NotSet)
          write(wr, indent, false, A2LC_Monotony, Monotony.ToString());
        if (mDeposit != DEPOSIT_TYPE.NotSet)
          write(wr, indent, false, A2LC_Deposit, mDeposit.ToString());
        if (mFixAxisPar != null)
          write(wr, indent, false, A2LC_FixAxisPar, $"{mFixAxisPar.Offset} {mFixAxisPar.Shift} {mFixAxisPar.Numberapo}");
        if (mFixAxisParDist != null)
          write(wr, indent, false, A2LC_FixAxisParDist, $"{mFixAxisParDist.Offset} {mFixAxisParDist.Distance} {mFixAxisParDist.Numberapo}");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LAXIS_PTS
  {
    internal A2LAXIS_PTS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, toHex(Address));
        writeValue(wr, indent, null, InputQuantity);
        writeValue(wr, indent, null, mRecordLayout);
        writeValue(wr, indent, null, toDec(MaxDiff));
        writeValue(wr, indent, null, mConversion);
        writeValue(wr, indent, null, MaxAxisPoints.ToString());
        writeBaseValues(wr, indent);
        if (Monotony != MONOTONY_TYPE.NotSet)
          write(wr, indent, false, A2LC_Monotony, Monotony.ToString());
        if (mDeposit != DEPOSIT_TYPE.NotSet)
          write(wr, indent, false, A2LC_Deposit, mDeposit.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LRECORD_LAYOUT
  {
    internal A2LRECORD_LAYOUT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();

        if (mAlignments != null)
        {
          for (int i = 0; i < mAlignments.Length; ++i)
            wr.WriteLine($"{indent}ALIGNMENT_{(i == 0 ? "BYTE" : i == 1 ? "WORD" : i == 2 ? "LONG" : i == 3 ? "FLOAT32_IEEE" : i == 4 ? "FLOAT64_IEEE" : "INT64")} {mAlignments[i]}");
        }

        for (int i = 0; i < FixNoAxisPts.Length; ++i)
        {
          int ad = FixNoAxisPts[i];
          if (ad == -1)
            continue;
          write(wr, indent, false, $"FIX_NO_AXIS_PTS_{(i == 0 ? 'X' : i == 1 ? 'Y' : i == 2 ? 'Z' : i == 3 ? '4' : '5')}", ad.ToString());
        }

        foreach (var item in LayoutEntries)
          write(wr, indent, false, null, item.ToString());

        if (Identification != null)
          write(wr, indent, false, "IDENTIFICATION", $"{Identification.Position} {Identification.DataType}");
        if (StaticRecordLayout)
          writeValue(wr, indent, null, A2LC_STATIC_RECORD_LAYOUT);
        if (StaticAddressOffsets)
          writeValue(wr, indent, null, A2LC_STATIC_ADDRESS_OFFSETS);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
    /// <summary>
    /// Tests, if the supplied instances are the same.
    /// </summary>
    /// <param name="y">The compare to instance</param>
    /// <returns>true, if the instances are the same</returns>
    bool equals(A2LRECORD_LAYOUT y, StringComparison comparison)
    {
      if (StaticRecordLayout != y.StaticRecordLayout)
        return false;
      if (FncValues == null && y.FncValues != null
        || FncValues != null && !FncValues.equals(y.FncValues)
        )
        return false;
      if (AxisRescaleX != null && y.AxisRescaleX != null && !AxisRescaleX.equals(y.AxisRescaleX))
        return false;
      if (NoRescaleX != null && y.NoRescaleX != null && !NoRescaleX.equals(y.NoRescaleX))
        return false;
      if (Reserved != null && y.Reserved != null && !Reserved.SequenceEqual(y.Reserved))
        return false;
      if (Identification != null && y.Identification != null && !Identification.equals(y.Identification))
        return false;
      if (RipAddrW != null && y.RipAddrW != null && !RipAddrW.equals(y.RipAddrW))
        return false;

      for (int i = 0; i < 5; ++i)
      {
        if (mAlignments != null && y.mAlignments != null)
        {
          if (0 != mAlignments[i].CompareTo(y.mAlignments[i]))
            return false;
        }
        if (0 != FixNoAxisPts[i].CompareTo(y.FixNoAxisPts[i]))
          return false;
        if (AxisPts[i] == null && y.AxisPts[i] != null
          || AxisPts[i] != null && !AxisPts[i].equals(y.AxisPts[i])
          )
          return false;
        if (NoAxisPts[i] == null && y.NoAxisPts[i] != null
          || NoAxisPts[i] != null && !NoAxisPts[i].equals(y.NoAxisPts[i])
          )
          return false;
        if (SrcAddress[i] == null && y.SrcAddress[i] != null
          || SrcAddress[i] != null && !SrcAddress[i].equals(y.SrcAddress[i])
          )
          return false;
        if (Offset[i] == null && y.Offset[i] != null
          || Offset[i] != null && !Offset[i].equals(y.Offset[i])
          )
          return false;
        if (DistOp[i] == null && y.DistOp[i] != null
          || DistOp[i] != null && !DistOp[i].equals(y.DistOp[i])
          )
          return false;
        if (RipAddr[i] == null && y.RipAddr[i] != null
          || RipAddr[i] != null && !RipAddr[i].equals(y.RipAddr[i])
          )
          return false;
        if (ShiftOp[i] == null && y.ShiftOp[i] != null
          || ShiftOp[i] != null && !ShiftOp[i].equals(y.ShiftOp[i])
          )
          return false;
      }
      return true;
    }
    /// <summary>
    /// Test is the current instance is redundant.
    /// </summary>
    /// <param name="dict">The test dictionary (adjusted for each call)</param>
    /// <param name="comparison">string comparison definition</param>
    /// <param name="replaceInstance">Iff return value is true, the replacing instance</param>
    /// <returns>true if the instance is redundant</returns>
    internal bool isRedundant(A2LRecLayDict dict, StringComparison comparison, out string replaceInstance)
    {
      replaceInstance = null;
      var en = dict.GetEnumerator();
      while (en.MoveNext())
      {
        if (en.Current.Value == this)
          // ourself, the end
          return replaceInstance != null;
        if (!equals(en.Current.Value, comparison))
          continue;
        replaceInstance = en.Current.Key;
        return true;
      }
      return false;
    }
  }
  partial class A2LCOMPU_METHOD
  {
    internal A2LCOMPU_METHOD() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, ConversionType.ToString());
        write(wr, indent, false, null, $"\"{Format}\" \"{mUnit}\"");
        switch (ConversionType)
        {
          case CONVERSION_TYPE.LINEAR:
            write(wr, indent, false, A2LC_CoeffsLinear, $"{toDec(Coeffs.b)} {toDec(Coeffs.c)}");
            break;
          case CONVERSION_TYPE.RAT_FUNC:
            write(wr, indent, false, A2LC_Coeffs
              , $"{toDec(Coeffs.a)} {toDec(Coeffs.b)} {toDec(Coeffs.c)} {toDec(Coeffs.d)} {toDec(Coeffs.e)} {toDec(Coeffs.f)}"
              );
            break;
          case CONVERSION_TYPE.TAB_INTP:
          case CONVERSION_TYPE.TAB_NOINTP:
          case CONVERSION_TYPE.TAB_VERB:
            write(wr, indent, false, A2LC_CompuTabRef, CompuTabRef);
            break;
          case CONVERSION_TYPE.FORM:
          case CONVERSION_TYPE.IDENTICAL: break;
        }
        if (!string.IsNullOrEmpty(StatusStringRef))
          write(wr, indent, false, A2LC_StatStrRef, StatusStringRef);
        if (!string.IsNullOrEmpty(mRefUnit))
          write(wr, indent, true, A2LC_RefUnit, mRefUnit);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
    /// <summary>
    /// Tests, if the supplied instances are the same.
    /// </summary>
    /// <param name="y">The compare to instance</param>
    /// <returns>true, if the instances are the same</returns>
    bool equals(A2LCOMPU_METHOD y, StringComparison comparison)
    {
      if (ConversionType != y.ConversionType)
        // different types
        return false;
      if (0 != string.Compare(Format, y.Format, StringComparison.OrdinalIgnoreCase))
        // different format
        return false;
      if (0 != string.Compare(mUnit, y.mUnit, comparison))
        // different unit
        return false;
      switch (ConversionType)
      {
        case CONVERSION_TYPE.IDENTICAL: return true;
        case CONVERSION_TYPE.LINEAR:
        case CONVERSION_TYPE.RAT_FUNC:
          return Coeffs.a == y.Coeffs.a
            && Coeffs.b == y.Coeffs.b
            && Coeffs.c == y.Coeffs.c
            && Coeffs.d == y.Coeffs.d
            && Coeffs.e == y.Coeffs.e
            && Coeffs.f == y.Coeffs.f;
      }
      return false;
    }
    /// <summary>
    /// Test is the current instance is redundant.
    /// </summary>
    /// <param name="dict">The test dictionary (adjusted for each call)</param>
    /// <param name="comparison">string comparison definition</param>
    /// <param name="replaceInstance">Iff return value is true, the replacing instance</param>
    /// <returns>true if the instance is redundant</returns>
    internal bool isRedundant(A2LCompDict dict, StringComparison comparison, out string replaceInstance)
    {
      replaceInstance = null;
      var en = dict.GetEnumerator();
      while (en.MoveNext())
      {
        if (en.Current.Value == this)
          // ourself, the end
          return replaceInstance != null;
        if (!equals(en.Current.Value, comparison))
          continue;
        replaceInstance = en.Current.Key;
        return true;
      }
      return false;
    }
  }
  partial class A2LCOMPU_TAB_BASE
  {
    protected A2LCOMPU_TAB_BASE() { }
    /// <summary>
    /// Writes the basic values of compu tabs to the output stream.
    /// </summary>
    /// <param name="wr">The stream to write to</param>
    internal protected void writeBase(StreamWriter wr)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      write(wr, indent, true, null, Description.Replace("\"", "\\\""));
      writeValue(wr, indent, null, ConversionType.ToString());
    }
  }
  partial class A2LCOMPU_TAB
  {
    internal A2LCOMPU_TAB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        writeBase(wr);
        string indent = getIndent(this, true);
        writeValue(wr, indent, null, Values.Count.ToString());
        foreach (var value in Values)
          write(wr, indent, false, toDec(value.Key), toDec(value.Value));
        if (!double.IsNaN(DefaultValueNumeric))
          writeValue(wr, indent, A2LC_DefaultValueNum, toDec(DefaultValueNumeric));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCOMPU_VTAB
  {
    internal A2LCOMPU_VTAB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        writeBase(wr);
        string indent = getIndent(this, true);
        writeValue(wr, indent, null, Verbs.Count.ToString());
        foreach (var value in Verbs)
          write(wr, indent, true, toDec(value.Key), value.Value);
        if (!string.IsNullOrEmpty(DefaultValue))
          write(wr, indent, true, A2LC_DefaultValue, DefaultValue);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
    /// <summary>
    /// Tests, if the supplied instances are the same.
    /// </summary>
    /// <param name="y">The compare to instance</param>
    /// <returns>true, if the instances are the same</returns>
    bool equals(A2LCOMPU_VTAB y, StringComparison comparison)
    {
      if (Verbs.Count != y.Verbs.Count)
        return false;
      if (0 != string.Compare(DefaultValue, y.DefaultValue, comparison))
        return false;
      foreach (var current in Verbs)
      {
        string value;
        if (!y.Verbs.TryGetValue(current.Key, out value))
          // key not found
          return false;
        if (0 != string.Compare(current.Value, value, comparison))
          // value is not the same
          return false;
      }
      return true;
    }
    /// <summary>
    /// Test is the current instance is redundant.
    /// </summary>
    /// <param name="dict">The test dictionary (adjusted for each call)</param>
    /// <param name="comparison">string comparison definition</param>
    /// <param name="replaceInstance">Iff return value is true, the replacing instance</param>
    /// <returns>true if the instance is redundant</returns>
    internal bool isRedundant(A2LCompVTabDict dict, StringComparison comparison, out string replaceInstance)
    {
      replaceInstance = null;
      var en = dict.GetEnumerator();
      while (en.MoveNext())
      {
        if (en.Current.Value == this)
          // ourself, the end
          return replaceInstance != null;
        if (!equals(en.Current.Value, comparison))
          continue;
        replaceInstance = en.Current.Key;
        return true;
      }
      return false;
    }
  }
  partial class A2LCOMPU_VTAB_RANGE
  {
    internal A2LCOMPU_VTAB_RANGE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        writeBase(wr);
        string indent = getIndent(this, true);
        writeValue(wr, indent, null, Verbs.Count.ToString());
        foreach (var value in Verbs)
          wr.WriteLine($"{indent}{toDec(value.Value.Min)} {toDec(value.Value.Max)} \"{value.Key}\"");
        if (!string.IsNullOrEmpty(DefaultValue))
          write(wr, indent, true, A2LC_DefaultValue, DefaultValue);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LMEMORY_SEGMENT
  {
    internal A2LMEMORY_SEGMENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        var sb = new StringBuilder();
        sb.AppendFormat("{0} {1} {2} {3} {4} ", PrgType.ToString(), MemoryType.ToString(), MemoryAttribute.ToString(), toHex(Address), toHex(Size));
        for (int i = 0; i < Offsets.Length; ++i)
          sb.AppendFormat("{0} ", Offsets[i]);
        sb.Remove(sb.Length - 1, 1);
        writeValue(wr, indent, null, sb.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCALIBRATION_METHOD
  {
    internal A2LCALIBRATION_METHOD() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Method);
        write(wr, indent, false, null, Version.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCALIBRATION_HANDLE
  {
    internal A2LCALIBRATION_HANDLE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (!string.IsNullOrEmpty(Text))
          write(wr, indent, true, "CALIBRATION_HANDLE_TEXT", Text);
        var sb = new StringBuilder();
        if (Handles.Count > 0)
        {
          for (int i = 0; i < Handles.Count; ++i)
            sb.AppendFormat("{0}{1}{2}", indent, toHex((ulong)Handles[i]), Environment.NewLine);
          sb.Remove(0, indent.Length);
          sb.Remove(sb.Length - Environment.NewLine.Length, Environment.NewLine.Length);
          writeValue(wr, indent, null, sb.ToString());
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LANNOTATION
  {
    internal A2LANNOTATION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (!string.IsNullOrEmpty(Origin))
          write(wr, indent, true, "ANNOTATION_ORIGIN", Origin.Replace("\"", "\\\""));
        if (!string.IsNullOrEmpty(Label))
          write(wr, indent, true, "ANNOTATION_LABEL", Label.Replace("\"", "\\\""));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LANNOTATION_TEXT
  {
    internal A2LANNOTATION_TEXT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (!string.IsNullOrEmpty(Text))
          write(wr, indent, true, null, Text.Replace("\"", "\\\""));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LFORMULA
  {
    internal A2LFORMULA() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (!string.IsNullOrEmpty(Formula))
          write(wr, indent, true, null, Formula);
        if (!string.IsNullOrEmpty(FormulaInv))
          write(wr, indent, true, A2LC_FormulaInv, FormulaInv);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LUSER_RIGHTS
  {
    internal A2LUSER_RIGHTS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (ReadOnly)
          writeValue(wr, indent, null, A2LC_ReadOnly);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LFRAME
  {
    internal A2LFRAME() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, ScalingUnit.ToString());
        writeValue(wr, indent, null, Rate.ToString());
        if (null != References)
        {
          for (int i = 0; i < References.Count; ++i)
            writeValue(wr, indent
              , i == 0 ? "FRAME_MEASUREMENT" : null
              , References[i]
              );
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LDEPENDENT_CHARACTERISTIC
  {
    internal A2LDEPENDENT_CHARACTERISTIC() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        wr.Write("{0}\"{1}\"", indent, Formula);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LBIT_OPERATION
  {
    internal A2LBIT_OPERATION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        switch (BitOperation)
        {
          case BITOPERATION_TYPE.LEFT_SHIFT:
          case BITOPERATION_TYPE.RIGHT_SHIFT:
            writeValue(wr, indent, BitOperation.ToString(), Count.ToString());
            break;
        }
        if (SignExtend)
          writeValue(wr, indent, null, A2LC_SignExtend);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LFIX_AXIS_PAR_LIST
  {
    internal A2LFIX_AXIS_PAR_LIST() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        foreach (var value in Values)
          writeValue(wr, indent, null, toDec(value));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LVARIANT_CODING
  {
    internal A2LVARIANT_CODING() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (!string.IsNullOrEmpty(VAR_SEPARATOR))
          write(wr, indent, true, A2LC_VAR_SEPARATOR, VAR_SEPARATOR);
        if (VAR_NAMING != VarNamingType.NotSet)
          writeValue(wr, indent, A2LC_VAR_NAMING, VAR_NAMING.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LVAR_CRITERION
  {
    internal A2LVAR_CRITERION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, string.IsNullOrEmpty(Description) ? string.Empty : Description.Replace("\"", "\\\""));
        if (!string.IsNullOrEmpty(mVarMeasurement))
          writeValue(wr, indent, A2LC_VAR_MEASUREMENT, mVarMeasurement);
        if (!string.IsNullOrEmpty(mVarSelectionCharacteristic))
          writeValue(wr, indent, A2LC_VAR_SELECTION_CHARACTERISTIC, mVarSelectionCharacteristic);

        writeReferences(wr, indent, CriterionValues);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LVAR_CHARACTERISTIC
  {
    internal A2LVAR_CHARACTERISTIC() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (CriterionValues != null)
          for (int i = 0; i < CriterionValues.Count; ++i)
            writeValue(wr, indent, null, CriterionValues[i]);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LVAR_ADDRESS
  {
    internal A2LVAR_ADDRESS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (Addresses != null)
          for (int i = 0; i < Addresses.Count; ++i)
            writeValue(wr, indent, null, toHex(Addresses[i]));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LVAR_FORBIDDEN_COMB
  {
    internal A2LVAR_FORBIDDEN_COMB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (ForbiddenDict != null)
        {
          var en = ForbiddenDict.GetEnumerator();
          while (en.MoveNext())
            writeValue(wr, indent, en.Current.Key, en.Current.Value);
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LUNIT
  {
    internal A2LUNIT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        write(wr, indent, true, null, Display);
        writeValue(wr, indent, null, UnitType.ToString());
        if (!string.IsNullOrEmpty(RefUnit))
          writeValue(wr, indent, A2LC_RefUnit, RefUnit);
        if (UnitConversion != null)
          writeValue(wr, indent, A2LC_UnitConv, $"{toDec(UnitConversion[0])} {toDec(UnitConversion[1])}");
        if (SIExponents != null)
          writeValue(wr, indent, A2LC_SIExponents
            , $"{SIExponents[0]} {SIExponents[1]} {SIExponents[2]} {SIExponents[3]} {SIExponents[4]} {SIExponents[5]} {SIExponents[6]}"
            );
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTYPEDEF
  {
    protected A2LTYPEDEF() { }
    internal protected virtual void writeBaseValues(StreamWriter wr, string indent) { }
  }
  partial class A2LTYPEDEF_BLOB
  {
    internal A2LTYPEDEF_BLOB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        base.writeBaseValues(wr, indent);
        writeValue(wr, indent, null, toHex(Size));
        if (AddrType != ADDR_TYPE.DIRECT)
          write(wr, indent, false, A2LC_ADDRESS_TYPE, AddrType.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTYPEDEF_STRUCTURE
  {
    internal A2LTYPEDEF_STRUCTURE() { }
    protected internal override void writeBaseValues(StreamWriter wr, string indent)
    {
      base.writeBaseValues(wr, indent);
      if (ConsistentExchange)
        write(wr, indent, false, null, A2LC_CONSISTENT_EXCHANGE);
    }
  }
  partial class A2LSTRUCTURE_COMPONENT
  {
    internal A2LSTRUCTURE_COMPONENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, mTypedefName);
        writeValue(wr, indent, null, toHex(AddressOffset));
        if (Layout != INDEX_MODE.NotSet)
          write(wr, indent, false, A2LC_Layout, Layout.ToString());
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTYPEDEF_CONVERSION_REF
  {
    protected A2LTYPEDEF_CONVERSION_REF() { }
    internal protected override void writeBaseValues(StreamWriter wr, string indent)
    {
      writeValue(wr, indent, null, $"{toDec(LowerLimit)} {toDec(UpperLimit)}");
      base.writeBaseValues(wr, indent);
      if (!string.IsNullOrEmpty(Format))
        write(wr, indent, true, A2LC_Format, Format);
      if (!string.IsNullOrEmpty(PhysUnit))
        write(wr, indent, true, A2LC_PhysUnit, PhysUnit);
      if (ByteOrder != BYTEORDER_TYPE.NotSet)
        write(wr, indent, false, A2LC_ByteOrder, ByteOrder.ToString());
    }
  }
  partial class A2LTYPEDEF_MEASUREMENT
  {
    internal A2LTYPEDEF_MEASUREMENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, DataType.ToString());
        writeValue(wr, indent, null, Conversion);
        writeValue(wr, indent, null, $"{Resolution} {toDec(Accuracy)}");
        writeBaseValues(wr, indent);
        if (Layout != INDEX_MODE.NotSet)
          write(wr, indent, false, A2LC_Layout, Layout.ToString());
        if (BitMask != UInt64.MaxValue)
          write(wr, indent, false, A2LC_BitMask, toHex(BitMask));
        if (ErrorMask != 0)
          write(wr, indent, false, A2LC_ErrorMask, toHex(ErrorMask));
        if (Discrete)
          write(wr, indent, false, null, A2LC_Discrete);
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
        if (AddrType != ADDR_TYPE.DIRECT)
          write(wr, indent, false, A2LC_ADDRESS_TYPE, AddrType.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTYPEDEF_RECORDLAYOUT_REF
  {
    protected A2LTYPEDEF_RECORDLAYOUT_REF() { }
    internal protected override void writeBaseValues(StreamWriter wr, string indent)
    {
      base.writeBaseValues(wr, indent);
      if (LowerLimitEx != LowerLimit || UpperLimit != UpperLimitEx)
        writeValue(wr, indent, null, $"{A2LC_ExLimits} {toDec(LowerLimitEx)} {toDec(UpperLimitEx)}");
      if (!double.IsNaN(StepSize))
        writeValue(wr, indent, A2LC_StepSize, toDec(StepSize));
    }
  }
  partial class A2LTYPEDEF_AXIS
  {
    internal A2LTYPEDEF_AXIS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, InputQuantity);
        writeValue(wr, indent, null, RecordLayout);
        writeValue(wr, indent, null, toDec(MaxDiff));
        writeValue(wr, indent, null, Conversion);
        writeValue(wr, indent, null, MaxAxisPoints.ToString());
        writeBaseValues(wr, indent);
        if (Monotony != MONOTONY_TYPE.NotSet)
          write(wr, indent, false, A2LC_Monotony, Monotony.ToString());
        if (mDeposit != DEPOSIT_TYPE.NotSet)
          write(wr, indent, false, A2LC_Deposit, mDeposit.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTYPEDEF_CHARCTERISTIC
  {
    internal A2LTYPEDEF_CHARCTERISTIC() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, CharType.ToString());
        writeValue(wr, indent, null, RecordLayout);
        writeValue(wr, indent, null, toDec(MaxDiff));
        writeValue(wr, indent, null, Conversion);
        writeBaseValues(wr, indent);
        if (Bitmask != UInt64.MaxValue)
          writeValue(wr, indent, A2LC_BitMask, toHex(Bitmask));
        if (Number != 0)
          write(wr, indent, false, A2LC_Number, Number.ToString());
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
        if (Discrete)
          write(wr, indent, false, null, A2LC_Discrete);
        if (Encoding != EncodingType.ASCII)
          write(wr, indent, false, A2LC_ENCODING, Encoding.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LBLOB
  {
    internal A2LBLOB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, toHex(Address));
        writeValue(wr, indent, null, toHex(Size));
        writeBaseValues(wr, indent);
        if (AddrType != ADDR_TYPE.DIRECT)
          write(wr, indent, false, A2LC_ADDRESS_TYPE, AddrType.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LINSTANCE
  {
    internal A2LINSTANCE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Description.Replace("\"", "\\\""));
        writeValue(wr, indent, null, mTypedefName);
        writeValue(wr, indent, null, toHex(Address));
        writeBaseValues(wr, indent);
        if (Layout != INDEX_MODE.NotSet)
          write(wr, indent, false, A2LC_Layout, Layout.ToString());
        if (ReadWrite)
          write(wr, indent, false, null, A2LC_ReadWrite);
        if (null != MatrixDim)
          writeMatrixDim(wr, major, minor, indent, MatrixDim);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LOVERWRITE
  {
    internal A2LOVERWRITE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, false, null, AxisNo.ToString());
        if (!string.IsNullOrEmpty(Conversion))
          write(wr, indent, false, A2LC_CONVERSION, Conversion);
        if (!string.IsNullOrEmpty(Format))
          write(wr, indent, false, A2LC_Format, Format);
        if (!string.IsNullOrEmpty(PhysUnit))
          write(wr, indent, false, A2LC_PhysUnit, PhysUnit);
        if (!string.IsNullOrEmpty(InputQuantity))
          write(wr, indent, false, A2LC_INPUT_QUANTITY, InputQuantity);
        if (Monotony != MONOTONY_TYPE.NotSet)
          write(wr, indent, false, A2LC_Monotony, Monotony.ToString());
        if (LimitsSet)
          writeValue(wr, indent, null, $"{A2LC_LIMITS} {toDec(LowerLimit)} {toDec(UpperLimit)}");
        if (LimitsExSet)
          writeValue(wr, indent, null, $"{A2LC_ExLimits} {toDec(LowerLimitEx)} {toDec(UpperLimitEx)}");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LTRANSFORMER
  {
    internal A2LTRANSFORMER() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, Version);
        write(wr, indent, true, null, Executable32);
        write(wr, indent, true, null, Executable64);
        write(wr, indent, false, null, Timeout.ToString());
        write(wr, indent, false, null, Trigger.ToString());
        write(wr, indent, false, null
          , string.IsNullOrEmpty(InverseTransformer)
            ? mNoInverseTransformer
            : InverseTransformer
          );
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
}
#region XCP
namespace jnsoft.ASAP2.XCP
{
  partial class A2LXCP_DAQ
  {
    internal A2LXCP_DAQ() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, Mode.ToString());
      writeValue(wr, indent, null, toHex(MaxDAQ));
      writeValue(wr, indent, null, toHex(MaxEvtChn));
      writeValue(wr, indent, null, toHex(MinDAQ));
      writeValue(wr, indent, null, "OPTIMISATION_TYPE_" + OptType.ToString());
      writeValue(wr, indent, null, A2LC_AdrExt + "_" + AdrExt.ToString());
      switch (IDField)
      {
        case XCP_ID_FIELD_TYPE.ABSOLUTE:
          writeValue(wr, indent, null, "IDENTIFICATION_FIELD_TYPE_" + IDField.ToString()); break;
        default:
          writeValue(wr, indent, null, "IDENTIFICATION_FIELD_TYPE_RELATIVE_" + IDField.ToString()); break;
      }
      writeValue(wr, indent, null, "GRANULARITY_ODT_ENTRY_SIZE_DAQ_" + ODTEntrySize.ToString());
      writeValue(wr, indent, null, toHex(MaxODTEntrySize));
      switch (OverloadInd)
      {
        case XCP_OVERLOAD_IND.INDICATION: writeValue(wr, indent, null, "NO_OVERLOAD_INDICATION"); break;
        default: writeValue(wr, indent, null, "OVERLOAD_INDICATION_" + OverloadInd.ToString()); break;
      }
      if (DAQAlternatingSupported != UInt16.MaxValue)
        writeValue(wr, indent, A2LC_DAQ_ALTERNATING_SUPPORTED, DAQAlternatingSupported.ToString());
      if (PrescalerSupported)
        writeValue(wr, indent, null, A2LC_PRESCALER_SUPPORTED);
      if (ResumeSupported)
        writeValue(wr, indent, null, A2LC_RESUME_SUPPORTED);
      if (StoreDAQSupported)
        writeValue(wr, indent, null, A2LC_STORE_DAQ_SUPPORTED);
      if (DtoCtrFieldSupported)
        writeValue(wr, indent, null, A2LC_DTO_CTR_FIELD_SUPPORTED);
      if (PIDOffSupported)
        writeValue(wr, indent, null, A2LC_PID_OFF_SUPPORTED);

      if (MaxDAQTotal > 0)
        write(wr, indent, false, A2LC_MAX_DAQ_TOTAL, MaxDAQTotal.ToString());
      if (MaxODTTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_TOTAL, MaxODTTotal.ToString());
      if (MaxODTDAQTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_DAQ_TOTAL, MaxODTDAQTotal.ToString());
      if (MaxODTSTIMTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_STIM_TOTAL, MaxODTSTIMTotal.ToString());
      if (MaxODTEntriesTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_ENTRIES_TOTAL, MaxODTEntriesTotal.ToString());
      if (MaxODTEntriesDAQTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_ENTRIES_DAQ_TOTAL, MaxODTEntriesDAQTotal.ToString());
      if (MaxODTEntriesSTIMTotal > 0)
        write(wr, indent, false, A2LC_MAX_ODT_ENTRIES_STIM_TOTAL, MaxODTEntriesSTIMTotal.ToString());
      if (CpuLoadMaxTotal > 0f)
        write(wr, indent, false, A2LC_CPU_LOAD_MAX_TOTAL, toDec(CpuLoadMaxTotal));
      if (CoreLoadMaxTotal > 0f)
        write(wr, indent, false, A2LC_CORE_LOAD_MAX_TOTAL, toDec(CoreLoadMaxTotal));

      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_DAQ_MEMORY_CONSUMPTION
  {
    internal A2LXCP_DAQ_MEMORY_CONSUMPTION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, DAQMemoryLimit.ToString());
      writeValue(wr, indent, null, DAQSize.ToString());
      writeValue(wr, indent, null, ODTSize.ToString());
      writeValue(wr, indent, null, ODTEntrySize.ToString());
      writeValue(wr, indent, null, ODTDAQBufferElementSize.ToString());
      writeValue(wr, indent, null, ODTSTIMBufferElementSize.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_BUFFER_RESERVE
  {
    internal A2LXCP_BUFFER_RESERVE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, ODT_DAQ.ToString());
      writeValue(wr, indent, null, ODT_STIM.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_PROTOCOL_LAYER
  {
    internal A2LXCP_PROTOCOL_LAYER() { }
    /// <summary>
    /// write extended communication modes.
    /// </summary>
    /// <param name="wr"></param>
    /// <param name="indent"></param>
    internal static void writeExtendedCommModes(StreamWriter wr, string indent, XCPCommModes CommModesSupported)
    {
      if (CommModesSupported.CommMode != XCP_BLOCK_MODE.STANDARD)
      {
        var BlockModeStr = new StringBuilder("BLOCK ");
        if ((CommModesSupported.CommMode & XCP_BLOCK_MODE.SLAVE) > 0)
          BlockModeStr.Append($"{XCP_BLOCK_MODE.SLAVE.ToString()} ");
        if ((CommModesSupported.CommMode & XCP_BLOCK_MODE.MASTER) > 0)
          BlockModeStr.Append($"{XCP_BLOCK_MODE.MASTER.ToString()} 0x{CommModesSupported.MaxBS:X2} 0x{CommModesSupported.MinST:X2} ");
        if ((CommModesSupported.CommMode & XCP_BLOCK_MODE.INTERLEAVED) > 0)
          BlockModeStr.Append($"{XCP_BLOCK_MODE.INTERLEAVED.ToString()} 0x{CommModesSupported.QueueSize:X2} ");
        writeValue(wr, indent, A2LC_COMM_MODE_SUPPORTED, BlockModeStr.ToString());
      }
    }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(Version));
      var sb = new StringBuilder();
      for (int i = 0; i < Timings.Length; ++i)
        sb.AppendFormat("{0} ", Timings[i]);
      sb.Remove(sb.Length - 1, 1);
      writeValue(wr, indent, null, sb.ToString());
      writeValue(wr, indent, null, toHex(MaxCTO));
      writeValue(wr, indent, null, toHex(MaxDTO));
      writeValue(wr, indent, null
        , ByteOrder == BYTEORDER_TYPE.BIG_ENDIAN
          ? A2LC_MSB_FIRST
          : A2LC_MSB_LAST
        );
      if ((CommModeBasic & CommModeBasic.AddressGranularityWORD) > 0)
        writeValue(wr, indent, null, A2LC_ADR_GRAN_WORD);
      else if ((CommModeBasic & CommModeBasic.AddressGranularityDWORD) > 0)
        writeValue(wr, indent, null, A2LC_ADR_GRAN_DWORD);
      else
        writeValue(wr, indent, null, A2LC_ADR_GRAN_BYTE);
      if (!string.IsNullOrEmpty(SeedAndKeyExternalFunction))
        write(wr, indent, true, A2LC_SK_EXT_FUNC, SeedAndKeyExternalFunction);

      foreach (var value in OptionalCmds)
        writeValue(wr, indent, A2LC_OPTIONAL_CMD, value);

      writeExtendedCommModes(wr, indent, CommModesSupported);
      if (MaxDTOStim != UInt16.MaxValue)
        writeValue(wr, indent, A2LC_MAX_DTO_STIM, toHex(MaxDTOStim));

      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ECU_STATES_STATE
  {
    internal A2LXCP_ECU_STATES_STATE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, StateNumber.ToString());
      write(wr, indent, true, null, StateName.Replace("\"", "\\\""));
      writeValue(wr, indent, null, CAL_PAG.ToString());
      writeValue(wr, indent, null, DAQ.ToString());
      writeValue(wr, indent, null, STIM.ToString());
      writeValue(wr, indent, null, PGM.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ECU_STATES_MEMORY_ACCESS
  {
    internal A2LXCP_ECU_STATES_MEMORY_ACCESS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, "READ_ACCESS_" + ReadAccess.ToString());
      writeValue(wr, indent, null, "WRITE_ACCESS_" + WriteAccess.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_STIM
  {
    internal A2LXCP_STIM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, "GRANULARITY_ODT_ENTRY_SIZE_STIM_" + ODTEntrySize.ToString());
      writeValue(wr, indent, null, toHex(MaxODTEntrySize));
      if (BitSTIMSupported)
        writeValue(wr, indent, null, A2LC_BIT_STIM_SUPPORTED);
      if (MinSTSTIM > 0)
        writeValue(wr, indent, A2LC_MIN_ST_STIM, MinSTSTIM.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_TIMESTAMP_SUPPORTED
  {
    internal A2LXCP_TIMESTAMP_SUPPORTED() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(Ticks));
      if (Size != XCP_TIMESTAMP_SIZE.NotSet)
        writeValue(wr, indent, null, "SIZE_" + Size.ToString());
      else
        writeValue(wr, indent, null, "NO_TIME_STAMP");
      writeValue(wr, indent, null, "UNIT_" + Resolution.ToString().Substring(1));
      if (IsFixed)
        writeValue(wr, indent, null, A2LC_TIMESTAMP_FIXED);
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT
  {
    internal A2LXCP_EVENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      write(wr, indent, true, null, NameLong.Replace("\"", "\\\""));
      write(wr, indent, true, null, NameShort.Replace("\"", "\\\""));
      writeValue(wr, indent, null, Id.ToString());
      writeValue(wr, indent, null, DAQListType.ToString());
      writeValue(wr, indent, null, MaxDAQList.ToString());
      writeValue(wr, indent, null, TimeCycle.ToString());
      writeValue(wr, indent, null, TimeUnit.ToString());
      writeValue(wr, indent, null, Priority.ToString());
      if (COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER < UInt16.MaxValue)
        writeValue(wr, indent, A2LC_COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER, COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER.ToString());
      if(Consistency != XCP_ADDRESS_MODE.NotSet)
        writeValue(wr, indent, A2LC_CONSISTENCY, Consistency.ToString());

      if (getParent<A2LIF_DATA>()?.Name == "XCPplus")
      {
        if (EVENT_COUNTER_PRESENT)
          writeValue(wr, indent, null, A2LC_EVENT_COUNTER_PRESENT);
        if (RELATED_EVENT_CHANNEL_NUMBER < UInt16.MaxValue)
          writeValue(wr, indent, A2LC_RELATED_EVENT_CHANNEL_NUMBER, RELATED_EVENT_CHANNEL_NUMBER.ToString());
        if (RELATED_EVENT_CHANNEL_NUMBER_FIXED)
          writeValue(wr, indent, null, A2LC_RELATED_EVENT_CHANNEL_NUMBER_FIXED);

        if (DTO_CTR_DAQ_MODE != DTO_CTR_DAQ_MODE.NotSet)
          writeValue(wr, indent, A2LC_DTO_CTR_DAQ_MODE, DTO_CTR_DAQ_MODE.ToString());
        if (DTO_CTR_DAQ_MODE_FIXED)
          writeValue(wr, indent, null, A2LC_DTO_CTR_DAQ_MODE_FIXED);

        if (DTO_CTR_STIM_MODE != DTO_CTR_STIM_MODE.NotSet)
          writeValue(wr, indent, A2LC_DTO_CTR_STIM_MODE, DTO_CTR_STIM_MODE.ToString());
        if (DTO_CTR_STIM_MODE_FIXED)
          writeValue(wr, indent, null, A2LC_DTO_CTR_STIM_MODE_FIXED);

        if (STIM_DTO_CTR_COPY_PRESENT)
          writeValue(wr, indent, null, A2LC_STIM_DTO_CTR_COPY_PRESENT);
        if (CPU_LOAD_MAX > 0f)
          writeValue(wr, indent, A2LC_CPU_LOAD_MAX, toDec(CPU_LOAD_MAX));
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_MIN_CYCLE_TIME
  {
    internal A2LXCP_EVENT_MIN_CYCLE_TIME() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, TimeCycle.ToString());
      writeValue(wr, indent, null, TimeUnit.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_DAQ_PACKED_MODE
  {
    internal A2LXCP_EVENT_DAQ_PACKED_MODE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, GroupMode.ToString());
      writeValue(wr, indent, null, "STS_" + STSMode.ToString());
      writeValue(wr, indent, null, PackMode.ToString());
      writeValue(wr, indent, null, SampleCount.ToString());
      foreach (var value in AltSampleCounts)
        writeValue(wr, indent, A2LC_ALT_SAMPLE_COUNT, value.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_CPU_LOAD_CONSUMPTION
  {
    internal A2LXCP_EVENT_CPU_LOAD_CONSUMPTION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toDec(DAQ_FACTOR));
      writeValue(wr, indent, null, toDec(ODT_FACTOR));
      writeValue(wr, indent, null, toDec(ODT_ENTRY_FACTOR));
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_CPU_LOAD_CONSUMPTION_QUEUE
  {
    internal A2LXCP_EVENT_CPU_LOAD_CONSUMPTION_QUEUE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toDec(ODT_FACTOR));
      writeValue(wr, indent, null, toDec(ODT_ELEMENT_LOAD));
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_ODT_ENTRY_SIZE_FACTOR_TABLE
  {
    internal A2LXCP_EVENT_ODT_ENTRY_SIZE_FACTOR_TABLE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, SIZE.ToString());
      writeValue(wr, indent, null, toDec(SIZE_FACTOR));
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_CORE_LOAD
  {
    internal A2LXCP_CORE_LOAD() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, CORE_NR.ToString());
      writeValue(wr, indent, null, toDec(CORE_LOAD));
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_NAMEDNODE
  {
    protected A2LXCP_NAMEDNODE() { }
    internal override void writeBegin(StreamWriter wr)
    {
      base.writeBegin(wr);
      wr.Write(" " + Name);
    }
  }
  partial class A2LXCP_DAQ_EVENT
  {
    internal A2LXCP_DAQ_EVENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      if (Events?.Count > 0)
      {
        var sb = new StringBuilder();
        foreach (var eventNo in Events)
          sb.Append($"{A2LC_EVENT} {eventNo} ");
        sb.Remove(sb.Length - 1, 1);
        write(wr, indent, false, null, sb.ToString());
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_LIST
  {
    internal A2LXCP_EVENT_LIST() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      if (Events?.Count > 0)
      {
        var sb = new StringBuilder();
        foreach (var eventNo in Events)
          sb.Append($"{A2LC_EVENT} {eventNo} ");
        sb.Remove(sb.Length - 1, 1);
        write(wr, indent, false, null, sb.ToString());
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_PAG
  {
    internal A2LXCP_PAG() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(MaxSegments));
      if ((Properties & PAGProperties.FreezeSupported) > 0)
        writeValue(wr, indent, null, "FREEZE_SUPPORTED");
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_PGM
  {
    internal A2LXCP_PGM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      switch (Mode)
      {
        case XCP_PGM_MODE.ABSOLUTE: writeValue(wr, indent, null, A2LC_PGMMODE_ABS); break;
        case XCP_PGM_MODE.FUNCTIONAL: writeValue(wr, indent, null, A2LC_PGMMODE_FUNC); break;
        case XCP_PGM_MODE.ABSOLUTE | XCP_PGM_MODE.FUNCTIONAL: writeValue(wr, indent, null, A2LC_PGMMODE_ABS_FUNC); break;
      }
      writeValue(wr, indent, null, $"{toHex(MaxSectors)} {toHex(MaxCTO)}");
      A2LXCP_PROTOCOL_LAYER.writeExtendedCommModes(wr, indent, CommModesSupported);
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_SECTOR
  {
    internal A2LXCP_SECTOR() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      write(wr, indent, true, null, NameLong.Replace("\"", "\\\""));
      writeValue(wr, indent, null, $"{toHex(Number)} {toHex(Address)} {toHex(Length)}");
      writeValue(wr, indent, null, $"{toHex(ClearSeqNo)} {toHex(PgmSeqNo)} {toHex(PgmMethod)}");
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ON_ETHERNET
  {
    protected A2LXCP_ON_ETHERNET() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(Version));
      writeValue(wr, indent, null, Port.ToString());
      IPAddress address;
      if (IPAddress.TryParse(Address, out address))
        write(wr, indent, true, A2LC_ADDRESS, address.ToString());
      else
        write(wr, indent, true, A2LC_HOST_NAME, address.ToString());
      if (!string.IsNullOrEmpty(IPV6))
        write(wr, indent, true, A2LC_IPV6, IPV6);
      if (!string.IsNullOrEmpty(TransportLayerInstance))
        write(wr, indent, true, A2LC_TRANSPORT_LAYER_INSTANCE, TransportLayerInstance);
      if (PacketAlignment != XCP_PACKET_ALIGMENT.NotSet)
        writeValue(wr, indent, null, "PACKET_ALIGMENT" + PacketAlignment.ToString());

      foreach (var value in OptionalTLCmds)
        writeValue(wr, indent, A2LC_OPTIONAL_CMD, value);

      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ON_CAN
  {
    internal A2LXCP_ON_CAN() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(Version));
      if (CANIDBroadcast != UInt32.MaxValue)
        writeValue(wr, indent, A2LC_CAN_ID_BROADCAST, toHex(CANIDBroadcast));
      if (CANIDGetDAQClockMulticast != UInt32.MaxValue)
        writeValue(wr, indent, A2LC_CAN_ID_GET_DAQ_CLOCK_MULTICAST, toHex(CANIDGetDAQClockMulticast));
      writeValue(wr, indent, A2LC_CAN_ID_MASTER, toHex(CANIDCmd));
      writeValue(wr, indent, A2LC_CAN_ID_SLAVE, toHex(CANIDResp));
      if (Baudrate > 0)
        writeValue(wr, indent, A2LC_BAUDRATE, Baudrate.ToString());
      if (SampleRate != 0)
        writeValue(wr, indent, A2LC_SAMPLE_POINT, SamplePoint.ToString());
      if (SampleRate != XCP_DAQ_LIST_CAN_SAMPLE_RATE.NotSet)
        writeValue(wr, indent, A2LC_SAMPLE_RATE, SampleRate.ToString());
      if (BTLCycles != 0)
        writeValue(wr, indent, A2LC_BTL_CYCLES, BTLCycles.ToString());
      if (SJW != 0)
        writeValue(wr, indent, A2LC_SJW, toHex(SJW));
      if (MaxDLCRequired)
        writeValue(wr, indent, null, A2LC_MAX_DLC_REQUIRED);
      if (SyncEdge != XCP_SYNCEDGE.NotSet)
        writeValue(wr, indent, A2LC_SYNC_EDGE, SyncEdge.ToString());
      if (MasterIncremental)
        writeValue(wr, indent, null, A2LC_CAN_ID_MASTER_INCREMENTAL);
      if (MaxBusLoad > 0)
        writeValue(wr, indent, A2LC_MAX_BUS_LOAD, toHex(MaxBusLoad));
      if (!string.IsNullOrEmpty(TransportLayerInstance))
        write(wr, indent, true, A2LC_TRANSPORT_LAYER_INSTANCE, TransportLayerInstance);

      foreach (var value in OptionalTLCmds)
        writeValue(wr, indent, A2LC_OPTIONAL_CMD, value);

      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_CAN_FD
  {
    internal A2LXCP_CAN_FD() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      if (MaxDLC > 0)
        writeValue(wr, indent, A2LC_MAX_DLC, MaxDLC.ToString());
      if (DataTransferBaudrate > 0)
        writeValue(wr, indent, A2LC_CAN_FD_DATA_TRANSFER_BAUDRATE, DataTransferBaudrate.ToString());
      if (SamplePoint != 0)
        writeValue(wr, indent, A2LC_SAMPLE_POINT, SamplePoint.ToString());
      if (BTLCycles != 0)
        writeValue(wr, indent, A2LC_BTL_CYCLES, BTLCycles.ToString());
      if (SJW != 0)
        writeValue(wr, indent, A2LC_SJW, toHex(SJW));
      if (MaxDLCRequired)
        writeValue(wr, indent, null, A2LC_MAX_DLC_REQUIRED);
      if (SyncEdge != XCP_SYNCEDGE.NotSet)
        writeValue(wr, indent, A2LC_SYNC_EDGE, SyncEdge.ToString());
      if (SecondarySamplePoint != 0)
        writeValue(wr, indent, A2LC_SECONDARY_SAMPLE_POINT, SamplePoint.ToString());
      if (TransceiverDelayCompensation != XCP_TRANSCEIVER_DELAY_COMPENSATION.NotSet)
        writeValue(wr, indent, A2LC_TRANSCEIVER_DELAY_COMPENSATION, TransceiverDelayCompensation.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_DAQ_LIST_CAN_ID
  {
    internal A2LXCP_DAQ_LIST_CAN_ID() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, DAQNo.ToString());
      switch (DAQListType)
      {
        case XCP_DAQ_LIST_CAN_TYPE.VARIABLE: writeValue(wr, indent, null, A2LC_VARIABLE); break;
        case XCP_DAQ_LIST_CAN_TYPE.FIXED:
          write(wr, indent, false, A2LC_FIXED, toHex(CANId));
          break;
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ODT
  {
    internal A2LXCP_ODT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, ODTNo.ToString());
      if (ODTEntries?.Count > 0)
        foreach (var oe in ODTEntries)
          write(wr, indent, false, A2LC_ODT_ENTRY, $"{oe.OdtEntryNo} 0x{oe.Address:X} 0x{oe.AddressExtension:X} 0x{oe.Size:X} 0x{oe.BitOffset:X}");
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_EVENT_CAN_ID_LIST
  {
    internal A2LXCP_EVENT_CAN_ID_LIST() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, EvtNo.ToString());
      if (FixedCANIds?.Count > 0)
      {
        var sb = new StringBuilder();
        foreach (var canId in FixedCANIds)
          sb.Append($"{A2LC_FIXED} {toHex(canId)} ");
        sb.Remove(sb.Length - 1, 1);
        write(wr, indent, false, null, sb.ToString());
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_SEGMENT
  {
    internal A2LXCP_SEGMENT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(SegmentNo));
      writeValue(wr, indent, null, toHex(NoOfPages));
      writeValue(wr, indent, null, toHex(AddressExtension));
      writeValue(wr, indent, null, toHex(CompressionMethod));
      writeValue(wr, indent, null, toHex(EncryptionMethod));
      if (PgmVerify > 0)
        writeValue(wr, indent, A2LC_PGM_VERIFY, toHex(PgmVerify));
      if (DefaultPageNumber != byte.MaxValue)
        writeValue(wr, indent, A2LC_DEFAULT_PAGE_NUMBER, DefaultPageNumber.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_PAGE
  {
    internal A2LXCP_PAGE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(PageNo));
      writeValue(wr, indent, null, "ECU_ACCESS_" + AccessECU.ToString());
      writeValue(wr, indent, null, "XCP_READ_ACCESS_" + AccessRead.ToString());
      writeValue(wr, indent, null, "XCP_WRITE_ACCESS_" + AccessWrite.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_CHECKSUM
  {
    internal A2LXCP_CHECKSUM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      if (CheckSum != ChecksumType.NotSet)
        writeValue(wr, indent, null, "XCP_" + CheckSum.ToString());
      if (MaxBlockSize < UInt32.MaxValue)
        writeValue(wr, indent, "MAX_BLOCK_SIZE", toHex(MaxBlockSize));
      if (!string.IsNullOrEmpty(ExternalFunction))
        write(wr, indent, true, A2LC_EXTERNAL_FUNCTION, ExternalFunction);
      if (MTABlockSizeAlign > 0)
        write(wr, indent, true, A2LC_MTA_BLOCK_SIZE_ALIGN, MTABlockSizeAlign.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_DAQ_LIST
  {
    internal A2LXCP_DAQ_LIST() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, DAQNo.ToString());
      if (DAQListType != XCP_DAQ_LIST_TYPE.NotSet)
        writeValue(wr, indent, A2LC_DAQ_LIST_TYPE, DAQListType.ToString());
      writeValue(wr, indent, A2LC_MAX_ODT, MaxODT.ToString());
      writeValue(wr, indent, A2LC_MAX_ODT_ENTRIES, MaxODTEntries.ToString());
      if (EventFixed != UInt16.MaxValue)
        writeValue(wr, indent, A2LC_EVENT_FIXED, EventFixed.ToString());
      if (FirstPID != byte.MaxValue)
        writeValue(wr, indent, A2LC_FIRST_PID, toHex(FirstPID));
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ADDRESS_MAPPING
  {
    internal A2LXCP_ADDRESS_MAPPING() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, $"{toHex(SrcAddress)} {toHex(DstAddress)} {toHex(Length)}");
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_TIME_CORRELATION
  {
    internal A2LXCP_TIME_CORRELATION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, A2LC_DAQ_TIMESTAMPS_RELATE_TO, TimestampsRelateTo.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_CLOCK
  {
    internal A2LXCP_CLOCK() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();

      var sb = new StringBuilder();
      foreach (var b in UUID)
        sb.Append($"0x{b:X02} ");
      sb.Remove(sb.Length - 1, 1);
      writeValue(wr, indent, null, sb.ToString());

      writeValue(wr, indent, null, Mode.ToString());
      writeValue(wr, indent, null, Readability.ToString());
      writeValue(wr, indent, null, Feature.ToString());
      writeValue(wr, indent, null, Quality.ToString());
      writeValue(wr, indent, null, MaxTimestmampValueBeforeWraparound.ToString());
      writeValue(wr, indent, null, Epoch.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_TIMESTAMP_CHARACTERIZATION
  {
    internal A2LXCP_TIMESTAMP_CHARACTERIZATION() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();

      writeValue(wr, indent, null, TimestampTicks.ToString());
      writeValue(wr, indent, null, "UNIT"+ Resolution.ToString());
      writeValue(wr, indent, null, Size.ToString());
      base.writeToStream(wr, major, minor);
    }
  }
#region xcp on usb
  partial class A2LXCP_DAQ_LIST_USB_ENDPOINT
  {
    internal A2LXCP_DAQ_LIST_USB_ENDPOINT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(DAQNo));
      switch (EPType)
      {
        case XCP_ENDPOINT.IN: writeValue(wr, indent, null, $"FIX_IN {EPNo}"); break;
        case XCP_ENDPOINT.OUT: writeValue(wr, indent, null, $"FIX_OUT {EPNo}"); break;
      }
      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ON_USB
  {
    internal A2LXCP_ON_USB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(Version));
      writeValue(wr, indent, null, toHex(VendorID));
      writeValue(wr, indent, null, toHex(ProductID));
      writeValue(wr, indent, null, NumberOfIF.ToString());
      writeValue(wr, indent, null, "HEADER_LEN_" + HeaderLen.ToString());
      if (AlternateSettingNo != byte.MaxValue)
        writeValue(wr, indent, A2LC_ALTERNATE_SETTING_NO, AlternateSettingNo.ToString());
      if (!string.IsNullOrEmpty(InterfaceDescriptor))
        writeValue(wr, indent, A2LC_INTERFACE_STRING_DESCRIPTOR, $"\"{InterfaceDescriptor.Replace("\"", "\\\"")}\"");
      if (!string.IsNullOrEmpty(TransportLayerInstance))
        write(wr, indent, true, A2LC_TRANSPORT_LAYER_INSTANCE, TransportLayerInstance);

      foreach (var value in OptionalTLCmds)
        writeValue(wr, indent, A2LC_OPTIONAL_CMD, value);

      base.writeToStream(wr, major, minor);
    }
  }
  partial class A2LXCP_ENDPOINT
  {
    protected A2LXCP_ENDPOINT() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      string indent = getIndent(this, true);
      wr.WriteLine();
      writeValue(wr, indent, null, toHex(EPNo));
      writeValue(wr, indent, null, TransferType.ToString());
      writeValue(wr, indent, null, toHex(MaxPktSize));
      writeValue(wr, indent, null, EPPollInterval.ToString());
      writeValue(wr, indent, null, "MESSAGE_PACKING_" + Packing.ToString());
      writeValue(wr, indent, null, "ALIGNMENT" + Alignment.ToString());
      if (HostBufferSize != UInt16.MaxValue)
        writeValue(wr, indent, A2LC_RECOMMENDED_HOST_BUFSIZE, toHex(HostBufferSize));
      base.writeToStream(wr, major, minor);
    }
  }
#endregion
}
#endregion
#region CCP
namespace jnsoft.ASAP2.CCP
{
  partial class A2LCCP_TP_BLOB
  {
    internal A2LCCP_TP_BLOB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(Version));
        writeValue(wr, indent, null, toHex(BlobVersion));
        writeValue(wr, indent, null, toHex(CANIDCmd));
        writeValue(wr, indent, null, toHex(CANIDResp));
        writeValue(wr, indent, null, toHex(StationAddress));
        writeValue(wr, indent, null, ((byte)ByteOrder).ToString());
        if (Baudrate > 0)
          writeValue(wr, indent, A2LC_BAUDRATE, Baudrate.ToString());
        if (DAQMode != CCP_DAQ_MODE.NotSet)
          writeValue(wr, indent, "DAQ_MODE", DAQMode.ToString());
        if (Consistency != CCP_ADDRESS_MODE.NotSet)
          writeValue(wr, indent, A2LC_CONSISTENCY, Consistency.ToString());
        if (AddressExt != CCP_ADDRESS_MODE.NotSet)
          writeValue(wr, indent, A2LC_AdrExt, AddressExt.ToString());

        foreach(var value in OptionalCmds)
          writeValue(wr, indent, A2LC_OPTIONAL_CMD, toHex(value));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_DEFINED_PAGES
  {
    internal A2LCCP_DEFINED_PAGES() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(No));
        write(wr, indent, true, null, PageName);
        writeValue(wr, indent, null, toHex(AddressExt));
        writeValue(wr, indent, null, toHex(Address));
        writeValue(wr, indent, null, toHex(Length));
        for (int i = 1; i < 0x200; i <<= 1)
          if (((CCP_MEMORY_PAGE_TYPE)i & PageType) > 0)
            writeValue(wr, indent, null, ((CCP_MEMORY_PAGE_TYPE)i).ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_CHECKSUM_PARAM
  {
    internal A2LCCP_CHECKSUM_PARAM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(mProcedure));
        writeValue(wr, indent, null, toHex(Limit));
        if (CalcType != CCP_CHECKSUM_CALC_TYPE.NotSet)
          writeValue(wr, indent, "CHECKSUM_CALCULATION", CalcType.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_SEED_KEY
  {
    internal A2LCCP_SEED_KEY() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        if (CalDll != null)
          write(wr, indent, true, null, CalDll);
        if (DaqDll != null)
          write(wr, indent, true, null, DaqDll);
        if (PgmDll != null)
          write(wr, indent, true, null, PgmDll);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_RASTER
  {
    internal A2LCCP_RASTER() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, NameLong.Replace("\"", "\\\""));
        write(wr, indent, true, null, NameShort.Replace("\"", "\\\""));
        writeValue(wr, indent, null, toHex(EvtChnNo));
        writeValue(wr, indent, null, ScalingUnit.ToString());
        writeValue(wr, indent, null, Rate.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_SOURCE
  {
    internal A2LCCP_SOURCE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, NameLong.Replace("\"", "\\\""));
        writeValue(wr, indent, null, ScalingUnit.ToString());
        writeValue(wr, indent, null, Rate.ToString());
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_QP_BLOB
  {
    internal A2LCCP_QP_BLOB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, DAQNo.ToString());
        writeValue(wr, indent, "LENGTH", Length.ToString());
        writeValue(wr, indent, "RASTER", Raster.ToString());
        switch (DAQListType)
        {
          case XCP.XCP_DAQ_LIST_CAN_TYPE.VARIABLE: writeValue(wr, indent, A2LC_CAN_ID_VARIABLE, toHex(CANId)); break;
          case XCP.XCP_DAQ_LIST_CAN_TYPE.FIXED: writeValue(wr, indent, A2LC_CAN_ID_FIXED, toHex(CANId)); break;
        }
        writeValue(wr, indent, A2LC_FIRST_PID, toHex(FirstPID));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_CAN_PARAM
  {
    internal A2LCCP_CAN_PARAM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, $"{toHex(Frequency)} {toHex(BTR0)} {toHex(BTR1)}");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LCCP_EVENT_GROUP
  {
    internal A2LCCP_EVENT_GROUP() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        write(wr, indent, true, null, NameLong.Replace("\"", "\\\""));
        write(wr, indent, true, null, NameShort.Replace("\"", "\\\""));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
}
#endregion
#region KWP
namespace jnsoft.ASAP2.KWP
{
  partial class A2LKWP_K_LINE
  {
    internal A2LKWP_K_LINE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, StimulationMode.ToString().Substring(1));
        writeValue(wr, indent, null, toHex(ECUAddress));
        writeValue(wr, indent, null, toHex(TesterAddress));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_DIAG_BAUD
  {
    internal A2LKWP_DIAG_BAUD() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, Baudrate.ToString());
        writeValue(wr, indent, null, toHex(DiagMode));
        writeByteArray(wr, indent, "BD_PARA", BD_PARA, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_TIME_DEF
  {
    internal A2LKWP_TIME_DEF() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        var sb = new StringBuilder();
        if (TimeDefs != null && TimeDefs.Length > 0)
        {
          foreach (var td in TimeDefs)
            sb.AppendFormat("{6}KWP_TIMING\t0x{0:X4}\t0x{1:X4}\t0x{2:X4}\t0x{3:X4}\t0x{4:X4}\t0x{5:X4}\r\n"
              , td.p1Max, td.p2Min, td.p2Max, td.p3Min, td.p3Max, td.p4Min
              , indent
              );
          sb.Remove(sb.Length - 2, 2);
          wr.WriteLine(sb);
        }
        if (USDTPTimings != null && USDTPTimings.Length > 0)
        {
          sb.Clear();
          foreach (var td in USDTPTimings)
            sb.AppendFormat("{3}USDTP_TIMING\t0x{0:X4}\t0x{1:X4}\t0x{2:X4}\r\n", td.AS, td.BS, td.CD, indent);
          sb.Remove(sb.Length - 2, 2);
          wr.WriteLine(sb);
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_COPY
  {
    internal A2LKWP_COPY() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, FLASHToRAMMode.ToString());
        writeValue(wr, indent, null, toHex(FLASHToRAMDiagMode));
        writeByteArray(wr, indent, A2LC_COPY_PARA, COPY_PARA, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_FLASH
  {
    internal A2LKWP_FLASH() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, RAMToFLASHMode.ToString());
        writeValue(wr, indent, null, RAMToFLASHRoutineNo.ToString());
        writeValue(wr, indent, null, Result.ToString());
        writeByteArray(wr, indent, A2LC_COPY_FRAME, COPY_FRAME, false);
        writeByteArray(wr, indent, A2LC_RNC_RESULT, RNC_RESULT, true);
        writeByteArray(wr, indent, A2LC_COPY_PARA, COPY_PARA, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_FLASH_COPY
  {
    internal A2LKWP_FLASH_COPY() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, RAMToFLASHMode.ToString());
        writeValue(wr, indent, null, RAMToFLASHRoutineNo.ToString());
        writeValue(wr, indent, null, Result.ToString());
        writeValue(wr, indent, null, FLASHToRAMMode.ToString());
        writeValue(wr, indent, null, toHex(FLASHToRAMDiagMode));
        writeByteArray(wr, indent, A2LC_COPY_FRAME, COPY_FRAME, false);
        writeByteArray(wr, indent, A2LC_RNC_RESULT, RNC_RESULT, true);
        writeByteArray(wr, indent, A2LC_COPY_PARA, COPY_PARA, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { writeEnd(wr); }
    }
  }
  partial class A2LKWP_ADDRESS
  {
    internal A2LKWP_ADDRESS() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(CANIdECU));
        writeValue(wr, indent, null, toHex(CANIdTester));
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_CHECKSUM
  {
    internal A2LKWP_CHECKSUM() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(ChkSumType));
        writeValue(wr, indent, null, OnlyOnActivePage ? "1" : "0");
        writeValue(wr, indent, null, LocalRoutineNo.ToString());
        writeValue(wr, indent, null, Result.ToString());
        writeByteArray(wr, indent, A2LC_RNC_RESULT, RNC_RESULT, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_PAGE_SWITCH
  {
    internal A2LKWP_PAGE_SWITCH() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, Mode.ToString());
        writeByteArray(wr, indent, A2LC_PAGE_CODE, PAGE_CODE, true);
        writeByteArray(wr, indent, A2LC_ESCAPE_CODE_PARA_SET, ESCAPE_CODE_PARA_SET, true);
        writeByteArray(wr, indent, A2LC_ESCAPE_CODE_PARA_GET, ESCAPE_CODE_PARA_GET, true);
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_CAN
  {
    internal A2LKWP_CAN() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, Baudrate.ToString());
        writeValue(wr, indent, null, SamplePoints.ToString());
        writeValue(wr, indent, null, SampleCount.ToString());
        writeValue(wr, indent, null, BTLCycles.ToString());
        writeValue(wr, indent, null, SJWLength.ToString());
        writeValue(wr, indent, null, SyncEdge.ToString());
        if (NetworkLimits != null)
        {
          writeValue(wr, indent, null, A2LC_NETWORK_LIMITS);
          writeValue(wr, indent, null, NetworkLimits.WFT_MAX.ToString());
          writeValue(wr, indent, null, toHex(NetworkLimits.XDL_MAX));
        }
        writeValue(wr, indent, null, $"{A2LC_START_STOP}\t{StartStopRoutineNo}");
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_SOURCE
  {
    internal A2LKWP_SOURCE() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, "\"" + NameLong.Replace("\"", "\\\"") + "\"");
        writeValue(wr, indent, null, CSEUnit.ToString());
        writeValue(wr, indent, null, Rate.ToString());
        if (QPBlob != null)
        {
          A2LKWP_TP_BLOB tpBlob = getParent<A2LNODE>().getNode<A2LKWP_TP_BLOB>(false);
          writeValue(wr, indent, null, A2LC_QP_BLOB);
          switch (tpBlob.EVersion)
          {
            case KWP_VERSION.NotSet:
              writeValue(wr, indent, null, QPBlob.NoOfSamplings.ToString());
              writeValue(wr, indent, null, QPBlob.MeasuremntMode.ToString());
              writeValue(wr, indent, null, toHex(QPBlob.BlockModeID));
              writeValue(wr, indent, null, QPBlob.MaxSamplingRate.ToString());
              writeValue(wr, indent, null, QPBlob.MaxNoOfSignals.ToString());
              break;
            case KWP_VERSION.VDA_1996:
              writeValue(wr, indent, null, QPBlob.PhysicalLayer.ToString().Replace('_', '+'));
              writeValue(wr, indent, null, QPBlob.MeasuremntMode.ToString());
              writeValue(wr, indent, null, toHex(QPBlob.BlockModeID));
              writeValue(wr, indent, null, QPBlob.MaxNoOfSignals.ToString());
              writeValue(wr, indent, null, QPBlob.MaxNoOfBytes.ToString());
              writeValue(wr, indent, null, QPBlob.CANId.ToString());
              writeValue(wr, indent, null, QPBlob.Discriminator.ToString());
              break;
          }
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
  partial class A2LKWP_TP_BLOB
  {
    internal A2LKWP_TP_BLOB() { }
    internal override void writeToStream(StreamWriter wr, int major, int minor)
    {
      try
      {
        string indent = getIndent(this, true);
        wr.WriteLine();
        writeValue(wr, indent, null, toHex(Version));
        switch (mEVersion)
        {
          case KWP_VERSION.NotSet:
            // smaller v20XX
            writeValue(wr, indent, null, toHex(ECUAddress));
            writeValue(wr, indent, null, toHex(TesterAddress));
            writeValue(wr, indent, null, StimulationMode.ToString().Substring(1));
            writeValue(wr, indent, null, ByteOrder.ToString());
            writeValue(wr, indent, null, StartDiagWithoutBdSwitch ? "1" : "0");
            writeValue(wr, indent, null, toHex(ProjectBaseAdress));
            if (SERAM != null)
            {
              writeValue(wr, indent, null, A2LC_DATA_SERAM);
              writeValue(wr, indent, null, toHex(SERAM.A));
              writeValue(wr, indent, null, toHex(SERAM.O));
              writeValue(wr, indent, null, toHex(SERAM.U));
              writeValue(wr, indent, null, toHex(SERAM.E));
              writeValue(wr, indent, null, toHex(SERAM.AdrFLASH));
              writeValue(wr, indent, null, toHex(SERAM.AdrRAM));
              writeValue(wr, indent, null, (SERAM.Flags & KWP_DATA_ACCESS_FLAGS.ReadData) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (SERAM.Flags & KWP_DATA_ACCESS_FLAGS.VerifyCode) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (SERAM.Flags & KWP_DATA_ACCESS_FLAGS.ReadCode) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (SERAM.Flags & KWP_DATA_ACCESS_FLAGS.RWOnlyOnActivePage) > 0 ? "1" : "0");
            }
            if (BAUD_DEFs != null)
            {
              foreach (BAUD_DEF bd in BAUD_DEFs)
              {
                writeValue(wr, indent, null, A2LC_BAUD_DEF);
                writeValue(wr, indent, null, bd.BaudRate.ToString());
                writeValue(wr, indent, null, toHex(bd.DiagMode));
                writeValue(wr, indent, null, toHex(bd.ik));
              }
            }
            if (TIME_DEFs != null)
            {
              foreach (KWP_TIME_DEF td in TIME_DEFs)
              {
                writeValue(wr, indent, null, A2LC_TIME_DEF);
                writeValue(wr, indent, null, toHex(td.p1Max));
                writeValue(wr, indent, null, toHex(td.p2Min));
                writeValue(wr, indent, null, toHex(td.p2Max));
                writeValue(wr, indent, null, toHex(td.p3Min));
                writeValue(wr, indent, null, toHex(td.p3Max));
                writeValue(wr, indent, null, toHex(td.p4Min));
              }
            }
            break;
          case KWP_VERSION.VDA_1996:
            // greater and equal v20XX
            writeValue(wr, indent, null, EVersion.ToString());
            writeValue(wr, indent, null, ByteOrder.ToString());
            if (DATA_ACCESS != null)
            {
              writeValue(wr, indent, null, A2LC_DATA_ACCESS);
              writeValue(wr, indent, null, toHex(DATA_ACCESS.AdrFLASH));
              writeValue(wr, indent, null, toHex(DATA_ACCESS.AdrRAM));
              writeValue(wr, indent, null, (DATA_ACCESS.Flags & KWP_DATA_ACCESS_FLAGS.ReadData) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (DATA_ACCESS.Flags & KWP_DATA_ACCESS_FLAGS.VerifyCode) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (DATA_ACCESS.Flags & KWP_DATA_ACCESS_FLAGS.ReadCode) > 0 ? "1" : "0");
              writeValue(wr, indent, null, (DATA_ACCESS.Flags & KWP_DATA_ACCESS_FLAGS.RWOnlyOnActivePage) > 0 ? "1" : "0");
            }
            break;
        }
        if (SECURITY_ACCESSs != null)
        {
          foreach (SECURITY_ACCESS sa in SECURITY_ACCESSs)
          {
            writeValue(wr, indent, null, A2LC_SECURITY_ACCESS);
            writeValue(wr, indent, null, sa.AccessMode.ToString());
            writeValue(wr, indent, null, sa.CalcMode.ToString());
            writeValue(wr, indent, null, sa.Delaytime.ToString());
          }
        }
      }
      catch (Exception ex)
      {
        throw new WriterException(this, string.Empty, ex);
      }
      finally { base.writeToStream(wr, major, minor); }
    }
  }
}
#endregion
#endif