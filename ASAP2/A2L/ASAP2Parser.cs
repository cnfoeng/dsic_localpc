﻿/*!
 * @file    
 * @brief   Implements the ASAP2 parser
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2.CCP;
using jnsoft.ASAP2.Formulas;
using jnsoft.ASAP2.Helpers;
using jnsoft.ASAP2.Properties;
using jnsoft.ASAP2.XCP;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// Contains the ASAP2 parser base and the ASAP2 object model implementation...
/// 
/// Currently the parser implements the following:
/// - All entities defined in the ASAM_MCD-2MC Specifcation V1.7.0 except the IF_DATA sections.
/// - Creating a fully-navigatable object model.
/// - Parser performance is about 4 MBit/s.
/// - The parser is able to rewrite the object model to an A2L file.
/// - The parser ist able to export the object model to a XML file.
/// <hr>
/// 
/// Integration of ASAP2 V1.7.A2LINSTANCEs into the object model:
/// - To keep the writer functionality, any A2L object must be reproduced into the A2L object model. This also applies to A2LINSTANCEs and A2LTYPEDEFs, too 
/// - An A2LINSTANCE and its corresponding A2LTYPEDEF represents an already known type of [A2LBLOB,A2LMEASUREMENT,A2LCHARACTERISTIC,A2LAXIS_PTS].
/// <br>The parser creates dynamically instances of this types from A2LINSTANCEs into the object model.
/// <br>The dynamically created instances could be indicated with the introduced property A2LNODE.IsInstanced.
/// <hr>
/// 
/// TODOs:
/// - Currently the IF_DATA sections are not supported because these are vendor specific.
/// Parse the AML language for integrating vendor independent IF_DATA sections.
/// <hr>
/// 
/// 
/// To get started with parsing see A2LParser
/// </summary>
namespace jnsoft.ASAP2
{
  /// <summary>
  /// The ASAP2 parser.
  /// 
  /// <remarks>
  /// Depending on the Settings.Default.ParseStrict value, the parser acts strictly against the ASAP2 specification.
  /// In perticular this means
  /// - Case sensitive identifier referencing
  /// - No regognition of MEMORY_SEGMENT without begin..end tags
  /// - No regognition of FORMULA, FORMULA_INV without begin..end tags
  /// </remarks>
  /// </summary>
  /// <example>
  /// Getting started with parsing (code example):
  /// <code>
  /// using jnsoft.ASAP2;
  /// 
  /// void parse(string a2lFilename)
  /// {
  ///   // Instantiate an new parser instance
  ///   using ( var a2lParser = new A2LParser())
  ///   {
  ///     // register delegate for tracking parser messages
  ///     a2lParser.ParserMessage += new EventHandler<ParserEventArgs>(onParserMessage);
  ///     // register delegate for tracking parser progress
  ///     mA2lParser.ProgressChanged += new EventHandler<A2LParserProgressEventArgs>(onProgressChanged);
  ///   
  ///     // starting the parser process (synchronous call)
  ///     a2lParser.parse(a2lFilename);
  ///     if (null == mA2lParser.Project)
  ///       // failed completely to parse or not an a2l file.
  ///       return;
  ///     // Complete A2L object model now in mA2LParser.Project
  ///     // do something with it
  ///   }
  /// }
  /// void onParserMessage(object sender, EventArgs e)
  /// {
  ///   var message = e as ParserEventArgs;
  ///   // do domething with the parser message
  ///   switch (message.Type)
  ///   {
  ///     case MessageType.Error:
  ///       break;
  ///     case MessageType.Warning:
  ///       break;
  ///     case MessageType.Info:
  ///       break;
  ///   }
  /// }
  /// void onProgressChanged(object sender, EventArgs e )
  /// {
  ///   var message = e as A2LParserProgressEventArgs;
  ///   Console.WriteLine("parsed " + message.LinesProcessed + " from " + message.LinesMax);
  /// }
  /// </code>
  /// </example>
  /// <example>
  /// Getting started with writing an a2lFile (code example):
  /// <code>
  /// using jnsoft.ASAP2;
  /// 
  /// void loadAndSaveFile(string a2lFilename)
  /// {
  ///   // Instantiate an new parser instance
  ///   using(var a2lParser = new A2LParser())
  ///   {
  ///     // starting the parser process (synchronous call)
  ///     a2lParser.parse(a2lFilename);
  ///   
  ///     // Rewriting the ASAP2 file
  ///     a2lParser.write(Path.ChangeExtension(a2lFilename, ".out")+".a2l", WriterSortMode.ByTypeAndName);
  ///   }
  /// }
  /// </code>
  /// </example>
  public sealed class A2LParser : IDisposable
  {
    #region local types
    /// <summary>
    /// Possible parser states while parsing.
    /// </summary>
    enum State
    {
      None,
      InComment,
      InLineComment,
      InDescription,
      PendingEndDescription,
      PendingInclude,
      InString,
    }
    sealed class NodeParameter
    {
      internal A2LNODE Node;
      internal StringBuilder Content;
      internal int ParListIndex;
      internal static NodeParameter Empty = new NodeParameter();
      NodeParameter() { }
      internal NodeParameter(A2LNODE node, int parListIndex) { Node = node; ParListIndex = parListIndex; }
      internal void append(char character)
      {
        if (Content == null)
          Content = new StringBuilder();
        Content.Append(character);
      }
    }
    #endregion
    #region members
    const int mFileCache = UInt16.MaxValue * 16;
    int mBufferSize = 0x10000;
    const string INCLUDE = "/include";
    const string START = "/begin";
    const string END = "/end";
    const string ASAP2_VERSION = "ASAP2_VERSION";
    A2LPROJECT mProject;
    A2LMeasDict mMeasDict = new A2LMeasDict();
    A2LCharDict mCharDict = new A2LCharDict();
    A2LRefBaseDict mFuncDict = new A2LRefBaseDict();
    A2LRefBaseDict mGroupDict = new A2LRefBaseDict();
    A2LCompDict mCompDict = new A2LCompDict();
    A2LRecLayDict mRecLayDict = new A2LRecLayDict();
    A2LCompTabDict mCompTabDict = new A2LCompTabDict();
    A2LCompVTabDict mCompVTabDict = new A2LCompVTabDict();
    A2LUnitDict mUnitDict = new A2LUnitDict();
    A2LVirtualDict mVirtualDict = new A2LVirtualDict();
    A2LBlobDict mBlobDict = new A2LBlobDict();
    A2LTypeDefDict mTypeDefDict = new A2LTypeDefDict();
    A2LInstanceDict mInstanceDict = new A2LInstanceDict();
    A2LTransformerDict mTransformerDict = new A2LTransformerDict();
    A2LCompVTabRangeDict mCompVTabRangeDict = new A2LCompVTabRangeDict();
    A2LFormulaDict mFormulaDict = new A2LFormulaDict();
    int mCountObjects;
    int mCountErrors;
    int mCountWarnings;
    int mMajorVersion = -1;
    int mMinorVersion = -1;
    string mFileName;
    #endregion
    #region events
    /// <summary>Register here for parser progress messages.</summary>
    public EventHandler<A2LParserProgressEventArgs> ProgressChanged;
    /// <summary>Register here for parser messages.</summary>
    public EventHandler<ParserEventArgs> ParserMessage;
    #endregion
    #region constructor
    static A2LParser()
    {
      var assembly = Assembly.GetExecutingAssembly();
      var version = assembly.GetName().Version;
      Version = string.Format("{0}.{1}.{2}", version.Major, version.MajorRevision, version.Build);
    }
    /// <summary>
    /// Creates a new instance of the A2LParser.
    /// </summary>
    public A2LParser()
    {
      IncludedFiles = new List<string>();
    }
    #endregion
    #region properties
    /// <summary>
    /// A potential temporary filename if the A2L contains Include commands.
    /// 
    /// This file is created while parsing the source A2L file and contains 
    /// merged file from contained /include commands, too.
    /// </summary>
    public string TmpFileName { get; private set; }
    /// <summary>
    /// List of (successfully) included files.
    /// 
    /// This is a result of contained 'include' commands in the A2L content.
    /// </summary>
    public List<string> IncludedFiles { get; private set; }
    /// <summary>
    /// Gets the parser version string in the format Release.Revision.Build.
    /// </summary>
    public static string Version { get; private set; }
    /// <summary>
    /// Gets the fully qualified path to the file parsed from (may be null).
    /// </summary>
    public string FileName { get { return mFileName; } }
    /// <summary>
    /// Gets the Project node (this is the root node of any ASAP2 file).
    /// 
    /// - The root node and the main entrance into the ASAP2 model. 
    /// Therefore the Parent of this instance is always null.
    /// - If this node is null, parsing was not started or failed.
    /// </summary>
    public A2LPROJECT Project { get { return mProject; } }
    /// <summary>
    /// Gets a value indicating the count of errors occured while the file was parsed.
    /// </summary>
    public int CountErrors { get { return mCountErrors; } }
    /// <summary>
    /// Gets a value indicating the count of warnings occured while the file was parsed.
    /// </summary>
    public int CountWarnings { get { return mCountWarnings; } }
    /// <summary>
    /// Gets the major version of the loaded ASAP2 document.
    /// 
    /// Returns -1 if not valid.
    /// </summary>
    public int MajorVersion { get { return mMajorVersion; } }
    /// <summary>
    /// Gets the minor version of the loaded ASAP2 document.
    /// 
    /// Returns -1 if not valid.
    /// </summary>
    public int MinorVersion { get { return mMinorVersion; } }
    #endregion
    #region methods
    #region interface
    /// <summary>
    /// Parses the specified file and builds the object model.
    /// 
    /// - You may request the properties CountErrors and CountWarnings after parsing
    /// to check if parsing was completely successful
    /// </summary>
    /// <param name="filename">The fully qualified path to the a2l source</param>
    /// <param name="deleteEmptyNodes">Delete nodes which does not contain any information</param>
    /// <param name="encoding">Potential file content encoding (maybe null, Encoding.Default is used if not set)</param>
    /// <returns>true if successful</returns>
    /// <exception cref="IOException">If specified file is not acccessible</exception>
    /// <exception cref="NonCompliantException">Thrown if file is not a2l compliant</exception>
    public bool parse(string filename, bool deleteEmptyNodes = false, Encoding encoding = null)
    {
      var startTime = DateTime.Now;
      int lineCount = parseImpl(filename, deleteEmptyNodes, encoding != null ? encoding : Encoding.Default);
      bool result = onFinished(lineCount, (UInt32)DateTime.Now.Subtract(startTime).TotalMilliseconds);
      if (!result)
        return false;

      // Check and build node links
      onParserMessage(new ParserEventArgs(mProject
        , MessageType.Info
        , string.Format(Resources.strStartCheckingLinks))
        );

      startTime = DateTime.Now;

      if (mMajorVersion >= 1 && mMinorVersion >= 70)
        addInstancesToModel();

      int count = checkNodeLinks();
      onParserMessage(new ParserEventArgs(mProject
        , MessageType.Info
        , string.Format(Resources.strNodeCompletedMsg, count, DateTime.Now.Subtract(startTime).Milliseconds)
        ));
      return result;
    }

    /// <summary>
    /// Adds defined A2LINSTANCE objects depending on their type to the object model.
    /// </summary>
    void addInstancesToModel()
    {
      var module = Project.getNode<A2LMODULE>(false);
      if (module == null)
        return;
      var instances = module.getNodeList<A2LINSTANCE>(false);
      foreach (var instance in instances)
      {
        var typeDef = instance.RefTypedef;
        if (typeDef == null)
        { // typedef not defined
          onParserMessage(new ParserEventArgs(typeDef
            , MessageType.Error
            , string.Format(Resources.strMissingTypedef, instance.TypedefName))
            );
          continue;
        }

        int count = 1;
        int[] indizes = null;
        if (instance.MatrixDim != null)
        { // MATRIX_DIM defined
          foreach (var dim in instance.MatrixDim)
            count *= dim;
          indizes = new int[instance.MatrixDim.Length];
        }

        A2LADDRESS instancedNode = null;
        for (int i = 0; i < count; ++i)
        {
          if (typeDef is A2LTYPEDEF_BLOB)
            instancedNode = new A2LBLOB(module, instance, (A2LTYPEDEF_BLOB)typeDef);
          else if (typeDef is A2LTYPEDEF_MEASUREMENT)
            instancedNode = new A2LMEASUREMENT(module, instance, (A2LTYPEDEF_MEASUREMENT)typeDef);
          else if (typeDef is A2LTYPEDEF_AXIS)
            instancedNode = new A2LAXIS_PTS(module, instance, (A2LTYPEDEF_AXIS)typeDef);
          else if (typeDef is A2LTYPEDEF_CHARCTERISTIC)
            instancedNode = new A2LCHARACTERISTIC(module, instance, (A2LTYPEDEF_CHARCTERISTIC)typeDef);

          if (instancedNode == null)
            continue;

          if (indizes != null)
          { // handle MATRIX_DIM definition
            var sb = new StringBuilder();
            for (int j = 0; j < indizes.Length; ++j)
              sb.Append($"[{indizes[j]}]");
            instancedNode.Name += sb.ToString();
            instancedNode.Address = (UInt32)(instance.Address + i * instancedNode.getMemorySize());

            for (int k = indizes.Length - 1; k >= 0; --k)
            { // update array indizes
              if (indizes[k] == instance.MatrixDim[k] - 1)
              { // update next index level
                indizes[k] = 0;
                continue;
              }
              ++indizes[k];
              break;
            }
          }

          module.Childs.Add(instancedNode);
        }
      }
    }

    /// <summary>
    /// Parses the specified file and builds the object model.
    /// 
    /// - You may request the properties CountErrors and CountWarnings after parsing
    /// to check if parsing was completely successful
    /// </summary>
    /// <param name="filename">The fully qualified path to the a2l source</param>
    /// <param name="deleteEmptyNodes">Delete nodes which does not contain any information</param>
    /// <param name="encoding">File content encoding</param>
    /// <returns>the count of lines parsed (if result is valid)</returns>
    /// <exception cref="IOException">If specified file is not acccessible</exception>
    /// <exception cref="NonCompliantException">Thrown if file is not a2l compliant</exception>
    int parseImpl(string filename, bool deleteEmptyNodes, Encoding encoding)
    {
      var fsr = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, mFileCache, FileOptions.SequentialScan);
      using (var sr = new StreamReader(fsr, encoding, true))
      {
        mFileName = filename;
        int nodeBalance = 0, commentCtr = 0, lineNo = 0;
        var nodeStack = new Stack<NodeParameter>();
        var nodePar = NodeParameter.Empty;
        var parameters = new List<string>();
        StringBuilder currStr = new StringBuilder(), currLine = new StringBuilder();
        bool nodeFound = false, nodeDeleted = false, parameterValid = false, emptyDescription = false;
        bool includeFound = false, asap2VersionFound = false, inUnsupportedNode = false;
        int inIFDATA = 0, startLine = 0, charIdx = 0, overallIdx = 0, overallLen = 0, charMax = 0;
        string dataIndent = string.Empty;
        var nestedCommentStarts = new Dictionary<int, int>();
        char currChar = '\0', prevChar;
        State state = State.None;
        var buffer = new char[mBufferSize];

        string tmpFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        var fs = new FileStream(tmpFile, FileMode.Create, FileAccess.Write, FileShare.None, mFileCache);
        using (var sw = new StreamWriter(fs, encoding))
        {
          TmpFileName = tmpFile;
          overallLen = (int)sr.BaseStream.Length;

          while (charIdx <= charMax)
          {
            if (charIdx == charMax)
            { // reread from file
              charMax = sr.Read(buffer, 0, mBufferSize);
              if (charMax == 0)
                break;
              charIdx = 0;
            }

            prevChar = currChar;
            currChar = buffer[charIdx];
            ++charIdx; ++overallIdx;

            if (overallIdx % 0x100000 == 0)
              // do a progress event
              onParserProgressMessage(new A2LParserProgressEventArgs(overallIdx, overallLen));

            if (nodePar.Node is A2LUNSUPPORTEDNODE && parameterValid)
              // append all characters if not is unsupported
              nodePar.append(currChar);

            if (!includeFound)
            { // content tracking
              if (currChar == '\x0D' || currChar == '\x0A' && prevChar != '\x0D')
              { // newline
                ++lineNo;
                if (currLine.Length == 0)
                  sw.WriteLine();
                else
                {
                  int LineOffset = currLine[0] == '\x0A' ? 1 : 0;
                  sw.WriteLine(currLine.ToString(LineOffset, currLine.Length - LineOffset));
                }
                currLine.Clear();
                if (state == State.InLineComment)
                  state = State.None;
              }
              else
                currLine.Append(currChar);
            }

            if (state == State.PendingEndDescription)
            { // Description end is pending
              if (currChar == '"')
              { // escaped quote (non-well formed, stay in description)
                state = State.InDescription;
                currStr.Append(currChar);
                continue;
              }
              else
                state = State.None;
            }
            else
            { // character
              switch (currChar)
              {
                case '*':
                  if (prevChar == '/' && state != State.InDescription)
                  { // start of comment
                    commentCtr = inUnsupportedNode ? 1 : commentCtr + 1;
                    currStr.Clear();

                    if (state == State.InComment && !inUnsupportedNode)
                    { // test violation (state is already in comment)
                      if (Settings.Default.StrictParsing)
                      { // ASAP2 spec does not allow nested commentss
                        onParserMessage(new ParserEventArgs(mProject, MessageType.Error
                         , string.Format(Resources.strNestedCommentNotAllowed, lineNo + 1)));
                        return 0;
                      }
                      if (!nestedCommentStarts.ContainsKey(commentCtr))
                        nestedCommentStarts[commentCtr] = lineNo + 1;
                    }
                    state = State.InComment;
                    break;
                  }
                  goto default;

                case '/':
                  if (state != State.InDescription)
                  {
                    if (prevChar == '*')
                    { // end of comment
                      --commentCtr;
                      if (commentCtr <= 0)
                      { // end of all nested comments
                        state = State.None;
                        break;
                      }
                    }
                    else if (prevChar == '/' && state != State.InComment)
                    { // start of line comment
                      state = State.InLineComment;
                      currStr.Clear();
                      continue;
                    }
                  }
                  goto default;

                case '\\':
                  if (state == State.InDescription && prevChar == '\\')
                  { // escaped backslash
                    currChar = '\0';
                    continue;
                  }
                  else
                    goto default;

                case '"':
                  if (state == State.InComment)
                    continue;
                  if (prevChar == '\\')
                  { // escaped quote
                    if (currStr.Length > 0)
                      currStr.Remove(currStr.Length - 1, 1);
                    goto default;
                  }
                  if (state == State.InDescription)
                  { // also test on the next character
                    state = State.PendingEndDescription;
                    emptyDescription = true;
                    continue;
                  }
                  else
                    state = State.InDescription;
                  break;

                case '\x0A':
                case '\x0D':
                case ' ':
                case '\t':
                  switch (state)
                  {
                    case State.InString: state = State.None; break;
                  }
                  if (state == State.InDescription)
                    goto default;
                  break;

                default:
                  if (state == State.None)
                    state = State.InString;
                  switch (state)
                  {
                    case State.InString:
                    case State.InDescription:
                      currStr.Append(currChar);
                      break;
                  }
                  break;
              }
            }

            if (state != State.None || (currStr.Length == 0 && !emptyDescription))
              continue;

            emptyDescription = false;
            string par = currStr.ToString();

            if (nodeStack.Count == 0 && par == ASAP2_VERSION)
              // indicate to start parsing the asap2 version
              asap2VersionFound = true;
            else if (asap2VersionFound && mMajorVersion == -1)
              // get major asap2 version
              int.TryParse(par, out mMajorVersion);
            else if (asap2VersionFound && mMajorVersion != -1)
            { // get minor asap2 version
              int.TryParse(par, out mMinorVersion);
              asap2VersionFound = false;
            }
            else
            {
              switch (par)
              {
                case INCLUDE: // Include keyword found
                  currLine.Clear();
                  includeFound = true;
                  break;
                case START: // begin keyword found
                  ++mCountObjects; ++nodeBalance; nodeFound = true; startLine = lineNo;
                  break;
                case END: // end keyword found
                  --nodeBalance; parameterValid = false;
                  nodePar = nodeStack.Count > 0 ? nodeStack.Pop() : NodeParameter.Empty;
                  if (nodePar.Node != null)
                  { // current node is available
                    nodePar.Node.EndLine = lineNo;
                    if (nodePar.Node.Type == A2LType.IF_DATA)
                      --inIFDATA;
                    if (nodePar.Content != null && nodePar.Content.Length > 0)
                    { // store content for unsupported nodes
                      nodePar.Content.Length -= (END.Length + 1);
                      int i = nodePar.Content.Length - 1;
                      while (i > 0 && char.IsWhiteSpace(nodePar.Content[i]))
                        --i;
                      nodePar.Content.Length = i + 1;
                      ((A2LUNSUPPORTEDNODE)nodePar.Node).Content = nodePar.Content.ToString();
                      nodePar.Content = null;
                    }
                    bool validParameter = true;
                    try
                    { // Initialize the node with collected parameters
                      validParameter = nodePar.Node.onInitFromParameter(this, ref parameters, nodePar.ParListIndex);
                    }
                    catch (FormatException ex)
                    { // catch all format exceptions within A2L Nodes
                      onParserMessage(new ParserEventArgs(nodePar.Node, MessageType.Error
                       , string.Format(Resources.strValueFormatError, ex.Message)));
                    }
                    catch (ArgumentException ex)
                    { // catch all argument exceptions within A2L Nodes
                      onParserMessage(new ParserEventArgs(nodePar.Node, MessageType.Error
                       , string.Format(Resources.strValueFormatError, ex.Message)));
                    }
                    catch (OverflowException ex)
                    { // catch all overflown exceptions within A2L Nodes
                      onParserMessage(new ParserEventArgs(nodePar.Node, MessageType.Error
                       , string.Format(Resources.strValueOverflowError, ex.Message)));
                    }
                    catch (Exception ex)
                    { // catch all other exceptions within A2L Nodes
                      onParserMessage(new ParserEventArgs(nodePar.Node, MessageType.Error
                       , string.Format(Resources.strValueUnkownError, ex.Message)));
                    }

                    // Remove processed parameters
                    parameters.RemoveRange(nodePar.ParListIndex, parameters.Count - nodePar.ParListIndex);

                    if (deleteEmptyNodes && !validParameter
                      && (nodePar.Node.Childs == null || nodePar.Node.Childs.Count == 0)
                      && nodePar.Node.Parent != null
                      )
                    { // no parameters nor any childs -> delete
                      nodePar.Node.Parent.Childs.Remove(nodePar.Node);
                      nodeDeleted = true;
                    }
                    // may be this node type is in any dictionary
                    else if (!addToDictionaries(nodePar.Node) && nodePar.Node.Parent != null)
                    { // double definition, delete element
                      nodePar.Node.Parent.Childs.Remove(nodePar.Node);
                      nodeDeleted = true;
                    }
                    inUnsupportedNode = false;
                    commentCtr = 0;
                  }
                  break;
                default:
                  if (includeFound)
                  { // open include file
                    includeFound = false;
                    prevChar = currChar;
                    int includeSize = getInclude(par, encoding, nodePar.Node, ref charIdx, ref charMax, ref mBufferSize, ref buffer);
                    overallLen += includeSize;
                  }
                  else
                  { // default
                    if (nodeFound)
                    { // node is found
                      parameterValid = true; nodeFound = false;
                      if (nodePar.Node != null && nodePar.Content != null && nodePar.Content.Length > 0)
                      { // handle content for unsupported nodes
                        string content = nodePar.Content.ToString();
                        int i = content.IndexOf('/');
                        if (i >= 0)
                          nodePar.Content.Length = i;
                      }
                      A2LNODE node = A2LNODE.create(this, nodePar.Node, par, startLine, inIFDATA > 0);
                      nodePar = new NodeParameter(node, parameters.Count);
                      if (mProject == null && A2LType.PROJECT == node.Type)
                        // first node -> create the project
                        mProject = node as A2LPROJECT;

                      inUnsupportedNode = !(node is A2LIF_DATA) && node is A2LUNSUPPORTEDNODE;

                      dataIndent = A2LNODE.getIndent(node, true);
                      nodeStack.Push(nodePar);
                    }
                    else
                    { // not a found node
                      if (parameterValid)
                      { // valid parameter
                        parameters.Add(par);
                        if (null != nodePar.Node && nodePar.Node.Type == A2LType.IF_DATA && nodePar.Node.Name == null)
                        { // Name for IF_DATA nodes
                          nodePar.Node.Name = par;
                          ++inIFDATA;
                        }
                      }
                      else
                      { // end tag check
                        parameterValid = true;
                        if (!nodeDeleted && nodePar.Node != null)
                        {
                          string expectedName = nodePar.Node.Type.ToString();
                          if (nodePar.Node.Type != A2LType.UNSUPPORTED && expectedName != par)
                          { // end delimiter incomplete
                            onParserMessage(new ParserEventArgs(nodePar.Node, MessageType.Warning
                              , string.Format(Resources.strNodeNameNotConform, lineNo, expectedName, par))
                              );
                          }
                        }
                        nodePar = nodeStack.Count > 0 ? nodeStack.Peek() : NodeParameter.Empty;
                      }
                    }
                  }
                  break;
              }
            }
            currStr.Clear();
          }

          if (currLine.Length > 0)
          {
            int LineOffset = currLine[0] == '\x0A' ? 1 : 0;
            string LineResult = currLine.ToString(LineOffset, currLine.Length - LineOffset);
            if (LineResult.Length > 0)
              sw.Write(LineResult);
          }
        }

        if (commentCtr > 0)
        { // comment not balanced
          var s = new StringBuilder();
          foreach (int line in nestedCommentStarts.Values)
            s.AppendLine(line.ToString());
          onParserMessage(new ParserEventArgs(mProject, MessageType.Error
           , string.Format(Resources.strCommentNotBalanced, s)));
        }
        else if (nodeBalance != 0)
          onParserMessage(new ParserEventArgs(mProject, MessageType.Error
           , string.Format(Resources.strNodeNotBalanced)));

        // add complete message
        onParserProgressMessage(new A2LParserProgressEventArgs(overallLen, overallLen));
        return commentCtr <= 0 && nodeBalance == 0 ? lineNo : 0;
      }
    }
    /// <summary>
    /// Opens, reads and inserts a potential include file into the currnet A2L content.
    /// </summary>
    /// <param name="filename">The filename to include</param>
    /// <param name="node">The current node</param>
    /// <param name="encoding">File content encoding</param>
    /// <param name="charIdx">The current character buffer index</param>
    /// <param name="charMax">The current maximum of buffered characters</param>
    /// <param name="bufferSize">[in/out] The (modified) buffersize</param>
    /// <param name="charBuffer">[in/out] The modified character buffer</param>
    /// <returns>The count of inserted characters</returns>
    int getInclude(string filename, Encoding encoding, A2LNODE node, ref int charIdx, ref int charMax, ref int bufferSize, ref char[] charBuffer)
    {
      string prevDir = Environment.CurrentDirectory;
      try
      {
        if (!Path.IsPathRooted(filename))
          // If path is relative to the file
          // set the current directory to the original file directory
          Environment.CurrentDirectory = Path.GetDirectoryName(mFileName);

        using (StreamReader sr = new StreamReader(filename, encoding, true, mBufferSize))
        {
          string s = sr.ReadToEnd();
          int existingCnt = charMax - charIdx;
          bufferSize = existingCnt + s.Length + 1;
          char[] newBuffer = new char[bufferSize];
          s.CopyTo(0, newBuffer, 0, s.Length);
          newBuffer[s.Length] = '\n';
          Array.Copy(charBuffer, charIdx, newBuffer, s.Length + 1, existingCnt);
          charBuffer = newBuffer;
          charIdx = 0;
          charMax = newBuffer.Length;
          IncludedFiles.Add(((FileStream)sr.BaseStream).Name);
          onParserMessage(new ParserEventArgs(node, MessageType.Info
            , string.Format(Resources.strRefIncludedFileMsg, Path.GetFileName(filename))
            ));
          return (int)sr.BaseStream.Length;
        }
      }
      catch (Exception ex)
      {
        onParserMessage(new ParserEventArgs(node, MessageType.Error
          , string.Format(Resources.strRefFailedToIncludedMsg, Path.GetFileName(filename), ex.Message)
          ));
        return 0;
      }
      finally
      { // force always to reset the current dir
        Environment.CurrentDirectory = prevDir;
      }
    }
#if ASAP2_WRITER
    /// <summary>
    /// Write the ASAP2 model to the specified file.
    /// 
    /// - All unsupported A2L nodes (e.g the A2ML node and unsupported IF_DATA childs)
    /// are written in their original form
    /// - Any managed A2L node gets completely rewritten
    /// - Comments get lost (this is by design, they are not parsed from the source)
    /// </summary>
    /// <param name="filename">the fully qualified path to the ASAP2 file to create</param>
    /// <param name="sortMode">The sort mode to apply before writing</param>
    /// <param name="optimize">true if the output should be optimized (remove redundant objects)</param>
    /// <param name="optimizeCaseInsensitive">true if the output optimizer should use case insensitivity</param>
    /// <param name="useOriginalVersion">true, if the ASAP2 version from the orginal document should be written</param>
    /// <returns>true if sucessful</returns>
    public bool write(string filename
      , WriterSortMode sortMode = WriterSortMode.None
      , bool optimize = false, bool optimizeCaseInsensitive = false
      , bool useOriginalVersion = true
      )
    {
      if (mProject == null || string.IsNullOrEmpty(filename))
        return false;
      try
      {
        if (optimize)
          mProject.optimizeModel(this, optimizeCaseInsensitive);

        mProject.sortModel(sortMode);

        // now write the whole thing
        using (var sw = new StreamWriter(filename, false, Encoding.Default, 16 * UInt16.MaxValue))
          mProject.writeToStream(sw
            , string.Format("Original file: {0}{1}"
            , Path.GetFileName(mFileName)
            , optimize
              ? string.Format("\n   created with optimizing mode")
              : string.Empty
            )
            , useOriginalVersion ? mMajorVersion : -1
            , useOriginalVersion ? mMinorVersion : -1
            );
      }
      catch (WriterException ex)
      {
        onParserMessage(new ParserEventArgs(ex.Node
          , MessageType.Error
          , string.Format(Resources.strWriterFailed, mProject.Name)
          ));
        return false;
      }
      onParserMessage(new ParserEventArgs(mProject
        , MessageType.Info
        , string.Format(Resources.strWriterSuccessful, filename)
        ));
      return true;
    }
    /// <summary>
    /// Write the ASAP2 object model in XML format to the specified file.
    /// 
    /// - All unsupported A2L nodes (e.g the A2ML node and unsupported IF_DATA childs)
    /// are written as empty nodes.
    /// - Comments get lost (this is by design, they are not parsed from the source)
    /// </summary>
    /// <param name="filename">the fully qualified path to the XML file to create</param>
    /// <param name="sortMode">The sort mode to apply before writing</param>
    /// <param name="optimize">true if the output should be optimized (remove redundant objects)</param>
    /// <param name="optimizeCaseInsensitive">true if the output optimizer should use case insensitivity</param>
    /// <returns>true if sucessful</returns>
    public bool writeToXML(string filename, WriterSortMode sortMode, bool optimize, bool optimizeCaseInsensitive)
    {
      if (mProject == null || string.IsNullOrEmpty(filename))
        return false;
      try
      {
        if (optimize)
          mProject.optimizeModel(this, optimizeCaseInsensitive);

        mProject.sortModel(sortMode);

        using (var tw = new XmlTextWriter(filename, Encoding.UTF8))
        { // use an XML text writer and prepare it
          tw.Formatting = Formatting.Indented;
          switch (Settings.Default.WriterIndent)
          {
            case WriterIndent.Space:
              tw.Indentation = 2;
              tw.IndentChar = ' ';
              break;
            case WriterIndent.Tab:
              tw.Indentation = 1;
              tw.IndentChar = '\t';
              break;
          }

          var serializer = jnsoft.Helpers.Extensions.getXmlSerializer(typeof(A2LPROJECT), mProject.getTypes());

          // prevent from exporting a namespace
          var ns = new XmlSerializerNamespaces();
          ns.Add("", "");

          // serialize to textwriter
          serializer.Serialize(tw, mProject, ns);
        }
      }
      catch (Exception ex)
      {
        onParserMessage(new ParserEventArgs(mProject
          , MessageType.Error
          , string.Format(Resources.strWriterXMLFailed
            , mProject.Name
            , ex.InnerException != null ? ex.InnerException.Message : ex.Message)
          ));
        return false;
      }
      onParserMessage(new ParserEventArgs(mProject
        , MessageType.Info
        , string.Format(Resources.strWriterSuccessful, filename)
        ));
      return true;
    }
#endif
    #endregion
    /// <summary>
    /// This method checks all links referenced from nodes.
    /// </summary>
    /// <returns>The count of nodes checked</returns>
    int checkNodeLinks()
    {
      int count = 0;
      foreach (var node in mProject.enumChildNodes<A2LNODE>())
      {
        try
        {
          ++count;
          //Console.WriteLine(string.Format("checking node {0}", node.Name));

          if (node is A2LNAMEDNODE)
          {
            if (!string.IsNullOrEmpty(node.Name) && !isValidIdentifier(node.Name))
            { // invalid identifier
              onParserMessage(new ParserEventArgs(node
                , MessageType.Warning
                , string.Format(Resources.strInvalidIdentifier, node.Name))
                );
            }

            if (!string.IsNullOrEmpty(CToCSharpMath.getFunction(node.Name)))
            { // ident name is a reserved name
              onParserMessage(new ParserEventArgs(node
                , MessageType.Warning
                , string.Format(Resources.strIdentIsReserved, node.Name))
                );
            }
          }
          var modpar = node as A2LMODPAR;
          if (modpar != null && modpar.SystemConstants.Count > 0)
          { // check SYSTEM_CONSTANT definitions
            var en = modpar.SystemConstants.GetEnumerator();
            while (en.MoveNext())
            {
              if (!string.IsNullOrEmpty(CToCSharpMath.getFunction(en.Current.Key)))
                onParserMessage(new ParserEventArgs(node
                  , MessageType.Warning
                  , string.Format(Resources.strConstNameIsReserved, node.Name))
                  );
            }
          }

          if (node is A2LAXIS_DESCR)
          { // axis description
            var axisDesc = (A2LAXIS_DESCR)node;
            if (axisDesc.RefCompuMethod == A2LCOMPU_METHOD.mReplaceCompuMethod)
            { // missing compumethod
              onParserMessage(new ParserEventArgs(node
                , MessageType.Error
                , string.Format(Resources.strMissingCompuMethod, axisDesc.Conversion))
                );
            }

            if (!string.IsNullOrEmpty(axisDesc.InputQuantity) && axisDesc.InputQuantity != A2LNODE.mNoInputQuantity
              && axisDesc.RefMeasurement == null
              )
            { // missing input quantity
              onParserMessage(new ParserEventArgs(node
                , MessageType.Error
                , string.Format(Resources.strMissingInputQuantity, axisDesc.InputQuantity))
                );
            }

            switch (axisDesc.AxisType)
            {
              case AXIS_TYPE.COM_AXIS:
              case AXIS_TYPE.RES_AXIS:
                if (axisDesc.RefAxisPtsRef == null)
                  onParserMessage(new ParserEventArgs(node
                    , MessageType.Error
                    , string.Format(Resources.strMissingAxisPtsRef, axisDesc.AxisPtsRef))
                    );
                break;
              case AXIS_TYPE.CURVE_AXIS:
                if (axisDesc.RefCurveAxisRef == null)
                  onParserMessage(new ParserEventArgs(node
                    , MessageType.Error
                    , string.Format(Resources.strMissingCurveAxisRef, axisDesc.CurveAxisRef))
                    );
                break;
            }
          }
          else if (node is A2LAXIS_PTS)
          { // Check for axis pts input quantity
            var axisPtsNode = (A2LAXIS_PTS)node;
            if (!string.IsNullOrEmpty(axisPtsNode.InputQuantity) && axisPtsNode.InputQuantity != A2LNODE.mNoInputQuantity
              && axisPtsNode.RefMeasurement == null
              )
              onParserMessage(new ParserEventArgs(axisPtsNode
                , MessageType.Error
                , string.Format(Resources.strMissingInputQuantity, axisPtsNode.InputQuantity))
                );

            // check for missing record layouts
            var layout = axisPtsNode.RefRecordLayout;
            if (layout == null)
              onParserMessage(new ParserEventArgs(axisPtsNode
                , MessageType.Error
                , string.Format(Resources.strMissingRecordLayout, axisPtsNode.RecordLayout))
                );
            else
            { // check for missing axis layout
              if (layout.AxisPts[0] == null)
                onParserMessage(new ParserEventArgs(layout
                  , MessageType.Error
                  , string.Format(Resources.strMissingAxisLayout, "X"))
                  );
            }
          }
          else if (node is A2LCOMPU_METHOD)
          { // Check for compumethods table references
            var method = (A2LCOMPU_METHOD)node;
            switch (method.ConversionType)
            {
              case CONVERSION_TYPE.TAB_INTP:
              case CONVERSION_TYPE.TAB_NOINTP:
              case CONVERSION_TYPE.TAB_VERB:
                if (method.RefCompuTab == null)
                  onParserMessage(new ParserEventArgs(method
                    , MessageType.Error
                    , string.Format(Resources.strMissingTabConversion, method.CompuTabRef))
                    );
                break;
            }
          }
          else if (node is A2LREFERENCE)
          { // reference lists
            var refNode = (A2LREFERENCE)node;
            try { var nodes = refNode.ReferencedNodes; }
            catch (MissingNodeException ex)
            {
              onParserMessage(new ParserEventArgs(node
                , MessageType.Error
                , ex.Message
                ));
            }
          }
          else if (node is A2LXCP_ON_CAN)
          { // check extended CAN ID definition
            var xcpCAN = (A2LXCP_ON_CAN)node;
            checkExtCANId(node, xcpCAN.CANIDCmd);
            checkExtCANId(node, xcpCAN.CANIDResp);
            checkExtCANId(node, xcpCAN.CANIDBroadcast);
          }
          else if (node is A2LXCP_DAQ_LIST_CAN_ID)
          { // check extended CAN ID definition
            var xcpDAQListCanId = (A2LXCP_DAQ_LIST_CAN_ID)node;
            checkExtCANId(node, xcpDAQListCanId.CANId);
          }
          else if (node is A2LCCP_TP_BLOB)
          { // check extended CAN ID definition
            var ccpCAN = (A2LCCP_TP_BLOB)node;
            checkExtCANId(node, ccpCAN.CANIDCmd);
            checkExtCANId(node, ccpCAN.CANIDResp);
          }
          else if (node is A2LCCP_QP_BLOB)
          { // check extended CAN ID definition
            var ccpCAN = (A2LCCP_QP_BLOB)node;
            checkExtCANId(node, ccpCAN.CANId);
          }


          // check all layout refs
          if (node is A2LRECORD_LAYOUT_REF)
          { // characteristics and standalone axis points
            var recLayoutRef = (A2LRECORD_LAYOUT_REF)node;

            // check for missing record layouts
            var layout = recLayoutRef.RefRecordLayout;
            if (layout == null)
              onParserMessage(new ParserEventArgs(recLayoutRef
                , MessageType.Error
                , string.Format(Resources.strMissingRecordLayout, recLayoutRef.RecordLayout))
                );

            var characteristic = node as A2LCHARACTERISTIC;
            if (null != characteristic)
            { // characteristics
              if (layout != null && layout.FncValues == null)
                onParserMessage(new ParserEventArgs(layout
                  , MessageType.Error
                  , Resources.strMissingFncValuesInRecordLayout)
                  );

              if (characteristic.UpperLimitEx < characteristic.UpperLimit)
                // Upper limits check
                onParserMessage(new ParserEventArgs(characteristic
                  , MessageType.Warning
                  , string.Format(Resources.strUpperLimitViolation, characteristic))
                  );
              if (characteristic.LowerLimitEx > characteristic.LowerLimit)
                // Lower limits check
                onParserMessage(new ParserEventArgs(characteristic
                  , MessageType.Warning
                  , string.Format(Resources.strLowerLimitViolation, characteristic))
                  );

              if (Settings.Default.FixBitmaskLimits && characteristic.Bitmask != UInt64.MaxValue)
              { // check bitmask limits
                int shiftcount = jnsoft.Helpers.Extensions.getShiftCount(characteristic.Bitmask);
                double min = characteristic.RefCompuMethod.toPhysical(0);
                double max = characteristic.RefCompuMethod.toPhysical(characteristic.Bitmask >> shiftcount);
                if (min < characteristic.LowerLimit)
                {
                  onParserMessage(new ParserEventArgs(characteristic
                    , MessageType.Warning
                    , string.Format(Resources.strLowerLimitBitmaskViolation, min))
                    );
                  characteristic.LowerLimitEx = characteristic.LowerLimit = min;
                }

                if (max < characteristic.UpperLimit)
                {
                  onParserMessage(new ParserEventArgs(characteristic
                    , MessageType.Warning
                    , string.Format(Resources.strUpperLimitBitmaskViolation, max))
                    );
                  characteristic.UpperLimitEx = characteristic.UpperLimit = max;
                }
              }

              // Check if the correct count of axis descriptions exists
              var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
              switch (characteristic.CharType)
              {
                case CHAR_TYPE.ASCII:
                case CHAR_TYPE.VAL_BLK:
                  if (characteristic.Number == 0 && characteristic.MatrixDim == null)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingCountElements, characteristic))
                      );
                  break;
                case CHAR_TYPE.CURVE:
                  if (axisDescs.Count != 1)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisDesc, characteristic))
                      );
                  break;
                case CHAR_TYPE.MAP:
                  if (axisDescs.Count != 2)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisDesc, characteristic))
                      );
                  break;
                case CHAR_TYPE.CUBOID:
                  if (axisDescs.Count != 3)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisDesc, characteristic))
                      );
                  break;
                case CHAR_TYPE.CUBE_4:
                  if (axisDescs.Count != 4)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisDesc, characteristic))
                      );
                  break;
                case CHAR_TYPE.CUBE_5:
                  if (axisDescs.Count != 5)
                    onParserMessage(new ParserEventArgs(characteristic
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisDesc, characteristic))
                      );
                  break;
              }

              if (layout != null)
                for (int i = 0; i < axisDescs.Count; ++i)
                { // check for missing axis layout description
                  var axisDesc = axisDescs[i];
                  if (axisDesc.AxisType == AXIS_TYPE.STD_AXIS && layout.AxisPts[i] == null)
                    onParserMessage(new ParserEventArgs(layout
                      , MessageType.Error
                      , string.Format(Resources.strMissingAxisLayout, i == 0 ? "X" : i == 1 ? "Y" : i == 2 ? "Z" : i == 3 ? "4" : "5"))
                      );
                }
            }
          }

          if (node is A2LCONVERSION_REF)
          { // check all conversion refs for referenced compu methods
            var convRef = (A2LCONVERSION_REF)node;
            if (convRef.RefCompuMethod == A2LCOMPU_METHOD.mReplaceCompuMethod)
              onParserMessage(new ParserEventArgs(convRef
                , MessageType.Error
                , string.Format(Resources.strMissingCompuMethod, convRef.Conversion))
                );
          }
          #region V1.7.0
          else if (node is A2LSTRUCTURE_COMPONENT)
          { // check all typedef refs for referenced typedefs
            var structure = (A2LSTRUCTURE_COMPONENT)node;
            if (structure.RefTypedef == null)
              onParserMessage(new ParserEventArgs(structure
                , MessageType.Error
                , string.Format(Resources.strMissingTypedef, structure.TypedefName))
                );
          }
          #endregion
        }
        catch (ArgumentNullException ex)
        { // catch all argument exceptions within A2L Nodes
          onParserMessage(new ParserEventArgs(node, MessageType.Error
           , string.Format(Resources.strNullArgumentErr, ex.Message)));
        }
      }
      return count;
    }
    /// <summary>
    /// Checks, if on extended CAN ID definitions Bit 31 is set (extended marker).
    /// </summary>
    /// <param name="node">The corresponding node</param>
    /// <param name="canID">The CAN Id to check</param>
    void checkExtCANId(A2LNODE node, UInt32 canID)
    {
      if (canID == uint.MaxValue)
        // undefined
        return;
      bool extended = (canID & CANProvider.CAN_EXT_FLAG) > 0;
      uint value = canID & ~CANProvider.CAN_EXT_FLAG;
      if (!extended && value > CANProvider.CAN_STD_ID_MASK
         || extended && value > CANProvider.CAN_EXT_ID_MASK
         )
        onParserMessage(new ParserEventArgs(node
          , MessageType.Error
          , string.Format(Resources.strWrongExtCANIdDef, canID))
          );
    }
    /// <summary>
    /// Parser is finished.
    /// </summary>
    /// <param name="lines">The count of lines read</param>
    /// <param name="elapsedTime">The time elapsed</param>
    /// <returns>true, if the parsed file is ASAP2 compliant</returns>
    bool onFinished(int lines, UInt32 elapsedTime)
    {
      bool result = mProject != null && lines > 0;
      A2LMODPAR modPar = null;
      if (!result)
      {
        // failed overall
        onParserMessage(new ParserEventArgs(null
          , MessageType.Error
          , Resources.strNotAnA2LFile));
      }
      else
      { // successful
        A2LMODULE module;
        if (null == (module = mProject.getNode<A2LMODULE>(false)))
        { // no module entry available
          onParserMessage(new ParserEventArgs(mProject, MessageType.Error, Resources.strInvalidPrjMissingModule));
          return false;
        }

        if (module.getNode<A2LMOD_COMMON>(false) == null)
        { // no mod_common entry available
          var modCom = A2LMOD_COMMON.createDefault(Settings.Default.ModCommonBO);
          module.addChild(modCom);
          onParserMessage(new ParserEventArgs(modCom, MessageType.Info, string.Format(Resources.strModCommonCreated, Settings.Default.ModCommonBO)));
        }

        if ((modPar = module.getNode<A2LMODPAR>(false)) == null)
          onParserMessage(new ParserEventArgs(mProject, MessageType.Warning
            , string.Format(Resources.strPrjMissingModPar, Application.ProductName))
            );

        // all clear, go on
        mProject.MeasDict = mMeasDict;
        mProject.CharDict = mCharDict;
        mProject.RecLayDict = mRecLayDict;
        mProject.FuncDict = mFuncDict;
        mProject.GroupDict = mGroupDict;
        mProject.CompDict = mCompDict;
        mProject.CompTabDict = mCompTabDict;
        mProject.CompVTabDict = mCompVTabDict;
        mProject.CompVTabRangeDict = mCompVTabRangeDict;
        mProject.UnitDict = mUnitDict;
        mProject.VirtualDict = mVirtualDict;
        mProject.BlobDict = mBlobDict;
        mProject.TypeDefDict = mTypeDefDict;
        mProject.InstanceDict = mInstanceDict;
        mProject.TransformerDict = mTransformerDict;
        mProject.FormulaDict = mFormulaDict;
      }
      onParserMessage(new ParserEventArgs(mProject
        , MessageType.Info
        , string.Format(Resources.strParsingCompleted
          , mMajorVersion > -1 ? string.Format("{0}.{1}", mMajorVersion, mMinorVersion) : "Unknown"
          , lines, mCountObjects
          , elapsedTime)
        ));

      if (!result)
        // already failed
        return result;

      // Try building the system constant class
      Dictionary<string, string> systemConstants = null;
      if (modPar != null && modPar.SystemConstants != null)
      { // check system constants
        systemConstants = new Dictionary<string, string>();
        var enSC = modPar.SystemConstants.GetEnumerator();
        while (enSC.MoveNext())
        {
          if (!string.IsNullOrEmpty(enSC.Current.Value))
            // check validity of system constants
            systemConstants[enSC.Current.Key] = enSC.Current.Value.Trim();
          else
            onParserMessage(new ParserEventArgs(mProject
              , MessageType.Warning
              , string.Format(Resources.strEmptySystemConstant, enSC.Current.Key)
              ));
        }
      }

      if (systemConstants != null)
      { // modpar and system constants available
        var enSC = systemConstants.GetEnumerator();
        while (enSC.MoveNext())
          mFormulaDict.add(this, enSC.Current.Key, enSC.Current.Value);
      }
      // iterate over compu methods to build formulas
      var enComp = mCompDict.GetEnumerator();
      while (enComp.MoveNext())
      {
        var compuMethod = enComp.Current.Value;
        if (compuMethod.ConversionType != CONVERSION_TYPE.FORM)
          continue;
        mFormulaDict.add(this, compuMethod);
      }
      mFormulaDict.buildAssembly(this, systemConstants);
      return result;
    }
    /// <summary>
    /// Throws a non-compliant exception with the specified message.
    /// </summary>
    /// <param name="message">The exception details</param>
    void throwNonCompliantException(string message)
    {
      mProject = null;
      throw new NonCompliantException(message);

    }
    /// <summary>
    /// Throws a non-compliant exception with the specified message.
    /// </summary>
    /// <param name="lineNr">The line in whioch the exception occured</param>
    /// <param name="message">The exception details</param>
    void throwNonCompliantException(int lineNr, string message)
    {
      throwNonCompliantException(string.Format("Line {0}: {1}", lineNr + 1, message));
    }
    /// <summary>
    /// Parses a string into an integer value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The int value</returns>
    public static int parse2IntVal(string par)
    {
      return (int)parse2Int64Val(par);
    }
    /// <summary>
    /// Parses a string into a 64 Bit integer value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The int value</returns>
    public static Int64 parse2Int64Val(string par)
    {
      var style = NumberStyles.Integer;
      if (par.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
      {
        style = NumberStyles.AllowHexSpecifier;
        par = par.Substring(2);
      }
      return Int64.Parse(par, style, CultureInfo.InvariantCulture);
    }
    /// <summary>
    /// Parses a string into a 64 Bit integer value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The int value</returns>
    public static UInt32 parse2UIntVal(string par)
    {
      return (UInt32)parse2UInt64Val(par);
    }
    /// <summary>
    /// Parses a string into a 64 Bit integer value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The int value</returns>
    public static UInt64 parse2UInt64Val(string par)
    {
      var style = NumberStyles.Integer;
      if (par.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
      {
        style = NumberStyles.AllowHexSpecifier;
        par = par.Substring(2);
      }
      return UInt64.Parse(par, style, CultureInfo.InvariantCulture);
    }

    /// <summary>
    /// Parses the MATRIX_DIM paramters in v1.7 style.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="parameterList"></param>
    /// <returns></returns>
    internal static int[] parseMatrixDim(ref int i, ref List<string> parameterList)
    {
      var dimensions = new List<int>();
      while (dimensions.Count < 3 && i < parameterList.Count)
      {
        try { dimensions.Add(A2LParser.parse2IntVal(parameterList[i++])); }
        catch (FormatException) { --i; break; }
      }
      return dimensions.Count == 0 || dimensions.Count == 1 && dimensions[0] == 1
        ? null
        : dimensions.ToArray();
    }

    /// <summary>
    /// Parses a string into a double double value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The double value</returns>
    [DebuggerStepThrough]
    public static double parse2DoubleVal(string par)
    {
      if (par.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
        return parse2Int64Val(par);
      return double.Parse(par, NumberStyles.Float, CultureInfo.InvariantCulture);
    }
    /// <summary>
    /// Parses a string into a float value.
    /// 
    /// The string may contain a hexadecimal value.
    /// </summary>
    /// <param name="par">The parameter string</param>
    /// <returns>The float value</returns>
    [DebuggerStepThrough]
    public static float parse2SingleVal(string par)
    {
      if (par.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
        return parse2Int64Val(par);
      return float.Parse(par, NumberStyles.Float, CultureInfo.InvariantCulture);
    }
    /// <summary>
    /// Return a value if the specified source is a valid identifier.
    /// 
    /// - Important if specified source should be used in e.g. math formulas.
    /// - An empty string is never an identifier
    /// - The first character may never be a number
    /// - Invalid characters are e.g. -, +, /, ...
    /// </summary>
    /// <param name="source">The source string</param>
    /// <returns>tru if the identifier is valid</returns>
    static bool isValidIdentifier(string source)
    {
      if (string.IsNullOrEmpty(source))
        // an empty string is never an identifier
        return false;
      bool result = true;
      for (int i = 0; result && i < source.Length; ++i)
      {
        char curr = source[i];
        if (i > 0)
          result = curr >= 'A' && curr <= 'Z' || curr >= 'a' && curr <= 'z' || curr >= '0' && curr <= '9' || curr == '_'
            || curr == ']' || curr == '[' || curr == '.';
        else
          result = curr >= 'A' && curr <= 'Z' || curr >= 'a' && curr <= 'z' || curr == '_';
      }
      return result;
    }
    /// <summary>
    /// Adds the given a2l instance to its corresponding dictionary.
    /// </summary>
    /// <param name="a2lBase"></param>
    /// <returns>false, if dictionary entry already exists</returns>
    bool addToDictionaries(A2LNODE a2lBase)
    {
      try
      {
        switch (a2lBase.Type)
        {
          case A2LType.MEASUREMENT: mMeasDict.Add(a2lBase.Name, a2lBase as A2LMEASUREMENT); break;
          case A2LType.AXIS_PTS:
          case A2LType.CHARACTERISTIC: mCharDict.Add(a2lBase.Name, a2lBase as A2LRECORD_LAYOUT_REF); break;
          case A2LType.FUNCTION: mFuncDict.Add(a2lBase.Name, a2lBase as A2LFUNCTION); break;
          case A2LType.GROUP: mGroupDict.Add(a2lBase.Name, a2lBase as A2LGROUP); break;
          case A2LType.COMPU_METHOD: mCompDict.Add(a2lBase.Name, a2lBase as A2LCOMPU_METHOD); break;
          case A2LType.COMPU_TAB: mCompTabDict.Add(a2lBase.Name, a2lBase as A2LCOMPU_TAB); break;
          case A2LType.COMPU_VTAB: mCompVTabDict.Add(a2lBase.Name, a2lBase as A2LCOMPU_VTAB); break;
          case A2LType.COMPU_VTAB_RANGE: mCompVTabRangeDict.Add(a2lBase.Name, a2lBase as A2LCOMPU_VTAB_RANGE); break;
          case A2LType.RECORD_LAYOUT: mRecLayDict.Add(a2lBase.Name, a2lBase as A2LRECORD_LAYOUT); break;
          case A2LType.UNIT: mUnitDict.Add(a2lBase.Name, a2lBase as A2LUNIT); break;
          case A2LType.BLOB: mBlobDict.Add(a2lBase.Name, a2lBase as A2LBLOB); break;
          case A2LType.TYPEDEF_AXIS:
          case A2LType.TYPEDEF_BLOB:
          case A2LType.TYPEDEF_CHARACTERISTIC:
          case A2LType.TYPEDEF_MEASUREMENT:
          case A2LType.TYPEDEF_STRUCTURE: mTypeDefDict.Add(a2lBase.Name, a2lBase as A2LTYPEDEF); break;
          case A2LType.INSTANCE: mInstanceDict.Add(a2lBase.Name, a2lBase as A2LINSTANCE); break;
          case A2LType.TRANSFORMER: mTransformerDict.Add(a2lBase.Name, a2lBase as A2LTRANSFORMER); break;
        }
        return true;
      }
      catch (ArgumentException)
      {
        onParserMessage(new ParserEventArgs(mProject, MessageType.Warning
         , string.Format(Resources.strRefAlreadyExisting
           , a2lBase.Type.ToString(), a2lBase.Name
         )));
        return false;
      }
    }
    /// <summary>
    /// Send parser messages to registered listeners.
    /// </summary>
    /// <param name="e">The parser event arguments</param>
    public void onParserMessage(ParserEventArgs e)
    {
      switch (e.Type)
      {
        case MessageType.Error: ++mCountErrors; break;
        case MessageType.Warning: ++mCountWarnings; break;
      }
      ParserMessage?.Invoke(this, e);
    }
    /// <summary>
    /// Send parser progress changes to registered listeners.
    /// </summary>
    /// <param name="e">The parser event arguments</param>
    void onParserProgressMessage(A2LParserProgressEventArgs e)
    {
      ProgressChanged?.Invoke(this, e);
    }
    /// <summary>
    /// Deletes the created temporary file.
    /// </summary>
    public void Dispose()
    {
      try
      {
        if (!string.IsNullOrEmpty(TmpFileName) && File.Exists(TmpFileName))
          File.Delete(TmpFileName);
      }
      catch { }
    }
    #endregion
  }
  /// <summary>
  /// Progress EventArgument generated while parsing a file.
  /// </summary>
  public sealed class A2LParserProgressEventArgs : EventArgs
  {
    /// <summary>Gets the currently processed lines</summary>
    public int LinesProcessed { get; private set; }
    /// <summary>Gets line count to process</summary>
    public int LinesMax { get; private set; }
    internal A2LParserProgressEventArgs(int linesProcessed, int linesMax)
    { LinesMax = linesMax; LinesProcessed = linesProcessed; }
  }
  /// <summary>
  /// Used to get an additive description for a A2LNode from user code.
  /// </summary>
  public sealed class AdditiveDescriptionEventArgs : EventArgs
  {
    /// <summary>Add the additive description for the specified node.</summary>
    public string DescriptionAddition { internal get; set; }
  }
  /// <summary>
  /// A list of ASAP2 nodes.
  /// </summary>
  [XmlType(AnonymousType = true)]
  public sealed class A2LNodeList : List<A2LNODE>
  {
    /// <summary>
    /// Returns a sublist with all items of only the specified type.
    /// </summary>
    /// <typeparam name="T">Node type to return</typeparam>
    /// <returns>A list instance, empty if requested type is not contained.</returns>
    public List<T> ToArray<T>() where T : A2LNODE
    {
      var result = new List<T>();
      foreach (var node in this)
      {
        T resultNode = node as T;
        if (resultNode == null)
          continue;
        result.Add((T)node);
      }
      return result;
    }
  }
  #region exceptions
  /// <summary>
  /// Exception class for non ASAP2 compliant files.
  /// 
  /// Occurs, if the input file is not ASAP2 compliant
  /// and therefore not usable.
  /// </summary>
  public sealed class NonCompliantException : ApplicationException
  {
    internal NonCompliantException(string message) : base(message) { }
  }
  /// <summary>
  /// Exception class representing an ASAP2 Writer exception.
  /// 
  /// Occurs, if the any exception is caught while writing the ASAP2 file.
  /// </summary>
  public sealed class WriterException : ApplicationException
  {
    A2LNODE mNode;
    /// <summary>
    /// Gets the node which caused the writer exception.
    /// </summary>
    public A2LNODE Node { get { return mNode; } }
    internal WriterException(A2LNODE node, string message, Exception innerException) : base(message, innerException) { mNode = node; }
  }
  /// <summary>
  /// Exception class for missing references.
  /// 
  /// Only used internally to create parser messages.
  /// </summary>
  internal sealed class MissingNodeException : ApplicationException
  {
    internal MissingNodeException(string message) : base(message) { }
  }
  #endregion
  #region dictionaries
  /// <summary>
  /// A MEASUREMENT dictionary (key = NodeName, value = A2LMEASUREMENT)
  /// </summary>
  public sealed class A2LMeasDict : Dictionary<string, A2LMEASUREMENT>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LMeasDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A CHARACTERISTIC dictionary (key = NodeName, value = A2LRECORD_LAYOUT_REF (A2LCHARACTERISTIC or A2LAXIS_PTS)
  /// </summary>
  public sealed class A2LCharDict : Dictionary<string, A2LRECORD_LAYOUT_REF>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LCharDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A FUNCTION/GROUP dictionary (key = NodeName, value = A2LREFBASE)
  /// </summary>
  public sealed class A2LRefBaseDict : SortedDictionary<string, A2LREFBASE>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LRefBaseDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A COMPU_METHOD dictionary (key = NodeName, value = A2LCOMPU_METHOD)
  /// </summary>
  public sealed class A2LCompDict : SortedDictionary<string, A2LCOMPU_METHOD>, ICloneable
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LCompDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
    #region ICloneable Members
    public object Clone()
    {
      var result = new A2LCompDict();
      var en = GetEnumerator();
      while (en.MoveNext())
        result[en.Current.Key] = en.Current.Value;
      return result;
    }
    #endregion
  }
  /// <summary>
  /// A RECORD_LAYOUT dictionary (key = NodeName, value = A2LRECORD_LAYOUT)
  /// </summary>
  public sealed class A2LRecLayDict : SortedDictionary<string, A2LRECORD_LAYOUT>, ICloneable
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LRecLayDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
    #region ICloneable Members
    public object Clone()
    {
      var result = new A2LRecLayDict();
      var en = GetEnumerator();
      while (en.MoveNext())
        result[en.Current.Key] = en.Current.Value;
      return result;
    }
    #endregion
  }
  /// <summary>
  /// A COMPU_TAB dictionary (key = NodeName, value = A2LCOMPU_TAB)
  /// </summary>
  public sealed class A2LCompTabDict : SortedDictionary<string, A2LCOMPU_TAB>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LCompTabDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A COMPU_VTAB dictionary (key = NodeName, value = A2LCOMPU_VTAB)
  /// </summary>
  public sealed class A2LCompVTabDict : SortedDictionary<string, A2LCOMPU_VTAB>, ICloneable
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LCompVTabDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
    #region ICloneable Members
    public object Clone()
    {
      var result = new A2LCompVTabDict();
      var en = GetEnumerator();
      while (en.MoveNext())
        result[en.Current.Key] = en.Current.Value;
      return result;
    }
    #endregion
  }
  /// <summary>
  /// A COMPU_VTAB dictionary (key = NodeName, value = A2LCOMPU_VTAB_RANGE)
  /// </summary>
  public sealed class A2LCompVTabRangeDict : SortedDictionary<string, A2LCOMPU_VTAB_RANGE>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LCompVTabRangeDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A UNIT dictionary (key = NodeName, value = A2LUNIT)
  /// </summary>
  public sealed class A2LUnitDict : SortedDictionary<string, A2LUNIT>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LUnitDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A Virtaul dictionary (key = NodeName, value = A2LVIRTUAL)
  /// </summary>
  public sealed class A2LVirtualDict : SortedDictionary<string, A2LVIRTUAL>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LVirtualDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A BLOBs dictionary (key = NodeName, value = A2LBLOB)
  /// </summary>
  public sealed class A2LBlobDict : SortedDictionary<string, A2LBLOB>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LBlobDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A TypeDefs dictionary (key = NodeName, value = A2LTYPEDEF)
  /// </summary>
  public sealed class A2LTypeDefDict : SortedDictionary<string, A2LTYPEDEF>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LTypeDefDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A Instances dictionary (key = NodeName, value = A2LINSTANCE)
  /// </summary>
  public sealed class A2LInstanceDict : SortedDictionary<string, A2LINSTANCE>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LInstanceDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  /// <summary>
  /// A Instances dictionary (key = NodeName, value = A2LTRANSFORMER)
  /// </summary>
  public sealed class A2LTransformerDict : SortedDictionary<string, A2LTRANSFORMER>
  {
    /// <summary>
    /// Intialises a new instance as case sensitive or case insensitive, depending on Settings.Default.StrictParsing.
    /// </summary>
    public A2LTransformerDict()
      : base(Settings.Default.StrictParsing ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase) { }
  }
  #endregion
  #region interfaces
  /// <summary>
  /// Interface for nodes containing a description property.
  /// </summary>
  public interface IA2LDescription
  {
    /// <summary>
    /// Gets the nodes name.
    /// </summary>
    string Name { get; }
    /// <summary>
    /// Gets the nodes description (may be null or empty).
    /// </summary>
    string Description { get; }
    /// <summary>
    /// Gets the node's display name.
    /// </summary>
    string DisplayName { get; }
    /// <summary>
    /// Gets the nodes description including a potential available annotation (may be null or empty).
    /// </summary>
    /// <param name="addUserObjects">true if user objects should be added (if available)</param>
    /// <param name="maxLines">The maximum lines the return string may contain</param>
    /// <returns>The string created out of description and annotation</returns>
    string getDescriptionAndAnnotation(bool addUserObjects, int maxLines);
  }
  /// <summary>
  /// Interface to access both types of memory.
  /// </summary>
  public interface IA2LMemory
  {
    /// <summary>
    /// Gets the memory address.
    /// </summary>
    uint Address { get; }
    /// <summary>
    /// Gets the memory size.
    /// </summary>
    uint Size { get; }
    /// <summary>
    /// Gets the memory offsets.
    /// </summary>
    int[] Offsets { get; }
    /// <summary>
    /// Builds user readable description.
    /// </summary>
    /// <returns></returns>
    string toUserDescriptrion();
    /// <summary>
    /// Returns a value, if the specified address is contained in the memory segment.
    /// </summary>
    /// <param name="address">The address to test</param>
    /// <returns>true if the address is contained</returns>
    bool contains(UInt32 address);
    /// <summary>
    /// Returns a value indicating if the memory segment is a readonly segment.
    /// 
    /// Readonly segments are any code segments not defined as data segments.
    /// </summary>
    bool IsReadOnly { get; }
  }
  #endregion
}
