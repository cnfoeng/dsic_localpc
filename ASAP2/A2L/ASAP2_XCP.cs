﻿/*!
 * @file    
 * @brief   Implements the ASAP2 XCP Interface stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    April 2010
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.ASAP2.Properties;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

/// <summary>
/// Implements the ASAP2 XCP/XCPplus (IF_DATA) Interface stuff.
/// </summary>
namespace jnsoft.ASAP2.XCP
{
  #region types
  /// <summary>
  /// Basic communication mode flags.
  /// 
  /// Used in A2LXCP_PROTOCOL_LAYER, RespConnect.
  /// </summary>
  [Flags]
  public enum CommModeBasic : byte
  {
    /// <summary>not set</summary>
    NotSet = 0x00,
    /// <summary>
    /// If not set, litle endian is used, big endian if set.
    /// </summary>
    BigEndian = 0x01,
    /// <summary>
    /// Address granularity is in bytes (8 Bit).
    /// </summary>
    AddressGranularityBYTE = 0x00,
    /// <summary>
    /// Address granularity is in words (16 Bit).
    /// </summary>
    AddressGranularityWORD = 0x02,
    /// <summary>
    /// Address granularity is in dwords (32 Bit).
    /// </summary>
    AddressGranularityDWORD = 0x04,
    /// <summary>
    /// The SlaveBlockMode flag indicates whether the Slave Block Mode is available.
    /// </summary>
    SlaveBlockMode = 0x40,
    /// <summary>
    /// The OPTIONAL flag indicates whether additional 
    /// information on supported types of Communication 
    /// mode is available. The master can get that 
    /// additional information with GET_COMM_MODE_INFO.
    /// </summary>
    Optional = 0x80,
  }
  /// <summary>
  /// The PAG properties is a bit mask described below.
  /// 
  /// Used in RespGetPagProcessorInfo.
  /// </summary>
  [Flags]
  public enum PAGProperties : byte
  {
    /// <summary>
    /// The device supports freezing RAM memory to Flash.
    /// </summary>
    FreezeSupported = 0x01,
  }
  /// <summary>
  /// Defines the ID fields of the DAQKeyByte.
  /// 
  /// Used in A2LXCP_DAQ.
  /// </summary>
  public enum XCP_ID_FIELD_TYPE
  {
    /// <summary>
    /// Absolute ODT number.
    /// </summary>
    ABSOLUTE,
    /// <summary>
    /// Relative ODT number, absolute DAQ list number (BYTE).
    /// </summary>
    BYTE,
    /// <summary>
    /// Relative ODT number, absolute DAQ list number (WORD).
    /// </summary>
    WORD,
    /// <summary>
    /// Relative ODT number, absolute DAQ list.
    /// </summary>
    ALIGNED,
  }
  /// <summary>
  /// Granularity for size(granularity) of ODT entry (STIM or DAQ).
  /// 
  /// Used in A2LXCP_DAQ, A2LXCP_STIM.
  /// </summary>
  public enum XCP_ODT_ENTRY_SIZE : byte
  {
    /// <summary>
    /// ODT entry size is a Byte (8 Bit).
    /// </summary>
    BYTE = 0x01,
    /// <summary>
    /// ODT entry size is a Word (16 Bit).
    /// </summary>
    WORD = 0x02,
    /// <summary>
    /// ODT entry size is a DWord (32 Bit).
    /// </summary>
    DWORD = 0x04,
    /// <summary>
    /// ODT entry size is a QWord (64 Bit).
    /// </summary>
    DLONG = 0x08,
  }
  /// <summary>
  /// Possible timestamp size definitions.
  /// 
  /// Used in A2LXCP_TIMESTAMP_SUPPORTED.
  /// </summary>
  public enum XCP_TIMESTAMP_SIZE
  {
    /// <summary>No timestamp defined</summary>
    NotSet = 0,
    /// <summary>
    /// Timestamp size is a Byte (8 Bit).
    /// </summary>
    BYTE = 1,
    /// <summary>
    /// Timestamp size is a Word (16 Bit).
    /// </summary>
    WORD = 2,
    /// <summary>
    /// Timestamp size is a DWord (32 Bit).
    /// </summary>
    DWORD = 4,
  }
  /// <summary>
  /// Possible ECU access types.
  /// </summary>
  [Flags]
  public enum XCP_ECU_ACCESS : byte
  {
    NOT_ALLOWED = 0x01,
    WITHOUT_XCP_ONLY = 0x02,
    WITH_XCP_ONLY = 0x04,
    DONT_CARE = 0x08,
  }
  /// <summary>
  /// Possible read and write access types.
  /// </summary>
  [Flags]
  public enum XCP_READ_WRITE_ACCESS : byte
  {
    NOT_ALLOWED = 0x01,
    WITHOUT_ECU_ONLY = 0x02,
    WITH_ECU_ONLY = 0x04,
    DONT_CARE = 0x08,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  [Flags]
  public enum XCP_PGM_MODE
  {
    ABSOLUTE = 0x01,
    FUNCTIONAL = 0x02,
  }
  /// <summary>
  /// Possible timestamp Resolutions.
  /// 
  /// Used in A2LXCP_TIMESTAMP_SUPPORTED.
  /// </summary>
  public enum XCP_TIMESTAMP_RESOLUTION
  {
    /// <summary>1 NS = 1 nanosecond = 10E-9 second.</summary>
    _1NS = 0x00,
    /// <summary>10 NS = 10 nanoseconds = 10E-8 seconds.</summary>
    _10NS = 0x10,
    /// <summary>100 NS = 100 nanoseconds = 10E-7 seconds.</summary>
    _100NS = 0x20,
    /// <summary>1 US = 1 microsecond = 10E-6 second.</summary>
    _1US = 0x30,
    /// <summary>10 US = 10 microseconds = 10E-5 seconds.</summary>
    _10US = 0x40,
    /// <summary>100 US = 100 microseconds = 10E-4 seconds.</summary>
    _100US = 0x50,
    /// <summary>1 MS = 1 millisecond = 10E-3 second.</summary>
    _1MS = 0x60,
    /// <summary>10 MS = 10 milliseconds = 10E-1 seconds.</summary>
    _10MS = 0x70,
    /// <summary>100 MS = 100 milliseconds = 10E-1 seconds.</summary>
    _100MS = 0x80,
    /// <summary>1 S = 1 second.</summary>
    _1S = 0x90,
    /// <summary>1 PS = 1 picosecond = 10E-12 second.</summary>
    _1PS = 0xA0,
    /// <summary>10 PS = 10 picoseconds = 10E-11 seconds.</summary>
    _10PS = 0xB0,
    /// <summary>100 PS = 100 picoseconds = 10E-10 seconds.</summary>
    _100PS = 0xC0,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_OVERLOAD_IND
  {
    /// <summary>No overload indication.</summary>
    INDICATION,
    PID,
    EVENT,
  }
  /// <summary>
  /// Supported types of the event channel.
  /// </summary>
  public enum XCP_DAQ_LIST_TYPE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>
    /// Only DAQ_LISTs with DIRECTION = DAQ.
    /// </summary>
    DAQ,
    /// <summary>
    /// Only STIM_LISTs with DIRECTION = STIM.
    /// </summary>
    STIM,
    /// <summary>
    /// both kind of DAQ_LISTs.
    /// </summary>
    DAQ_STIM,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_DAQ_LIST_CAN_TYPE
  {
    /// <summary>not set</summary>
    NotSet,
    VARIABLE,
    FIXED,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_DAQ_LIST_CAN_SAMPLE_RATE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>1 sample per bit</summary>
    SINGLE,
    /// <summary>3 samples per bit</summary>
    TRIPLE,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_SYNCEDGE
  {
    /// <summary>not set</summary>
    NotSet,
    /// <summary>on falling edge only</summary>
    SINGLE,
    /// <summary>on falling and rising edge</summary>
    DUAL,
  }
  /// <summary>See XCP Version > 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_TRANSCEIVER_DELAY_COMPENSATION
  {
    /// <summary>not set</summary>
    NotSet,
    ON,
    OFF,
  }
  /// <summary>
  /// Possible XCP DAQ modes.
  /// 
  /// Used in A2LXCP_DAQ.
  /// </summary>
  public enum XCP_DAQ_MODE
  {
    /// <summary>
    /// Static DAQ lists.
    /// </summary>
    STATIC,
    /// <summary>
    /// Dynamic DAQ lists.
    /// </summary>
    DYNAMIC,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  [Flags]
  public enum XCP_OPTIMISATION_TYPE : byte
  {
    DEFAULT = 0x00,
    ODT_TYPE_16 = 0x01,
    ODT_TYPE_32 = 0x02,
    ODT_TYPE_64 = ODT_TYPE_16 | ODT_TYPE_32,
    ODT_TYPE_ALIGNMENT = 0x04,
    MAX_ENTRY_SIZE = ODT_TYPE_ALIGNMENT | ODT_TYPE_16,
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public enum XCP_ADDRESS_EXT : byte
  {
    /// <summary>
    /// address extension can be different within one and the same ODT.
    /// </summary>
    FREE = 0x00,
    /// <summary>
    /// address extension to be the same for all entries within one ODT.
    /// </summary>
    ODT = 0x01,
    /// <summary>
    /// address extension to be the same for all entries within one DAQ.
    /// </summary>
    DAQ = 0x03,
  }
  /// <summary>
  /// Header length for XCP on USB.
  /// </summary>
  public enum XCP_HEADER_LEN
  {
    /// <summary>
    /// Only the message length in one byte.
    /// </summary>
    BYTE,
    /// <summary>
    /// The message ctr and the length in one byte.
    /// </summary>
    CTR_BYTE,
    /// <summary>
    /// The fill byte.
    /// </summary>
    FILL_BYTE,
    /// <summary>
    /// Only the message length in one word.
    /// </summary>
    WORD,
    /// <summary>
    /// The message ctr and the length in one word.
    /// </summary>
    CTR_WORD,
    /// <summary>
    /// The fill word
    /// </summary>
    FILL_WORD,
  }
  /// <summary>
  /// Endpoint direction for XCP on USB.
  /// </summary>
  public enum XCP_ENDPOINT
  {
    NotSet,
    /// <summary>
    /// IN Direction.
    /// </summary>
    IN,
    /// <summary>
    /// OUT Direction.
    /// </summary>
    OUT,
  }
  /// <summary>
  /// Transfer types for XCP on USB.
  /// </summary>
  public enum XCP_TRANSFER
  {
    BULK_TRANSFER,
    INTERRUPT_TRANSFER,
  }
  /// <summary>
  /// Streaming mode for XCP on USB.
  /// </summary>
  public enum XCP_MESSAGE_PACKING
  {
    SINGLE,
    MULTIPLE,
    STREAMING,
  }
  /// <summary>
  /// Endpoint alignment for XCP on USB.
  /// </summary>
  public enum XCP_ALIGNMENT
  {
    /// <summary>8 Bit alignment</summary>
    _8_BIT,
    /// <summary>16 Bit alignment</summary>
    _16_BIT,
    /// <summary>32 Bit alignment</summary>
    _32_BIT,
    /// <summary>64 Bit alignment</summary>
    _64_BIT,
  }
  /// <summary>
  /// Possible supported BLOCK Modes.
  /// </summary>
  [Flags]
  public enum XCP_BLOCK_MODE
  {
    /// <summary>Standard mode</summary>
    STANDARD = 0x00,
    /// <summary>Slave Block mode is supported</summary>
    SLAVE = 0x01,
    /// <summary>Master Block mode is supported</summary>
    MASTER = SLAVE << 1,
    /// <summary>Interleaved Block mode is supported</summary>
    INTERLEAVED = SLAVE << 2,
  }
  /// <summary>When inserting the DTO CTR field.</summary>
  public enum DTO_CTR_DAQ_MODE
  {
    NotSet = -1,
    /// <summary>use CTR of the related event channel</summary>
    INSERT_COUNTER,
    /// <summary>use STIM CTR CPY of the related event channel</summary>
    INSERT_STIM_COUNTER_COPY,
  }
  /// <summary>When receiving DTOs with CTR field.</summary>
  public enum DTO_CTR_STIM_MODE
  {
    NotSet = -1,
    /// <summary>do not check CTR</summary>
    DO_NOT_CHECK_COUNTER,
    /// <summary>check CTR</summary>
    CHECK_COUNTER,
  }
  /// <summary>DAQ Event packed Group modes.</summary>
  public enum XCP_GROUP_MODE
  {
    /// <summary>A0A1A2B0B1B2C0C1C2D0D1D2</summary>
    ELEMENT_GROUPED,
    /// <summary>A0B0C0D0A1B1C1D1A2B2C2D2</summary>
    EVENT_GROUPED,
  }
  /// <summary>DAQ Event packed single timestamp modes.</summary>
  public enum XCP_STS_MODE
  {
    /// <summary>single timestamp of last sample</summary>
    LAST,
    /// <summary>single timestamp of first sample</summary>
    FIRST,
  }
  /// <summary>
  /// DAQ Event packed modes.
  /// </summary>
  public enum XCP_PACK_MODE
  {
    /// <summary>optional, EVENT allows also non-packed mode</summary>
    OPTIONAL,
    /// <summary>mandatory, only packed mode allowed</summary>
    MANDATORY,
  }
  /// <summary>
  ///  Ethernet packet alignment.
  /// </summary>
  public enum XCP_PACKET_ALIGMENT
  {
    _8, _16, _32, NotSet,
  }
  /// <summary>ECU State resource states.</summary>
  public enum XCP_RESOURCE_STATE
  {
    /// <summary>Resource is not active.</summary>
    NOT_ACTIVE,
    /// <summary>Resource is active.</summary>
    ACTIVE,
  }
  /// <summary>ECU State memory access.</summary>
  public enum XCP_MEMORY_ACCESS
  {
    /// <summary>Memory access on resource is not allowed.</summary>
    NOT_ALLOWED,
    /// <summary>Memory access on resource is allowed.</summary>
    ALLOWED,
  }
  /// <summary>DAQ timestamp relation.</summary>
  public enum XCP_TS_RELATION
  {
    /// <summary></summary>
    XCP_SLAVE_CLOCK,
    /// <summary></summary>
    ECU_CLOCK,
  }
  /// <summary>DAQ timestamp relation.</summary>
  public enum XCP_CLOCK_MODE
  {
    /// <summary></summary>
    XCP_SLAVE_CLOCK,
    /// <summary></summary>
    ECU_CLOCK,
    /// <summary>related to XCP_SLAVE_CLOCK</summary>
    XCP_SLAVE_GRANDMASTER_CLOCK,
    /// <summary>related to ECU_CLOCK in case of an external slave</summary>
    ECU_GRANDMASTER_CLOCK,
  }
  /// <summary>Clock readability</summary>
  public enum XCP_CLOCK_READABILITY
  {
    /// <summary></summary>
    RANDOMLY_READABLE,
    /// <summary></summary>
    LIMITED_READABLE,
    /// <summary></summary>
    NOT_READABLE,
  }
  /// <summary>Synchronization features.</summary>
  public enum XCP_CLOCK_SYNC_FEATURE
  {
    /// <summary>clock neither supports synchronization nor syntonization</summary>
    SYN_UNSUPPORTED,
    /// <summary>clock only supports synchronization to external grandmaster clock</summary>
    SYNCHRONIZATION_ONLY,
    /// <summary>clock only supports syntonization to external grandmaster clock</summary>
    SYNTONIZATION_ONLY,
    /// <summary>clock supports synchronization as well as syntonization to external grandmaster clock</summary>
    SYN_ALL,
  }
  /// <summary>Clock Epoch's.</summary>
  public enum XCP_CLOCK_EPOCH
  {
    /// <summary>International Atomic Time (TAI)</summary>
    ATOMIC_TIME,
    /// <summary>Coordinated Universal Time (UTC)</summary>
    UNIVERSAL_COORDINATED_TIME,
    /// <summary>unkown</summary>
    ARBITRARY,
  }
  /// <summary>Clocks timestamp characteristics native timestamp size</summary>
  public enum XCP_NATIVE_TIMESTAMP_SIZE
  {
    SIZE_FOUR_BYTE,
    SIZE_EIGHT_BYTE,
  }
  /// <summary>See XCP Version 1.1, EVENT</summary>
  public enum XCP_ADDRESS_MODE
  {
    /// <summary>not set</summary>
    NotSet,
    DAQ,
    EVENT,
  }

  /// <summary>
  /// Supported communication modes.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public struct XCPCommModes
  {
    /// <summary>A combination of supported comunication modes</summary>
    [DefaultValue(XCP_BLOCK_MODE.STANDARD)]
    public XCP_BLOCK_MODE CommMode { get; set; }
    /// <summary>Indicates the maximum allowed block size as the number of consecutive command packets (DOWNLOAD_NEXT) in a block sequence.</summary>
    [DefaultValue(typeof(byte), "0")]
    public byte MaxBS { get; set; }
    /// <summary>Indicates the required minimum separation time between the packets of a block transfer from the master device to the slave device in units of 100 microseconds.</summary>
    [DefaultValue(typeof(byte), "0")]
    public byte MinST { get; set; }
    /// <summary>Indicates the maximum number of consecutive command packets the master can send to the receipt queue of the slave.</summary>
    [DefaultValue(typeof(byte), "0")]
    public byte QueueSize { get; set; }
  }
  /// <summary>
  /// Represents a set of unique strings used to optional commands.
  /// </summary>
  public sealed class OptionalCommandSet : HashSet<string>
  {
    public override string ToString()
    {
      var sb = new StringBuilder();
      foreach (var s in this)
        sb.Append(s + "\n");
      if (sb.Length > 0)
        sb.Remove(sb.Length - 1, 1);
      return sb.ToString();
    }
  }

  #endregion
  /// <summary>
  /// Base class for all A2L node definitions concerning XCP.
  /// </summary>
  public partial class A2LXCP_NODE : A2LNODE
  {
    #region members
    protected const string XCPPLusDesc = "Requires IF_DATA defined as XCPplus";
    #endregion
    #region constructor
#if ASAP2_WRITER
    protected A2LXCP_NODE() { }
#endif
    internal A2LXCP_NODE(int startLine) : base(startLine) { }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      return true;
    }
    /// <summary>
    /// Gets the supported communciation modes, if defined.
    /// </summary>
    /// <param name="node">An XCP node instance</param>
    /// <param name="parameterList">The parameter list</param>
    /// <param name="i">The index into the parameter list to start from</param>
    /// <param name="parser">The parser object for notifiying messages</param>
    /// <returns>supported communication mode instance</returns>
    protected static XCPCommModes getCommModes(A2LXCP_NODE node, A2LParser parser, ref List<string> parameterList, ref int i)
    {
      var commModes = new XCPCommModes();
      if (parameterList[i++] == "BLOCK")
      { // check for supported communication modes
        int startModes = i;
        while (i < parameterList.Count)
        {
          var localMode = node.parse2Enum<XCP_BLOCK_MODE>(parameterList[i++], parser, true);
          if (localMode == XCP_BLOCK_MODE.STANDARD)
          { // no more modes found, or end of modes
            --i;
            break;
          }
          commModes.CommMode |= localMode;
          switch (localMode)
          {
            case XCP_BLOCK_MODE.MASTER:
              commModes.MaxBS = (byte)A2LParser.parse2IntVal(parameterList[i++]);
              commModes.MinST = (byte)A2LParser.parse2IntVal(parameterList[i++]);
              break;
            case XCP_BLOCK_MODE.INTERLEAVED:
              commModes.QueueSize = (byte)A2LParser.parse2IntVal(parameterList[i++]);
              break;
          }
        }
      }
      return commModes;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all A2LXCP entities with specific names.
  /// </summary>
  public abstract partial class A2LXCP_NAMEDNODE : A2LXCP_NODE
  {
    #region constructor
    /// <summary>
    /// Initialises a new instance of A2LNAMEDNODE with the specified parameters.
    /// </summary>
    /// <param name="startLine">The starting line</param>
    protected A2LXCP_NAMEDNODE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Gets the name of the node.
    /// 
    /// Use the setter with care! Changing the name requires to adjust all references to this instance.
    /// </summary>
    [Category(Constants.CATData), Description("The unique name of the A2LXCP_NAMEDNODE.")]
    public sealed override string Name { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      Name = parameterList[startIndex];
      return true;
    }
    #endregion
  }
  /// <summary>
  /// Hold a DAQList with it's attached Event.
  /// </summary>
  public struct DAQAndEvt
  {
    /// <summary>DAQ List</summary>
    public A2LXCP_DAQ_LIST DAQList;
    /// <summary>Event (may be null)</summary>
    public A2LXCP_EVENT Evt;
    /// <summary>
    /// Creates a new instance of DAQAndEvt.
    /// </summary>
    /// <param name="daqList">A DAQ List</param>
    /// <param name="evt">Event to attach</param>
    public DAQAndEvt(A2LXCP_DAQ_LIST daqList, A2LXCP_EVENT evt) { DAQList = daqList; Evt = evt; }
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_DAQ : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_DAQ(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Gets or sets a value if the DAQ lists that are not PREDEFINED shall be configured statically or dynamically.</summary>
    [Category(Constants.CATData), Description("Indicates whether the DAQ lists that are not PREDEFINED shall be configured statically or dynamically.")]
    public XCP_DAQ_MODE Mode { get; set; }
    /// <summary>Gets or sets the maximum number of DAQ Lists (static and configurable ones)</summary>
    [Category(Constants.CATData), Description("The maximum number of DAQ Lists (static and configurable ones.")]
    public UInt16 MaxDAQ { get; set; }
    /// <summary>Gets or sets the maximum number of available event channels</summary>
    [Category(Constants.CATData), Description("The maximum number of available event channels.")]
    public UInt16 MaxEvtChn { get; set; }
    /// <summary>Gets or sets the first configurable DAQ List number</summary>
    [Category(Constants.CATData), Description("The first configurable DAQ List number.")]
    public byte MinDAQ { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(EnumConverter))]
    public XCP_OPTIMISATION_TYPE OptType { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(EnumConverter))]
    public XCP_ADDRESS_EXT AdrExt { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(EnumConverter))]
    public XCP_ID_FIELD_TYPE IDField { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(EnumConverter)), DefaultValue(XCP_ODT_ENTRY_SIZE.BYTE)]
    public XCP_ODT_ENTRY_SIZE ODTEntrySize { get; set; }
    /// <summary>Gets or sets the maxmimum size in bytes of a single ODT entry.</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description("The maxmimum size in bytes of a single ODT entry.")]
    public byte MaxODTEntrySize { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(EnumConverter))]
    public XCP_OVERLOAD_IND OverloadInd { get; set; }
    /// <summary>
    /// Display Event Channel Number.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(UInt16), "65535"), Description("Display Event Channel Number.")]
    public UInt16 DAQAlternatingSupported { get; set; } = UInt16.MaxValue;
    [Category(Constants.CATData), DefaultValue(false)]
    public bool PrescalerSupported { get; set; }
    [Category(Constants.CATData), DefaultValue(false)]
    public bool ResumeSupported { get; set; }
    [Category(Constants.CATData), DefaultValue(false)]
    public bool StoreDAQSupported { get; set; }
    [Category(Constants.CATData), DefaultValue(false)]
    public bool DtoCtrFieldSupported { get; set; }
    [Category(Constants.CATData), DefaultValue(false)]
    public bool PIDOffSupported { get; set; }
    /// <summary>Configuration limit DAQ, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxDAQTotal { get; set; }
    /// <summary>Configuration limit ODT, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTTotal { get; set; }
    /// <summary>Configuration limit ODT/DAQ, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTDAQTotal { get; set; }
    /// <summary>Configuration limit ODT/STIM, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTSTIMTotal { get; set; }
    /// <summary>Configuration limit ODT Entries, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTEntriesTotal { get; set; }
    /// <summary>Configuration limit ODT Entries on DAQ, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTEntriesDAQTotal { get; set; }
    /// <summary>Configuration limit ODT Entries on STIM, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MaxODTEntriesSTIMTotal { get; set; }
    /// <summary>Configuration limit CPU load, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(0f)]
    public float CpuLoadMaxTotal { get; set; }
    /// <summary>Configuration limit Core load, (XCPplus extension)</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(0f)]
    public float CoreLoadMaxTotal { get; set; }
    /// <summary>Holds DAQ List and corresponding XCP Event</summary>
    [XmlIgnore, Browsable(false)]
    public SortedDictionary<UInt16, DAQAndEvt> DAQAndEvtMap { get; private set; } = new SortedDictionary<UInt16, DAQAndEvt>();
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Mode = parse2Enum<XCP_DAQ_MODE>(parameterList[i++], parser);
      MaxDAQ = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      MaxEvtChn = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      MinDAQ = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      int lastIndex = parameterList[i].IndexOf('_') + 1;
      lastIndex += parameterList[i].Substring(lastIndex).IndexOf('_') + 1;
      OptType = parse2Enum<XCP_OPTIMISATION_TYPE>(parameterList[i++].Substring(lastIndex), parser);
      lastIndex = parameterList[i].LastIndexOf('_');
      AdrExt = parse2Enum<XCP_ADDRESS_EXT>(parameterList[i++].Substring(lastIndex + 1), parser);
      lastIndex = parameterList[i].LastIndexOf('_');
      IDField = parse2Enum<XCP_ID_FIELD_TYPE>(parameterList[i++].Substring(lastIndex + 1), parser);
      lastIndex = parameterList[i].LastIndexOf('_');
      ODTEntrySize = parse2Enum<XCP_ODT_ENTRY_SIZE>(parameterList[i++].Substring(lastIndex + 1), parser);
      MaxODTEntrySize = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      lastIndex = parameterList[i].LastIndexOf('_');
      OverloadInd = parse2Enum<XCP_OVERLOAD_IND>(parameterList[i++].Substring(lastIndex + 1), parser);

      while (i < parameterList.Count)
        switch (parameterList[i++])
        {
          case A2LC_DAQ_ALTERNATING_SUPPORTED: DAQAlternatingSupported = (UInt16)A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_PRESCALER_SUPPORTED: PrescalerSupported = true; break;
          case A2LC_RESUME_SUPPORTED: ResumeSupported = true; break;
          case A2LC_STORE_DAQ_SUPPORTED: StoreDAQSupported = true; break;
          case A2LC_DTO_CTR_FIELD_SUPPORTED: DtoCtrFieldSupported = true; break;
          case A2LC_PID_OFF_SUPPORTED: PIDOffSupported = true; break;
          case A2LC_MAX_DAQ_TOTAL: MaxDAQTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_TOTAL: MaxODTTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_DAQ_TOTAL: MaxODTDAQTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_STIM_TOTAL: MaxODTSTIMTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_ENTRIES_TOTAL: MaxODTEntriesTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_ENTRIES_DAQ_TOTAL: MaxODTEntriesDAQTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_MAX_ODT_ENTRIES_STIM_TOTAL: MaxODTEntriesSTIMTotal = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_CPU_LOAD_MAX_TOTAL: CpuLoadMaxTotal = A2LParser.parse2SingleVal(parameterList[i++]); break;
          case A2LC_CORE_LOAD_MAX_TOTAL: CoreLoadMaxTotal = A2LParser.parse2SingleVal(parameterList[i++]); break;
        }

      buildDAQAndEvtDict(parser);
      return true;
    }
    /// <summary>
    /// Gets the size of the ID field.
    /// </summary>
    /// <returns></returns>
    public byte getIDFieldSize()
    {
      byte result = 0;
      switch (IDField)
      {
        case XCP_ID_FIELD_TYPE.ABSOLUTE: result = 1; break;
        case XCP_ID_FIELD_TYPE.BYTE: result = 2; break;
        case XCP_ID_FIELD_TYPE.WORD: result = 3; break;
        case XCP_ID_FIELD_TYPE.ALIGNED: result = 4; break;
      }
      return result;
    }
    /// <summary>
    /// Maps the existing XCP Events to existing DAQ Lists.
    /// </summary>
    void buildDAQAndEvtDict(A2LParser parser)
    {
      var daqLists = getNodeList<A2LXCP_DAQ_LIST>(false);

      var evtDict = new Dictionary<ushort, A2LXCP_EVENT>();
      foreach (var evnt in getNodeList<A2LXCP_EVENT>(false))
        evtDict[evnt.Id] = evnt;

      var evtUsedCnt = new Dictionary<A2LXCP_EVENT, int>();
      foreach (var evnt in evtDict.Values)
        evtUsedCnt[evnt] = 0;

      foreach (var daqList in daqLists)
      {
        A2LXCP_EVENT evnt;
        if (daqList.EventFixed != UInt16.MaxValue)
        {
          if (evtDict.TryGetValue(daqList.EventFixed, out evnt))
          { // fixed connection from DAQ List to specific event
            if (evtUsedCnt[evnt] < evnt.MaxDAQList)
            { // event allows to connect to DAQ List
              DAQAndEvtMap[daqList.DAQNo] = new DAQAndEvt(daqList, evnt);
              ++evtUsedCnt[evnt];
            }
          }
          else
            parser.onParserMessage(new ParserEventArgs(this, MessageType.Error
              , string.Format("DAQ List {0} is referencing an undefined Event ID {1}!", daqList.DAQNo, daqList.EventFixed)
              ));
        }
        else
        { // distribute remaining (non-fixed) events
          A2LXCP_EVENT selectedEvnt = null;
          foreach (var localEvent in evtDict.Values)
          {
            if (evtUsedCnt[localEvent] < localEvent.MaxDAQList)
            { // event allows to connect to DAQ List
              selectedEvnt = localEvent;
              ++evtUsedCnt[localEvent];
              break;
            }
          }
          DAQAndEvtMap[daqList.DAQNo] = new DAQAndEvt(daqList, selectedEvnt);
        }
      }
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
  public sealed partial class A2LXCP_DAQ_MEMORY_CONSUMPTION : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_DAQ_MEMORY_CONSUMPTION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>DAQ_MEMORY_LIMIT: in Elements[AG]</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt32), "0")]
    public UInt32 DAQMemoryLimit { get; set; }
    /// <summary>DAQ_SIZE: number of elements[AG] per DAQ list</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 DAQSize { get; set; }
    /// <summary>ODT_SIZE: number of elements[AG] per ODT</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 ODTSize { get; set; }
    /// <summary>ODT_ENTRY_SIZE: number of elements[AG] per ODT_entry</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 ODTEntrySize { get; set; }
    /// <summary>ODT_DAQ_BUFFER_ELEMENT_SIZE: number of payload elements[AG]*factor = sizeof(send buffer)[AG]</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 ODTDAQBufferElementSize { get; set; }
    /// <summary>ODT_STIM_BUFFER_ELEMENT_SIZE: number of payload elements[AG]*factor = sizeof(send buffer)[AG]</summary>
    [Category(Constants.CATDataXCPplus), Description(XCPPLusDesc), DefaultValue(typeof(UInt16), "0")]
    public UInt16 ODTSTIMBufferElementSize { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQMemoryLimit = Math.Min((UInt32)A2LParser.parse2Int64Val(parameterList[i++]), UInt32.MaxValue);
      DAQSize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue);
      ODTSize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue);
      ODTEntrySize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue);
      ODTDAQBufferElementSize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue);
      ODTSTIMBufferElementSize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, overrules default BUFFER_RESERVE for this EVENT).
  /// 
  /// Overrules default BUFFER_RESERVE for this EVENT.
  /// </summary>
  public sealed partial class A2LXCP_BUFFER_RESERVE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_BUFFER_RESERVE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>ODT_DAQ_BUFFER_ELEMENT_RESERVE in % of ODT_DAQ_BUFFER_ELEMENT_SIZE</summary>
    [Category(Constants.CATData)]
    public byte ODT_DAQ { get; set; }
    /// <summary>ODT_STIM_BUFFER_ELEMENT_RESERVE in % of ODT_STIM_BUFFER_ELEMENT_SIZE</summary>
    [Category(Constants.CATData)]
    public byte ODT_STIM { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      ODT_DAQ = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      ODT_STIM = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_STIM : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_STIM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public XCP_ODT_ENTRY_SIZE ODTEntrySize { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(byte), mStrHexByteMax)]
    public byte MaxODTEntrySize { get; set; } = byte.MaxValue;
    /// <summary>
    /// bitwise stimulation supported
    /// </summary>
    [Category(Constants.CATDataXCPplus), DefaultValue(false), Description("bitwise stimulation supported.")]
    public bool BitSTIMSupported { get; set; }
    /// <summary>
    /// Separation time between DTOs time in units of 100 microseconds.
    /// </summary>
    [Category(Constants.CATDataXCPplus), DefaultValue(typeof(byte), "0"), Description("Separation time between DTOs time in units of 100 microseconds.")]
    public byte MinSTSTIM { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      int lastIndex = parameterList[i].LastIndexOf('_');
      ODTEntrySize = parse2Enum<XCP_ODT_ENTRY_SIZE>(parameterList[i++].Substring(lastIndex + 1), parser);
      MaxODTEntrySize = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_BIT_STIM_SUPPORTED: BitSTIMSupported = true; break;
          case A2LC_MIN_ST_STIM: MinSTSTIM = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  /// </summary>
  public sealed partial class A2LXCP_CHECKSUM : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_CHECKSUM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// The checksum computation method defined to compute checksums.
    /// </summary>
    [Category(Constants.CATData), Description("The checksum computation method defined to compute checksums.")]
    public ChecksumType CheckSum { get; set; }
    /// <summary>
    /// Maximum block size for checksum calculation (optional).
    /// </summary>
    [Category(Constants.CATData), Description("Maximum block size for checksum calculation (optional)."), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt32), mStrHexDWordMax)]
    public UInt32 MaxBlockSize { get; set; } = UInt32.MaxValue;
    /// <summary>
    /// Name of the Checksum function including file extension without path (optional).
    /// </summary>
    [Category(Constants.CATData), Description("Name of the Checksum function including file extension without path (optional).")]
    public string ExternalFunction { get; set; }
    /// <summary>Required alignment of MTA and block size (XCPplus)</summary>
    [Category(Constants.CATDataXCPplus), Description("Required alignment of MTA and block size."), DefaultValue(typeof(UInt16), "0")]
    public UInt16 MTABlockSizeAlign { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        string sub4 = parameterList[i++].Substring(0, 4);
        switch (sub4)
        {
          case "XCP_": CheckSum = parse2Enum<ChecksumType>(parameterList[i - 1].Substring(4, parameterList[i - 1].Length - 4), parser); break;
          case "MAX_": MaxBlockSize = Math.Min((UInt32)A2LParser.parse2IntVal(parameterList[i++]), UInt32.MaxValue); break;
          case "EXTE": ExternalFunction = parameterList[i++]; break;
          case "MTA_": MTABlockSizeAlign = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_TIMESTAMP_SUPPORTED : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_TIMESTAMP_SUPPORTED(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 Ticks { get; set; }
    [Category(Constants.CATData)]
    public XCP_TIMESTAMP_SIZE Size { get; set; }
    [Category(Constants.CATData)]
    public XCP_TIMESTAMP_RESOLUTION Resolution { get; set; }
    /// <summary>
    /// Indicates if the timestamps are fixed.
    /// </summary>
    [Category(Constants.CATData), Description("Indicates if the timestamps are fixed."), DefaultValue(false)]
    public bool IsFixed { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Ticks = (UInt16)A2LParser.parse2Int64Val(parameterList[i++]);
      string sizeString;
      if (!(sizeString = parameterList[i++]).StartsWith("NO_"))
        Size = parse2Enum<XCP_TIMESTAMP_SIZE>(sizeString.Substring(5), parser);
      Resolution = parse2Enum<XCP_TIMESTAMP_RESOLUTION>(parameterList[i++].Substring(4), parser);
      if (parameterList.Count > i)
        IsFixed = parameterList[i++] == A2LC_TIMESTAMP_FIXED;
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_DAQ_LIST : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_DAQ_LIST(int startLine)
    : base(startLine)
    {
      EventFixed = UInt16.MaxValue;
      FirstPID = byte.MaxValue;
      Active = true;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 DAQNo { get; set; }
    [Category(Constants.CATData)]
    public XCP_DAQ_LIST_TYPE DAQListType { get; set; }
    [Category(Constants.CATData)]
    public byte MaxODT { get; set; }
    [Category(Constants.CATData)]
    public byte MaxODTEntries { get; set; }
    /// <summary>
    /// Always fixed to this A2LXCP_EVENT.
    /// 
    /// If not defined the value is UInt16.MaxValue.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description("Always fixed to this A2LXCP_EVENT"), DefaultValue(typeof(UInt16), mStrHexWordMax)]
    public UInt16 EventFixed { get; set; } = UInt16.MaxValue;
    [Category(Constants.CATData)]
    public byte FirstPID { get; set; }
    /// <summary>
    /// Used from the XCPMaster to mark as available for online measuring.
    /// </summary>
    [Category(Constants.CATData), XmlIgnore, DefaultValue(true), Description("Used from the XCPMaster to mark as available for online measuring.")]
    public bool Active { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
        switch (parameterList[i++])
        {
          case A2LC_DAQ_LIST_TYPE: DAQListType = parse2Enum<XCP_DAQ_LIST_TYPE>(parameterList[i++], parser); break;
          case A2LC_MAX_ODT: MaxODT = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_MAX_ODT_ENTRIES: MaxODTEntries = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_EVENT_FIXED: EventFixed = (UInt16)A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_FIRST_PID: FirstPID = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (PREDEFINED)</summary>
  public sealed partial class A2LXCP_ODT : A2LXCP_NODE
  {
    #region types
    /// <summary>Represents a single ODT entry.</summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class ODT_ENTRY
    {
      public byte OdtEntryNo { get; set; }
      [TypeConverter(typeof(TCHexType))]
      public UInt32 Address { get; set; }
      [TypeConverter(typeof(TCHexType))]
      public byte AddressExtension { get; set; }
      [TypeConverter(typeof(TCHexType))]
      public byte Size { get; set; }
      [TypeConverter(typeof(TCHexType))]
      public byte BitOffset { get; set; }
#if ASAP2_WRITER
      internal ODT_ENTRY() { }
#endif
      public ODT_ENTRY(byte odtEntryNo, UInt32 address, byte addressExtension, byte size, byte bitOffset)
      { OdtEntryNo = odtEntryNo; Address = address; AddressExtension = addressExtension; Size = size; BitOffset = bitOffset; }
    }
    #endregion
    #region constructor
    internal A2LXCP_ODT(int startLine)
    : base(startLine)
    {
    }
    #endregion
    #region properties
    /// <summary>ODT Number</summary>
    [Category(Constants.CATData)]
    public byte ODTNo { get; set; }
    /// <summary>Set of ODT Entries (may be null or empty).</summary>
    [Category(Constants.CATData)]
    public List<ODT_ENTRY> ODTEntries { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      ODTNo = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count - 5)
        switch (parameterList[i++])
        {
          case A2LC_ODT_ENTRY:
            if (ODTEntries == null)
              ODTEntries = new List<ODT_ENTRY>();
            ODTEntries.Add(new ODT_ENTRY((byte)A2LParser.parse2IntVal(parameterList[i++])
              , (UInt32)A2LParser.parse2IntVal(parameterList[i++])
              , (byte)A2LParser.parse2IntVal(parameterList[i++])
              , (byte)A2LParser.parse2IntVal(parameterList[i++])
              , (byte)A2LParser.parse2IntVal(parameterList[i++])
              ));
            break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_EVENT : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public string NameShort { get; set; }
    [Category(Constants.CATData)]
    public UInt16 Id { get; set; }
    [Category(Constants.CATData)]
    public XCP_DAQ_LIST_TYPE DAQListType { get; set; }
    [Category(Constants.CATData)]
    public byte MaxDAQList { get; set; }
    [Category(Constants.CATData)]
    public byte TimeCycle { get; set; }
    [Category(Constants.CATData)]
    public byte TimeUnit { get; set; }
    [Category(Constants.CATData)]
    public byte Priority { get; set; }
    /// <summary>For compatibility reasons not to be considered, if 1.3 Bypassing features are implemented, UInt16.MaxValue if not defined</summary>
    [Category(Constants.CATData), DefaultValue(typeof(UInt16), "65535")]
    public UInt16 COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER { get; set; } = UInt16.MaxValue;
    [Category(Constants.CATData), DefaultValue(XCP_ADDRESS_MODE.NotSet)]
    public XCP_ADDRESS_MODE Consistency { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(false)]
    public bool EVENT_COUNTER_PRESENT { get; set; }
    /// <summary>UInt16.MaxValue if not defined</summary>
    [Category(Constants.CATDataXCPplus), DefaultValue(typeof(UInt16), "65535"), Description(XCPPLusDesc)]
    public UInt16 RELATED_EVENT_CHANNEL_NUMBER { get; set; } = UInt16.MaxValue;
    [Category(Constants.CATDataXCPplus), DefaultValue(false), Description(XCPPLusDesc)]
    public bool RELATED_EVENT_CHANNEL_NUMBER_FIXED { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(false), Description(XCPPLusDesc)]
    public bool DTO_CTR_DAQ_MODE_FIXED { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(DTO_CTR_DAQ_MODE.NotSet), Description(XCPPLusDesc)]
    public DTO_CTR_DAQ_MODE DTO_CTR_DAQ_MODE { get; set; } = DTO_CTR_DAQ_MODE.NotSet;
    [Category(Constants.CATDataXCPplus), DefaultValue(false), Description(XCPPLusDesc)]
    public bool DTO_CTR_STIM_MODE_FIXED { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(DTO_CTR_STIM_MODE.NotSet), Description(XCPPLusDesc)]
    public DTO_CTR_STIM_MODE DTO_CTR_STIM_MODE { get; set; } = DTO_CTR_STIM_MODE.NotSet;
    [Category(Constants.CATDataXCPplus), DefaultValue(false), Description(XCPPLusDesc)]
    public bool STIM_DTO_CTR_COPY_PRESENT { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(0f), Description(XCPPLusDesc)]
    public float CPU_LOAD_MAX { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      NameLong = parameterList[i++];
      NameShort = parameterList[i++];
      Id = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      DAQListType = parse2Enum<XCP_DAQ_LIST_TYPE>(parameterList[i++], parser);
      MaxDAQList = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      TimeCycle = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      TimeUnit = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      Priority = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER: COMPLEMENTARY_BYPASS_EVENT_CHANNEL_NUMBER = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_CONSISTENCY: Consistency = parse2Enum<XCP_ADDRESS_MODE>(parameterList[i++], parser); break;
          case A2LC_EVENT_COUNTER_PRESENT: EVENT_COUNTER_PRESENT = true; break;
          case A2LC_RELATED_EVENT_CHANNEL_NUMBER: RELATED_EVENT_CHANNEL_NUMBER = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue); break;
          case A2LC_RELATED_EVENT_CHANNEL_NUMBER_FIXED: RELATED_EVENT_CHANNEL_NUMBER_FIXED = true; break;

          case A2LC_DTO_CTR_DAQ_MODE: DTO_CTR_DAQ_MODE = parse2Enum<DTO_CTR_DAQ_MODE>(parameterList[i++], parser); break;
          case A2LC_DTO_CTR_DAQ_MODE_FIXED: DTO_CTR_DAQ_MODE_FIXED = true; break;

          case A2LC_DTO_CTR_STIM_MODE: DTO_CTR_STIM_MODE = parse2Enum<DTO_CTR_STIM_MODE>(parameterList[i++], parser); break;
          case A2LC_DTO_CTR_STIM_MODE_FIXED: DTO_CTR_STIM_MODE_FIXED = true; break;

          case A2LC_STIM_DTO_CTR_COPY_PRESENT: STIM_DTO_CTR_COPY_PRESENT = true; break;
          case A2LC_CPU_LOAD_MAX: CPU_LOAD_MAX = A2LParser.parse2SingleVal(parameterList[i++]); break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Event channels).
  /// 
  /// - See XCP Version 1.3, Event channels
  /// - Configuration with 0-0 not allowed.
  /// </summary>
  public sealed partial class A2LXCP_EVENT_MIN_CYCLE_TIME : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_MIN_CYCLE_TIME(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public byte TimeCycle { get; set; }
    [Category(Constants.CATData)]
    public byte TimeUnit { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      var value = A2LParser.parse2IntVal(parameterList[i++]);
      TimeCycle = (byte)Math.Min(value, byte.MaxValue);
      value = A2LParser.parse2IntVal(parameterList[i++]);
      TimeUnit = (byte)Math.Min(value, byte.MaxValue);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, DAQ packed mode, applies for all associated DAQ lists).
  /// </summary>
  public sealed partial class A2LXCP_EVENT_DAQ_PACKED_MODE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_DAQ_PACKED_MODE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>DAQ packed mode, applies for all associated DAQ lists</summary>
    [Category(Constants.CATData)]
    public XCP_GROUP_MODE GroupMode { get; set; }
    /// <summary>Single Timestamp mode</summary>
    [Category(Constants.CATData)]
    public XCP_STS_MODE STSMode { get; set; }
    /// <summary>Usage</summary>
    [Category(Constants.CATData)]
    public XCP_PACK_MODE PackMode { get; set; }
    /// <summary>DAQ packed mode sample count</summary>
    [Category(Constants.CATData)]
    public UInt32 SampleCount { get; set; }
    /// <summary>other valid sample count values (optional)</summary>
    [Category(Constants.CATData)]
    public List<UInt32> AltSampleCounts { get; set; } = new List<uint>();
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      GroupMode = parse2Enum<XCP_GROUP_MODE>(parameterList[i++], parser);
      STSMode = parse2Enum<XCP_STS_MODE>(parameterList[i++].Substring(4), parser);
      PackMode = parse2Enum<XCP_PACK_MODE>(parameterList[i++], parser);
      SampleCount = A2LParser.parse2UIntVal(parameterList[i++]);
      while (i < parameterList.Count)
        switch (parameterList[i++])
        {
          case A2LC_ALT_SAMPLE_COUNT: AltSampleCounts.Add(A2LParser.parse2UIntVal(parameterList[i++])); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption)
  /// </summary>
  public sealed partial class A2LXCP_EVENT_CPU_LOAD_CONSUMPTION : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_CPU_LOAD_CONSUMPTION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public float DAQ_FACTOR { get; set; }
    [Category(Constants.CATData)]
    public float ODT_FACTOR { get; set; }
    [Category(Constants.CATData)]
    public float ODT_ENTRY_FACTOR { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQ_FACTOR = A2LParser.parse2SingleVal(parameterList[i++]);
      ODT_FACTOR = A2LParser.parse2SingleVal(parameterList[i++]);
      ODT_ENTRY_FACTOR = A2LParser.parse2SingleVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load consumption queue)
  /// </summary>
  public sealed partial class A2LXCP_EVENT_CPU_LOAD_CONSUMPTION_QUEUE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_CPU_LOAD_CONSUMPTION_QUEUE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public float ODT_FACTOR { get; set; }
    [Category(Constants.CATData)]
    public float ODT_ELEMENT_LOAD { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      ODT_FACTOR = A2LParser.parse2SingleVal(parameterList[i++]);
      ODT_ELEMENT_LOAD = A2LParser.parse2SingleVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, ODT Entry size factor table)
  /// </summary>
  public sealed partial class A2LXCP_EVENT_ODT_ENTRY_SIZE_FACTOR_TABLE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_ODT_ENTRY_SIZE_FACTOR_TABLE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt32 SIZE { get; set; }
    [Category(Constants.CATData)]
    public float SIZE_FACTOR { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      SIZE = A2LParser.parse2UIntVal(parameterList[i++]);
      SIZE_FACTOR = A2LParser.parse2SingleVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, CPU load max,consumption; max load on single core)
  /// </summary>
  public sealed partial class A2LXCP_CORE_LOAD : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_CORE_LOAD(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Core reference number</summary>
    [Category(Constants.CATData)]
    public UInt32 CORE_NR { get; set; }
    /// <summary>max load or max of this event part</summary>
    [Category(Constants.CATData)]
    public float CORE_LOAD { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      CORE_NR = A2LParser.parse2UIntVal(parameterList[i++]);
      CORE_LOAD = A2LParser.parse2SingleVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_DAQ_EVENT : A2LXCP_NAMEDNODE
  {
    #region constructor
    internal A2LXCP_DAQ_EVENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Events of a fixed event list (may be null or empty).</summary>
    [Category(Constants.CATData), Description("Events of a fixed event list.")]
    public List<UInt16> Events { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      base.onInitFromParameter(parser, ref parameterList, startIndex);
      if (Name.Equals(A2LC_VARIABLE, Settings.Default.StrictParsing ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase))
        return true;

      int i = startIndex + 1;
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_EVENT:
            if (Events == null)
              Events = new List<UInt16>();
            var eventNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            if (!Events.Contains(eventNo))
              Events.Add(eventNo);
            break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_EVENT_LIST : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_LIST(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Events of a fixed event list (may be null or empty).</summary>
    [Category(Constants.CATData), Description("Events of a fixed event list.")]
    public List<UInt16> Events { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_EVENT:
            if (Events == null)
              Events = new List<UInt16>();
            var eventNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            if (!Events.Contains(eventNo))
              Events.Add(eventNo);
            break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_PAG : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_PAG(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public byte MaxSegments { get; set; }
    [Category(Constants.CATData)]
    public PAGProperties Properties { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      MaxSegments = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "FREEZE_SUPPORTED": Properties |= PAGProperties.FreezeSupported; break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_PGM : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_PGM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public XCP_PGM_MODE Mode { get; set; }
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0")]
    public byte MaxSectors { get; set; }
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0")]
    public byte MaxCTO { get; set; }
    [Category(Constants.CATData), Description("Supported communication modes")]
    public XCPCommModes CommModesSupported { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      switch (parameterList[i++])
      {
        case A2LC_PGMMODE_ABS: Mode = XCP_PGM_MODE.ABSOLUTE; break;
        case A2LC_PGMMODE_FUNC: Mode = XCP_PGM_MODE.FUNCTIONAL; break;
        case A2LC_PGMMODE_ABS_FUNC: Mode = XCP_PGM_MODE.ABSOLUTE | XCP_PGM_MODE.FUNCTIONAL; break;
      }
      MaxSectors = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      MaxCTO = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      if (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_COMM_MODE_SUPPORTED: CommModesSupported = getCommModes(this, parser, ref parameterList, ref i); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_SECTOR : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_SECTOR(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public byte Number { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Address { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Length { get; set; }
    [Category(Constants.CATData)]
    public byte ClearSeqNo { get; set; }
    [Category(Constants.CATData)]
    public byte PgmSeqNo { get; set; }
    [Category(Constants.CATData)]
    public byte PgmMethod { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      NameLong = parameterList[i++];
      Number = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      Address = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      Length = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      ClearSeqNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      PgmSeqNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      PgmMethod = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// XCP on CAN parameters.
  /// 
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed partial class A2LXCP_ON_CAN : A2LXCP_MEDIA
  {
    #region constructor
    internal A2LXCP_ON_CAN(int startLine) : base(startLine) { }
    /// <summary>
    /// Constructor to support simulator entries.
    /// </summary>
    /// <param name="src">The source instance</param>
    public A2LXCP_ON_CAN(A2LXCP_ON_CAN src)
    : base(int.MaxValue)
    {
      Version = src.Version;
      CANIDCmd = src.CANIDCmd;
      CANIDResp = src.CANIDResp;
      CANIDBroadcast = src.CANIDBroadcast;
      CANIDGetDAQClockMulticast = src.CANIDGetDAQClockMulticast;
      Baudrate = src.Baudrate;
      SamplePoint = src.SamplePoint;
      SampleRate = src.SampleRate;
      BTLCycles = src.BTLCycles;
      SJW = src.SJW;
      MaxDLCRequired = src.MaxDLCRequired;
      SyncEdge = src.SyncEdge;
      MasterIncremental = src.MasterIncremental;
      TransportLayerInstance = src.TransportLayerInstance;
      MaxBusLoad = src.MaxBusLoad;
      // Reference the childs from the original source
      Childs = src.Childs;
    }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIDResp { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIDCmd { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt32), mStrHexDWordMax)]
    public UInt32 CANIDBroadcast { get; set; } = UInt32.MaxValue;
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt32), mStrHexDWordMax)]
    public UInt32 CANIDGetDAQClockMulticast { get; set; } = UInt32.MaxValue;
    [Category(Constants.CATData)]
    public UInt32 Baudrate { get; set; }
    /// <summary>
    /// Sample point receiver [% complete bit time].
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0"), Description("Sample point receiver [% complete bit time].")]
    public byte SamplePoint { get; set; }
    [Category(Constants.CATData), DefaultValue(XCP_DAQ_LIST_CAN_SAMPLE_RATE.NotSet)]
    public XCP_DAQ_LIST_CAN_SAMPLE_RATE SampleRate { get; set; }
    /// <summary>
    /// BTL_CYCLES [slots per bit time].
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0"), Description("BTL_CYCLES [slots per bit time].")]
    public byte BTLCycles { get; set; }
    /// <summary>
    /// Length synchr. segment [BTL_CYCLES].
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0"), Description("length synchr. segment [BTL_CYCLES].")]
    public byte SJW { get; set; }
    /// <summary>
    /// Master to slave frames always to have DLC = MAX_DLC_for CAN-FD.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false), Description("Master to slave frames always to have DLC = MAX_DLC_for CAN - FD.")]
    public bool MaxDLCRequired { get; set; }
    [Category(Constants.CATData), DefaultValue(XCP_SYNCEDGE.NotSet)]
    public XCP_SYNCEDGE SyncEdge { get; set; }
    /// <summary>
    /// Master uses range of CAN-IDs
    /// 
    /// - Start of range = CAN_ID_MASTER
    /// - End of range = CAN_ID_MASTER + MAX_BS(_PGM)-1
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false)]
    public bool MasterIncremental { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_OPTIONAL_TL_SUBCMD: OptionalTLCmds.Add(parameterList[i++]); break;
          case A2LC_CAN_ID_MASTER: CANIDCmd = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_CAN_ID_SLAVE: CANIDResp = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_CAN_ID_BROADCAST: CANIDBroadcast = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_CAN_ID_GET_DAQ_CLOCK_MULTICAST: CANIDGetDAQClockMulticast = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_BAUDRATE: Baudrate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_SAMPLE_POINT: SamplePoint = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_SAMPLE_RATE: SampleRate = parse2Enum<XCP_DAQ_LIST_CAN_SAMPLE_RATE>(parameterList[i++], parser); break;
          case A2LC_BTL_CYCLES: BTLCycles = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_SJW: SJW = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_MAX_DLC_REQUIRED: MaxDLCRequired = true; break;
          case A2LC_SYNC_EDGE: SyncEdge = parse2Enum<XCP_SYNCEDGE>(parameterList[i++], parser); break;
          case A2LC_CAN_ID_MASTER_INCREMENTAL: MasterIncremental = true; break;
          case A2LC_MAX_BUS_LOAD: MaxBusLoad = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_TRANSPORT_LAYER_INSTANCE: TransportLayerInstance = parameterList[i++]; break;
        }
      }
      return true;
    }
    public override string ToString()
    {
      var canFD = getNode<A2LXCP_CAN_FD>(false);
      var type = StartLine == int.MaxValue ? "Simulate on " : string.Empty;
      var cmdRespPair = CANProvider.toCANIDPairString(CANIDCmd, CANIDResp);
      return canFD != null
        ? $"{type}XCP on CANFD; IDs[{cmdRespPair}]; Baudrates {Baudrate}/{canFD.DataTransferBaudrate} Bit/s"
        : $"{type}XCP on CAN; IDs[{cmdRespPair}]; Baudrate {Baudrate} Bit/s";
    }
    #endregion
  }
  /// <summary>See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_DAQ_LIST_CAN_ID : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_DAQ_LIST_CAN_ID(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt16 DAQNo { get; set; }
    [Category(Constants.CATData), DefaultValue(XCP_DAQ_LIST_CAN_TYPE.NotSet)]
    public XCP_DAQ_LIST_CAN_TYPE DAQListType { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANId { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_VARIABLE: DAQListType = XCP_DAQ_LIST_CAN_TYPE.VARIABLE; break;
          case A2LC_FIXED:
            DAQListType = XCP_DAQ_LIST_CAN_TYPE.FIXED;
            CANId = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version > 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_EVENT_CAN_ID_LIST : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_EVENT_CAN_ID_LIST(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Reference to EVENT_NUMBER.</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 EvtNo { get; set; }
    /// <summary>This event is associated with this CAN IDs (may be null or empty).</summary>
    [Category(Constants.CATData)]
    public List<UInt32> FixedCANIds { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      EvtNo = (UInt16)A2LParser.parse2Int64Val(parameterList[i++]);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_FIXED:
            if (FixedCANIds == null)
              FixedCANIds = new List<UInt32>();
            var canId = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
            if (!FixedCANIds.Contains(canId))
              FixedCANIds.Add(canId);
            break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version > 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LXCP_CAN_FD : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_CAN_FD(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Values of 8, 12, 16, 20, 24, 32, 48 or 64 are valid.
    /// </summary>
    [Category(Constants.CATData)]
    public UInt16 MaxDLC { get; set; }
    /// <summary>
    /// Data transfer BAUDRATE [Hz].
    /// </summary>
    [Category(Constants.CATData), Description("Data transfer BAUDRATE [Hz]")]
    public UInt32 DataTransferBaudrate { get; set; }
    /// <summary>
    /// Sample point receiver [% complete bit time].
    /// </summary>
    [Category(Constants.CATData), Description("Sample point receiver [% complete bit time].")]
    public byte SamplePoint { get; set; }
    /// <summary>
    /// Sender sample point [% complete bit time].
    /// </summary>
    [Category(Constants.CATData), Description("Sender sample point [% complete bit time].")]
    public byte SecondarySamplePoint { get; set; }
    /// <summary>
    /// BTL_CYCLES [slots per bit time].
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0"), Description("BTL_CYCLES [slots per bit time].")]
    public byte BTLCycles { get; set; }
    /// <summary>
    /// Length synchr. segment [BTL_CYCLES].
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), "0"), Description("length synchr. segment [BTL_CYCLES].")]
    public byte SJW { get; set; }
    [Category(Constants.CATData), DefaultValue(XCP_SYNCEDGE.NotSet)]
    public XCP_SYNCEDGE SyncEdge { get; set; }
    [Category(Constants.CATData), DefaultValue(XCP_TRANSCEIVER_DELAY_COMPENSATION.NotSet)]
    public XCP_TRANSCEIVER_DELAY_COMPENSATION TransceiverDelayCompensation { get; set; }
    /// <summary>
    /// Master to slave frames always to have DLC = MAX_DLC for CAN-FD.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(false), Description("Requires Master to slave frames always to have DLC = MAX_DLC for CAN FD.")]
    public bool MaxDLCRequired { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_MAX_DLC: MaxDLC = (UInt16)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_CAN_FD_DATA_TRANSFER_BAUDRATE: DataTransferBaudrate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]); break;
          case A2LC_SAMPLE_POINT: SamplePoint = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_SECONDARY_SAMPLE_POINT: SecondarySamplePoint = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_BTL_CYCLES: BTLCycles = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_SJW: SJW = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue); break;
          case A2LC_SYNC_EDGE: SyncEdge = parse2Enum<XCP_SYNCEDGE>(parameterList[i++], parser); break;
          case A2LC_TRANSCEIVER_DELAY_COMPENSATION: TransceiverDelayCompensation = parse2Enum<XCP_TRANSCEIVER_DELAY_COMPENSATION>(parameterList[i++], parser); break;
          case A2LC_MAX_DLC_REQUIRED: MaxDLCRequired = true; break;
        }
      }
      return true;
    }
    #endregion
  }

  /// <summary>
  /// Base class for any XCP supported media (Ethernet, USB, CAN, FlexRay,...)
  /// </summary>
  public abstract class A2LXCP_MEDIA : A2LXCP_NODE
  {
    #region properties
    /// <summary>
    /// Gets or sets the Version.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Version { get; set; }
    /// <summary>
    /// Transport layer instance is only valid for XCP > v1.1.
    /// </summary>
    [Category(Constants.CATDataXCPplus)]
    public string TransportLayerInstance { get; set; }
    /// <summary>
    /// Maximum available bus load in percent (only valid for XCP > v1.1).
    /// </summary>
    [Category(Constants.CATDataXCPplus), DefaultValue(typeof(UInt32), "0"), Description("Maximum available bus load in percent (only valid for XCP > v1.1).")]
    public UInt32 MaxBusLoad { get; set; }
    /// <summary>
    /// Optional commands for transport layer (GET_SLAVE_ID, GET_DAQ_ID, SET_DAQ_ID, GET_DAQ_CLOCK_MULTICAST).
    /// </summary>
    [Category(Constants.CATDataXCPplus)]
    public OptionalCommandSet OptionalTLCmds { get; set; } = new OptionalCommandSet();
    #endregion
    #region constructor
#if ASAP2_WRITER
    protected A2LXCP_MEDIA() { }
#endif
    protected A2LXCP_MEDIA(int startLine) : base(startLine) { }
    #endregion
  }
  /// <summary>Base class for the XCP ethernet media (UDP/TCP)</summary>
  public abstract partial class A2LXCP_ON_ETHERNET : A2LXCP_MEDIA
  {
    #region constructor
    protected A2LXCP_ON_ETHERNET(int startLine) : base(startLine) { }
    /// <summary>
    /// Constructor to support simulator entries.
    /// </summary>
    /// <param name="src">The source instance</param>
    protected A2LXCP_ON_ETHERNET(A2LXCP_ON_ETHERNET src)
    : base(int.MaxValue)
    { Version = src.Version; Address = "127.0.0.1"; Port = src.Port; }
    #endregion
    #region properties
    /// <summary>
    /// The ethernet port to connect to.
    /// </summary>
    [Category(Constants.CATData), Description("The ethernet port to connect to.")]
    public UInt16 Port { get; set; }
    /// <summary>
    /// The ethernet address to connect to.
    /// 
    /// May be an IP(V4) address or a host name.
    /// </summary>
    [Category(Constants.CATData), Description("The ethernet address to connect to. May be an IP(V4) address or a host name.")]
    public string Address { get; set; }
    /// <summary>
    /// IP(V6) address.
    /// </summary>
    [Category(Constants.CATData), Description("IPV6 address.")]
    public string IPV6 { get; set; }
    /// <summary>
    /// Network speed which is the base for MAX_BUS_LOAD in Mbit (only valid for XCP > v1.1).
    /// </summary>
    [Category(Constants.CATDataXCPplus), DefaultValue(typeof(UInt32), "0"), Description("Network speed which is the base for MAX_BUS_LOAD in Mbit.")]
    public UInt32 MaxBitRate { get; set; }
    [Category(Constants.CATDataXCPplus), DefaultValue(XCP_PACKET_ALIGMENT.NotSet)]
    public XCP_PACKET_ALIGMENT PacketAlignment { get; set; } = XCP_PACKET_ALIGMENT.NotSet;
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Port = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        string tag = parameterList[i++];
        switch (tag)
        {
          case A2LC_OPTIONAL_TL_SUBCMD: OptionalTLCmds.Add(parameterList[i++]); break;
          case A2LC_ADDRESS: Address = parameterList[i++]; break;
          case A2LC_HOST_NAME: Address = parameterList[i++]; break;
          case A2LC_IPV6: IPV6 = parameterList[i++]; break;
          case A2LC_MAX_BUS_LOAD: MaxBusLoad = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          case A2LC_MAX_BIT_RATE: MaxBitRate = (UInt32)A2LParser.parse2Int64Val(parameterList[i++]); break;
          default:
            if (tag.StartsWith("PACKET_ALIGNMENT"))
              PacketAlignment = parse2Enum<XCP_PACKET_ALIGMENT>(tag.Substring(16), parser);
            break;
        }
      }
      return true;
    }
    /// <summary>
    /// Builds a string representation for the XCPEntry.
    /// </summary>
    /// <returns>The instance as a string</returns>
    public override string ToString()
    {
      return string.Format("{3}{0}:{1} over {2}/IP"
      , Address, Port, this is A2LXCP_ON_TCP_IP ? "TCP" : "UDP"
      , StartLine == int.MaxValue ? "Simulate on " : string.Empty
      );
    }
    #endregion
  }
  /// <summary>
  /// XCP on UDP parameters.
  /// 
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed class A2LXCP_ON_UDP_IP : A2LXCP_ON_ETHERNET
  {
    #region constructor
#if ASAP2_WRITER
    internal A2LXCP_ON_UDP_IP() { }
#endif
    internal A2LXCP_ON_UDP_IP(int startLine) : base(startLine) { }
    /// <summary>
    /// Constructor to support simulator entries.
    /// </summary>
    /// <param name="src">The source instance</param>
    public A2LXCP_ON_UDP_IP(A2LXCP_ON_UDP_IP src) : base(src) { }
    #endregion
  }
  /// <summary>
  /// XCP on TCP parameters.
  /// 
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed class A2LXCP_ON_TCP_IP : A2LXCP_ON_ETHERNET
  {
    #region constructor
#if ASAP2_WRITER
    internal A2LXCP_ON_TCP_IP() { }
#endif
    internal A2LXCP_ON_TCP_IP(int startLine) : base(startLine) { }
    /// <summary>
    /// Constructor to support simulator entries.
    /// </summary>
    /// <param name="src">The source instance</param>
    public A2LXCP_ON_TCP_IP(A2LXCP_ON_TCP_IP src) : base(src) { }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed partial class A2LXCP_PROTOCOL_LAYER : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_PROTOCOL_LAYER(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Protocol layer version.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Version { get; set; }
    /// <summary>
    /// Protocol Timing settings.
    /// 
    /// Indizes mapped to: T1=0, T2=1, T3=2, T4=3, T5=4, T6=5, T7=6.
    /// </summary>
    [Category(Constants.CATData)]
    public UInt16[] Timings { get; set; }
    /// <summary>
    /// Indicates the maximum length of a CTO packet in bytes.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description("Indicates the maximum length of a CTO packet in bytes.")]
    public byte MaxCTO { get; set; } = byte.MaxValue;
    /// <summary>
    /// Indicates the maximum length of a DTO packet in bytes (Default for DAQ and STIM).
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description("Indicates the maximum length of a DTO packet in bytes.")]
    public UInt16 MaxDTO { get; set; } = UInt16.MaxValue;
    /// <summary>
    /// Overrules MaxDTO for STIM use case.
    /// </summary>
    [Category(Constants.CATDataXCPplus), TypeConverter(typeof(TCHexType)), Description("Overrules MaxDTO for STIM use case.")]
    public UInt16 MaxDTOStim { get; set; } = UInt16.MaxValue;
    /// <summary>
    /// 
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrder))]
    public BYTEORDER_TYPE ByteOrder
    {
      get
      {
        return (CommModeBasic & CommModeBasic.BigEndian) > 0
        ? BYTEORDER_TYPE.BIG_ENDIAN
        : BYTEORDER_TYPE.LITTLE_ENDIAN;
      }
    }
    [Category(Constants.CATData)]
    public CommModeBasic CommModeBasic { get; set; }
    /// <summary>Allowed optional commands.</summary>
    [XmlIgnore, Category(Constants.CATData), Description("Allowed optional commands"), ReadOnly(true)]
    public OptionalCommandSet OptionalCmds { get; set; } = new OptionalCommandSet();
    [Category(Constants.CATData), Description("Supported communication modes")]
    public XCPCommModes CommModesSupported { get; set; }
    /// <summary>
    /// Name of the Seed&Key function including file extension without path.
    /// </summary>
    [Category(Constants.CATData), Description("Name of the Seed&Key function including file extension without path.")]
    public string SeedAndKeyExternalFunction { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings = new UInt16[7];
      Timings[0] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[1] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[2] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[3] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[4] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[5] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Timings[6] = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      MaxCTO = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      MaxDTO = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_MSB_FIRST: CommModeBasic |= CommModeBasic.BigEndian; break;
          case A2LC_MSB_LAST: CommModeBasic &= ~CommModeBasic.BigEndian; break;
          case A2LC_ADR_GRAN_BYTE: CommModeBasic |= CommModeBasic.AddressGranularityBYTE; break;
          case A2LC_ADR_GRAN_WORD: CommModeBasic |= CommModeBasic.AddressGranularityWORD; break;
          case A2LC_ADR_GRAN_DWORD: CommModeBasic |= CommModeBasic.AddressGranularityDWORD; break;
          case A2LC_OPTIONAL_CMD: OptionalCmds.Add(parameterList[i++]); break;
          case A2LC_SK_EXT_FUNC: SeedAndKeyExternalFunction = parameterList[i++]; break;
          case A2LC_COMM_MODE_SUPPORTED: CommModesSupported = getCommModes(this, parser, ref parameterList, ref i); break;
          case A2LC_MAX_DTO_STIM: MaxDTOStim = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Protocol Layer, ECU States)</summary>
  public sealed partial class A2LXCP_ECU_STATES_STATE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_ECU_STATES_STATE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    public byte StateNumber { get; set; }
    public string StateName { get; set; }
    public XCP_RESOURCE_STATE CAL_PAG { get; set; }
    public XCP_RESOURCE_STATE DAQ { get; set; }
    public XCP_RESOURCE_STATE STIM { get; set; }
    public XCP_RESOURCE_STATE PGM { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      StateNumber = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      StateName = parameterList[i++];
      CAL_PAG = parse2Enum<XCP_RESOURCE_STATE>(parameterList[i++], parser);
      DAQ = parse2Enum<XCP_RESOURCE_STATE>(parameterList[i++], parser);
      STIM = parse2Enum<XCP_RESOURCE_STATE>(parameterList[i++], parser);
      PGM = parse2Enum<XCP_RESOURCE_STATE>(parameterList[i++], parser);
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Protocol Layer, ECU States, State)</summary>
  public sealed partial class A2LXCP_ECU_STATES_MEMORY_ACCESS : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_ECU_STATES_MEMORY_ACCESS(int startLine) : base(startLine) { }
    #endregion
    #region properties
    public XCP_MEMORY_ACCESS ReadAccess { get; set; }
    public XCP_MEMORY_ACCESS WriteAccess { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      ReadAccess = parse2Enum<XCP_MEMORY_ACCESS>(parameterList[i++].Substring(12), parser);
      WriteAccess = parse2Enum<XCP_MEMORY_ACCESS>(parameterList[i++].Substring(13), parser);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed partial class A2LXCP_SEGMENT : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_SEGMENT(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// The Segment number.
    /// </summary>
    [Category(Constants.CATData), Description("The Segment number.")]
    public byte SegmentNo { get; set; }
    /// <summary>
    /// The number of pages.
    /// </summary>
    [Category(Constants.CATData), Description("The number of pages.")]
    public byte NoOfPages { get; set; }
    /// <summary>
    /// The Address extension of the segment.
    /// </summary>
    [Category(Constants.CATData), Description("The Address extension of the segment."), DefaultValue(typeof(byte), "0")]
    public byte AddressExtension { get; set; }
    /// <summary>
    /// The compression method.
    /// 
    /// - 0x00..0x7f Data uncompressed (default)
    /// - 0x80..0xff User defined
    /// </summary>
    [Category(Constants.CATData), Description("The compression method"), DefaultValue(typeof(byte), "0")]
    public byte CompressionMethod { get; set; }
    /// <summary>
    /// The encryption method.
    /// 
    /// - 0x00..0x7f Data unencrypted (default)
    /// - 0x80..0xff User defined
    /// </summary>
    [Category(Constants.CATData), Description("The encryption method"), DefaultValue(typeof(byte), "0")]
    public byte EncryptionMethod { get; set; }
    /// <summary>
    /// Verification value for PGM.
    /// </summary>
    [Category(Constants.CATDataXCPplus), Description("The verification value for PGM"), DefaultValue(typeof(uint), "0")]
    public UInt32 PgmVerify { get; set; }
    /// <summary>
    ///  Number of the default page
    /// </summary>
    [Category(Constants.CATDataXCPplus), Description("Number of the default page"), DefaultValue(typeof(byte), "255")]
    public byte DefaultPageNumber { get; set; } = byte.MaxValue;
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      SegmentNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      NoOfPages = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      AddressExtension = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      CompressionMethod = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      EncryptionMethod = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_PGM_VERIFY: PgmVerify = A2LParser.parse2UIntVal(parameterList[i++]); break;
          case A2LC_DEFAULT_PAGE_NUMBER: DefaultPageNumber = (byte)A2LParser.parse2IntVal(parameterList[i++]); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed partial class A2LXCP_PAGE : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_PAGE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// Page number for this segment.
    /// </summary>
    [Category(Constants.CATData), Description("Page number for this segment.")]
    public byte PageNo { get; set; }
    /// <summary>
    /// ECU Access type.
    /// </summary>
    [Category(Constants.CATData), Description("ECU Access type."), TypeConverter(typeof(EnumConverter))]
    public XCP_ECU_ACCESS AccessECU { get; set; }
    /// <summary>
    /// Read Access type.
    /// </summary>
    [Category(Constants.CATData), Description("Read Access type."), TypeConverter(typeof(EnumConverter))]
    public XCP_READ_WRITE_ACCESS AccessRead { get; set; }
    /// <summary>
    /// Write Access type.
    /// </summary>
    [Category(Constants.CATData), Description("Write Access type."), TypeConverter(typeof(EnumConverter))]
    public XCP_READ_WRITE_ACCESS AccessWrite { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      PageNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      string ecuAccessRaw = parameterList[i++].Substring(11);
      AccessECU = parse2Enum<XCP_ECU_ACCESS>(ecuAccessRaw, parser);
      string readAccessRaw = parameterList[i++].Substring(16);
      AccessRead = parse2Enum<XCP_READ_WRITE_ACCESS>(readAccessRaw, parser);
      string writeAccessRaw = parameterList[i++].Substring(17);
      AccessWrite = parse2Enum<XCP_READ_WRITE_ACCESS>(writeAccessRaw, parser);
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP (COMMON_PARAMETERS)
  /// </summary>
  public sealed partial class A2LXCP_ADDRESS_MAPPING : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_ADDRESS_MAPPING(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 SrcAddress { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 DstAddress { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 Length { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      SrcAddress = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      DstAddress = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      Length = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus)</summary>
  public sealed partial class A2LXCP_TIME_CORRELATION : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_TIME_CORRELATION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>
    /// XCP_SLAVE_CLOCK and ECU_CLOCK need not necessarily be the same clock, i.e. in case of an external XCP Slave, these clocks might differ
    /// </summary>
    [Category(Constants.CATData), Description("")]
    public XCP_TS_RELATION TimestampsRelateTo { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      TimestampsRelateTo = parse2Enum<XCP_TS_RELATION>(parameterList[i++], parser);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_DAQ_TIMESTAMPS_RELATE_TO: TimestampsRelateTo = parse2Enum<XCP_TS_RELATION>(parameterList[i++], parser); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Time correlation)</summary>
  public sealed partial class A2LXCP_CLOCK : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_CLOCK(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public byte[] UUID { get; set; } = new byte[8];
    [Category(Constants.CATData)]
    public XCP_CLOCK_MODE Mode { get; set; }
    [Category(Constants.CATData)]
    public XCP_CLOCK_READABILITY Readability { get; set; }
    [Category(Constants.CATData)]
    public XCP_CLOCK_SYNC_FEATURE Feature { get; set; }
    /// <summary>Clock quality, stratum level</summary>
    [Category(Constants.CATData)]
    public byte Quality { get; set; }
    [Category(Constants.CATData)]
    public UInt64 MaxTimestmampValueBeforeWraparound { get; set; }
    [Category(Constants.CATData)]
    public XCP_CLOCK_EPOCH Epoch { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      for (int j = 0; i < UUID.Length; ++j)
        UUID[j] = Math.Min((byte)A2LParser.parse2Int64Val(parameterList[i++]), byte.MaxValue);
      Mode = parse2Enum<XCP_CLOCK_MODE>(parameterList[i++], parser);
      Readability = parse2Enum<XCP_CLOCK_READABILITY>(parameterList[i++], parser);
      Feature = parse2Enum<XCP_CLOCK_SYNC_FEATURE>(parameterList[i++], parser);
      Quality = Math.Min((byte)A2LParser.parse2Int64Val(parameterList[i++]), byte.MaxValue);
      MaxTimestmampValueBeforeWraparound = A2LParser.parse2UInt64Val(parameterList[i++]);
      Epoch = parse2Enum<XCP_CLOCK_EPOCH>(parameterList[i++], parser);
      return true;
    }
    #endregion
  }
  /// <summary>See XCP Version 1.3, ASAM MCD 2MC AML FOR XCP (XCPPlus, Time correlation, Clock)</summary>
  public sealed partial class A2LXCP_TIMESTAMP_CHARACTERIZATION : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_TIMESTAMP_CHARACTERIZATION(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt32 TimestampTicks { get; set; }
    [Category(Constants.CATData)]
    public XCP_TIMESTAMP_RESOLUTION Resolution { get; set; }
    /// <summary>Native timestamp size</summary>
    [Category(Constants.CATData), Description("Native timestamp size")]
    public XCP_NATIVE_TIMESTAMP_SIZE Size { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      TimestampTicks = Math.Min((UInt32)A2LParser.parse2Int64Val(parameterList[i++]), UInt32.MaxValue);
      Resolution = parse2Enum<XCP_TIMESTAMP_RESOLUTION>(parameterList[i++].Substring(4), parser);
      Size = parse2Enum<XCP_NATIVE_TIMESTAMP_SIZE>(parameterList[i++].Substring(4), parser);

      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          default: break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// XCP on USB parameters.
  /// 
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed partial class A2LXCP_ON_USB : A2LXCP_MEDIA
  {
    #region constructor
    internal A2LXCP_ON_USB(int startLine) : base(startLine) { }
    public A2LXCP_ON_USB(A2LXCP_ON_USB src)
    : base(int.MaxValue)
    {
      Version = src.Version;
      VendorID = src.VendorID;
      ProductID = src.ProductID;
      HeaderLen = src.HeaderLen;
      NumberOfIF = src.NumberOfIF;
      AlternateSettingNo = src.AlternateSettingNo;
      InterfaceDescriptor = src.InterfaceDescriptor;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the Vendor ID.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 VendorID { get; set; }
    /// <summary>
    /// Gets or sets the Product ID.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 ProductID { get; set; }
    /// <summary>
    /// Number of interfaces.
    /// </summary>
    [Category(Constants.CATData)]
    public byte NumberOfIF { get; set; }
    /// <summary>
    /// Header length.
    /// </summary>
    [Category(Constants.CATData)]
    public XCP_HEADER_LEN HeaderLen { get; set; }
    /// <summary>
    /// Alternate setting number.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(typeof(byte), mStrHexByteMax)]
    public byte AlternateSettingNo { get; set; } = byte.MaxValue;
    /// <summary>
    /// Interface string descriptor.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public string InterfaceDescriptor { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue);
      VendorID = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue);
      ProductID = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue);
      NumberOfIF = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      HeaderLen = parse2Enum<XCP_HEADER_LEN>(parameterList[i++].Substring(11), parser);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_OPTIONAL_TL_SUBCMD: OptionalTLCmds.Add(parameterList[i++]); break;
          case A2LC_ALTERNATE_SETTING_NO: AlternateSettingNo = Math.Min((byte)A2LParser.parse2Int64Val(parameterList[i++]), byte.MaxValue); break;
          case A2LC_INTERFACE_STRING_DESCRIPTOR: InterfaceDescriptor = parameterList[i++]; break;
        }
      return true;
    }
    public override string ToString()
    {
      return $"XCP on USB ({(string.IsNullOrEmpty(InterfaceDescriptor) ? "unknown" : InterfaceDescriptor)})\n(currently not supported)";
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed class A2LXCP_OUT_EP_CMD_STIM : A2LXCP_ENDPOINT
  {
#if ASAP2_WRITER
    internal A2LXCP_OUT_EP_CMD_STIM() { }
#endif
    public A2LXCP_OUT_EP_CMD_STIM(int startLine) : base(startLine) { }
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed class A2LXCP_OUT_EP_ONLY_STIM : A2LXCP_ENDPOINT
  {
#if ASAP2_WRITER
    internal A2LXCP_OUT_EP_ONLY_STIM() { }
#endif
    public A2LXCP_OUT_EP_ONLY_STIM(int startLine) : base(startLine) { }
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed class A2LXCP_IN_EP_ONLY_DAQ : A2LXCP_ENDPOINT
  {
#if ASAP2_WRITER
    internal A2LXCP_IN_EP_ONLY_DAQ() { }
#endif
    public A2LXCP_IN_EP_ONLY_DAQ(int startLine) : base(startLine) { }
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed class A2LXCP_IN_EP_ONLY_EVSERV : A2LXCP_ENDPOINT
  {
#if ASAP2_WRITER
    internal A2LXCP_IN_EP_ONLY_EVSERV() { }
#endif
    public A2LXCP_IN_EP_ONLY_EVSERV(int startLine) : base(startLine) { }
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed class A2LXCP_IN_EP_RESERR_DAQ_EVSERV : A2LXCP_ENDPOINT
  {
#if ASAP2_WRITER
    internal A2LXCP_IN_EP_RESERR_DAQ_EVSERV() { }
#endif
    public A2LXCP_IN_EP_RESERR_DAQ_EVSERV(int startLine) : base(startLine) { }
  }
  /// <summary>
  /// Base class for all endpoint classes.
  /// </summary>
  public abstract partial class A2LXCP_ENDPOINT : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_ENDPOINT(int startLine)
    : base(startLine)
    { HostBufferSize = UInt16.MaxValue; }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the endpoint Number.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public byte EPNo { get; set; }
    /// <summary>
    /// Gets or sets the endpoint transfer type.
    /// </summary>
    [Category(Constants.CATData)]
    public XCP_TRANSFER TransferType { get; set; }
    /// <summary>
    /// Gets or sets the maximum packet size.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 MaxPktSize { get; set; }
    /// <summary>
    /// Gets or sets the endpoint polling interval.
    /// </summary>
    [Category(Constants.CATData)]
    public byte EPPollInterval { get; set; }
    /// <summary>
    /// Gets or sets the endpoint transfer type.
    /// </summary>
    [Category(Constants.CATData)]
    public XCP_MESSAGE_PACKING Packing { get; set; }
    /// <summary>
    /// Gets or sets the endpoint transfer type.
    /// </summary>
    [Category(Constants.CATData)]
    public XCP_ALIGNMENT Alignment { get; set; }
    /// <summary>
    /// Gets or sets the recommended host buffer size (optional).
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(UInt16), mStrHexByteMax)]
    public UInt16 HostBufferSize { get; set; } = byte.MaxValue;
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      EPNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      TransferType = parse2Enum<XCP_TRANSFER>(parameterList[i++], parser);
      MaxPktSize = (UInt16)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), UInt16.MaxValue);
      EPPollInterval = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
      Packing = parse2Enum<XCP_MESSAGE_PACKING>(parameterList[i++].Substring(16), parser);
      Alignment = parse2Enum<XCP_ALIGNMENT>(parameterList[i++].Substring(9), parser);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case A2LC_RECOMMENDED_HOST_BUFSIZE: HostBufferSize = Math.Min((UInt16)A2LParser.parse2Int64Val(parameterList[i++]), UInt16.MaxValue); break;
        }
      return true;
    }
    #endregion
  }
  /// <summary>
  /// See XCP Version 1.1, ASAM MCD 2MC AML FOR XCP Part 3 - Transport Layer Specification XCP on USB
  /// </summary>
  public sealed partial class A2LXCP_DAQ_LIST_USB_ENDPOINT : A2LXCP_NODE
  {
    #region constructor
    internal A2LXCP_DAQ_LIST_USB_ENDPOINT(int startLine)
    : base(startLine)
    {
      EPNo = byte.MaxValue;
      EPType = XCP_ENDPOINT.NotSet;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or sets the referenced DAQ Number.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 DAQNo { get; set; }
    /// <summary>
    /// Gets or sets the EndPoint Number.
    /// 
    /// Only valid if EndPoint type is set.
    /// </summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(byte), mStrHexByteMax)]
    public byte EPNo { get; set; } = byte.MaxValue;
    /// <summary>
    /// Gets or sets the EndPoint type.
    /// </summary>
    [Category(Constants.CATData), DefaultValue(XCP_ENDPOINT.NotSet)]
    public XCP_ENDPOINT EPType { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      DAQNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count - 1)
        switch (parameterList[i++])
        {
          case "FIXED_IN":
          case "FIXED_OUT":
            EPType = parse2Enum<XCP_ENDPOINT>(parameterList[i++].Substring(6), parser);
            EPNo = (byte)Math.Min(A2LParser.parse2IntVal(parameterList[i++]), byte.MaxValue);
            break;
        }
      return true;
    }
    #endregion
  }
}
 