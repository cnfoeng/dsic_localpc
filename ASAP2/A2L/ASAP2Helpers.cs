﻿/*!
 * @file    
 * @brief   Contains the ASAP2 parser helper stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Contains the ASAP2 parser helper stuff.
/// </summary>
namespace jnsoft.ASAP2.Helpers
{
  #region type converters
  /// <summary>Typeconverter to edit a reference to A2LCOMPUMETHOD.</summary>
  public sealed class TCConversionRef : TypeConverter
  {
    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var compList = new List<string>(((A2LNODE)context.Instance).getParent<A2LPROJECT>().CompDict.Keys);
      compList.Sort();
      return new StandardValuesCollection(compList);
    }
  }
  /// <summary>Typeconverter to edit a reference to a A2LMEASUREMENT.</summary>
  public sealed class TCMeasurementRef : TypeConverter
  {
    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var measList = new List<string>(((A2LNODE)context.Instance).getParent<A2LPROJECT>().MeasDict.Keys);
      measList.Add(null);
      measList.Sort();
      return new StandardValuesCollection(measList.ToArray());
    }
  }
  /// <summary>Typeconverter to edit a reference to A2LTYPEDEF.</summary>
  public sealed class TCTypedefRef : TypeConverter
  {
    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var typedefList = new List<string>(((A2LNODE)context.Instance).getParent<A2LPROJECT>().TypeDefDict.Keys);
      typedefList.Sort();
      return new StandardValuesCollection(typedefList);
    }
  }
  /// <summary>Typeconverter to edit a reference to A2LTRANSFORMER.</summary>
  public sealed class TCTransformerRef : TypeConverter
  {
    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var transformerList = new List<string>(((A2LNODE)context.Instance).getParent<A2LPROJECT>().TransformerDict.Keys);
      transformerList.Sort();
      return new StandardValuesCollection(transformerList);
    }
  }
  /// <summary>Typeconverter to edit a reference to A2LUNIT.</summary>
  public sealed class TCUnitRef : TypeConverter
  {
    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return false; }
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var standardValues = new Dictionary<string, string>();
      standardValues[string.Empty] = null;

      var node = (A2LNODE)context.Instance;
      var en = node.getParent<A2LPROJECT>().UnitDict.GetEnumerator();
      while (en.MoveNext())
      {
        string unit = en.Current.Value.Display;
        if (string.IsNullOrEmpty(unit))
          continue;
        standardValues[unit] = null;
      }
      var module = node.getParent<A2LMODULE>();
      foreach (var convRef in module.enumChildNodes<A2LCONVERSION_REF>())
      {
        string unit = convRef.Unit;
        if (string.IsNullOrEmpty(unit))
          continue;
        standardValues[unit] = null;

        var characteristic = convRef as A2LCHARACTERISTIC;
        if (characteristic == null)
          continue;
        foreach (var axisDesc in characteristic.enumChildNodes<A2LAXIS_DESCR>())
          standardValues[axisDesc.Unit] = null;
      }
      var allUnits = new List<string>(standardValues.Keys);
      allUnits.Sort();
      return new StandardValuesCollection(allUnits);
    }
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
        return true;
      return base.CanConvertFrom(context, sourceType);
    }
    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      string s = value as string;
      if (string.IsNullOrEmpty(s))
        // reset value to not set
        return null;
      return s;
    }
  }
  /// <summary>Typeconverter to edit a reference to A2L Format string.</summary>
  public sealed class TCFormatRef : TypeConverter
  {
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context) { return true; }
    public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      var standardValues = new Dictionary<string, string>();
      var node = (A2LNODE)context.Instance;
      var en = node.getParent<A2LPROJECT>().CompDict.GetEnumerator();
      while (en.MoveNext())
      {
        string format = en.Current.Value.Format;
        if (string.IsNullOrEmpty(format))
          continue;
        standardValues[format] = null;
      }

      var module = node.getParent<A2LMODULE>();
      foreach (var convRef in module.enumChildNodes<A2LCONVERSION_REF>())
      {
        string format = convRef.Format;
        if (string.IsNullOrEmpty(format))
          continue;
        standardValues[format] = null;
      }
      var allFormats = new List<string>(standardValues.Keys);
      allFormats.Sort();
      return new StandardValuesCollection(allFormats);
    }
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
        return true;
      return base.CanConvertFrom(context, sourceType);
    }
    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
    {
      string s = value as string;
      if (string.IsNullOrEmpty(s))
        // reset value to not set
        return null;

      if (s[0] == '%')
        s = s.Substring(1);
      string[] splits = s.ToLower().Split(A2LCONVERSION_REF.mFormatSplitChars);
      if (splits.Length < 1 || splits.Length > 2)
        throw new FormatException(Resources.strInvalidValueFormat);

      int dummy;
      if (!int.TryParse(splits[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out dummy)
        || dummy < 0
        )
        throw new FormatException(Resources.strInvalidValueFormat);

      if (splits.Length > 1 && !int.TryParse(splits[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out dummy)
        || dummy < 0
        )
        throw new FormatException(Resources.strInvalidValueFormat);

      return '%' + s;
    }
  }
  #endregion

  #region math
  /// <summary>
  /// Rational coefficients of an A2LCOMPU_METHOD.
  /// </summary>
  public struct RationalCoeffs
  {
    /// <summary>see ASAP2 specification</summary>
    public double a;
    /// <summary>see ASAP2 specification</summary>
    public double b;
    /// <summary>see ASAP2 specification</summary>
    public double c;
    /// <summary>see ASAP2 specification</summary>
    public double d;
    /// <summary>see ASAP2 specification</summary>
    public double e;
    /// <summary>see ASAP2 specification</summary>
    public double f;
    /// <summary>
    /// Default coefficients
    /// 
    /// The default coefficients leads to an identity formula,
    /// which means a raw value equals to it's corresponding 
    /// physical value.
    /// </summary>
    public static RationalCoeffs mDefault = new RationalCoeffs(0, 1, 0, 0, 0, 1);
    RationalCoeffs(double _a, double _b, double _c, double _d, double _e, double _f)
    { a = _a; b = _b; c = _c; d = _d; e = _e; f = _f; }
    internal RationalCoeffs(List<string> parameterList, int offset, int parmCount)
    {
      switch (parmCount)
      {
        case 2:
          a = d = e = 0;
          b = A2LParser.parse2DoubleVal(parameterList[offset]);
          c = A2LParser.parse2DoubleVal(parameterList[offset + 1]);
          f = 1;
          break;
        case 6:
          a = A2LParser.parse2DoubleVal(parameterList[offset]);
          b = A2LParser.parse2DoubleVal(parameterList[offset + 1]);
          c = A2LParser.parse2DoubleVal(parameterList[offset + 2]);
          d = A2LParser.parse2DoubleVal(parameterList[offset + 3]);
          e = A2LParser.parse2DoubleVal(parameterList[offset + 4]);
          f = A2LParser.parse2DoubleVal(parameterList[offset + 5]);
          break;
        default: throw new NotSupportedException(parmCount.ToString());
      }
    }
    public override string ToString()
    { return string.Format(Resources.strCoeffsToStringFormat, a, b, c, d, e, f); }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// 
    /// The ASAP2 computation method formula inverts to a solution 
    /// of a quadratic equation, which may have three outcomes:
    /// - Equation is degenerate: If A (see below) is zero, the equation degrates into
    /// solution of a linear equation.
    /// - Equation is non-degenerate with common root: If the two roots of the solution
    /// (x1 and x2) are equal, the formula can be applied.
    /// - Equation is non-degenerate with different roots: If the two roots of the
    /// solution (x1 and x2) are not equal, there is no basis to select the "right"
    /// solution. Fail with an exception in this case.
    /// </summary>
    /// <param name="rawValue">The raw value to transform to physical.</param>
    /// <returns>Physical value.</returns>
    public double toPhysical(double rawValue)
    {
      double result = double.NaN;
      try
      {
        var A = d * rawValue - a;
        var B = e * rawValue - b;
        var C = f * rawValue - c;
        if (A != 0 && !double.IsNaN(A) && !double.IsInfinity(A))
        { // Full solution, A != 0
          var sqrt = Math.Sqrt(B * B - 4 * A * C);
          var denominator = 2 * A;
          var x1 = (-B + sqrt) / denominator;
          var x2 = (-B - sqrt) / denominator;
          if (Math.Round(x1, 10) == Math.Round(x2, 10))
            throw new ApplicationException(Resources.strToPhysicalFailedMsg);
          result = x1;
        }
        else
          // Degenerate case, A == 0
          result = -C / B;
      }
      catch { }
      return result;
    }
    /// <summary>
    /// Computes the raw value from a physical value.
    /// 
    /// In this case simply apply the
    /// ASAP2 computation method formula below.
    /// 
    /// The asap formula: y = (a*x*x+b*x+c)/(d*x*x+e*x+f).
    /// </summary>
    /// <param name="dataType">The raw data type of the value</param>
    /// <param name="physicalValue">Physical value to transform to raw value.</param>
    /// <returns>Raw value.</returns>
    public double toRaw(DATA_TYPE dataType, double physicalValue)
    {
      double x = physicalValue;
      double x2 = x * x;
      double y = (a * x2 + b * x + c) / (d * x2 + e * x + f);
      switch (dataType)
      {
        case DATA_TYPE.FLOAT32_IEEE:
        case DATA_TYPE.FLOAT64_IEEE:
          break;
        default:
          y = Math.Round(y, 0);
          break;
      }
      return y;
    }
  }

  /// <summary>
  /// Tab conversion computations.
  /// </summary>
  public static class TabCoeffs
  {
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    /// <param name="convType">Conversion type (in this case CONVERSION_TYPE.TAB_INTP or CONVERSION_TYPE.TAB_NOINTP)</param>
    /// <param name="compuTab">The tab dictionary</param>
    /// <param name="rawValue">The raw value</param>
    /// <returns>Physical value.</returns>
    public static double toPhysical(CONVERSION_TYPE convType, A2LCOMPU_TAB compuTab, double rawValue)
    {
      SortedList<float, double> tabDict = compuTab.Values;
      switch (convType)
      {
        case CONVERSION_TYPE.TAB_INTP:
          double rawStart = compuTab.Values.Keys[0];
          double physStart = compuTab.Values.Values[0];
          for (int i = 1; i < compuTab.Values.Count; ++i)
          {
            if (rawValue < rawStart)
              // smaller than start
              return physStart;
            double rawEnd = compuTab.Values.Keys[i];
            double physEnd = compuTab.Values.Values[i];
            if (rawValue <= rawEnd)
              // between
              return (rawValue - rawStart) / (rawEnd - rawStart) * (physEnd - physStart) + physStart;
            rawStart = rawEnd;
            physStart = physEnd;
          }
          return physStart;
        case CONVERSION_TYPE.TAB_NOINTP:
          float rawInt = Convert.ToSingle(rawValue);
          double result;
          if (!tabDict.TryGetValue(rawInt, out result))
            return compuTab.DefaultValueNumeric;
          return result;
      }
      return 0;
    }
    /// <summary>
    /// Computes the raw value from a physical value.
    /// </summary>
    /// <param name="convType">Conversion type (in this case CONVERSION_TYPE.TAB_INTP or CONVERSION_TYPE.TAB_NOINTP)</param>
    /// <param name="compuTab">The tab dictionary</param>
    /// <param name="dataType">The raw data type of the value</param>
    /// <param name="physicalValue">The raw value</param>
    /// <returns>Raw value.</returns>
    public static double toRaw(CONVERSION_TYPE convType, A2LCOMPU_TAB compuTab, DATA_TYPE dataType, double physicalValue)
    {
      switch (convType)
      {
        case CONVERSION_TYPE.TAB_INTP:
          double rawStart = compuTab.Values.Keys[0];
          double physStart = compuTab.Values.Values[0];
          for (int i = 1; i < compuTab.Values.Count; ++i)
          {
            if (physicalValue < physStart)
              // smaller than start
              return rawStart;
            double rawEnd = compuTab.Values.Keys[i];
            double physEnd = compuTab.Values.Values[i];
            if (physicalValue <= physEnd)
              // between
              return (physicalValue - physStart) / (physEnd - physStart) * (rawEnd - rawStart) + rawStart;
            rawStart = rawEnd;
            physStart = physEnd;
          }
          return rawStart;
        case CONVERSION_TYPE.TAB_NOINTP:
          try
          {
            return compuTab.Values.Keys[compuTab.Values.IndexOfValue(physicalValue)];
          }
          catch
          {
            return 0;
          }
      }
      return 0;
    }
  }

  /// <summary>
  /// Maps standard math C functions to .NET framework math function strings.
  /// 
  /// Supports the ASAP standard 1.7 and standards prev to 1.7, see ASAP2 specification, too.
  /// 
  /// Provided c functions are:
  /// - sin, asin, sinh, cos, acos, cosh, tan, atan, tanh
  /// - exp, log, log10, sqrt, abs, pow
  /// - ceil, floor
  /// - ASAP2 standrad prev to v1.7: arcsin, arcos, arctan
  /// </summary>
  public static class CToCSharpMath
  {
    static Dictionary<string, string> mConversionDict;
    static CToCSharpMath()
    {
      mConversionDict = new Dictionary<string, string>();
      // ASAP standard 1.7
      mConversionDict.Add("sin", "Math.Sin");
      mConversionDict.Add("cos", "Math.Cos");
      mConversionDict.Add("tan", "Math.Tan");
      mConversionDict.Add("asin", "Math.Asin");
      mConversionDict.Add("acos", "Math.Acos");
      mConversionDict.Add("atan", "Math.Atan");
      mConversionDict.Add("sinh", "Math.Sinh");
      mConversionDict.Add("cosh", "Math.Cosh");
      mConversionDict.Add("tanh", "Math.Tanh");
      mConversionDict.Add("exp", "Math.Exp");
      mConversionDict.Add("log", "Math.Log");
      mConversionDict.Add("log10", "Math.Log10");
      mConversionDict.Add("sqrt", "Math.Sqrt");
      mConversionDict.Add("abs", "Math.Abs");
      mConversionDict.Add("pow", "Math.Pow");
      // non standard
      mConversionDict.Add("min", "Math.Min");
      mConversionDict.Add("max", "Math.Max");
      mConversionDict.Add("ceil", "Math.Ceiling");
      mConversionDict.Add("floor", "Math.Floor");

      // constants
      //mConversionDict.Add("M_E", "Math.E");
      //mConversionDict.Add("M_LOG10E", "Math.Log10(Math.E)");
      //mConversionDict.Add("M_LN2", "Math.Log(2)");
      //mConversionDict.Add("M_LN10", "Math.Log(10)");
      //mConversionDict.Add("M_PI", "Math.PI");
      //mConversionDict.Add("M_PI_2", "(Math.PI/2.0)");
      //mConversionDict.Add("M_PI_4", "(Math.PI/4.0)");
      //mConversionDict.Add("M_1_PI", "(1.0/Math.PI)");
      //mConversionDict.Add("M_2_PI", "(2.0/Math.PI)");
      //mConversionDict.Add("M_2_SQRTPI", "(2.0/Math.Sqrt(Math.PI))");
      //mConversionDict.Add("M_SQRT2", "Math.Sqrt(2.0)");
      //mConversionDict.Add("M_SQRT1_2", "(1.0/Math.Sqrt(2.0))");

      // ASAP2 standard prev to v1.6
      mConversionDict.Add("arcsin", "Math.Asin");
      mConversionDict.Add("arcos", "Math.Acos");
      mConversionDict.Add("arctan", "Math.Atan");
    }
    /// <summary>
    /// Gets the corresponding .NET framework math function strings to the specified math C function.
    /// </summary>
    /// <param name="mathCFun">The standar ANSI C function</param>
    /// <returns>the corresponding .NET Framework function or null or string.Empty if not found</returns>
    public static string getFunction(string mathCFun)
    {
      string result;
      mConversionDict.TryGetValue(mathCFun, out result);
      return result;
    }
  }
  #endregion

  /// <summary>
  /// Implements basic helper stuff.
  /// </summary>
  public static class Extensions
  {
    #region members
    /// <summary>Regex for data files files</summary>
    static Regex mValidDataFileMatcher = new Regex(Resources.strValidDataFileRegEx, RegexOptions.IgnoreCase);
    /// <summary>Regex for Intel hex files</summary>
    static Regex mHexFileMatcher = new Regex(Resources.strValidIntelFileRegEx, RegexOptions.IgnoreCase);
    /// <summary>Regex for Motorola files</summary>
    static Regex mSFileMatcher = new Regex(Resources.strValidMotorolaFileRegEx, RegexOptions.IgnoreCase);
    #endregion
    #region methods
    /// <summary>
    /// Checks the specified Path for a valid data file name extension.
    /// </summary>
    /// <param name="dataFilePath">The path to check</param>
    /// <returns>true if the extension indicates a valid extension.</returns>
    public static bool isDataFileExtension(string dataFilePath)
    {
      return mValidDataFileMatcher.Match(Path.GetExtension(dataFilePath)).Success;
    }
    /// <summary>
    /// Checks the specified Path for a valid data Intel(Hex) file name extension.
    /// </summary>
    /// <param name="dataFilePath">The path to check</param>
    /// <returns>true if the extension indicates a valid extension.</returns>
    public static bool isHexFileExtension(string dataFilePath)
    {
      return mHexFileMatcher.Match(Path.GetExtension(dataFilePath)).Success;
    }
    /// <summary>
    /// Checks the specified Path for a valid data Motorola(S-Record) file name extension.
    /// </summary>
    /// <param name="dataFilePath">The path to check</param>
    /// <returns>true if the extension indicates a valid extension.</returns>
    public static bool isSFileExtension(string dataFilePath)
    {
      return mSFileMatcher.Match(Path.GetExtension(dataFilePath)).Success;
    }
    /// <summary>
    /// Gets a the data file filter suitable for the specified extension.
    /// </summary>
    /// <param name="dataFilePath">The path to get a file filter for</param>
    /// <returns>The file filter or null</returns>
    public static string getDataFileFilter(string dataFilePath)
    {
      string extension = Path.GetExtension(dataFilePath);
      return mHexFileMatcher.Match(extension).Success
        ? Resources.strHexFileFilter
        : mSFileMatcher.Match(extension).Success
          ? Resources.strs19FileFilter
          : Resources.strBinFileFilter;
    }
    /// <summary>
    /// Sorts the specified A2L measurements according it's data type.
    /// 
    /// Sorts the smallest measurements to the front.
    /// </summary>
    /// <param name="x">first measurement</param>
    /// <param name="y">second measurement</param>
    /// <returns>See CompareTo return value</returns>
    public static int sortByDataType(A2LMEASUREMENT x, A2LMEASUREMENT y)
    {
      return x.DataType.CompareTo(y.DataType);
    }
    #endregion
  }

  /// <summary>
  /// Provides methods for building checksums over data.
  /// 
  /// All checksum algorithms required by ASAP2 and XCP are supported.
  /// </summary>
  public static class Checksum
  {
    #region members
    /// <summary>
    /// CRC table for the CRC-16. 
    /// 
    /// The poly is 0x8005 (x^16 + x^15 + x^2 + 1).
    /// </summary>
    static readonly ushort[] mCRC16Table;
    /// <summary>
    /// CRC table for the CRC-32. 
    /// </summary>
    static readonly UInt32[] mCRC32Table;
    #endregion
    #region ctors
    static Checksum()
    {
      // build CRC32 table
      const uint poly32 = 0xedb88320;
      mCRC32Table = new uint[256];
      uint temp32 = 0;
      for (uint i = 0; i < mCRC32Table.Length; ++i)
      {
        temp32 = i;
        for (int j = 8; j > 0; --j)
        {
          if ((temp32 & 1) == 1)
            temp32 = (uint)((temp32 >> 1) ^ poly32);
          else
            temp32 >>= 1;
        }
        mCRC32Table[i] = temp32;
      }
      // build CRC16 table
      const ushort poly16 = 0xA001;
      mCRC16Table = new UInt16[256];
      ushort value;
      ushort temp;
      for (ushort i = 0; i < mCRC16Table.Length; ++i)
      {
        value = 0;
        temp = i;
        for (byte j = 0; j < 8; ++j)
        {
          if (((value ^ temp) & 0x0001) != 0)
            value = (ushort)((value >> 1) ^ poly16);
          else
            value >>= 1;
          temp >>= 1;
        }
        mCRC16Table[i] = value;
      }
    }
    #endregion
    #region methods
    /// <summary>
    /// Builds the CRC32 checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC32 value</returns>
    public static UInt32 CRC32(byte[] data, int fromOffset, int length)
    {
      UInt32 chk = UInt32.MaxValue;
      for (UInt32 i = (UInt32)fromOffset; i < length; ++i)
        chk = mCRC32Table[(chk ^ data[i]) & byte.MaxValue] ^ (chk >> 8);
      return chk ^ UInt32.MaxValue;
    }
    /// <summary>
    /// Builds the CRC16 checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC16 value</returns>
    public static UInt16 CRC16(byte[] data, int fromOffset, int length)
    {
      UInt16 crc = 0;
      for (int i = fromOffset; i < length; ++i)
        crc = (ushort)(mCRC16Table[(crc ^ (ushort)data[i]) & byte.MaxValue] ^ ((crc >> 8) & byte.MaxValue));
      return crc;
    }
    /// <summary>
    /// Builds the CRC16CCITT checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC16CCITT value</returns>
    public static UInt16 CRC16CCITT(byte[] data, int fromOffset, int length)
    {
      UInt16 result = UInt16.MaxValue;
      for (int i = fromOffset; i < length; ++i)
      {
        UInt16 uiTemp = (UInt16)(data[i] << 8);
        for (int n = 0; n < 8; ++n)
        {
          result = (((result ^ uiTemp) & 0x8000) > 0)
            ? (UInt16)((result << 1) ^ 0x1021)
            : result <<= 1;
          uiTemp <<= 1;
        }
      }
      return result;
    }
#if WIN32_OPTIMIZED
    /// <summary>
    /// Builds the CRC32 checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC32 value</returns>
    public static UInt32 CRC32(IntPtr data, int fromOffset, int length)
    {
      UInt32 chk = UInt32.MaxValue;
      for (UInt32 i = (UInt32)fromOffset; i < length; ++i)
        chk = mCRC32Table[(chk ^ Marshal.ReadByte(data, (int)(fromOffset + i))) & byte.MaxValue] ^ (chk >> 8);
      return chk ^ UInt32.MaxValue;
    }
    /// <summary>
    /// Builds the CRC32 checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC32 value</returns>
    public static UInt32 CRC16(IntPtr data, int fromOffset, int length)
    {
      UInt16 crc = 0;
      for (int i = fromOffset; i < length; ++i)
        crc = (ushort)(mCRC16Table[(crc ^ (ushort)Marshal.ReadByte(data, i)) & byte.MaxValue] ^ ((crc >> 8) & byte.MaxValue));
      return crc;
    }
    /// <summary>
    /// Builds the CRC16CCITT checksum over the given data.
    /// </summary>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The CRC16CCITT value</returns>
    public static UInt16 CRC16CCITT(IntPtr data, int fromOffset, int length)
    {
      UInt16 result = UInt16.MaxValue;
      for (int i = fromOffset; i < length; ++i)
      {
        UInt16 uiTemp = (UInt16)(Marshal.ReadByte(data, i) << 8);
        for (int n = 0; n < 8; ++n)
        {
          result = (((result ^ uiTemp) & 0x8000) > 0)
            ? (UInt16)((result << 1) ^ 0x1021)
            : result <<= 1;
          uiTemp <<= 1;
        }
      }
      return result;
    }
    /// <summary>
    /// Builds the checksum over the specified data with the specified checksum type.
    /// </summary>
    /// <param name="type">The type of the checksum to build</param>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The checksum value</returns>
    /// <exception cref="NotImplementedException">Thrown for ChecksumType.XCP_USER_DEFINED</exception>
    public static UInt32 buildChecksum(ChecksumType type, IntPtr data, int fromOffset, int length)
    {
      UInt32 result = 0;
      switch (type)
      {
        case ChecksumType.ADD_11:
          byte localByte = 0;
          for (int n = fromOffset; n < length; ++n)
            localByte += Marshal.ReadByte(data, n);
          result = localByte;
          break;
        case ChecksumType.ADD_12:
          UInt16 localWord = 0;
          for (int n = fromOffset; n < length; ++n)
            localWord += Marshal.ReadByte(data, n);
          result = localWord;
          break;
        case ChecksumType.ADD_14:
          UInt32 localDWord = 0;
          for (int n = fromOffset; n < length; ++n)
            localDWord += Marshal.ReadByte(data, n);
          result = localDWord;
          break;
        case ChecksumType.ADD_22:
          localWord = 0;
          for (int n = fromOffset; n < length; n += 2)
            localWord += (UInt16)Marshal.ReadInt16(data, n);
          result = localWord;
          break;
        case ChecksumType.ADD_24:
          localDWord = 0;
          for (int n = fromOffset; n < length; n += 2)
            localDWord += (UInt16)Marshal.ReadInt16(data, n);
          result = localDWord;
          break;
        case ChecksumType.ADD_44:
          localDWord = 0;
          for (int n = fromOffset; n < length; n += 4)
            localDWord += (UInt32)Marshal.ReadInt32(data, n);
          result = localDWord;
          break;
        case ChecksumType.CRC_8:
          // not implemented
          result = 0;
          break;
        case ChecksumType.CRC_16:
          result = CRC16(data, fromOffset, length);
          break;
        case ChecksumType.CRC_2_16:
          // not implemented
          result = 0;
          break;
        case ChecksumType.CRC_16_CITT:
          result = CRC16CCITT(data, fromOffset, length);
          break;
        case ChecksumType.CRC_32:
          result = CRC32(data, fromOffset, length);
          break;
        case ChecksumType.USER_DEFINED:
          // not implemented
          result = 0;
          break;
        default:
          // unsupported enum
          throw new NotSupportedException(type.ToString());
      }
      return result;
    }
#else
    /// <summary>
    /// Builds the checksum over the specified data with the specified checksum type.
    /// </summary>
    /// <param name="type">The type of the checksum to build</param>
    /// <param name="data">The data to build the checksum over</param>
    /// <param name="fromOffset">The offset into the specified data to build the checksum over</param>
    /// <param name="length">The length of the data to build the checksum over</param>
    /// <returns>The checksum value</returns>
    /// <exception cref="NotImplementedException">Thrown for ChecksumType.XCP_USER_DEFINED</exception>
    public static UInt32 buildChecksum(ChecksumType type, byte[] data, int fromOffset, int length)
    {
      UInt32 result = 0;
      switch (type)
      {
        case ChecksumType.ADD_11:
          byte localByte = 0;
          for (int n = fromOffset; n < length; ++n)
            localByte += data[n];
          result = localByte;
          break;
        case ChecksumType.ADD_12:
          UInt16 localWord = 0;
          for (int n = fromOffset; n < length; ++n)
            localWord += data[n];
          result = localWord;
          break;
        case ChecksumType.ADD_14:
          UInt32 localDWord = 0;
          for (int n = fromOffset; n < length; ++n)
            localDWord += data[n];
          result = localDWord;
          break;
        case ChecksumType.ADD_22:
          localWord = 0;
          for (int n = fromOffset; n < length; n += 2)
            localWord += BitConverter.ToUInt16(data, n);
          result = localWord;
          break;
        case ChecksumType.ADD_24:
          localDWord = 0;
          for (int n = fromOffset; n < length; n += 2)
            localDWord += BitConverter.ToUInt16(data, n);
          result = localDWord;
          break;
        case ChecksumType.ADD_44:
          localDWord = 0;
          for (int n = fromOffset; n < length; n += 4)
            localDWord += BitConverter.ToUInt32(data, n);
          result = localDWord;
          break;
        case ChecksumType.CRC_8:
          // not implemented
          result = 0;
          break;
        case ChecksumType.CRC_16:
          result = CRC16(data, fromOffset, length);
          break;
        case ChecksumType.CRC_2_16:
          // not implemented
          result = 0;
          break;
        case ChecksumType.CRC_16_CITT:
          result = CRC16CCITT(data, fromOffset, length);
          break;
        case ChecksumType.CRC_32:
          result = CRC32(data, fromOffset, length);
          break;
        case ChecksumType.USER_DEFINED:
          // not implemented
          result = 0;
          break;
        default:
          // unsupported enum
          throw new NotSupportedException(type.ToString());
      }
      return result;
    }
#endif
    #endregion
  }

  /// <summary>
  /// Implements the Douglas Peucker algorithm to reduce the number of points in a points list.
  /// </summary>
  public static class DouglasPeucker
  {
    #region methods
    /// <summary>
    /// Reduces the number of points.
    /// </summary>
    /// <param name="points">The points.</param>
    /// <param name="tolerance">The tolerance.</param>
    /// <returns>The resulting points</returns>
    public static List<PointF> reduction(List<PointF> points, double tolerance)
    {
      if (points == null || points.Count < 3)
        return points;

      int firstPoint = 0;
      int lastPoint = points.Count - 1;
      List<int> pointIndexsToKeep = new List<Int32>();

      // Add the first and last index to the keepers
      pointIndexsToKeep.Add(firstPoint);
      pointIndexsToKeep.Add(lastPoint);

      // The first and the last point can not be the same
      while (points[firstPoint].Equals(points[lastPoint]))
      {
        --lastPoint;
        pointIndexsToKeep.Add(lastPoint);
      }

      DouglasPeuckerReduction(points, firstPoint, lastPoint, tolerance, ref pointIndexsToKeep);

      var returnPoints = new List<PointF>();
      pointIndexsToKeep.Sort();
      for (int i = 0; i < pointIndexsToKeep.Count; ++i)
        returnPoints.Add(points[pointIndexsToKeep[i]]);
      return returnPoints;
    }
    /// <summary>
    /// Uses the Douglas Peucker algorithm to reduce the number of points.
    /// 
    /// Douglases the peucker reduction.
    /// </summary>
    /// <param name="points">The points.</param>
    /// <param name="firstPoint">The first point.</param>
    /// <param name="lastPoint">The last point.</param>
    /// <param name="tolerance">The tolerance.</param>
    /// <param name="pointIndizesToKeep">The point indexs to keep.</param>
    static void DouglasPeuckerReduction(List<PointF> points
      , int firstPoint
      , int lastPoint
      , double tolerance
      , ref List<int> pointIndizesToKeep
      )
    {
      double maxDistance = 0;
      int indexFarthest = 0;

      for (int index = firstPoint; index < lastPoint; ++index)
      {
        double distance = PerpendicularDistance(points[firstPoint], points[lastPoint], points[index]);
        if (distance > maxDistance)
        {
          maxDistance = distance;
          indexFarthest = index;
        }
      }

      if (maxDistance > tolerance && indexFarthest != 0)
      { // Add the largest point that exceeds the tolerance
        pointIndizesToKeep.Add(indexFarthest);
        DouglasPeuckerReduction(points, firstPoint, indexFarthest, tolerance, ref pointIndizesToKeep);
        DouglasPeuckerReduction(points, indexFarthest, lastPoint, tolerance, ref pointIndizesToKeep);
      }
    }
    /// <summary>
    /// The distance of a point from a line made from p1 and p2.
    /// </summary>
    /// <param name="p1">The PT1.</param>
    /// <param name="p2">The PT2.</param>
    /// <param name="p">The p.</param>
    /// <returns>The result</returns>
    static double PerpendicularDistance(PointF p1, PointF p2, PointF p)
    {
      double area = Math.Abs(.5 * (p1.X * p2.Y + p2.X * p.Y + p.X * p1.Y - p2.X * p1.Y - p.X * p2.Y - p1.X * p.Y));
      double bottom = Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
      double height = area / bottom * 2;
      return height;
    }
    #endregion
  }

  /// <summary>
  /// Holds a Timestamp together with a MemoryRange to read from an ECU.
  /// </summary>
  public sealed class AdaptiveState
  {
    #region members
    /// <summary>
    /// Holds a list of memory ranges.
    /// 
    /// - This is a result of the characteristics added so far.
    /// - Don't modify this list directly, use the add method!
    /// </summary>
    public MemoryRangeList MemoryRanges = new MemoryRangeList();
    /// <summary>
    /// The timestamp from the last successful call to isUpdateRequired.
    /// </summary>
    public long LastElapsedTicks = DateTime.Now.Ticks;
    /// <summary>
    /// The list of adaptive characteristics.
    /// 
    /// Don't modify this list directly, use the add method!
    /// </summary>
    public readonly List<A2LCHARACTERISTIC> AdaptiveCharList = new List<A2LCHARACTERISTIC>();
    #endregion
    #region methods
    /// <summary>
    /// Adds a characteristic to the list of adaptive characteristics.
    /// 
    /// To reduce the number of ECU queries, the characteristics gets
    /// summarized into memory ranges.
    /// </summary>
    /// <param name="characteristic">The characteristic to add to the adaptive characteristics list</param>
    public void add(A2LCHARACTERISTIC characteristic)
    {
      AdaptiveCharList.Add(characteristic);
      MemoryRanges.add(characteristic.Address, characteristic.getSize());
    }
    /// <summary>
    /// Indicates if an update from the ECU data is required.
    /// 
    /// This method supports only time based refresh units.
    /// </summary>
    /// <param name="unit">The refresh unit</param>
    /// <param name="rate">The refresh rate</param>
    /// <param name="currentTicks">The currently elapsed ticks</param>
    /// <returns>true if an update from the ECU data is required</returns>
    /// <exception cref="NotImplementedException">Thrown if the refresh unit is not implemented.</exception>
    public bool isUpdateRequired(ScalingUnits unit, int rate, long currentTicks)
    {
      double secs = new TimeSpan(currentTicks - LastElapsedTicks).TotalSeconds;
      double uSecs = secs / 1000000.0;
      switch (unit)
      {
        case ScalingUnits.Time_1uSec: return uSecs > 1.0 * rate;
        case ScalingUnits.Time_10uSec: return uSecs > 10.0 * rate;
        case ScalingUnits.Time_100uSec: return uSecs > 100.0 * rate;
        case ScalingUnits.Time_1mSec: return uSecs > 1000.0 * rate;
        case ScalingUnits.Time_10mSec: return uSecs > 10000.0 * rate;
        case ScalingUnits.Time_100mSec: return uSecs > 100000.0 * rate;
        case ScalingUnits.Time_1Sec: return secs > 1.0 * rate;
        case ScalingUnits.Time_10Sec: return secs > 10.0 * rate;
        case ScalingUnits.Time_1Min: return secs > 60.0 * rate;
        case ScalingUnits.Time_1Hour: return secs > 3600.0 * rate;
        case ScalingUnits.Time_1Day: return secs > 86400.0 * rate;
        default:
          throw new NotImplementedException(unit.ToString());
      }
    }
    #endregion
  }

  /// <summary>
  /// A dictionary to map an adaptive characteristics refreh rate to a corresponding AdaptiveState.
  /// </summary>
  public sealed class AdaptiveCharDict : Dictionary<UInt64, AdaptiveState>, ICloneable
  {
    #region ICloneable Members
    public object Clone()
    {
      var cloneDict = new AdaptiveCharDict();
      var en = GetEnumerator();
      while (en.MoveNext())
        cloneDict[en.Current.Key] = en.Current.Value;
      return cloneDict;
    }
    #endregion
  }
}