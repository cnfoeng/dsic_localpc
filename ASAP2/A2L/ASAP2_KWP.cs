﻿/*!
 * @file    
 * @brief   Implements the ASAP2 KWP Interface stuff.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    April 2015
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;

/// <summary>
/// Implements the ASAP2 KWP (IF_DATA) Interface stuff.
/// </summary>
namespace jnsoft.ASAP2.KWP
{
  #region types
  /// <summary>
  /// KWP possible physical layers.
  /// </summary>
  public enum KWP_PHYSICAL_LAYER
  {
    NotSet,
    /// <summary>K-LINE</summary>
    KLINE,
    /// <summary>CAN</summary>
    CAN,
    /// <summary>K-LINE and CAN</summary>
    KLINE_CAN,
    /// <summary>K-LINE and CAN</summary>
    KLINE_AND_CAN,
  }
  /// <summary>
  /// Modes of KWP Stimulation.
  /// </summary>
  public enum KWP_STIMULATION_MODE
  {
    NotSet,
    /// <summary>WuP</summary>
    _WuP,
    /// <summary>5 Baud</summary>
    _5Baud,
  }
  /// <summary>
  /// KWP Version greater v2000
  /// </summary>
  public enum KWP_VERSION
  {
    NotSet,
    VDA_1996,
  }
  /// <summary>
  /// KWP Measurement mode of ECU: Block with local Id or single by address
  /// </summary>
  public enum KWP_MEASUREMENT_MODE
  {
    /// <summary>Address mode</summary>
    ADDRESSMODE,
    /// <summary>Block mode</summary>
    BLOCKMODE,
  }
  /// <summary>
  /// ECU mode for copying Flash into RAM.
  /// </summary>
  public enum KWP_COPY_MODE
  {
    /// <summary>RAM will be initialized by the ECU</summary>
    RAM_InitByECU,
    /// <summary>RAM must be initialized by the Tool</summary>
    RAM_InitByTool,
  }
  /// <summary>
  /// ECU mode for copying RAM into Flash.
  /// </summary>
  public enum KWP_FLASH_MODE
  {
    NOFLASHBACK,
    AUTOFLASHBACK,
    TOOLFLASHBACK,
  }
  /// <summary>
  /// Results of copy routine.
  /// </summary>
  public enum KWP_FLASH_RESULT
  {
    RequestRoutineResults,
    StartRoutine,
    CodedResult,
  }
  /// <summary>
  /// Timing definition smaller v2000
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed class KWP_USDTP_TIMING
  {
    /// <summary>As [ms]</summary>
    public UInt16 AS { get; set; }
    /// <summary>Bs [ms]</summary>
    public UInt16 BS { get; set; }
    /// <summary>Cr [ms]</summary>
    public UInt16 CD { get; set; }
  }
  /// <summary>
  /// Timing definition smaller v2000
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed class KWP_TIME_DEF
  {
    /// <summary>p1MAx in [ms]</summary>
    public UInt16 p1Max { get; set; }
    /// <summary>p2Min in [ms]</summary>
    public UInt16 p2Min { get; set; }
    /// <summary>p2Max in [ms]</summary>
    public UInt16 p2Max { get; set; }
    /// <summary>p3Min in [ms]</summary>
    public UInt16 p3Min { get; set; }
    /// <summary>p3Max in [ms]</summary>
    public UInt16 p3Max { get; set; }
    /// <summary>p4Min in [ms]</summary>
    public UInt16 p4Min { get; set; }
  }
  /// <summary>Type of page switching service</summary>
  public enum KWP_PAGE_SWITCH_MODE
  {
    ESCAPE_CODE,
    LOCAL_ROUTINE,
  }
  /// <summary>Data Access flags</summary>
  [Flags]
  public enum KWP_DATA_ACCESS_FLAGS
  {
    /// <summary>Upload from ECU flash data</summary>
    ReadData = 1,
    /// <summary>Verify access on ECU flash code</summary>
    VerifyCode = ReadData << 1,
    /// <summary>Upload from ECU flash code</summary>
    ReadCode = ReadData << 2,
    /// <summary>Read and write only on active page</summary>
    RWOnlyOnActivePage = ReadData << 3,
  }
  #endregion
  /// <summary>
  /// Base class for all A2L node definitions concerning KWP.
  /// </summary>
  public abstract class A2LKWP_NODE : A2LNODE
  {
    protected const string mV10XXValid = "Only valid if version is smaller v2000";
    protected const string mV20XXValid = "Only valid if version is equal or greater v2000";
    #region constructor
#if ASAP2_WRITER
    protected A2LKWP_NODE() { }
#endif
    internal A2LKWP_NODE(int startLine) : base(startLine) { }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_K_LINE : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_K_LINE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), DefaultValue(KWP_STIMULATION_MODE._WuP)]
    public KWP_STIMULATION_MODE StimulationMode { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 ECUAddress { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 TesterAddress { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      StimulationMode = parse2Enum<KWP_STIMULATION_MODE>('_' + parameterList[i++], parser);
      ECUAddress = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      TesterAddress = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_DIAG_BAUD : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_DIAG_BAUD(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public UInt32 Baudrate { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 DiagMode { get; set; }
    [Category(Constants.CATData)]
    public byte[] BD_PARA { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Baudrate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      DiagMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      var bdParaBytes = new List<byte>();
      List<byte> currBytes = null;
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case "BD_PARA": currBytes = bdParaBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (bdParaBytes.Count > 0)
        BD_PARA = bdParaBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_PAGE_SWITCH : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_PAGE_SWITCH(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>type of page switching service</summary>
    [Category(Constants.CATData)]
    public KWP_PAGE_SWITCH_MODE Mode { get; set; }
    [Category(Constants.CATData)]
    public byte[] ESCAPE_CODE_PARA_SET { get; set; }
    [Category(Constants.CATData)]
    public byte[] ESCAPE_CODE_PARA_GET { get; set; }
    [Category(Constants.CATData)]
    public byte[] PAGE_CODE { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Mode = parse2Enum<KWP_PAGE_SWITCH_MODE>(parameterList[i++], parser);
      var paraGetBytes = new List<byte>();
      var paraSetBytes = new List<byte>();
      var pageCodeBytes = new List<byte>();
      List<byte> currBytes = null;
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case A2LC_ESCAPE_CODE_PARA_GET: currBytes = paraGetBytes; break;
          case A2LC_ESCAPE_CODE_PARA_SET: currBytes = paraSetBytes; break;
          case A2LC_PAGE_CODE: currBytes = pageCodeBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (paraGetBytes.Count > 0)
        ESCAPE_CODE_PARA_GET = paraGetBytes.ToArray();
      if (paraSetBytes.Count > 0)
        ESCAPE_CODE_PARA_SET = paraSetBytes.ToArray();
      if (pageCodeBytes.Count > 0)
        PAGE_CODE = pageCodeBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_ROUTINE_PARA : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_ROUTINE_PARA(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>result of copy routine delivered by</summary>
    [Category(Constants.CATData), DefaultValue(KWP_FLASH_RESULT.RequestRoutineResults)]
    public KWP_FLASH_RESULT Result { get; set; }
    /// <summary>No of local routine for page switching</summary>
    [Category(Constants.CATData)]
    public UInt16 LocalRoutineNo { get; set; }
    [Category(Constants.CATData)]
    public byte[] RNC_RESULT { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Result = parse2Enum<KWP_FLASH_RESULT>(parameterList[i++], parser);
      LocalRoutineNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      var rncResultBytes = new List<byte>();
      var currBytes = new List<byte>();
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case A2LC_RNC_RESULT: currBytes = rncResultBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (rncResultBytes.Count > 0)
        RNC_RESULT = rncResultBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_ADDRESS : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_ADDRESS(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>CAN_ID ECU(or gateway) - Tester</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIdECU { get; set; }
    /// <summary>CAN_ID Tester - ECU(or gateway)</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 CANIdTester { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      CANIdECU = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      CANIdTester = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_TIME_DEF : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_TIME_DEF(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Timing Parameter for KWP greater and equal v2000</summary>
    [Category(Constants.CATData)]
    public KWP_TIME_DEF[] TimeDefs { get; set; }
    /// <summary>USDTP Timing Parameter</summary>
    [Category(Constants.CATData)]
    public KWP_USDTP_TIMING[] USDTPTimings { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      var timeDefs = new List<KWP_TIME_DEF>();
      var usdtpDefs = new List<KWP_USDTP_TIMING>();
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case "KWP_TIMING":
            KWP_TIME_DEF timeDef;
            timeDefs.Add(timeDef = new KWP_TIME_DEF());
            timeDef.p1Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            timeDef.p2Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            timeDef.p2Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            timeDef.p3Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            timeDef.p3Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            timeDef.p4Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case "USDTP_TIMING":
            KWP_USDTP_TIMING usdtpDef;
            usdtpDefs.Add(usdtpDef = new KWP_USDTP_TIMING());
            usdtpDef.AS = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            usdtpDef.BS = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            usdtpDef.CD = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            break;
        }
      }
      if (timeDefs.Count > 0)
        TimeDefs = timeDefs.ToArray();
      if (usdtpDefs.Count > 0)
        USDTPTimings = usdtpDefs.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_COPY : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_COPY(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>ECU mode for copying Flash into RAM</summary>
    [Category(Constants.CATData), DefaultValue(KWP_COPY_MODE.RAM_InitByECU)]
    public KWP_COPY_MODE FLASHToRAMMode { get; set; }
    /// <summary>diagnostic mode for copy RAM to Flash</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(byte), "0x86")]
    public byte FLASHToRAMDiagMode { get; set; }
    /// <summary>list of add. parameters for local routine</summary>
    [Category(Constants.CATData)]
    public byte[] COPY_PARA { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      FLASHToRAMMode = parse2Enum<KWP_COPY_MODE>(parameterList[i++], parser);
      FLASHToRAMDiagMode = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      if (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_COPY_PARA:
            var copyParas = new List<byte>();
            while (i < parameterList.Count)
              copyParas.Add((byte)A2LParser.parse2IntVal(parameterList[i++]));
            if (copyParas.Count > 0)
              COPY_PARA = copyParas.ToArray();
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public partial class A2LKWP_FLASH : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_FLASH(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>ECU mode for copying RAM into Flash</summary>
    [Category(Constants.CATData), DefaultValue(KWP_FLASH_MODE.TOOLFLASHBACK)]
    public KWP_FLASH_MODE RAMToFLASHMode { get; set; }
    [Category(Constants.CATData), DefaultValue(typeof(UInt16), "3")]
    public UInt16 RAMToFLASHRoutineNo { get; set; }
    /// <summary>result of copy routine delivered by</summary>
    [Category(Constants.CATData), DefaultValue(KWP_FLASH_RESULT.RequestRoutineResults)]
    public KWP_FLASH_RESULT Result { get; set; }
    [Category(Constants.CATData)]
    public byte[] COPY_FRAME { get; set; }
    [Category(Constants.CATData)]
    public byte[] RNC_RESULT { get; set; }
    [Category(Constants.CATData)]
    public byte[] COPY_PARA { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      RAMToFLASHMode = parse2Enum<KWP_FLASH_MODE>(parameterList[i++], parser);
      RAMToFLASHRoutineNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Result = parse2Enum<KWP_FLASH_RESULT>(parameterList[i++], parser);
      var copyframeBytes = new List<byte>();
      var rncResultBytes = new List<byte>();
      var copyParaBytes = new List<byte>();
      List<byte> currBytes = null;
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case A2LC_COPY_FRAME: currBytes = copyframeBytes; break;
          case A2LC_RNC_RESULT: currBytes = rncResultBytes; break;
          case A2LC_COPY_PARA: currBytes = copyParaBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (copyframeBytes.Count > 0)
        COPY_FRAME = copyframeBytes.ToArray();
      if (rncResultBytes.Count > 0)
        RNC_RESULT = rncResultBytes.ToArray();
      if (copyParaBytes.Count > 0)
        COPY_PARA = copyParaBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_FLASH_COPY : A2LKWP_FLASH
  {
    #region constructor
    internal A2LKWP_FLASH_COPY(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>ECU mode for copying Flash into RAM</summary>
    [Category(Constants.CATData), DefaultValue(KWP_COPY_MODE.RAM_InitByECU)]
    public KWP_COPY_MODE FLASHToRAMMode { get; set; }
    /// <summary>diagnostic mode for copy RAM to Flash</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), DefaultValue(typeof(byte), "0x86")]
    public byte FLASHToRAMDiagMode { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      RAMToFLASHMode = parse2Enum<KWP_FLASH_MODE>(parameterList[i++], parser);
      RAMToFLASHRoutineNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Result = parse2Enum<KWP_FLASH_RESULT>(parameterList[i++], parser);
      FLASHToRAMMode = parse2Enum<KWP_COPY_MODE>(parameterList[i++], parser);
      FLASHToRAMDiagMode = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      var copyframeBytes = new List<byte>();
      var rncResultBytes = new List<byte>();
      var copyParaBytes = new List<byte>();
      List<byte> currBytes = null;
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case A2LC_COPY_FRAME: currBytes = copyframeBytes; break;
          case A2LC_RNC_RESULT: currBytes = rncResultBytes; break;
          case A2LC_COPY_PARA: currBytes = copyParaBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (copyframeBytes.Count > 0)
        COPY_FRAME = copyframeBytes.ToArray();
      if (rncResultBytes.Count > 0)
        RNC_RESULT = rncResultBytes.ToArray();
      if (copyParaBytes.Count > 0)
        COPY_PARA = copyParaBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_CHECKSUM : A2LKWP_NODE
  {
    #region constructor
    internal A2LKWP_CHECKSUM(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>check sum type qualifier (see table)</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt32 ChkSumType { get; set; }
    /// <summary>check sum calculation only on active page</summary>
    [Category(Constants.CATData)]
    public bool OnlyOnActivePage { get; set; }
    /// <summary>No of local routine for check sum</summary>
    [Category(Constants.CATData)]
    public UInt16 LocalRoutineNo { get; set; }
    /// <summary>result of checksum routine delivered by</summary>
    [Category(Constants.CATData), DefaultValue(KWP_FLASH_RESULT.RequestRoutineResults)]
    public KWP_FLASH_RESULT Result { get; set; }
    [Category(Constants.CATData)]
    public byte[] RNC_RESULT { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      ChkSumType = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      OnlyOnActivePage = parameterList[i++] == "1";
      LocalRoutineNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      Result = parse2Enum<KWP_FLASH_RESULT>(parameterList[i++], parser);

      var rncResultBytes = new List<byte>();
      List<byte> currBytes = null;
      string currStr;
      while (i < parameterList.Count)
      {
        switch ((currStr = parameterList[i++]))
        {
          case A2LC_RNC_RESULT: currBytes = rncResultBytes; break;
          default:
            if (currBytes != null)
              currBytes.Add((byte)A2LParser.parse2IntVal(currStr));
            break;
        }
      }
      if (rncResultBytes.Count > 0)
        RNC_RESULT = rncResultBytes.ToArray();
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_CAN : A2LKWP_NODE
  {
    #region types
    /// <summary>
    /// KWP QP_BLOB definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class NETWORK_LIMITS
    {
      /// <summary>(max. waitframes)</summary>
      public byte WFT_MAX { get; set; }
      /// <summary> (max. datalength)</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt16 XDL_MAX { get; set; }
    }
    #endregion
    #region constructor
    internal A2LKWP_CAN(int startLine) : base(startLine) { }
    #endregion
    #region properties
    /// <summary>Baudrate [Hz]</summary>
    [Category(Constants.CATData)]
    public UInt32 Baudrate { get; set; }
    /// <summary>Sample point [% complete bit time]</summary>
    [Category(Constants.CATData)]
    public byte SamplePoints { get; set; }
    /// <summary>1 or 3 samples per bit</summary>
    [Category(Constants.CATData)]
    public byte SampleCount { get; set; }
    /// <summary>BTL_CYCLES [slots/bit time]</summary>
    [Category(Constants.CATData)]
    public byte BTLCycles { get; set; }
    /// <summary>SJW length synchr. segm. in BTL_CYCLES</summary>
    [Category(Constants.CATData)]
    public byte SJWLength { get; set; }
    /// <summary>SYNC_EDGE</summary>
    [Category(Constants.CATData)]
    public byte SyncEdge { get; set; }
    [Category(Constants.CATData)]
    public NETWORK_LIMITS NetworkLimits { get; set; }
    /// <summary>Start stop FREE_RUNNING routine number</summary>
    [Category(Constants.CATData)]
    public UInt16 StartStopRoutineNo { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Baudrate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
      SamplePoints = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      SampleCount = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      BTLCycles = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      SJWLength = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      SyncEdge = (byte)A2LParser.parse2IntVal(parameterList[i++]);
      while (i < parameterList.Count)
      {
        switch (parameterList[i++])
        {
          case A2LC_NETWORK_LIMITS:
            NetworkLimits = new NETWORK_LIMITS();
            NetworkLimits.WFT_MAX = (byte)A2LParser.parse2IntVal(parameterList[i++]);
            NetworkLimits.XDL_MAX = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            break;
          case A2LC_START_STOP:
            StartStopRoutineNo = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            break;
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_SOURCE : A2LKWP_NODE
  {
    #region types
    /// <summary>
    /// KWP QP_BLOB definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class QP_BLOB
    {
      public KWP_PHYSICAL_LAYER PhysicalLayer { get; set; }
      /// <summary>No of sampling raster</summary>
      public UInt16 NoOfSamplings { get; set; }
      /// <summary></summary>
      public KWP_MEASUREMENT_MODE MeasuremntMode { get; set; }
      /// <summary>Block mode: local identifier</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt16 BlockModeID { get; set; }
      /// <summary>max. sampling rate (+ p3min)</summary>
      public UInt16 MaxSamplingRate { get; set; }
      /// <summary>max. no of signals</summary>
      public UInt16 MaxNoOfSignals { get; set; }
      /// <summary>max. no of signals</summary>
      public UInt16 MaxNoOfBytes { get; set; }
      /// <summary>CAN-Id for EUDTP frames for free running mode</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 CANId { get; set; }
      /// <summary>discriminator if CAN-id shared for free running mode</summary>
      public UInt16 Discriminator { get; set; }
    }
    #endregion
    #region constructor
    internal A2LKWP_SOURCE(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public string NameLong { get; set; }
    [Category(Constants.CATData)]
    public Int16 CSEUnit { get; set; }
    [Category(Constants.CATData)]
    public Int32 Rate { get; set; }
    [Category(Constants.CATData)]
    public QP_BLOB QPBlob { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;

      NameLong = parameterList[i++];
      CSEUnit = (Int16)A2LParser.parse2IntVal(parameterList[i++]);
      Rate = A2LParser.parse2IntVal(parameterList[i++]);
      if (i < parameterList.Count)
      {
        string currStr = parameterList[i++];
        if (A2LC_QP_BLOB == currStr)
        {
          QPBlob = new QP_BLOB();
          currStr = parameterList[i++];
          string temp = currStr.Replace('+', '_');
          QPBlob.PhysicalLayer = parse2Enum<KWP_PHYSICAL_LAYER>(temp, parser);
          if (QPBlob.PhysicalLayer > KWP_PHYSICAL_LAYER.NotSet)
          {
            QPBlob.MeasuremntMode = parse2Enum<KWP_MEASUREMENT_MODE>(parameterList[i++], parser);
            QPBlob.BlockModeID = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.MaxNoOfSignals = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.MaxNoOfBytes = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.CANId = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.Discriminator = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
          }
          else
          {
            QPBlob.NoOfSamplings = (UInt16)A2LParser.parse2IntVal(currStr);
            QPBlob.MeasuremntMode = parse2Enum<KWP_MEASUREMENT_MODE>(parameterList[i++], parser);
            QPBlob.BlockModeID = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.MaxSamplingRate = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
            QPBlob.MaxNoOfSignals = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
          }
        }
      }
      return true;
    }
    #endregion
  }
  /// <summary>See ASAP1B_KWP (COMMON_PARAMETERS)</summary>
  public sealed partial class A2LKWP_TP_BLOB : A2LKWP_NODE
  {
    #region types
    /// <summary>
    /// KWP SERAM definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class SECURITY_ACCESS
    {
      /// <summary>Access mode for KWP2000 service 27h</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt16 AccessMode { get; set; }
      /// <summary>method of seed&key calculation</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt16 CalcMode { get; set; }
      /// <summary>delaytime in sec from stimulation to seed request</summary>
      public UInt16 Delaytime { get; set; }
    }
    /// <summary>
    /// KWP Baudrate definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class BAUD_DEF
    {
      /// <summary>Baud rate</summary>
      public UInt32 BaudRate { get; set; }
      /// <summary>Diag mode</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt16 DiagMode { get; set; }
      /// <summary>i,k for ECU baudrate</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 ik { get; set; }
    }
    /// <summary>
    /// KWP SERAM definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class KWP_SERAM
    {
      /// <summary>RAM begin</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 A { get; set; }
      /// <summary>RAM begin</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 U { get; set; }
      /// <summary>RAM end</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 O { get; set; }
      /// <summary>RAM end</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 E { get; set; }
      /// <summary>ECU address type qualifier for Flash page</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 AdrFLASH { get; set; }
      /// <summary>ECU address type qualifier for Flash page</summary>
      [TypeConverter(typeof(TCHexType))]
      public UInt32 AdrRAM { get; set; }
      /// <summary>Data access flags</summary>
      public KWP_DATA_ACCESS_FLAGS Flags { get; set; }
    }
    /// <summary>
    /// KWP Baudrate definition smaller v2000
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class KWP_DATA_ACCESS
    {
      /// <summary>ECU address type qualifier for Flash page </summary>
      public UInt32 AdrFLASH { get; set; }
      /// <summary>ECU address type qualifier for RAM page </summary>
      public UInt32 AdrRAM { get; set; }
      /// <summary>Data access flags</summary>
      public KWP_DATA_ACCESS_FLAGS Flags { get; set; }
    }
    #endregion
    #region members
    KWP_VERSION mEVersion;
    #endregion
    #region constructor
    internal A2LKWP_TP_BLOB(int startLine) : base(startLine) { }
    #endregion
    #region properties
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType))]
    public UInt16 Version { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description(mV10XXValid)]
    public UInt16 ECUAddress { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description(mV10XXValid)]
    public UInt16 TesterAddress { get; set; }
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrder))]
    public BYTEORDER_TYPE ByteOrder { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), Description(mV10XXValid), DefaultValue(KWP_STIMULATION_MODE._WuP)]
    public KWP_STIMULATION_MODE StimulationMode { get; set; }
    /// <summary>Only valid if version is v2000 or greater</summary>
    [Category(Constants.CATData), Description(mV20XXValid)]
    public KWP_VERSION EVersion { get { return mEVersion; } set { mEVersion = value; } }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), Description(mV10XXValid)]
    public bool StartDiagWithoutBdSwitch { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description(mV10XXValid)]
    public UInt32 ProjectBaseAdress { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), Description(mV10XXValid)]
    public KWP_SERAM SERAM { get; set; }
    /// <summary>Only valid if version is greater v2000</summary>
    [Category(Constants.CATData), Description(mV20XXValid)]
    public KWP_DATA_ACCESS DATA_ACCESS { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), Description(mV10XXValid)]
    public BAUD_DEF[] BAUD_DEFs { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData), Description(mV10XXValid)]
    public KWP_TIME_DEF[] TIME_DEFs { get; set; }
    /// <summary>Only valid if version is smaller v2000</summary>
    [Category(Constants.CATData)]
    public SECURITY_ACCESS[] SECURITY_ACCESSs { get; set; }
    #endregion
    #region methods
    internal override bool onInitFromParameter(A2LParser parser, ref List<string> parameterList, int startIndex)
    {
      int i = startIndex;
      Version = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
      string secondPar = parameterList[i++];
      var saDefs = new List<SECURITY_ACCESS>();
      if (Enum.TryParse(secondPar, out mEVersion))
      { // greater v2000
        ByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser);
        while (i < parameterList.Count)
        {
          switch (parameterList[i++])
          {
            case A2LC_SECURITY_ACCESS:
              SECURITY_ACCESS saDef;
              saDefs.Add(saDef = new SECURITY_ACCESS());
              saDef.AccessMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              saDef.CalcMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              saDef.Delaytime = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              break;
            case A2LC_DATA_ACCESS:
              DATA_ACCESS = new KWP_DATA_ACCESS();
              DATA_ACCESS.AdrFLASH = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              DATA_ACCESS.AdrRAM = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              if (parameterList[i++] == "1")
                DATA_ACCESS.Flags |= KWP_DATA_ACCESS_FLAGS.ReadData;
              if (parameterList[i++] == "1")
                DATA_ACCESS.Flags |= KWP_DATA_ACCESS_FLAGS.VerifyCode;
              if (parameterList[i++] == "1")
                DATA_ACCESS.Flags |= KWP_DATA_ACCESS_FLAGS.ReadCode;
              if (parameterList[i++] == "1")
                DATA_ACCESS.Flags |= KWP_DATA_ACCESS_FLAGS.RWOnlyOnActivePage;
              break;
          }
        }
      }
      else
      { // smaller v2000
        ECUAddress = (UInt16)A2LParser.parse2IntVal(secondPar);
        TesterAddress = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
        StimulationMode = KWP_STIMULATION_MODE._WuP; ++i;
        ByteOrder = parse2Enum<BYTEORDER_TYPE>(parameterList[i++], parser);
        StartDiagWithoutBdSwitch = parameterList[i++] == "1";
        ProjectBaseAdress = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);

        var baudDefs = new List<BAUD_DEF>();
        var timeDefs = new List<KWP_TIME_DEF>();
        while (i < parameterList.Count)
        {
          switch (parameterList[i++])
          {
            case A2LC_DATA_SERAM:
              SERAM = new KWP_SERAM();
              SERAM.A = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              SERAM.O = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              SERAM.U = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              SERAM.E = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              SERAM.AdrFLASH = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              SERAM.AdrRAM = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              if (parameterList[i++] == "1") SERAM.Flags |= KWP_DATA_ACCESS_FLAGS.ReadData;
              if (parameterList[i++] == "1") SERAM.Flags |= KWP_DATA_ACCESS_FLAGS.VerifyCode;
              if (parameterList[i++] == "1") SERAM.Flags |= KWP_DATA_ACCESS_FLAGS.ReadCode;
              if (parameterList[i++] == "1") SERAM.Flags |= KWP_DATA_ACCESS_FLAGS.RWOnlyOnActivePage;
              break;
            case A2LC_BAUD_DEF:
              BAUD_DEF baudDef;
              baudDefs.Add(baudDef = new BAUD_DEF());
              baudDef.BaudRate = (UInt32)A2LParser.parse2IntVal(parameterList[i++]);
              baudDef.DiagMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              baudDef.ik = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              break;
            case A2LC_TIME_DEF:
              KWP_TIME_DEF timeDef;
              timeDefs.Add(timeDef = new KWP_TIME_DEF());
              timeDef.p1Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              timeDef.p2Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              timeDef.p2Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              timeDef.p3Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              timeDef.p3Max = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              timeDef.p4Min = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              break;
            case A2LC_SECURITY_ACCESS:
              SECURITY_ACCESS saDef;
              saDefs.Add(saDef = new SECURITY_ACCESS());
              saDef.AccessMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              saDef.CalcMode = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              saDef.Delaytime = (UInt16)A2LParser.parse2IntVal(parameterList[i++]);
              break;
          }
        }
        if (baudDefs.Count > 0)
          BAUD_DEFs = baudDefs.ToArray();
        if (timeDefs.Count > 0)
          TIME_DEFs = timeDefs.ToArray();
      }
      if (saDefs.Count > 0)
        SECURITY_ACCESSs = saDefs.ToArray();
      return true;
    }
    /// <summary>
    /// Returns the string representation.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return string.Format("{0}KWP, Version: {1:X4}"
        , StartLine == -1 ? "Simulate on " : string.Empty
        , Version
        );
    }
    #endregion
  }
}