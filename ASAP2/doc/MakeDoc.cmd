::
:: Creates the ASAP2 parser documentation
:: Prerequisites:
:: - An installed doxygen version see http://www.stack.nl/~dimitri/doxygen/
::   The path to doxygen.exe must be in your system's search path
:: - An installed HTML Help workshop see http://msdn.microsoft.com/en-us/library/windows/desktop/ms669985%28v=vs.85%29.aspx
::   The path to hhc.exe must be in your system's search path
::
Doxygen Doxyfile
cd ./html
hhc index.hhp
cd ..
del html /Q


