﻿/*!
 * @file    
 * @brief   Defines the MDF interface.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    June 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

/// <summary>
/// All the MDF (Measurement Data Format) stuff.
/// 
/// - This module supports any MDF version up to V4.XX.
/// - An additional goal of the MDF module is to have a common  interface to read/write MDF files for any MDF Version (V1.XX..V3.XX and V4.XX)./// 
/// - Use MDFReader to read MDF files
/// - Use MDFWriter to write MDF files
/// </summary>
namespace jnsoft.MDF
{
  /// <summary>
  /// Specifies the MDF type.
  /// 
  /// As the format is radically changed between MDF Versions V3 and V4
  /// </summary>
  public enum MDFType
  {
    /// <summary>MDF Version V3 (all MDF versions < V4.00)</summary>
    [Description("V3.30")]
    V3,
    /// <summary>MDF Version >= V4.00</summary>
    [Description("V4.10")]
    V4,
  }

  /// <summary>
  /// Specifies possible MDF file extensions.
  /// </summary>
  public enum MDFFileExtension
  {
    /// <summary>V3, V4</summary>
    dat,
    /// <summary>V3, V4</summary>
    mdf,
    /// <summary>V3</summary>
    mf3,
    /// <summary>V4</summary>
    mf4,
  }

  /// <summary>Channel types</summary>
  public enum ChannelType
  {
    /// <summary>
    /// Fixed length data channel. Channel value is contained in record.
    /// </summary>
    Data,
    /// <summary>
    /// Also denoted as "variable length signal data" (VLSD) channel. Channel value in the record 
    /// is an unsigned integer value (LE/Intel Byte order) of the specified number of bits. 
    /// This value is the offset to a variable length signal value in the data section of the SDBLOCK 
    /// referenced by cn_data. Channel data type and CC rule refer to data in SDBLOCK, not to record data. 
    /// Signal data must point to an SDBLOCK or a DLBLOCK with list of SDBLOCKs. For unsorted data groups, 
    /// alternatively it can point to a VLSD CGBLOCK, 
    /// see explanation in 5.14.3 Variable Length Signal Data (VLSD) CGBLOCKV4.
    /// </summary>
    VariableData,
    /// <summary>
    /// Master channel for all signals of this group. Must be combined with one of the following 
    /// sync types: 1,2,3 (see table line for CNBLOCKV4.SyncType).
    /// In each channel group, not more than one channel can be defined as master or virtual master channel 
    /// for each possible sync type. Value decoding is equal to a fixed length data channel. Physical values 
    /// of this channel must return the SI unit (see [SI]) of the respective sync type, with or without CCBLOCK, 
    /// i.e. seconds for a time master channel. Values of this channel listed over the record index must be 
    /// strictly monotonic increasing. They are always relative to the respective start value in the HDBLOCK.
    /// </summary>
    Master,
    /// <summary>
    /// Like a master channel, except that the channel value is not contained in the record.
    /// 
    /// Instead the physical value must be calculated by feeding the zero-based record index 
    /// to the conversion rule. The data type of the virtual master channel must be unsigned 
    /// integer with Little Endian byte order.
    /// </summary>
    VirtualMaster,
    /// <summary>
    /// Must be combined with one of the following sync types: 1,2,3,4 (see table line for CNBLOCKV4.SyncType). 
    /// A synchronization channel is used to synchronize the records of this channel group with samples in 
    /// some other stream, e.g. AVI frames. Physical values of this channel must return the unit of the 
    /// respective synchronization domain, i.e. seconds for CNBLOCKV4.SyncType = 1, or an index value for CNBLOCKV4.SyncType = 4.
    /// These values are used for synchronization with the stream. Hence, the stream must use the same 
    /// synchronization domain, i.e. a data row with synchronization values or index-based samples (only for CNBLOCKV4.SyncType = 4). 
    /// The signal data link (cn_data) must refer to an ATBLOCK in the global list of attachments. The ATBLOCK contains the 
    /// stream data or a reference to a stream file.
    /// </summary>
    Sync,
    /// <summary>
    /// Also denoted as "maximum length signal data" (MLSD) channel Record contains range with maximum number of Bytes 
    /// for channel value like for a fixed length data channel (cn_type = 0), but the number of currently valid Bytes 
    /// is given by the value of a channel referenced by cn_data (denoted as "size signal"). Since the size signal defines 
    /// the number of Bytes to use, its physical values must be Integer values between 0 and (cn_bit_count>>3), i.e. its 
    /// values must not exceed the number of Bytes reserved in the record for this channel. Usually, the size signal should 
    /// have some Integer data type (cn_data_type smaller than 4) without conversion rule and without unit. The value of 
    /// the size signal must be contained in the same record as the current channel, i.e. both channels must be in the same 
    /// channel group. Note: this channel type must not be used with numeric or CANopen data types, i.e. it can occur only for 
    /// (cn_data_type between 6 and 12). valid since MDF 4.1.0, should not occur for earlier versions.
    /// </summary>
    MaxLenData,
    /// <summary>
    /// Similar to a virtual master channel the channel value is not contained in the record (cn_bit_count must be zero). 
    /// Instead the physical value must be calculated by feeding the zero-based record index to the conversion rule. 
    /// The data type of the virtual master channel must be unsigned integer with Little Endian byte order (cn_data_type = 0). 
    /// Except of this, the same rules apply as for a fixed length data channel (cn_type = 0). A virtual data channel may be 
    /// used to specify a channel with constant values without consuming any space in the record. The constant value can be given 
    /// by the offset of a linear conversion rule with factor equal to zero. valid since MDF 4.1.0, should not occur 
    /// for earlier versions.
    /// </summary>
    VirtualData,
  }

  /// <summary>Synchronisation type</summary>
  public enum SyncType
  {
    /// <summary>None (to be used for normal data channels)</summary>
    None = 0,
    /// <summary>Time (physical values must be seconds)</summary>
    Time = 1,
    /// <summary>Angle (physical values must be radians)</summary>
    Angle = 2,
    /// <summary>Distance (physical values must be meters)</summary>
    Distance = 3,
    /// <summary>Index (physical values must be zero-based index values)</summary>
    Index = 4,
  }

  /// <summary>Conversion types</summary>
  public enum ConversionType
  {
    /// <summary>parametric, linear</summary>
    ParametricLinear = 0,
    /// <summary>tabular with interpolatio</summary>
    TabInt = 1,
    /// <summary>tabular</summary>
    Tab = 2,
    /// <summary>polynomial function, V3 only</summary>
    Polynomial = 6,
    /// <summary>exponential function, V3 only</summary>
    Exponential = 7,
    /// <summary>logarithmic function, V3 only</summary>
    Logarithmic = 8,
    /// <summary>rational conversion formula</summary>
    Rational = 9,
    /// <summary>ASAM-MCD2 Text formula.</summary>
    TextFormula = 10,
    /// <summary>ASAM-MCD2 Text Table, (COMPU_VTAB).</summary>
    TextTable = 11,
    /// <summary>ASAM-MCD2 Text Range Table (COMPU_VTAB_RANGE).</summary>
    TextRange = 12,
    /// <summary>date (Based on 7 Byte Date data structure, V3 only).</summary>
    Date = 132,
    /// <summary>time (Based on 6 Byte Time data structure, V3 only).</summary>
    Time = 133,
    /// <summary>value range to value tabular look-up (V4 only)</summary>
    TabRange,
    /// <summary>text to value tabular look-up (V4 only)</summary>
    TextToValue,
    /// <summary>text to text tabular look-up (translation, V4 only)</summary>
    TextToText,
    /// <summary>1:1 conversion formula (Int = Phys).</summary>
    None = 0xFFFF,
  }

  /// <summary>MDF Signal types (V4 specification)</summary>
  public enum SignalType
  {
    /// <summary>unsigned integer little endian</summary>
    UINT_LE,
    /// <summary>unsigned integer big endian</summary>
    UINT_BE,
    /// <summary>signed integer little endian</summary>
    SINT_LE,
    /// <summary>signed integer big endian</summary>
    SINT_BE,
    /// <summary>IEEE 754 floating-point format little endian</summary>
    FLOAT_LE,
    /// <summary>IEEE 754 floating-point format big endian</summary>
    FLOAT_BE,
    /// <summary>String (NULL terminated)</summary>
    STRING,
    /// <summary>UTF8 String (NULL terminated)</summary>
    STRING_UTF8,
    /// <summary>UTF16 String (NULL terminated) little endian</summary>
    STRING_UTF16_LE,
    /// <summary>UTF16 String (NULL terminated) big endian</summary>
    STRING_UTF16_BE,
    /// <summary>Byte Array with unknown content (e.g. structure)</summary>
    BYTE_ARRAY,
    /// <summary>MIME sample (sample is Byte Array with MIME content-type specified in cn_md_unit)</summary>
    MIME_SAMPLE,
    /// <summary>MIME stream (all samples of channel represent a stream with MIME content-type specified in cn_md_unit)</summary>
    MIME_STREAM,
    /// <summary>CANopen date (Based on 7 Byte CANopen Date data structure, see Table 35)</summary>
    CANOPEN_DATA,
    /// <summary>CANopen date (Based on 6 Byte CANopen Time data structure, see Table 35)</summary>
    CANOPEN_TIME,
  }

  /// <summary>
  /// Time quality.
  /// </summary>
  public enum TimeQualityType
  {
    /// <summary>Local PC reference time (Default).</summary>
    LocalPC = 0,
    /// <summary>external time source.</summary>
    ExternalSource = 10,
    /// <summary>external absolute synchronized time.</summary>
    ExternalAbsolute = 16,
  }

  /// <summary>
  /// 
  /// </summary>
  public enum TimeFlagsType
  {
    LocalTime = 1,
    OffsetsValid = 2,
  }

  /// <summary>
  /// Standard Flags for unfinalized MDF.
  /// 
  /// Bit combination of flags that indicate the steps required to finalize the MDF file. 
  /// For a finalized MDF file, the value must be 0 (no flag set).
  /// </summary>
  [Flags]
  public enum UnfinalizedFlagsType
  {
    /// <summary>
    /// Update of record counters for CGBLOCKs required.
    /// 
    /// The value for the number of records (i.e. number of samples) in a CGBLCOK may be wrong (zero or not up-to-date).
    /// This flag must only be set in case a successful restoration is possible, i.e. if any of the following restrictions 
    /// for the data block of the parent DGBLOCK is fulfilled: The data block is limited by the end of the file (EOF).
    /// - The data block is limited by the start of another MDF block within the block hierarchy of the MDF file.
    /// - The data block is limited by a value not used as record ID (only in case a record ID is used).
    /// - The data block is limited by a record that contains an invalid time stamp, e.g. a time stamp smaller than the 
    /// previous time stamp within the same channel group.
    /// 
    /// A correction of the record counters usually requires the iteration  over all records, so this may also be done 
    /// during sorting an unsorted data block (see 4.2).
    /// </summary>
    UpdateCGBlockRequired = 0x01,
    /// <summary>
    /// Update of reduced sample counters for SRBLOCKs required The value for the number of reduced samples in a SRBLOCKs 
    /// may be wrong (zero or not up-to-date). This flag must only be set in case a successful restoration is possible, 
    /// i.e. if any of the following restrictions for the data block of the SRBLOCK is fulfilled: 
    /// - The data block is limited by the end of the file (EOF).
    /// - The data block is limited by the start of another MDF block within the block hierarchy of the MDF file.
    /// - The data block is limited by a record that contains an invalid time stamp for the interval start, e.g. a 
    /// time stamp smaller than the previous time stamp.
    /// 
    /// A correction of the reduced sample counters usually requires 
    /// the iteration over all records.
    /// </summary>
    UpdateSRBlockRequired = UpdateCGBlockRequired << 1,
  }

  /// <summary>
  /// Custom Flags for unfinalized MDF.
  /// 
  /// Bit combination of flags that indicate custom steps required to finalize the MDF file.
  /// For a finalized MDF file, the value must be 0 (no flag set). See also 3.3.2 Unfinalized MDF.
  /// Custom flags should only be used to handle cases that are not covered by the (currently known) 
  /// standard flags(see above). The meaning of the flags depends on the creator tool, i.e. the 
  /// application that has written the MDF file(see id_prog). Finalization should only be done by the 
  /// creator tool or a tool that is familiar with all custom finalization steps required for a file 
  /// from this creator tool.
  /// </summary>
  [Flags]
  public enum CustomFlagsType
  {
  }

  /// <summary>
  /// Number of record IDs in the data block.
  /// </summary>
  public enum RecordIDType
  {
    /// <summary>data records without record ID</summary>
    None,
    /// <summary>record ID (UINT8) before each data record</summary>
    Before8Bit,
    /// <summary>record ID (UINT16) before each data record</summary>
    Before16Bit,
    /// <summary>record ID (UINT32) before each data record</summary>
    Before32Bit = 4,
    /// <summary>record ID (UINT64) before each data record</summary>
    Before64Bit = 8,
    /// <summary>record ID (UINT8) before and after each data record (V3 only)</summary>
    BeforeAndAfter8Bit = 0xff,
  }

  /// <summary>Dependency data.</summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public sealed class DependencyType
  {
    #region ctors
    public DependencyType(Int64 linkDG, Int64 linkCG, Int64 linkCN)
    { LinkDG = linkDG; LinkCG = linkCG; LinkCN = linkCN; }
    #endregion
    #region properties
    /// <summary>Data Group link</summary>
    public Int64 LinkDG { get; }
    /// <summary>Channel Group link</summary>
    public Int64 LinkCG { get; }
    /// <summary>Channel link</summary>
    public Int64 LinkCN { get; }
    /// <summary>Data Group</summary>
    public IDGBLOCK DataGroup { get; private set; }
    /// <summary>Channel Group</summary>
    public ICGBLOCK ChannelGroup { get; private set; }
    /// <summary>Channel</summary>
    public ICNBLOCK Channel { get; private set; }
    #endregion
    #region methods
    internal bool createDependencies(Dictionary<Int64, IMDFBlock> references)
    {
      IMDFBlock block;
      if (!references.TryGetValue(LinkDG, out block) || !(block is IDGBLOCK))
        return false;
      DataGroup = (IDGBLOCK)block;

      if (!references.TryGetValue(LinkCG, out block) || !(block is ICGBLOCK))
        return false;
      ChannelGroup = (ICGBLOCK)block;

      if (!references.TryGetValue(LinkCN, out block) || !(block is ICNBLOCK))
        return false;
      Channel = (ICNBLOCK)block;

      return true;
    }
    #endregion
  }

  /// <summary>
  /// Basic MDF block interface.
  /// </summary>
  [TypeConverter(typeof(ExpandableObjectConverter))]
  public interface IMDFBlock
  {
    #region properties
    /// <summary>Gets the Name.</summary>
    string Name { get; }
    /// <summary>Gets the size of the MDF block.</summary>
    Int64 BlockSize { get; }
    /// <summary>
    /// Gets or sets the Tag property.
    /// 
    /// - This property may be used to bind any user reference to this instance.
    /// </summary>
    object Tag { get; set; }
    #endregion
    #region methods
    /// <summary>
    /// Writes the block to stream.
    /// </summary>
    /// <param name="bw">The stream to write to</param>
    /// <param name="isLast">true if the last block of a block list is written</param>
    /// <returns>The position the block is written to</returns>
    Int64 writeBlock(BinaryWriter bw, bool isLast = true);
    #endregion
  }

  public interface IDependencyBlock : IMDFBlock
  {
    List<DependencyType> Dependencies { get; }
    void createDependencies(Dictionary<long, IMDFBlock> references);
  }

  /// <summary>
  /// Sample reduction Block interface.
  /// </summary>
  public interface ISRBLOCK : IMDFBlock
  {
    /// <summary>Number of reduced samples in the data block</summary>
    UInt64 NrOfRedSamples { get; }
    /// <summary>Length of time interval [s] used to calculate the reduced samples.</summary>
    double LenOfTimeInt { get; }
  }

  /// <summary>
  /// ID Block interface.
  /// </summary>
  public interface IIDBLOCK : IMDFBlock
  {
    /// <summary>File identifier, always contains "MDF␣␣␣␣␣"</summary>
    string FileID { get; }
    /// <summary>Version number of MDF format</summary>
    UInt16 Version { get; }
    /// <summary>Standard Flags for unfinalized MDF.</summary>
    UnfinalizedFlagsType UnfinalizedFlags { get; }
    /// <summary>Custom Flags for unfinalized MDF.</summary>
    CustomFlagsType CustomFlags { get; }
    /// <summary>V1..V3 or V4 MDF type. (not contained in file content)</summary>
    MDFType Type { get; }
  }

  /// <summary>
  /// Interface to access TXBLOCK comments.
  /// </summary>
  public interface IMDFComment : IMDFBlock
  {
    /// <summary>
    /// Gets the corresponding comment.
    /// </summary>
    string Comment { get; }
  }

  /// <summary>
  /// Header Block interface.
  /// </summary>
  public interface IHDBLOCK : IMDFComment
  {
    /// <summary>
    /// Gets the corresponding DGBLOCK (Data group) children.
    /// </summary>
    List<IDGBLOCK> DGBlocks { get; }
    /// <summary>Time, the recording was started</summary>
    DateTime RecordingTime { get; }
  }

  /// <summary>
  /// Interface for Data containers like SRBLOCK and DGBLOCK.
  /// </summary>
  public interface IDataContainer
  {
    /// <summary>
    /// Reads recorded data from the specific offset into the specified buffer.
    /// </summary>
    /// <param name="offset">Offset to recording start</param>
    /// <param name="buffer">buffer to store the data read</param>
    bool readData(Int64 offset, ref byte[] buffer);
    /// <summary>
    /// Gets the reader from the specified offset.
    /// </summary>
    /// <param name="offset">Offset to get the reader from</param>
    /// <returns>The stream reader instance</returns>
    BinaryReader getReader(long offset);
  }

  /// <summary>
  /// Data Group Block interface..
  /// </summary>
  public interface IDGBLOCK : IMDFComment, IDataContainer
  {
    /// <summary>
    /// Gets the record ID type.
    /// </summary>
    RecordIDType RecordIDType { get; }
    /// <summary>
    /// Gets the corresponding CGBLOCK (Channel group) children.
    /// </summary>
    List<ICGBLOCK> CGBlocks { get; }
    /// <summary>
    /// The handler to write recorded raw Data.
    /// 
    /// Called for each IDGBLOCK when MDFWriter.save is called.
    /// </summary>
    event EventHandler<WriteRawDataEventArgs> OnWriteRawData;
  }

  /// <summary>
  /// Channel group Block interface.
  /// </summary>
  public interface ICGBLOCK : IMDFComment
  {
    /// <summary>
    /// Gets the corresponding CNBLOCK (Channel reduction) children.
    /// </summary>
    List<ICNBLOCK> CNBlocks { get; }
    /// <summary>
    /// Number of records of this type in the data block,
    /// i.e. number of samples for this channel group.
    /// </summary>
    UInt64 RecordCount { get; set; }
    /// <summary>
    /// Size of data record in Bytes (without record ID), 
    /// i.e. size of plain data (including the time channel) 
    /// for a each recorded sample of this channel group.
    /// </summary>
    UInt32 RecordSize { get; set; }
    /// <summary>
    /// Gets the parent data group of the channel group.
    /// </summary>
    IDGBLOCK DGBlock { get; }
    /// <summary>
    /// List of SRBlocks.
    /// </summary>
    List<ISRBLOCK> SRBlocks { get; }
    /// <summary>
    /// Gets the size of a single data record (inclusive record id).
    /// </summary>
    /// <returns></returns>
    Int64 getRecordSize();
  }

  /// <summary>
  /// Channel Block interface.
  /// </summary>
  public interface ICNBLOCK : IMDFComment
  {
    /// <summary>Gets the channel's type</summary>
    ChannelType ChannelType { get; }
    /// <summary>Gets the signal type.</summary>
    SignalType SignalType { get; }
    /// <summary>Gets the synchronization type</summary>
    SyncType SyncType { get; }
    /// <summary>
    /// Gets the Number of bits for signal value in record.
    /// </summary>
    UInt32 NoOfBits { get; set; }
    /// <summary>
    /// Gets the Offset to the first Byte in the data record that contains bits of the signal value.
    /// 
    /// The offset is applied to the plain record data, i.e.skipping the IDGBLOCK.RecordIDType.
    /// </summary>
    UInt32 AddOffset { get; }
    /// <summary>
    /// Gets the the Bit offset (0-7): first bit (=LSB) of signal value after Byte offset has been applied(see 5.21.4.2 Reading the Signal Value).
    /// 
    /// If zero, the signal value is 1-Byte aligned.A value different to zero is only allowed for Integer data types and if the Integer 
    /// signal value fits into 8 contiguous Bytes (ICNBLOCK.NoOfBits + ICNBLOCK.BitOffset within 64). For all other cases, 
    /// ICNBLOCK.BitOffset must be zero.
    /// </summary>
    UInt16 BitOffset { get; }
    /// <summary>Gets the chanels' Unit</summary>
    string Unit { get; }
    /// <summary>Minimum signal value that occurred for this signal (raw value)</summary>
    double MinRaw { get; set; }
    /// <summary>Maximum signal value that occurred for this signal (raw value)</summary>
    double MaxRaw { get; set; }
    /// <summary>Minimum signal value that occurred for this signal (physical value)</summary>
    double Min { get; }
    /// <summary>Maximum signal value that occurred for this signal (physical value)</summary>
    double Max { get; }
    /// <summary>Extende Minimum signal value that occurred for this signal (physical value)</summary>
    double MinEx { get; }
    /// <summary>Extende Maximum signal value that occurred for this signal (physical value)</summary>
    double MaxEx { get; }
    /// <summary>
    /// Gets the parent channel group of the channel.
    /// </summary>
    ICGBLOCK CGBlock { get; }
    /// <summary>
    /// Gets the corresponding CCBLOCK.
    /// </summary>
    ICCBLOCK CCBlock { get; }
    /// <summary>
    /// Gets the corresponding CDBlock (only valid format smaller than V4.00).
    /// </summary>
    IMDFBlock CDBlock { get; }
    /// <summary>
    /// Gets the coresponding CEBlock (only valid format smaller than V4.00).
    /// </summary>
    IMDFBlock CEBlock { get; }
    /// <summary>
    /// Gets the read position for a specific record.
    /// </summary>
    /// <param name="recordIndex">The record's index</param>
    /// <returns>The read position</returns>
    Int64 getReadPosition(Int64 recordIndex);
  }

  /// <summary>
  /// Channel computation Block interface.
  /// </summary>
  public interface ICCBLOCK : IMDFComment
  {
    ConversionType ConversionType { get; }
    UInt16 TabSize { get; }
    /// <summary>Minimum physical signal value</summary>
    double Min { get; set; }
    /// <summary>Maximum physical signal value</summary>
    double Max { get; set; }
    /// <summary>Physical unit</summary>
    string Unit { get; }
    /// <summary>Formula string for ConversionType.ParametricLinear, ConversionType.Polynomial, ConversionType.Exponential, ConversionType.Logarithmic, ConversionType.Rational</summary>
    double[] Params { get; set; }
    /// <summary>Key/Value pairs for ConversionType.TextTable, ConversionType.Tab, ConversionType.TabInt</summary>
    SortedList<object, object> KeyValuePairs { get; set; }
    /// <summary>Formula string for ConversionType.TextFormula</summary>
    string Formula { get; set; }
    /// <summary>DefaultText for ConversionType.TextRange</summary>
    string DefaultText { get; set; }
    /// <summary>
    /// Link to inverse CCBlock (V4 only).
    /// </summary>
    ICCBLOCK InvCCBlock { get; }
    /// <summary>
    /// Computes the physical value from a raw value.
    /// </summary>
    double toPhysical(double rawValue);
  }
}