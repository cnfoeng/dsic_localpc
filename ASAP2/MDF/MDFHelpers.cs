﻿/*!
 * @file    
 * @brief   Implements the MDF helpers.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    September 2017
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.MDF.V4;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;

namespace jnsoft.MDF
{
  /// <summary>
  /// MDF Helper functions and constants.
  /// </summary>
  static class Helpers
  {
    #region constants
    internal static DateTime DATE_TIME_1970 = new DateTime(1970, 1, 1);
    internal const int BUFFER_SIZE = 16 * UInt16.MaxValue;
    internal const long HEADER_POS = 64;
    internal static readonly double[] LINEAR_IDENTITY = new double[] { 1, 0 };
    internal static readonly double[] RAT_IDENTITY = new double[] { 0, 1, 0, 0, 0, 1 };
    #endregion
    #region methods
    /// <summary>Gets a string (with default encoding) from a byte array</summary>
    /// <param name="valArray">array</param>
    /// <returns>string instance</returns>
    internal static string getString(ref byte[] valArray)
    {
      return getString(ref valArray, Encoding.Default);
    }

    /// <summary>Gets a string with the specified encoding from a byte array</summary>
    /// <param name="valArray">array</param>
    /// <param name="encoding">Encoding for interpreting the string</param>
    /// <returns>string instance</returns>
    internal static string getString(ref byte[] valArray, Encoding encoding)
    {
      return valArray == null
        ? string.Empty
        : encoding.GetString(valArray).Trim('\0');
    }

    /// <summary>Creates a byte array from a string</summary>
    /// <param name="target">target byte array</param>
    /// <param name="maxLen">the maximum length of the array to create</param>
    /// <param name="text">the text to fill into the array</param>
    /// <param name="preserveTermination">true if a null termination byte should be preserved</param>
    internal static void createArrayFromString(out byte[] target, int maxLen, string text, bool preserveTermination = true)
    {
      target = new byte[maxLen];
      if (!string.IsNullOrEmpty(text))
        Array.Copy(Encoding.Default.GetBytes(text), target, Math.Min(text.Length, preserveTermination ? maxLen - 1 : maxLen));
    }

    /// <summary>
    /// Sorting method for ICNBLOCKs.
    /// 
    /// Sorts by master channels and any others by name.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    internal static int sortByMasterThenName(ICNBLOCK x, ICNBLOCK y)
    {
      if (x == y)
        return 0;
      if (x.ChannelType == ChannelType.Master || x.ChannelType == ChannelType.VirtualMaster)
        return -1;
      if (y.ChannelType == ChannelType.Master || y.ChannelType == ChannelType.VirtualMaster)
        return 1;
      return x.Name.CompareTo(y.Name);
    }

    /// <summary>
    /// Reads and builds the CC Block computation parameters.
    /// </summary>
    /// <param name="br"></param>
    /// <param name="count"></param>
    /// <param name="conversion"></param>
    /// <returns>A double array with computation parameters</returns>
    internal static double[] getCCParameters(BinaryReader br, int count, ConversionType conversion)
    {
      var Params = new double[count];
      for (int i = 0; i < count; ++i)
        Params[i] = br.ReadDouble();
      switch (conversion)
      {
        case ConversionType.ParametricLinear:
          if (Params.SequenceEqual(Helpers.RAT_IDENTITY))
            Params = LINEAR_IDENTITY;
          break;
        case ConversionType.Rational:
          if (Params.SequenceEqual(Helpers.RAT_IDENTITY))
            Params = RAT_IDENTITY;
          else
          { // MDF specifies to compute with inversed parameters.
            for (int i = 0; i < Params.Length; ++i)
            {
              if (Params[i] == 0)
                continue;
              Params[i] = i == 2 ? -Params[i] : 1.0 / Params[i];
            }
          }
          break;
      }
      return Params;
    }
    #endregion
  }

  /// <summary>
  /// MDF DataReader handling data blocks (DTBLOCKV4) and zipped data blocks (DZBLOCKV4) and decompression.
  /// </summary>
  sealed class DataReader : IDisposable
  {
    #region types
    sealed class Adler32Computer
    {
      const int Modulus = 65521;
      int a = 1;
      int b = 0;
      public int Checksum { get { return ((b * 65536) + a); } }
      public void Update(byte[] data, int offset, int length)
      {
        for (int counter = 0; counter < length; ++counter)
        {
          a = (a + (data[offset + counter])) % Modulus;
          b = (b + a) % Modulus;
        }
      }
    }
    #endregion
    #region members
    /// <summary>The maximum size of uncompressed data to compress into a single DZBLOCKV4, 4 MByte</summary>
    internal const int MAX_UNCOMPRESSED = (int)((UInt16.MaxValue + 1) << 6);
    MemoryMappedFile mMMFile;
    string mTempFile;
    Stream mDecompressedStream;
    #endregion
    #region ctors

    /// <summary>
    /// Creates an instance with the specified file.
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="tryMemoryMapped"></param>
    internal DataReader(string filename, bool tryMemoryMapped = true)
    {
      Reader = createReader(filename, tryMemoryMapped);
    }

    /// <summary>
    /// Creates an instance for compressed data fragments.
    /// </summary>
    internal DataReader()
    {
      mTempFile = Path.GetTempFileName();
      mDecompressedStream = new FileStream(mTempFile, FileMode.Create, FileAccess.Write, FileShare.None, MAX_UNCOMPRESSED);
    }

    #endregion
    #region properties

    /// <summary>Gets the binary reader for the underlying file.</summary>
    internal BinaryReader Reader { get; private set; }

    /// <summary>Gets or sets the reader's position</summary>
    internal long Position
    {
      get { return Reader.BaseStream.Position; }
      set { Reader.BaseStream.Position = value; }
    }

    #endregion
    #region methods
    /// <summary>
    /// Add a zipped data block and deompress to internal stream.
    /// </summary>
    /// <param name="dzBlock">The zipped data block to decompress</param>
    /// <param name="compressedData">The zipped data</param>
    internal void Add(DZBLOCKV4 dzBlock, byte[] compressedData)
    {
      DecompressToFile(dzBlock, compressedData, mDecompressedStream);
    }

    /// <summary>
    /// Creates the binary reader to start reading from uncompressed data.
    /// 
    /// This function call is required after the last DZBlock was added with DataReader.Add.
    /// </summary>
    /// <param name="tryMemoryMapped"></param>
    internal void createReader(bool tryMemoryMapped = true)
    {
      mDecompressedStream.Dispose();
      mDecompressedStream = null;
      Reader = createReader(mTempFile, tryMemoryMapped);
    }

    /// <summary>
    /// Creates the reader with the specified filename.
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="tryMemoryMapped"></param>
    /// <returns>The binary reader instance</returns>
    BinaryReader createReader(string filename, bool tryMemoryMapped = true)
    {
      Stream fs;
      if (tryMemoryMapped)
      {
        try
        { // try to read the file via a memory mapped file
          mMMFile = MemoryMappedFile.CreateFromFile(filename);
          fs = mMMFile.CreateViewStream(0, new FileInfo(filename).Length, MemoryMappedFileAccess.Read);
        }
        catch
        { // exception thrown, try to read the file via a standard file stream
          if (mMMFile != null)
          {
            mMMFile.Dispose();
            mMMFile = null;
          }
          fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 8, FileOptions.RandomAccess);
        }
      }
      else
        fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 8, FileOptions.RandomAccess);
      return new BinaryReader(fs);
    }

    /// <summary>
    /// Frees local resources.
    /// </summary>
    public void Dispose()
    {
      if (Reader != null)
        Reader.Dispose();

      if (mMMFile != null)
        mMMFile.Dispose();

      if (mDecompressedStream != null)
        mDecompressedStream.Dispose();

      if (!string.IsNullOrEmpty(mTempFile) && File.Exists(mTempFile))
        File.Delete(mTempFile);
    }

    #region static helper methods
    /// <summary>
    /// Used to transpose data to improve compression rate.
    /// </summary>
    /// <param name="dataIn"></param>
    /// <param name="nc"></param>
    /// <param name="mc"></param>
    /// <param name="targetStream"></param>
    static void transpose(ref byte[] dataIn, UInt32 nc, UInt32 mc, Stream targetStream)
    {
      for (uint n = 0; n < nc; ++n)
        for (uint m = 0; m < mc; ++m)
          targetStream.WriteByte(dataIn[m * nc + n]);

      int start = (int)(nc * mc);
      if (dataIn.Length > start)
        // copy remaining bytes
        targetStream.Write(dataIn, start, dataIn.Length - start);
    }

    /// <summary>
    /// (ZLib-)Decompress a compressed stream to the specified file.
    /// </summary>
    /// <param name="dzBlock">Compressed data block</param>
    /// <param name="data">Compressed data</param>
    /// <param name="targetStream">target stream for decompressed data</param>
    internal static void DecompressToFile(DZBLOCKV4 dzBlock, byte[] data, Stream targetStream)
    {
      // strip zlib header from compressed data
      using (var compressedStream = new MemoryStream(data, 2, data.Length - 2))
      {
        using (var deflateStream = new DeflateStream(compressedStream, CompressionMode.Decompress))
        {
          switch (dzBlock.ZipType)
          {
            case ZipType.TransposeAndDeflate:
              using (var targetMem = new MemoryStream())
              {
                deflateStream.CopyTo(targetMem);
                targetMem.Position = 0;
                using (var br = new BinaryReader(targetMem))
                { // transpose uncompressed bytes and write to target stream
                  var nonTransposedBytes = br.ReadBytes((int)targetMem.Length);
                  transpose(ref nonTransposedBytes, (UInt32)(dzBlock.Size / dzBlock.ZipParameter), dzBlock.ZipParameter, targetStream);
                }
              }
              break;

            case ZipType.Deflate:
              // transpose uncompressed bytes and write to target stream
              deflateStream.CopyTo(targetStream);
              break;
          }
        }
      }
    }

    /// <summary>
    /// Compresses the specified byte array.
    /// </summary>
    /// <param name="uncompressed">uncompressed bytes</param>
    /// <returns>compressed bytes</returns>
    static byte[] compress(byte[] uncompressed)
    {
      using (var compressStream = new MemoryStream())
      using (var compressor = new DeflateStream(compressStream, CompressionMode.Compress))
      {
        compressor.Write(uncompressed, 0, uncompressed.Length);
        compressor.Close();
        return compressStream.ToArray();
      }
    }

    /// <summary>
    /// Compress data from stream into a compressed) memory stream.
    /// </summary>
    /// <param name="stream">Stream to compress</param>
    /// <param name="zipType">compression type</param>
    /// <param name="zipParameter">Compression Transpose parameter</param>
    /// <returns>The compressed stream</returns>
    internal static MemoryStream CompressToStream(SingleDataStream stream, ZipType zipType, uint zipParameter)
    {
      var result = new MemoryStream();

      // Add zip file header
      result.WriteByte(0x78);
      result.WriteByte(0x01);

      // Add data
      using (var dataFs = new FileStream(stream.Filename, FileMode.Open, FileAccess.Read, FileShare.None))
      {
        var uncompressedData = new byte[dataFs.Length];
        dataFs.Read(uncompressedData, 0, uncompressedData.Length);
        switch (zipType)
        {
          case ZipType.Deflate:
            break;
          case ZipType.TransposeAndDeflate:
            var transposedStream = new MemoryStream();
            transpose(ref uncompressedData, zipParameter, (UInt32)(uncompressedData.Length / zipParameter), transposedStream);
            transposedStream.Position = 0;
            transposedStream.Read(uncompressedData, 0, uncompressedData.Length);
            break;
        }

        var compressedBytes = compress(uncompressedData);
        result.Write(compressedBytes, 0, compressedBytes.Length);

        // Add 32 Bit ADLER
        var adler = new Adler32Computer();
        adler.Update(uncompressedData, 0, uncompressedData.Length);
        uint checksum = (uint)adler.Checksum;
        result.WriteByte((byte)(checksum >> 24));
        result.WriteByte((byte)(checksum >> 16));
        result.WriteByte((byte)(checksum >> 8));
        result.WriteByte((byte)(checksum >> 0));

        // important - reset to front
        result.Position = 0;
        return result;
      }
    }
    #endregion

    #endregion
  }

  /// <summary>
  /// EVent arguments to request the MDFWriter to write the raw data to the stream.
  /// </summary>
  public sealed class WriteRawDataEventArgs : EventArgs
  {
    /// <summary>Calling Block</summary>
    public IMDFBlock Block { get; }
    /// <summary>Stream to write the raw data to.</summary>
    public BinaryWriter Bw { get; }
    public WriteRawDataEventArgs(IMDFBlock block, BinaryWriter bw)
    { Block = block; Bw = bw; }
  }

  /// <summary>Handles a temporary stream per data handle.</summary>
  sealed class SingleDataStream : IDisposable
  {
    #region members
    static readonly object mSync = new object();
    BinaryWriter mBw;
    #endregion
    #region ctors
    /// <summary>
    /// Creates a new instance with the specified parameters.
    /// </summary>
    /// <param name="zipType"></param>
    /// <param name="zipParameter"></param>
    public SingleDataStream(ZipType zipType = ZipType.None, uint zipParameter = 0)
    {
      Filename = Path.GetTempFileName();
      ZipType = zipType;
      ZipParameter = zipParameter;

      mBw = new BinaryWriter(new FileStream(Filename
        , FileMode.Create, FileAccess.Write, FileShare.None
        ));
    }
    /// <summary>
    /// Copy constructor.
    /// </summary>
    /// <param name="src">source to copy parameters from</param>
    public SingleDataStream(SingleDataStream src)
    {
      Filename = Path.GetTempFileName();
      ZipType = src.ZipType;
      ZipParameter = src.ZipParameter;

      mBw = new BinaryWriter(new FileStream(Filename
        , FileMode.Create, FileAccess.Write, FileShare.None
        ));
    }
    #endregion
    #region properties
    public ZipType ZipType { get; }
    public uint ZipParameter { get; }
    public string Filename { get; private set; }
    public long UncompressedSize { get; private set; }
    public bool Done { get; private set; }
    public Int64 Size { get { return mBw.BaseStream.Position; } }
    #endregion
    #region methods
    public void addData(double timeStamp, ref byte[] data, int size)
    {
      mBw.Write(timeStamp);
      mBw.Write(data, 0, size);
    }

    void writeCompressedFile(object state)
    {
      lock (mBw)
      {
        try
        {
          string compressedFilename;
          lock (mSync)
            compressedFilename = Path.GetTempFileName();

          var fs = new FileStream(compressedFilename, FileMode.Create, FileAccess.Write, FileShare.None, Helpers.BUFFER_SIZE);
          using (var bw = new BinaryWriter(fs))
          {
            var ms = DataReader.CompressToStream(this, ZipType, ZipParameter);
            ms.CopyTo(bw.BaseStream, Helpers.BUFFER_SIZE);
          }

          try { File.Delete(Filename); } catch { }
          Filename = compressedFilename;
        }
        catch { }
        finally { Done = true; }
      }
    }

    public void close(bool writeAsync = false)
    {
      lock (mBw)
      {
        if (!Done)
        {
          mBw.Flush();
          UncompressedSize = mBw.BaseStream.Position;
          mBw.Close();

          switch (ZipType)
          {
            case ZipType.None:
              break;

            default:
              if (writeAsync)
              {
                ThreadPool.QueueUserWorkItem(writeCompressedFile);
                return;
              }
              writeCompressedFile(null);
              break;
          }
          Done = true;
        }
      }
    }

    public void Dispose()
    {
      try
      {
        lock (mBw)
          mBw.Close();
        if (File.Exists(Filename))
          File.Delete(Filename);
      }
      catch { }
    }
    #endregion
  }

  /// <summary>
  /// Structure holding a single annotation and it's timestamp.
  /// </summary>
  public struct Annotation
  {
    /// <summary>
    /// Timestamp in [s].
    /// </summary>
    public double Timestamp;
    /// <summary>
    /// The annotation's text
    /// </summary>
    public string Text;
    /// <summary>
    /// Creates a new instance of Annotation.
    /// </summary>
    /// <param name="timestamp"></param>
    /// <param name="text"></param>
    public Annotation(double timestamp, string text) { Timestamp = timestamp; Text = text; }
  }

  /// <summary>
  /// Holds a list of Annotations.
  /// </summary>
  public sealed class AnnotationList : List<Annotation>
  {
    /// <summary>
    /// Gets the maximum length contained Annotation's.
    /// </summary>
    public int MaxLength { get; private set; }
    /// <summary>
    /// Adds a new annotation.
    /// </summary>
    /// <param name="annotation"></param>
    public new void Add(Annotation annotation)
    {
      MaxLength = Math.Max(MaxLength, annotation.Text.Length + 1);
      base.Add(annotation);
    }
    /// <summary>
    /// Writes the annotation data to the specified data stream.
    /// </summary>
    /// <param name="bw">The stream to write to</param>
    internal void writeAll(BinaryWriter bw)
    {
      Sort((x, y) => { return x.Timestamp.CompareTo(y.Timestamp); });
      foreach (var annotation in this)
      {
        bw.Write(annotation.Timestamp);
        byte[] buffer;
        Helpers.createArrayFromString(out buffer, MaxLength, annotation.Text);
        bw.Write(buffer);
      }
    }
  }
}
