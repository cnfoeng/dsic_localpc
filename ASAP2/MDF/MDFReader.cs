﻿/*!
 * @file    
 * @brief   Implements the MDF reader.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Formulas;
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using jnsoft.MDF.V3;
using jnsoft.MDF.V4;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

/// <summary>
/// All the MDF (Measurement Data Format) stuff.
/// </summary>
namespace jnsoft.MDF
{
  /// <summary>
  /// Implements the MDF (Measurement Data Format) reader.
  /// 
  /// - Supports MDF versions (up to 4.x)
  /// - If possible, uses a memory mapped file to provide maximum performance 
  /// when reading data from the MDF file.
  /// </summary>
  /// <example>Example code to request records of the first second for each of the contained CNBLOCK instances:
  /// <code>
  /// using(var mdfFile = MDFReader.open("test.mdf"))
  /// {
  ///   var channels = new List<ICNBLOCK>(mdfFile.OpenChannels.Keys);
  ///   var dataMem = new IntPtr[channels.Length];
  ///   var maxPoints = new uint[channels.Length];
  ///   DataLimits[] limitsX, limitsY;
  ///   for(int i = 0; i < channels.Length; ++i)
  ///   { // create memory for a maximum of 1000 samples
  ///     dataMem[i] = new IntPtr(DataPoint.Size * 1000);
  ///     maxPoints[i] = 1000;
  ///   }
  ///   bool result = mdfFile.getData(ValueObjectFormat.Physical, 0, 1, channels.ToArray(), maxPoints, dataMem, out limitsX, out limitsY);
  ///   // print values
  ///   for(int i = 0; i < channels.Length; ++i )
  ///   { // Iterate over channels
  ///     for (int j = 0; j < maxPoints[i]; ++j)
  ///     { // Iterate over sample points
  ///       DataPoint p = MDFReader.getDatafromMemory(dataMem[i], j);
  ///       Console.WriteLine("channel={0} value[{1}]={2}", channels[i].getName(mdfFile.Reader), j, p);
  ///     }
  ///   }
  ///   // cleanup memory
  ///   foreach (IntPtr ptr in dataMem)
  ///     Marshal.FreeHGlobal(ptr);
  /// }
  /// </code>
  /// </example>
  public sealed partial class MDFReader : IDisposable
  {
    #region types
    /// <summary>Represents MDF signal limits.</summary>
    public sealed class Limits { public double Min, Max; }
    #endregion
    #region members
    A2LFormulaDict mFormulaDict = new A2LFormulaDict();
    DataReader mDR;
    Dictionary<long, IMDFBlock> mReferences = new Dictionary<long, IMDFBlock>();
    #endregion
    #region constructor
    /// <summary>
    /// Creates a new instance of the MDFFile.
    /// 
    /// Used by the MDFFile.open method.
    /// </summary>
    /// <param name="path">the fully qualified MDF filename</param>
    /// <param name="id">The IDBlock instance (IDBLOCK or IDBLOCKV4)</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    MDFReader(string path, IIDBLOCK id
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      ParserEvents = new List<ParserEventArgs>();
      SourceFile = path;
      ID = id;

      try
      {
        onParserMessage(new ParserEventArgs(this, MessageType.Info
          , string.Format(Resources.strMDFOpenMsg, id.Version / 100, id.Version - (id.Version / 100) * 100, path))
          );
        progressHandler?.Invoke(this, new ProgressArgs(0));
        mDR = new DataReader(path);
        mDR.Position = Helpers.HEADER_POS;

        // read header block
        Header = id.Type == MDFType.V4
          ? (IHDBLOCK)new HDBLOCKV4(mDR.Reader, mReferences)
          : new HDBLOCK(mDR.Reader, mReferences, ((IDBLOCK)id).ByteOrder);

        // create opened channel and Data group dictionary
        int cntDG = 0;
        foreach (var dgBlock in Header.DGBlocks)
        { // Iterate over Data groups
          progressHandler?.Invoke(this, new ProgressArgs((cntDG++) * 100 / Header.DGBlocks.Count));
          foreach (var cgBlock in dgBlock.CGBlocks)
          { // Iterate over Channel groups
            OpenChannelGroups.Add(cgBlock);
            foreach (var cnBlock in cgBlock.CNBlocks)
            { // Iterate over Channels and get the conversion
              OpenChannels.Add(cnBlock);
              switch (cnBlock.CCBlock?.ConversionType)
              {
                case ConversionType.TextFormula:
                  mFormulaDict.add(null, cnBlock.Name, cnBlock.CCBlock.Formula);
                  break;
              }
            }
          }
        }

        // create dependencies

        if (Header is HDBLOCKV4)
        { // create dependencies in V4 header
          foreach (var block in ((HDBLOCKV4)Header).CHBlocks)
            ((IDependencyBlock)block).createDependencies(mReferences);
        }

        foreach (var block in OpenChannels)
        { // create dependencies in channel blocks

          if (block is CNBLOCKV4)
            ((CNBLOCKV4)block).createDependencies(mReferences);

          if (block.CDBlock is IDependencyBlock)
            ((IDependencyBlock)block.CDBlock).createDependencies(mReferences);
        }

        mFormulaDict.buildAssembly(null, null);
        onParserMessage(new ParserEventArgs(this, MessageType.Info, Resources.strMDFOpenedMsg));
      }
      catch
      {
        if (mDR != null)
          mDR.Dispose();
        throw;
      }
      finally
      {
        progressHandler?.Invoke(this, new ProgressArgs(100));
      }
    }
    #endregion
    #region events
    /// <summary>Register here for parser messages.</summary>
    public EventHandler<ParserEventArgs> ParserMessage;
    #endregion
    #region properties
    /// <summary>Gets events the MDF object has created.</summary>
    public List<ParserEventArgs> ParserEvents { get; internal set; }
    /// <summary>Gets the count of errors occured.</summary>
    public int CountErrors { get; private set; }
    /// <summary>Gets the count of warnings occured.</summary>
    public int CountWarnings { get; private set; }
    /// <summary>Gets the fully qualified path to the source file.</summary>
    [Category(Constants.CATData), ReadOnly(true), Description("Source file path.")]
    public string SourceFile { get; private set; }
    /// <summary>
    /// Gets the IDBlock instance.
    /// </summary>
    public IMDFBlock ID { get; private set; }
    /// <summary>
    /// Gets the HDBlock instance.
    /// 
    /// - This is the root node of the MDF file.
    /// - All childs are reachable from this instance.
    /// </summary>
    public IHDBLOCK Header { get; private set; }
    /// <summary>
    /// Gets the opened (available) channels dictionary.
    /// </summary>
    public HashSet<ICNBLOCK> OpenChannels { get; private set; } = new HashSet<ICNBLOCK>();
    /// <summary>
    /// Gets the opened (available) channel group dictionary.
    /// </summary>
    public HashSet<ICGBLOCK> OpenChannelGroups { get; private set; } = new HashSet<ICGBLOCK>();
    /// <summary>Gets the start and End of all Master channels.</summary>
    public Limits MasterLimits { get; private set; }
    #endregion
    #region methods
    #region interface
    /// <summary>
    /// Opens a MDF file and returns a MDFFile instance.
    /// 
    /// </summary>
    /// <param name="path">the fully qualified MDF file filename</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>A new MDFFile instance or null if failed</returns>
    public static MDFReader open(string path, EventHandler<ProgressArgs> progressHandler = null)
    {
      IIDBLOCK idBlock = null;
      using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        using (var br = new BinaryReader(fs))
        {
          idBlock = new IDBLOCK(br);
          if (idBlock.FileID != Resources.strMDFMagicHeader)
            // not supported
            return null;

          if (idBlock.Version >= 400)
          { // interpret file as MDF V4
            br.BaseStream.Position = 0;
            idBlock = new IDBLOCKV4(br);
          }
        }
      }
      return new MDFReader(path, idBlock, progressHandler);
#if MDFV4_SUPPORT
#endif
    }
    /// <summary>
    /// Gets a Datapoint from the specified memory on the specified channelindex.
    /// </summary>
    /// <param name="dataMem">Memory of DataPoint structures</param>
    /// <param name="channelIndex">The index to get the DataPoint from</param>
    /// <returns>The DataPoint instance</returns>
    public static DataPoint getDatafromMemory(IntPtr dataMem, int channelIndex)
    {
      return (DataPoint)Marshal.PtrToStructure(IntPtr.Add(dataMem, channelIndex * DataPoint.Size), typeof(DataPoint));
    }
    /// <summary>
    /// Gets a DatapointF from the specified memory on the specified channelindex.
    /// </summary>
    /// <param name="dataMem">Memory of DataPoint structures</param>
    /// <param name="channelIndex">The index to get the DataPointF from</param>
    /// <returns>The DataPointF instance</returns>
    public static DataPointF getDataFfromMemory(IntPtr dataMem, int channelIndex)
    {
      return (DataPointF)Marshal.PtrToStructure(IntPtr.Add(dataMem, channelIndex * DataPointF.Size), typeof(DataPointF));
    }
    /// <summary>
    /// Gets the overall relative length in seconds of an opened file.
    /// </summary>
    /// <returns>The relative length in seconds.</returns>
    public double getLength()
    {
      if (MasterLimits == null)
      {
        var start = double.MaxValue;
        var end = double.MinValue;

        var en = OpenChannelGroups.GetEnumerator();
        while (en.MoveNext())
        {
          if (en.Current.RecordCount < 1)
            continue;

          var masterChn = getMasterChannel(en.Current);
          if (masterChn == null)
            continue;

          var timeStart = getData(ValueObjectFormat.Physical, masterChn, 0);
          if (double.IsNaN(timeStart))
            continue;

          double timeEnd = getData(ValueObjectFormat.Physical, masterChn, (long)en.Current.RecordCount - 1);
          if (double.IsNaN(timeEnd))
            continue;

          if (timeEnd >= timeStart)
          {
            start = Math.Min(start, timeStart);
            end = Math.Max(end, timeEnd);
          }
        }

        if (end != double.MinValue)
          MasterLimits = new Limits { Min = start, Max = end };
      }
      return MasterLimits == null
        ? double.NaN
        : MasterLimits.Max - MasterLimits.Min;
    }

    /// <summary>
    /// Gets the value for the specified signal from the specified record index.
    /// </summary>
    /// <param name="format">The data format (supported is ValueObjectFormat.Physical and ValueObjectFormat.Raw in this case)</param>
    /// <param name="cnBlock">The channel</param>
    /// <param name="recordIndex">The recordindex</param>
    /// <returns>The value or double.NaN if failed</returns>
    public double getData(ValueObjectFormat format, ICNBLOCK cnBlock, Int64 recordIndex)
    {
      if (cnBlock.CGBlock.RecordSize == 0)
        return double.NaN;

      // create record buffer
      var buffer = new byte[cnBlock.CGBlock.getRecordSize()];

      var offset = cnBlock.getReadPosition(recordIndex);
      if (!cnBlock.CGBlock.DGBlock.readData(offset, ref buffer))
        return double.NaN;

      return toSingleValue(cnBlock, format, ref buffer);
    }

    /// <summary>
    /// Gets data for the specified signals in double precision (a set of DataPointF, float version).
    /// 
    /// - This method delivers float data points. 
    /// - This saves memory but results in a loss of data precision.
    /// - To get the full precision provided by a MDF file, use the getData method.
    /// </summary>
    /// <param name="format">The data format (supported is ValueObjectFormat.Physical and ValueObjectFormat.Raw in this case)</param>
    /// <param name="startTime">The start time to get data</param>
    /// <param name="endTime">The end time to get data</param>
    /// <param name="cnBlocks">The list of signals to get data for</param>
    /// <param name="maxPoints">
    /// - [in] The list of maximum count of points returnable in the points parameter
    /// - [out] The list of points returned in the points parameter
    /// </param>
    /// <param name="points">
    /// <para>[in/out]An IntPtr (as memory) for each channel to store the data into.
    /// The IntPtr should provide enough memory for DataPoint.Size (8 Bytes * maxPoints[n])</para>
    /// <para><b>Attention: The application memory gets corrupted if the provided size is too small to hold the requested size of DataPoints!</b></para>
    /// </param>
    /// <param name="limitsX">[out] Supplies limits in X-direction (time axis)</param>
    /// <param name="limitsY">[out] Supplies limits in Y-Direction (Value axis)</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown, if the arrays got a different size</exception>
    public bool getDataF(ValueObjectFormat format
      , double startTime, double endTime
      , ICNBLOCK[] cnBlocks
      , uint[] maxPoints
      , IntPtr[] points
      , out DataLimits[] limitsX
      , out DataLimits[] limitsY
      )
    {
      return getData(DataPointF.Size, format, startTime, endTime, cnBlocks, maxPoints, points, out limitsX, out limitsY);
    }
    /// <summary>
    /// Gets data for the specified signals in double precision (a set of DataPoint, double version).
    /// 
    /// - This method delivers double data points. 
    /// - Calling this method ensures to get the full precision provided by the MDF file.
    /// </summary>
    /// <param name="format">The data format (supported is ValueObjectFormat.Physical and ValueObjectFormat.Raw in this case)</param>
    /// <param name="startTime">The start time to get data</param>
    /// <param name="endTime">The end time to get data</param>
    /// <param name="cnBlocks">The list of signals to get data for</param>
    /// <param name="maxPoints">
    /// - [in] The list of maximum count of points returnable in the points parameter
    /// - [out] The list of points returned in the points parameter
    /// </param>
    /// <param name="points">
    /// <para>[in/out]An IntPtr (as memory) for each channel to store the data into.
    /// The IntPtr should provide enough memory for DataPoint.Size (16 Bytes * maxPoints[n])</para>
    /// <para><b>Attention: The application memory gets corrupted if the provided size is too small to hold the requested size of DataPoints!</b></para>
    /// </param>
    /// <param name="limitsX">[out] Supplies limits in X-direction (time axis)</param>
    /// <param name="limitsY">[out] Supplies limits in Y-Direction (Value axis)</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown, if the arrays got a different size</exception>
    public bool getData(ValueObjectFormat format
      , double startTime, double endTime
      , ICNBLOCK[] cnBlocks
      , uint[] maxPoints
      , IntPtr[] points
      , out DataLimits[] limitsX
      , out DataLimits[] limitsY
      )
    {
      return getData(DataPoint.Size, format, startTime, endTime, cnBlocks, maxPoints, points, out limitsX, out limitsY);
    }
    /// <summary>
    /// Gets all available annotations from the MDF File.
    /// </summary>
    /// <returns>The annotation array (may be empty)</returns>
    public Annotation[] getAnnotations()
    {
      var result = new List<Annotation>();
      var en = OpenChannels.GetEnumerator();
      while (en.MoveNext())
      {
        var cnBlock = en.Current;
        switch (cnBlock.SignalType)
        {
          case SignalType.STRING_UTF8:
          case SignalType.STRING_UTF16_LE:
          case SignalType.STRING_UTF16_BE:
          case SignalType.STRING:
            break;
          default:
            // not a string channel
            continue;
        }
        var time = getMasterChannel(cnBlock.CGBlock);
        if (time == null)
          // time channel not found
          continue;
        switch (cnBlock.ChannelType)
        {
          case ChannelType.Data:
            for (long i = 0; i < (long)cnBlock.CGBlock.RecordCount; ++i)
            {
              var str = readAnnotation(cnBlock, i);
              var ts = getData(ValueObjectFormat.Physical, time, i);
              if (double.IsNaN(ts))
                continue;
              result.Add(new Annotation(ts, str));
            }
            break;
          case ChannelType.VariableData:
#if old
            long j = 0;
            double timestamp;
            while(!double.IsNaN(timestamp=getData(ValueObjectFormat.Physical, time, j++)))
              { 
              var buffer = new byte[8];
              var position = cnBlock.getReadPosition(j++);

              var br = cnBlock.CGBlock.DGBlock.getReader(position);
              var bb = new BLOCKBASEV4(br);

              if (cnBlock.CGBlock.DGBlock.readData(position, ref buffer))
              {
                position = BitConverter.ToInt64(buffer, 0);
                cnBlock.CGBlock.DGBlock.readData(position,)
              }
              var offset = getData(ValueObjectFormat.Raw, cnBlock, 0);
              var position = mReader.BaseStream.Position;
              var bb = new BLOCKBASEV4(mReader);
              switch (bb.Id)
              {
                case "TX":
                  var str = new TXBLOCKV4(mReader, position).Text;
                  result.Add(new Annotation(timestamp, str));
                  break;
              }
              mReader.BaseStream.Position = position;
            }
#endif
            break;
        }
      }
      return result.ToArray();
    }
    /// <summary>
    /// Returns the count of sample points in the file.
    /// </summary>
    /// <returns>the count of sample points</returns>
    public UInt64 getSamplePointCount()
    {
      UInt64 result = 0;
      var en = OpenChannelGroups.GetEnumerator();
      while (en.MoveNext())
        result += (ulong)(en.Current.CNBlocks.Count) * en.Current.RecordCount;
      return result;
    }
    #endregion
    /// <summary>
    /// Gets data for the specified signal as raw data blocks (a set of RawDataRecord).
    /// </summary>
    /// <param name="startTime">The start time to get data</param>
    /// <param name="endTime">The end time to get data</param>
    /// <param name="cnBlock">The signals to get raw data for</param>
    /// <param name="data">[out] Data record.</param>
    /// <param name="limitsX">[out] Supplies limits in X-direction (time axis)</param>
    /// <returns>true if successful</returns>
    public bool getRawData(double startTime, double endTime
      , ICNBLOCK cnBlock
      , out IList<RawDataRecord> data
      , out DataLimits limitsX
      )
    {
      // Create border defaults
      data = null;
      limitsX = new DataLimits();
      var localData = new List<RawDataRecord>();

      Int64 startIdx, endIdx;
      var time = getMasterChannel(cnBlock.CGBlock);
      if (time == null)
        // no time channel found, may never occur
        return false;
      // search start and end index from time window
      findTimeIndex(time, startTime, endTime, out startIdx, out endIdx);

      // create record buffer
      var buffer = new byte[time.CGBlock.getRecordSize()];

      Int64 cnt = startIdx;
      while (cnt < (Int64)time.CGBlock.RecordCount && cnt <= endIdx)
      {
        // read complete record
        var offset = cnBlock.getReadPosition(cnt);
        if (!cnBlock.CGBlock.DGBlock.readData(offset, ref buffer))
          return false;

        var record = new RawDataRecord();

        for (int i = 0; i < 2; ++i)
        { // iterate over time and data channel
          var currChn = i == 0 ? time : cnBlock;

          if (i == 0)
          { // time channel
            double resultValue = toSingleValue(currChn, ValueObjectFormat.Physical, ref buffer);
            record.Timestamp = limitsX.Max = resultValue;
            if (cnt == 0)
              limitsX.Min = resultValue;
          }
          else
          { // data channel
            int byteOffset = (int)currChn.AddOffset + currChn.BitOffset / 8;
            var chnIdx = currChn.CGBlock.CNBlocks.IndexOf(currChn);
            var length = currChn.CGBlock.getRecordSize() - byteOffset;
            if (chnIdx + 1 < currChn.CGBlock.CNBlocks.Count - 1)
            {
              var nextChn = currChn.CGBlock.CNBlocks[chnIdx + 1];
              int nextByteOffset = (int)nextChn.AddOffset + nextChn.BitOffset / 8;
              length = Math.Max(1, nextByteOffset - byteOffset);
            }
            record.Data = new byte[length];
            Array.Copy(buffer, byteOffset, record.Data, 0, length);
            localData.Add(record);
          }
        }
        ++cnt;
      }
      data = localData.ToArray();
      return true;
    }

    /// <summary>
    /// Gets data for the specified signals in double precision (a set of DataPointF, float version).
    /// 
    /// - This method delivers float data points. 
    /// - This saves memory but results in a loss of data precision.
    /// - To get the full precision provided by a MDF file, use the getData method.
    /// </summary>
    /// <param name="dataSize">Size of the requested single data point (8 or 16)</param>
    /// <param name="format">The data format (supported is ValueObjectFormat.Physical and ValueObjectFormat.Raw in this case)</param>
    /// <param name="startTime">The start time to get data</param>
    /// <param name="endTime">The end time to get data</param>
    /// <param name="cnBlocks">The list of signals to get data for</param>
    /// <param name="maxPoints">
    /// - [in] The list of maximum count of points returnable in the points parameter
    /// - [out] The list of points returned in the points parameter
    /// </param>
    /// <param name="points">
    /// <para>[in/out]An IntPtr (as memory) for each channel to store the data into.
    /// The IntPtr should provide enough memory for DataPoint.Size (8 Bytes * maxPoints[n])</para>
    /// <para><b>Attention: The application memory gets corrupted if the provided size is too small to hold the requested size of DataPoints!</b></para>
    /// </param>
    /// <param name="limitsX">[out] Supplies limits in X-direction (time axis)</param>
    /// <param name="limitsY">[out] Supplies limits in Y-Direction (Value axis)</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown, if the arrays got a different size</exception>
    bool getData(int dataSize
        , ValueObjectFormat format
        , double startTime, double endTime
        , ICNBLOCK[] cnBlocks
        , uint[] maxPoints
        , IntPtr[] points
        , out DataLimits[] limitsX
        , out DataLimits[] limitsY
        )
    {
      if (dataSize != DataPoint.Size && dataSize != DataPointF.Size)
        throw new ArgumentException("dataSize size not supported");
      if (cnBlocks.Length != maxPoints.Length || cnBlocks.Length != points.Length)
        throw new ArgumentException("Array size not unique");

      // Create border defaults
      limitsX = new DataLimits[cnBlocks.Length];
      limitsY = new DataLimits[cnBlocks.Length];

      Int64 lastUsedStartIdx = -1, lastUsedEndIdx = -1;
      ICNBLOCK lastUsedTimeChannel = null;
      for (int k = 0; k < cnBlocks.Length; ++k)
      {
        double min = double.MaxValue, max = double.MinValue;
        var cnBlock = cnBlocks[k];
        ICNBLOCK time = null;
        Int64 startIdx, endIdx;
        if (lastUsedTimeChannel != null && lastUsedTimeChannel.CGBlock == cnBlock.CGBlock)
        { // same time channel, reuse data
          time = lastUsedTimeChannel;
          startIdx = lastUsedStartIdx;
          endIdx = lastUsedEndIdx;
        }
        else
        { // new time channel required
          time = getMasterChannel(cnBlock.CGBlock);
          if (time == null)
          { // no time channel found, may never occur
            maxPoints[k] = 0;
            return false;
          }
          // search start and end index from time window
          findTimeIndex(time, startTime, endTime, out startIdx, out endIdx);
        }

        // create record buffer
        var buffer = new byte[time.CGBlock.getRecordSize()];

        int startValue = 0;
        if (lastUsedTimeChannel == time)
        { // optimization: prevent from getting
          // the same time channel again, instead copy it
          limitsX[k] = limitsX[k - 1];
          Extensions.copyMemory(points[k], points[k - 1], (uint)(maxPoints[k - 1] * dataSize));
          startValue = 1;
        }

        Int64 cnt = startIdx, currCnt = 0;
        while (cnt < (Int64)time.CGBlock.RecordCount && cnt <= endIdx && currCnt < maxPoints[k])
        {
          // read complete record
          var offset = cnBlock.getReadPosition(cnt);
          if (!cnBlock.CGBlock.DGBlock.readData(offset, ref buffer))
            return false;

          var targetDataPoint = new double[2];
          for (int i = startValue; i < 2; ++i)
          { // iterate over time and data channel
            var currChn = i == 0 ? time : cnBlock;

            if (i > 0 && currChn.SignalType >= SignalType.STRING)
              // filter strings and byte arrays
              break;

            double resultValue = toSingleValue(currChn, i == 0 ? ValueObjectFormat.Physical : format, ref buffer);

            if (i == 0)
            { // time channel, keep
              targetDataPoint[0] = limitsX[k].Max = resultValue;
              if (cnt == 0)
                limitsX[k].Min = resultValue;
            }
            else
            { // marshal to memory target
              targetDataPoint[1] = resultValue;
              max = Math.Max(max, resultValue);
              min = Math.Min(min, resultValue);
              IntPtr p = IntPtr.Add(points[k], (int)(currCnt * dataSize) + startValue * (dataSize / 2));
              switch (dataSize)
              {
                case 8:
                  var floatDataPoint = new float[]
                  {
                     Convert.ToSingle(targetDataPoint[0])
                    ,Convert.ToSingle(targetDataPoint[1])
                  };
                  Marshal.Copy(floatDataPoint, startValue, p, 2 - startValue);
                  break;
                case 16:
                  Marshal.Copy(targetDataPoint, startValue, p, 2 - startValue);
                  break;
              }
            }
          }
          ++cnt; ++currCnt;
        }
        maxPoints[k] = (uint)currCnt;
        limitsY[k] = new DataLimits(min, max);
        lastUsedTimeChannel = time;
        lastUsedStartIdx = startIdx;
        lastUsedEndIdx = endIdx;
      }
      return true;
    }

    /// <summary>
    /// Reads from the specified buffer and converts int a single data value.
    /// </summary>
    /// <param name="cnBlock">The channel block</param>
    /// <param name="format">The data format (supported is ValueObjectFormat.Physical and ValueObjectFormat.Raw in this case)</param>
    /// <param name="buffer">Buffer to read from</param>
    /// <returns>The value read</returns>
    double toSingleValue(ICNBLOCK cnBlock, ValueObjectFormat format, ref byte[] buffer)
    {
      double resultValue = double.NaN;
      bool adjustBO = adjustByteOrder(cnBlock.ChannelType == ChannelType.VirtualData ? SignalType.UINT_LE : cnBlock.SignalType);
      int byteOffset = (int)cnBlock.AddOffset + cnBlock.BitOffset / 8;
      int bitOffset = cnBlock.BitOffset % 8;
      var bitmask = UInt64.MaxValue;
      if (bitOffset > 0 || cnBlock.NoOfBits % 8 > 0)
      { // create the bitmask
        bitmask = 1;
        for (int j = 1; j < cnBlock.NoOfBits; ++j)
        {
          bitmask <<= 1;
          bitmask |= 1;
        }
      }


      if (cnBlock.NoOfBits <= 8)
      { // byte value
        byte value = buffer[byteOffset];
        // downshift and mask
        value >>= bitOffset;
        value &= (byte)bitmask;
        switch (cnBlock.SignalType)
        {
          case SignalType.UINT_BE:
          case SignalType.UINT_LE:
            resultValue = value;
            break;
          case SignalType.SINT_BE:
          case SignalType.SINT_LE:
            resultValue = (sbyte)value;
            break;
        }
      }
      else if (cnBlock.NoOfBits <= 16)
      { // 16 Bit value
        UInt16 value = BitConverter.ToUInt16(buffer, byteOffset);
        if (adjustBO)
          value = Extensions.changeEndianess(value);
        // downshift and mask
        value >>= bitOffset;
        value &= (UInt16)bitmask;
        switch (cnBlock.SignalType)
        {
          case SignalType.UINT_BE:
          case SignalType.UINT_LE:
            resultValue = value;
            break;
          case SignalType.SINT_BE:
          case SignalType.SINT_LE:
            resultValue = (Int16)value;
            break;
        }
      }
      else if (cnBlock.NoOfBits <= 32)
      { // 32 Bit value
        UInt32 value = BitConverter.ToUInt32(buffer, byteOffset);
        if (adjustBO)
          value = Extensions.changeEndianess(value);
        // downshift and mask
        value >>= bitOffset;
        value &= (UInt32)bitmask;
        switch (cnBlock.SignalType)
        {
          case SignalType.UINT_BE:
          case SignalType.UINT_LE:
            resultValue = value;
            break;
          case SignalType.SINT_BE:
          case SignalType.SINT_LE:
            resultValue = (Int32)value;
            break;
          case SignalType.FLOAT_BE:
          case SignalType.FLOAT_LE:
            resultValue = BitConverter.ToSingle(BitConverter.GetBytes(value), 0);
            break;
        }
      }
      else
      { // 64 Bit value
        UInt64 value = BitConverter.ToUInt64(buffer, byteOffset);
        if (adjustBO)
          value = Extensions.changeEndianess(value);
        // downshift and mask
        value >>= bitOffset;
        value &= bitmask;
        switch (cnBlock.SignalType)
        {
          case SignalType.UINT_BE:
          case SignalType.UINT_LE:
            resultValue = value;
            break;
          case SignalType.SINT_BE:
          case SignalType.SINT_LE:
            resultValue = (Int64)value;
            break;
          case SignalType.FLOAT_BE:
          case SignalType.FLOAT_LE:
            resultValue = BitConverter.ToDouble(BitConverter.GetBytes(value), 0);
            break;
        }
      }

      if (format == ValueObjectFormat.Physical)
        // compute to physical
        resultValue = computePhysical(cnBlock, resultValue);
      return resultValue;
    }

    /// <summary>
    /// Gets the physical from the raw value.
    /// </summary>
    /// <param name="cnBlock">The channel block</param>
    /// <param name="rawValue">The rawValue to convert</param>
    /// <returns>The physical value</returns>
    double computePhysical(ICNBLOCK cnBlock, double rawValue)
    {
      double result = rawValue;
      if (cnBlock.CCBlock != null)
        switch (cnBlock.CCBlock.ConversionType)
        {
          case ConversionType.TextFormula: result = mFormulaDict[cnBlock.Name].toPhysical(rawValue); break;
          default: result = cnBlock.CCBlock.toPhysical(rawValue); break;
        }
      return result;
    }

    /// <summary>
    /// Indicates if the byte order should be adjusted.
    /// 
    /// Depending on the current system and the specified byte order.
    /// </summary>
    /// <param name="bo">The Byte order</param>
    /// <returns>Indicates if the byte order should be adjusted.</returns>
    static bool adjustByteOrder(BYTEORDER_TYPE bo)
    {
      return bo == BYTEORDER_TYPE.MSB_FIRST && BitConverter.IsLittleEndian
        || bo == BYTEORDER_TYPE.MSB_LAST && !BitConverter.IsLittleEndian;
    }

    /// <summary>
    /// Indicates if the byte order should be adjusted.
    /// 
    /// Depending on the Signal type, the current system and the specified byte order.
    /// </summary>
    /// <param name="type">The signal type</param>
    /// <returns>Indicates if the byte order should be adjusted.</returns>
    static bool adjustByteOrder(SignalType type)
    {
      switch (type)
      {
        case SignalType.STRING:
        case SignalType.STRING_UTF8:
        case SignalType.MIME_SAMPLE:
        case SignalType.MIME_STREAM:
        case SignalType.CANOPEN_TIME:
        case SignalType.CANOPEN_DATA:
        case SignalType.BYTE_ARRAY: return false;

        case SignalType.STRING_UTF16_BE:
        case SignalType.FLOAT_BE:
        case SignalType.SINT_BE:
        case SignalType.UINT_BE: return adjustByteOrder(BYTEORDER_TYPE.MSB_FIRST);

        case SignalType.STRING_UTF16_LE:
        case SignalType.FLOAT_LE:
        case SignalType.SINT_LE:
        case SignalType.UINT_LE: return adjustByteOrder(BYTEORDER_TYPE.MSB_LAST);
        default: throw new NotSupportedException(type.ToString());
      }
    }

    /// <summary>
    /// Gets the one and only master channel from the list of channels.
    /// </summary>
    /// <param name="cgBlock">The Channel group to get the time channel for</param>
    /// <returns>The time channel or null if not found</returns>
    ICNBLOCK getMasterChannel(ICGBLOCK cgBlock)
    {
      var en = OpenChannels.GetEnumerator();
      while (en.MoveNext())
      { // iterate to search the corresponding time channel
        if (en.Current.CGBlock == cgBlock
          && (en.Current.ChannelType == ChannelType.Master || en.Current.ChannelType == ChannelType.VirtualMaster)
           )
          // corresponding time channel found
          return en.Current;
      }
      return null;
    }

    /// <summary>
    /// Finds the first valid index to be within the specified time.
    /// </summary>
    /// <param name="timeChn">The time channel to search</param>
    /// <param name="firstTime">The first timestamp</param>
    /// <param name="relTime">The relative timestamp to search</param>
    /// <returns>The first valid index to be read within the specified time</returns>
    Int64 findTimeIndex(ICNBLOCK timeChn, double firstTime, double relTime)
    {
      Int64 nLow = 0;
      Int64 nHigh = (Int64)timeChn.CGBlock.RecordCount;
      while (nLow < nHigh)
      {
        Int64 nMid = nLow + (nHigh - nLow) / 2; // workaround for sum overflow
        double compareTime = getData(ValueObjectFormat.Physical, timeChn, nMid);
        if (compareTime <= relTime + firstTime)
          nLow = nMid + 1;
        else
          nHigh = nMid;
      }
      return nLow > 0 ? nLow - 1 : 0;
    }

    /// <summary>
    /// Finds the first valid index to be within the specified start time.
    /// </summary>
    /// <param name="timeChn">The time channel to search</param>
    /// <param name="startTime">The (relative) start time in [s] to search for</param>
    /// <param name="endTime">The (relative) end time in [s] to search for</param>
    /// <param name="startIdx">The resulting start index</param>
    /// <param name="endIdx">The resulting end index</param>
    void findTimeIndex(ICNBLOCK timeChn
      , double startTime, double endTime
      , out Int64 startIdx, out Int64 endIdx
      )
    {
      endIdx = findTimeIndex(timeChn, 0, endTime);
      Int64 nLow = 0;
      Int64 nHigh = (Int64)endIdx + 1;
      while (nLow < nHigh)
      {
        Int64 nMid = nLow + (nHigh - nLow) / 2; // workaround for sum overflow
        double compareTime = getData(ValueObjectFormat.Physical, timeChn, nMid);
        if (compareTime <= startTime)
          nLow = nMid + 1;
        else
          nHigh = nMid;
      }
      startIdx = nLow > 0 ? nLow - 1 : 0;
    }

    /// <summary>
    /// Send parser messages to registered listeners.
    /// </summary>
    /// <param name="e">The parser event arguments</param>
    internal void onParserMessage(ParserEventArgs e)
    {
      switch (e.Type)
      {
        case MessageType.Error: ++CountErrors; break;
        case MessageType.Warning: ++CountWarnings; break;
      }
      ParserEvents.Add(e);
      ParserMessage?.Invoke(this, e);
    }

    /// <summary>
    /// Reads a specific annotation from the specified stream.
    /// </summary>
    /// <param name="cnBlock">channel block</param>
    /// <param name="recordIndex"></param>
    /// <returns>Annotation string</returns>
    static string readAnnotation(ICNBLOCK cnBlock, Int64 recordIndex)
    {
      var offset = cnBlock.getReadPosition(recordIndex);
      offset += cnBlock.AddOffset + cnBlock.BitOffset / 8;
      var buffer = new byte[cnBlock.NoOfBits / 8];
      if (cnBlock.CGBlock.DGBlock.readData(offset, ref buffer))
        return Helpers.getString(ref buffer, Encoding.UTF8);
      return string.Empty;
    }

    #region export to CSV
    /// <summary>
    /// Exports the specified channels into CSV format.
    /// </summary>
    /// <param name="targetPath">The destination path</param>
    /// <param name="masterDecimalPlaces">The count of decimal places to round the master axis values to</param>
    /// <param name="channels">The channels to filter to (may be null for exporting all channels)</param>
    /// <param name="progressHandler">Callback to receive progress, maybe null</param>
    /// <returns>true if successful</returns>
    public bool ToCSV(string targetPath, byte masterDecimalPlaces
      , List<ICNBLOCK> channels = null
      , EventHandler<ProgressArgs> progressHandler = null
      )
    {
      using (var fs = new FileStream(targetPath, FileMode.Create, FileAccess.Write, FileShare.None, Helpers.BUFFER_SIZE, FileOptions.WriteThrough))
      using (var sw = new StreamWriter(fs))
      {
        foreach (var cgBlock in OpenChannelGroups)
        {
          var channelSet = new HashSet<ICNBLOCK>();
          foreach (var entry in OpenChannels)
          {
            if (entry.ChannelType == ChannelType.Data && (channels != null && !channels.Contains(entry)))
              // channel is data channel and not requested to export
              continue;

            if (entry.CGBlock != cgBlock)
              // channel not in the current group
              continue;

            channelSet.Add(entry);
          }

          if (channelSet.Count < 2)
            // no channels in this channel group at all
            continue;

          // dictionary per channelgroup complete -> output values
          var sortedCNBlockList = new List<ICNBLOCK>(channelSet);
          sortedCNBlockList.Sort(Helpers.sortByMasterThenName);

          var sb = new StringBuilder();
          foreach (var cnBlock in sortedCNBlockList)
            sb.AppendFormat("\"{0}[{1}]\";", cnBlock.Name, cnBlock.Unit);
          sb.Remove(sb.Length - 1, 1);
          sw.WriteLine(sb);

          var cgBlockMaster = sortedCNBlockList[0].CGBlock;
          if (cgBlockMaster.RecordCount == 0)
            // no data available
            continue;

          // read values
          Int64 maxRecords = (Int64)cgBlockMaster.RecordCount, i = 0;
          do
          {
            sb.Clear();
            foreach (var cnBlock in sortedCNBlockList)
            {
              double value;
              switch (cnBlock.ChannelType)
              {
                case ChannelType.Master:
                  value = getData(ValueObjectFormat.Physical, cnBlock, i);
                  if (double.IsNaN(value))
                    return false;
                  value = Math.Round(value, masterDecimalPlaces);
                  break;
                default:
                  switch (cnBlock.SignalType)
                  {
                    case SignalType.STRING:
                      sb.AppendFormat("\"{0}\";", readAnnotation(cnBlock, i));
                      continue;
                    default:
                      value = getData(ValueObjectFormat.Physical, cnBlock, i);
                      if (double.IsNaN(value))
                        return false;
                      break;
                  }
                  break;
              }
              sb.AppendFormat(CultureInfo.InvariantCulture, "{0};", value);
            }
            sb.Remove(sb.Length - 1, 1);
            sw.WriteLine(sb);
          } while (++i < maxRecords);
        }
        return true;
      }
    }
    #endregion

    public override string ToString()
    {
      return SourceFile != null ? Path.GetFileName(SourceFile) : string.Empty;
    }

    /// <summary>
    /// Frees the opened stream.
    /// </summary>
    public void Dispose()
    {
      if (mDR != null)
        mDR.Dispose();
      if (Header is IDisposable)
        ((IDisposable)Header).Dispose();
    }
    #endregion
  }
  /// <summary>
  /// Used to transfer MDF raw data record.
  /// </summary>
  public class RawDataRecord
  {
    /// <summary>
    /// The timestamp (in physical format).
    /// </summary>
    public double Timestamp;
    /// <summary>
    /// The raw data.
    /// </summary>
    public byte[] Data;
  }
}
