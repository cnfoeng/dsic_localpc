﻿/*!
 * @file    
 * @brief   Implements the MDF writer.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2;
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using jnsoft.MDF.V3;
using jnsoft.MDF.V4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace jnsoft.MDF
{
  /// <summary>
  /// Implements the MDF (Measurement Data Format) recorder.
  /// </summary>
  public sealed partial class MDFWriter : IDisposable
  {
    #region types
    sealed class WriteHandle
    {
      public ICGBLOCK CGBlock;
      public double LastTimestamp;
      public WriteHandle(ICGBLOCK cgBlock) { CGBlock = cgBlock; LastTimestamp = double.NaN; }
    }
    #endregion
    #region members
    const int ANNOTATION_HANDLE = int.MinValue;
    Dictionary<int, WriteHandle> mHandleMap = new Dictionary<int, WriteHandle>();
    Dictionary<IDGBLOCK, List<SingleDataStream>> mDGBlockData = new Dictionary<IDGBLOCK, List<SingleDataStream>>();
    AnnotationList mAnnotationList;
    int mNextHandle = ANNOTATION_HANDLE;
    IIDBLOCK mIDBlock;
    IHDBLOCK mHDBlock;
    bool mDataReceived;
    volatile bool mClosed, mStopped;
    double mFirstTimestamp = double.NaN;
    #endregion
    #region ctor
    /// <summary>
    /// Creates a new instance of MDFWriter with the specified arguments.
    /// </summary>
    /// <param name="author">The author (32 bytes supported)</param>
    /// <param name="organization">The organization (32 bytes supported)</param>
    /// <param name="project">The project (32 bytes supported)</param>
    /// <param name="subject">The subject (32 bytes supported)</param>
    /// <param name="headerComment">The comment on the header block</param>
    /// <param name="type">MDF format used to write</param>
    /// <param name="compress">true to compress data (only supported with type = MDFType.V4)</param>
    public MDFWriter(string author, string organization, string project, string subject, string headerComment
      , MDFType type = MDFType.V4, bool compress = false
      )
    {
      if (type < MDFType.V4 && compress)
        throw new ArgumentException("Compressing data is only supported by the V4 format");

      Type = type;
      Compress = compress;
      switch (Type)
      {
        case MDFType.V3:
          mIDBlock = new IDBLOCK("ASAP2Lib", (UInt16)Encoding.Default.CodePage);
          mHDBlock = new HDBLOCK(DateTime.Now, TimeQualityType.LocalPC, author, organization, project, subject, headerComment);
          break;
        case MDFType.V4:
          mIDBlock = new IDBLOCKV4("ASAP2Lib");
          mHDBlock = new HDBLOCKV4(DateTime.Now, TimeQualityType.LocalPC
            , author
            , string.Format(Resources.strMDFHistoryFmt, author, organization, project, subject)
            , headerComment
            );
          break;
        default: throw new NotSupportedException(Type.ToString());
      }
    }
    #endregion
    #region properties
    /// <summary>Gets the MDF format to write the measurement data to.</summary>
    public MDFType Type { get; }
    /// <summary>Gets if the the measurement data </summary>
    public bool Compress { get; }
    #endregion
    #region methods
    /// <summary>
    /// Gets the signal size of a measurement.
    /// </summary>
    /// <param name="convRef"></param>
    /// <param name="signalType"></param>
    /// <param name="addOffset">offset in [bit]</param>
    /// <param name="bitSize">Size in [bit]</param>
    static void getSignalSize(A2LCONVERSION_REF convRef, out SignalType signalType, out UInt32 addOffset, out UInt32 bitSize)
    {
      if (!(convRef is A2LMEASUREMENT))
      {
        bitSize = (UInt32)(((A2LRECORD_LAYOUT_REF)convRef).getSize() * 8);
        addOffset = 0;
        signalType = SignalType.BYTE_ARRAY;
        return;
      }

      var measurement = (A2LMEASUREMENT)convRef;
      int maskOffset;
      bitSize = (UInt32)measurement.getBitCount(out maskOffset);
      addOffset = (UInt32)maskOffset;
      switch (measurement.ByteOrder)
      {
        case BYTEORDER_TYPE.MSB_FIRST: // Big Endian
          switch (measurement.DataType)
          {
            case DATA_TYPE.A_UINT64:
            case DATA_TYPE.ULONG:
            case DATA_TYPE.UWORD:
            case DATA_TYPE.UBYTE: signalType = SignalType.UINT_BE; break;

            case DATA_TYPE.A_INT64:
            case DATA_TYPE.SLONG:
            case DATA_TYPE.SWORD:
            case DATA_TYPE.SBYTE: signalType = SignalType.SINT_BE; break;

            case DATA_TYPE.FLOAT32_IEEE:
            case DATA_TYPE.FLOAT64_IEEE: signalType = SignalType.FLOAT_BE; break;

            default: throw new NotSupportedException(measurement.DataType.ToString());
          }
          break;

        case BYTEORDER_TYPE.MSB_LAST:
          switch (measurement.DataType)
          {
            case DATA_TYPE.A_UINT64:
            case DATA_TYPE.ULONG:
            case DATA_TYPE.UWORD:
            case DATA_TYPE.UBYTE: signalType = SignalType.UINT_LE; break;

            case DATA_TYPE.A_INT64:
            case DATA_TYPE.SLONG:
            case DATA_TYPE.SWORD:
            case DATA_TYPE.SBYTE: signalType = SignalType.SINT_LE; break;

            case DATA_TYPE.FLOAT32_IEEE:
            case DATA_TYPE.FLOAT64_IEEE: signalType = SignalType.FLOAT_LE; break;

            default: throw new NotSupportedException(measurement.DataType.ToString());
          }
          break;
        default:
          throw new NotSupportedException(measurement.ByteOrder.ToString());
      }
    }

    /// <summary>
    /// Creates the time channel.
    /// </summary>
    /// <param name="type">MDF type</param>
    /// <returns>time channel channel block instance</returns>
    static ICNBLOCK createTimeChannel(MDFType type)
    {
      const string TIME_CHANNEL_NAME = "time";
      const string TIME_CHANNEL_DESC = "timestamp channel";
      var signalType = BitConverter.IsLittleEndian ? SignalType.FLOAT_LE : SignalType.FLOAT_BE;
      var ccBlock = type == MDFType.V4
        ? (ICCBLOCK)new CCBLOCKV4(ConversionType.None, "s")
        : new CCBLOCK(ConversionType.None, "s");
      return type == MDFType.V4
        ? (ICNBLOCK)new CNBLOCKV4(signalType, ChannelType.Master, TIME_CHANNEL_NAME, TIME_CHANNEL_DESC, 0, 0, 64, ccBlock, SyncType.Time)
        : new CNBLOCK(signalType, ChannelType.Master, TIME_CHANNEL_NAME, TIME_CHANNEL_DESC, 0, 0, 64, ccBlock);
    }

    /// <summary>
    /// Creates a new Data- and Channel Group, a time channel and adds the specified raw data size to them.
    /// </summary>
    /// <param name="cgComment">The comment on the channel group (may be null or empty)</param>
    /// <param name="recordSize">The size of data to write with a single addDataEntry call</param>
    /// <param name="name">The raw data block's name</param>
    /// <param name="description">The raw data block's description</param>
    /// <param name="signalType">The raw data block's signal type</param>
    /// <returns>The handle to use in the addDataEntry method</returns>
    /// <exception cref="ArgumentException">Thrown if the allowed record size is exceeded</exception>>
    public int addRawData(string cgComment, uint recordSize
      , string name
      , string description = null
      , SignalType signalType = SignalType.BYTE_ARRAY
      )
    {
      if (mStopped)
        // recording file is closed
        return -1;

      if (recordSize > UInt16.MaxValue && Type == MDFType.V3)
        throw new ArgumentException($"Signalsize exceeded, {Type} allows a maximum recordsize of {(UInt16.MaxValue + 1 / 8) - 1} bytes.");

      IDGBLOCK dgBlock = Type == MDFType.V4
        ? (IDGBLOCK)new DGBLOCKV4()
        : new DGBLOCK();

      ICGBLOCK cgBlock = Type == MDFType.V4
        ? (ICGBLOCK)new CGBLOCKV4(dgBlock, cgComment)
        : new CGBLOCK(dgBlock, cgComment);

      // add a fixed time channel
      cgBlock.CNBlocks.Add(createTimeChannel(Type));
      cgBlock.RecordSize += 8;

      var bitOffset = (UInt32)(cgBlock.getRecordSize() * 8);

      var bitSize = (UInt32)(recordSize * 8);

      var cnBlock = Type == MDFType.V4
        ? (ICNBLOCK)new CNBLOCKV4(signalType, ChannelType.Data, name, description, bitOffset, 0, bitSize)
        : new CNBLOCK(signalType, ChannelType.Data, name, description, bitOffset, 0, bitSize, null);
      cgBlock.CNBlocks.Add(cnBlock);
      cgBlock.RecordSize += recordSize;
      dgBlock.CGBlocks.Add(cgBlock);
      mHDBlock.DGBlocks.Add(dgBlock);

      dgBlock.OnWriteRawData += dgBlock_OnWriteRawData;
      var zipType = Compress ? ZipType.TransposeAndDeflate : ZipType.None;
      var zipParmeter = zipType == ZipType.TransposeAndDeflate ? (uint)dgBlock.CGBlocks[0].getRecordSize() : 0;
      mDGBlockData[dgBlock] = new List<SingleDataStream>(new SingleDataStream[] { new SingleDataStream(zipType, zipParmeter) });

      mHandleMap[++mNextHandle] = new WriteHandle(cgBlock);
      return mNextHandle;
    }

    /// <summary>
    /// Adds source information to the specified channel group.
    /// 
    /// This method is only supported by MDF V4.
    /// </summary>
    /// <param name="handle">The channel group handle, refers to the handle given in addMeasurements or returned by addRawData</param>
    /// <param name="source">The source type</param>
    /// <param name="bus">The bus type</param>
    /// <param name="name">The acquisition source name</param>
    /// <param name="path">The acquisition source path</param>
    /// <param name="comment">The acquisition source comment</param>
    /// <param name="flags">The acquisition source flags</param>
    /// <exception cref="ArgumentException">Thrown, if type is not V4 or the specified handle is not found</exception>
    public void addSourceInformation(int handle
      , SourceType source
      , BusType bus = BusType.NONE
      , string name = null, string path = null, string comment = null
      , SourceFlags flags = SourceFlags.None
      )
    {
      if (Type == MDFType.V3)
        throw new ArgumentException($"Method not supported for {Type}");
      int localHandle = handle >= 0 ? handle + 1 : handle;
      WriteHandle writeHandle;
      if (!mHandleMap.TryGetValue(localHandle, out writeHandle))
        // handle not found, missing call to addMeasurements before
        throw new ArgumentException($"Handle {handle} not found");
      ((CGBLOCKV4)writeHandle.CGBlock).AcquisitionSource = new SIBLOCKV4(source, bus, name, path, comment, flags);
    }

    /// <summary>
    /// Adds the specified A2L measurements and/or characteristics to the MDF writer instance.
    /// 
    /// Creates a new Data- and Channel Group, a time channel and adds the specified channels to them.
    /// Important: The list of measurement channels requires to be in the same order as 
    /// the data in addDataEntry gets added!
    /// </summary>
    /// <param name="daqListNo">A unique id (usually the DAQ list number)</param>
    /// <param name="convRefs">The measurements/characteristics to add</param>
    /// <param name="arrayIndexBase">Index base for name suffix on measurement arrays</param>
    /// <exception cref="ArgumentException">Thrown if the MDF limits are exceeded or the specified daqListNo is already existing</exception>>
    public void addMeasurements(int daqListNo, IEnumerable<A2LCONVERSION_REF> convRefs, uint arrayIndexBase = 0)
    {
      if (mStopped)
        // recording file is closed
        return;

      int localHandle = daqListNo + 1;
      if (mHandleMap.ContainsKey(localHandle))
        throw new ArgumentException($"Handle {daqListNo} is already existing.");

      // create DGBlock
      var dgBlock = Type == MDFType.V4
        ? (IDGBLOCK)new DGBLOCKV4()
        : new DGBLOCK();

      // create CGBlock
      var cgBlock = Type == MDFType.V4
        ? (ICGBLOCK)new CGBLOCKV4(dgBlock)
        : new CGBLOCK(dgBlock);
      dgBlock.CGBlocks.Add(cgBlock);
      mHDBlock.DGBlocks.Add(dgBlock);

      // add a fixed time channel
      cgBlock.CNBlocks.Add(createTimeChannel(Type));
      cgBlock.RecordSize += 8;

      long recordSize = cgBlock.getRecordSize();
      // add data channels
      foreach (var convRef in convRefs)
      {
        ICCBLOCK ccBlock = null;
        SignalType signalType; UInt32 bitSize, addOffset;
        getSignalSize(convRef, out signalType, out addOffset, out bitSize);

        if (recordSize > UInt16.MaxValue && Type == MDFType.V3)
          throw new ArgumentException($"Signalsize exceeded, {Type} allows a maximum recordsize of {(UInt16.MaxValue + 1 / 8) - 1} bytes.");

        if (convRef is A2LMEASUREMENT)
        {
          var compuMethod = convRef.RefCompuMethod;

          switch (compuMethod.ConversionType)
          {
            case CONVERSION_TYPE.IDENTICAL:
              ccBlock = Type == MDFType.V4
                ? (ICCBLOCK)new CCBLOCKV4(ConversionType.None, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                : new CCBLOCK(ConversionType.None, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
              break;

            case CONVERSION_TYPE.LINEAR:
              ccBlock = Type == MDFType.V4
                ? (ICCBLOCK)new CCBLOCKV4(ConversionType.ParametricLinear, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                : new CCBLOCK(ConversionType.ParametricLinear, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
              ccBlock.Params = new double[] { compuMethod.Coeffs.c, compuMethod.Coeffs.b };
              break;

            case CONVERSION_TYPE.RAT_FUNC:
              ccBlock = Type == MDFType.V4
                ? (ICCBLOCK)new CCBLOCKV4(ConversionType.Rational, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                : new CCBLOCK(ConversionType.Rational, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
              ccBlock.Params = new double[] { compuMethod.Coeffs.a, compuMethod.Coeffs.b, compuMethod.Coeffs.c, compuMethod.Coeffs.d, compuMethod.Coeffs.e, compuMethod.Coeffs.f };
              for (int i = 0; i < ccBlock.Params.Length; ++i)
              {
                if (ccBlock.Params[i] == 0)
                  continue;
                // MDF specifies to compute with inversed parameters
                ccBlock.Params[i] = i == 2 ? -ccBlock.Params[i] : 1.0 / ccBlock.Params[i];
              }
              break;

            case CONVERSION_TYPE.FORM:
              ccBlock = Type == MDFType.V4
                ? (ICCBLOCK)new CCBLOCKV4(ConversionType.TextFormula, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                : new CCBLOCK(ConversionType.TextFormula, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
              var formula = compuMethod.getNode<A2LFORMULA>(false);
              ccBlock.Formula = formula.Formula;
              break;

            case CONVERSION_TYPE.TAB_VERB:
              var range = compuMethod.RefCompuTab as A2LCOMPU_VTAB_RANGE;
              if (range != null)
              { // tab verb range
                ccBlock = Type == MDFType.V4
                  ? (ICCBLOCK)new CCBLOCKV4(ConversionType.TextRange, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                  : new CCBLOCK(ConversionType.TextRange, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
                ccBlock.DefaultText = compuMethod.RefCompuTab.DefaultValue;
                var verbRange = new Dictionary<object, object>();
                foreach (var pair in ((A2LCOMPU_VTAB_RANGE)compuMethod.RefCompuTab).Verbs)
                  ccBlock.KeyValuePairs[pair.Key] = pair.Value;
              }
              else
              { // tab verb
                ccBlock = Type == MDFType.V4
                  ? (ICCBLOCK)new CCBLOCKV4(ConversionType.TextTable, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                  : new CCBLOCK(ConversionType.TextTable, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);
                foreach (var pair in ((A2LCOMPU_VTAB)compuMethod.RefCompuTab).Verbs)
                  ccBlock.KeyValuePairs[Convert.ToDouble(pair.Key)] = pair.Value;
              }
              break;

            case CONVERSION_TYPE.TAB_INTP:
            case CONVERSION_TYPE.TAB_NOINTP:
              // numeric tab (interpolation or no interpolation)
              var conversion = compuMethod.ConversionType == CONVERSION_TYPE.TAB_INTP
                ? ConversionType.TabInt
                : ConversionType.Tab;

              ccBlock = Type == MDFType.V4
                ? (ICCBLOCK)new CCBLOCKV4(conversion, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit)
                : new CCBLOCK(conversion, convRef.Unit, convRef.LowerLimit, convRef.UpperLimit);

              foreach (var pair in ((A2LCOMPU_TAB)compuMethod.RefCompuTab).Values)
                ccBlock.KeyValuePairs[Convert.ToDouble(pair.Key)] = pair.Value;

              break;
          }
        }

        var count = (convRef is A2LMEASUREMENT) ? ((A2LMEASUREMENT)convRef).getArraySize() : 1;
        string name = convRef.Name;

        for (int i = 0; i < count; ++i)
        {
          var byteSize = Math.Max(1, bitSize / 8);
          if (count > 1)
            name = $"{convRef.Name}[{(i + arrayIndexBase)}]";

          var cnBlock = Type == MDFType.V4
            ? (ICNBLOCK)new CNBLOCKV4(signalType, ChannelType.Data, name, convRef.Description, (UInt32)(recordSize * 8), addOffset, bitSize, ccBlock)
            : new CNBLOCK(signalType, ChannelType.Data, name, convRef.Description, (UInt32)(recordSize * 8), addOffset, bitSize, ccBlock);
          cgBlock.CNBlocks.Add(cnBlock);

          recordSize += byteSize;
          cgBlock.RecordSize += byteSize;

          if (recordSize > UInt16.MaxValue && Type == MDFType.V3)
            throw new ArgumentException($"Channelgroup record size exceeded, {Type} allows a maximum recordsize of {UInt16.MaxValue} bytes.");
        }
      }

      dgBlock.OnWriteRawData += dgBlock_OnWriteRawData;
      var zipType = Compress ? ZipType.TransposeAndDeflate : ZipType.None;
      var zipParmeter = zipType == ZipType.TransposeAndDeflate ? (uint)dgBlock.CGBlocks[0].getRecordSize() : 0;
      mDGBlockData[dgBlock] = new List<SingleDataStream>(new SingleDataStream[] { new SingleDataStream(zipType, zipParmeter) });

      mHandleMap[localHandle] = new WriteHandle(cgBlock);
    }

    /// <summary>
    /// Get the corresponding CGBLOCK to the specified 
    /// </summary>
    /// <param name="handle">The channel group handle, refers to the handle given in addMeasurements or returned by addRawData</param>
    /// <returns>A ICGBLOCK Instance or null if not found</returns>
    public ICGBLOCK this[int handle]
    {
      get
      {
        WriteHandle writeHandle;
        int localHandle = handle >= 0 ? handle + 1 : handle;
        return mHandleMap.TryGetValue(localHandle, out writeHandle)
          ? writeHandle.CGBlock
          : null;
      }
    }

    /// <summary>
    /// Ensures that written timestamps are always ascending.
    /// </summary>
    /// <param name="timestamp">The timestamp in (recording relative) seconds</param>
    /// <param name="preventAnno">true if an annotation shouldn't be added</param>
    /// <returns>The resulting timestamp</returns>
    double getTimeStamp(double timestamp, double lastTimestamp, bool preventAnno = false)
    {
      if (double.IsNaN(mFirstTimestamp))
        // remember the start
        mFirstTimestamp = timestamp;

      var tsToWrite = timestamp - mFirstTimestamp;

      bool invalidTs = false;
      if (!preventAnno && !double.IsNaN(lastTimestamp) && tsToWrite < lastTimestamp)
      { // timestamp is invalid (smaller than previous)
        mFirstTimestamp = lastTimestamp;
        tsToWrite = timestamp - mFirstTimestamp;
        invalidTs = true;
      }

      lastTimestamp = tsToWrite;
      if (invalidTs)
        addAnnotation(tsToWrite, Resources.strMDFInvalidTSDesc);
      return tsToWrite;
    }

    /// <summary>
    /// Adds data to the specified channel group.
    /// </summary>
    /// <param name="handle">The channel group handle, refers to the handle given in addMeasurements or returned by addRawData</param>
    /// <param name="timestamp">The timestamp in (recording relative) seconds</param>
    /// <param name="data">The raw data</param>
    /// <returns>true if successful</returns>
    public bool addDataEntry(int handle, double timestamp, ref byte[] data)
    {
      if (mStopped)
        // recording file is closed or handle invalid
        return false;

      int localHandle = handle >= 0 ? handle + 1 : handle;
      WriteHandle writeHandle;
      if (!mHandleMap.TryGetValue(localHandle, out writeHandle))
        // handle not found, missing call to addMeasurements before
        return false;
      ICGBLOCK cgBlock = writeHandle.CGBlock;

      if (Type == MDFType.V3 && cgBlock.RecordCount == UInt32.MaxValue || cgBlock.RecordCount == UInt64.MaxValue)
        // no more space available
        return false;

      if (cgBlock.RecordSize - 8 > data.Length)
        // insufficient data
        return false;

      var dataStreams = mDGBlockData[cgBlock.DGBlock];
      var activeStream = dataStreams[dataStreams.Count - 1];

      writeHandle.LastTimestamp = getTimeStamp(timestamp, writeHandle.LastTimestamp);
      activeStream.addData(writeHandle.LastTimestamp, ref data, (int)(cgBlock.RecordSize - 8));

      // update time channel
      if (cgBlock.RecordCount == 0)
        cgBlock.CNBlocks[0].CCBlock.Min = writeHandle.LastTimestamp;
      cgBlock.CNBlocks[0].CCBlock.Max = writeHandle.LastTimestamp;

      if (activeStream.ZipType != ZipType.None && activeStream.Size > DataReader.MAX_UNCOMPRESSED - cgBlock.RecordSize)
      { // create next stream in compressed mode
        activeStream.close(true);
        dataStreams.Add(new SingleDataStream(activeStream));
      }

      ++cgBlock.RecordCount;
      mDataReceived = true;

      return true;
    }

    /// <summary>
    /// Adds an annotation with the specified timestamp.
    /// </summary>
    /// <param name="timestamp">The timestamp in (relative) seconds</param>
    /// <param name="text">The annotation text</param>
    public void addAnnotation(double timestamp, string text)
    {
      if (mStopped)
        return;
      const string strAnnotations = "Annotations";
      const string strAnno = "Annotation";
      ICGBLOCK cgBlock = null;
      WriteHandle writeHandle;
      if (!mHandleMap.ContainsKey(ANNOTATION_HANDLE))
      { // create datagroup, channelgroup and channel for annotations
        mAnnotationList = new AnnotationList();
        var dgBlock = Type == MDFType.V4
          ? (IDGBLOCK)new DGBLOCKV4()
          : new DGBLOCK();
        dgBlock.OnWriteRawData += dgBlock_OnWriteRawData;
        cgBlock = Type == MDFType.V4
          ? (ICGBLOCK)new CGBLOCKV4(dgBlock, strAnnotations)
          : new CGBLOCK(dgBlock, strAnnotations);
        dgBlock.CGBlocks.Add(cgBlock);
        mHDBlock.DGBlocks.Insert(0, dgBlock);

        // add time channel
        cgBlock.CNBlocks.Add(createTimeChannel(Type));

        // add string channel
        UInt16 bitOffset = 64;
        var cnBlock = Type == MDFType.V4
          ? (ICNBLOCK)new CNBLOCKV4(SignalType.STRING, ChannelType.Data, strAnno, string.Empty, bitOffset, 0, 0)
          : new CNBLOCK(SignalType.STRING, ChannelType.Data, strAnno, string.Empty, bitOffset, 0, 0, new CCBLOCK(ConversionType.None, string.Empty));
        cgBlock.CNBlocks.Add(cnBlock);
        mHandleMap[ANNOTATION_HANDLE] = writeHandle = new WriteHandle(cgBlock);
      }
      else
      {
        writeHandle = mHandleMap[ANNOTATION_HANDLE];
        cgBlock = writeHandle.CGBlock;
      }

      ++writeHandle.CGBlock.RecordCount;

      writeHandle.LastTimestamp = getTimeStamp(timestamp, writeHandle.LastTimestamp, true);
      mAnnotationList.Add(new Annotation(writeHandle.LastTimestamp, text));

      // update time channel
      if (mAnnotationList.Count == 1)
        cgBlock.CNBlocks[0].CCBlock.Min = writeHandle.LastTimestamp;
      cgBlock.CNBlocks[0].CCBlock.Max = writeHandle.LastTimestamp;

      // Track always the maximum string length
      var stringCNBlock = cgBlock.CNBlocks[1];
      stringCNBlock.NoOfBits = (UInt32)mAnnotationList.MaxLength * 8;
      cgBlock.RecordSize = 8 + stringCNBlock.NoOfBits / 8;
    }

    /// <summary>
    /// Write the MDF V4 Version of Raw Data.
    /// </summary>
    /// <param name="e"></param>
    void writeV4RawData(WriteRawDataEventArgs e)
    {
      List<SingleDataStream> rawDataStreams;
      if (mDGBlockData.TryGetValue((DGBLOCKV4)e.Block, out rawDataStreams))
      { // measured data
        var dataLinks = new List<Int64>();
        Int64 equalSize = 0;
        var zipType = ZipType.None;
        DLBLOCKV4 dlBlock = null;
        HLBLOCKV4 hlBlock = null;
        var dlPos = e.Bw.BaseStream.Position;

        foreach (var stream in rawDataStreams)
        {
          stream.close();

          while (!stream.Done)
            Thread.Sleep(10);

          if (dlBlock == null && rawDataStreams.Count > 1)
          { // first of mutliple streams
            dlBlock = new DLBLOCKV4(DataBlockFlags.EqualLength, rawDataStreams.Count);
            hlBlock = new V4.HLBLOCKV4(stream.ZipType, DataBlockFlags.EqualLength);
            e.Bw.BaseStream.Position += dlBlock.BlockSize + hlBlock.BlockSize;
            equalSize = stream.UncompressedSize;
            zipType = stream.ZipType;
          }

          using (var dataFs = new FileStream(stream.Filename, FileMode.Open, FileAccess.Read, FileShare.None, Helpers.BUFFER_SIZE, FileOptions.SequentialScan))
          {
            BLOCKBASEV4 dataBlock = null;
            if (stream.ZipType == ZipType.None)
              // create a standard data block
              dataBlock = new DTBLOCKV4(stream.UncompressedSize);
            else
              // create a zipped data block
              dataBlock = new DZBLOCKV4((UInt64)dataFs.Length, stream.UncompressedSize, stream.ZipType, stream.ZipParameter);

            var link = dataBlock.writeBlock(e.Bw);
            dataLinks.Add(link);
            dataFs.CopyTo(e.Bw.BaseStream, Helpers.BUFFER_SIZE);
          }
          stream.Dispose();

          while (e.Bw.BaseStream.Position % 8 != 0)
            e.Bw.BaseStream.Position++;
        }

        if (rawDataStreams.Count > 1)
        { // add structure for multiple streams (HLBLOCKV4, DLBLOCKV4)
          var links = dataLinks.ToArray();
          dlBlock.addDataLinksBeforeWrite((UInt64)equalSize, ref links);
          var endPos = e.Bw.BaseStream.Position;
          e.Bw.BaseStream.Position = dlPos;
          hlBlock.writeBlock(e.Bw);
          dlBlock.writeBlock(e.Bw);
          e.Bw.BaseStream.Position = endPos;
        }
      }
      else if (mAnnotationList != null)
      { // annotation data
        var dtBlock = new DTBLOCKV4(mAnnotationList.Count * (mAnnotationList.MaxLength + 8));
        dtBlock.writeBlock(e.Bw);
        mAnnotationList.writeAll(e.Bw);
        while (e.Bw.BaseStream.Position % 8 != 0)
          e.Bw.BaseStream.Position++;
      }
    }

    /// <summary>
    /// Write the MDF V3 Version of Raw Data.
    /// </summary>
    /// <param name="e"></param>
    void writeV3RawData(WriteRawDataEventArgs e)
    {
      var dgBlock = (DGBLOCK)e.Block;
      List<SingleDataStream> rawDataStreams;
      if (mDGBlockData.TryGetValue(dgBlock, out rawDataStreams))
      { // measured data
        var activeDataStream = rawDataStreams[rawDataStreams.Count - 1];
        activeDataStream.close();
        using (var dataFs = new FileStream(activeDataStream.Filename, FileMode.Open, FileAccess.Read, FileShare.None, Helpers.BUFFER_SIZE, FileOptions.SequentialScan))
          dataFs.CopyTo(e.Bw.BaseStream, Helpers.BUFFER_SIZE);
        activeDataStream.Dispose();
      }
      else if (mAnnotationList != null)
      { // annotation data
        mAnnotationList.writeAll(e.Bw);
      }
    }

    /// <summary>
    /// Called when a DGBLOCK requests to write the raw data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void dgBlock_OnWriteRawData(object sender, WriteRawDataEventArgs e)
    {
      switch (Type)
      {
        case MDFType.V4:
          writeV4RawData(e);
          break;

        case MDFType.V3:
          writeV3RawData(e);
          break;

        default: throw new NotSupportedException(Type.ToString());
      }
    }

    /// <summary>
    /// Saves the MDF file structure to the specified file path.
    /// </summary>
    /// <param name="path">The file path to store</param>
    /// <returns>true if measured data is stored</returns>
    public bool save(string path)
    {
      if (mClosed || !mDataReceived)
        return true;
      mStopped = true;
      try
      {

        using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, Helpers.BUFFER_SIZE, FileOptions.RandomAccess))
        {
          using (var bw = new BinaryWriter(fs))
          { // write ID Block
            mIDBlock.writeBlock(bw);
            mHDBlock.writeBlock(bw);
          }
        }
        mClosed = true;
        return true;
      }
      catch { return false; }
    }

    /// <summary>
    /// Frees used resources.
    /// </summary>
    public void Dispose()
    {
      mStopped = mClosed = true;
      foreach (var streams in mDGBlockData.Values)
      {
        foreach (var stream in streams)
          stream.Dispose();
      }
      mDGBlockData.Clear();
      mHandleMap.Clear();
    }
    #endregion
  }
}