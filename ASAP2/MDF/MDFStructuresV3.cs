﻿/*!
 * @file    
 * @brief   Implements the MDF V3 Structures.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;

/// <summary>
/// Implements the MDF V1.XX..V3.XX specific stuff.
/// </summary>
namespace jnsoft.MDF.V3
{
  /// <summary>
  /// Extension type identifier
  /// 
  /// Note:
  /// - for CAN signals, ExtensionType.VectorCAN (19) should be used
  /// - for ECU signals (CCP, XCP), ExtensionType.DIM should be used
  /// </summary>
  public enum ExtensionType
  {
    /// <summary>DIM block supplement</summary>
    DIM = 2,
    /// <summary>Vector CAN block supplement</summary>
    VectorCAN = 19,
  }

  /// <summary>Floating point format types</summary>
  public enum FPFormatType
  {
    /// <summary>Floating-point format compliant with IEEE 754 standard.</summary>
    IEEE_754,
    /// <summary>Floating-point format compliant with G_Float (VAX architecture) (obsolete).</summary>
    G_FLOAT,
    /// <summary>Floating-point format compliant with D_Float (VAX architecture) (obsolete).</summary>
    D_FLOAT,
  }

  /// <summary>
  /// The File Identification Block
  /// 
  /// The IDBLOCK always begins at file position 0 and has a constant length of 64 Bytes. 
  /// It contains information to identify the file. This includes information about the 
  /// source of the file and general format specifications.
  /// </summary>
  public sealed class IDBLOCK : IIDBLOCK
  {
    #region ctors
    /// <summary>
    /// Creates a standard conformant IDBlock with the specified programID.
    /// </summary>
    /// <param name="programID">programID String (max 8 Bytes)</param>
    /// <param name="codePage">Codepage</param>
    /// <param name="version">Version</param>
    /// <param name="fpFormat">floating point format</param>
    public IDBLOCK(string programID, UInt16 codePage, UInt16 version = 330, FPFormatType fpFormat = FPFormatType.IEEE_754)
    {
      FileID = Resources.strMDFMagicHeader;
      FormatId = Resources.strMDFFormatMagicV3;
      ProgramId = programID;
      FPFormat = fpFormat;
      CodePage = codePage;
      Version = version;
    }
    internal IDBLOCK(BinaryReader br)
    {
      var buffer = new byte[8];
      br.Read(buffer, 0, buffer.Length);
      FileID = Helpers.getString(ref buffer);
      br.Read(buffer, 0, buffer.Length);
      FormatId = Helpers.getString(ref buffer);
      br.Read(buffer, 0, buffer.Length);
      ProgramId = Helpers.getString(ref buffer);
      ByteOrder = br.ReadUInt16() > 0 ? BYTEORDER_TYPE.MSB_FIRST : BYTEORDER_TYPE.MSB_LAST;
      FPFormat = (FPFormatType)br.ReadUInt16();
      Version = br.ReadUInt16();
      CodePage = br.ReadUInt16();
      for (int i = 0; i < 7; ++i)
        br.ReadUInt32();
      UnfinalizedFlags = (UnfinalizedFlagsType)br.ReadUInt16();
      CustomFlags = (CustomFlagsType)br.ReadUInt16();
    }
    #endregion
    #region properties
    [Browsable(false)]
    public MDFType Type { get { return MDFType.V3; } }
#if !DEBUG
    [Browsable(false)]
#endif
    public Int64 BlockSize { get { return 64; } }
    [Category(Constants.CATData)]
    public string FileID { get; }
    [Category(Constants.CATData)]
    public string FormatId { get; }
    [Category(Constants.CATData)]
    public string ProgramId { get; }
    [Category(Constants.CATData)]
    public UInt16 Version { get; }
    [Category(Constants.CATData), DisplayName("Code page")]
    public UInt16 CodePage { get; }
    [Category(Constants.CATData), TypeConverter(typeof(TCByteOrder))]
    public BYTEORDER_TYPE ByteOrder { get; }
    [Category(Constants.CATData), DisplayName("Floating point format"), DefaultValue(FPFormatType.IEEE_754)]
    public FPFormatType FPFormat { get; }
    [Category(Constants.CATData)]
    public UnfinalizedFlagsType UnfinalizedFlags { get; }
    [Category(Constants.CATData)]
    public CustomFlagsType CustomFlags { get; }
    [Browsable(false)]
    public string Name { get { return "ID"; } }
    [Category(Constants.CATNavigation), Description(Constants.DESCTagProperty)]
    public object Tag { get; set; }
    #endregion
    #region methods
    public Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      byte[] buffer;
      Helpers.createArrayFromString(out buffer, 8, FileID, false);
      bw.Write(buffer);
      Helpers.createArrayFromString(out buffer, 8, FormatId, false);
      bw.Write(buffer);
      Helpers.createArrayFromString(out buffer, 8, ProgramId, false);
      bw.Write(buffer);

      bw.Write((UInt16)0);  // set byte order always to little endian
      bw.Write((UInt16)FPFormat);
      bw.Write(Version);
      bw.Write(CodePage);
      for (int i = 0; i < 4; ++i)
        bw.Write((UInt64)0);
      return 0;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all MDF V3 BLOCK types (except the IDBLOCK).
  /// </summary>
  public class BLOCKBASE : IMDFBlock
  {
    #region ctors
    protected BLOCKBASE() { }
    /// <summary>
    /// Creates a new instance of BLOCKBASE with the specified parameters.
    /// </summary>
    /// <param name="id">The block id (always 2 characters)</param>
    /// <param name="blockSize">The structure size</param>
    protected BLOCKBASE(string id, int blockSize)
    {
      Id = id;
      BlockSize = (UInt16)blockSize;
    }
    #endregion
    #region properties
    [Browsable(false)]
    public virtual string Name { get { return Id; } protected set { } }
#if !DEBUG
    [Browsable(false)]
#endif
    public Int64 BlockSize { get; protected set; }
    /// <summary>
    /// Gets the Blocktype (id).
    /// </summary>
#if !DEBUG
    [Browsable(false)]
#endif
    public string Id { get; private set; }
    /// <summary>
    /// Gets or sets a reference to a user object.
    /// </summary>
    [Category(Constants.CATNavigation), Description(Constants.DESCTagProperty)]
    public object Tag { get; set; }
    #endregion
    #region methods
    protected void readData(BinaryReader br)
    {
      Id = Encoding.ASCII.GetString(br.ReadBytes(2));
      BlockSize = br.ReadUInt16();
    }
    public virtual Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var pos = bw.BaseStream.Position;
      bw.Write(Encoding.ASCII.GetBytes(Id));
      bw.Write((UInt16)BlockSize);
      return pos;
    }
    public override string ToString() { return Id; }
    #endregion
  }
  /// <summary>
  /// The Text Block.
  /// 
  /// The TXBLOCK contains an optional comment for the measured data file, 
  /// channel group or signal, or the long name of a signal.The text length 
  /// results from the block size.
  /// </summary>
  public sealed class TXBLOCK : BLOCKBASE
  {
    #region ctors
    internal TXBLOCK(BinaryReader br, UInt32 link)
    {
      if (link == 0)
        return;
      var position = br.BaseStream.Position;
      try
      {
        br.BaseStream.Position = link;
        readData(br);
        var size = (int)(BlockSize - 4);
        if (size > 0)
        {
          var buffer = new byte[size];
          br.Read(buffer, 0, size);
          Text = Helpers.getString(ref buffer);
        }
      }
      catch { }
      finally { br.BaseStream.Position = position; }
    }
    public TXBLOCK(string text) : base("TX", 4 + text.Length + 1) { Text = text; }
    #endregion
    #region properties
    public string Text { get; set; } = string.Empty;
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var pos = base.writeBlock(bw);
      if (!string.IsNullOrEmpty(Text))
        bw.Write(Encoding.Default.GetBytes(Text));
      bw.Write('\0');
      return pos;
    }
    #endregion
  }
  /// <summary>
  /// The Channel Extension Block.
  /// 
  /// The CEBLOCK serves to specify source specific properties of a channel, 
  /// e.g. name of the ECU or name and sender of the CAN message.
  /// </summary>
  public sealed class CEBLOCK : BLOCKBASE
  {
    #region subtypes
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class DIMType
    {
      public UInt16 Module { get; }
      public UInt32 Address { get; }
      public string Description { get; }
      public string ECUId { get; }
      public DIMType(UInt16 module, UInt32 address, string description, string ecuId) { Module = module; Address = address; Description = description; ECUId = ecuId; }
    }
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class VectorCANType
    {
      public UInt32 Id { get; }
      public UInt32 Index { get; }
      public string Message { get; }
      public string Sender { get; }
      public VectorCANType(UInt32 id, UInt32 index, string message, string sender) { Id = id; Index = index; Message = message; Sender = sender; }
    }
    #endregion
    #region ctors
    internal CEBLOCK(BinaryReader br)
    {
      readData(br);
      ExtensionType = (ExtensionType)br.ReadUInt16();
      switch (ExtensionType)
      {
        case ExtensionType.DIM:
          var bufferDesc = new byte[80];
          br.Read(bufferDesc, 0, bufferDesc.Length);
          var bufferECUId = new byte[32];
          br.Read(bufferECUId, 0, bufferECUId.Length);
          DIM = new DIMType(br.ReadUInt16(), br.ReadUInt32(), Helpers.getString(ref bufferDesc), Helpers.getString(ref bufferECUId));
          break;
        case ExtensionType.VectorCAN:
          var bufferMessage = new byte[36];
          br.Read(bufferMessage, 0, bufferMessage.Length);
          var bufferSender = new byte[36];
          br.Read(bufferSender, 0, bufferSender.Length);
          VectorCAN = new VectorCANType(br.ReadUInt32(), br.ReadUInt32(), Helpers.getString(ref bufferMessage), Helpers.getString(ref bufferSender));
          break;
        default:
          break;
      }
    }
    public CEBLOCK(DIMType dim) : base("CE", 6) { ExtensionType = ExtensionType.DIM; DIM = dim; }
    public CEBLOCK(VectorCANType vectorCAN) : base("CE", 6) { ExtensionType = ExtensionType.VectorCAN; VectorCAN = vectorCAN; }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public ExtensionType ExtensionType { get; }
    [Category(Constants.CATData)]
    public override string Name { get { return ExtensionType.ToString(); } }
    [Category(Constants.CATData)]
    public DIMType DIM { get; set; }
    [Category(Constants.CATData)]
    public VectorCANType VectorCAN { get; set; }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      BlockSize += (UInt16)(ExtensionType == ExtensionType.DIM ? 118 : 80);
      var startPos = base.writeBlock(bw);
      bw.Write((UInt16)ExtensionType);
      switch (ExtensionType)
      {
        case ExtensionType.DIM:
          bw.Write(DIM.Module);
          bw.Write(DIM.Address);
          byte[] buffer;
          Helpers.createArrayFromString(out buffer, 80, DIM.Description);
          bw.Write(buffer);
          Helpers.createArrayFromString(out buffer, 32, DIM.ECUId);
          bw.Write(buffer);
          break;
        case ExtensionType.VectorCAN:
          bw.Write(VectorCAN.Id);
          bw.Write(VectorCAN.Index);
          Helpers.createArrayFromString(out buffer, 36, VectorCAN.Message);
          bw.Write(buffer);
          Helpers.createArrayFromString(out buffer, 36, VectorCAN.Sender);
          bw.Write(buffer);
          break;
        default:
          throw new NotSupportedException(ExtensionType.ToString());
      }
      return startPos;
    }
    #endregion
  }
  /// <summary>
  /// The Channel Dependency Block.
  /// 
  /// A signal may be a composition of other signals, 
  /// e.g. for a map/curve or a structure. When saving 
  /// such a compound signal, this can be modelled in MDF 
  /// by a CNBLOCK for the compound signal itself having a 
  /// link to a CDBLOCK. The CDBLOCK then lists all signal 
  /// dependencies, i.e. all signals the compound signal 
  /// consists of.
  /// </summary>
  public sealed class CDBLOCK : BLOCKBASE, IDependencyBlock
  {
    #region ctors
    internal CDBLOCK(BinaryReader br)
    {
      readData(br);
      DependencyType = br.ReadUInt16();
      var noOfSignals = br.ReadUInt16();
      for (int i = 0; i < noOfSignals; ++i)
        Dependencies.Add(new DependencyType(br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32()));
    }
    public CDBLOCK() : base("CD", 8) { }
    #endregion
    #region properties
    /// <summary>
    /// Dependency type.
    /// 
    /// 0 = no dependency
    /// 1 = linear dependency (vector)
    /// 2 = matrix dependency (signals for each matrix line in a separate CGBLOCK)
    /// Since version 3.01:
    /// 255 + N = N-dimensional dependency (size of each dimension is listed after the
    /// variable length LINK list, dependent signals can be stored in same CGBLOCK)
    /// </summary>
    [Category(Constants.CATData)]
    public UInt16 DependencyType { get; }
    [Category(Constants.CATData)]
    public List<DependencyType> Dependencies { get; } = new List<DependencyType>();
    public override string Name { get { return "Dependency"; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      BlockSize += Dependencies.Count * 3 * 4;
      var startPos = base.writeBlock(bw);
      bw.Write(DependencyType);
      bw.Write((UInt16)Dependencies.Count);
      foreach (var dep in Dependencies)
      {
        bw.Write((UInt32)dep.LinkDG);
        bw.Write((UInt32)dep.LinkCG);
        bw.Write((UInt32)dep.LinkCN);
      }
      return startPos;
    }
    void IDependencyBlock.createDependencies(Dictionary<long, IMDFBlock> references)
    {
      for (int i = Dependencies.Count - 1; i >= 0; --i)
      {
        var dep = Dependencies[i];
        if (!dep.createDependencies(references))
          // failed to create dependency
          Dependencies.RemoveAt(i);
      }
    }
    #endregion
  }
  /// <summary>Base class for any MDF V3 CCBLOCKs.</summary>
  public sealed class CCBLOCK : BLOCKBASE, ICCBLOCK
  {
    #region types
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class TimeType
    {
      public UInt32 MS { get; }
      public byte Days { get; }
      public TimeType(UInt32 ms, byte days) { MS = ms; Days = days; }
      public void write(BinaryWriter bw) { bw.Write(MS); bw.Write(Days); }
    }
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class DateType
    {
      public UInt16 MS { get; }
      public byte Minute { get; }
      public byte Hour { get; }
      public byte Day { get; }
      public byte Month { get; }
      public byte Year { get; }
      public DateType(UInt16 ms, byte minute, byte hour, byte day, byte month, byte year)
      { MS = ms; Minute = minute; Hour = hour; Day = day; Month = month; Year = year; }
      public void write(BinaryWriter bw) { bw.Write(MS); bw.Write(Minute); bw.Write(Hour); bw.Write(Day); bw.Write(Month); bw.Write(Year); }
    }
    #endregion
    #region ctors
    internal CCBLOCK(BinaryReader br)
    {
      readData(br);

      var rangeValid = br.ReadUInt16() > 0;
      if (rangeValid)
      {
        Min = br.ReadDouble();
        Max = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      var buffer = new byte[20];
      br.Read(buffer, 0, buffer.Length);
      Unit = Helpers.getString(ref buffer);
      ConversionType = (ConversionType)br.ReadUInt16();
      TabSize = br.ReadUInt16();

      int maxParam = 0;
      switch (ConversionType)
      {
        case ConversionType.None: break;

        case ConversionType.ParametricLinear:
        case ConversionType.Polynomial:
        case ConversionType.Exponential:
        case ConversionType.Logarithmic:
        case ConversionType.Rational: maxParam = TabSize; break;

        case ConversionType.TextFormula:
          buffer = new byte[TabSize];
          br.Read(buffer, 0, buffer.Length);
          Formula = Helpers.getString(ref buffer);
          break;

        case ConversionType.Tab:
        case ConversionType.TabInt:
          for (int i = 0; i < TabSize; ++i)
            KeyValuePairs[br.ReadDouble()] = br.ReadDouble();
          break;

        case ConversionType.TextTable:
          for (int i = 0; i < TabSize; ++i)
          {
            var key = br.ReadDouble();
            buffer = new byte[32];
            br.Read(buffer, 0, buffer.Length);
            KeyValuePairs[key] = Helpers.getString(ref buffer);
          }
          break;

        case ConversionType.TextRange:
          br.ReadDouble();
          br.ReadDouble();
          DefaultText = new TXBLOCK(br, br.ReadUInt32()).Text;

          for (int i = 0; i < TabSize - 1; ++i)
          {
            var value = new sRange { Min = br.ReadDouble(), Max = br.ReadDouble() };
            var key = new TXBLOCK(br, br.ReadUInt32()).Text;
            KeyValuePairs[key] = value;
          }
          break;

        case ConversionType.Date:
          Date = new DateType(br.ReadUInt16(), br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte());
          break;

        case ConversionType.Time:
          Time = new TimeType(br.ReadUInt32(), br.ReadByte());
          break;

        default:
          throw new NotSupportedException(ConversionType.ToString());
      }

      if (maxParam > 0)
        // read and create parameters
        Params = Helpers.getCCParameters(br, maxParam, ConversionType);
    }

    public CCBLOCK(ConversionType conversionType, string unit, double min = double.NaN, double max = double.NaN)
      : base("CC", 46)
    {
      ConversionType = conversionType;
      Unit = unit;
      if (!double.IsNaN(min) && !double.IsNaN(max))
      {
        Min = min;
        Max = max;
      }
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public ConversionType ConversionType { get; }
    [Category(Constants.CATData)]
    public string Unit { get; }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN)]
    public double Min { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN)]
    public double Max { get; set; } = double.NaN;
    [Category(Constants.CATData), ReadOnly(true)]
    public UInt16 TabSize { get; internal set; }
    [Category(Constants.CATData)]
    public double[] Params { get; set; }
    [Category(Constants.CATData)]
    public SortedList<object, object> KeyValuePairs { get; set; } = new SortedList<object, object>();
    [Category(Constants.CATData), ReadOnly(true)]
    public string DefaultText { get; set; } = string.Empty;
    [Category(Constants.CATData), ReadOnly(true)]
    public string Formula { get; set; } = string.Empty;
    /// <summary>Data for ConversionType.Time</summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public TimeType Time { get; }
    /// <summary>Data for ConversionType.Date</summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public DateType Date { get; }
    [Browsable(false)]
    public string Comment { get { return string.Empty; } }
    [Browsable(false)]
    public ICCBLOCK InvCCBlock { get { return null; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      TabSize = 0;
      BlockSize = 46;
      switch (ConversionType)
      {
        case ConversionType.None: break;

        case ConversionType.ParametricLinear:
        case ConversionType.Polynomial:
        case ConversionType.Exponential:
        case ConversionType.Logarithmic:
        case ConversionType.Rational:
          TabSize = (UInt16)Params.Length;
          BlockSize += (UInt16)(TabSize * 8);
          break;

        case ConversionType.TextFormula:
          TabSize = (UInt16)(Formula.Length);
          BlockSize += TabSize + 1;
          break;

        case ConversionType.Tab:
        case ConversionType.TabInt:
          TabSize = (UInt16)KeyValuePairs.Count;
          BlockSize += (UInt16)(TabSize * (2 * 8));
          break;

        case ConversionType.TextTable:
          TabSize = (UInt16)KeyValuePairs.Count;
          BlockSize += (UInt16)(TabSize * 40);
          break;

        case ConversionType.TextRange:
          TabSize = (UInt16)(KeyValuePairs.Count + 1);
          BlockSize += (UInt16)(TabSize * 20);
          break;

        case ConversionType.Date:
          TabSize = 6;
          BlockSize += 7;
          break;

        case ConversionType.Time:
          TabSize = 2;
          BlockSize += 5;
          break;

        default: throw new NotSupportedException(ConversionType.ToString());
      }

      var startPos = base.writeBlock(bw);

      bw.Write((UInt16)(double.IsNaN(Min) || double.IsNaN(Max) ? 0 : 1));
      bw.Write(Min);
      bw.Write(Max);

      byte[] buffer;
      Helpers.createArrayFromString(out buffer, 20, Unit);
      bw.Write(buffer);
      bw.Write((UInt16)ConversionType);

      bw.Write(TabSize);

      switch (ConversionType)
      {
        case ConversionType.None: break;

        case ConversionType.ParametricLinear:
        case ConversionType.Polynomial:
        case ConversionType.Exponential:
        case ConversionType.Logarithmic:
        case ConversionType.Rational:
          foreach (var par in Params)
            bw.Write(par);
          break;

        case ConversionType.TextFormula:
          Helpers.createArrayFromString(out buffer, 256, Formula);
          bw.Write(buffer);
          break;

        case ConversionType.Tab:
        case ConversionType.TabInt:
          foreach (var kvp in KeyValuePairs)
          {
            bw.Write((double)kvp.Key);
            bw.Write((double)kvp.Value);
          }
          break;

        case ConversionType.TextTable:
          foreach (var kvp in KeyValuePairs)
          {
            bw.Write((double)kvp.Key);
            Helpers.createArrayFromString(out buffer, 32, (string)kvp.Value);
            bw.Write(buffer);
          }
          break;

        case ConversionType.TextRange:
          bw.Write((double)0);
          bw.Write((double)0);
          new TXBLOCK(DefaultText).writeBlock(bw);
          foreach (var kvp in KeyValuePairs)
          {
            string text = (string)kvp.Key;
            var value = (sRange)kvp.Value;
            bw.Write(value.Min);
            bw.Write(value.Max);
            new TXBLOCK(text).writeBlock(bw);
          }
          break;

        case ConversionType.Date: Date.write(bw); break;

        case ConversionType.Time: Time.write(bw); break;

        default: throw new NotSupportedException(ConversionType.ToString());
      }
      return startPos;
    }

    /// <summary>
    /// Get the physical value from the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>The physical value</returns>
    public double toPhysical(double rawValue)
    {
      double physValue = rawValue;
      try
      {
        switch (ConversionType)
        {
          case ConversionType.ParametricLinear:
            if (Params != Helpers.LINEAR_IDENTITY)
              physValue = Params[1] * rawValue + Params[0];
            break;

          case ConversionType.Rational:
            if (Params != Helpers.RAT_IDENTITY)
            {
              var A = Params[3] * rawValue - Params[0];
              var B = Params[4] * rawValue - Params[1];
              var C = Params[5] * rawValue - Params[2];
              if (A != 0 && !double.IsNaN(A) && !double.IsInfinity(A))
              { // Full solution, A != 0
              }
              else
                // Degenerate case, A == 0
                physValue = -C / B;
            }
            break;

          case ConversionType.Tab:
            object value = rawValue;
            if (KeyValuePairs.TryGetValue(rawValue, out value))
              physValue = (double)value;
            break;

          case ConversionType.TabInt:
            double physStart = physValue;
            if (KeyValuePairs.Count > 0)
            {
              double rawInt = rawValue;
              double rawStart = (double)KeyValuePairs.Keys[0];
              physStart = (double)KeyValuePairs.Values[0];
              for (int i = 1; i < KeyValuePairs.Values.Count; ++i)
              {
                if (rawInt < rawStart)
                  // smaller than start
                  return physStart;
                double rawEnd = (double)KeyValuePairs.Keys[i];
                double physEnd = (double)KeyValuePairs.Values[i];
                if (rawInt <= rawEnd)
                  // between
                  return (rawInt - rawStart) / (rawEnd - rawStart) * (physEnd - physStart) + physStart;
                rawStart = rawEnd;
                physStart = physEnd;
              }
            }
            physValue = physStart;
            break;

          case ConversionType.Polynomial:
            double term = rawValue - Params[4] - Params[5];
            double denominator = Params[2] * (term) - Params[0];
            double nominator = Params[1] - (Params[3] * (term));
            physValue = nominator / denominator;
            break;

          case ConversionType.Exponential:
            if (Params[0] == 0)
              physValue = Math.Exp(((Params[2] / (rawValue - Params[6])) - Params[5]) / Params[3]) / Params[4];
            else if (Params[3] == 0)
              physValue = Math.Exp(((rawValue - Params[6]) * Params[5] - Params[2]) / Params[0]) / Params[1];
            break;

          case ConversionType.Logarithmic:
            if (Params[0] == 0)
              physValue = Math.Exp(((Params[2] / (rawValue - Params[6])) - Params[5]) / Params[3]) / Params[4];
            else if (Params[4] == 0)
              physValue = Math.Exp(((rawValue - Params[6]) * Params[5] - Params[2]) / Params[0]) / Params[1];
            break;

          case ConversionType.TextFormula:
            break;

          case ConversionType.TextTable:
            break;

          case ConversionType.TextRange:
            break;

          case ConversionType.Date:
            break;

          case ConversionType.Time:
            break;
        }
      }
      catch { }
      return physValue;
    }
    #endregion
  }
  /// <summary>
  /// The Channel Block.
  /// 
  /// The channel block contains detailed information
  /// about a signal and how it is stored in the record.
  /// </summary>
  public sealed class CNBLOCK : BLOCKBASE, ICNBLOCK
  {
    #region types
    /// <summary>Signal types</summary>
    enum SignalV3Type
    {
      /// <summary>unsigned integer</summary>
      UINT,
      /// <summary>signed integer</summary>
      SINT,
      /// <summary>IEEE 754 floating-point format FLOAT (4 bytes)</summary>
      FLOAT,
      /// <summary>IEEE 754 floating-point format DOUBLE (8 bytes)</summary>
      DOUBLE,
      /// <summary>VAX floating-point format (F_Float), obsolete</summary>
      F_FLOAT,
      /// <summary>VAX floating-point format (G_Float), obsolete</summary>
      G_FLOAT,
      /// <summary>VAX floating-point format (D_Float), obsolete</summary>
      D_FLOAT,
      /// <summary>String (NULL terminated)</summary>
      STRING,
      /// <summary>Byte Array (max. 8191 Bytes, constant record length!)</summary>
      BYTE_ARRAY,
      /// <summary>unsigned integer (big endian)</summary>
      UINT_BigEndian,
      /// <summary>signed integer (big endian)</summary>
      SINT_BigEndian,
      /// <summary>IEEE 754 floating-point format FLOAT (4 bytes, big endian)</summary>
      FLOAT_BigEndian,
      /// <summary>IEEE 754 floating-point format FLOAT (8 bytes, big endian)</summary>
      DOUBLE_BigEndian,
      /// <summary>unsigned integer (little endian)</summary>
      UINT_LittleEndian,
      /// <summary>signed integer (little endian)</summary>
      SINT_LittleEndian,
      /// <summary>IEEE 754 floating-point format FLOAT (4 bytes, little endian)</summary>
      FLOAT_LittleEndian,
      /// <summary>IEEE 754 floating-point format FLOAT (8 bytes, little endian)</summary>
      DOUBLE_LittleEndian,
    }
    #endregion
    #region data members
    UInt32 LinkCC, LinkCE, LinkCD, LinkTX, LinkTXName, LinkTXDisplayName;
    internal UInt32 LinkNextCN;
    #endregion
    #region ctors
    internal CNBLOCK(ICGBLOCK cgBlock, BinaryReader br, BYTEORDER_TYPE byteOrder)
    {
      CGBlock = cgBlock;
      var start = br.BaseStream.Position;
      readData(br);
      LinkNextCN = br.ReadUInt32();
      LinkCC = br.ReadUInt32();
      LinkCE = br.ReadUInt32();
      LinkCD = br.ReadUInt32();
      LinkTX = br.ReadUInt32();

      UInt16 channelType = br.ReadUInt16();
      switch (channelType)
      {
        case 0: ChannelType = ChannelType.Data; break;
        case 1: ChannelType = ChannelType.Master; break;
      }

      var name = new byte[32];
      br.Read(name, 0, name.Length);
      Name = Helpers.getString(ref name);

      var description = new byte[128];
      br.Read(description, 0, description.Length);
      Description = Helpers.getString(ref description);

      BitOffset = br.ReadUInt16();
      NoOfBits = br.ReadUInt16();
      SignalType = toSignalType((SignalV3Type)br.ReadUInt16(), byteOrder);

      if (br.ReadUInt16() > 0)
      { // range valid
        MinRaw = br.ReadDouble();
        MaxRaw = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      Rate = br.ReadDouble();

      if (br.BaseStream.Position - start < BlockSize)
        LinkTXName = br.ReadUInt32();
      if (br.BaseStream.Position - start < BlockSize)
        LinkTXDisplayName = br.ReadUInt32();
      if (br.BaseStream.Position - start < BlockSize)
        AddOffset = br.ReadUInt16();

      if (LinkCC > 0)
      {
        br.BaseStream.Position = LinkCC;
        CCBlock = new CCBLOCK(br);
      }
      if (LinkCE > 0)
      {
        br.BaseStream.Position = LinkCE;
        CEBlock = new CEBLOCK(br);
      }
      if (LinkCD > 0)
      {
        br.BaseStream.Position = LinkCD;
        CDBlock = new CDBLOCK(br);
      }

      Comment = new TXBLOCK(br, LinkTX).Text;
      LongName = new TXBLOCK(br, LinkTXName).Text;
      DisplayName = new TXBLOCK(br, LinkTXDisplayName).Text;
    }

    internal CNBLOCK(SignalType signalType, ChannelType channelType
      , string name, string description
      , UInt32 bitOffset, UInt32 addOffset, UInt32 noOfBits
      , ICCBLOCK ccBlock
      , string comment = ""
      , string longName = ""
      , string displayName = ""
      )
      : base("CN", 228)
    {
      SignalType = signalType;
      if (channelType != ChannelType.Data && channelType != ChannelType.Master)
        throw new NotSupportedException($"{channelType.ToString()} is not supported for MDFv3");
      ChannelType = channelType;
      Name = name;
      Description = description;
      NoOfBits = noOfBits;
      AddOffset = (UInt16)addOffset;
      BitOffset = (UInt16)bitOffset;
      CCBlock = ccBlock;
      Comment = comment;
      LongName = longName;
      DisplayName = displayName;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public override string Name { get; protected set; }
    [Category(Constants.CATData)]
    public string Unit { get { return CCBlock != null ? ((CCBLOCK)CCBlock).Unit : string.Empty; } }
    [Category(Constants.CATData)]
    public string Description { get; }
    [Category(Constants.CATData), DisplayName("Bit offset")]
    public UInt16 BitOffset { get; internal set; }
    [Category(Constants.CATData), DisplayName("Number of Bits"), ReadOnly(true)]
    public UInt32 NoOfBits { get; set; }
    [Category(Constants.CATData), DisplayName("Type")]
    public ChannelType ChannelType { get; }
    [Browsable(false)]
    public SyncType SyncType { get { return SyncType.Time; } }
    [Category(Constants.CATData), DisplayName("Signal type")]
    public SignalType SignalType { get; }
    [Category(Constants.CATData), DisplayName("Additional byte offset")]
    public UInt32 AddOffset { get; internal set; }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MinRaw { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MaxRaw { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double Min { get { return CCBlock != null ? ((CCBLOCK)CCBlock).Min : double.NaN; } }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double Max { get { return CCBlock != null ? ((CCBLOCK)CCBlock).Max : double.NaN; } }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MinEx { get { return Min; } }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MaxEx { get { return Max; } }
    /// <summary>Sampling rate for a virtual time channel in [s].</summary>
    [Category(Constants.CATData), DefaultValue(0.0), ReadOnly(true)]
    public double Rate { get; set; }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData), DisplayName("Long name")]
    public string LongName { get; set; } = string.Empty;
    [Category(Constants.CATData), DisplayName("Display name")]
    public string DisplayName { get; set; } = string.Empty;
    [Category(Constants.CATNavigation)]
    public ICGBLOCK CGBlock { get; }
    [Category(Constants.CATDataLink)]
    public ICCBLOCK CCBlock { get; }
    [Category(Constants.CATDataLink)]
    public IMDFBlock CEBlock { get; }
    [Category(Constants.CATDataLink)]
    public IMDFBlock CDBlock { get; }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      if (!string.IsNullOrEmpty(LongName))
        LinkTXName = (UInt32)(new TXBLOCK(LongName).writeBlock(bw));

      if (!string.IsNullOrEmpty(DisplayName))
        LinkTXDisplayName = (UInt32)(new TXBLOCK(DisplayName).writeBlock(bw));

      if (!string.IsNullOrEmpty(Comment))
        LinkTX = (UInt32)(new TXBLOCK(Comment).writeBlock(bw));

      if (CCBlock != null)
        LinkCC = (UInt32)CCBlock.writeBlock(bw);

      if (CEBlock != null)
        LinkCE = (UInt32)CEBlock.writeBlock(bw);

      if (CDBlock != null)
        LinkCD = (UInt32)CDBlock.writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextCN = (UInt32)endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkNextCN);
      bw.Write(LinkCC);
      bw.Write(LinkCE);
      bw.Write(LinkCD);
      bw.Write(LinkTX);

      bw.Write((UInt16)(ChannelType == ChannelType.Master ? 1 : 0));

      byte[] buffer;
      Helpers.createArrayFromString(out buffer, 32, Name);
      bw.Write(buffer);
      Helpers.createArrayFromString(out buffer, 128, Description);
      bw.Write(buffer);

      bw.Write(BitOffset);
      bw.Write((UInt16)NoOfBits);
      bw.Write(toV3SignalType(SignalType, NoOfBits));
      bw.Write((UInt16)(double.IsNaN(MinRaw) || double.IsNaN(MaxRaw) ? 0 : 1));
      bw.Write(MinRaw);
      bw.Write(MaxRaw);
      bw.Write(Rate);
      bw.Write(LinkTXName);
      bw.Write(LinkTXDisplayName);
      bw.Write((UInt16)AddOffset);
      bw.BaseStream.Position = endPos;
      return startPos;
    }

    Int64 ICNBLOCK.getReadPosition(Int64 recordIndex)
    {
      var result = recordIndex * CGBlock.getRecordSize();
      switch (CGBlock.DGBlock.RecordIDType)
      {
        case RecordIDType.Before8Bit:
        case RecordIDType.BeforeAndAfter8Bit: result += 1; break;
      }
      return result;
    }

    static SignalType toSignalType(SignalV3Type signalType, BYTEORDER_TYPE byteOrder)
    {

      switch (signalType)
      {
        case SignalV3Type.UINT_BigEndian: return SignalType.UINT_BE;
        case SignalV3Type.UINT_LittleEndian: return SignalType.UINT_LE;
        case SignalV3Type.SINT_BigEndian: return SignalType.SINT_BE;
        case SignalV3Type.SINT_LittleEndian: return SignalType.SINT_LE;
        case SignalV3Type.DOUBLE_BigEndian:
        case SignalV3Type.FLOAT_BigEndian: return SignalType.FLOAT_BE;
        case SignalV3Type.DOUBLE_LittleEndian:
        case SignalV3Type.FLOAT_LittleEndian: return SignalType.FLOAT_LE;

        case SignalV3Type.STRING: return SignalType.STRING;
        case SignalV3Type.BYTE_ARRAY: return SignalType.BYTE_ARRAY;

        case SignalV3Type.UINT: return byteOrder == BYTEORDER_TYPE.MSB_FIRST ? SignalType.UINT_BE : SignalType.UINT_LE;
        case SignalV3Type.SINT: return byteOrder == BYTEORDER_TYPE.MSB_FIRST ? SignalType.SINT_BE : SignalType.SINT_LE;
        case SignalV3Type.DOUBLE:
        case SignalV3Type.FLOAT: return byteOrder == BYTEORDER_TYPE.MSB_FIRST ? SignalType.FLOAT_BE : SignalType.FLOAT_LE;
        default: throw new NotSupportedException(signalType.ToString());
      }
    }

    static UInt16 toV3SignalType(SignalType signalType, uint noOfBits)
    {
      SignalV3Type result;
      switch (signalType)
      {
        case SignalType.BYTE_ARRAY: result = SignalV3Type.BYTE_ARRAY; break;
        case SignalType.STRING: result = SignalV3Type.STRING; break;
        case SignalType.UINT_BE: result = SignalV3Type.UINT_BigEndian; break;
        case SignalType.UINT_LE: result = SignalV3Type.UINT_LittleEndian; break;
        case SignalType.SINT_BE: result = SignalV3Type.SINT_BigEndian; break;
        case SignalType.SINT_LE: result = SignalV3Type.SINT_LittleEndian; break;
        case SignalType.FLOAT_BE: result = noOfBits < 64 ? SignalV3Type.FLOAT_BigEndian : SignalV3Type.DOUBLE_BigEndian; break;
        case SignalType.FLOAT_LE: result = noOfBits < 64 ? SignalV3Type.FLOAT_LittleEndian : SignalV3Type.DOUBLE_LittleEndian; break;
        default: throw new NotSupportedException(signalType.ToString());
      }
      return (UInt16)result;
    }
    #endregion
  }
  /// <summary>
  /// The Sample Reduction Block.
  /// 
  /// The SRBLOCK serves to describe a sample reduction for a channel group, i.e. an alternative sampling
  /// of the signals in the channel group with a usually lower number of sampling points. There can be
  /// several sample reductions for the same channel group which are stored in a forward linked list of
  /// SRBLOCKs starting at the CGBLOCK.
  /// Each SRBLOCK describes a sample reduction calculated with a specific time interval. Roughly
  /// explained, the reduction divides the time axis into intervals of the given fixed length and compresses
  /// all samples within one interval to three values: mean, minimum and maximum value. Each sample
  /// reduction applies to all signals of its parent channel group. For details of calculating and storing a
  /// sample reduction for a channel group, see below.
  /// Sample reductions are only "convenience" information to accelerate graphical display or offline
  /// analysis of the signal data. It may be omitted without any loss of information. If not present, it can be
  /// added later by offline processing of the file.
  /// </summary>
  public sealed class SRBLOCK : BLOCKBASE, ISRBLOCK
  {
    #region data members
    internal const string DP_NAME_RED_SAMPLES = "Number of reduced samples";
    internal const string DP_NAME_LEN_TIME = "Length of time interval[s]";
    internal UInt32 LinkNextSR;
    /// <summary>Pointer to the data block for this sample reduction</summary>
    UInt32 LinkDB;
    #endregion
    #region ctors
    internal SRBLOCK(BinaryReader br)
    {
      readData(br);
      LinkNextSR = br.ReadUInt32();
      LinkDB = br.ReadUInt32();
      NrOfRedSamples = br.ReadUInt32();
      LenOfTimeInt = br.ReadDouble();
    }
    internal SRBLOCK() : base("SR", 24) { }
    #endregion
    #region properties
    [Category(Constants.CATData), DisplayName(DP_NAME_RED_SAMPLES)]
    public UInt64 NrOfRedSamples { get; }
    [Category(Constants.CATData), DisplayName(DP_NAME_LEN_TIME)]
    public double LenOfTimeInt { get; }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      // todo Write DB
      //LinkDB = (UInt32)bw.BaseStream.Position;

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextSR = (UInt32)(endPos);
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw, isLast);
      bw.Write(LinkNextSR);
      bw.Write(LinkDB);
      bw.Write((UInt32)NrOfRedSamples);
      bw.Write(LenOfTimeInt);
      bw.BaseStream.Position = endPos;
      return startPos;
    }
    #endregion
  }
  /// <summary>
  /// The Channel Group Block.
  /// 
  /// The CGBLOCK contains a collection of channels which are stored in a record, 
  ///i.e. which have the same time sampling.
  /// </summary>
  public sealed class CGBLOCK : BLOCKBASE, ICGBLOCK
  {
    #region data member
    UInt32 LinkTX, LinkSR, LinkCN;
    internal UInt32 LinkNextCG;
    #endregion
    #region ctors
    internal CGBLOCK(IDGBLOCK dgBlock, BinaryReader br, Dictionary<long, IMDFBlock> references, BYTEORDER_TYPE byteOrder)
    {
      DGBlock = dgBlock;
      var start = br.BaseStream.Position;
      readData(br);
      LinkNextCG = br.ReadUInt32();
      LinkCN = br.ReadUInt32();
      LinkTX = br.ReadUInt32();
      RecordID = br.ReadUInt16();
      var noChannels = br.ReadUInt16();
      RecordSize = br.ReadUInt16();
      RecordCount = br.ReadUInt32();
      if (br.BaseStream.Position - start < BlockSize)
        LinkSR = br.ReadUInt32();

      CNBLOCK block;
      var link = LinkCN;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new CNBLOCK(this, br, byteOrder);
        CNBlocks.Add(block);
        link = block.LinkNextCN;
      }
      CNBlocks.Sort((x, y) => { return (x.AddOffset + x.BitOffset / 8).CompareTo(y.AddOffset + y.BitOffset / 8); });

      Comment = new TXBLOCK(br, LinkTX).Text;

      link = LinkSR;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var srBlock = new SRBLOCK(br);
        SRBlocks.Add(srBlock);
        link = srBlock.LinkNextSR;
      }
    }
    public CGBLOCK(IDGBLOCK dgBlock, string comment = "") : base("CG", 30) { DGBlock = dgBlock; Comment = comment; }
    #endregion
    #region properties
    /// <summary>
    /// Record ID, i.e. value of the identifier for a record if the DGBLOCK defines a number of record IDs > 0
    /// </summary>
    [Category(Constants.CATData), DisplayName("Record ID")]
    public UInt16 RecordID { get; }
    [Category(Constants.CATData), DisplayName("Record size"), ReadOnly(true)]
    public UInt32 RecordSize { get; set; }
    [Category(Constants.CATData), DisplayName("Record count")]
    public UInt64 RecordCount { get; set; }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATNavigation)]
    public IDGBLOCK DGBlock { get; }
    [Category(Constants.CATDataLink)]
    public List<ISRBLOCK> SRBlocks { get; } = new List<ISRBLOCK>();
    [Browsable(false)]
    public List<ICNBLOCK> CNBlocks { get; } = new List<ICNBLOCK>();
    [Browsable(false)]
    public override string Name { get { return "Channel group"; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      LinkCN = (UInt32)(CNBlocks.Count > 0 ? bw.BaseStream.Position : 0);
      for (int i = 0; i < CNBlocks.Count; ++i)
        CNBlocks[i].writeBlock(bw, i == CNBlocks.Count - 1);

      LinkSR = (UInt32)(SRBlocks.Count > 0 ? bw.BaseStream.Position : 0);
      for (int i = 0; i < SRBlocks.Count; ++i)
        SRBlocks[i].writeBlock(bw, i == SRBlocks.Count - 1);

      if (!string.IsNullOrEmpty(Comment))
        LinkTX = (UInt32)(new TXBLOCK(Comment).writeBlock(bw));

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextCG = (UInt32)endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkNextCG);
      bw.Write(LinkCN);
      bw.Write(LinkTX);
      bw.Write(RecordID);
      bw.Write((UInt16)CNBlocks.Count);
      bw.Write((UInt16)RecordSize);
      bw.Write((UInt32)RecordCount);
      bw.Write(LinkSR);
      bw.BaseStream.Position = endPos;
      return startPos;
    }

    Int64 ICGBLOCK.getRecordSize()
    {
      var size = RecordSize;
      switch (DGBlock.RecordIDType)
      {
        case RecordIDType.BeforeAndAfter8Bit: size += 2; break;
        case RecordIDType.Before8Bit: size += 1; break;
        case RecordIDType.Before16Bit: size += 2; break;
        case RecordIDType.Before32Bit: size += 4; break;
        case RecordIDType.Before64Bit: size += 8; break;
      }
      return size;
    }
    #endregion
  }
  /// <summary>
  /// The Trigger Block
  /// 
  /// The TRBLOCK serves to describe trigger events that occurred during the recording. The trigger
  /// events are related to a data group. For each trigger event, the time when the trigger occurred is
  /// recorded. As additional information, the pre and post trigger times of the trigger event can be stated,
  /// i.e. the time periods recorded before and after the trigger occurred. The comment might be used to
  /// give information about the trigger source or condition.
  /// </summary>
  public sealed class TRBLOCK : BLOCKBASE, IMDFComment
  {
    #region types
    /// <summary>Trigger event data.</summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public sealed class TriggerEvent
    {
      public double Time { get; }
      public double PreTime { get; }
      public double PostTime { get; }
      public TriggerEvent(double time, double preTime, double postTime) { Time = time; PreTime = preTime; PostTime = postTime; }
    }
    #endregion
    #region data members
    UInt32 LinkTX;
    #endregion
    #region ctor
    internal TRBLOCK(BinaryReader br)
    {
      readData(br);
      LinkTX = br.ReadUInt32();
      var noTriggerEvents = br.ReadUInt16();
      for (int i = 0; i < noTriggerEvents; ++i)
        TriggerEvents.Add(new TriggerEvent(br.ReadDouble(), br.ReadDouble(), br.ReadDouble()));
      Comment = new TXBLOCK(br, LinkTX).Text;
    }
    internal TRBLOCK(TriggerEvent[] triggerEvents) : base("TR", 10)
    {
      TriggerEvents.AddRange(triggerEvents);
      BlockSize += (UInt16)(TriggerEvents.Count * 3 * 8);
    }
    #endregion
    #region properties
    [Category(Constants.CATData), ReadOnly(true)]
    public List<TriggerEvent> TriggerEvents { get; } = new List<TriggerEvent>();
    [Category(Constants.CATData)]
    public string Comment { get; set; }
    public override string Name { get { return "Trigger"; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      BlockSize += (UInt16)(TriggerEvents.Count * 3 * 8);

      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      if (!string.IsNullOrEmpty(Comment))
        LinkTX = (UInt32)(new TXBLOCK(Comment).writeBlock(bw));

      var endPos = bw.BaseStream.Position;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkTX);
      bw.Write((UInt16)TriggerEvents.Count);
      foreach (var trEvent in TriggerEvents)
      {
        bw.Write(trEvent.Time);
        bw.Write(trEvent.PreTime);
        bw.Write(trEvent.PostTime);
      }
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// The Data Group Block.
  /// 
  /// The DGBLOCK gathers information and links related to its data block. Thus the branch in the tree of 
  /// MDF blocks that is opened by the DGBLOCK contains all information necessary to understand and 
  /// decode the data block referenced by the DGBLOCK.  
  /// 
  /// The DGBLOCK can contain several channel groups. In this case the MDF file is "unsorted". If there is
  /// only one channel group in the DGBLOCK, the MDF file is "sorted" (please refer to chapter 4 for details).
  /// </summary>
  public sealed class DGBLOCK : BLOCKBASE, IDGBLOCK
  {
    #region data members
    UInt32 LinkDB, LinkTR, LinkCG;
    internal UInt32 LinkNextDG;
    BinaryReader mBr;
    #endregion
    #region ctors
    internal DGBLOCK(BinaryReader br, Dictionary<long, IMDFBlock> references, BYTEORDER_TYPE byteOrder)
    {
      mBr = br;
      readData(br);
      LinkNextDG = br.ReadUInt32();
      LinkCG = br.ReadUInt32();
      LinkTR = br.ReadUInt32();
      LinkDB = br.ReadUInt32();

      var noCG = br.ReadUInt16();

      var recordIDType = br.ReadUInt16();
      switch (recordIDType)
      {
        case 0: RecordIDType = RecordIDType.None; break;
        case 1: RecordIDType = RecordIDType.Before8Bit; break;
        case 2: RecordIDType = RecordIDType.BeforeAndAfter8Bit; break;
      }

      CGBLOCK block;
      var link = LinkCG;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new CGBLOCK(this, br, references, byteOrder);
        CGBlocks.Add(block);
        link = block.LinkNextCG;
      }
      if (LinkTR > 0)
      {
        br.BaseStream.Position = LinkTR;
        TRBlock = new TRBLOCK(br);
      }
    }
    public DGBLOCK() : base("DG", 28) { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public RecordIDType RecordIDType { get; }
    [Browsable(false)]
    public List<ICGBLOCK> CGBlocks { get; } = new List<ICGBLOCK>();
    [Category(Constants.CATDataLink)]
    public TRBLOCK TRBlock { get; }
    [Browsable(false)]
    public string Comment { get { return string.Empty; } }
    [Category(Constants.CATData)]
    public override string Name { get { return "Data group"; } }
    #endregion
    #region events
    public event EventHandler<WriteRawDataEventArgs> OnWriteRawData;
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      if (TRBlock != null)
        LinkTR = (UInt32)TRBlock.writeBlock(bw);

      // Write CG Blocks
      LinkCG = (UInt32)(CGBlocks.Count > 0 ? bw.BaseStream.Position : 0);
      for (int i = 0; i < CGBlocks.Count; ++i)
        CGBlocks[i].writeBlock(bw, i == CGBlocks.Count - 1);

      if (OnWriteRawData != null)
      { // Write Data blocks
        LinkDB = (UInt32)bw.BaseStream.Position;
        OnWriteRawData(this, new WriteRawDataEventArgs(this, bw));
      }

      // Write data
      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextDG = (UInt32)endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkNextDG);
      bw.Write(LinkCG);
      bw.Write(LinkTR);
      bw.Write(LinkDB);
      bw.Write((UInt16)CGBlocks.Count);
      UInt16 recordIDType = 0;
      switch (RecordIDType)
      {
        case RecordIDType.Before8Bit: recordIDType = 1; break;
        case RecordIDType.BeforeAndAfter8Bit: recordIDType = 2; break;
      }
      bw.Write(recordIDType);
      bw.Write((UInt32)0);  // reserved
      bw.BaseStream.Position = endPos;
      return startPos;
    }

    BinaryReader IDataContainer.getReader(long offset) { return mBr; }

    bool IDataContainer.readData(Int64 offset, ref byte[] buffer)
    {
      mBr.BaseStream.Position = LinkDB + offset;
      mBr.Read(buffer, 0, buffer.Length);
      return true;
    }
    #endregion
  }

  /// <summary>
  /// The Header Block.
  /// 
  /// The HDBLOCK always begins at file position 64. It contains general information about 
  /// the contents of the measured data file.
  /// </summary>
  public sealed class HDBLOCK : BLOCKBASE, IHDBLOCK
  {
    #region data members
    UInt32 LinkTX, LinkPR, LinkDG;
    byte[] date;
    byte[] time;
    byte[] author;
    byte[] organization;
    byte[] project;
    byte[] subject;
    UInt64 timestamp;
    byte[] timerId;
    #endregion
    #region ctors
    internal HDBLOCK(BinaryReader br, Dictionary<long, IMDFBlock> references, BYTEORDER_TYPE byteOrder)
    {
      var start = br.BaseStream.Position;
      readData(br);
      LinkDG = br.ReadUInt32();
      LinkTX = br.ReadUInt32();
      LinkPR = br.ReadUInt32();
      var nrDataGroups = br.ReadUInt16();
      date = new byte[10];
      br.Read(date, 0, date.Length);
      time = new byte[8];
      br.Read(time, 0, time.Length);
      author = new byte[32];
      br.Read(author, 0, author.Length);
      organization = new byte[32];
      br.Read(organization, 0, organization.Length);
      project = new byte[32];
      br.Read(project, 0, project.Length);
      subject = new byte[32];
      br.Read(subject, 0, subject.Length);
      if (br.BaseStream.Position - start < BlockSize)
        timestamp = br.ReadUInt64();
      if (br.BaseStream.Position - start < BlockSize)
        UTCOffset = br.ReadInt16();
      if (br.BaseStream.Position - start < BlockSize)
        TimeQuality = (TimeQualityType)br.ReadUInt16();
      if (br.BaseStream.Position - start < BlockSize)
      {
        timerId = new byte[32];
        br.Read(timerId, 0, timerId.Length);
      }

      DGBLOCK block;
      long link = LinkDG;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new DGBLOCK(br, references, byteOrder);
        DGBlocks.Add(block);
        link = block.LinkNextDG;
      }
      Comment = new TXBLOCK(br, LinkTX).Text;
      ProgramSpecificData = new TXBLOCK(br, LinkPR).Text;
    }

    /// <summary>
    /// Creates a new HDBLOCK instance.
    /// </summary>
    /// <param name="dateTime">Recording time</param>
    /// <param name="timeQuality">Recording quality</param>
    /// <param name="_author"></param>
    /// <param name="_organization"></param>
    /// <param name="_project"></param>
    /// <param name="_subject"></param>
    /// <param name="comment">Comment string</param>
    /// <param name="programSpecificData">PRBLOCK data string</param>
    public HDBLOCK(DateTime dateTime, TimeQualityType timeQuality
      , string _author = "", string _organization = "", string _project = "", string _subject = ""
      , string comment = ""
      , string programSpecificData = ""
      )
      : base("HD", 208)
    {
      date = Encoding.ASCII.GetBytes(dateTime.ToString("dd:MM:yyyy"));
      time = Encoding.ASCII.GetBytes(dateTime.ToString("HH:mm:ss"));
      TimeSpan since1970 = dateTime.Subtract(Helpers.DATE_TIME_1970);
      timestamp = (UInt64)(since1970.Ticks * 100);
      DateTime utc = DateTime.FromFileTime(dateTime.ToFileTimeUtc());
      TimeSpan ts = dateTime.Subtract(utc);
      UTCOffset = Convert.ToInt16(Math.Round(TimeZone.CurrentTimeZone.GetUtcOffset(utc).TotalHours));
      TimeQuality = timeQuality;
      Helpers.createArrayFromString(out author, 32, _author);
      Helpers.createArrayFromString(out organization, 32, _organization);
      Helpers.createArrayFromString(out project, 32, _project);
      Helpers.createArrayFromString(out subject, 32, _subject);
      Helpers.createArrayFromString(out timerId, 32, Resources.strMDFRefTime);
      Comment = comment;
      ProgramSpecificData = programSpecificData;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public DateTime RecordingTime
    {
      get
      {
        if (timestamp > 0)
          return TimeStamp;
        DateTime val;
        if (DateTime.TryParseExact(Date + " " + Time, "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out val))
          return val;
        return TimeStamp;
      }
    }
    [Category(Constants.CATData), ReadOnly(false)]
    public string Author
    {
      get { return Helpers.getString(ref author); }
      set { Helpers.createArrayFromString(out author, 32, value); }
    }
    [Category(Constants.CATData), ReadOnly(false)]
    public string Organization
    {
      get { return Helpers.getString(ref organization); }
      set { Helpers.createArrayFromString(out organization, 32, value); }
    }
    [Category(Constants.CATData), ReadOnly(false)]
    public string Project
    {
      get { return Helpers.getString(ref project); }
      set { Helpers.createArrayFromString(out project, 32, value); }
    }
    [Category(Constants.CATData), ReadOnly(false)]
    public string Subject
    {
      get { return Helpers.getString(ref subject); }
      set { Helpers.createArrayFromString(out subject, 32, value); }
    }
    [Browsable(false)]
    public string Date { get { return Helpers.getString(ref date).Replace(':', '.'); } }
    [Browsable(false)]
    public string Time { get { return Helpers.getString(ref time); } }
    [Browsable(false)]
    public DateTime TimeStamp { get { return Helpers.DATE_TIME_1970.AddTicks((long)(timestamp / 100)); } }
    [Category(Constants.CATData)]
    public Int16 UTCOffset { get; }
    [Category(Constants.CATData), DisplayName("Time quality")]
    public TimeQualityType TimeQuality { get; }
    [Category(Constants.CATData)]
    public string TimerID { get { return Helpers.getString(ref timerId); } }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    /// <summary>PRBLOCK Data.</summary>
    [Category(Constants.CATData)]
    public string ProgramSpecificData { get; set; } = string.Empty;
    [Browsable(false)]
    public List<IDGBLOCK> DGBlocks { get; } = new List<IDGBLOCK>();
    [Browsable(false)]
    public override string Name { get { return string.IsNullOrEmpty(Project) ? "Header" : Project; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      bw.BaseStream.Position = Helpers.HEADER_POS + BlockSize;

      if (!string.IsNullOrEmpty(Comment))
      {
        LinkTX = (UInt32)bw.BaseStream.Position;
        new TXBLOCK(Comment).writeBlock(bw);
      }

      if (!string.IsNullOrEmpty(ProgramSpecificData))
      {
        LinkPR = (UInt32)bw.BaseStream.Position;
        new TXBLOCK(ProgramSpecificData).writeBlock(bw);
      }

      LinkDG = (UInt32)(DGBlocks.Count > 0 ? bw.BaseStream.Position : 0);
      for (int i = 0; i < DGBlocks.Count; ++i)
        DGBlocks[i].writeBlock(bw, i == DGBlocks.Count - 1);

      bw.BaseStream.Position = Helpers.HEADER_POS;
      base.writeBlock(bw);
      bw.Write(LinkDG);
      bw.Write(LinkTX);
      bw.Write(LinkPR);
      bw.Write((UInt16)DGBlocks.Count);
      bw.Write(date);
      bw.Write(time);
      bw.Write(author);
      bw.Write(organization);
      bw.Write(project);
      bw.Write(subject);
      bw.Write(timestamp);
      bw.Write(UTCOffset);
      bw.Write((UInt16)TimeQuality);
      bw.Write(timerId);
      return Helpers.HEADER_POS;
    }
    #endregion
  }
}