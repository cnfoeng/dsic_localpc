﻿/*!
 * @file    
 * @brief   Implements the MDF V4 structures.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2014 Joe Nachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

/// <summary>
/// Implements the MDF V4.XX specific stuff.
/// 
/// Advantages over previous MDF versions:
/// - File size could be greater than 4 GB
/// - Measurement data could be compressed
/// </summary>
namespace jnsoft.MDF.V4
{
  /// <summary>
  /// The File Identification Block V4
  /// 
  /// The IDBLOCK always begins at file position 0 and has a constant length of 64 Bytes. 
  /// It contains information to identify the file. This includes information about the 
  /// source of the file and general format specifications.
  /// </summary>
  public sealed class IDBLOCKV4 : IIDBLOCK
  {
    #region ctors
    internal IDBLOCKV4(BinaryReader br)
    {
      var buffer = new byte[8];
      br.Read(buffer, 0, buffer.Length);
      FileID = Helpers.getString(ref buffer);
      br.Read(buffer, 0, buffer.Length);
      FormatId = Helpers.getString(ref buffer);
      br.Read(buffer, 0, buffer.Length);
      ProgramId = Helpers.getString(ref buffer);
      br.ReadUInt32();
      Version = br.ReadUInt16();
      for (int i = 0; i < 15; ++i)
        br.ReadUInt16();
      UnfinalizedFlags = (UnfinalizedFlagsType)br.ReadUInt16();
      CustomFlags = (CustomFlagsType)br.ReadUInt16();
    }
    /// <summary>
    /// Creates a standard conformant IDBlock with the specified programID.
    /// </summary>
    /// <param name="programID">programID String (max 8 Bytes)</param>
    /// <param name="version">Version</param>
    public IDBLOCKV4(string programID, UInt16 version = 410)
    {
      FileID = Resources.strMDFMagicHeader;
      FormatId = Resources.strMDFFormatMagicV4;
      ProgramId = programID;
      Version = version;
    }
    #endregion
    #region properties
#if !DEBUG
    [Browsable(false)]
#endif
    public MDFType Type { get { return MDFType.V4; } }
#if !DEBUG
    [Browsable(false)]
#endif
    public Int64 BlockSize { get { return 64; } }
    [Category(Constants.CATData)]
    public string FileID { get; }
    [Category(Constants.CATData)]
    public string FormatId { get; }
    [Category(Constants.CATData)]
    public string ProgramId { get; }
    [Category(Constants.CATData)]
    public UInt16 Version { get; }
    [Category(Constants.CATData)]
    public UnfinalizedFlagsType UnfinalizedFlags { get; }
    [Category(Constants.CATData)]
    public CustomFlagsType CustomFlags { get; }
    [Browsable(false)]
    public string Name { get { return "ID"; } }
    [Category(Constants.CATNavigation), Description(Constants.DESCTagProperty)]
    public object Tag { get; set; }
    #endregion
    #region methods
    public Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      byte[] buffer;
      Helpers.createArrayFromString(out buffer, 8, FileID, false);
      bw.Write(buffer);
      Helpers.createArrayFromString(out buffer, 8, FormatId, false);
      bw.Write(buffer);
      Helpers.createArrayFromString(out buffer, 8, ProgramId, false);
      bw.Write(buffer);
      bw.Write((UInt32)0);
      bw.Write(Version);
      for (int i = 0; i < 17; ++i)
        bw.Write((UInt16)0);
      return 0;
    }
    #endregion
  }
  /// <summary>
  /// Base class for all BLOCKV4 types (except the IDBLOCK).
  /// </summary>
  public class BLOCKBASEV4 : IMDFBlock
  {
    const byte HASHTAG = 0x23;
    #region data members
    protected UInt64 mNoLinks;
    #endregion
    #region ctors
    public BLOCKBASEV4() { }
    internal BLOCKBASEV4(BinaryReader br) { Int64[] dummy; readData(br, out dummy); }
    /// <summary>
    /// CReates a new instance of BLOCKBASE with the specified parameters.
    /// </summary>
    /// <param name="id">The block id (always 2 characters)</param>
    /// <param name="structSize">The structure size</param>
    /// <param name="noLinks">Number of links</param>
    protected BLOCKBASEV4(string id, Int64 structSize, int noLinks)
    {
      Id = id;
      BlockSize = structSize;
      mNoLinks = (UInt64)noLinks;
    }
    #endregion
    #region properties
    /// <summary>Gets the name of the instance</summary>
    public virtual string Name { get { return Id; } protected set { } }
#if !DEBUG
    [Browsable(false)]
#endif
    public Int64 BlockSize { get; protected set; }
    /// <summary>
    /// Gets the Blocktype (id).
    /// </summary>
#if !DEBUG
    [Browsable(false)]
#endif
    public string Id { get; private set; }
    /// <summary>
    /// Gets or sets a reference to a user object.
    /// </summary>
    [Category(Constants.CATNavigation), Description(Constants.DESCTagProperty)]
    public object Tag { get; set; }
    #endregion
    #region methods
    protected void readData(BinaryReader br, out Int64[] linkList)
    {
      var hashTag1 = br.ReadByte();
      var hashTag2 = br.ReadByte();
      if (hashTag1 != HASHTAG || hashTag2 != HASHTAG)
        throw new NotSupportedException("MDF V4 format not recognized");

      Id = Encoding.ASCII.GetString(br.ReadBytes(2));
      br.ReadUInt32();
      BlockSize = br.ReadInt64();
      mNoLinks = br.ReadUInt64();
      linkList = new Int64[mNoLinks];
      for (UInt64 i = 0; i < mNoLinks; ++i)
        linkList[i] = br.ReadInt64();
    }
    /// <summary>
    /// Writes the block to stream.
    /// </summary>
    /// <param name="bw">The stream to write to</param>
    /// <param name="isLast">true if the last block of a block list is written</param>
    /// <returns>Position from where the write started</returns>
    public virtual Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var pos = bw.BaseStream.Position;
      bw.Write(HASHTAG);
      bw.Write(HASHTAG);
      bw.Write(Encoding.ASCII.GetBytes(Id));
      bw.Write((UInt32)0);
      bw.Write(BlockSize);
      bw.Write(mNoLinks);
      return pos;
    }
    #endregion
    public override string ToString() { return Id; }
  }

  /// <summary>
  /// Base class for SD, MD or RD blocks.
  /// 
  /// Container for signal values of variable length (SD) or Container for data record triples with result of a sample reduction (RD).
  /// </summary>
  public class RDSDBLOCKV4 : BLOCKBASEV4
  {
    #region ctors
    internal RDSDBLOCKV4(BinaryReader br, Int64 link)
    {
      if (link == 0)
        return;
      var position = br.BaseStream.Position;
      br.BaseStream.Position = link;
      Int64[] linkList;
      readData(br, out linkList);
      var size = (int)(BlockSize - 24);
      if (size > 0)
      {
        Data = new byte[size];
        br.Read(Data, 0, size);
      }
      br.BaseStream.Position = position;
    }

    protected RDSDBLOCKV4(string text, bool asMDBlock) : base(asMDBlock ? "MD" : "TX", 24, 0)
    { Data = Encoding.UTF8.GetBytes(text); BlockSize += Data.Length + 1; }

    internal RDSDBLOCKV4(byte[] data, bool asSDBlock) : base(asSDBlock ? "SD" : "RD", 24 + data.Length, 0)
    { Data = data; }
    #endregion
    #region properties
    public byte[] Data { get; }
    #endregion
    #region methods
    public sealed override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = base.writeBlock(bw);
      switch (Id)
      {
        case "MD":
        case "TX":
          if (Data != null)
            bw.Write(Data);
          bw.BaseStream.WriteByte(0);
          break;
        case "RD":
        case "SD":
          if (Data != null)
            bw.Write(Data);
          break;
        default: throw new NotSupportedException(Id);
      }
      while (bw.BaseStream.Position % 8 != 0)
        bw.BaseStream.Position++;
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// The Text Block.
  /// 
  /// The TXBLOCK contains an optional comment for the measured data file, 
  /// channel group or signal, or the long name of a signal.The text length 
  /// results from the block size.
  /// </summary>
  internal sealed class TXBLOCKV4 : RDSDBLOCKV4
  {
    #region ctors
    internal TXBLOCKV4(BinaryReader br, Int64 link) : base(br, link) { }
    internal TXBLOCKV4(string text, bool asMDBlock = false) : base(text, asMDBlock) { }
    #endregion
    #region properties
    /// <summary>
    /// Gets the Block defined text.
    /// </summary>
    public string Text
    {
      get
      {
        byte[] data = Data;
        return Helpers.getString(ref data, Encoding.UTF8);
      }
    }
    #endregion
  }

  /// <summary>
  /// File history block.
  /// </summary>
  public sealed class FHBLOCKV4 : BLOCKBASEV4, IMDFComment
  {
    #region data members
    internal Int64 LinkNextFH;
    UInt64 timestamp;
    #endregion
    #region ctors
    internal FHBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextFH = currLink; break;
          case 1: LinkTX = currLink; break;
        }
      }
      timestamp = br.ReadUInt64();
      UTCOffset = br.ReadInt16();
      DstOffset = br.ReadInt16();
      Flags = (TimeFlagsType)br.ReadByte();
      br.ReadUInt16();
      br.ReadByte();

      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }

    internal FHBLOCKV4(string author, string historyEntry, UInt64 _timestamp, Int16 utcOffset = 0, Int16 dstOffset = 0, TimeFlagsType flags = TimeFlagsType.LocalTime) : base("FH", 56, 2)
    {
      Comment = string.Format(Resources.strMDFFHCommentFmt, historyEntry, Application.ProductVersion, author);
      timestamp = _timestamp;
      UTCOffset = utcOffset;
      DstOffset = dstOffset;
      Flags = flags;
    }
    #endregion
    #region properties
    public DateTime TimeStamp { get { return Helpers.DATE_TIME_1970.AddTicks((long)(timestamp / 100)); } }
    /// <summary>Time zone offset in minutes. The value is not necessarily a multiple of 60 and can be negative!</summary>
    [Category(Constants.CATData)]
    public Int16 UTCOffset { get; }
    /// <summary>Daylight saving time (DST) offset in minutes for start time stamp.</summary>
    [Category(Constants.CATData)]
    public Int16 DstOffset { get; }
    [Category(Constants.CATData)]
    public TimeFlagsType Flags { get; }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    #endregion
    #region methods
    public override long writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkTX = 0;
      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment, true).writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextFH = endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw, isLast);
      for (UInt64 i = 0; i < mNoLinks; ++i)
      {
        switch (i)
        {
          case 0: bw.Write(LinkNextFH); break;
          case 1: bw.Write(LinkTX); break;
          default: bw.Write((UInt64)0); break;
        }
      }
      bw.Write(timestamp);
      bw.Write(UTCOffset);
      bw.Write(DstOffset);
      bw.Write((byte)Flags);
      bw.Write((UInt16)0);
      bw.Write((byte)0);
      bw.BaseStream.Position = endPos;
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// Channel hierarchy block.
  /// </summary>
  public sealed class CHBLOCKV4 : BLOCKBASEV4, IDependencyBlock
  {
    #region data members
    /// <summary>Link to next CHBLOCK</summary>
    internal Int64 LinkNextCH;
    /// <summary>Link to data start</summary>
    List<Int64> DataLinks = new List<Int64>();
    #endregion
    #region ctors
    internal CHBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkCH = 0, LinkName = 0, LinkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextCH = currLink; break;
          case 1: LinkCH = currLink; break;
          case 2: LinkName = currLink; break;
          case 3: LinkTX = currLink; break;
          default:
            if (currLink > 0)
              DataLinks.Add(currLink); break;
        }
      }

      var noOfSignals = br.ReadUInt32();
      HierarchyType = (HierarchyType)br.ReadByte();
      for (int i = 0; i < 3; ++i)
        br.ReadByte();

      for (int i = 0, j = 0; i < noOfSignals && j < DataLinks.Count; ++i, j += 3)
        Dependencies.Add(new DependencyType(DataLinks[j], DataLinks[j + 2], DataLinks[j + 2]));

      Name = new TXBLOCKV4(br, LinkName).Text;
      Comment = new TXBLOCKV4(br, LinkTX).Text;

      var link = LinkCH;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var chBlock = new CHBLOCKV4(br);
        CHBlocks.Add(chBlock);
        link = chBlock.LinkNextCH;
      }
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public override string Name { get; protected set; } = string.Empty;
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData)]
    public HierarchyType HierarchyType { get; }
    [Category(Constants.CATData)]
    public List<DependencyType> Dependencies { get; } = new List<DependencyType>();
    [Category(Constants.CATData)]
    public List<CHBLOCKV4> CHBlocks { get; } = new List<CHBLOCKV4>();
    #endregion
    #region methods
    void IDependencyBlock.createDependencies(Dictionary<long, IMDFBlock> references)
    {
      for (int i = Dependencies.Count - 1; i >= 0; --i)
      {
        var dep = Dependencies[i];
        if (!dep.createDependencies(references))
          // failed to create dependency
          Dependencies.RemoveAt(i);
      }
      foreach (var subBlock in CHBlocks)
        ((IDependencyBlock)subBlock).createDependencies(references);
    }
    #endregion
  }

  /// <summary>
  /// Event block.
  /// 
  /// Description of an event.
  /// </summary>
  public sealed class EVBLOCKV4 : BLOCKBASEV4, IMDFComment
  {
    #region data members
    internal Int64 LinkNextEV;
    List<Int64> ScopeLinks = new List<Int64>();
    #endregion
    #region ctors
    internal EVBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkParent = 0, LinkEVRange = 0, LinkName = 0, LinkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextEV = currLink; break;
          case 1: LinkParent = currLink; break;
          case 2: LinkEVRange = currLink; break;
          case 3: LinkName = currLink; break;
          case 4: LinkTX = currLink; break;
          default:
            if (currLink > 0)
              ScopeLinks.Add(currLink);
            break;
        }
      }
      Type = (EventType)br.ReadByte();
      Sync = (SyncType)br.ReadByte();
      Range = (RangeType)br.ReadByte();
      Cause = (CauseType)br.ReadByte();
      Flags = (EventFlags)br.ReadByte();
      for (int i = 0; i < 3; ++i)
        br.ReadByte();
      var scopeCount = br.ReadUInt32();
      var attachmentCount = br.ReadUInt16();
      CreatorIndex = br.ReadUInt16();
      SyncBaseValue = br.ReadInt64();
      SyncFactor = br.ReadDouble();
      Name = new TXBLOCKV4(br, LinkName).Text;
      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public EventType Type { get; }
    [Category(Constants.CATData), DisplayName("Sync type")]
    public SyncType Sync { get; }
    [Category(Constants.CATData), DisplayName("Range type")]
    public RangeType Range { get; }
    [Category(Constants.CATData), DisplayName("Cause type")]
    public CauseType Cause { get; }
    [Category(Constants.CATData)]
    public EventFlags Flags { get; }
    /// <summary>
    /// Creator index.
    /// 
    /// i.e. zero-based index of FHBLOCK in global list of FHBLOCKs that 
    /// specifies which application has created or changed this event 
    /// (e.g. when generating event offline).
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public UInt16 CreatorIndex { get; }
    /// <summary>
    /// The synchronization value of the event is the product of base value and factor (ev_sync_base_value x ev_sync_factor). See also remark for ev_sync_factor.
    /// 
    /// The calculated synchronization value depends on ev_sync_type:
    /// - For ev_sync_type smaller 4, the synchronization value is a time/angle/distance value relative 
    /// to the respective start value in HDBLOCK.Negative synchronization values can be used for events 
    /// that occurred before the start value.
    /// - For ev_sync_type = 4, the synchronization value is the absolute record index in the channel group 
    /// specified by the scope.The event thus occurred at the same time as the record indicated by the index.
    /// In this case, the value must be rounded to an Integer value between 0 and  cg_cycle_count, i.e. it must 
    /// be less than the number of cycles specified for the channel group.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public Int64 SyncBaseValue { get; }
    /// <summary>
    /// Factor for event synchronization value.
    /// 
    /// The event synchronization value is the product of base value and factor (ev_sync_base_value x ev_sync_factor). 
    /// Generally, base value and factor can be chosen arbitrarily, but as recommendation they should be used to reflect 
    /// the raw value and conversion factor of the master channel to be synchronized with. For instance, assume a time master 
    /// channel using a 64-bit Integer channel data type to store the raw time value in nanoseconds using a linear conversion 
    /// with offset 0 and factor 1e-9. In this case, ev_sync_factor could be set to 1e-9 and ev_sync_base_value could store the 
    /// nanosecond time stamp value. Thus, a higher precision is available than when just specifying the time value in seconds 
    /// as REAL value. For ev_sync_type = 4, ev_sync_factor generally could be set to 1.0, so that the record index will be 
    /// given by ev_sync_base_value.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public double SyncFactor { get; }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    #endregion
    #region methods
    #endregion

  }
  /// <summary>
  /// The Header Block V4.
  /// 
  /// The HDBLOCK always begins at file position 64. It contains general information about 
  /// the contents of the measured data file.
  /// </summary>
  public sealed class HDBLOCKV4 : BLOCKBASEV4, IHDBLOCK, IDisposable
  {
    #region data members
    UInt64 timestamp;
    #endregion
    #region ctors
    internal HDBLOCKV4(BinaryReader br, Dictionary<long, IMDFBlock> references)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkFH = 0, LinkCH = 0, LinkAT = 0, LinkEV = 0, LinkTX = 0, LinkDG = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkDG = currLink; break;
          case 1: LinkFH = currLink; break;
          case 2: LinkCH = currLink; break;
          case 3: LinkAT = currLink; break;
          case 4: LinkEV = currLink; break;
          case 5: LinkTX = currLink; break;
        }
      }

      timestamp = br.ReadUInt64();
      UTCOffset = br.ReadInt16();
      DstOffset = br.ReadInt16();

      Flags = (TimeFlagsType)br.ReadByte();
      TimeQuality = (TimeQualityType)br.ReadByte();

      byte valids = br.ReadByte();
      if ((valids & 0x01) > 0)
        StartAngle = br.ReadDouble();
      else
        br.ReadDouble();
      if ((valids & 0x02) > 0)
        StartDistance = br.ReadDouble();
      else
        br.ReadDouble();

      DGBLOCKV4 block;
      var link = LinkDG;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new DGBLOCKV4(br, references);
        DGBlocks.Add(block);
        link = block.LinkNext;
      }

      link = LinkFH;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var fhBlock = new FHBLOCKV4(br);
        FHBlocks.Add(fhBlock);
        link = fhBlock.LinkNextFH;
      }

      link = LinkCH;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var chBlock = new CHBLOCKV4(br);
        CHBlocks.Add(chBlock);
        link = chBlock.LinkNextCH;
      }

      link = LinkAT;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var atBlock = new ATBLOCKV4(br);
        references[link] = atBlock;
        ATBlocks.Add(atBlock);
        link = atBlock.LinkNextAT;
      }

      link = LinkEV;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var evBlock = new EVBLOCKV4(br);
        references[link] = evBlock;
        EVBlocks.Add(evBlock);
        link = evBlock.LinkNextEV;
      }

      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }

    internal HDBLOCKV4(DateTime dateTime, TimeQualityType timeQuality, string author, string historyEntry, string comment = "") : base("HD", 104, 6)
    {
      TimeSpan since1970 = dateTime.Subtract(Helpers.DATE_TIME_1970);
      timestamp = (UInt64)(since1970.Ticks * 100);
      DateTime utc = DateTime.FromFileTime(dateTime.ToFileTimeUtc());
      TimeSpan ts = dateTime.Subtract(utc);
      UTCOffset = Convert.ToInt16(Math.Round(TimeZone.CurrentTimeZone.GetUtcOffset(utc).TotalMinutes));
      Flags = TimeFlagsType.OffsetsValid;
      TimeQuality = timeQuality;
      FHBlocks.Add(new FHBLOCKV4(author, historyEntry, timestamp, UTCOffset, DstOffset, Flags));
      Comment = comment;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public DateTime RecordingTime { get { return Helpers.DATE_TIME_1970.AddTicks((long)(timestamp / 100)); } }
    /// <summary>Time zone offset in minutes. The value is not necessarily a multiple of 60 and can be negative!</summary>
    [Category(Constants.CATData)]
    public Int16 UTCOffset { get; }
    /// <summary>Daylight saving time (DST) offset in minutes for start time stamp.</summary>
    [Category(Constants.CATData)]
    public Int16 DstOffset { get; }
    [Category(Constants.CATData)]
    public TimeFlagsType Flags { get; }
    [Category(Constants.CATData)]
    public TimeQualityType TimeQuality { get; }
    [Category(Constants.CATData), DefaultValue(double.NaN)]
    public double StartAngle { get; } = double.NaN;
    [Category(Constants.CATData), DefaultValue(double.NaN)]
    public double StartDistance { get; } = double.NaN;
    [Browsable(false)]
    public List<IDGBLOCK> DGBlocks { get; } = new List<IDGBLOCK>();
    [Category(Constants.CATDataLink)]
    public List<FHBLOCKV4> FHBlocks { get; } = new List<FHBLOCKV4>();
    [Category(Constants.CATDataLink)]
    public List<CHBLOCKV4> CHBlocks { get; } = new List<CHBLOCKV4>();
    [Category(Constants.CATDataLink)]
    public List<ATBLOCKV4> ATBlocks { get; } = new List<ATBLOCKV4>();
    [Category(Constants.CATDataLink)]
    public List<EVBLOCKV4> EVBlocks { get; } = new List<EVBLOCKV4>();
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    public override string Name { get { return "Header"; } }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      bw.BaseStream.Position = Helpers.HEADER_POS + BlockSize;

      Int64 LinkFH = 0, LinkCH = 0, LinkAT = 0, LinkEV = 0, LinkTX = 0, LinkDG = 0;
      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment).writeBlock(bw);

      LinkDG = DGBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < DGBlocks.Count; ++i)
        DGBlocks[i].writeBlock(bw, i == DGBlocks.Count - 1);

      LinkFH = FHBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < FHBlocks.Count; ++i)
        FHBlocks[i].writeBlock(bw, i == FHBlocks.Count - 1);

      LinkCH = CHBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < CHBlocks.Count; ++i)
        CHBlocks[i].writeBlock(bw, i == CHBlocks.Count - 1);

      LinkAT = ATBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < ATBlocks.Count; ++i)
        ATBlocks[i].writeBlock(bw, i == ATBlocks.Count - 1);

      LinkEV = EVBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < EVBlocks.Count; ++i)
        EVBlocks[i].writeBlock(bw, i == EVBlocks.Count - 1);

      bw.BaseStream.Position = Helpers.HEADER_POS;
      base.writeBlock(bw);
      bw.Write(LinkDG);
      bw.Write(LinkFH);
      bw.Write(LinkCH);
      bw.Write(LinkAT);
      bw.Write(LinkEV);
      bw.Write(LinkTX);

      bw.Write(timestamp);
      bw.Write(UTCOffset);
      bw.Write(DstOffset);
      bw.Write((byte)Flags);
      bw.Write((byte)TimeQuality);
      byte valids = 0;
      if (!double.IsNaN(StartAngle))
        valids |= 0x01;
      if (!double.IsNaN(StartDistance))
        valids |= 0x02;
      bw.Write(valids);
      bw.Write(double.IsNaN(StartAngle) ? 0.0 : StartAngle);
      bw.Write(double.IsNaN(StartDistance) ? 0.0 : StartDistance);
      return Helpers.HEADER_POS;
    }

    public void Dispose()
    {
      foreach (var dgBlock in DGBlocks)
        ((IDisposable)dgBlock).Dispose();
    }
    #endregion
  }

  /// <summary>
  /// Base class for V4 DataGroups like SRBLOCKV4 and DGBLOCKV4.
  /// </summary>
  public abstract class DataContainerV4 : BLOCKBASEV4, IDataContainer, IDisposable
  {
    #region data members
    internal Int64 LinkNext;
    internal protected BinaryReader mBr;
    internal DataReader mReader;
    #endregion
    #region ctors
    /// <summary>
    /// Creates a new instance of DataContainerV4 with the specified parameters.
    /// </summary>
    /// <param name="id">The block id (always 2 characters)</param>
    /// <param name="structSize">The structure size</param>
    /// <param name="noLinks">Number of links</param>
    protected DataContainerV4(string id, Int64 structSize, int noLinks) : base(id, structSize, noLinks) { }
    protected DataContainerV4() { }
    /// <summary>
    /// Creates data container from the specified (data container)linkList.
    /// 
    /// The containers will be added to the Blocks property.
    /// </summary>
    /// <param name="br">data stream to read from</param>
    /// <param name="linkList">links-List</param>
    /// <param name="dataLinkIdx">Index into the LinkList to identify the data container link</param>
    protected void readDataContainer(BinaryReader br, out Int64[] linkList, int dataLinkIdx)
    {
      mBr = br;
      readData(br, out linkList);
      LinkNext = linkList[0];
      var link = linkList[dataLinkIdx];
      var currPos = br.BaseStream.Position;
      try
      {
        while (link > 0)
        {
          br.BaseStream.Position = link;
          var baseBlock = new BLOCKBASEV4(br);
          br.BaseStream.Position = link;
          switch (baseBlock.Id)
          {
            case "RD": link = 0; Blocks.Add(new RDSDBLOCKV4(br, link)); break;
            case "HL": link = 0; Blocks.Add(new HLBLOCKV4(br)); break;
            case "DT": link = 0; Blocks.Add(new DTBLOCKV4(br)); break;

            case "DL":
              var dlBlock = new DLBLOCKV4(br);
              Blocks.Add(dlBlock);
              link = dlBlock.LinkNextDL;
              break;

            case "DZ":
              link = 0;
              byte[] compressedData;
              var dzBlock = new DZBLOCKV4(br, out compressedData);
              if (mReader == null)
              {
                mReader = new DataReader();
                Blocks.Add(dzBlock);
              }
              mReader.Add(dzBlock, compressedData);
              mReader.createReader();
              break;
          }
        }
      }
      finally { br.BaseStream.Position = currPos; }
    }
    #endregion
    #region properties
    [Category(Constants.CATDataLink)]
    public List<BLOCKBASEV4> Blocks { get; private set; } = new List<BLOCKBASEV4>();
    #endregion
    #region methods
    BinaryReader IDataContainer.getReader(long offset)
    {
      string id = Blocks[0].Id;
      switch (id)
      {
        case "DT":
          mBr.BaseStream.Position = ((DTBLOCKV4)Blocks[0]).BasePosition + offset;
          return mBr;

        case "DZ":
          mReader.Position = offset;
          return mReader.Reader;

        case "DL":
          return ((DLBLOCKV4)Blocks[0]).getReader(offset);

        case "HL":
          return ((HLBLOCKV4)Blocks[0]).DLBlocks[0].getReader(offset);

        default: throw new NotSupportedException(id);
      }
    }

    bool IDataContainer.readData(Int64 offset, ref byte[] buffer)
    {
      try
      {
        var reader = ((IDGBLOCK)this).getReader(offset);
        if (reader == null)
          return false;
        reader.Read(buffer, 0, buffer.Length);
        return true;
      }
      catch { return false; }
    }

    public void Dispose()
    {
      if (mReader != null)
        mReader.Dispose();
      foreach (var block in Blocks)
        if (block is IDisposable)
          ((IDisposable)block).Dispose();
    }
    #endregion
  }

  /// <summary>
  /// The Data Group Block V4.
  /// 
  /// The DGBLOCK gathers information and links related to its data block. Thus the branch in the tree of 
  /// MDF blocks that is opened by the DGBLOCK contains all information necessary to understand and 
  /// decode the data block referenced by the DGBLOCK.  
  /// 
  /// The DGBLOCK can contain several channel groups. In this case the MDF file is "unsorted". If there is
  /// only one channel group in the DGBLOCK, the MDF file is "sorted" (please refer to chapter 4 for details).
  /// </summary>
  public sealed class DGBLOCKV4 : DataContainerV4, IDGBLOCK
  {
    #region ctors
    internal DGBLOCKV4(BinaryReader br, Dictionary<long, IMDFBlock> references)
    {
      mBr = br;
      Int64[] linkList;
      readDataContainer(br, out linkList, 2);

      Int64 LinkDL = 0, LinkTX = 0, LinkCG = 0;
      for (int i = 1; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 1: LinkCG = currLink; break;
          case 2: LinkDL = currLink; break;
          case 3: LinkTX = currLink; break;
        }
      }
      RecordIDType = (RecordIDType)br.ReadByte();

      CGBLOCKV4 block;
      var link = LinkCG;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new CGBLOCKV4(this, br, references);
        CGBlocks.Add(block);
        link = block.LinkNextCG;
      }

      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }
    public DGBLOCKV4() : base("DG", 64, 4) { }
    #endregion
    #region events
    public event EventHandler<WriteRawDataEventArgs> OnWriteRawData;
    #endregion
    #region properties
    /// <summary>
    /// Gets the record ID type.
    /// </summary>
    [Category(Constants.CATData)]
    public RecordIDType RecordIDType { get; }
    [Category(Constants.CATData)]
    public string Comment { get; }
    public override string Name { get { return "Data group"; } }
    [Browsable(false)]
    public List<ICGBLOCK> CGBlocks { get; } = new List<ICGBLOCK>();
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkDL = 0, LinkTX = 0, LinkCG = 0;
      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment).writeBlock(bw);

      // Write CG Blocks
      LinkCG = CGBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < CGBlocks.Count; ++i)
        CGBlocks[i].writeBlock(bw, i == CGBlocks.Count - 1);

      if (OnWriteRawData != null)
      { // Write Data blocks
        LinkDL = bw.BaseStream.Position;
        OnWriteRawData(this, new WriteRawDataEventArgs(this, bw));
      }

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNext = endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkNext);
      bw.Write(LinkCG);
      bw.Write(LinkDL);
      bw.Write(LinkTX);

      bw.Write((byte)RecordIDType);
      bw.BaseStream.Position = endPos;
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// Header information for linked list of data blocks.
  /// </summary>
  public sealed class HLBLOCKV4 : BLOCKBASEV4, IDisposable
  {
    #region ctors
    internal HLBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Flags = (DataBlockFlags)br.ReadUInt16();
      ZipType = (ZipType)br.ReadByte();

      var link = linkList[0];
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var dlBlock = new DLBLOCKV4(br);
        DLBlocks.Add(dlBlock);
        link = dlBlock.LinkNextDL;
      }
    }
    public HLBLOCKV4(ZipType zipType, DataBlockFlags flags = DataBlockFlags.EqualLength) : base("HL", 40, 1)
    {
      ZipType = zipType;
      Flags = flags;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public DataBlockFlags Flags { get; }
    [Category(Constants.CATData)]
    public ZipType ZipType { get; }
    [Category(Constants.CATDataLink)]
    public List<DLBLOCKV4> DLBlocks { get; } = new List<DLBLOCKV4>();
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      base.writeBlock(bw, isLast);
      bw.Write((Int64)(startPos + BlockSize));
      bw.Write((UInt16)Flags);
      bw.Write((byte)ZipType);
      bw.Write((byte)0);
      bw.Write((UInt32)0);
      return startPos;
    }
    public void Dispose()
    {
      foreach (var dlblock in DLBlocks)
        dlblock.Dispose();
    }
    #endregion
  }

  /// <summary>
  /// Container for data records with signal values.
  /// </summary>
  public sealed class DTBLOCKV4 : BLOCKBASEV4
  {
    #region ctors
    internal DTBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);
      BasePosition = br.BaseStream.Position;
    }
    internal DTBLOCKV4(Int64 size) : base("DT", 24 + size, 0) { }
    #endregion
    #region properties
    public Int64 Size { get { return BlockSize - 24; } }
    public Int64 BasePosition { get; }
    #endregion
  }

  /// <summary>
  /// DZ Block.
  /// 
  /// Container for zipped (compressed) data section of a DT/SD/RD block as replacement of such a block.
  /// </summary>
  public sealed class DZBLOCKV4 : BLOCKBASEV4
  {
    #region ctors
    internal DZBLOCKV4(BinaryReader br, out byte[] compressedData)
    {
      Int64[] linkList;
      readData(br, out linkList);
      BlockType = Encoding.ASCII.GetString(br.ReadBytes(2));
      ZipType = (ZipType)br.ReadByte();
      br.ReadByte();
      ZipParameter = br.ReadUInt32();
      Size = br.ReadInt64();
      Length = br.ReadUInt64();

      switch (ZipType)
      {
        case ZipType.Deflate:
          compressedData = br.ReadBytes((int)Length);
          break;
        case ZipType.TransposeAndDeflate:
          compressedData = br.ReadBytes((int)Length);
          break;
        default: throw new NotSupportedException(ZipType.ToString());
      }
    }

    public DZBLOCKV4(UInt64 compressedSize, Int64 uncompressedSize
      , ZipType zipType
      , UInt32 zipParameter
      , string blockType = "DT")
      : base("DZ", 48 + (Int64)compressedSize, 0)
    {
      Size = uncompressedSize;
      Length = compressedSize;
      BlockType = blockType;
      ZipType = zipType;
      ZipParameter = zipParameter;
    }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    string BlockType { get; }
    [Category(Constants.CATData)]
    public ZipType ZipType { get; }
    [Category(Constants.CATData)]
    public UInt32 ZipParameter { get; }
    /// <summary>Uncompressed size of data block.</summary>
    public Int64 Size { get; }
    /// <summary>Compressed size of data block.</summary>
    [Category(Constants.CATData)]
    public UInt64 Length { get; }
    #endregion
    #region methods
    public override long writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      base.writeBlock(bw, isLast);
      bw.Write(Encoding.ASCII.GetBytes(BlockType));
      bw.Write((byte)ZipType);
      bw.BaseStream.Position += 1;
      bw.Write(ZipParameter);
      bw.Write(Size);
      bw.Write(Length);
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// DL Block.
  /// 
  /// Ordered list of links to (signal/reduction) data blocks if the data is spread over multiple blocks.
  /// </summary>
  public sealed class DLBLOCKV4 : BLOCKBASEV4, IDisposable
  {
    #region data members
    internal Int64 LinkNextDL;
    BinaryReader mBr;
    DataReader mReader;
    List<Int64> mDataLinks;
    #endregion
    #region ctors
    internal DLBLOCKV4(BinaryReader br)
    {
      mBr = br;
      Int64[] linkList;
      readData(br, out linkList);
      LinkNextDL = linkList[0];

      Flags = (DataBlockFlags)br.ReadByte();
      br.ReadUInt16();
      br.ReadByte();
      Count = br.ReadUInt32();
      EqualLength = br.ReadUInt64();

      for (int i = 1; i < linkList.Length; ++i)
      {
        var currLink = linkList[i];
        if (currLink <= 0)
          continue;
        br.BaseStream.Position = currLink;
        var baseBlock = new BLOCKBASEV4(br);
        br.BaseStream.Position = currLink;
        switch (baseBlock.Id)
        {
          case "DT":
            DataBlocks.Add(new DTBLOCKV4(br));
            break;
          case "DZ":
            byte[] compressedData;
            var dzBlock = new DZBLOCKV4(br, out compressedData);

            if (mReader == null)
              mReader = new DataReader();

            mReader.Add(dzBlock, compressedData);
            DataBlocks.Add(dzBlock);
            break;
        }
      }

      if (mReader != null)
        mReader.createReader();
    }

    public DLBLOCKV4(DataBlockFlags flags, int count) : base("DL", 48 + 8 * count, 1 + count)
    {
      Count = (uint)count;
      Flags = flags;
      mDataLinks = new List<Int64>();
    }
    #endregion
    #region properties
    public DataBlockFlags Flags { get; }
    public UInt32 Count { get; }
    public UInt64 EqualLength { get; private set; }
    public List<BLOCKBASEV4> DataBlocks { get; private set; } = new List<BLOCKBASEV4>();
    #endregion
    #region methods
    /// <summary>
    /// Adds the specified data block links before teh instance gets written.
    /// </summary>
    /// <param name="equalLength"></param>
    /// <param name="dataLinks"></param>
    internal void addDataLinksBeforeWrite(UInt64 equalLength, ref Int64[] dataLinks)
    {
      EqualLength = equalLength;
      mDataLinks.AddRange(dataLinks);
    }

    public override long writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextDL = bw.BaseStream.Position + BlockSize;
      base.writeBlock(bw);
      bw.Write(LinkNextDL);
      foreach (var link in mDataLinks)
        bw.Write(link);
      bw.Write((byte)Flags);
      bw.Write((UInt16)0);
      bw.Write((byte)0);
      bw.Write(Count);
      bw.Write(EqualLength);
      return startPos;
    }

    /// <summary>
    /// Gets the BinaryReader and its position for the specified Data offset.
    /// </summary>
    /// <param name="offset"></param>
    /// <returns>The Binary reader (including position) or null if failed</returns>
    internal BinaryReader getReader(long offset)
    {
      if (DataBlocks.Count == 0)
        return null;

      if (mReader != null)
      { // expanded linearized DZ blocks
        if (offset > mReader.Reader.BaseStream.Length)
          // expanded file is too short
          return null;
        mReader.Position = offset;
        return mReader.Reader;
      }

      // DT version (add local reader base position)
      long currOffset = 0; int i = 0;
      while (currOffset <= offset && i < DataBlocks.Count)
      {
        var dataBlock = (DTBLOCKV4)DataBlocks[i++];
        if (offset < currOffset + dataBlock.Size)
        { // requested offset is within thie current Data block
          mBr.BaseStream.Position = dataBlock.BasePosition + (offset - currOffset);
          return mBr;
        }
        currOffset += dataBlock.Size;
      }
      return null;
    }

    public void Dispose()
    {
      if (mReader != null)
      {
        mReader.Dispose();
        mReader = null;
      }
    }
    #endregion
  }

  /// <summary>
  /// Source information block.
  /// 
  /// Specifies source information for a channel or for the acquisition of a channel group.
  /// </summary>
  public sealed class SIBLOCKV4 : BLOCKBASEV4, IMDFComment
  {
    #region ctors
    internal SIBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkName = 0, LinkPath = 0, LInkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkName = currLink; break;
          case 1: LinkPath = currLink; break;
          case 2: LInkTX = currLink; break;
        }
      }
      Source = (SourceType)br.ReadByte();
      Bus = (BusType)br.ReadByte();
      Flags = (SourceFlags)br.ReadByte();
      Name = new TXBLOCKV4(br, LinkName).Text;
      Path = new TXBLOCKV4(br, LinkPath).Text;
      Comment = new TXBLOCKV4(br, LInkTX).Text;
    }
    internal SIBLOCKV4(SourceType source
      , BusType bus = BusType.NONE
      , string name = null, string path = null, string comment = null
      , SourceFlags flags = SourceFlags.None
      ) : base("SI", 56, 3)
    {
      Source = source;
      Name = name;
      Path = path;
      Comment = comment;
      Bus = bus;
      Flags = flags;
    }
    #endregion
    #region properties
    [Category(Constants.CATData), DefaultValue("")]
    public override string Name { get; protected set; }
    [Category(Constants.CATData), DefaultValue("")]
    public string Path { get; set; } = string.Empty;
    [Category(Constants.CATData), DefaultValue("")]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData), DefaultValue(SourceType.OTHER)]
    public SourceType Source { get; set; }
    [Category(Constants.CATData), DefaultValue(BusType.NONE)]
    public BusType Bus { get; set; }
    [Category(Constants.CATData), DefaultValue(SourceFlags.None)]
    public SourceFlags Flags { get; set; }
    #endregion
    #region methods
    /// <summary>
    /// Writes the block to stream.
    /// </summary>
    /// <param name="bw">The stream to write to</param>
    /// <param name="isLast">true if the last block of a block list is written</param>
    /// <returns>Position from where the write started</returns>
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkName = 0, LinkPath = 0, LinkComment = 0;

      if (!string.IsNullOrEmpty(Name))
        LinkName = new TXBLOCKV4(Name).writeBlock(bw);

      if (!string.IsNullOrEmpty(Path))
        LinkPath = new TXBLOCKV4(Path).writeBlock(bw);

      if (!string.IsNullOrEmpty(Comment))
        LinkComment = new TXBLOCKV4(Comment).writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);

      bw.Write(LinkName);
      bw.Write(LinkPath);
      bw.Write(LinkComment);

      bw.Write((byte)Source);
      bw.Write((byte)Bus);
      bw.Write((byte)Flags);
      bw.Write((byte)0);
      bw.Write((UInt32)0);
      bw.BaseStream.Position = endPos;
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// The Channel Group Block.
  /// 
  /// The CGBLOCK contains a collection of channels which are stored in a record, 
  ///i.e. which have the same time sampling.
  /// </summary>
  public sealed class CGBLOCKV4 : BLOCKBASEV4, ICGBLOCK, IDisposable
  {
    #region data member
    internal Int64 LinkNextCG;
    #endregion
    #region ctors
    internal CGBLOCKV4(IDGBLOCK dgBlock, BinaryReader br, Dictionary<long, IMDFBlock> references)
    {
      DGBlock = dgBlock;
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkAN = 0, LinkSI = 0, LinkSR = 0, LinkTX = 0, LinkCN = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextCG = currLink; break;
          case 1: LinkCN = currLink; break;
          case 2: LinkAN = currLink; break;
          case 3: LinkSI = currLink; break;
          case 4: LinkSR = currLink; break;
          case 5: LinkTX = currLink; break;
        }
      }
      RecordID = br.ReadUInt64();
      RecordCount = br.ReadUInt64();
      Flags = (ChannelGroupFlags)br.ReadUInt16();
      PathSeparator = br.ReadUInt16().ToString()[0];
      br.ReadUInt32();
      RecordSize = br.ReadUInt32();
      InvalSize = br.ReadUInt32();

      CNBLOCKV4 block;
      var link = LinkCN;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        references[link] = block = new CNBLOCKV4(this, br, references);
        CNBlocks.Add(block);
        link = block.LinkNextCN;
      }
      CNBlocks.Sort((x, y) => { return (x.AddOffset + x.BitOffset / 8).CompareTo(y.AddOffset + y.BitOffset / 8); });

      AcquisitionName = new TXBLOCKV4(br, LinkAN).Text;

      link = LinkSI;
      if (link > 0)
      {
        br.BaseStream.Position = link;
        AcquisitionSource = new SIBLOCKV4(br);
      }

      link = LinkSR;
      while (link > 0)
      {
        br.BaseStream.Position = link;
        var srBlock = new SRBLOCKV4(br);
        SRBlocks.Add(srBlock);
        link = srBlock.LinkNext;
      }

      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }

    public CGBLOCKV4(IDGBLOCK dgBlock, string comment = "") : base("CG", 104, 6) { DGBlock = dgBlock; Comment = comment; }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    /// <summary>
    /// Record ID, i.e. value of the identifier for a record if the DGBLOCK defines a number of record IDs > 0
    /// </summary>
    public UInt64 RecordID { get; }
    /// <summary>
    /// Size of data record in Bytes (without record ID), 
    /// i.e. size of plain data for a each recorded sample of this channel group.
    /// </summary>
    [Category(Constants.CATData), DisplayName("Record size"), ReadOnly(true)]
    public UInt32 RecordSize { get; set; }
    /// <summary>
    /// Number of records of this type in the data block,
    /// i.e. number of samples for this channel group.
    /// </summary>
    [Category(Constants.CATData), DisplayName("Record count"), ReadOnly(true)]
    public UInt64 RecordCount { get; set; }
    [Browsable(false)]
    public override string Name { get { return "Channel group"; } }
    [Category(Constants.CATData)]
    public string AcquisitionName { get; set; }
    [Category(Constants.CATData)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData)]
    public ChannelGroupFlags Flags { get; }
    [Category(Constants.CATData)]
    public UInt32 InvalSize { get; }
    [Category(Constants.CATData)]
    public char PathSeparator { get; }
    [Category(Constants.CATNavigation)]
    public IDGBLOCK DGBlock { get; }
    [Category(Constants.CATDataLink)]
    public SIBLOCKV4 AcquisitionSource { get; set; }
    [Browsable(false)]
    public List<ICNBLOCK> CNBlocks { get; } = new List<ICNBLOCK>();
    [Category(Constants.CATDataLink)]
    public List<ISRBLOCK> SRBlocks { get; } = new List<ISRBLOCK>();
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkAN = 0, LinkSI = 0, LinkSR = 0, LinkTX = 0, LinkCN = 0;
      LinkCN = CNBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < CNBlocks.Count; ++i)
        CNBlocks[i].writeBlock(bw, i == CNBlocks.Count - 1);

      if (!string.IsNullOrEmpty(AcquisitionName))
        LinkAN = new TXBLOCKV4(AcquisitionName).writeBlock(bw);

      if (AcquisitionSource != null)
        LinkSI = AcquisitionSource.writeBlock(bw);

      LinkSR = SRBlocks.Count > 0 ? bw.BaseStream.Position : 0;
      for (int i = 0; i < SRBlocks.Count; ++i)
        SRBlocks[i].writeBlock(bw, i == SRBlocks.Count - 1);

      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment).writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextCG = endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);

      bw.Write(LinkNextCG);
      bw.Write(LinkCN);
      bw.Write(LinkAN);
      bw.Write(LinkSI);
      bw.Write(LinkSR);
      bw.Write(LinkTX);

      bw.Write(RecordCount > 0 ? 1ul : RecordID);
      bw.Write(RecordCount);
      bw.Write((UInt16)Flags);
      bw.Write((UInt16)PathSeparator);
      bw.Write((UInt32)0);
      bw.Write(RecordSize);
      bw.Write(InvalSize);
      bw.BaseStream.Position = endPos;
      return startPos;
    }

    Int64 ICGBLOCK.getRecordSize()
    {
      var size = RecordSize;
      switch (DGBlock.RecordIDType)
      {
        case RecordIDType.BeforeAndAfter8Bit: size += 2; break;
        case RecordIDType.Before8Bit: size += 1; break;
        case RecordIDType.Before16Bit: size += 2; break;
        case RecordIDType.Before32Bit: size += 4; break;
        case RecordIDType.Before64Bit: size += 8; break;
      }
      return size;
    }

    public void Dispose()
    {
      foreach (var sr in SRBlocks)
        if (sr is IDisposable)
          ((IDisposable)sr).Dispose();
    }
    #endregion
  }

  /// <summary>
  /// The Sample Reduction Block.
  /// </summary>
  public sealed class SRBLOCKV4 : DataContainerV4, ISRBLOCK
  {
    #region ctors
    internal SRBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readDataContainer(br, out linkList, 1);

      Int64 LinkDB = linkList[1];
      NrOfRedSamples = br.ReadUInt64();
      LenOfTimeInt = br.ReadDouble();
      Flags = (SRFlags)br.ReadByte();
    }
    #endregion
    #region events
    public event EventHandler<WriteRawDataEventArgs> OnWriteRawData;
    #endregion
    #region properties
    [Category(Constants.CATData), DisplayName(V3.SRBLOCK.DP_NAME_RED_SAMPLES)]
    public UInt64 NrOfRedSamples { get; }
    [Category(Constants.CATData), DisplayName(V3.SRBLOCK.DP_NAME_LEN_TIME)]
    public double LenOfTimeInt { get; }
    [Category(Constants.CATData)]
    public SRFlags Flags { get; }
    #endregion
    #region methodes
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkDB = 0;

      if (OnWriteRawData != null)
      { // Write Data blocks
        LinkDB = bw.BaseStream.Position;
        OnWriteRawData(this, new WriteRawDataEventArgs(this, bw));
      }

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNext = endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      bw.Write(LinkNext);
      bw.Write(LinkDB);
      bw.Write(NrOfRedSamples);
      bw.Write(LenOfTimeInt);
      bw.Write((byte)Flags);
      bw.BaseStream.Position = endPos;
      return startPos;
    }
    #endregion
  }

  /// <summary>
  /// Container for binary data or a reference to an external file.
  /// </summary>
  public sealed class ATBLOCKV4 : BLOCKBASEV4, IMDFBlock
  {
    #region data members
    internal Int64 LinkNextAT;
    #endregion
    #region ctors
    internal ATBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkFileName = 0, LinkMimeType = 0, LinkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextAT = currLink; break;
          case 1: LinkFileName = currLink; break;
          case 2: LinkMimeType = currLink; break;
          case 3: LinkTX = currLink; break;
        }
      }
      AttachmentFlags = (AttachmentFlags)br.ReadUInt16();
      CreatorIndex = br.ReadUInt16();
      br.ReadUInt32();
      Filename = new TXBLOCKV4(br, LinkFileName).Text;
      MimeType = new TXBLOCKV4(br, LinkMimeType).Text;
      Comment = new TXBLOCKV4(br, LinkTX).Text;
    }
    #endregion
    #region properties
    [Category(Constants.CATData), ReadOnly(true)]
    public string Filename { get; } = string.Empty;
    [Category(Constants.CATData), ReadOnly(true)]
    public string MimeType { get; } = string.Empty;
    [Category(Constants.CATData), ReadOnly(true)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData), ReadOnly(true)]
    public AttachmentFlags AttachmentFlags { get; }
    /// <summary>
    /// Creator index.
    /// 
    /// i.e. zero-based index of FHBLOCK in global list of FHBLOCKs that specifies 
    /// which application has created this attachment, or changed it most recently.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true)]
    public UInt16 CreatorIndex { get; }
    #endregion
  }

  /// <summary>
  /// The Channel Block.
  /// 
  /// The channel block contains detailed information
  /// about a signal and how it is stored in the record.
  /// </summary>
  public sealed class CNBLOCKV4 : BLOCKBASEV4, ICNBLOCK
  {
    #region data members
    internal Int64 LinkNextCN;
    List<Int64> mAttachmentLinks = new List<Int64>();
    #endregion
    #region ctors
    internal CNBLOCKV4(ICGBLOCK cgBlock, BinaryReader br, Dictionary<long, IMDFBlock> references)
    {
      CGBlock = cgBlock;
      Int64[] linkList;
      readData(br, out linkList);

      Int64 LinkCM = 0, LinkName = 0, LinkSI = 0, LinkCC = 0, LinkSD = 0, LinkUnit = 0, LinkTX = 0;
      for (int i = 0; i < linkList.Length; ++i)
      {
        Int64 currLink = linkList[i];
        switch (i)
        {
          case 0: LinkNextCN = currLink; break;
          case 1: LinkCM = currLink; break;
          case 2: LinkName = currLink; break;
          case 3: LinkSI = currLink; break;
          case 4: LinkCC = currLink; break;
          case 5: LinkSD = currLink; break;
          case 6: LinkUnit = currLink; break;
          case 7: LinkTX = currLink; break;
          default:
            var atLink = currLink;
            if (atLink > 0)
              mAttachmentLinks.Add(atLink); break;
        }
      }
      ChannelType = (ChannelType)br.ReadByte();
      SyncType = (SyncType)br.ReadByte();
      SignalType = (SignalType)br.ReadByte();
      BitOffset = br.ReadByte();
      AddOffset = br.ReadUInt32();
      NoOfBits = br.ReadUInt32();
      Flags = (ChannelFlags)br.ReadUInt32();
      InvalBitPos = br.ReadUInt32();
      Precision = br.ReadByte();
      br.ReadByte();
      var attachmentCount = br.ReadUInt16();

      if ((Flags & ChannelFlags.ValueRangeValid) > 0)
      {
        MinRaw = br.ReadDouble();
        MaxRaw = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      if ((Flags & ChannelFlags.LimitRangeValid) > 0)
      {
        Min = br.ReadDouble();
        Max = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      if ((Flags & ChannelFlags.ExtendedLimitRangeValid) > 0)
      {
        MinEx = br.ReadDouble();
        MaxEx = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      Name = new TXBLOCKV4(br, LinkName).Text;
      Unit = new TXBLOCKV4(br, LinkUnit).Text;
      Comment = new TXBLOCKV4(br, LinkTX).Text;

      if (LinkSI > 0)
      {
        br.BaseStream.Position = LinkSI;
        SIBlock = new SIBLOCKV4(br);
      }

      if (LinkCC > 0)
      {
        br.BaseStream.Position = LinkCC;
        CCBlock = new CCBLOCKV4(br);
        if (double.IsNaN(Min) && !double.IsNaN(CCBlock.Min))
          Min = CCBlock.Min;
        if (double.IsNaN(Max) && !double.IsNaN(CCBlock.Max))
          Max = CCBlock.Max;
      }

      if (LinkSD > 0)
      {
        IMDFBlock referencedBlock;
        if (references.TryGetValue(LinkSD, out referencedBlock))
          SDBlock = (BLOCKBASEV4)referencedBlock;
        else
        {
          br.BaseStream.Position = LinkSD;
          var baseBlock = new BLOCKBASEV4(br);
          br.BaseStream.Position = LinkSD;
          switch (baseBlock.Id)
          {
            case "AT":
              SDBlock = new ATBLOCKV4(br);
              break;
            case "HL":
              SDBlock = new HLBLOCKV4(br);
              break;
            case "DL":
              SDBlock = new DLBLOCKV4(br);
              break;
            case "DZ":
              byte[] compressedData;
              SDBlock = new DZBLOCKV4(br, out compressedData);
              var reader = new DataReader();
              reader.Add((DZBLOCKV4)SDBlock, compressedData);
              break;
          }
        }
      }
    }

    internal CNBLOCKV4(SignalType signalType, ChannelType channelType
      , string name, string description
      , UInt32 bitOffset, UInt32 addOffset, UInt32 noOfBits
      , ICCBLOCK ccBlock = null
      , SyncType syncType = SyncType.None
      , ChannelFlags flags = ChannelFlags.None
      , UInt32 invalBitPos = 0
      , byte precision = 0
      )
      : base("CN", 160, 8)
    {
      SignalType = signalType;
      ChannelType = channelType;
      Name = name;
      Comment = description;
      SyncType = syncType;
      NoOfBits = noOfBits;

      AddOffset = (addOffset + bitOffset) / 8;
      BitOffset = (byte)(bitOffset % 8);

      Flags = flags;
      InvalBitPos = invalBitPos;
      Precision = precision;
      CCBlock = ccBlock;
    }
    #endregion
    #region properties
    public string Unit { get; }
    [Category(Constants.CATData)]
    public override string Name { get; protected set; }
    [Category(Constants.CATData), ReadOnly(true)]
    public string Comment { get; set; } = string.Empty;
    [Category(Constants.CATData), DisplayName("Bit offset")]
    public UInt16 BitOffset { get; }
    [Category(Constants.CATData), DisplayName("Number of Bits"), ReadOnly(true)]
    public UInt32 NoOfBits { get; set; }
    /// <summary>Gets the channel's type</summary>
    [Category(Constants.CATData), DisplayName("Type")]
    public ChannelType ChannelType { get; }
    [Category(Constants.CATData), DisplayName("Sync type")]
    public SyncType SyncType { get; }
    /// <summary>Gets the signal type.</summary>
    [Category(Constants.CATData), DisplayName("Signal type")]
    public SignalType SignalType { get; }
    [Category(Constants.CATData), DisplayName("Additional byte offset")]
    public UInt32 AddOffset { get; }
    /// <summary>Channel specific flags</summary>
    [Category(Constants.CATData)]
    public ChannelFlags Flags { get; private set; }
    [Category(Constants.CATData)]
    public UInt32 InvalBitPos { get; }
    [Category(Constants.CATData)]
    public byte Precision { get; }
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MinRaw { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MaxRaw { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double Min { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double Max { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MinEx { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit), DefaultValue(double.NaN), ReadOnly(true)]
    public double MaxEx { get; set; } = double.NaN;
    [Category(Constants.CATNavigation)]
    public ICGBLOCK CGBlock { get; }
    [Category(Constants.CATDataLink)]
    public ICCBLOCK CCBlock { get; set; }
    /// <summary>Composition of channels: Pointer to channel array block(CABLOCK) or channel block (CNBLOCK) (can be NIL).</summary>
    [Category(Constants.CATDataLink)]
    public BLOCKBASEV4 CMBlock { get; set; }
    [Category(Constants.CATDataLink)]
    public BLOCKBASEV4 SDBlock { get; set; }
    [Category(Constants.CATDataLink)]
    public SIBLOCKV4 SIBlock { get; }
    [Category(Constants.CATDataLink)]
    public IMDFBlock CEBlock { get; set; }
    [Category(Constants.CATDataLink)]
    public IMDFBlock CDBlock { get; set; }
    [Category(Constants.CATDataLink)]
    public List<ATBLOCKV4> Attachments { get; } = new List<ATBLOCKV4>();
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      var startPos = bw.BaseStream.Position;
      bw.BaseStream.Position += BlockSize;

      Int64 LinkCM = 0, LinkName = 0, LinkSI = 0, LinkCC = 0, LinkSD = 0, LinkUnit = 0, LinkTX = 0;
      if (CMBlock != null)
        LinkCM = CMBlock.writeBlock(bw);

      if (SDBlock != null)
        LinkSD = SDBlock.writeBlock(bw);

      if (SIBlock != null)
        LinkSI = SIBlock.writeBlock(bw);

      if (CCBlock != null)
        LinkCC = CCBlock.writeBlock(bw);

      if (!string.IsNullOrEmpty(Name))
        LinkName = new TXBLOCKV4(Name).writeBlock(bw);

      if (!string.IsNullOrEmpty(Unit))
        LinkUnit = new TXBLOCKV4(Unit).writeBlock(bw);

      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment).writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      if (!isLast)
        LinkNextCN = endPos;
      bw.BaseStream.Position = startPos;
      base.writeBlock(bw);
      for (UInt64 i = 0; i < mNoLinks; ++i)
      {
        switch (i)
        {
          case 0: bw.Write(LinkNextCN); break;
          case 1: bw.Write(LinkCM); break;
          case 2: bw.Write(LinkName); break;
          case 3: bw.Write(LinkSI); break;
          case 4: bw.Write(LinkCC); break;
          case 5: bw.Write(LinkSD); break;
          case 6: bw.Write(LinkUnit); break;
          case 7: bw.Write(LinkTX); break;
          default: bw.Write((UInt64)0); break;
        }
      }
      bw.Write((byte)ChannelType);
      bw.Write((byte)SyncType);
      bw.Write((byte)SignalType);
      bw.Write((byte)BitOffset);
      bw.Write(AddOffset);
      bw.Write(NoOfBits);
      if (CCBlock != null)
      {
        Min = CCBlock.Min;
        Max = CCBlock.Max;
      }
      if (!double.IsNaN(Min) && !double.IsNaN(Max))
        Flags |= ChannelFlags.LimitRangeValid;
      bw.Write((UInt32)Flags);
      bw.Write(InvalBitPos);
      bw.Write(Precision);
      bw.Write((byte)0);
      bw.Write((UInt16)Attachments.Count);
      bw.Write(MinRaw);
      bw.Write(MaxRaw);
      bw.Write(Min);
      bw.Write(Max);
      bw.Write(MinEx);
      bw.Write(MaxEx);
      bw.BaseStream.Position = endPos;
      return startPos;
    }

    internal bool createDependencies(Dictionary<Int64, IMDFBlock> references)
    {
      foreach (var link in mAttachmentLinks)
      {
        IMDFBlock block;
        if (!references.TryGetValue(link, out block) || !(block is ATBLOCKV4))
          continue;
        Attachments.Add((ATBLOCKV4)block);
      }
      return true;
    }

    Int64 ICNBLOCK.getReadPosition(Int64 recordIndex)
    {
      var result = recordIndex * CGBlock.getRecordSize();
      switch (CGBlock.DGBlock.RecordIDType)
      {
        case RecordIDType.Before8Bit:
        case RecordIDType.BeforeAndAfter8Bit: result += 1; break;
        case RecordIDType.Before16Bit: result += 2; break;
        case RecordIDType.Before32Bit: result += 4; break;
        case RecordIDType.Before64Bit: result += 8; break;
      }
      return result;
    }
    #endregion
  }

  /// <summary>Base class for any CCBLOCKs.</summary>
  public sealed class CCBLOCKV4 : BLOCKBASEV4, ICCBLOCK
  {
    #region data members
    Int64 LinkName, LinkUnit, LinkTX, LinkCCInv;
    #endregion
    #region ctors
    public CCBLOCKV4(BinaryReader br)
    {
      Int64[] linkList;
      readData(br, out linkList);
      for (int i = 0; i < linkList.Length; ++i)
      {
        var currLink = linkList[i];
        switch (i)
        {
          case 0: LinkName = currLink; break;
          case 1: LinkUnit = currLink; break;
          case 2: LinkTX = currLink; break;
          case 3: LinkCCInv = currLink; break;
        }
      }
      var conversionType = br.ReadByte();
      switch (conversionType)
      {
        case 0: ConversionType = ConversionType.None; break;
        case 1: ConversionType = ConversionType.ParametricLinear; break;
        case 2: ConversionType = ConversionType.Rational; break;
        case 3: ConversionType = ConversionType.TextFormula; break;
        case 4: ConversionType = ConversionType.TabInt; break;
        case 5: ConversionType = ConversionType.Tab; break;
        case 6: ConversionType = ConversionType.TabRange; break;
        case 7: ConversionType = ConversionType.TextTable; break;
        case 8: ConversionType = ConversionType.TextRange; break;
        case 9: ConversionType = ConversionType.TextToValue; break;
        case 10: ConversionType = ConversionType.TextToText; break;
      }
      Precision = br.ReadByte();
      Flags = (ConversionFlags)br.ReadUInt16();
      RefCount = br.ReadUInt16();
      TabSize = br.ReadUInt16();

      if ((Flags & ConversionFlags.LimitRangeValid) > 0)
      {
        Min = br.ReadDouble();
        Max = br.ReadDouble();
      }
      else
        br.BaseStream.Position += 16;

      int maxParam = 0;
      switch (ConversionType)
      {
        case ConversionType.None: break;

        case ConversionType.ParametricLinear:
        case ConversionType.Rational: maxParam = TabSize; break;

        case ConversionType.TextFormula:
          if (linkList.Length > 4)
            Formula = new TXBLOCKV4(br, linkList[4]).Text;
          break;

        case ConversionType.Tab:
        case ConversionType.TabInt:
          for (int i = 0; i < TabSize / 2; ++i)
            KeyValuePairs[br.ReadDouble()] = br.ReadDouble();
          break;

        case ConversionType.TextTable:
          var values = new double[TabSize];
          for (int i = 0; i < TabSize; ++i)
            values[i] = br.ReadDouble();
          var texts = new string[linkList.Length - 4];
          for (int i = 0; i < texts.Length; ++i)
            texts[i] = new TXBLOCKV4(br, linkList[i + 4]).Text;
          for (int i = 0; i < TabSize; ++i)
            KeyValuePairs[values[i]] = texts[i];
          DefaultText = texts[texts.Length - 1];
          break;

        case ConversionType.TextRange:
          var ranges = new sRange[TabSize / 2];
          for (int i = 0, j = 0; i < TabSize; i += 2, ++j)
            ranges[j] = new sRange { Min = br.ReadDouble(), Max = br.ReadDouble() };
          texts = new string[linkList.Length - 4];
          for (int i = 0; i < texts.Length; ++i)
            texts[i] = new TXBLOCKV4(br, linkList[i + 4]).Text;
          for (int i = 0; i < texts.Length - 1; ++i)
            KeyValuePairs[texts[i]] = ranges[i];
          DefaultText = texts[texts.Length - 1];
          break;

        case ConversionType.TabRange:
        case ConversionType.TextToValue:
        case ConversionType.TextToText:
          // todo
          break;

        default:
          throw new NotSupportedException(ConversionType.ToString());
      }

      if (maxParam > 0)
        // read and create parameters
        Params = Helpers.getCCParameters(br, maxParam, ConversionType);

      Name = new TXBLOCKV4(br, LinkName).Text;
      Unit = new TXBLOCKV4(br, LinkUnit).Text;
      Comment = new TXBLOCKV4(br, LinkTX).Text;
      if (LinkCCInv > 0)
      {
        br.BaseStream.Position = LinkCCInv;
        InvCCBlock = new CCBLOCKV4(br);
      }
    }

    public CCBLOCKV4(ConversionType conversionType, string unit, double min = double.NaN, double max = double.NaN)
      : base("CC", 80, 4)
    {
      ConversionType = conversionType;
      Unit = unit;
      if (!double.IsNaN(min) && !double.IsNaN(max))
      {
        Flags |= ConversionFlags.LimitRangeValid;
        Min = min;
        Max = max;
      }
    }
    public CCBLOCKV4() { }
    #endregion
    #region properties
    [Category(Constants.CATData)]
    public override string Name { get; protected set; } = string.Empty;
    [Category(Constants.CATData)]
    public string Comment { get; } = string.Empty;
    [Category(Constants.CATData)]
    public string Unit { get; } = string.Empty;
    [Category(Constants.CATData)]
    public ConversionType ConversionType { get; }
    [Category(Constants.CATData)]
    public byte Precision { get; }
    public ConversionFlags Flags { get; private set; }
    [Category(Constants.CATDataLimit)]
    public double Min { get; set; } = double.NaN;
    [Category(Constants.CATDataLimit)]
    public double Max { get; set; } = double.NaN;
    [Category(Constants.CATData), ReadOnly(true)]
    public UInt16 TabSize { get; internal set; }
    [Category(Constants.CATData)]
    public double[] Params { get; set; }
    [Category(Constants.CATData)]
    public SortedList<object, object> KeyValuePairs { get; set; } = new SortedList<object, object>();
    [Category(Constants.CATData), ReadOnly(true)]
    public string DefaultText { get; set; } = string.Empty;
    [Category(Constants.CATData), ReadOnly(true)]
    public string Formula { get; set; } = string.Empty;
    [Category(Constants.CATData)]
    public UInt16 RefCount { get; }
    [Category(Constants.CATDataLink)]
    public ICCBLOCK InvCCBlock { get; }
    #endregion
    #region methods
    public override Int64 writeBlock(BinaryWriter bw, bool isLast = true)
    {
      TabSize = 0;
      BlockSize = 80;
      mNoLinks = 4;
      var RefLinks = new List<Int64>();
      var startPos = bw.BaseStream.Position;

      bw.BaseStream.Position = startPos + BlockSize;
      if (TabSize == 0)
      {
        switch (ConversionType)
        {
          case ConversionType.None: break;

          case ConversionType.ParametricLinear:
          case ConversionType.Rational:
            TabSize = (UInt16)Params.Length;
            BlockSize += TabSize * 8;
            bw.BaseStream.Position = startPos + BlockSize;
            break;

          case ConversionType.TextFormula:
            BlockSize += 8;
            bw.BaseStream.Position = startPos + BlockSize;
            RefLinks.Add(new TXBLOCKV4(Formula).writeBlock(bw));
            break;

          case ConversionType.Tab:
          case ConversionType.TabInt:
            TabSize = (UInt16)(KeyValuePairs.Count * 2);
            BlockSize += TabSize * 8;
            bw.BaseStream.Position = startPos + BlockSize;
            break;

          case ConversionType.TextTable:
            TabSize = (UInt16)KeyValuePairs.Count;
            BlockSize += TabSize * 2 * 8 + 8;
            bw.BaseStream.Position = startPos + BlockSize;
            foreach (var value in KeyValuePairs.Values)
              RefLinks.Add(new TXBLOCKV4((string)value).writeBlock(bw));
            RefLinks.Add(new TXBLOCKV4(DefaultText).writeBlock(bw));
            break;

          case ConversionType.TextRange:
            TabSize = (UInt16)(KeyValuePairs.Count + 1);
            BlockSize += TabSize * 20;
            bw.BaseStream.Position = startPos + BlockSize;
            break;

          case ConversionType.TabRange:
          case ConversionType.TextToValue:
          case ConversionType.TextToText:
            // todo
            break;

          default: throw new NotSupportedException(ConversionType.ToString());
        }
      }

      if (!string.IsNullOrEmpty(Name))
        LinkName = new TXBLOCKV4(Name).writeBlock(bw);
      if (!string.IsNullOrEmpty(Unit))
        LinkUnit = new TXBLOCKV4(Unit).writeBlock(bw);
      if (!string.IsNullOrEmpty(Comment))
        LinkTX = new TXBLOCKV4(Comment).writeBlock(bw);
      if (InvCCBlock != null)
        LinkCCInv = InvCCBlock.writeBlock(bw);

      var endPos = bw.BaseStream.Position;
      bw.BaseStream.Position = startPos;
      mNoLinks += (UInt64)RefLinks.Count;
      base.writeBlock(bw);
      for (UInt64 i = 0; i < mNoLinks; ++i)
      {
        switch (i)
        {
          case 0: bw.Write(LinkName); break;
          case 1: bw.Write(LinkUnit); break;
          case 2: bw.Write(LinkTX); break;
          case 3: bw.Write(LinkCCInv); break;
          default: bw.Write(RefLinks[(int)(i - 4)]); break;
        }
      }

      byte conversionType = 0;
      switch (ConversionType)
      {
        case ConversionType.None: conversionType = 0; break;
        case ConversionType.ParametricLinear: conversionType = 1; break;
        case ConversionType.Rational: conversionType = 2; break;
        case ConversionType.TextFormula: conversionType = 3; break;
        case ConversionType.TabInt: conversionType = 4; break;
        case ConversionType.Tab: conversionType = 5; break;
        case ConversionType.TabRange: conversionType = 6; break;
        case ConversionType.TextTable: conversionType = 7; break;
        case ConversionType.TextRange: conversionType = 8; break;
        case ConversionType.TextToValue: conversionType = 9; break;
        case ConversionType.TextToText: conversionType = 10; break;
      }
      bw.Write(conversionType);
      bw.Write(Precision);
      if (!double.IsNaN(Min) && !double.IsNaN(Max))
        Flags |= ConversionFlags.LimitRangeValid;
      bw.Write((UInt16)Flags);
      bw.Write((UInt16)RefLinks.Count);
      bw.Write(TabSize);
      if ((Flags & ConversionFlags.LimitRangeValid) > 0)
      {
        bw.Write(Min);
        bw.Write(Max);
      }
      else
        bw.BaseStream.Position += 16;

      switch (ConversionType)
      {
        case ConversionType.ParametricLinear:
        case ConversionType.Rational:
          foreach (var par in Params)
            bw.Write(par);
          break;

        case ConversionType.TextFormula:
          break;

        case ConversionType.Tab:
        case ConversionType.TabInt:
          foreach (var pair in KeyValuePairs)
          {
            bw.Write((double)pair.Key);
            bw.Write((double)pair.Value);
          }
          break;

        case ConversionType.TextTable:
          foreach (var value in KeyValuePairs.Keys)
            bw.Write((double)value);
          break;
      }

      bw.BaseStream.Position = endPos;
      return startPos;
    }

    /// <summary>
    /// Get the physical value from the specified raw value.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <returns>The physical value</returns>
    public double toPhysical(double rawValue)
    {
      double physValue = rawValue;
      try
      {
        switch (ConversionType)
        {
          case ConversionType.ParametricLinear:
            if (Params != Helpers.LINEAR_IDENTITY)
              physValue = Params[1] * rawValue + Params[0];
            break;

          case ConversionType.Rational:
            if (Params != Helpers.RAT_IDENTITY)
            {
              var A = Params[3] * rawValue - Params[0];
              var B = Params[4] * rawValue - Params[1];
              var C = Params[5] * rawValue - Params[2];
              if (A != 0 && !double.IsNaN(A) && !double.IsInfinity(A))
              { // Full solution, A != 0
              }
              else
                // Degenerate case, A == 0
                physValue = -C / B;
            }
            break;

          case ConversionType.Tab:
            object value = rawValue;
            if (KeyValuePairs.TryGetValue(rawValue, out value))
              physValue = (double)value;
            break;

          case ConversionType.TabInt:
            double physStart = physValue;
            if (KeyValuePairs.Count > 0)
            {
              double rawInt = rawValue;
              double rawStart = (double)KeyValuePairs.Keys[0];
              physStart = (double)KeyValuePairs.Values[0];
              for (int i = 1; i < KeyValuePairs.Values.Count; ++i)
              {
                if (rawInt < rawStart)
                  // smaller than start
                  return physStart;
                double rawEnd = (double)KeyValuePairs.Keys[i];
                double physEnd = (double)KeyValuePairs.Values[i];
                if (rawInt <= rawEnd)
                  // between
                  return (rawInt - rawStart) / (rawEnd - rawStart) * (physEnd - physStart) + physStart;
                rawStart = rawEnd;
                physStart = physEnd;
              }
            }
            physValue = physStart;
            break;

          case ConversionType.TextFormula:
          case ConversionType.TextTable:
          case ConversionType.TextRange:
          case ConversionType.TabRange:
          case ConversionType.TextToText:
          case ConversionType.TextToValue:
          case ConversionType.None:
            break;
          default: throw new NotSupportedException(ConversionType.ToString());
        }
      }
      catch { }
      return physValue;
    }
  }
  #endregion
}