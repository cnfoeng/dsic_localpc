﻿/*!
 * @file    
 * @brief   Defines the MDF V4 types.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    Spetember 2017
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2014 Joe Nachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;

/// <summary>
/// Implements the MDF V4.XX specific stuff.
/// 
/// Advantages over previous MDF versions:
/// - File size could be greater than 4 GB
/// - Measurement data could be compressed
/// </summary>
namespace jnsoft.MDF.V4
{
  /// <summary>
  /// Supported Channel Flags.
  /// </summary>
  [Flags]
  public enum ChannelFlags
  {
    None,
    /// <summary>
    /// If set, all values of this channel are invalid. If in addition an invalidation bit is used (bit 1 set), 
    /// then the value of the invalidation bit must be set (high) for every value of this channel. Must not be 
    /// set for a master channel (channel types 2 and 3).
    /// </summary>
    Invalid = 1,
    /// <summary>
    /// If set, this channel uses an invalidation bit (position specified by cn_inval_bit_pos). Must not be 
    /// set if cg_inval_bytes is zero. Must not be set for a master channel (channel types 2 and 3).
    /// </summary>
    InvalBytesValid = Invalid << 1,
    /// <summary>
    /// If set, the precision value for display of floating point values specified in cn_precision is valid 
    /// and overrules a possibly specified precision value of the conversion rule (cc_precision).
    /// </summary>
    PrecisionValid = Invalid << 2,
    /// <summary>
    /// If set, both the minimum and the maximum raw value that occurred for this signal within the samples 
    /// recorded in this file are known and stored in cn_val_range_min and cn_val_range_max. Otherwise the two 
    /// fields are not valid. Note: the raw value range can only be expressed for numeric channel data types 
    /// (cn_data_type smaller 6). For all other data types, the flag must not be set.
    /// </summary>
    ValueRangeValid = Invalid << 3,
    /// <summary>
    /// If set, the limits of the signal value are known and stored in cn_limit_min and cn_limit_max. Otherwise the two 
    /// fields are not valid (see [MCD-2 MC] keywords LowerLimit/UpperLimit for MEASUREMENT and CHARACTERISTIC). This 
    /// limit range defines the range of plausible values for this channel and may be used for display a warning. 
    /// Note: the (extended) limit range can only be expressed for a channel whose conversion rule returns a numeric value 
    /// (REAL) or which has a numeric channel data type (cn_data_type smaller or equal 5). In all other cases, the flag 
    /// must not be set. Depending on the type of conversion, the limit values are interpreted as physical or raw values. 
    /// If the conversion rule results in numeric values, the limits must be interpreted as physical values, 
    /// otherwise (e.g. for verbal / scale conversion) as raw values.
    /// </summary>
    LimitRangeValid = Invalid << 4,
    /// <summary>
    /// If set, the extended limits of the signal value are known and stored in cn_limit_ext_min and cn_limit_ext_max. 
    /// Otherwise the two fields are not valid. (see [MCD-2 MC] keyword EXTENDED_LIMITS) The extended limit range must 
    /// be larger or equal to the limit range (if valid). Values outside the extended limit range indicate an error 
    /// during measurement acquisition. The extended limit range can be used for any limits, e.g. physical ranges from 
    /// analog sensors. See also remarks for 'limit range valid' flag (bit 4).
    /// </summary>
    ExtendedLimitRangeValid = Invalid << 5,
    /// <summary>
    /// If set, the signal values of this channel are discrete and must not be interpolated. (see [MCD-2 MC] keyword DISCRETE).
    /// </summary>
    DiscreteValue = Invalid << 6,
    /// <summary>
    /// If set, the signal values of this channel correspond to a calibration object, otherwise to a measurement object 
    /// (see [MCD-2 MC] keywords MEASUREMENT and CHARACTERISTIC).
    /// </summary>
    Calibration = Invalid << 7,
    /// <summary>
    /// If set, the values of this channel have been calculated from other channel inputs. 
    /// (see [MCD-2 MC] keywords VIRTUAL and DEPENDENT_CHARACTERISTIC) In MDBLOCK for 
    /// cn_md_comment the used input signals and the calculation formula can be documented, 
    /// see Table 37.
    /// </summary>
    Calculated = Invalid << 8,
    /// <summary>
    /// If set, this channel is virtual, i.e. it is simulated by the recording tool. (see [MCD-2 MC] 
    /// keywords VIRTUAL and VIRTUAL_CHARACTERISTIC) Note: for a virtual measurement according to 
    /// MCD-2 MC both the 'Virtual' flag (bit 9) and the 'Calculated' flag (bit 8) should be set.
    /// </summary>
    Virtual = Invalid << 9,
    /// <summary>
    /// If set, this channel contains information about a bus event. For details please refer to [MDFBL]. 
    /// Valid since MDF 4.1.0, should not be set for earlier versions.
    /// </summary>
    BusEvent = Invalid << 10,
    /// <summary>
    /// If set, this channel contains only strictly monotonous increasing/decreasing values. 
    /// The flag is optional. valid since MDF 4.1.0, should not be set for earlier versions.
    /// </summary>
    Montonous = Invalid << 11,
    /// <summary>
    /// If set, a channel to be preferably used as X axis is specified by cn_default_x. 
    /// This is only a recommendation, a tool may choose to use a different X axis. 
    /// Valid since MDF 4.1.0, should not be set for earlier versions.
    /// </summary>
    DefaultXAxis = Invalid << 12,
  }

  /// <summary>
  /// SUpported Channel group flags.
  /// </summary>
  [Flags]
  public enum ChannelGroupFlags
  {
    /// <summary>
    /// If set, this is a "variable length signal data" (VLSD) channel group. 
    /// See explanation in 5.14.3 Variable Length Signal Data (VLSD) CGBLOCKV4.
    /// </summary>
    VLSD = 1,
    /// <summary>
    /// If set, this channel group contains information about a bus event, i.e. it contains a structure channel with bit 10 
    /// (bus event falg) set in cn_flags. For details please refer to [MDF-BL]. valid since MDF 4.1.0, should not be set for 
    /// earlier versions.
    /// </summary>
    BusEvent = VLSD << 1,
    /// <summary>
    /// Only relevant if "bus event channel group" flag (bit 1) is set.
    /// If set, this indicates that only the plain bus event is stored in this channel group, but no channels describing the 
    /// signals transported in the payload of the bus event. If not set, at least one channel for a signal transported in the 
    /// payload of the bus event (data frame/PDU) must be present. For details please refer to [MDF-BL]. valid since MDF 4.1.0, 
    /// should not be set for earlier versions.
    /// </summary>
    PlainBusEvent = VLSD << 2,
  }

  /// <summary>
  /// Supported converson flags.
  /// </summary>
  [Flags]
  public enum ConversionFlags
  {
    /// <summary>
    /// If set, the precision value for display of floating point values specified in cc_precision is valid
    /// </summary>
    PrecisionValid = 0x01,
    /// <summary>
    /// If set, both the minimum and the maximum physical value that occurred after conversion for this signal 
    /// within the samples recorded in this file are known and stored in cc_phy_range_min and cc_phy_range_max. 
    /// Otherwise the two fields are not valid. Note: the physical value range can only be expressed for 
    /// conversions which return a numeric value (REAL). For conversions returning a string value or for the 
    /// inverse conversion rule the flag must not be set.
    /// </summary>
    LimitRangeValid = PrecisionValid << 1,
    /// <summary>
    /// This flag indicates for conversion types 7 and 8 (value/value range to text/scale conversion tabular look-up) 
    /// that the normal table entries are status strings (only reference to TXBLOCK), and the actual conversion rule 
    /// is given in CCBLOCK referenced by default value. This also implies special handling of limits, 
    /// see [MCD-2 MC] keyword STATUS_STRING_REF. Can only be set for cc_type between  7 and 8.
    /// </summary>
    StatusString = PrecisionValid << 2,
  }

  /// <summary>Type of hierarchy level</summary>
  public enum HierarchyType
  {
    /// <summary>
    /// All elements and children of this hierarchy level form a logical group (see [MCD-2 MC] keyword GROUP).
    /// </summary>
    Group,
    /// <summary>
    /// All children of this hierarchy level form a functional group (see [MCD-2 MC] keyword FUNCTION).
    /// 
    /// For this type, the hierarchy must not contain CNBLOCK references (ch_element_count must be 0).
    /// </summary>
    Function,
    /// <summary>
    /// All elements and children of this hierarchy level form a 'fragmented' structure, see 5.18.1 Structures. 
    /// 
    /// Note: Do not use 'fragmented' and 'compact' structure in parallel. If possible prefer a 'compact' structure.
    /// </summary>
    Structure,
    /// <summary>
    /// All elements of this hierarchy level form a map list (see [MCD-2 MC] keyword MAP_LIST):
    /// - the first element represents the z axis (must be a curve, i.e. CNBLOCK with CABLOCK of type 'scaling axis')
    /// - all other elements represent the maps (must be 2-dimensional map, i.e. CNBLOCK with CABLOCK of type 'lookup')
    /// </summary>
    MapList,
    /// <summary>
    /// input variables of function (see [MCD-2 MC] keyword IN_MEASUREMENT). All referenced channels must be 
    /// measurement objects (Calibration flag not set in CNBLOCKV4.Flags).
    /// </summary>
    InMeasurement,
    /// <summary>
    /// output variables of function see [MCD-2 MC] keyword OUT_MEASUREMENT). All referenced channels must be 
    /// measurement objects (Calibration flag not set in CNBLOCKV4.Flags).
    /// </summary>
    OutMeasurement,
    /// <summary>
    /// local variables of function (see [MCD-2 MC] keyword LOC_MEASUREMENT). All referenced channels must be
    /// measurement objects (Calibration flag not set in CNBLOCKV4.Flags).
    /// </summary>
    LocMeasurement,
    /// <summary>
    /// calibration objects defined in function (see [MCD-2 MC] keyword DEF_CHARACTERISTIC). 
    /// All referenced channels must be calibration objects (Calibration flag set in CNBLOCKV4.Flags).
    /// </summary>
    DefCharacteristic,
    /// <summary>
    /// calibration objects referenced in function (see [MCD-2 MC] keyword REF_CHARACTERISTIC). 
    /// All referenced channel must be calibration objects (Calibration flag set in CNBLOCKV4.Flags).
    /// </summary>
    RefCharacteristic,
  }

  /// <summary>
  /// Datablock flags.
  /// </summary>
  [Flags]
  public enum DataBlockFlags
  {
    /// <summary>
    /// If set, each DLBLOCK in the linked list has the same number of referenced blocks (dl_count) 
    /// and the (uncompressed) data sections of the blocks referenced by dl_data have a common length 
    /// given by dl_equal_length. The only exception is that for the last DLBLOCK in the list 
    /// (dl_dl_next = NIL), its number of referenced blocks dl_count can be less than or equal to 
    /// dl_count of the previous DLBLOCK, and the data section length of its last referenced block 
    /// (dl_data[dl_count-1]) can be less than or equal to dl_equal_length. 
    /// 
    /// If not set, the number of referenced blocks dl_count may be different for each DLBLOCK in the linked list, 
    /// and the data section lengths of the referenced blocks may be different and a table of offsets is given in 
    /// dl_offset. Note: The value of the "equal length" flag must be equal for all DLBLOCKs in the linked list. 
    /// </summary>
    EqualLength = 1,
  }

  /// <summary>
  /// Attachment flags.
  /// </summary>
  [Flags]
  public enum AttachmentFlags
  {
    /// <summary>
    /// If set, the attachment data is embedded, otherwise it is contained in an external file referenced by file path and name in ATBLOCKV4.Filename. 
    /// </summary>
    EmbeddedData = 0x01,
    /// <summary>
    /// If set, the stream for the embedded data is compressed using the Defalte zip algorithm (see[DEF] and[ZLIB]). Can only be set if "embedded data" flag(bit 0) is set.
    /// </summary>
    CompressedEmbbeddedData = EmbeddedData << 1,
    /// <summary>
    /// If set, the at_md5_checksum field contains the MD5 check sum of the data.
    /// </summary>
    MD5ChecksumValid = EmbeddedData << 2,
  }

  /// <summary>
  /// Event type flags.
  /// </summary>
  public enum EventType
  {
    /// <summary>
    /// This event type specifies a recording period, i.e. the first and last time a signal value could theoretically be recorded. 
    /// Recording events must always define a range, i.e. ev_range_type between 1 and 2. All recording events in the MDF file must 
    /// only occur on exactly one scope level: either all have a global scope, e.g. if file has been created by a single recorder; 
    /// or all of them have a scope on CG level (ev_scope list must contain links to one or more CGBLOCKs), e.g. after combining 
    /// two files. Recording events which apply to single channels only (CN level scope) generally is not possible. Within one scope, 
    /// there can be several recording periods, e.g. if the recording was paused between these periods (although this should be modeled 
    /// by a recording interrupt event, see below). However, these periods must not overlap.
    /// </summary>
    Recording,
    /// <summary>
    /// This event type indicates that the recording has been interrupted. It can only occur within the range of a matching recording period. 
    /// Like the recording event type, it can only have a global scope or a CG level scope. It should be either on the same scope level as 
    /// the recording event, or on a lower level, i.e. if the recording events have a global scope, the recording interrupt event can 
    /// either have a global or a CG level scope. A recording interrupt can be defined as point single event) or as range (pair of events 
    /// as explained for ev_ev_range). If it defines a point, the interruption should be considered to automatically have finished when 
    /// the next sample for a signal value for a channel in its scope level occurred. Since a recording interrupt event is closely related 
    /// to a recording period, its ev_ev_parent should point to the respective "range begin" recording event.
    /// </summary>
    RecordingInterrupt,
    /// <summary>
    /// This event type indicates that not only the recording, but already the acquisition of the signal values has been interrupted. 
    /// Except of this, the same rules apply as for the recording interrupt event type.
    /// </summary>
    AcquisitionInterrupt,
    /// <summary>
    /// This event type specifies an event which started the recording of signal values due to some condition (including user interaction). 
    /// The trigger condition can be specified in the ev_md_comment MDBLOCK, (see Table 27). Here also a pre and post trigger interval 
    /// can be specified. A start recording trigger event can only occur as point (ev_range_type = 0), not as range. It usually is closely 
    /// related to a recording period, i.e. it should have the same scope as the respective recording events (note that due to the pre-trigger 
    /// interval, the matching "range begin" recording event can be before this event). Here ev_ev_parent may be used to point to the 
    /// "range begin" recording event. For some other use case, ev_ev_parent instead might point to another trigger event, e.g. if this 
    /// trigger activated the condition for the start recording trigger.
    /// </summary>
    StartRecordingTrigger,
    /// <summary>
    /// Symmetrically to the "start recording trigger" event type, this event type specifies an event which stopped the recording of signal 
    /// values due to some condition. The same rules apply as for the start recording trigger event type. Note that the two event types may 
    /// occur in pairs, but they do not have to.
    /// </summary>
    StopRecordingTrigger,
    /// <summary>
    /// This event type generally specifies an event that occurred due to some condition (except of the special start and stop recording 
    /// trigger event types). The trigger condition can be specified in the ev_md_comment MDBLOCK, (see Table 27). A trigger event can only 
    /// occur as point (ev_range_type = 0), not as range.
    /// </summary>
    Trigger,
    /// <summary>
    /// This event type specifies a marker for a point or a range. As examples, a marker could be a user-generated comment or an automatically 
    /// generated bookmark.
    /// </summary>
    Marker,
  }

  /// <summary>
  /// Range type
  /// </summary>
  public enum RangeType
  {
    /// <summary>event defines a point</summary>
    Point,
    /// <summary>event defines the beginning of a range</summary>
    BeginRange,
    /// <summary>event defines the end of a range</summary>
    EndRange
  }

  /// <summary>
  /// Cause of event
  /// </summary>
  public enum CauseType
  {
    /// <summary>
    /// cause of event is not known or does not fit into given categories.
    /// </summary>
    Other,
    /// <summary>
    /// event was caused by some error.
    /// </summary>
    Error,
    /// <summary>
    /// event was caused by tool-internal condition, e.g. trigger condition or reconfiguration.
    /// </summary>
    Tool,
    /// <summary>
    /// event was caused by a scripting command.
    /// </summary>
    Script,
    /// <summary>
    /// event was caused directly by user, e.g. user input or some other interaction with GUI.
    /// </summary>
    User,
  }

  /// <summary>
  /// Event flags
  /// </summary>
  [Flags]
  public enum EventFlags
  {
    /// <summary>
    /// If set, the event has been generated during post processing of the file.
    /// </summary>
    PostProcessing = 0x01,
  }

  /// <summary>
  /// Supported zip types.
  /// </summary>
  public enum ZipType
  {
    /// <summary>
    /// The Deflate zip algorithm as used in various zip implementations (see <a href="https://en.wikipedia.org/wiki/Zlib">ZLIB</a>). 
    /// </summary>
    Deflate,
    /// <summary>
    /// Before compression, the data block is transposed as explained in 5.26.2 Transposition of Data.
    /// Typically only used for sorted data groups and DT or RD block types.
    /// </summary>
    TransposeAndDeflate,
    /// <summary>
    /// Internal value to indicate that no zip algorithm is used.
    /// </summary>
    None,
  }

  /// <summary>
  /// Source type.
  /// </summary>
  public enum SourceType
  {
    /// <summary>A source type which does not fit into given categories or is unknown</summary>
    OTHER,
    /// <summary>The ECU</summary>
    ECU,
    /// <summary>a bus (e.g. for bus monitoring)</summary>
    BUS,
    /// <summary>I/O device (e.g. analog I/O)</summary>
    IO,
    /// <summary>a software tool (e.g. for tool generated signals/events)</summary>
    TOOL,
    /// <summary>a user interaction/input (e.g. for user generated events)</summary>
    USER,
  }

  /// <summary>
  /// Bus type, additional classification of used bus(should be 0 for SourceType ≥ SourceType.TOOL):
  /// </summary>
  public enum BusType
  {
    NONE,
    OTHER,
    CAN,
    LIN,
    MOST,
    FLEXRAY,
    K_LINE,
    ETHERNET,
    USB,
  }

  /// <summary>
  /// Source information flags.
  /// </summary>
  [Flags]
  public enum SourceFlags
  {
    None,
    /// <summary>
    /// Source is only a simulation (can be hardware or software simulated). 
    /// Cannot be set for SourceType.TOOL.
    /// </summary>
    SimulatedSource = 0x01,
  }

  /// <summary>
  /// Sample reduction flags.
  /// </summary>
  [Flags]
  public enum SRFlags
  {
    /// <summary>
    /// If set, the sample reduction record contains invalidation Bytes, i.e. after the three data Byte sections for mean, 
    /// minimum and maximum values, there is one invalidation Byte section. If not set, the invalidation Bytes are omitted. 
    /// Must only be set if cg_inval_bytes > 0. If invalidation Bytes are used, and if the invalidation bit of the respective 
    /// channel is valid (i.e. "invalidation bit valid" flag (bit 1) is set in cn_flags), then a set invalidation bit in the 
    /// invalidation Bytes of the sample reduction record means that within the sample interval all signal values of the 
    /// respective channel have been invalid.
    /// </summary>
    InvalidationBytes = 0x01,
  }
}