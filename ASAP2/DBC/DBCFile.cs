﻿/*!
 * @file    
 * @brief   Implements the DBC (Vector CANDb++) file format reader/writer.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Comm.CAN;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Implements the DBC (Vector CANDb++) file format reader/writer.
/// 
/// Basic access to a DBC file is provided by the DBCFile class.
/// It provides methods and properties to open, save and access the DBC model.
/// 
/// <example>
/// Getting started with reading and writing a DBC File (code example):
/// <code>
/// using jnsoft.DBC;
/// 
/// void loadAndSaveFile(string dbcFilename)
/// {
///   // Open DBC file
///   var dbcFile = DBCFile.open(dbcFilename);
///   Console.WriteLine("Opened DBC file {0} version: {1} contained signals: {2}"
///     , dbcFile.SourceFile, dbcFile.Version, dbcFile.getCountSignals()
///     );
///   
///   // change a value
///   dbcFile.Comment = "Changed comment in dbc file";
///   
///   // save the changed DBC file
///   dbcFile.save(dbcFilename+".out.dbc");
/// }
/// </code></example>
/// </summary>
namespace jnsoft.DBC
{
  /// <summary>
  /// Implements the DBC (Vector CANDb++) file format reader/writer.
  /// </summary>
  public sealed class DBCFile
  {
    #region types
    /// <summary>
    /// Possible sort mode for the DBC file writer.
    /// </summary>
    public enum SortMode
    {
      /// <summary>
      /// Not sorted.
      /// </summary>
      None,
      /// <summary>
      /// Sort by name.
      /// </summary>
      ByName,
      /// <summary>
      /// Sort by value.
      /// </summary>
      ByValue,
    }
    /// <summary>
    /// Possible parser states.
    /// </summary>
    enum State
    {
      Unknown = int.MaxValue,
      /// <summary>
      /// Version
      /// 
      /// Format VERSION "string"
      /// </summary>
      VERSION = Unknown - 1,
      /// <summary>
      /// Command definitions
      /// 
      /// Format NS_: {Name} {Name} {Name}...multiline
      /// </summary>
      NS_ = Unknown - 2,
      BS_ = Unknown - 3,
      /// <summary>
      /// Format: BU_: {Name} {Name} {Name}...
      /// </summary>
      BU_ = Unknown - 4,
      /// <summary>
      /// Message
      /// 
      /// Format: BO_ {ID} {Name}:{DLC} {BU}
      /// </summary>
      BO_ = Unknown - 5,
      /// <summary>
      /// Signal
      /// 
      /// Format: SG_ {Name} : 
      /// </summary>
      SG_ = Unknown - 6,
      /// <summary>
      /// Environment
      /// 
      /// Format: EV_ {Name} : 
      /// </summary>
      EV_ = Unknown - 7,
      /// <summary>
      /// Comment
      /// 
      /// Format: CM_ {SG_,BO_} {comment};
      /// </summary>
      CM_ = Unknown - 9,
      /// <summary>
      /// Format: BA_ "{Name}" {BU_} {Name} {"string"|number}
      /// Format: BA_ "{Name}" {BO_} {ID} {number} {number}
      /// </summary>
      BA_ = Unknown - 10,
      /// <summary>
      /// Format BA_DEF_ "string" {DATATYPE(INT,HEX,STRING,ENUM)} {number} {number}
      /// Format BA_DEF_ {BU_|SG_} "string" {DATATYPE(INT,HEX,STRING,ENUM)} {number} {number}
      /// </summary>
      BA_DEF_ = Unknown - 11,
      BA_DEF_REL_ = Unknown - 12,
      /// <summary>
      /// Format BA_DEF_DEF_ "string" {number|string}
      /// </summary>
      BA_DEF_DEF_ = Unknown - 13,
      BA_DEF_DEF_REL_ = Unknown - 14,
      /// <summary>
      /// Value (enum definition)
      /// 
      /// Format: {number} {alphaID} [{number} "string"] [{number} "string"]...
      /// </summary>
      VAL_ = Unknown - 15,
      /// <summary>
      /// Value table (enum definition)
      /// 
      /// Format: {number} {alphaID} [{number} "string"] [{number} "string"]...
      /// </summary>
      VAL_TABLE_ = Unknown - 16,
      /// <summary>
      /// Signal Group.
      /// </summary>
      SIG_GROUP_ = Unknown - 17,
      /// <summary>
      /// Signal value type.
      /// </summary>
      SIG_VALTYPE_ = Unknown - 18,
    }
    /// <summary>
    /// Signal flags (may be combined).
    /// </summary>
    [Flags]
    public enum SignalFlagsType : byte
    {
      /// <summary>
      /// The signal raw value is in big endian format.
      /// </summary>
      BigEndian = 0x01,
      /// <summary>
      /// The signal raw value is a signed value.
      /// </summary>
      Signed = 0x02,
      /// <summary>
      /// The signal is the multiplexer signal.
      /// </summary>
      MultiplexSignal = 0x04,
      /// <summary>
      /// The signal is available only if the corresponding multiplexer value is set in the message 
      /// </summary>
      Multiplexed = 0x08,
    }
    /// <summary>
    /// Possible signal float types.
    /// </summary>
    public enum SignalFloatType
    {
      /// <summary>An integer value type</summary>
      Integer,
      /// <summary>IEEE Float (32 Bit)</summary>
      IEEEFloat32,
      /// <summary>IEEE float (64 Bit)</summary>
      IEEEFloat64,
    }
    /// <summary>
    /// Interface any DBC base class must implement.
    /// </summary>
    public interface IDBCBase
    {
      string Name { get; set; }
      /// <summary>
      /// Gets or sets a reference to a user object.
      /// </summary>
      object Tag { get; set; }
      /// <summary>
      /// Builds the DBC representation string.
      /// </summary>
      /// <returns>the DBC representation string.</returns>
      string toFileString();
    }
    /// <summary>
    /// Interface any DBC base class attributes must implement.
    /// </summary>
    public interface IDBCAttribute : IDBCBase
    {
      /// <summary>
      /// Builds the DBC default representation string.
      /// </summary>
      /// <returns>the DBC default representation string.</returns>
      string toFileDefaultString();
    }
    /// <summary>
    /// Interface any DBC base class supporting comments must implement.
    /// </summary>
    public interface IDBCComment : IDBCBase
    {
      /// <summary>
      /// Builds the DBC comment string.
      /// </summary>
      /// <returns>the DBC comment string.</returns>
      string toFileComment();
      /// <summary>
      /// Builds the DBC tooltip string
      /// </summary>
      /// <returns></returns>
      string toToolTipString();
    }
    /// <summary>
    /// Interface any DBC base class supporting signals must implement.
    /// </summary>
    public interface IDBCSignal : IDBCComment
    {
      /// <summary>
      /// Builds the DBC VAL_ string
      /// </summary>
      /// <returns>a VAL_ string or string.Empty</returns>
      string toValString();
    }
    /// <summary>
    /// CANDb++ (DBC) Device type.
    /// </summary>
    public sealed class SourceType : IDBCComment
    {
      #region members
      DBCFile mParent;
      string mName;
      #endregion
      #region properties
      [Browsable(false)]
      public DBCFile Parent { get { return mParent; } }
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name
      {
        get { return mName; }
        set
        {
          if (checkIdentifier(mParent, this, value))
            mName = value;
        }
      }
      /// <summary>Gets or sets the source comment.</summary>
      [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor)), Description(mCommentDesc)]
      public string Comment { get; set; }
      /// <summary>Gets or sets the source messages.</summary>
      [Category(Constants.CATData)]
      public List<MsgType> Messages;
      /// <summary>
      /// Gets or sets the Attributes dictionary.
      /// </summary>
      [Category(Constants.CATData)]
      public AttributeTypeDict Attributes { get; set; }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region constructors
      public SourceType(string name, DBCFile parent)
      { mName = name; mParent = parent; Messages = new List<MsgType>(); Attributes = new AttributeTypeDict(); }
      #endregion
      string IDBCBase.toFileString()
      {
        throw new NotSupportedException();
      }
      string IDBCComment.toFileComment()
      {
        return string.IsNullOrEmpty(Comment)
          ? null
          : string.Format("CM_ BU_ {0} \"{1}\";", Name, Comment);
      }
      string IDBCComment.toToolTipString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat(mTTSrcMessagesFmt
          , Messages.Count
          , string.IsNullOrEmpty(Comment)
            ? string.Empty
            : string.Format("\n{0}", Comment)
          );
        if (Attributes != null && Attributes.Count > 0)
        { // add attributes
          sb.Append(mTTAttr);
          foreach (var attr in Attributes)
            sb.AppendFormat(mTTAttribFmt, attr.Key, attr.Value);
        }
        return sb.ToString();
      }
      /// <summary>
      /// Gets the object's identifier.
      /// </summary>
      public override string ToString() { return mName; }
    }
    /// <summary>
    /// Possible signal data types.
    /// </summary>
    public enum DataType
    {
      /// <summary>unsigned byte (8 Bit)</summary>
      UBYTE,
      /// <summary>signed byte (8 Bit)</summary>
      SBYTE,
      /// <summary>unsigned word (16 Bit)</summary>
      UWORD,
      /// <summary>signed word (16 Bit)</summary>
      SWORD,
      /// <summary>unsigned long (32 Bit)</summary>
      ULONG,
      /// <summary>signed long (32 Bit)</summary>
      SLONG,
      /// <summary>IEEE Float (32 Bit)</summary>
      FLOAT32,
      /// <summary>unsigned longlong (64 Bit)</summary>
      UINT64,
      /// <summary>signed longlong (64 Bit)</summary>
      INT64,
      /// <summary>IEEE Float (64 Bit)</summary>
      FLOAT64,
      /// <summary>a bitfield</summary>
      BITFIELD,
    }
    /// <summary>
    /// Possible environment data types.
    /// </summary>
    public enum EnvType
    {
      /// <summary>integer data type</summary>
      Integer,
      /// <summary>float data type</summary>
      Float,
    }
    /// <summary>
    /// Source dictionary type.
    /// 
    /// Maps CANdb++ source names to it's corresponding messages.
    /// </summary>
    public sealed class SourceDict : SortedDictionary<string, SourceType>
    {
      const string VECTOR_XXX = "Vector__XXX";
      internal string toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat("{0}: ", State.BU_);
        var en = GetEnumerator();
        while (en.MoveNext())
        {
          if (en.Current.Key == VECTOR_XXX)
            continue;
          sb.Append(en.Current.Key);
          sb.Append(' ');
        }
        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
      }
    }
    /// <summary>
    /// Possible attribute definition object types.
    /// </summary>
    public enum AttribDefObjectType
    {
      /// <summary>Network</summary>
      [Description("Network")]
      Network,
      /// <summary>Node</summary>
      [Description("Node")]
      BU_,
      /// <summary>Message</summary>
      [Description("Message")]
      BO_,
      /// <summary>Signal</summary>
      [Description("Signal")]
      SG_,
      /// <summary>Environment</summary>
      [Description("Environment value")]
      EV_,
      /// <summary>Node - TX message</summary>
      [Description("Node - TxMessage")]
      BU_BO_REL_,
      /// <summary>Node - mapped RX Signal</summary>
      [Description("Node - Mapped RxSignal")]
      BU_SG_REL_,
      /// <summary>Device - Environment relative</summary>
      [Description("Device - Environment value")]
      BU_EV_REL_,
    }
    /// <summary>
    /// Possible attribute definition value types.
    /// </summary>
    public enum AttribDefValueType
    {
      /// <summary>Value is represented as string.</summary>
      STRING,
      /// <summary>Value is represented as integer.</summary>
      INT,
      /// <summary>Value is represented as float.</summary>
      FLOAT,
      /// <summary>Value is represented as integer (displayed in hex format).</summary>
      HEX,
      /// <summary>Value is represented as a enumeration value (string).</summary>
      ENUM,
    }
    /// <summary>
    /// Attributes dictionary type.
    /// 
    /// Maps CANdb++ environment names to it's corresponding messages.
    /// </summary>
    public sealed class AttribDefDict : SortedDictionary<string, AttribDefType>, IDBCAttribute
    {
      string IDBCBase.Name { get { throw new NotSupportedException(); } set { } }
      [Browsable(false), Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      /// <summary>
      /// Builds the DBC default representation string.
      /// </summary>
      /// <returns>the DBC default representation string.</returns>
      string IDBCAttribute.toFileDefaultString()
      {
        if (Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        var en = GetEnumerator();
        while (en.MoveNext())
          sb.AppendLine(((IDBCAttribute)en.Current.Value).toFileDefaultString());
        return sb.ToString();
      }
      /// <summary>
      /// Builds the DBC representation string.
      /// </summary>
      /// <returns>the DBC representation string.</returns>
      string IDBCBase.toFileString()
      {
        if (Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        var en = GetEnumerator();
        while (en.MoveNext())
          sb.AppendLine(((IDBCBase)en.Current.Value).toFileString());
        return sb.ToString();
      }
    }
    /// <summary>
    /// CANDb++ Attribute definition type.
    /// </summary>
    public sealed class AttribDefType : IDBCAttribute
    {
      #region members
      static Regex mMinMaxRegex = new Regex(Resources.strDBC_BADEFMinMaxRegex);
      static Regex mEnumRegex = new Regex(Resources.strDBC_BADEFEnumRegex);
      DBCFile mParent;
      string mName;
      AttribDefObjectType mObjectType;
      AttribDefValueType mDataType;
      #endregion
      #region constructor
      internal AttribDefType(DBCFile parent, string text, Match m)
      {
        mParent = parent;
        mName = m.Groups[1].Value;
        Enum.TryParse(m.Groups[2].Value, out mDataType);
        string remainder = text.Substring(m.Index + m.Length);
        switch (mDataType)
        {
          case AttribDefValueType.INT:
          case AttribDefValueType.HEX:
          case AttribDefValueType.FLOAT:
            Match minMaxMatch;
            if ((minMaxMatch = mMinMaxRegex.Match(remainder)).Success)
            {
              Min = double.Parse(minMaxMatch.Groups[1].Value);
              Max = double.Parse(minMaxMatch.Groups[2].Value);
            }
            else
            { // file format error
            }
            break;
          case AttribDefValueType.ENUM:
            Match m1 = mEnumRegex.Match(remainder);
            if (m1.Success)
            {
              if (Enums == null)
                Enums = new List<string>();
              Enums.Add(m1.Groups[1].Value);
              while ((m1 = m1.NextMatch()).Success)
                Enums.Add(m1.Groups[1].Value);
            }
            else
            { // file format error
            }
            break;
        }

        if (m.Index == 0)
          mObjectType = AttribDefObjectType.Network;
        else
        {
          string typeStr = text.Substring(0, m.Index).Trim(mDelimiters);
          Enum.TryParse(typeStr, out mObjectType);
        }
      }
      /// <summary>
      /// Creates a new instance of AttribDefType with the specified parameters.
      /// </summary>
      /// <param name="name">The unique attribute name</param>
      public AttribDefType(string name) { mName = name; }
      #endregion
      #region properties
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name
      {
        get { return mName; }
        set
        {
          if (checkIdentifier(mParent, this, value))
            mName = value;
        }
      }
      /// <summary>Gets or sets the object type of the attribute.</summary>
      [ReadOnly(true), Category(Constants.CATData), Description("Gets or sets the object type of the attribute.")]
      public AttribDefObjectType ObjectType { get { return mObjectType; } set { mObjectType = value; } }
      /// <summary>Gets or sets the data type of the attribute.</summary>
      [ReadOnly(true), Category(Constants.CATData), Description("Gets or sets the data type of the attribute.")]
      public AttribDefValueType DataType { get { return mDataType; } set { mDataType = value; } }
      /// <summary>Gets or sets the minimum value of the attribute.</summary>
      [Category(Constants.CATData), Description("Gets or sets the minimum value of the attribute.")]
      public double Min { get; set; }
      /// <summary>Gets or sets the maximum value of the attribute.</summary>
      [Category(Constants.CATData), Description("Gets or sets the maximum value of the attribute.")]
      public double Max { get; set; }
      /// <summary>Gets or sets the enumeration values of the attribute. (may be null)</summary>
      [Category(Constants.CATData), Description("Gets or sets the enumeration values of the attribute.")]
      public List<string> Enums { get; set; }
      /// <summary>Gets or sets the default value of the attribute.</summary>
      [Category(Constants.CATData), Description("Gets or sets the default value of the attribute.")]
      public object Value { get; set; }
      [Browsable(false), Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region methods
      string IDBCAttribute.toFileDefaultString()
      {
        var sb = new StringBuilder();
        if (Value != null)
        { // write default value.
          sb.AppendFormat("BA_DEF_{0} \"{1}\" "
            , mObjectType < AttribDefObjectType.BU_BO_REL_ ? "DEF_" : "DEF_REL_"
            , mName
            );
          switch (mDataType)
          {
            case AttribDefValueType.STRING:
            case AttribDefValueType.ENUM:
              sb.AppendFormat("\"{0}\"", Value);
              break;
            case AttribDefValueType.INT:
            case AttribDefValueType.HEX:
              sb.Append(Convert.ToInt64(Value));
              break;
            case AttribDefValueType.FLOAT:
              sb.Append(((double)Value).ToString(CultureInfo.InvariantCulture));
              break;
          }
          sb.Append(";");
        }
        return sb.ToString();
      }
      string IDBCBase.toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat("BA_{0} \"{1}\" {2} "
          , string.Format("{0} {1}"
            , mObjectType < AttribDefObjectType.BU_BO_REL_ ? "DEF_" : "DEF_REL_"
            , mObjectType == AttribDefObjectType.Network ? string.Empty : mObjectType.ToString()
            )
          , mName
          , mDataType
          );
        switch (mDataType)
        {
          case AttribDefValueType.STRING: break;
          case AttribDefValueType.INT:
          case AttribDefValueType.HEX:
            sb.AppendFormat("{0} {1}", Convert.ToInt64(Min), Convert.ToInt64(Max));
            break;
          case AttribDefValueType.FLOAT:
            sb.AppendFormat(CultureInfo.InvariantCulture, "{0} {1}", Min, Max);
            break;
          case AttribDefValueType.ENUM:
            if (Enums != null)
            {
              foreach (string s in Enums)
                sb.AppendFormat("\"{0}\",", s);
              if (Enums.Count > 0)
                sb.Remove(sb.Length - 1, 1);
            }
            break;
        }
        sb.Append(";");
        return sb.ToString();
      }
      internal static object fromStrToObject(AttribDefValueType valueType, string value)
      {
        switch (valueType)
        {
          case AttribDefValueType.ENUM:
          case AttribDefValueType.STRING:
            return value.Trim('"');
          case AttribDefValueType.INT:
          case AttribDefValueType.HEX:
          case AttribDefValueType.FLOAT:
            return double.Parse(value, mDblStyle, CultureInfo.InvariantCulture);
          default: throw new NotSupportedException(valueType.ToString());
        }
      }
      /// <summary>
      /// Gets the object's identifier.
      /// </summary>
      public override string ToString() { return Name; }
      #endregion
    }
    /// <summary>
    /// CANDb++ Attributes dictionary.
    /// </summary>
    public sealed class AttributeTypeDict : Dictionary<string, AttributeType> { }
    /// <summary>
    /// CANDb++ Attribute type.
    /// </summary>
    public sealed class AttributeType
    {
      public AttribDefType AttribDef { get; set; }
      public string Value { get; set; }
      public AttributeType(AttribDefType attribDef, string value)
      {
        AttribDef = attribDef;
        Value = value;
      }
      public override string ToString()
      {
        switch (AttribDef.DataType)
        {
          case AttribDefValueType.ENUM:
            int index = 0;
            if (int.TryParse(Value, out index) && index < AttribDef.Enums.Count)
              return AttribDef.Enums[index];
            break;
        }
        return Value;
      }
      /// <summary>
      /// Gets the attributes as DBC file representation.
      /// </summary>
      /// <param name="parent">The CANdb++ parent object</param>
      /// <returns>The DBC file representation</returns>
      internal string writeToFile(IDBCBase parent)
      {
        string writeValue;
        switch (AttribDef.DataType)
        {
          case AttribDefValueType.STRING: writeValue = '"' + Value + '"'; break;
          default: writeValue = Value; break;
        }
        var sb = new StringBuilder();
        if (parent == null)
          sb.AppendFormat("BA_ \"{0}\" {1};", AttribDef.Name, writeValue);
        else if (parent is SourceType)
          sb.AppendFormat("BA_ \"{0}\" BU_ {1} {2};", AttribDef.Name, ((SourceType)parent).Name, writeValue);
        else if (parent is MsgType)
          sb.AppendFormat("BA_ \"{0}\" BO_ {1} {2};", AttribDef.Name, ((MsgType)parent).ID, writeValue);
        else if (parent is SignalType)
          sb.AppendFormat("BA_ \"{0}\" SG_ {1} {2} {3};", AttribDef.Name, ((SignalType)parent).Msg.ID, ((SignalType)parent).Name, writeValue);
        else if (parent is EnvironmentType)
          sb.AppendFormat("BA_ \"{0}\" EV_ {1} {2};", AttribDef.Name, ((MsgType)parent).ID, writeValue);
        return sb.ToString();
      }
    }
    /// <summary>
    /// Environment dictionary type.
    /// 
    /// Maps CANdb++ environment names to it's corresponding messages.
    /// </summary>
    public sealed class EnvironmentDict : SortedDictionary<string, EnvironmentType>, IDBCBase
    {
      string IDBCBase.Name { get { throw new NotSupportedException(); } set { } }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      /// <summary>
      /// Builds the DBC representation string.
      /// </summary>
      /// <returns>the DBC representation string.</returns>
      string IDBCBase.toFileString()
      {
        if (Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        var en = GetEnumerator();
        while (en.MoveNext())
          sb.AppendLine(((IDBCBase)en.Current.Value).toFileString());
        return sb.ToString();
      }
    }
    /// <summary>
    /// Possible environment access types.
    /// </summary>
    public enum EnvAccessType
    {
      /// <summary>Not restricted</summary>
      Unrestricted = 0,
      /// <summary>Write</summary>
      Write = 2,
      /// <summary>Read</summary>
      ReadWrite = 3,
      /// <summary>Read/Write</summary>
      Read = 8001,
    }
    /// <summary>
    /// CANDb++ Environment type.
    /// </summary>
    public sealed class EnvironmentType : IDBCComment
    {
      #region members
      static readonly string mAccessIndicator = "DUMMY_NODE_VECTOR";
      DBCFile mParent;
      string mName;
      EnvType mType;
      double mMin;
      double mMax;
      string mUnit;
      double mStartValue;
      int mIndex;
      EnvAccessType mAccessType;
      string mComment;
      List<string> mReceivers;
      #endregion
      #region constructors
      /// <summary>
      /// Creates a new instance of EnvironmentType with the specified parameters.
      /// </summary>
      /// <param name="name">The environment name</param>
      /// <param name="description">The description (may be null or empty)</param>
      public EnvironmentType(string name, string description) { mName = name; mComment = description; }
      internal EnvironmentType(DBCFile file, string text, Match m)
      {
        Attributes = new AttributeTypeDict();
        mParent = file;
        mName = m.Groups[1].Value;
        Enum.TryParse(m.Groups[2].Value, out mType);
        double.TryParse(m.Groups[3].Value, mDblStyle, CultureInfo.InvariantCulture, out mMin);
        double.TryParse(m.Groups[4].Value, mDblStyle, CultureInfo.InvariantCulture, out mMax);
        mUnit = m.Groups[5].Value;
        double.TryParse(m.Groups[6].Value, mDblStyle, CultureInfo.InvariantCulture, out mStartValue);
        int.TryParse(m.Groups[7].Value, mDblStyle, CultureInfo.InvariantCulture, out mIndex);
        string access = m.Groups[8].Value.Replace(mAccessIndicator, string.Empty);
        switch (access)
        {
          default:
          case "0": mAccessType = EnvAccessType.Unrestricted; break;
          case "2": mAccessType = EnvAccessType.Write; break;
          case "3": mAccessType = EnvAccessType.ReadWrite; break;
          case "8001": mAccessType = EnvAccessType.Read; break;
        }
        string remainder = text.Substring(m.Value.Length).Trim(mDelimiters).Trim(';');
        if (!string.IsNullOrEmpty(remainder))
          mReceivers = new List<string>(remainder.Split(mSrcRefDel, StringSplitOptions.RemoveEmptyEntries));
      }
      #endregion
      #region properties
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name
      {
        get { return mName; }
        set
        {
          if (checkIdentifier(mParent, this, value))
            mName = value;
        }
      }
      /// <summary>Gets or sets the type of the environment.</summary>
      [Category(Constants.CATData), Description("Gets or sets the type of the environment.")]
      public EnvType Type { get { return mType; } set { mType = value; } }
      /// <summary>Gets or sets the physical minimum of the environment.</summary>
      [Category(Constants.CATData), Description("The physical minimum of the environment.")]
      public double Min { get { return mMin; } set { mMin = value; } }
      /// <summary>Gets or sets physical maximum  of the environment.</summary>
      [Category(Constants.CATData), Description("The physical maximum of the environment.")]
      public double Max { get { return mMax; } set { mMax = value; } }
      /// <summary>Gets or sets the unit of the environment.</summary>
      [Category(Constants.CATData), Description("The environment's unit")]
      public string Unit { get { return mUnit; } set { mUnit = value; } }
      /// <summary>Gets or sets the start value of the environment.</summary>
      [Category(Constants.CATData), Description("The environment's start value")]
      public double StartValue { get { return mStartValue; } set { mStartValue = value; } }
      /// <summary>Gets or sets the start value of the environment.</summary>
      [Category(Constants.CATData), ReadOnly(true), Description("The environment's index value"), Browsable(false)]
      public int Index { get { return mIndex; } set { mIndex = value; } }
      /// <summary>Gets or sets the access type of the environment.</summary>
      [Category(Constants.CATData), DefaultValue(EnvAccessType.Unrestricted), Description("The environment's access type")]
      public EnvAccessType Access { get { return mAccessType; } set { mAccessType = value; } }
      /// <summary>Gets or sets the list of receivers.</summary>
      [Category(Constants.CATData), Description("The list of receiving ECU's")]
      public List<string> Receivers { get { return mReceivers; } set { mReceivers = value; } }
      /// <summary>Gets or sets the signal comment.</summary>
      [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor)), Description(mCommentDesc)]
      public string Comment { get { return mComment; } set { mComment = value; } }
      /// <summary>
      /// Gets or sets the Attributes dictionary.
      /// </summary>
      [Category(Constants.CATData)]
      public AttributeTypeDict Attributes { get; set; }
      [Browsable(false), Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region methods
      string IDBCBase.toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat(CultureInfo.InvariantCulture
          , "EV_ {0}: {1} [{2}|{3}] \"{4}\" {5} {6} {7}"
          , mName
          , (int)mType
          , mType == EnvType.Integer ? ((Int64)mMin).ToString(CultureInfo.InvariantCulture) : mMin.ToString(CultureInfo.InvariantCulture)
          , mType == EnvType.Integer ? ((Int64)mMax).ToString(CultureInfo.InvariantCulture) : mMax.ToString(CultureInfo.InvariantCulture)
          , mUnit
          , mType == EnvType.Integer ? ((Int64)mStartValue).ToString(CultureInfo.InvariantCulture) : mStartValue.ToString(CultureInfo.InvariantCulture)
          , mIndex
          , string.Format("{0}{1}", mAccessIndicator, ((int)mAccessType).ToString())
          );
        if (mReceivers != null && mReceivers.Count > 0)
        { // add the source references
          sb.Append(' ');
          foreach (string s in mReceivers)
            sb.AppendFormat("{0},", s);
          sb.Remove(sb.Length - 1, 1);
        }
        sb.Append(';');
        return sb.ToString();
      }
      string IDBCComment.toFileComment()
      {
        return string.IsNullOrEmpty(mComment)
          ? null
          : string.Format("CM_ EV_ {0} \"{1}\";", mName, mComment);
      }
      string IDBCComment.toToolTipString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat("Datatype:\t{0}\nLimits:\t\t{1}..{2} {3}\nStartValue:\t{4} {5} {6}"
          , mType.ToString()
          , mMin, mMax, mUnit
          , mType == EnvType.Integer
            ? ((Int64)mStartValue).ToString(CultureInfo.InvariantCulture)
            : mStartValue.ToString(CultureInfo.InvariantCulture)
          , mUnit
          , mAccessType
          );
        return sb.ToString();
      }
      /// <summary>
      /// Gets the object's identifier.
      /// </summary>
      public override string ToString() { return Name; }
      #endregion
    }
    /// <summary>
    /// CANDb++ (DBC) Message type.
    /// </summary>
    public sealed class MsgType : IDBCComment
    {
      #region members
      uint mID;
      string mName;
      byte mDLC = 8;
      SourceType mSource;
      string mComment;
      internal List<SignalType> mSignals = new List<SignalType>();
      #endregion
      #region constructors
      /// <summary>
      /// Creates a new instance of MsgType with the specified parameters.
      /// </summary>
      /// <param name="name">The message name</param>
      /// <param name="description">The description (may be null or empty)</param>
      public MsgType(string name, string description) { mName = name; mComment = description; }
      internal MsgType(Match m, DBCFile parent)
      {
        Attributes = new AttributeTypeDict();
        uint.TryParse(m.Groups[1].Value, out mID);
        mName = m.Groups[2].Value;
        byte.TryParse(m.Groups[3].Value, out mDLC);
        string sourceName = m.Groups[4].Value;
        if (!parent.Sources.TryGetValue(sourceName, out mSource))
        {
          mSource = new SourceType(sourceName, parent);
          parent.Sources[sourceName] = mSource;
        }
      }
      #endregion
      #region properties
      /// <summary>Gets or sets the raw CAN ID.</summary>
      [Category(Constants.CATData), TypeConverter(typeof(TCHexType)), Description("The raw CAN ID may contain the extended CAN ID flag (0x80000000).")]
      public uint ID { get { return mID; } set { mID = value; } }
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name
      {
        get { return mName; }
        set
        {
          if (checkIdentifier(mSource.Parent, this, value))
            mName = value;
        }
      }
      /// <summary>Gets or sets the message length.</summary>
      [Category(Constants.CATData), DefaultValue((byte)8)]
      public byte DLC
      {
        get { return mDLC; }
        set
        {
          if (value > 64
            || value > 8 && value != 12 && value != 16 && value != 20 && value != 24 && value != 32 && value != 48 && value != 64
            )
            throw new NotSupportedException(string.Format(Resources.strDBCInvalidDLCMsg, value));
          mDLC = value;
        }
      }
      /// <summary>Gets or sets the source device.</summary>
      [Category(Constants.CATData), ReadOnly(true)]
      public SourceType Source { get { return mSource; } set { mSource = value; } }
      /// <summary>
      /// Gets a string representation for the CAN ID.
      /// </summary>
      public string IDString { get { return CANProvider.toCANIDString(mID); } }
      /// <summary>Gets or sets the message comment.</summary>
      [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor)), Description(mCommentDesc)]
      public string Comment { get { return mComment; } set { mComment = value; } }
      /// <summary>
      /// Indicates, if the ID is valid (depending on Standard or extended flags).
      /// </summary>
      public bool IsValid
      {
        get
        {
          if ((CANProvider.CAN_EXT_FLAG & mID) > 0)
            return (mID & ~CANProvider.CAN_EXT_FLAG) <= CANProvider.CAN_EXT_ID_MASK;
          else
            return mID <= CANProvider.CAN_STD_ID_MASK;
        }
      }
      /// <summary>
      /// Gets all signals of the message.
      /// </summary>
      [Category(Constants.CATData)]
      public List<SignalType> Signals { get { return mSignals; } }
      /// <summary>
      /// Gets or sets the Attributes dictionary.
      /// </summary>
      [Category(Constants.CATData)]
      public AttributeTypeDict Attributes { get; set; }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      /// <summary>
      /// Gets the timestamp of the last value received.
      /// </summary>
      [Category(SignalType.mValueCategory), ReadOnly(true)]
      public DateTime LastUpdate { get; internal set; }
      /// <summary>
      /// Gets the CAN Bus ID from which the value is received.
      /// </summary>
      [Category(SignalType.mValueCategory), ReadOnly(true)]
      public string BusID { get; internal set; }
      #endregion
      #region methods
      /// <summary>
      /// Gets the signal to the corresponding signal name.
      /// </summary>
      /// <param name="signalName">signal's name</param>
      /// <returns>the signal to the corresponding signal name or null if not found.</returns>
      public SignalType getSignal(string signalName)
      {
        for (int i = mSignals.Count - 1; i >= 0; --i)
        {
          var signal = mSignals[i];
          if (signal.Name == signalName)
            return signal;
        }
        return null;
      }
      /// <summary>
      /// Gets the defined multiplexor signal within the message (if available, may be null).
      /// </summary>
      /// <returns>the multiplexor signal instance or null.</returns>
      public SignalType getMultiplexSignal()
      {
        return mSignals.Count > 0 && mSignals[0].IsMultiplexSignal
          ? mSignals[0]
          : null;
      }
      /// <summary>
      /// Adds a signal to the message.
      /// 
      /// A potential multplexor signal is always inserted in the top of the list.
      /// </summary>
      /// <param name="signal">The signal to add to the msg</param>
      public void addSignal(SignalType signal)
      {
        signal.Msg = this;
        if (signal.IsMultiplexSignal)
          mSignals.Insert(0, signal);
        else
          mSignals.Add(signal);
      }
      /// <summary>The received rawValue.</summary>
      [Browsable(false)]
      public BigInteger RawValue { get; private set; }
      /// <summary>Length in byte of received raw value.</summary>
      [Browsable(false)]
      public int RawValueLen { get; private set; }
      public override string ToString()
      {
        return string.Format("{0}({1:X})", mName, mID);
      }
      string IDBCBase.toFileString()
      {
        return string.Format(CultureInfo.InvariantCulture
          , Resources.strDBC_BOFmt
          , mID, mName, mDLC, mSource
          );
      }
      string IDBCComment.toFileComment()
      {
        return string.IsNullOrEmpty(mComment)
          ? null
          : string.Format("CM_ BO_ {0} \"{1}\";", mID, mComment);
      }
      string IDBCComment.toToolTipString()
      {
        AttributeType attrJ1939;
        string frameStrJ1939 = string.Empty;
        if ((ID & CANProvider.CAN_EXT_FLAG) > 0
          && Attributes.TryGetValue("VFrameFormat", out attrJ1939)
          && attrJ1939.ToString() == "J1939PG"
          )
        {
          UInt32 j1939CANId = ID & CANProvider.CAN_EXT_ID_MASK;
          byte pduSrc = (byte)(j1939CANId & 0xff);
          j1939CANId >>= 8;
          UInt16 pgn = (UInt16)(j1939CANId & 0xFFFF);
          j1939CANId >>= 16;
          byte dataPage = (byte)(j1939CANId & 0x01);
          // step over reserved
          j1939CANId >>= 2;
          byte prio = (byte)(j1939CANId & 0x07);
          frameStrJ1939 = string.Format(mTTJ1939Fmt
            , pgn, pduSrc, dataPage, prio
            );
        }

        var sb = new StringBuilder();
        sb.AppendFormat(mTTMsgSignalsFmt
          , mSignals.Count
          , IDString
          , frameStrJ1939
          , string.IsNullOrEmpty(Comment)
            ? string.Empty
            : string.Format("\n\n{0}", Comment)
          );

        if (Attributes != null && Attributes.Count > 0)
        { // add attributes
          sb.Append(mTTAttr);
          foreach (var attr in Attributes)
            sb.AppendFormat(mTTAttribFmt, attr.Key, attr.Value);
        }
        return sb.ToString();
      }
      /// <summary>
      /// Returns true if the signals value is valid.
      /// 
      /// For non-multipleded signals the return value is always true.
      /// </summary>
      /// <param name="signal">The signal to check</param>
      /// <returns>true if the signals value is valid.</returns>
      internal bool checkRowCtr(SignalType signal)
      {
        if (!signal.IsMultiplexed)
          // not multiplexed
          return true;

        // get the row counter signal
        var rowCtrSignal = getMultiplexSignal();
        if (rowCtrSignal == null)
          // row counter signal not found
          return false;

        // row counter (multiplexer) signal found
        // return true, if the multiplex value of the multiplexed signal is hit
        UInt64 rawValue;
        bool result = rowCtrSignal.getRawValue(out rawValue);
        return result && rawValue == signal.MultiplexValue;
      }
      /// <summary>
      /// Handles the received CAN data.
      /// </summary>
      /// <param name="frame">The received CAN frame</param>
      internal void onCANReceived(CANFrame frame)
      {
        RawValueLen = frame.Data.Length;
        LastUpdate = DateTime.Now;
        BusID = frame.BusID;
        RawValue = new BigInteger(frame.Data);
      }
      #endregion
    }
    /// <summary>
    /// CANDb++ (DBC) Signal type.
    /// </summary>
    public sealed class SignalType : IDBCSignal
    {
      #region members
      internal const string mValueCategory = "Value";
      MsgType mMsg;
      string mName;
      SignalFlagsType mFlags;
      ushort mStart;
      byte mLen;
      byte mMultiplexValue;
      double mFactor = 1.0;
      double mOffset;
      double mMin;
      double mMax;
      string mUnit;
      string mComment;
      List<string> mReceivers;
      UInt64 mMask;
      SignalFloatType mFloatType;
      #endregion
      #region constructors
      /// <summary>
      /// Creates a new instance of SignalType with the specified parameters.
      /// </summary>
      /// <param name="name">The signal name</param>
      /// <param name="description">The description (may be null or empty)</param>
      public SignalType(string name, string description) { mName = name; mComment = description; }
      internal SignalType(string text, Match m)
      {
        Attributes = new AttributeTypeDict();
        Group g = m.Groups[1];
        string name = g.Value;
        if (m.Index > 0)
        {
          switch (name[0])
          {
            case 'M': // multiplexer signal
              Flags |= SignalFlagsType.MultiplexSignal;
              break;
            case 'm': // multiplexed value
              byte.TryParse(name.Substring(1), out mMultiplexValue);
              Flags |= SignalFlagsType.Multiplexed;
              break;
          }
          mName = text.Substring(0, g.Index).Trim(mDefDelimiters);
        }
        else
          mName = name;
        ushort.TryParse(m.Groups[2].Value, out mStart);
        byte.TryParse(m.Groups[3].Value, out mLen);
        if (m.Groups[4].Value == "0")
          mFlags |= SignalFlagsType.BigEndian;
        if (m.Groups[5].Value == "-")
          mFlags |= SignalFlagsType.Signed;

        mStart = getAdjustedStartBit();

        double.TryParse(m.Groups[6].Value, mDblStyle, CultureInfo.InvariantCulture, out mFactor);
        double.TryParse(m.Groups[7].Value, mDblStyle, CultureInfo.InvariantCulture, out mOffset);
        double.TryParse(m.Groups[8].Value, mDblStyle, CultureInfo.InvariantCulture, out mMin);
        double.TryParse(m.Groups[9].Value, mDblStyle, CultureInfo.InvariantCulture, out mMax);
        mUnit = m.Groups[10].Value;
        string remainder = text.Substring(m.Index + m.Value.Length).Trim(mDelimiters);
        if (!string.IsNullOrEmpty(remainder))
          mReceivers = new List<string>(remainder.Split(mSrcRefDel, StringSplitOptions.RemoveEmptyEntries));
      }
      #endregion
      #region properties
      /// <summary>Gets or sets the message reference.</summary>
      [Category(Constants.CATData), ReadOnly(true)]
      public MsgType Msg { get { return mMsg; } set { mMsg = value; } }
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name
      {
        get { return mName; }
        set
        {
          if (checkIdentifier(mMsg.Source.Parent, this, value))
            mName = value;
        }
      }
      /// <summary>
      /// Gets or sets the multiplex value when the signal is valid.
      /// 
      /// Only valid if the signal is a multiplexed signal (see IsMultiplexed).
      /// </summary>
      [Category(Constants.CATData), DefaultValue(byte.MinValue), Description("Only valid if the signal is a multiplexed signal (see IsMultiplexed).")]
      public byte MultiplexValue { get { return mMultiplexValue; } set { mMultiplexValue = value; } }
      /// <summary>Gets or sets the start bit of the signal.</summary>
      [Category(Constants.CATData), Description("The start bit within the message of the signal (zero-based)")]
      public UInt16 Start { get { return mStart; } set { mStart = value; } }
      /// <summary>Gets or sets the length of the signal in bits.</summary>
      [Category(Constants.CATData), Description("The length of the signal in bits.")]
      public byte Len { get { return mLen; } set { mLen = value; mMask = 0; } }
      /// <summary>Gets or sets the factor value of the signal.</summary>
      [Category(Constants.CATData), DefaultValue(1.0), Description("The factor to multiply the raw value with")]
      public double Factor { get { return mFactor; } set { mFactor = value; } }
      /// <summary>Gets or sets the offset value of the signal.</summary>
      [Category(Constants.CATData), DefaultValue(0.0), Description("The physical offset to add to the raw value.")]
      public double Offset { get { return mOffset; } set { mOffset = value; } }
      /// <summary>Gets or sets the physical minimum of the signal.</summary>
      [Category(mValueCategory), Description("The physical minimum of the signal.")]
      public double Min { get { return mMin; } set { mMin = value; } }
      /// <summary>Gets or sets physical maximum  of the signal.</summary>
      [Category(mValueCategory), Description("The physical maximum of the signal.")]
      public double Max { get { return mMax; } set { mMax = value; } }
      /// <summary>Gets or sets the unit of the signal.</summary>
      [Category(mValueCategory), Description("The signal's unit")]
      public string Unit { get { return mUnit; } set { mUnit = value; } }
      /// <summary>Gets or sets the signal flags.</summary>
      [Category(Constants.CATData), ReadOnly(true)]
      public SignalFlagsType Flags { get { return mFlags; } set { mFlags = value; } }
      /// <summary>Gets or sets the signal comment.</summary>
      [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor)), Description(mCommentDesc)]
      public string Comment { get { return mComment; } set { mComment = value; } }
      /// <summary>Gets or sets the list of receivers.</summary>
      [Category(Constants.CATData), Description("The list of receiving ECU's")]
      public List<string> Receivers { get { return mReceivers; } set { mReceivers = value; } }
      /// <summary>Gets or sets the little endian property.</summary>
      [Category(Constants.CATData), DefaultValue(true)]
      public bool IsLittleEndian
      {
        get { return (mFlags & SignalFlagsType.BigEndian) == 0; }
        set
        {
          if (value)
            mFlags &= ~SignalFlagsType.BigEndian;
          else
            mFlags |= SignalFlagsType.BigEndian;
        }
      }
      /// <summary>Gets or sets the multiplex signal property.</summary>
      [Category(Constants.CATData), DefaultValue(false)]
      public bool IsMultiplexSignal
      {
        get { return (mFlags & SignalFlagsType.MultiplexSignal) > 0; }
        set
        {
          if (value)
            mFlags |= SignalFlagsType.MultiplexSignal;
          else
            mFlags &= ~SignalFlagsType.MultiplexSignal;
        }
      }
      /// <summary>
      /// Gets or sets a value if the signal is multiplexed.
      /// 
      /// In this case the MultiplexValue is valid.
      /// </summary>
      [Category(Constants.CATData), DefaultValue(false)]
      public bool IsMultiplexed
      {
        get { return (mFlags & SignalFlagsType.Multiplexed) > 0; }
        set
        {
          if (value)
            mFlags |= SignalFlagsType.Multiplexed;
          else
            mFlags &= ~SignalFlagsType.Multiplexed;
        }
      }
      /// <summary>Gets or sets the signed property.</summary>
      [Category(Constants.CATData), DefaultValue(false)]
      public bool IsSigned
      {
        get { return (mFlags & SignalFlagsType.Signed) > 0; }
        set
        {
          if (value)
            mFlags |= SignalFlagsType.Signed;
          else
            mFlags &= ~SignalFlagsType.Signed;
        }
      }
      /// <summary>
      /// Gets the data type of the raw value.
      /// </summary>
      [Category(mValueCategory), ReadOnly(true)]
      public DataType DataType
      {
        get
        {
          switch (mFloatType)
          {
            case SignalFloatType.IEEEFloat32: return DataType.FLOAT32;
            case SignalFloatType.IEEEFloat64: return DataType.FLOAT64;
            default:
              switch (mLen)
              {
                case 8: return IsSigned ? DBCFile.DataType.SBYTE : DataType.UBYTE;
                case 16: return IsSigned ? DBCFile.DataType.SWORD : DataType.UWORD;
                case 32: return IsSigned ? DBCFile.DataType.SLONG : DataType.ULONG;
                case 64: return IsSigned ? DBCFile.DataType.INT64 : DataType.UINT64;
                default: return DBCFile.DataType.BITFIELD;
              }
          }
        }
      }
      /// <summary>
      /// Gets or sets the float type of the value.
      /// </summary>
      [DefaultValue(SignalFloatType.Integer), Category(mValueCategory), Description("Gets or sets the floating-point type of the value.")]
      public SignalFloatType FloatType
      {
        get { return mFloatType; }
        set
        {
          mFloatType = value;
          switch (mFloatType)
          {
            case SignalFloatType.IEEEFloat32: mLen = 32; mFlags |= SignalFlagsType.Signed; break;
            case SignalFloatType.IEEEFloat64: mLen = 64; mFlags |= SignalFlagsType.Signed; break;
          }
          mMask = 0;
        }
      }
      /// <summary>
      /// Gets the most recent raw value received.
      /// </summary>
      /// <param name="ui64">Result if return value is true</param>
      /// <returns>true, if a valid value is available</returns>
      public bool getRawValue(out UInt64 ui64)
      {
        ui64 = 0;
        if (mMsg.LastUpdate == DateTime.MinValue)
          return false;

        BigInteger rawData = mMsg.RawValue;
        if (rawData == null || mLen == 0 || mStart + mLen > mMsg.RawValueLen * 8)
          // signal not valid, or signal not in data
          return false;

        if (!mMsg.checkRowCtr(this))
          // signal is not valid for current multiplex value
          return false;

        if (mStart > 0)
          // shift down from signal start bit
          rawData >>= mStart;

        rawData &= UInt64.MaxValue;
        ui64 = (ulong)rawData & Mask;
        bool changeEndianess = BitConverter.IsLittleEndian != IsLittleEndian;
        if (changeEndianess)
        {
          switch (DataType)
          {
            case DataType.BITFIELD:
              if (mLen > 32)
              {
                ui64 = Extensions.changeEndianess(ui64);
                ui64 = (UInt64)(ui64 & UInt32.MaxValue | (ui64 & 0xffffffff00000000) >> (64 - mLen));
              }
              else if (mLen > 16)
              {
                ui64 = Extensions.changeEndianess((UInt32)ui64);
                ui64 = (UInt64)(ui64 & UInt16.MaxValue | (ui64 & 0xffff0000) >> (32 - mLen));
              }
              else if (mLen > 8)
              {
                ui64 = Extensions.changeEndianess((UInt16)ui64);
                ui64 = (UInt64)(ui64 & byte.MaxValue | (ui64 & 0xff00) >> (16 - mLen));
              }
              break;
            case DataType.SWORD:
            case DataType.UWORD: ui64 = Extensions.changeEndianess((UInt16)ui64); break;
            case DataType.SLONG:
            case DataType.ULONG: ui64 = Extensions.changeEndianess((UInt32)ui64); break;
            case DataType.INT64:
            case DataType.UINT64: ui64 = Extensions.changeEndianess((UInt64)ui64); break;
            case DataType.FLOAT32:
              var bytes = Extensions.changeEndianess(BitConverter.GetBytes(ui64), 0, 4);
              ui64 = BitConverter.ToUInt32(bytes, 0);
              break;
            case DataType.FLOAT64:
              bytes = Extensions.changeEndianess(BitConverter.GetBytes(ui64), 0, 8);
              ui64 = BitConverter.ToUInt64(bytes, 0);
              break;
          }
        }
        return true;
      }
      /// <summary>
      /// Gets the computed mask for the raw value.
      /// </summary>
      [Category(mValueCategory), ReadOnly(true), TypeConverter(typeof(TCHexType))]
      public UInt64 Mask
      {
        get
        {
          if (mMask == 0)
          { // build mask from datatype
            switch (DataType)
            {
              case DataType.SBYTE:
              case DataType.UBYTE: mMask = byte.MaxValue; break;
              case DataType.SWORD:
              case DataType.UWORD: mMask = UInt16.MaxValue; break;
              case DataType.SLONG:
              case DataType.ULONG:
              case DataType.FLOAT32: mMask = UInt32.MaxValue; break;
              case DataType.INT64:
              case DataType.UINT64:
              case DataType.FLOAT64: mMask = UInt64.MaxValue; break;
              case DataType.BITFIELD:
                mMask = 0;
                for (int i = mLen - 1; i >= 0; --i)
                {
                  mMask |= 0x01;
                  if (i > 0)
                    mMask <<= 1;
                }
                break;
              default: throw new NotSupportedException(DataType.ToString());
            }
          }
          return mMask;
        }
      }
      /// <summary>
      /// Gets or sets the enum dictionary for the value (may be null).
      /// </summary>
      [Category(mValueCategory), ReadOnly(true), Description("Predefined value mapping.")]
      public EnumDict Enums { get; set; }
      /// <summary>
      /// Gets or sets the Attributes dictionary.
      /// </summary>
      [Category(Constants.CATData)]
      public AttributeTypeDict Attributes { get; set; }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region methods
      /// <summary>
      /// Gets the current value as a string representation.
      /// </summary>
      /// <param name="format">The value format to display</param>
      /// <returns>The string represntation of the value (string.Empty if no value is available)</returns>
      public string toStringValue(ValueObjectFormat format)
      {
        UInt64 rawValue;
        if (!getRawValue(out rawValue))
          // raw value not available
          return string.Empty;
        return toStringValue(rawValue, format);
      }
      /// <summary>
      /// Gets the current value as a string representation.
      /// </summary>
      /// <param name="rawValue">The raw value</param>
      /// <param name="format">The value format to display</param>
      /// <returns>The string represntation of the value (string.Empty if no value is available)</returns>
      public string toStringValue(UInt64 rawValue, ValueObjectFormat format)
      {
        switch (format)
        {
          case ValueObjectFormat.Physical:
            if (Enums == null)
            { // numeric value
              double value = double.NaN;
              switch (DataType)
              {
                case DataType.FLOAT32:
                  var bytes = BitConverter.GetBytes(rawValue);
                  value = BitConverter.ToSingle(bytes, 0);
                  break;
                case DataType.FLOAT64:
                  bytes = BitConverter.GetBytes(rawValue);
                  value = BitConverter.ToDouble(bytes, 0);
                  break;
                default:
                  value = rawValue;
                  break;
              }
              value = value * mFactor + mOffset;
              return value.ToString("f6", CultureInfo.InvariantCulture);
            }
            else
            { // enumeration
              string valueStr;
              if (Enums.TryGetValue(Convert.ToInt64(rawValue), out valueStr))
                // enum value
                return valueStr;
              return "default value (" + rawValue + ")";
            }
          case ValueObjectFormat.Raw:
            return toDecimalString(rawValue, DataType);
          case ValueObjectFormat.RawHex:
            return toHexString(rawValue, DataType);
          case ValueObjectFormat.RawBin:
            return toBinaryString(rawValue, DataType);
          default: throw new NotSupportedException(format.ToString());
        }
      }
      /// <summary>
      /// Delivers a decimal string representation for the specified raw value.
      /// </summary>
      /// <param name="rawValue">The raw value</param>
      /// <param name="dataType">The corresponding raw value data type</param>
      /// <returns>The string representation</returns>
      static string toDecimalString(ulong rawValue, DataType dataType)
      {
        return Extensions.toDecimalString(rawValue
          , dataType == DataType.BITFIELD ? DATA_TYPE.A_UINT64 : (DATA_TYPE)dataType
          );
      }
      /// <summary>
      /// Delivers a hexadecimal string representation for the specified raw value.
      /// </summary>
      /// <param name="rawValue">The raw value</param>
      /// <param name="dataType">The corresponding raw value data type</param>
      /// <returns>The string representation</returns>
      string toHexString(ulong rawValue, DataType dataType)
      {
        switch (dataType)
        {
          case DataType.BITFIELD:
            string valueStr;
            int countBytes = Len / 8 + 1;
            switch (countBytes)
            {
              case 1: valueStr = Extensions.toHexString(rawValue, DATA_TYPE.UBYTE, true); break;
              case 2: valueStr = Extensions.toHexString(rawValue, DATA_TYPE.UWORD, true); break;
              case 3:
              case 4: valueStr = Extensions.toHexString(rawValue, DATA_TYPE.ULONG, true); break;
              case 5:
              case 6:
              case 7:
              case 8: valueStr = Extensions.toHexString(rawValue, DATA_TYPE.A_UINT64, true); break;
              default: throw new NotSupportedException(countBytes.ToString());
            }
            return valueStr;
          default:
            return Extensions.toHexString(rawValue, (DATA_TYPE)dataType, false);
        }
      }
      /// <summary>
      /// Delivers a binary string representation for the specified raw value.
      /// </summary>
      /// <param name="rawValue">The raw value</param>
      /// <param name="dataType">The corresponding raw value data type</param>
      /// <returns>The string representation</returns>
      string toBinaryString(ulong rawValue, DataType dataType)
      {
        switch (dataType)
        {
          case DataType.BITFIELD:
            string valueStr;
            int countBytes = Len / 8 + 1;
            switch (countBytes)
            {
              case 1: valueStr = Extensions.toBinaryString(rawValue, DATA_TYPE.UBYTE, true); break;
              case 2: valueStr = Extensions.toBinaryString(rawValue, DATA_TYPE.UWORD, true); break;
              case 3:
              case 4: valueStr = Extensions.toBinaryString(rawValue, DATA_TYPE.ULONG, true); break;
              case 5:
              case 6:
              case 7:
              case 8: valueStr = Extensions.toBinaryString(rawValue, DATA_TYPE.A_UINT64, true); break;
              default: throw new NotSupportedException(countBytes.ToString());
            }
            return valueStr;
          default:
            return Extensions.toBinaryString(rawValue, (DATA_TYPE)dataType, true);
        }
      }
      /// <summary>
      /// Gets the object's identifier.
      /// </summary>
      public override string ToString() { return mName; }
      string IDBCComment.toToolTipString()
      {
        var sb = new StringBuilder();
        if (IsMultiplexSignal)
          sb.AppendLine("Multiplexor signal");
        if (IsMultiplexed)
        {
          var muxSignal = mMsg.getMultiplexSignal();
          sb.AppendLine(string.Format("Data valid if multiplexor signal {0} equals {1}"
            , muxSignal != null ? muxSignal.Name : "[not defined!]"
            , mMultiplexValue
            ));
        }
        sb.AppendFormat(mTTSignalFmt
          , IsLittleEndian ? "Little endian" : "Big endian"
          , DataType == DataType.BITFIELD ? string.Format("{0}({1})", DataType, mLen) : DataType.ToString()
          , mMin, mMax, mUnit
          );
        if (mFactor != 1.0)
          sb.AppendFormat(mTTFactorFmt, mFactor);
        if (mOffset != 0.0)
          sb.AppendFormat(mTTOffsetFmt, mOffset);

        if (!string.IsNullOrEmpty(Comment))
          sb.AppendFormat("\n{0}", Comment);

        if (Attributes != null && Attributes.Count > 0)
        { // add attributes
          sb.Append(mTTAttr);
          foreach (var attr in Attributes)
            sb.AppendFormat(mTTAttribFmt, attr.Key, attr.Value);
        }
        return sb.ToString();
      }
      string IDBCBase.toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat(CultureInfo.InvariantCulture
          , Resources.strDBC_SGFmt
          , Name
          , (mFlags & SignalFlagsType.MultiplexSignal) > 0
            ? " M "
            : (mFlags & SignalFlagsType.Multiplexed) > 0
              ? string.Format(" m{0} ", mMultiplexValue)
              : string.Empty
          , getAdjustedStartBit(), mLen
          , (mFlags & SignalFlagsType.BigEndian) > 0 ? '0' : '1'
          , (mFlags & SignalFlagsType.Signed) > 0 ? '-' : '+'
          , mFactor, mOffset
          , mMin, mMax
          , mUnit
          );
        if (mReceivers != null && mReceivers.Count > 0)
        { // add the source references
          sb.Append(' ');
          foreach (string s in mReceivers)
            sb.AppendFormat("{0},", s);
          sb.Remove(sb.Length - 1, 1);
        }
        return sb.ToString();
      }
      string IDBCComment.toFileComment()
      {
        return string.IsNullOrEmpty(mComment)
          ? null
          : string.Format("CM_ SG_ {0} {1} \"{2}\";", mMsg.ID, mName, mComment);
      }
      string IDBCSignal.toValString()
      {
        if (Enums == null || Enums.Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        sb.AppendFormat("VAL_ {0} {1} ", mMsg.ID, mName);
        var en = Enums.GetEnumerator();
        while (en.MoveNext())
          sb.AppendFormat("{0} \"{1}\" ", en.Current.Key, en.Current.Value);
        sb.Remove(sb.Length - 1, 1);
        sb.Append(';');
        return sb.ToString();
      }
      /// <summary>
      /// Adjusts the start bit for the DBC file after importing and before exporting. 
      /// </summary>
      ushort getAdjustedStartBit()
      {
        return IsLittleEndian
          ? mStart
          : (ushort)((~mStart & 0x07) + (mStart & 0xFFF8));
      }
      #endregion
    }
    /// <summary>
    /// Defines a value table dictionary (mapping double values to strings).
    /// </summary>
    public sealed class EnumDict : SortedDictionary<Int64, string> { }
    /// <summary>
    /// Defines a value table dictionary (mapping value table idents to value tables).
    /// </summary>
    public sealed class ValueTableDict : Dictionary<string, ValueTable>, IDBCBase
    {
      string IDBCBase.Name { get { throw new NotSupportedException(); } set { } }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      /// <summary>
      /// Builds the DBC representation string.
      /// </summary>
      /// <returns>the DBC representation string.</returns>
      string IDBCBase.toFileString()
      {
        if (Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        var en = GetEnumerator();
        while (en.MoveNext())
          sb.AppendLine(((IDBCBase)en.Current.Value).toFileString());
        return sb.ToString();
      }
    }
    /// <summary>
    /// Defines the CANDb++ value table.
    /// </summary>
    public sealed class ValueTable : IDBCBase
    {
      #region constructor
      /// <summary>
      /// Creates a new instance of ValueTable with the specified parameters.
      /// </summary>
      /// <param name="name">The value table's name</param>
      public ValueTable(string name) { Name = name; Enum = new EnumDict(); }
      #endregion
      #region properties
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name { get; set; }
      /// <summary>Gets or sets the corresponding enumerations.</summary>
      [Category(Constants.CATData)]
      public EnumDict Enum { get; set; }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region methods
      string IDBCBase.toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat("{0} {1} ", State.VAL_TABLE_, Name);
        var en = Enum.GetEnumerator();
        while (en.MoveNext())
          sb.AppendFormat(CultureInfo.InvariantCulture, "{0} \"{1}\" ", en.Current.Key, en.Current.Value);
        if (Enum.Count > 0)
          sb.Remove(sb.Length - 1, 1);
        sb.Append(';');
        return sb.ToString();
      }
      /// <summary>
      /// Gets the object's identifier.
      /// </summary>
      public override string ToString() { return Name; }
      #endregion
    }
    /// <summary>
    /// Defines a signal group table dictionary (mapping signal group IDs to signal groups).
    /// </summary>
    public sealed class SignalGroupDict : Dictionary<uint, SignalGroupType>, IDBCBase
    {
      string IDBCBase.Name { get { throw new NotSupportedException(); } set { } }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      /// <summary>
      /// Builds the DBC representation string.
      /// </summary>
      /// <returns>the DBC representation string.</returns>
      string IDBCBase.toFileString()
      {
        if (Count == 0)
          return string.Empty;
        var sb = new StringBuilder();
        var en = GetEnumerator();
        while (en.MoveNext())
          sb.AppendLine(((IDBCBase)en.Current.Value).toFileString());
        return sb.ToString();
      }
    }
    /// <summary>
    /// Defines the CANDb++ signal group table.
    /// </summary>
    public sealed class SignalGroupType : IDBCBase
    {
      #region constructor
      internal SignalGroupType(string text, Match m)
      {
        ID = uint.Parse(m.Groups[1].Value);
        Name = m.Groups[2].Value;
        Number = int.Parse(m.Groups[3].Value);
        string remainder = text.Substring(m.Value.Length);
        SignalRefs = new HashSet<string>();
        if (!string.IsNullOrEmpty(remainder))
        {
          string[] refs = remainder.Split(mSigGroupDel, StringSplitOptions.RemoveEmptyEntries);
          foreach (string reference in refs)
            SignalRefs.Add(reference);
        }
      }
      #endregion
      #region properties
      /// <summary>Gets or sets the ID.</summary>
      [Category(Constants.CATData), ReadOnly(true)]
      public uint ID { get; set; }
      /// <summary>Gets or sets the ident name.</summary>
      [Category(Constants.CATData), Description(mObjectIDDesc)]
      public string Name { get; set; }
      /// <summary>Gets or sets the number.</summary>
      [Category(Constants.CATData), DefaultValue(0)]
      public int Number { get; set; }
      /// <summary>Gets or sets the signal references.</summary>
      [Category(Constants.CATData)]
      public HashSet<string> SignalRefs { get; set; }
      [Category(Constants.CATData), Description("Gets or sets a reference to a user object.")]
      public object Tag { get; set; }
      #endregion
      #region methods
      string IDBCBase.toFileString()
      {
        var sb = new StringBuilder();
        sb.AppendFormat("{0} {1} {2} {3}: ", State.SIG_GROUP_, ID, Name, Number);
        var en = SignalRefs.GetEnumerator();
        while (en.MoveNext())
          sb.AppendFormat("{0} ", en.Current);
        if (SignalRefs.Count > 0)
          sb.Remove(sb.Length - 1, 1);
        sb.Append(';');
        return sb.ToString();
      }
      #endregion
    }
    #endregion
    #region members
    const string mObjectIDDesc = "The object identifier";
    const string mCommentDesc = "The object's description.";
    const string mTTSignalFmt = "Endianess:\t{0}\nDatatype:\t{1}\nLimits:\t\t{2}..{3} {4}\n";
    const string mTTJ1939Fmt = "\n\nJ1939\nPGN:\t\t0x{0:X4}({0})\nPDU Src:\t\t0x{1:X2}\nDataPage:\t{2}\nPriority:\t\t{3}";
    const string mTTMsgSignalsFmt = "Count signals:\t{0}\nCAN ID:\t\t{1}{2}{3}";
    const string mTTSrcMessagesFmt = "Count messages: {0}{1}";
    const string mTTAttr = "\n\nAttributes:";
    const string mTTAttribFmt = "\n{0}: {1}";
    const string mTTFactorFmt = "Factor:\t\t{0}\n";
    const string mTTOffsetFmt = "Offset:\t\t{0}\n";
    static readonly char[] mDefDelimiters = new char[] { ' ', '\t' };
    static Regex mDesignatorRegex = new Regex("[A-Z_]+[A-Z0-9_]*", RegexOptions.IgnoreCase);
    /// <summary>
    /// Regex for matching the BO_ type.
    /// </summary>
    static Regex mBORegex = new Regex(Resources.strDBC_BORegex);
    /// <summary>
    /// Regex for matching the SG_ type.
    /// </summary>
    static Regex mSGRegex = new Regex(Resources.strDBC_SGRegex, RegexOptions.IgnoreCase);
    /// <summary>
    /// Regex for matching the VAL_TABLE enums.
    /// </summary>
    static Regex mValTableRegex = new Regex(Resources.strDBC_ValTableRegex, RegexOptions.IgnoreCase);
    /// <summary>
    /// Regex for matching the Environment types.
    /// </summary>
    static Regex mEVRegex = new Regex(Resources.strDBC_EVRegex, RegexOptions.IgnoreCase);
    /// <summary>
    /// Regex for matching the Comment EV_ type.
    /// </summary>
    static Regex mCMEVRegex = new Regex(Resources.strDBC_CMEVRegex);
    /// <summary>
    /// Regex for matching the BA_ type.
    /// </summary>
    static Regex mBARegex = new Regex(Resources.strDBC_BARegex);
    static Dictionary<AttribDefObjectType, Regex> mBARegexDict;
    /// <summary>
    /// Regex for matching the BA_DEF type.
    /// </summary>
    static Regex mBADEFRegex = new Regex(Resources.strDBC_BADEFRegex);
    /// <summary>
    /// Regex for matching the BA_DEF_DEF_ type.
    /// </summary>
    static Regex mBADEFDEFRegex = new Regex(Resources.strDBC_BADEFDEFRegex);
    /// <summary>
    /// Regex for matching the VAL_TABLE enums.
    /// </summary>
    static Regex mSigGroupRegex = new Regex(Resources.strDBC_SigGroupRegex, RegexOptions.IgnoreCase);
    /// <summary>
    /// Regex for matching the Comment BO_ type.
    /// </summary>
    static Regex mCMBORegex = new Regex(Resources.strDBC_CMBORegex);
    /// <summary>
    /// Regex for matching the Comment BU_ type.
    /// </summary>
    static Regex mCMBURegex = new Regex(Resources.strDBC_CMBURegex);
    /// <summary>
    /// Regex for matching the Comment main type.
    /// </summary>
    static Regex mCMRegex = new Regex(Resources.strDBC_CMRegex);
    /// <summary>
    /// Regex for matching the Comment SG_ type.
    /// </summary>
    static Regex mCMSGRegex = new Regex(Resources.strDBC_CMSGRegex);
    /// <summary>
    /// Regex for matching the VAL_ type.
    /// </summary>
    static Regex mVALRegex = new Regex(Resources.strDBC_VALRegex);
    /// <summary>
    /// Regex for matching the SIG_VALTYPE_ type.
    /// </summary>
    static Regex mSIGVALTYPERegex = new Regex(Resources.strDBC_SIGVALTYPERegex);
    static readonly char[] mDelimiters = new char[] { ' ', '\t', ':' };
    static readonly char[] mSrcRefDel = new char[] { ',', ' ', '\t' };
    static readonly char[] mSigGroupDel = new char[] { ' ', '\t', ';' };
    static readonly char[] mBAValueDel = new char[] { '"', ';' };
    static NumberStyles mDblStyle = NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowExponent;
    string mSourceFile;
    Dictionary<uint, MsgType> mCANRceciverCache;
    #endregion
    #region constructors
    static DBCFile()
    {
      mBARegexDict = new Dictionary<AttribDefObjectType, Regex>();
      mBARegexDict[AttribDefObjectType.BU_] = new Regex(Resources.strDBC_BABURegex);
      mBARegexDict[AttribDefObjectType.BO_] = new Regex(Resources.strDBC_BABORegex);
      mBARegexDict[AttribDefObjectType.SG_] = new Regex(Resources.strDBC_BASGRegex);
      mBARegexDict[AttribDefObjectType.EV_] = new Regex(Resources.strDBC_BAEVRegex);
    }
    /// <summary>
    /// Creates an (empty) instance of DBCFile.
    /// </summary>
    public DBCFile()
    {
      Sources = new SourceDict();
      ValueTables = new ValueTableDict();
      Environments = new EnvironmentDict();
      AttributeDefinitions = new AttribDefDict();
      Attributes = new AttributeTypeDict();
      SignalGroups = new SignalGroupDict();
    }
    /// <summary>
    /// Creates an (empty) instance of DBCFile.
    /// </summary>
    /// <param name="version">A version to set</param>
    public DBCFile(string version)
      : this()
    {
      Version = version;
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the fully qualified path to the source file.
    /// </summary>
    [Category(Constants.CATData), ReadOnly(true), Description("Source file path.")]
    public string SourceFile { get { return mSourceFile; } }
    /// <summary>
    /// Gets or set the CANdb++ file version.
    /// </summary>
    [Category(Constants.CATData), Description("The content version string.")]
    public string Version { get; set; }
    /// <summary>
    /// Gets or set the CANdb++ file comment.
    /// </summary>
    [Category(Constants.CATData), Editor(typeof(MultilineStringEditor), typeof(UITypeEditor)), Description(mCommentDesc)]
    public string Comment { get; set; }
    /// <summary>
    /// Gets or sets the namespace.
    /// </summary>
    [Category(Constants.CATData), Description("The collection of supported keywords.")]
    public List<string> NS { get; set; }
    /// <summary>
    /// Gets or sets the contained sources.
    /// 
    /// Maps CANdb++ source names to it's corresponding messages.
    /// </summary>
    [Category(Constants.CATData), Description("The set of network devices.")]
    public SourceDict Sources { get; set; }
    /// <summary>
    /// Gets or sets the contained value tables.
    /// </summary>
    [Category(Constants.CATData)]
    public ValueTableDict ValueTables { get; set; }
    /// <summary>
    /// Gets or sets the enviroments dictionary.
    /// </summary>
    [Category(Constants.CATData)]
    public EnvironmentDict Environments { get; set; }
    /// <summary>
    /// Gets or sets the Attributes dictionary.
    /// </summary>
    [Category(Constants.CATData)]
    public AttribDefDict AttributeDefinitions { get; set; }
    /// <summary>
    /// Gets or sets the contained signal groups.
    /// </summary>
    [Category(Constants.CATData)]
    public SignalGroupDict SignalGroups { get; set; }
    /// <summary>
    /// Gets or sets the Attributes dictionary.
    /// </summary>
    [Category(Constants.CATData)]
    public AttributeTypeDict Attributes { get; set; }
    #endregion
    #region methods
    /// <summary>
    /// Opens a DBC file and returns a DBCFile instance.
    /// </summary>
    /// <param name="path">the fully qualified DBC filename</param>
    /// <returns>A new DBCFile instance or null if failed</returns>
    public static DBCFile open(string path)
    {
      var instance = new DBCFile();
      instance.mSourceFile = path;
      using (var sr = new StreamReader(path, Encoding.Default))
      {
        int lineCount = 0;
        var ns = new List<string>();
        var cm = new List<string>();
        MsgType msg = null;
        var msgDict = new Dictionary<uint, MsgType>();
        instance.Version = string.Empty;
        string line;
        while (null != (line = sr.ReadLine()))
        {
          ++lineCount;

          line = line.Trim(mDefDelimiters);
          if (string.IsNullOrEmpty(line))
            // line is empty
            continue;
          int endCodeIdx = line.IndexOfAny(mDefDelimiters);
          string cmd = endCodeIdx > -1 ? line.Substring(0, endCodeIdx).Trim(mDelimiters) : null;
          string valStr = line.Substring(endCodeIdx + 1).Trim(mDelimiters);
          State newState = State.Unknown;
          Enum.TryParse(cmd, out newState);

          switch (newState)
          {
            case State.VERSION:
              instance.Version = valStr.Trim('"');
              break;

            case State.NS_:
              if (!string.IsNullOrEmpty(valStr))
                ns.Add(cmd);

              string nextLine;
              while (null != (nextLine = sr.ReadLine()))
              {
                nextLine = nextLine.Trim(mDelimiters);
                if (nextLine.StartsWith(State.BS_.ToString()))
                  break;
                if (!string.IsNullOrEmpty(nextLine))
                  ns.Add(nextLine);
              }
              break;

            case State.BU_:
              string[] BUs = valStr.Split(mDelimiters, StringSplitOptions.RemoveEmptyEntries);
              foreach (string s in BUs)
                instance.Sources[s] = new SourceType(s, instance);
              break;

            case State.BA_: // Attribute
              while (valStr[valStr.Length - 1] != ';')
              {
                nextLine = sr.ReadLine();
                valStr += Environment.NewLine + nextLine.Trim(mDelimiters);
              }
              Match m = mBARegex.Match(valStr);
              if (m.Success)
              {
                AttribDefType attrib;
                string attribName = m.Groups[1].Value;
                if (instance.AttributeDefinitions.TryGetValue(attribName, out attrib))
                {
                  string remainder = valStr.Substring(m.Index + m.Length);
                  switch (attrib.ObjectType)
                  {
                    case AttribDefObjectType.Network:
                      instance.Attributes[attribName] = new AttributeType(attrib, remainder.Trim(mBAValueDel));
                      break;
                    case AttribDefObjectType.BU_:
                      if ((m = mBARegexDict[attrib.ObjectType].Match(remainder)).Success)
                      {
                        remainder = remainder.Substring(m.Index + m.Length);
                        SourceType source;
                        if (instance.Sources.TryGetValue(m.Groups[1].Value, out source))
                          source.Attributes[attribName] = new AttributeType(attrib, remainder.Trim(mBAValueDel));
                      }
                      break;
                    case AttribDefObjectType.BO_:
                      if ((m = mBARegexDict[attrib.ObjectType].Match(remainder)).Success)
                      {
                        remainder = remainder.Substring(m.Index + m.Length);
                        uint id;
                        uint.TryParse(m.Groups[1].Value, out id);
                        if (msgDict.TryGetValue(id, out msg))
                          msg.Attributes[attribName] = new AttributeType(attrib, remainder.Trim(mBAValueDel));
                      }
                      break;
                    case AttribDefObjectType.SG_:
                      if ((m = mBARegexDict[attrib.ObjectType].Match(remainder)).Success)
                      {
                        remainder = remainder.Substring(m.Index + m.Length);
                        uint id;
                        uint.TryParse(m.Groups[1].Value, out id);
                        if (msgDict.TryGetValue(id, out msg))
                        {
                          var signal = msg.getSignal(m.Groups[2].Value);
                          if (signal != null)
                            signal.Attributes[attribName] = new AttributeType(attrib, remainder.Trim(mBAValueDel));
                        }
                      }
                      break;
                    case AttribDefObjectType.EV_:
                      if ((m = mBARegexDict[attrib.ObjectType].Match(remainder)).Success)
                      {
                        remainder = remainder.Substring(m.Index + m.Length);
                        EnvironmentType ev;
                        if (instance.Environments.TryGetValue(m.Groups[1].Value, out ev))
                          ev.Attributes[attribName] = new AttributeType(attrib, remainder.Trim(mBAValueDel));
                      }
                      break;
                  }
                }
                else
                { // file error - attribute undefined
                }
              }
              else
              { // file format error
              }
              break;

            case State.BA_DEF_:     // Attribute Definition
            case State.BA_DEF_REL_: // Attribute relative Definition
              m = mBADEFRegex.Match(valStr);
              if (m.Success)
              {
                string name = m.Groups[1].Value;
                AttribDefValueType attribValueType;
                Enum.TryParse(m.Groups[2].Value, out attribValueType);
                instance.AttributeDefinitions[name] = new AttribDefType(instance, valStr, m);
              }
              else
              { // file format error
              }
              break;
            case State.BA_DEF_DEF_:
            case State.BA_DEF_DEF_REL_:
              m = mBADEFDEFRegex.Match(valStr);
              if (m.Success)
              {
                AttribDefType attribute;
                if (instance.AttributeDefinitions.TryGetValue(m.Groups[1].Value, out attribute))
                  attribute.Value = AttribDefType.fromStrToObject(attribute.DataType, m.Groups[2].Value);
                else
                { // not defined
                }
              }
              else
              { // file format error
              }
              break;

            case State.VAL_TABLE_:  // Value table
              int index = valStr.IndexOfAny(mSrcRefDel);
              var valTable = new ValueTable(valStr.Substring(0, index));
              while (valStr[valStr.Length - 1] != ';')
              {
                nextLine = sr.ReadLine();
                valStr += ' ' + nextLine.Trim(mDelimiters);
              }
              m = mValTableRegex.Match(valStr.Substring(index));
              while (m.Success)
              {
                Int64 val;
                if (Int64.TryParse(m.Groups[1].Value, mDblStyle, CultureInfo.InvariantCulture, out val))
                {
                  valTable.Enum[val] = m.Groups[2].Value;
                }
                else
                { // file format error
                }
                m = m.NextMatch();
              }
              instance.ValueTables[valTable.Name] = valTable;
              break;

            case State.SIG_GROUP_:  // Signal group
              while (valStr[valStr.Length - 1] != ';')
              {
                nextLine = sr.ReadLine();
                valStr += ' ' + nextLine.Trim(mDelimiters);
              }
              m = mSigGroupRegex.Match(valStr);
              if (m.Success)
              {
                var signalGroup = new SignalGroupType(valStr, m);
                instance.SignalGroups[signalGroup.ID] = signalGroup;
              }
              else
              { // file format error
              }
              break;

            case State.BO_: // Message
              m = mBORegex.Match(valStr);
              if (m.Success && m.Groups.Count == 5)
              {
                msg = new MsgType(m, instance);
                msgDict[msg.ID] = msg;
                msg.Source.Messages.Add(msg);
              }
              else
              { // file format error
              }
              break;

            case State.SG_: // Signal
              m = mSGRegex.Match(valStr);
              if (msg != null && m.Success && m.Groups.Count == 11)
              {
                var signal = new SignalType(valStr, m);
                msg.addSignal(signal);
              }
              else
              { // file format error
              }
              break;

            case State.SIG_VALTYPE_:  // Signal value type
              m = mSIGVALTYPERegex.Match(valStr);
              if (m.Success && m.Groups.Count == 4)
              {
                uint id;
                uint.TryParse(m.Groups[1].Value, out id);
                if (msgDict.TryGetValue(id, out msg))
                {
                  var signal = msg.getSignal(m.Groups[2].Value);
                  if (null != signal)
                    signal.FloatType = m.Groups[3].Value[0] == '1'
                      ? SignalFloatType.IEEEFloat32
                      : SignalFloatType.IEEEFloat64;
                }
              }
              else
              { // file format error
              }
              break;

            case State.EV_: // Environment
              m = mEVRegex.Match(valStr);
              if (m.Success && m.Groups.Count == 9)
              {
                var env = new EnvironmentType(instance, valStr, m);
                instance.Environments[env.Name] = env;
              }
              else
              { // file format error
              }
              break;

            case State.VAL_:  // Value
              m = mVALRegex.Match(valStr);
              if (m.Success)
              {
                uint id;
                uint.TryParse(m.Groups[1].Value, out id);
                if (msgDict.TryGetValue(id, out msg))
                {
                  var signal = msg.getSignal(m.Groups[2].Value);
                  if (null != signal)
                  {
                    signal.Enums = new EnumDict();
                    m = mValTableRegex.Match(valStr.Substring(m.Index + m.Value.Length).Trim(mDelimiters));
                    while (m.Success)
                    {
                      Int64 val;
                      if (Int64.TryParse(m.Groups[1].Value, mDblStyle, CultureInfo.InvariantCulture, out val))
                        signal.Enums[val] = m.Groups[2].Value;
                      else
                      { // file format error
                      }
                      m = m.NextMatch();
                    }
                  }
                }
              }
              else
              { // format error
              }
              break;

            case State.CM_: // Comment
              while (valStr[valStr.Length - 1] != ';')
              {
                nextLine = sr.ReadLine();
                valStr += Environment.NewLine + nextLine.Trim(mDelimiters);
              }
              if ((m = mCMSGRegex.Match(valStr)).Success)
              { // SG comment
                string remainder = valStr.Substring(m.Index + m.Length);
                uint id;
                if (uint.TryParse(m.Groups[1].Value, out id))
                {
                  if (msgDict.TryGetValue(id, out msg))
                  {
                    var signal = msg.getSignal(m.Groups[2].Value);
                    if (null != signal)
                      signal.Comment = remainder.Trim(mBAValueDel);
                  }
                }
              }
              else if ((m = mCMBORegex.Match(valStr)).Success)
              { // BO comment
                string remainder = valStr.Substring(m.Index + m.Length);
                uint id;
                if (uint.TryParse(m.Groups[1].Value, out id))
                {
                  if (msgDict.TryGetValue(id, out msg))
                    msg.Comment = remainder.Trim(mBAValueDel);
                }
              }
              else if ((m = mCMEVRegex.Match(valStr)).Success)
              { // EV comment
                string remainder = valStr.Substring(m.Index + m.Length);
                EnvironmentType env;
                if (instance.Environments.TryGetValue(m.Groups[1].Value, out env))
                  env.Comment = remainder.Trim(mBAValueDel);
              }
              else if ((m = mCMBURegex.Match(valStr)).Success)
              { // BU comment
                string remainder = valStr.Substring(m.Index + m.Length);
                SourceType source;
                if (instance.Sources.TryGetValue(m.Groups[1].Value, out source))
                  source.Comment = remainder.Trim(mBAValueDel);
              }
              else
                // global
                instance.Comment = valStr.Trim(mBAValueDel);
              break;
          }
        }
        instance.NS = ns;
      }
      return instance;
    }
    /// <summary>
    /// Saves the CanDb++ in DBC file format.
    /// </summary>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <returns>true if successful</returns>
    public bool save(string path)
    {
      using (var wr = new StreamWriter(path, false, Encoding.Default))
      { // write version
        wr.WriteLine("{0} \"{1}\"\r\n\r\n", State.VERSION, Version);
        // write namespace (NS_)
        wr.WriteLine(Resources.strDbcNs);
        // write sources (BU_)
        wr.WriteLine(Sources.toFileString());
        // write value tables (VAL_TABLE_)
        wr.WriteLine();
        wr.WriteLine(((IDBCBase)ValueTables).toFileString());

        // write messages and signals
        var enSource = Sources.GetEnumerator();
        while (enSource.MoveNext())
        {
          var source = enSource.Current.Value;
          foreach (var msg in source.Messages)
          { // write message
            wr.WriteLine();
            wr.WriteLine(((IDBCBase)msg).toFileString());

            foreach (var signal in msg.Signals)
              // write signal
              wr.WriteLine(((IDBCBase)signal).toFileString());
          }
        }

        // write environment table (EV_)
        wr.WriteLine();
        wr.WriteLine(((IDBCBase)Environments).toFileString());

        // write file comment
        if (!string.IsNullOrEmpty(Comment))
          wr.WriteLine("CM_ \"{0}\";", Comment);
        foreach (var source in Sources.Values)
        { // write source comments
          string s = ((IDBCComment)source).toFileComment();
          if (!string.IsNullOrEmpty(s))
            wr.WriteLine(s);
          foreach (var msg in source.Messages)
          { // write msg comments
            s = ((IDBCComment)msg).toFileComment();
            if (!string.IsNullOrEmpty(s))
              wr.WriteLine(s);
            foreach (var signal in msg.Signals)
            { // write signal comments
              s = ((IDBCComment)signal).toFileComment();
              if (!string.IsNullOrEmpty(s))
                wr.WriteLine(s);
            }
          }
        }

        // write attributes table (BA_DEF_)
        wr.WriteLine();
        wr.WriteLine(((IDBCBase)AttributeDefinitions).toFileString());

        // write attributes table (BA_DEF_DEF_)
        wr.WriteLine(((IDBCAttribute)AttributeDefinitions).toFileDefaultString());

        // write specific attributes
        foreach (var attribute in Attributes.Values)
          wr.WriteLine(attribute.writeToFile(null));
        foreach (var source in Sources.Values)
        { // write source specific attributes
          foreach (var attribute in source.Attributes.Values)
            wr.WriteLine(attribute.writeToFile(source));
          foreach (var msg in source.Messages)
          { // write message specific attributes
            foreach (var attribute in msg.Attributes.Values)
              wr.WriteLine(attribute.writeToFile(msg));
            foreach (var signal in msg.Signals)
            { // write signal specific attributes
              foreach (AttributeType attribute in signal.Attributes.Values)
                wr.WriteLine(attribute.writeToFile(signal));
            }
          }
        }

        foreach (var source in Sources.Values)
        { // write source comments
          foreach (var msg in source.Messages)
          { // write msg comments
            foreach (var signal in msg.Signals)
            { // write signal comments
              string s = ((IDBCSignal)signal).toValString();
              if (!string.IsNullOrEmpty(s))
                wr.WriteLine(s);
            }
          }
        }

        // write Values
        foreach (var source in Sources.Values)
        { // write source comments
          foreach (var msg in source.Messages)
          { // write msg comments
            foreach (var signal in msg.Signals)
            { // write signal comments
              string s = ((IDBCSignal)signal).toValString();
              if (!string.IsNullOrEmpty(s))
                wr.WriteLine(s);
            }
          }
        }

        // write Signal Value types
        foreach (var source in Sources.Values)
        { // write source comments
          foreach (var msg in source.Messages)
          { // write msg comments
            foreach (var signal in msg.Signals)
            { // write signal comments
              switch (signal.DataType)
              {
                case DataType.FLOAT32: wr.WriteLine("SIG_VALTYPE_ {0} {1}: 1;", msg.ID, signal.Name); break;
                case DataType.FLOAT64: wr.WriteLine("SIG_VALTYPE_ {0} {1}: 2;", msg.ID, signal.Name); break;
              }
            }
          }
        }

        // write signal groups (SIG_GROUP_)
        wr.WriteLine();
        wr.WriteLine(((IDBCBase)SignalGroups).toFileString());
      }
      mSourceFile = path;
      return true;
    }
    /// <summary>
    /// Gets the count of Sources.
    /// </summary>
    /// <returns>the count of Sources.</returns>
    public int getCountSources() { return Sources.Count; }
    /// <summary>
    /// Gets the count of Messages.
    /// </summary>
    /// <returns>the count of Messages.</returns>
    public int getCountMessages()
    {
      int i = 0;
      var en = Sources.GetEnumerator();
      while (en.MoveNext())
        i += en.Current.Value.Messages.Count;
      return i;
    }
    /// <summary>
    /// Gets the count of Signals.
    /// </summary>
    /// <returns>the count of Signals.</returns>
    public int getCountSignals()
    {
      int i = 0;
      var en = Sources.GetEnumerator();
      while (en.MoveNext())
        foreach (var msg in en.Current.Value.Messages)
          i += msg.Signals.Count;
      return i;
    }
    /// <summary>
    /// Handles received CAN frames.
    /// </summary>
    /// <param name="frame">The received CAN frame</param>
    public void onCANReceived(CANFrame frame)
    {
      if (mCANRceciverCache == null)
      { // build access cache
        mCANRceciverCache = new Dictionary<uint, MsgType>();
        foreach (var src in Sources.Values)
          foreach (var dbcMsg in src.Messages)
            mCANRceciverCache[dbcMsg.ID] = dbcMsg;
      }

      MsgType msg;
      if (!mCANRceciverCache.TryGetValue(frame.ID, out msg))
        // message not found
        return;
      msg.onCANReceived(frame);
    }
    /// <summary>
    /// Checks the content for plausibility.
    /// </summary>
    /// <returns></returns>
    public Dictionary<IDBCBase, List<string>> doPlausibilityCheck()
    {
      var result = new Dictionary<IDBCBase, List<string>>();
      List<string> currEntry = null;
      foreach (var src in Sources.Values)
      { // iterate devices

        foreach (var msg in src.Messages)
        { // Iterate messages

          bool hasMultiplexedSignals = false;

          foreach (var signal in msg.mSignals)
          { // iterate signals

            if (signal.IsMultiplexed)
              hasMultiplexedSignals = true;

            result.TryGetValue(signal, out currEntry);

            if (signal.Min > signal.Max)
            {
              if (currEntry == null)
                currEntry = new List<string>();
              currEntry.Add("Min > max");
            }

            if (currEntry != null)
              result[signal] = currEntry;
          }

          if (hasMultiplexedSignals)
          {
            if (null == msg.getMultiplexSignal())
            {
              result.TryGetValue(msg, out currEntry);
              if (currEntry == null)
              {
                currEntry = new List<string>();
                result[msg] = currEntry;
              }
              currEntry.Add("Missing multiplexor signal");
            }
          }
          else
          { // no multiplexed signals used
            SignalType mux;
            if (null != (mux = msg.getMultiplexSignal()))
            {
              result.TryGetValue(msg, out currEntry);
              if (currEntry == null)
              {
                currEntry = new List<string>();
                result[msg] = currEntry;
              }
              currEntry.Add(string.Format("Unused multiplexor signal {0}", mux.Name));
            }
          }
        }
      }
      return result;
    }
    /// <summary>
    /// Checks if the specified identifier is valid.
    /// </summary>
    /// <param name="dbcFile">The dbc file instance</param>
    /// <param name="dbcBase">A potential available IDBCBase instance (may be null)</param>
    /// <param name="identifier">The identifier to check</param>
    /// <returns>true if the specified identifier is valid.</returns>
    /// <exception cref="ApplicationException">Thrown, if the identifier is not valid</exception>
    public static bool checkIdentifier(DBCFile dbcFile, IDBCBase dbcBase, string identifier)
    {
      if (string.IsNullOrEmpty(identifier))
        return false;

      if (identifier.Length > 255)
        return false;

      Match m;
      if (!(m = mDesignatorRegex.Match(identifier)).Success || m.Groups[0].Value != identifier)
        throw new ApplicationException("Name not valid");

      foreach (var valTable in dbcFile.ValueTables.Values)
        if (valTable.Name == identifier)
          throw new ApplicationException(Resources.strDBC_NameAlreadyUsed);

      foreach (var env in dbcFile.Environments.Values)
        if (env.Name == identifier)
          throw new ApplicationException(Resources.strDBC_NameAlreadyUsed);

      foreach (var attr in dbcFile.AttributeDefinitions.Values)
        if (attr.Name == identifier)
          throw new ApplicationException(Resources.strDBC_NameAlreadyUsed);

      foreach (var source in dbcFile.Sources.Values)
      {
        if (dbcBase != source && source.Name == identifier)
          throw new ApplicationException(Resources.strDBC_NameAlreadyUsed);
        foreach (var msg in source.Messages)
        {
          if (dbcBase != msg && msg.Name == identifier)
            throw new ApplicationException(Resources.strDBC_NameAlreadyUsed);
        }
      }
      return true;
    }
    #endregion
  }
}
