﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ASAP2")]
[assembly: AssemblyDescription("ASAP2 Standard library (includes the ASAP2 types and the parser itself)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("jnsoft")]
[assembly: AssemblyProduct("ASAP2 Library")]
[assembly: AssemblyCopyright("Copyright © JNachbaur 2011-2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dcbc151d-736c-454e-a20c-bb8fe0fd02a8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("7.0.24.0")]
[assembly: AssemblyFileVersion("7.0.24.0")]

#if DEBUG
[assembly: InternalsVisibleTo("ASAP2UnitTests")]
#endif
