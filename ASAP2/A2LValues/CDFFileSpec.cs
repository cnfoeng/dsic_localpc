﻿/*!
 * @file    
 * @brief   Contains the classes mapping the Calibration Data Format (XML).
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    September 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.ComponentModel;

/// <summary>
/// Contains the classes mapping the Calibration Data Format (XML).
/// </summary>
namespace jnsoft.ASAP2.Values.CDF
{
  /// <summary>
  /// Physical axis values.
  /// </summary>
  public sealed class SWAXISCONT
  {
    public string CATEGORY;
    [XmlElement("UNIT-DISPLAY-NAME"), DefaultValue(null)]
    public string UNITDISPLAYNAME;
    [XmlElement("SW-INSTANCE-REF"), DefaultValue(null)]
    public string SWINSTANCEREF;
    [XmlArray("SW-ARRAYSIZE"), XmlArrayItem("V", typeof(int))]
    public int[] SWARRAYSIZE;
    [XmlArray("SW-VALUES-PHYS"), XmlArrayItem("V", typeof(double)), XmlArrayItem("VT", typeof(string))]
    public object[] Vs;
    public SWAXISCONT() { }
  }
  /// <summary>
  /// Holds double, text or group values.
  /// </summary>
  public sealed class VG
  {
    [DefaultValue(null)]
    public string LABEL;
    [XmlElement("V", typeof(double)), XmlElement("VT", typeof(string)), XmlElement("VG", typeof(VG))]
    public object[] Vs;
    public VG() { }
    public VG(string label, object[] vs) { LABEL = label; Vs = vs; }
  }
  /// <summary>
  /// Physical values.
  /// </summary>
  public sealed class SWVALUECONT
  {
    [XmlElement("UNIT-DISPLAY-NAME"), DefaultValue(null)]
    public string UNITDISPLAYNAME;
    [XmlArray("SW-ARRAYSIZE"), XmlArrayItem("V", typeof(int)), DefaultValue(null)]
    public int[] SWARRAYSIZE;
    [XmlArray("SW-VALUES-PHYS"), XmlArrayItem("V", typeof(double)), XmlArrayItem("VT", typeof(string)), XmlArrayItem("VG", typeof(VG))]
    public object[] Vs;
    public SWVALUECONT() { }
  }
  /// <summary>
  /// Represents one particular calibration item and therefore corresponds to CAL-ITEM in CDF.
  /// </summary>
  public sealed class SWINSTANCE
  {
    [XmlElement("SHORT-NAME")]
    public string SHORTNAME;
    [XmlElement("LONG-NAME"), DefaultValue(null)]
    public string LONGNAME;
    public string CATEGORY;
    [XmlElement("SW-FEATURE-REF"), DefaultValue(null)]
    public string SWFEATUREREF;
    [XmlElement("SW-VALUE-CONT"), DefaultValue(null)]
    public SWVALUECONT SWVALUECONT;
    [XmlArray("SW-AXIS-CONTS"), XmlArrayItem("SW-AXIS-CONT")]
    public SWAXISCONT[] SWAXISCONTS;
    public SWINSTANCE() { }
    public SWINSTANCE(string shortname, string category, SWVALUECONT valueCont)
    {
      SHORTNAME = shortname;
      CATEGORY = category;
      SWVALUECONT = valueCont;
    }
  }
  public sealed class SWCSCOLLECTION
  {
    public string CATEGORY;
    [XmlElement("SW-FEATURE-REF")]
    public string SWFEATUREREF;
    public SWCSCOLLECTION() { }
    public SWCSCOLLECTION(string category, string featureRef) { CATEGORY = category; SWFEATUREREF = featureRef; }
  }
  public sealed class SWINSTANCETREEORIGIN
  {
    [XmlElement("SYMBOLIC-FILE"), DefaultValue(null)]
    public string SYMBOLICFILE;
    [XmlElement("DATA-FILE"), DefaultValue(null)]
    public string DATAFILE;
    public SWINSTANCETREEORIGIN() { }
    public SWINSTANCETREEORIGIN(string symbolicFile, string dataFile)
    {
      SYMBOLICFILE = symbolicFile;
      DATAFILE = dataFile;
    }
  }
  /// <summary>
  /// Receives one particular set of calibration items.
  /// 
  /// Therefore it corresponds to CAL-LIST within CDF specification.
  /// </summary>
  public sealed class SWINSTANCETREE
  {
    [XmlElement("SHORT-NAME")]
    public string SHORTNAME;
    [DefaultValue(null)]
    public string CATEGORY;
    [XmlElement("SW-INSTANCE-TREE-ORIGIN"), DefaultValue(null)]
    public SWINSTANCETREEORIGIN SWINSTANCETREEORIGIN;
    [XmlArray("SW-CS-COLLECTIONS"), XmlArrayItem("SW-CS-COLLECTION"), DefaultValue(null)]
    public List<SWCSCOLLECTION> SWCSCOLLECTIONS;
    [XmlElement("SW-INSTANCE")]
    public List<SWINSTANCE> SWINSTANCEs;
    public SWINSTANCETREE() { }
    public SWINSTANCETREE(string shortName, string category, SWINSTANCETREEORIGIN origin, List<SWCSCOLLECTION> csColl, List<SWINSTANCE> instances)
    {
      SHORTNAME = shortName;
      CATEGORY = category;
      SWINSTANCETREEORIGIN = origin;
      SWCSCOLLECTIONS = csColl;
      SWINSTANCEs = instances;
    }
  }
  /// <summary>
  /// The section within a SWSYSTEM which takes all information about the instances 
  /// of data items within the ECU, in particular the calibration items.
  /// </summary>
  public struct SWINSTANCESPEC
  {
    [XmlElement("SW-INSTANCE-TREE")]
    public List<SWINSTANCETREE> SWINSTANCETREEs;
  }
  /// <summary>
  /// Describes one particular system within the current file.
  /// 
  /// This supports to transfer the calibration data of multi ecu systems within one file.
  /// </summary>
  public sealed class SWSYSTEM
  {
    [XmlElement("SHORT-NAME")]
    public string SHORTNAME;
    [XmlElement("SW-INSTANCE-SPEC")]
    public SWINSTANCESPEC SWINSTANCESPEC;
    public SWSYSTEM() { }
    public SWSYSTEM(SWINSTANCESPEC spec)
    {
      SHORTNAME = "information-not-available";
      SWINSTANCESPEC = spec;
    }
  }
  /// <summary>
  /// The root element of the file.
  /// </summary>
  [XmlInclude(typeof(SWSYSTEM))
  , XmlInclude(typeof(SWINSTANCESPEC))
  , XmlInclude(typeof(SWINSTANCETREE))
  , XmlInclude(typeof(SWINSTANCETREEORIGIN))
  , XmlInclude(typeof(SWCSCOLLECTION))
  , XmlInclude(typeof(SWINSTANCE))
  , XmlInclude(typeof(SWVALUECONT))
  , XmlInclude(typeof(VG))
  , XmlInclude(typeof(SWAXISCONT))
  ]
  public sealed class MSRSW
  {
    [XmlAttribute]
    public string CREATOR;
    [XmlAttribute("CREATOR-VERSION")]
    public string CREATORVERSION;
    [XmlElement("SHORT-NAME")]
    public string SHORTNAME;
    [DefaultValue(null)]
    public string INTRODUCTION;
    [DefaultValue(null)]
    public string CATEGORY;
    [XmlArray("SW-SYSTEMS"), XmlArrayItem("SW-SYSTEM"), DefaultValue(null)]
    public List<SWSYSTEM> SWSYSTEMS;
    public MSRSW() { }
    internal MSRSW(string shortname, string creator, string creatorVersion, string introduction)
    {
      CATEGORY = "CDF";
      SHORTNAME = shortname;
      CREATOR = creator;
      CREATORVERSION = creatorVersion;
      if (!string.IsNullOrEmpty(introduction))
        INTRODUCTION = introduction;
    }
    /// <summary>
    /// Gets all available SWINSTANCE instances from the MSRSW.
    /// </summary>
    /// <returns></returns>
    internal SWINSTANCE[] getInstances()
    {
      var result = new List<SWINSTANCE>();
      if (SWSYSTEMS != null)
        foreach (var system in SWSYSTEMS)
        {
          if (system.SWINSTANCESPEC.SWINSTANCETREEs != null)
            foreach (var tree in system.SWINSTANCESPEC.SWINSTANCETREEs)
            {
              if (tree.SWINSTANCEs != null)
                result.AddRange(tree.SWINSTANCEs);
            }
        }
      return result.ToArray();
    }
  }
}