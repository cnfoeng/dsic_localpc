﻿/*!
 * @file    
 * @brief   Implements memory access for measurement values.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    August 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Collections.Generic;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Manages static memory data for A2LMEASUREMENTs.
  /// </summary>
  public sealed class MeasurementAccessData
  {
    #region members
    MemoryRangeList mMemoryRanges = new MemoryRangeList();
    List<byte[]> mDataChunks;
    #endregion
    #region constructors
    /// <summary>
    /// Cretae a new instance of MeasurementAccesData with the specified parameters.
    /// </summary>
    /// <param name="measurements">The list of measurements to create the memory for</param>
    public MeasurementAccessData(List<A2LMEASUREMENT> measurements)
    {
      // create the memory ranges
      measurements.Sort(A2LNODE.sortByAddress);
      foreach (A2LMEASUREMENT measurement in measurements)
        mMemoryRanges.add(measurement.Address, measurement.getMemorySize());

      // sort the ranges
      mMemoryRanges.ensureSorted();

      // create the corresponding memory
      mDataChunks = new List<byte[]>();
      foreach (MemoryRange range in mMemoryRanges)
        mDataChunks.Add(new byte[range.Next - range.Start]);
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the created memory ranges.
    /// 
    /// Do not modify, the memory ranges are created by the constructor.
    /// </summary>
    public MemoryRangeList MemoryRanges { get { return mMemoryRanges; } }
    /// <summary>
    /// Gets the data chunks.
    /// 
    /// Do not modify, the data chunks are created by the constructor.
    /// </summary>
    public List<byte[]> DataChunks { get { return mDataChunks; } }
    #endregion
    #region methods
    /// <summary>
    /// Gets the data for the specified address and size.
    /// </summary>
    /// <param name="address">The address to get the data fo</param>
    /// <param name="size">The size of the data to get</param>
    /// <returns>byte array instance, or null if not found</returns>
    public byte[] getData(UInt32 address, int size)
    {
      int index = mMemoryRanges.findRangeIndex(address, size);
      if (index < 0)
        return null;
      var r = mMemoryRanges[index];
      if (r.Next < address + size)
        return null;
      var result = new byte[size];
      Array.Copy(mDataChunks[index], address - r.Start, result, 0, size);
      return result;
    }
    /// <summary>
    /// Gets the data and offset for the specified address and size.
    /// </summary>
    /// <param name="address">The address to get the data fo</param>
    /// <param name="size">The size of the data to get</param>
    /// <param name="data">The resulting data</param>
    /// <param name="offset">The resuling offset</param>
    /// <returns>byte array instance, or null if not found</returns>
    public bool getData(UInt32 address, int size, out byte[] data, out int offset)
    {
      data = null;
      offset = 0;
      int index = mMemoryRanges.findRangeIndex(address, size);
      if (index < 0)
        return false;
      var r = mMemoryRanges[index];
      if (r.Next < address + size)
        return false;
      data = mDataChunks[index];
      offset = (int)(address - r.Start);
      return true;
    }
    /// <summary>
    /// Sets the data to the specified address.
    /// </summary>
    /// <param name="address">The address to get the data fo</param>
    /// <param name="data">The data to set</param>
    /// <returns>true if successful</returns>
    public bool setData(UInt32 address, byte[] data)
    {
      int index = mMemoryRanges.findRangeIndex(address, data.Length);
      if (index < 0)
        return false;
      var r = mMemoryRanges[index];
      if (r.Next < address + data.Length)
        return false;
      Array.Copy(data, 0, mDataChunks[index], address - r.Start, data.Length);
      return true;
    }
    /// <summary>
    /// Gets the last received raw value for the specified measurement address.
    /// </summary>
    /// <param name="measurement">The measurement to get the raw value for</param>
    /// <param name="rawValue">[out] the raw value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getRawValue(A2LMEASUREMENT measurement, out double rawValue)
    {
      return getRawValue(measurement, 0, 0, out rawValue);
    }
    /// <summary>
    /// Gets the last received raw value for the specified measurement address.
    /// 
    /// This is the version for an A2LMEASUREMENT with ARRAY_SIZE > 1 or a MATRIX_DIM specification.
    /// </summary>
    /// <param name="measurement">The measurement to get the raw value for</param>
    /// <param name="x">The column</param>
    /// <param name="y">The row</param>
    /// <param name="rawValue">[out] the raw value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getRawValue(A2LMEASUREMENT measurement, int x, int y, out double rawValue)
    {
      if ((x > 0 || y > 0) && measurement.MatrixDim == null)
        // wrong call to a array which is not defined as an array
        throw new ApplicationException();

      int lengthInByte = measurement.DataType.getSizeInByte();
      uint address = measurement.Address + measurement.getAddressOffset(x, y);

      rawValue = double.NaN;
      byte[] data; int offset;
      if ( !getData(address, lengthInByte, out data, out offset))
        return false;

      rawValue = CharacteristicValue.getSingleRawValue(data, offset
        , measurement.DataType
        , measurement.ByteOrder
        , measurement.BitMask
        );

      return true;
    }
    /// <summary>
    /// Gets the last received physical value for the specified measurement address.
    /// </summary>
    /// <param name="measurement">The measurement to get the physical value for</param>
    /// <param name="value">[out] the value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getPhysValue(A2LMEASUREMENT measurement, out double value)
    {
      return getPhysValue(measurement, 0, 0, out value);
    }
    /// <summary>
    /// Gets the last received physical value for the specified measurement address.
    /// 
    /// This is the version for an A2LMEASUREMENT with ARRAY_SIZE > 1 or a MATRIX_DIM specification.
    /// </summary>
    /// <param name="measurement">The measurement to get the physical value for</param>
    /// <param name="x">The column</param>
    /// <param name="y">The row</param>
    /// <param name="value">[out] the value (double.NaN if not measured)</param>
    /// <returns>true if the value is measured, else false</returns>
    public bool getPhysValue(A2LMEASUREMENT measurement, int x, int y, out double value)
    {
      if (!getRawValue(measurement, x, y, out value))
        return false;
      value = measurement.RefCompuMethod.toPhysical(value);
      return true;
    }
    /// <summary>
    /// Sets the specified value to the measurement's memory.
    /// 
    /// This method is mainly used by the ECU simulator.
    /// </summary>
    /// <param name="measurement">The measurement</param>
    /// <param name="x">The column</param>
    /// <param name="y">The row</param>
    /// <param name="value">The physical value</param>
    /// <returns>true if the successful</returns>
    public bool setPhysValue(A2LMEASUREMENT measurement, int x, int y, double value)
    {
      bool turnAround = BitConverter.IsLittleEndian && measurement.ByteOrder == BYTEORDER_TYPE.BIG_ENDIAN;
      value = measurement.RefCompuMethod.toRaw(measurement.DataType, value);
      var buffer = CharacteristicValue.getSingleRawValueBuffer(value, measurement.DataType, measurement.ByteOrder);
      return setData(measurement.Address + measurement.getAddressOffset(x, y), buffer);
    }
    #endregion
  }
}