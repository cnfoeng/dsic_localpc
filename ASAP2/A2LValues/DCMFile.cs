﻿/*!
 * @file    
 * @brief   Implements the DCM file parser.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    August 2012
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Base class for any import and export calibration data file formats
  /// </summary>
  public abstract class DataConservation
  {
    protected static readonly string mEOL = Environment.NewLine;
    public static readonly char[] mDelimiters = new char[] { ' ', '\t' };
    protected static readonly char[] mLineDelimiter = new char[] { '\n', '\r' };
    protected List<ICharValue> mValues = new List<ICharValue>();
    /// <summary>
    /// Gets the dictionary containing succesfully imported values from the DCM file.
    /// </summary>
    public List<ICharValue> Values { get { return mValues; } }
  }
  /// <summary>
  /// Implements the ETAS INCA DCM file format 2.x reader/writer.
  /// 
  /// The DCM file format is a Data Conservation format,
  /// a DAMOS format used in ASCET and INCA.
  /// </summary>
  public sealed class DCMFile : DataConservation
  {
    static ILog mLogger = LogManager.GetLogger(typeof(DCMFile));
    static readonly char[] mDCMCommentedLine = new char[] { '*', '!', '.' };
    static Regex mParMatch = new Regex("(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|(?:[^\\s\t]+)", RegexOptions.None);
    /// <summary>
    /// Saves all characteristics of the module into a DCM file.
    /// </summary>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var layoutRef in module.enumChildNodes<A2LRECORD_LAYOUT_REF>())
      {
        ICharValue value = CharacteristicValue.createValue(dataFile, layoutRef, ValueObjectFormat.Physical);
        if (value == null
          || value.CharType >= CHAR_TYPE.CUBOID // undefined in DCM file format
          )
          continue;
        charValues.Add(value);
      }
      var dict = ((A2LPROJECT)module.Parent).FuncDict;
      var functions = new A2LFUNCTION[dict.Count];
      dict.Values.CopyTo(functions, 0);
      return save(path, description, charValues.ToArray(), functions);
    }
    /// <summary>
    /// Saves the specified characteristics into a DCM file.
    /// </summary>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="characteristics">The characteristics to save</param>
    /// <param name="exportFunctions">true if functions should be exported, too</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      , A2LCHARACTERISTIC[] characteristics
      , bool exportFunctions
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var characteristic in characteristics)
      {
        ICharValue value = null;
        switch (characteristic.CharType)
        {
          case CHAR_TYPE.VALUE: value = CharacteristicValue.getValue<SingleValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
          case CHAR_TYPE.ASCII: value = CharacteristicValue.getValue<ASCIIValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
          case CHAR_TYPE.VAL_BLK: value = CharacteristicValue.getValue<ValBlkValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
          case CHAR_TYPE.CURVE: value = CharacteristicValue.getValue<CurveValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
          case CHAR_TYPE.MAP: value = CharacteristicValue.getValue<MapValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
        }
        if (value == null)
          continue;
        charValues.Add(value);
      }
      A2LFUNCTION[] functions = null;
      if (exportFunctions)
      {
        var dict = ((A2LPROJECT)module.Parent).FuncDict;
        functions = new A2LFUNCTION[dict.Count];
        dict.Values.CopyTo(functions, 0);
      }

      var modPar = module.getNode<A2LMODPAR>(false);
      description = string.IsNullOrEmpty(description)
        ? string.Format("EPK: {0}", modPar.EPK)
        : string.Format("EPK: {0}{1}{1}{2}", modPar.EPK, mEOL, description);
      return save(path, description, charValues.ToArray(), functions);
    }
    /// <summary>
    /// Saves the specified characteristics into a DCM file.
    /// </summary>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="values">The values to save into the DCM file</param>
    /// <param name="functions">Functions to save into the DCM file (may be null or empty)</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(string path
      , string description
      , ICharValue[] values
      , A2LFUNCTION[] functions
      )
    {
      try
      {
        using (var sw = new StreamWriter(path, false, Encoding.Default))
        {
          int counter = 0;
          sw.WriteLine(Resources.strDCMEncoding, Encoding.Default.BodyName);
          sw.WriteLine(Resources.strDCMFormat);
          sw.WriteLine(string.Format(Resources.strDCMCreatedBy
            , Application.ProductName
            , Application.ProductVersion
            ));
          var now = DateTime.Now;
          sw.WriteLine(string.Format(Resources.strDCMCreatedAt
            , now.ToShortDateString()
            , now.ToLongTimeString()
            ));
          if (!string.IsNullOrEmpty(description))
          {
            var splits = description.Split(new string[] { mEOL }, StringSplitOptions.None);
            for (int i = 0; i < splits.Length; ++i)
              sw.WriteLine(string.Format("* {0}", splits[i].Trim(' ')));
          }
          sw.WriteLine();
          sw.WriteLine(Resources.strDCMVersion);
          sw.WriteLine();

          // Write the functions
          if (functions != null && functions.Length > 0)
          { // functions defined
            sw.WriteLine(Resources.strDCMFunctions);
            foreach (var function in functions)
              sw.WriteLine(Resources.strDCMFunction, function.Name, function.Version, function.Description);
            sw.WriteLine(Resources.strDCMSingleEnd, mEOL);
          }

          // Write the values
          foreach (ICharValue value in values)
          {
            var recLayoutRef = value.Characteristic;
            var defFunction = recLayoutRef.getDefCharacteristicFunction();
            var characteristic = recLayoutRef as A2LCHARACTERISTIC;
            if (null != characteristic)
            {
              switch (characteristic.CharType)
              {
                case CHAR_TYPE.VALUE:
                  if (value.Value is double && double.IsNaN((double)value.Value))
                    // filter out NaN values
                    continue;
                  sw.WriteLine(Resources.strDCMFestwert, characteristic.Name);
                  sw.WriteLine(Resources.strDCMLangName, characteristic.Description);
                  if (defFunction != null)
                    sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
                  sw.WriteLine(Resources.strDCMUnitW, value.Unit);
                  bool isString = characteristic.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  sw.Write(isString ? Resources.strDCMText : Resources.strDCMValue);
                  if (isString)
                    sw.WriteLine("\"{0}\"", value.toSingleValue());
                  else
                    sw.WriteLine(value.toSingleValue());
                  sw.WriteLine(Resources.strDCMSingleEnd, mEOL);
                  break;
                case CHAR_TYPE.ASCII:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;
                  sw.WriteLine(Resources.strDCMAscii, characteristic.Name);
                  sw.WriteLine(Resources.strDCMLangName, characteristic.Description);
                  if (defFunction != null)
                    sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
                  sw.WriteLine("{0}\"{1}\"", Resources.strDCMText, value.Value);
                  sw.WriteLine(Resources.strDCMSingleEnd, mEOL);
                  break;
                case CHAR_TYPE.VAL_BLK:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;
                  var number = characteristic.Number;
                  sw.WriteLine(Resources.strDCMValBlk, characteristic.Name, number);
                  sw.WriteLine(Resources.strDCMLangName, characteristic.Description);
                  if (defFunction != null)
                    sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
                  sw.WriteLine(Resources.strDCMUnitW, value.Unit);

                  isString = characteristic.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  sw.Write(isString ? Resources.strDCMText : Resources.strDCMValue);
                  for (int i = 0; i < number; ++i)
                    sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, 0, 0));
                  sw.WriteLine(Resources.strDCMDualEnd, mEOL);
                  break;
                case CHAR_TYPE.CURVE:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;
                  var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  bool isGroup = axisDescs[0].RefAxisPtsRef != null;
                  isString = axisDescs[0].RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;

                  sw.WriteLine("{0} {1} {2}", isGroup ? Resources.strDCMGroupCurve : Resources.strDCMCurve
                    , characteristic.Name, value.AxisValue[0].Length
                    );
                  sw.WriteLine(Resources.strDCMLangName, characteristic.Description);
                  if (defFunction != null)
                    sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
                  sw.WriteLine(Resources.strDCMUnitX, value.UnitAxis[0]);
                  sw.WriteLine(Resources.strDCMUnitW, value.Unit);
                  if (isGroup)
                    sw.WriteLine("*SSTX {0}", axisDescs[0].RefAxisPtsRef.Name);
                  sw.Write(isString ? "ST_TX/X " : "ST/X ");
                  for (int i = 0; i < value.AxisValue[0].Length; ++i)
                    sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, -1, 0));
                  sw.WriteLine();
                  isString = characteristic.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  sw.Write(isString ? Resources.strDCMText : Resources.strDCMValue);
                  for (int i = 0; i < value.AxisValue[0].Length; ++i)
                    sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, 0, 0));
                  sw.WriteLine(Resources.strDCMDualEnd, mEOL);
                  break;
                case CHAR_TYPE.MAP:
                  if (value.Value is double[,] && double.IsNaN(((double[,])value.Value)[0, 0]))
                    // filter out NaN values
                    continue;
                  axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  bool isGroupX = axisDescs[0].RefAxisPtsRef != null;
                  bool isGroupY = axisDescs[1].RefAxisPtsRef != null;
                  sw.WriteLine("{0} {1} {2} {3}", isGroupX || isGroupY ? Resources.strDCMGroupMap : Resources.strDCMMap
                    , characteristic.Name, value.AxisValue[0].Length, value.AxisValue[1].Length
                    );
                  sw.WriteLine(Resources.strDCMLangName, characteristic.Description);
                  if (defFunction != null)
                    sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
                  sw.WriteLine(Resources.strDCMUnitX, value.UnitAxis[0]);
                  sw.WriteLine(Resources.strDCMUnitY, value.UnitAxis[1]);
                  sw.WriteLine(Resources.strDCMUnitW, value.Unit);
                  if (isGroupX)
                    sw.WriteLine("*SSTX {0}", axisDescs[0].RefAxisPtsRef.Name);
                  if (isGroupY)
                    sw.WriteLine("*SSTY {0}", axisDescs[1].RefAxisPtsRef.Name);
                  isString = axisDescs[0].RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  sw.Write(isString ? "ST_TX/X " : "ST/X ");
                  for (int i = 0; i < value.AxisValue[0].Length; ++i)
                    sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, -1, 0));
                  isString = characteristic.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  bool isYAxisString = axisDescs[1].RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  for (int j = 0; j < value.AxisValue[1].Length; ++j)
                  {
                    sw.WriteLine();
                    sw.WriteLine(isYAxisString ? "ST_TX/Y \"{0}\"" : "ST/Y {0}"
                      , value.toSingleValue(-1, j, 0)
                      );
                    sw.Write(isString ? Resources.strDCMText : Resources.strDCMValue);
                    for (int i = 0; i < value.AxisValue[0].Length; ++i)
                      sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, j, 0));
                  }
                  sw.WriteLine(Resources.strDCMDualEnd, mEOL);
                  break;
                default:
                  throw new NotSupportedException(characteristic.CharType.ToString());
              }
            }
            else
            { // A2LAxisPts
              if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                // filter out NaN values
                continue;
              var number = ((A2LAXIS_PTS)recLayoutRef).MaxAxisPoints;
              sw.WriteLine(Resources.strDCMAxisPts, recLayoutRef.Name, number);
              sw.WriteLine(Resources.strDCMLangName, recLayoutRef.Description);
              if (defFunction != null)
                sw.WriteLine(Resources.strDCMDefFunction, defFunction.Name);
              sw.WriteLine(Resources.strDCMUnitW, value.Unit);

              bool isString = recLayoutRef.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
              sw.Write(isString ? Resources.strDCMText : Resources.strDCMValue);
              for (int i = 0; i < number; ++i)
                sw.Write(isString ? "\"{0}\" " : "{0} ", value.toSingleValue(i, 0, 0));
              sw.WriteLine(Resources.strDCMDualEnd, mEOL);
            }
            ++counter;
          }
          return counter;
        }
      }
      catch { return -1; }
    }
    /// <summary>
    /// Opens a DCM file and returns a DCMFile instance.
    /// </summary>
    /// <param name="path">the fully qualified dcm/par filename</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <returns>A new DCMFile instance or null if failed</returns>
    public static DataConservation open(string path, A2LMODULE module)
    {
      var instance = new DCMFile();
      var project = module.getParent<A2LPROJECT>();
      var charDict = project.CharDict;
      using (var sr = new StreamReader(path, Encoding.Default))
      {
        bool startFound = false;
        ICharValue charValue = null;
        A2LRECORD_LAYOUT_REF characteristic = null;
        List<A2LAXIS_DESCR> axisDescs = null;
        A2LCOMPU_VTAB verbalTab = null, verbalTabX = null, verbalTabY = null;
        int currXAxisIndex = 0, currYAxisIndex = 0, currColIndex = 0, currRowIndex = 0, lineCount = 0;

        string line;
        while (null != (line = sr.ReadLine()))
        {
          ++lineCount;

          line = line.Trim(mDelimiters);
          if (string.IsNullOrEmpty(line))
            // line is empty
            continue;

          if (0 == line.IndexOfAny(mDCMCommentedLine))
            // line is a comment
            continue;

          try
          {
            var parameters = split2DCMParameter(line);
            string dcmTypeStr = parameters[0];
            string name = parameters.Length > 1 ? parameters[1] : null;
            switch (dcmTypeStr)
            {
              case "KONSERVIERUNG_FORMAT":
                int version;
                if (int.TryParse(name.Split('.')[0], out version) && version >= 2)
                  startFound = true;
                break;

              case "FESTWERT":
                // single value
                if (!startFound || !charDict.TryGetValue(name, out characteristic) || !checkType(characteristic, dcmTypeStr))
                  continue;
                charValue = new SingleValue(characteristic);
                verbalTab = characteristic.RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                break;

              case "TEXTSTRING":
                // ASCII value
                if (!startFound || !charDict.TryGetValue(name, out characteristic) || !checkType(characteristic, dcmTypeStr))
                  continue;
                charValue = new ASCIIValue(characteristic);
                break;

              case "STUETZSTELLENVERTEILUNG":
              case "FESTWERTEBLOCK":
                // ValBlk or AxisPts
                if (!startFound || !charDict.TryGetValue(name, out characteristic) || !checkType(characteristic, dcmTypeStr))
                  continue;
                var localSize = characteristic is A2LAXIS_PTS
                  ? ((A2LAXIS_PTS)characteristic).MaxAxisPoints
                  : ((A2LCHARACTERISTIC)characteristic).Number;
                int sizeX = A2LParser.parse2IntVal(parameters[2]);
                if (sizeX != localSize)
                  continue;
                charValue = new ValBlkValue(characteristic);
                verbalTab = characteristic.RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                break;

              case "GRUPPENKENNLINIE":
              case "FESTKENNLINIE":
              case "KENNLINIE":
                // Curve
                if (!startFound || !charDict.TryGetValue(name, out characteristic) || !checkType(characteristic, dcmTypeStr))
                  continue;
                axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                sizeX = A2LParser.parse2IntVal(parameters[2]);
                if (axisDescs[0].MaxAxisPoints != sizeX)
                  continue;
                charValue = new CurveValue(characteristic);
                verbalTab = characteristic.RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                verbalTabX = axisDescs[0].RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                break;

              case "GRUPPENKENNFELD":
              case "KENNFELD":
              case "FESTKENNFELD":
                // Map
                if (!startFound || !charDict.TryGetValue(name, out characteristic) || !checkType(characteristic, dcmTypeStr))
                  continue;
                axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                sizeX = A2LParser.parse2IntVal(parameters[2]);
                int sizeY = A2LParser.parse2IntVal(parameters[3]);
                if (axisDescs[0].MaxAxisPoints != sizeX || axisDescs[1].MaxAxisPoints != sizeY)
                  continue;
                charValue = new MapValue(characteristic);
                verbalTab = characteristic.RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                verbalTabX = axisDescs[0].RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                verbalTabY = axisDescs[1].RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                break;

              case "WERT":
              case "TEXT":
                if (charValue == null)
                  continue;
                switch (charValue.CharType)
                {
                  case CHAR_TYPE.VALUE:
                    charValue.Value = getValue(parameters[1], verbalTab);
                    break;
                  case CHAR_TYPE.ASCII:
                    charValue.Value = parameters[1];
                    break;
                  case CHAR_TYPE.VAL_BLK:
                    var curveValues = charValue.Value as double[];
                    if (curveValues == null)
                    { // create the valBlk if not already done
                      localSize = characteristic is A2LAXIS_PTS
                        ? ((A2LAXIS_PTS)characteristic).MaxAxisPoints
                        : ((A2LCHARACTERISTIC)characteristic).Number;
                      charValue.Value = curveValues = new double[localSize];
                    }
                    for (int i = 1; i < parameters.Length; ++i)
                      curveValues[currColIndex++] = getValue(parameters[i], verbalTab);
                    break;
                  case CHAR_TYPE.CURVE:
                    curveValues = charValue.Value as double[];
                    if (curveValues == null)
                      // create the curve if not already done
                      charValue.Value = curveValues = new double[axisDescs[0].MaxAxisPoints];
                    for (int i = 1; i < parameters.Length; ++i)
                      curveValues[currColIndex++] = getValue(parameters[i], verbalTab);
                    break;
                  case CHAR_TYPE.MAP:
                    var mapValues = charValue.Value as double[,];
                    if (mapValues == null)
                      // create the map if not already done
                      charValue.Value = mapValues = new double[axisDescs[0].MaxAxisPoints, axisDescs[1].MaxAxisPoints];
                    for (int i = 1; i < parameters.Length; ++i)
                      mapValues[currColIndex++, currRowIndex] = getValue(parameters[i], verbalTab);
                    if (currColIndex == mapValues.GetLength(0))
                    { // row complete, next row
                      currColIndex = 0;
                      ++currRowIndex;
                    }
                    break;
                }
                break;

              case "ST/X":
              case "ST_TX/X":
                if (charValue == null)
                  continue;
                switch (charValue.CharType)
                {
                  case CHAR_TYPE.VAL_BLK:
                    var axisPtsValues = charValue.Value as double[];
                    if (axisPtsValues == null)
                      charValue.Value = axisPtsValues = new double[((A2LAXIS_PTS)characteristic).MaxAxisPoints];
                    for (int i = 1; i < parameters.Length; ++i)
                      axisPtsValues[currXAxisIndex++] = getValue(parameters[i], verbalTabX);
                    break;
                  case CHAR_TYPE.CURVE:
                    var axisValues = charValue.AxisValue as double[][];
                    if (axisValues == null)
                    {
                      charValue.AxisValue = axisValues = new double[1][];
                      axisValues[0] = new double[axisDescs[0].MaxAxisPoints];
                    }
                    for (int i = 1; i < parameters.Length; ++i)
                      axisValues[0][currXAxisIndex++] = getValue(parameters[i], verbalTabX);
                    break;
                  case CHAR_TYPE.MAP:
                    axisValues = charValue.AxisValue as double[][];
                    if (axisValues == null)
                    {
                      charValue.AxisValue = axisValues = new double[2][];
                      axisValues[0] = new double[axisDescs[0].MaxAxisPoints];
                      axisValues[1] = new double[axisDescs[1].MaxAxisPoints];
                    }
                    for (int i = 1; i < parameters.Length; ++i)
                      axisValues[0][currXAxisIndex++] = getValue(parameters[i], verbalTabX);
                    break;
                }
                break;

              case "ST/Y":
              case "ST_TX/Y":
                if (charValue == null)
                  continue;
                switch (charValue.CharType)
                {
                  case CHAR_TYPE.MAP:
                    var axisValues = charValue.AxisValue as double[][];
                    if (axisValues == null)
                    {
                      charValue.AxisValue = axisValues = new double[2][];
                      axisValues[0] = new double[axisDescs[0].MaxAxisPoints];
                      axisValues[1] = new double[axisDescs[1].MaxAxisPoints];
                    }
                    for (int i = 1; i < parameters.Length; ++i)
                      axisValues[1][currYAxisIndex++] = getValue(parameters[i], verbalTabY);
                    break;
                }
                break;
              case "LANGNAME":
              case "DISPLAYNAME":
              case "FUNKTIONEN":
              case "FUNKTION":
              case "FKT":
                // not interesting, as this is defined by the destination A2L
                break;
              case "EINHEIT_W":
                if (charValue == null)
                  continue;
                charValue.Unit = parameters[1]; break;
              case "EINHEIT_X":
                if (charValue == null)
                  continue;
                if (charValue.UnitAxis == null)
                  charValue.UnitAxis = new string[charValue.CharType == CHAR_TYPE.MAP ? 2 : 1];
                charValue.UnitAxis[0] = parameters[1]; break;
              case "EINHEIT_Y":
                if (charValue == null)
                  continue;
                if (charValue.UnitAxis == null)
                  charValue.UnitAxis = new string[charValue.CharType == CHAR_TYPE.MAP ? 2 : 1];
                charValue.UnitAxis[1] = parameters[1]; break;
              case "END":
                // the end is reached
                if (charValue != null)
                  // add characteristic
                  instance.mValues.Add(charValue);

                // reset temp values
                charValue = null;
                characteristic = null;
                axisDescs = null;
                verbalTab = null; verbalTabX = null; verbalTabY = null;
                currXAxisIndex = currYAxisIndex = currColIndex = currRowIndex = 0;
                break;
            }
          }
          catch (Exception ex)
          {
            mLogger.Error(string.Format("caught parser exception in {0}({1})"
              , Path.GetFileName(path), lineCount)
              , ex
              );
          }
        }
      }
      return instance;
    }
    /// <summary>
    /// Checks the specified DCM type string against the corresponding characteristic type.
    /// </summary>
    /// <param name="characteristic">The characteristic to check</param>
    /// <param name="dcmType">The DCM type string</param>
    /// <returns>true if the types are matching</returns>
    static bool checkType(A2LRECORD_LAYOUT_REF characteristic, string dcmType)
    {
      switch (dcmType)
      {
        case "FESTWERT":
          return characteristic is A2LCHARACTERISTIC && ((A2LCHARACTERISTIC)characteristic).CharType == CHAR_TYPE.VALUE;
        case "TEXTSTRING":
          return characteristic is A2LCHARACTERISTIC && ((A2LCHARACTERISTIC)characteristic).CharType == CHAR_TYPE.ASCII;
        case "STUETZSTELLENVERTEILUNG": return characteristic is A2LAXIS_PTS;
        case "FESTWERTEBLOCK": return characteristic is A2LCHARACTERISTIC && ((A2LCHARACTERISTIC)characteristic).CharType == CHAR_TYPE.VAL_BLK;
        case "GRUPPENKENNLINIE":
        case "FESTKENNLINIE":
        case "KENNLINIE":
          return characteristic is A2LCHARACTERISTIC && ((A2LCHARACTERISTIC)characteristic).CharType == CHAR_TYPE.CURVE;
        case "GRUPPENKENNFELD":
        case "KENNFELD":
        case "FESTKENNFELD": return characteristic is A2LCHARACTERISTIC && ((A2LCHARACTERISTIC)characteristic).CharType == CHAR_TYPE.MAP;
      }
      return false;
    }
    /// <summary>
    /// Splits the specified DCM string into a parameter list.
    /// 
    /// Splits by blanks and tabs, preserves contained strings containing blanks.
    /// </summary>
    /// <param name="line">The line to solit to</param>
    /// <returns>The list of parameters</returns>
    static string[] split2DCMParameter(string line)
    {
      var result = new List<string>();
      var matches = mParMatch.Matches(line);
      int count = matches.Count;
      for (int j = 0; j < count; ++j)
      {
        var m = matches[j];
        result.Add(m.Value.Trim('"'));
      }
      return result.ToArray();
    }
    /// <summary>
    /// Gets the double value from the specified string parameter.
    /// 
    /// In case a verbal TabRef is given, the value is converted to
    /// the corresponding verbal raw value. 
    /// </summary>
    /// <param name="parameter">The DCM value parameter as a string</param>
    /// <param name="verbalTabRef">The verbal computation method (may be null)</param>
    /// <returns>The physical value as a double</returns>
    static double getValue(string parameter, A2LCOMPU_VTAB verbalTabRef)
    {
      return verbalTabRef != null
        ? verbalTabRef.toRaw(parameter)
        : A2LParser.parse2DoubleVal(parameter);
    }
  }
}
