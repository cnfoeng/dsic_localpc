/*!
 * @file    
 * @brief   Base class for reading and writing S19 and HEX files.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Base class for reading/writing S19 and HEX files.
  /// 
  /// - This class is constructed by the static DataFile.open method.
  /// - Use DataFile.open to load S19 or HEX files.
  /// - use DataFile.getNearestDataFile to find potential fitting files to a specified A2L file.
  /// 
  /// To load a data file into an already loaded A2L model:
  /// - Open the data file by DataFile.open
  /// - Get values for your characteristics by calling CharacteristicValue.getValue<T>
  /// with any of your characteristics from the A2L model
  /// </summary>
  public abstract class DataFile : ICloneable
  {
    #region local types
    //protected internal struct DataPart
    protected internal sealed class DataPart
    {
      internal UInt32 Address;
      internal byte[] Data;
      internal DataPart(UInt32 address, ref byte[] data)
      {
        Address = address;
        Data = data;
      }
    }
    #endregion
    #region members
    protected static readonly char[] mLineTrim = new char[] { '\t', ' ' };
    protected static readonly string mStrByteFormat = "{0:X2}";
    protected MemorySegmentList mSegments;
    protected object mTag;
    #endregion
    #region constructors
    /// <summary>
    /// Constructs a new instance of this class.
    /// </summary>
    /// <param name="filename">The data file source filename</param>
    /// <param name="segments">Segments to initialize with, may be null or empty.</param>
    protected DataFile(string filename, MemorySegmentList segments)
    {
      SourceFilename = filename;
      mSegments = segments != null
        ? (MemorySegmentList)segments.Clone()
        : new MemorySegmentList();
    }
    #endregion
    #region properties
    protected string SourceFilename { get; set; }
    protected bool IsDirty { get; set; }
    protected HashSet<A2LRECORD_LAYOUT_REF> ChangedValues { get; } = new HashSet<A2LRECORD_LAYOUT_REF>();
    /// <summary>
    /// Gets ors sets the Tag property.
    /// 
    /// This property may be used to bind any user reference to a datafile.
    /// </summary>
    public object Tag { get; set; }
    #endregion
    #region  methods
    protected internal static int compare(DataPart x, DataPart y)
    {
      return x.Address.CompareTo(y.Address);
    }
    /// <summary>
    /// Add the changed characteristic name to the list of changed characteristics
    /// </summary>
    /// <param name="charName"></param>
    internal void changedValue(A2LRECORD_LAYOUT_REF characteristic)
    {
      IsDirty = true;
      ChangedValues.Add(characteristic);
    }
    /// <summary>
    /// Opens the specified datafile (may be a Motorola S or Intel HEX file).
    /// 
    /// The data read will be written into the specified MemorySegmentList.
    /// </summary>
    /// <example>
    /// <para>Example code:</para>
    /// <code>
    /// // Create the list of memory segments from the A2L model...
    /// var memSegs = createEmptyMemorySegments();
    /// // Load the data file into the memory segments...
    /// var dataFile = DataFile.open(dataFilename, memSegs);
    /// </code>
    /// </example>
    /// <param name="filename">the fully qualified path to the file to open</param>
    /// <param name="segments">A list of memory segments created from the A2L source file (usually all data segmensts), may be null or empty.</param>
    /// <returns>A new instance of a data file or null if the file is not accessible, not valid</returns>
    public static IDataFile open(string filename, MemorySegmentList segments)
    {
      string extension = Path.GetExtension(filename).ToLower();
      var datafile = extension == ".bin"
        ? (IDataFile)new DataFileBin(filename, segments)
        : Helpers.Extensions.isSFileExtension(filename)
          ? (IDataFile)new DataFileS19(filename, segments)
          : (IDataFile)new DataFileHex(filename, segments);
      return ((DataFile)datafile).load(filename)
        ? datafile
        : null;
    }
    /// <summary>
    /// Gets the nearest data file (S19 or HEX) to the specified a2l file.
    /// 
    /// Alogorithm:
    /// - Check each file (extensions S19 or HEX) within the specified source directory
    /// - return the file which corresponds to the name of the specified souce file excactly
    /// - if no one fits return the first file found (extensions S19 or HEX)
    /// </summary>
    /// <param name="srcFile">The fully qualified path to the source (A2L) file</param>
    /// <param name="byteOrder">The byte order to get a file for</param>
    /// <returns>A fully qualified string of a potential fitting data file (S19 or HEX) or null if no fitting data file is found</returns>
    public static string getNearestDataFile(string srcFile, BYTEORDER_TYPE byteOrder)
    {
      string a2LPpath = Path.GetDirectoryName(srcFile);
      var srcFileList = new List<string>();
      srcFileList.AddRange(Directory.GetFiles(a2LPpath
        , Resources.strHexExtWildcard
        , SearchOption.TopDirectoryOnly
        ));
      srcFileList.AddRange(Directory.GetFiles(a2LPpath
        , Resources.strs19ExtWildcard
        , SearchOption.TopDirectoryOnly
        ));

      if (srcFileList.Count == 0)
        return null;

      string searchFileName = Path.GetFileNameWithoutExtension(srcFile);
      string testFile = null;
      for (int i = 0; i < srcFileList.Count; ++i)
      {
        string current = Path.GetFileNameWithoutExtension(srcFileList[i]);
        if (current.Length < searchFileName.Length)
          continue;
        if (0 != string.Compare(searchFileName, 0, current, 0, searchFileName.Length, true))
          continue;
        testFile = srcFileList[i];
        break;
      }
      return string.IsNullOrEmpty(testFile) ? srcFileList[0] : testFile;
    }
    /// <summary>
    /// Creates MemorySegments from the specified data.
    /// 
    /// -# Merge the fragments into connected segments.
    /// -# Merge the upper result into the defined segments.
    /// -# Takeover not merged segments
    /// </summary>
    /// <param name="partList">List of data parts</param>
    protected internal void createAndMergeMemSegs(ref List<DataPart> partList)
    {
      var srcList = new List<MemorySegment>();
      // create segments from fragments
      var memSeg = new List<byte>();
      UInt32 nextAddress = UInt32.MaxValue, segStart = UInt32.MaxValue;
      for (int k = 0; k < partList.Count; ++k)
      {
        var part = partList[k];
        UInt32 address = part.Address;
        if (segStart == UInt32.MaxValue)
          segStart = address;
        int len = part.Data.Length;
        if (nextAddress != UInt32.MaxValue && address != nextAddress)
        { // new seg
          srcList.Add(new MemorySegment((IDataFile)this, segStart, ref memSeg));
          segStart = address;
        }
        nextAddress = (UInt32)(address + len);
        memSeg.AddRange(part.Data);
      }
      if (memSeg.Count > 0)
      { // add remaining values
        var seg = new MemorySegment((IDataFile)this, segStart, ref memSeg);
        srcList.Add(seg);
      }

      // merge segments
      for (int j = srcList.Count - 1; j >= 0; --j)
      {
        var srcSeg = srcList[j];
        bool merged = false;
        for (int i = mSegments.Count - 1; i >= 0; --i)
        {
          var dstSeg = mSegments[i];
          dstSeg.DataFile = (IDataFile)this;
          if (dstSeg.Address + dstSeg.Size < srcSeg.Address || dstSeg.Address > srcSeg.Address + srcSeg.Size)
            continue;

          merged = true;

          int srcIndex = (int)(dstSeg.Address >= srcSeg.Address ? dstSeg.Address - srcSeg.Address : 0);
          int dstIndex = (int)(dstSeg.Address < srcSeg.Address ? srcSeg.Address - dstSeg.Address : 0);
          int len = Math.Min(dstSeg.Size - dstIndex, srcSeg.Size - srcIndex);
#if WIN32_OPTIMIZED
          jnsoft.Helpers.Extensions.copyMemory(IntPtr.Add(dstSeg.Data, dstIndex), IntPtr.Add(srcSeg.Data, srcIndex), (uint)len);
#else
          Array.Copy(srcSeg.Data, srcIndex, dstSeg.Data, dstIndex, len);
#endif
          dstSeg.IsInitialized = true;
        }
        if (merged)
        { // segment is merged in any destination, remove
          srcSeg.Dispose();
          srcList.RemoveAt(j);
        }
      }
      mSegments.AddRange(srcList);
      mSegments.Sort(MemorySegmentList.sortDataSegsToFront);
      ChangedValues.Clear();
      IsDirty = false;
    }
    /// <summary>
    /// Computes the byte value from a 2 digit hex string.
    /// </summary>
    /// <param name="s">The string to parse</param>
    /// <param name="offset">The offset to parse into</param>
    /// <returns>The byte value</returns>
    protected static byte toByte(ref string s, int offset)
    {
      char c = s[offset];
      int value = c < 'A' ? c - '0' : char.ToUpper(c) - 'A' + 10;
      byte result = (byte)(value << 4);
      c = s[offset + 1];
      value = c < 'A' ? c - '0' : char.ToUpper(c) - 'A' + 10;
      result += (byte)(value & 0xf);
      return result;
    }
    /// <summary>
    /// Frees used resources.
    /// </summary>
    public void Dispose()
    {
      foreach (var memSeg in mSegments)
        memSeg.Dispose();
      //mSegments.Clear();
      //mChangedValues.Clear();
      mTag = null;
    }
    #endregion
    #region ICloneable Member
    /// <summary>
    /// Clone method does a deep copy of the data file.
    /// </summary>
    /// <returns>a new instance cloned from the current instance</returns>
    public abstract object Clone();
    #endregion
    #region IDataFile Member
    public MemorySegmentList SegmentList { get { return mSegments; } }
    /// <summary>
    /// Load the contents of a data file into memory.
    /// </summary>
    /// <param name="fileName">Fully qualified path to the data file to read.</param>
    /// <returns>true if successful.</returns>
    protected abstract bool load(string fileName);
    /// <summary>
    /// Finds the memory segment the specified data is within.
    /// </summary>
    /// <param name="address">The start address to find</param>
    /// <param name="len">The length of the data</param>
    /// <returns>The segment instance or null if not found</returns>
    protected MemorySegment findMemSeg(UInt32 address, int len)
    {
      return mSegments.findMemSeg(address, len);
    }
    /// <summary>
    /// Checks all segments to find the specified epk string.
    /// </summary>
    /// <param name="epkAddress">The epk address</param>
    /// <param name="expectedEPK">The epk string to compare to</param>
    /// <returns>true if the specified EPK is found</returns>
    protected bool EPKCheck(UInt32 epkAddress, string expectedEPK)
    {
      if (string.IsNullOrEmpty(expectedEPK))
        // EPK check not possible (not supported)
        return true;

      var seg = findMemSeg(epkAddress, expectedEPK.Length);
      if (null == seg)
        return false;
      return seg.epkCheck(epkAddress, expectedEPK);
    }
    /// <summary>
    /// Set the EPK to the specified address.
    /// </summary>
    /// <param name="epkAddress">The epk address</param>
    /// <param name="epkToSet">The EPK string to set</param>
    /// <returns>true if the specified EPK is set</returns>
    protected bool setEPK(UInt32 epkAddress, string epkToSet)
    {
      if (string.IsNullOrEmpty(epkToSet))
        // EPK check not possible 
        return false;
      var seg = findMemSeg(epkAddress, epkToSet.Length);
      if (null == seg)
        return false;
      seg.setDataBytes(epkAddress
        , Encoding.ASCII.GetBytes(epkToSet)
        );
      seg.DataFile = (IDataFile)this;
      IsDirty = true;
      return true;
    }
    /// <summary>
    /// Set segments data from the specified segments.
    /// 
    /// - A segment exists if only if the starting addresses and the length is the same
    /// - If a segment is not existing in the target, the data will not be added
    /// - If a segment exists, the specified data will override the data in the target.
    /// </summary>
    /// <param name="segments">The segments to set data from</param>
    protected void setSegmentsData(MemorySegmentList segments)
    {
      foreach (var memSeg in segments)
      {
        for (int i = mSegments.Count - 1; i >= 0; --i)
        {
          var targetSeg = mSegments[i];
          if (targetSeg.Address != memSeg.Address || targetSeg.Size != memSeg.Size)
            continue;
          // segment hit, copy data
#if WIN32_OPTIMIZED
          jnsoft.Helpers.Extensions.copyMemory(targetSeg.Data, memSeg.Data, (uint)targetSeg.Size);
#else
          Array.Copy(memSeg.Data, targetSeg.Data, targetSeg.Size);
#endif
          IsDirty = true;
        }
      }
    }
    /// <summary>
    /// Imports the specified values into the datafile.
    /// 
    /// May be used e.g from a DCMFile import.
    /// </summary>
    /// <param name="dataFile">The datd file to import into</param>
    /// <param name="values">The values to import</param>
    protected static void import(IDataFile dataFile, ICharValue[] values)
    {
      foreach (ICharValue value in values)
        CharacteristicValue.setValue(dataFile, value);
    }
    /// <summary>
    /// Supplies a string representation of the datafile.
    /// </summary>
    /// <returns>the string representation</returns>
    public override string ToString()
    {
      return ChangedValues.Count == 0
        ? mSegments.ToString()
        : string.Format("{0} changed values\n\n{1}", ChangedValues.Count, mSegments.ToString());
    }
    #endregion
  }
}
