/*!
 * @file    
 * @brief   Defines the 'in-memory' representation for a characteristic value.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
 using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

/// <summary>
/// Defines and implements stuff for reading, writing and modifying characteristic values.
/// </summary>
namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Creates a value representation for the different characteristic value types.
  /// 
  /// - Provides setValue and getValue to read/write data from/to byte streams.
  /// - Supported characteristic types: A2LCHARACTERISTIC (Value, ASCII, Value block, Curve, Map) and A2LAXIS_PTS
  /// - Not supported characteristic types: A2LCHARACTERISTIC (Cuboid, Cube_4, Cube_5)
  /// </summary>
  public static class CharacteristicValue
  {
    #region methods
    /// <summary>
    /// Writes count values to the specified offset with the specified parameters.
    /// 
    /// - adds padding with the specified alignment
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="segment">The memory segment to write to</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in] The values to write</param>
    static void writeValues(int count, MemorySegment segment
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      writeValues(count, segment, alignment, dt, bo, bm
        , INDEX_ORDER.INDEX_INCR, DEPOSIT_TYPE.ABSOLUTE
        , cm, ref offset, ref values
        );
    }
    /// <summary>
    /// Writes count values to the specified offset with the specified parameters.
    /// 
    /// - adds padding with the specified alignment
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="segment">The memory segment to write to</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="io">The index order setting</param>
    /// <param name="deposit">The deposit type setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in] The values to write</param>
    static void writeValues(int count, MemorySegment segment
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm, INDEX_ORDER io, DEPOSIT_TYPE deposit, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      int singleSize = dt.getSizeInByte();
      offset = Extensions.alignUp(offset, alignment);  // Alignment
      double predecessorRawValue = double.NaN;
      for (int i = 0; i < count; ++i, offset += singleSize)
      {
        double value2Write = cm.toRaw(dt, values[i]);
        if (deposit == DEPOSIT_TYPE.DIFFERENCE && i > 0)
          // difference mode
          value2Write -= predecessorRawValue;
        setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
        predecessorRawValue = value2Write;
      }
    }
#if WIN32_OPTIMIZED
    /// <summary>
    /// Reads count raw values from the specified offset with the specified parameters.
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readValues(int count, IntPtr data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      readValues(count, data, alignment, dt, bo, UInt64.MaxValue
        , INDEX_ORDER.INDEX_INCR, DEPOSIT_TYPE.ABSOLUTE
        , cm, ref offset, ref values
        );
    }
    /// <summary>
    /// Reads count raw values from the specified offset with the specified parameters.
    /// 
    /// - adds padding with the specified alignment
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="io">The index order setting</param>
    /// <param name="deposit">The deposit type setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readValues(int count, IntPtr data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, ulong bm, INDEX_ORDER io, DEPOSIT_TYPE deposit, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      offset = Extensions.alignUp(offset, alignment);  // Alignment
      int singleSize = dt.getSizeInByte();
      for (int i = 0; i < count; ++i, offset += singleSize)
      {
        double value = getSingleRawValue(data, offset, dt, bo, bm);
        if (deposit == DEPOSIT_TYPE.DIFFERENCE && i > 0)
        { // difference mode
          int predecessor = io != INDEX_ORDER.INDEX_DECR ? i - 1 : count - i;
          value += values[predecessor];
        }
        values[io != INDEX_ORDER.INDEX_DECR ? i : count - 1 - i] = cm.toPhysical(value);
      }
    }
#else    
    /// <summary>
    /// Reads count raw values from the specified offset with the specified parameters.
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readValues(int count, byte[] data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      readValues(count, data, alignment, dt, bo, UInt64.MaxValue
        , INDEX_ORDER.INDEX_INCR, DEPOSIT_TYPE.ABSOLUTE
        , cm, ref offset, ref values
        );
    }
    /// <summary>
    /// Reads count raw values from the specified offset with the specified parameters.
    /// 
    /// - adds padding with the specified alignment
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">the bitmask</param>
    /// <param name="io">The index order setting</param>
    /// <param name="deposit">The deposit type setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readValues(int count, byte[] data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm, INDEX_ORDER io, DEPOSIT_TYPE deposit, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      offset = Extensions.alignUp(offset, alignment);  // Alignment
      int singleSize = dt.getSizeInByte();
      for (int i = 0; i < count; ++i, offset += singleSize)
      {
        double value = getSingleRawValue(data, offset, dt, bo, bm);
        if (deposit == DEPOSIT_TYPE.DIFFERENCE && i > 0)
        { // difference mode
          int predecessor = io != INDEX_ORDER.INDEX_DECR ? i - 1 : count - i;
          value += values[predecessor];
        }
        values[io != INDEX_ORDER.INDEX_DECR ? i : count - 1 - i] = cm.toPhysical(value);
      }
    }
    /// <summary>
    /// Writes count values to the specified offset with the specified parameters.
    /// 
    /// - adds padding with the specified alignment
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in] The values to write</param>
    static void writeValues(int count, byte[] data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      writeValues(count, data, alignment, dt, bo
        , INDEX_ORDER.INDEX_INCR, DEPOSIT_TYPE.ABSOLUTE
        , cm, ref offset, ref values
        );
    }
    /// <summary>
    /// Writes count values to the specified offset with the specified parameters.
    /// </summary>
    /// <param name="count">The count of values to read</param>
    /// <param name="data">The corresponding raw byte bufer</param>
    /// <param name="alignment">The alignment setting</param>
    /// <param name="dt">The single value datatype</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="io">The index order setting</param>
    /// <param name="deposit">The deposit type setting</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in] The values to write</param>
    static void writeValues(int count, byte[] data
      , int alignment, DATA_TYPE dt, BYTEORDER_TYPE bo, INDEX_ORDER io, DEPOSIT_TYPE deposit, A2LCOMPU_METHOD cm
      , ref int offset, ref double[] values
      )
    {
      int singleSize = dt.getSizeInByte();
      offset = Extensions.alignUp(offset, alignment);  // Alignment
      double predecessorRawValue = double.NaN;
      for (int i = 0; i < count; ++i, offset += singleSize)
      {
        double value2Write = cm.toRaw(dt, values[i]);
        if (deposit == DEPOSIT_TYPE.DIFFERENCE && i > 0)
          // difference mode
          value2Write -= predecessorRawValue;
        var buffer = getSingleRawValueBuffer(value2Write, dt, bo);
        Array.Copy(buffer, 0, data, offset, buffer.Length);
        predecessorRawValue = value2Write;
      }
    }
#endif
    /// <summary>
    /// Creates the AXISPTS value.
    /// </summary>
    /// <typeparam name="T">ValBlkValue type</typeparam>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="axisPts">The source characteristic</param>
    /// <param name="format">The value format to create</param>
    /// <returns>a ValBlkValue instance</returns>
    static T getAxisPtsValue<T>(MemorySegment segment, A2LAXIS_PTS axisPts, ValueObjectFormat format) where T : ICharValue, new()
    {
      try
      {
        T result = new T();
        result.Characteristic = axisPts;
        result.ValueFormat = format;
        switch (format)
        {
          case ValueObjectFormat.Physical:
            result.DecimalCount = axisPts.DecimalCount;
            result.Unit = axisPts.Unit;
            break;
          default:
            result.DecimalCount = 0;
            result.Unit = string.Empty;
            break;
        }
        result.Value = getCOMAxisValues(segment, axisPts, format);
        return result;
      }
      catch (IndexOutOfRangeException)
      {
        return default(T);
      }
    }
    /// <summary>
    /// Reads the values from an A2LAXIS_PTS instance.
    /// </summary>
    /// <param name="segment">The memory segment to read from</param>
    /// <param name="axisPts">The axisPts instance</param>
    /// <param name="format">The value format to create</param>
    /// <returns>The axis point values as an array</returns>
    static double[] getCOMAxisValues(MemorySegment segment, A2LAXIS_PTS axisPts, ValueObjectFormat format)
    {
      var compuMethod = format == ValueObjectFormat.Physical
        ? axisPts.RefCompuMethod
        : A2LCOMPU_METHOD.mDefaultCompuMethod;
      var layout = axisPts.RefRecordLayout;

      int offset = (int)(axisPts.Address - segment.Address);
      var noAxisValues = new double[] { axisPts.MaxAxisPoints };
      double[] axisValues = null;
      for (int j = 1; j < 9; ++j)
      {
        if (j == layout.NoAxisPts[0]?.Position)
        { // read axis pts count
          var dt = layout.NoAxisPts[0].DataType;
          readValues(1, segment.Data, layout.getAlignment(dt)
            , dt, axisPts.ByteOrder
            , A2LCOMPU_METHOD.mDefaultCompuMethod
            , ref offset, ref noAxisValues
            );
          if (noAxisValues[0] < 0 || noAxisValues[0] > axisPts.MaxAxisPoints)
            noAxisValues[0] = axisPts.MaxAxisPoints;
        }
        if (j == layout.AxisPts[0]?.Position)
        { // read axis pts
          var dt = layout.AxisPts[0].DataType;
          axisValues = new double[(int)noAxisValues[0]];
          readValues((int)noAxisValues[0], segment.Data, layout.getAlignment(dt)
            , dt, axisPts.ByteOrder, UInt64.MaxValue
            , layout.AxisPts[0].IndexOrder
            , axisPts.Deposit
            , compuMethod, ref offset, ref axisValues);
        }
#if RES_AXIS_SUPPORTED
        if (j == layout.NoRescale[0]?.Position)
        { // read rescale axis pts count
          var dt = layout.NoRescale[0].DataType;
          readValues(1, segment.Data, layout.getAlignment(dt)
            , dt, axisPts.ByteOrder
            , A2LCOMPU_METHOD.mDefaultCompuMethod
            , ref offset, ref noAxisValues
            );
          noAxisValues[0] = Math.Min(noAxisValues[0], layout.AxisRescale[0].MaxNoRescalePairs);
        }
        if (j == layout.AxisRescale[0]?.Position)
        { // read rescale axis pts
          var dt = layout.AxisRescale[0].DataType;
          axisValues = new double[(int)noAxisValues[0] * 2];
          readValues(2 * (int)noAxisValues[0], segment.Data, layout.getAlignment(dt)
            , dt, axisPts.ByteOrder, UInt64.MaxValue
            , layout.AxisRescale[0].IndexOrder
            , axisPts.Deposit
            , compuMethod, ref offset, ref axisValues);
        }
#endif
      }
      return axisValues;
    }
    /// <summary>
    /// Writes the values to an A2LAXIS_PTS instance.
    /// </summary>
    /// <param name="segment">The memory segment to write from</param>
    /// <param name="axisPts">The axisPts instance</param>
    /// <param name="axisValues">The values to write</param>
    /// <param name="format">The value format to create</param>
    /// <returns>The axis point values as an array</returns>
    static void setCOMAxisValues(MemorySegment segment, A2LAXIS_PTS axisPts, double[] axisValues, ValueObjectFormat format)
    {
      var compuMethod = format == ValueObjectFormat.Physical
        ? axisPts.RefCompuMethod
        : A2LCOMPU_METHOD.mDefaultCompuMethod;
      var layout = axisPts.RefRecordLayout;

      int offset = (int)(axisPts.Address - segment.Address);
      setAxisValues(segment, offset
        , axisPts.MaxAxisPoints, axisPts.ByteOrder, axisPts.Deposit
        , layout, compuMethod, 0, axisValues, format
        );
    }
    /// <summary>
    /// Writes the values to an A2LAXIS_PTS instance.
    /// </summary>
    /// <param name="segment">The memory segment to write from</param>
    /// <param name="axisPts">The axisPts instance</param>
    /// <param name="axisValues">The values to write</param>
    /// <param name="format">The value format to create</param>
    /// <returns>The axis point values as an array</returns>
    static void setAxisValues(MemorySegment segment, int offset
      , int maxAxisPoints, BYTEORDER_TYPE bo, DEPOSIT_TYPE deposit, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm
      , int axisIndex, double[] axisValues, ValueObjectFormat format
      )
    {
      var noAxisValues = new double[] { Math.Min(maxAxisPoints, axisValues.Length) };
      for (int j = 1; j < 9; ++j)
      {
        if (j == layout.NoAxisPts[axisIndex]?.Position)
        { // write axis pts count
          var dt = layout.NoAxisPts[axisIndex].DataType;
          writeValues(1, segment, layout.getAlignment(dt)
            , dt, bo, UInt64.MaxValue
            , A2LCOMPU_METHOD.mDefaultCompuMethod
            , ref offset, ref noAxisValues
            );
        }
        if (j == layout.AxisPts[axisIndex]?.Position)
        { // write axis pts
          var dt = layout.AxisPts[axisIndex].DataType;
          writeValues((int)noAxisValues[0], segment, layout.getAlignment(dt)
            , dt, bo, UInt64.MaxValue
            , layout.AxisPts[axisIndex].IndexOrder
            , deposit
            , cm, ref offset, ref axisValues);
        }
#if RES_AXIS_SUPPORTED
        if (j == layout.NoRescale[0]?.Position)
        { // write rescale axis pts count
          var dt = layout.NoRescale[0].DataType;
          writeValues(1, segment, layout.getAlignment(dt)
            , dt, bo, UInt64.MaxValue
            , A2LCOMPU_METHOD.mDefaultCompuMethod
            , ref offset, ref noAxisValues
            );
          noAxisValues[0] = Math.Min(noAxisValues[0], layout.AxisRescale[0].MaxNoRescalePairs);
        }
        if (j == layout.AxisRescale[0]?.Position)
        { // write rescale axis pts
          var dt = layout.AxisRescale[0].DataType;
          writeValues((int)noAxisValues[0], segment, layout.getAlignment(dt)
            , dt, bo, UInt64.MaxValue
            , layout.AxisRescale[0].IndexOrder
            , deposit
            , cm, ref offset, ref axisValues);
        }
#endif
      }
    }
#if WIN32_OPTIMIZED
    /// <summary>
    /// Gets the raw value from a data buffer on the specified location.
    /// 
    /// - Reads the raw value according the datatype from the buffer
    /// - corrects endianess if required
    /// </summary>
    /// <param name="buffer">The corresponding raw byte bufer</param>
    /// <param name="offset">Offset withing the buffer</param>
    /// <param name="dt">the raw datatype of input value</param>
    /// <param name="bo">the byteOrder of the raw value</param>
    /// <returns>the raw value of a single value</returns>
    static double getSingleRawValue(IntPtr buffer, int offset, DATA_TYPE dt, BYTEORDER_TYPE bo)
    {
      return getSingleRawValue(buffer, offset, dt, bo, UInt64.MaxValue);
    }
    /// <summary>
    /// Gets the raw value from a data buffer on the specified location.
    /// 
    /// - Reads the raw value according the datatype from the buffer
    /// - corrects endianess if required
    /// - Shift the raw data according the specified bitmask
    /// </summary>
    /// <param name="buffer">The corresponding raw byte bufer</param>
    /// <param name="offset">Offset withing the buffer</param>
    /// <param name="dt">the raw datatype of input value</param>
    /// <param name="bo">the byteOrder of the raw value</param>
    /// <param name="bm">The bitmask</param>
    /// <returns>the raw value of a single value</returns>
    static double getSingleRawValue(IntPtr buffer, int offset, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm)
    {
      bool turnAround = BitConverter.IsLittleEndian && bo == BYTEORDER_TYPE.BIG_ENDIAN;
      Int64 i64Value;
      switch (dt)
      {
        case DATA_TYPE.UBYTE:
          i64Value = Marshal.ReadByte(buffer, offset);
          break;
        case DATA_TYPE.SBYTE:
          i64Value = (sbyte)Marshal.ReadByte(buffer, offset);
          break;
        case DATA_TYPE.UWORD:
          i64Value = turnAround
            ? Extensions.changeEndianess((UInt16)Marshal.ReadInt16(buffer, offset))
            : (UInt16)Marshal.ReadInt16(buffer, offset);
          break;
        case DATA_TYPE.SWORD:
          i64Value = (Int16)(turnAround
            ? Extensions.changeEndianess((UInt16)Marshal.ReadInt16(buffer, offset))
            : (UInt16)Marshal.ReadInt16(buffer, offset));
          break;
        case DATA_TYPE.ULONG:
          i64Value = turnAround
            ? Extensions.changeEndianess((UInt32)Marshal.ReadInt32(buffer, offset))
            : (UInt32)Marshal.ReadInt32(buffer, offset);
          break;
        case DATA_TYPE.SLONG:
          i64Value = (Int32)(turnAround
            ? Extensions.changeEndianess((UInt32)Marshal.ReadInt32(buffer, offset))
            : (UInt32)Marshal.ReadInt32(buffer, offset));
          break;
        case DATA_TYPE.A_UINT64:
          i64Value = (Int64)(turnAround
            ? Extensions.changeEndianess((UInt64)Marshal.ReadInt64(buffer, offset))
            : (UInt64)Marshal.ReadInt64(buffer, offset));
          break;
        case DATA_TYPE.A_INT64:
          i64Value = (Int64)(turnAround
            ? Extensions.changeEndianess((UInt64)Marshal.ReadInt64(buffer, offset))
            : (UInt64)Marshal.ReadInt64(buffer, offset));
          break;
        case DATA_TYPE.FLOAT32_IEEE:
          // ignore a potential bitmask in this case
          UInt32 value32 = (UInt32)(turnAround
            ? Extensions.changeEndianess((UInt32)Marshal.ReadInt32(buffer, offset))
            : (UInt32)Marshal.ReadInt32(buffer, offset));
          return BitConverter.ToSingle(BitConverter.GetBytes(value32), 0);
        case DATA_TYPE.FLOAT64_IEEE:
          // ignore a potential bitmask in this case
          UInt64 value64 = (UInt64)(turnAround
            ? Extensions.changeEndianess((UInt64)Marshal.ReadInt64(buffer, offset))
            : (UInt64)Marshal.ReadInt64(buffer, offset));
          return BitConverter.ToDouble(BitConverter.GetBytes(value64), 0);
        default:
          throw new NotSupportedException(dt.ToString());
      }
      if (bm != UInt64.MaxValue)
      {
        UInt64 ui64Value = ((UInt64)i64Value) & bm;
        // see ASAP2 specification: shift value down, until the first first bit set in the bitmask is reached
        ui64Value >>= Extensions.getShiftCount(bm);
        i64Value = (Int64)ui64Value;
      }
      return i64Value;
    }
#else
    /// <summary>
    /// Gets the raw value from a single raw data buffer on the specified location.
    /// 
    /// Gets the value without
    /// </summary>
    /// <param name="buffer">The corresponding raw byte bufer</param>
    /// <param name="offset">Offset withing the buffer</param>
    /// <param name="dt">the raw datatype of input value</param>
    /// <param name="bo">the byteOrder of the raw value</param>
    /// <returns>the raw value of a single raw data buffer</returns>
    public static double getSingleRawValue(byte[] buffer, int offset, DATA_TYPE dt, BYTEORDER_TYPE bo)
    {
      return getSingleRawValue(buffer, offset, dt, bo, UInt64.MaxValue);
    }
#endif
    /// <summary>
    /// Gets the raw value from a data buffer on the specified location.
    /// 
    /// - Reads the raw value according the datatype from the buffer
    /// - corrects endianess if required
    /// - Shift the raw data according the specified bitmask
    /// </summary>
    /// <param name="buffer">The corresponding raw byte bufer</param>
    /// <param name="offset">Offset withing the buffer</param>
    /// <param name="dt">the raw datatype of input value</param>
    /// <param name="bo">the byteOrder of the raw value</param>
    /// <param name="bm">The bitmask</param>
    /// <returns>the raw value of a single raw data buffer</returns>
    public static double getSingleRawValue(byte[] buffer, int offset, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm)
    {
      bool turnAround = BitConverter.IsLittleEndian && bo == BYTEORDER_TYPE.BIG_ENDIAN;
      Int64 i64Value;
      switch (dt)
      {
        case DATA_TYPE.UBYTE: i64Value = buffer[offset]; break;
        case DATA_TYPE.SBYTE: i64Value = (sbyte)buffer[offset]; break;
        case DATA_TYPE.UWORD:
          i64Value = turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt16(buffer, offset))
            : BitConverter.ToUInt16(buffer, offset);
          break;
        case DATA_TYPE.SWORD:
          i64Value = (Int16)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt16(buffer, offset))
            : BitConverter.ToUInt16(buffer, offset));
          break;
        case DATA_TYPE.ULONG:
          i64Value = turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt32(buffer, offset))
            : BitConverter.ToUInt32(buffer, offset);
          break;
        case DATA_TYPE.SLONG:
          i64Value = (Int32)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt32(buffer, offset))
            : BitConverter.ToUInt32(buffer, offset));
          break;
        case DATA_TYPE.A_UINT64:
          i64Value = (Int64)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt64(buffer, offset))
            : BitConverter.ToUInt64(buffer, offset));
          break;
        case DATA_TYPE.A_INT64:
          i64Value = (Int64)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt64(buffer, offset))
            : BitConverter.ToUInt64(buffer, offset));
          break;
        case DATA_TYPE.FLOAT32_IEEE:
          UInt32 value32 = (UInt32)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt32(buffer, offset))
            : BitConverter.ToUInt32(buffer, offset));
          // ignore a potential bitmask in this case
          return BitConverter.ToSingle(BitConverter.GetBytes(value32), 0);
        case DATA_TYPE.FLOAT64_IEEE:
          UInt64 value64 = (UInt64)(turnAround
            ? Extensions.changeEndianess(BitConverter.ToUInt64(buffer, offset))
            : BitConverter.ToUInt64(buffer, offset));
          // ignore a potential bitmask in this case
          return BitConverter.ToDouble(BitConverter.GetBytes(value64), 0);
        default:
          throw new NotSupportedException(dt.ToString());
      }

      if (bm != UInt64.MaxValue)
      {
        UInt64 ui64Value = ((UInt64)i64Value) & bm;
        // see ASAP2 specification: shift value down, until the first first bit set in the bitmask is reached
        ui64Value >>= Extensions.getShiftCount(bm);
        i64Value = (Int64)ui64Value;
      }
      return i64Value;
    }
    /// <summary>
    /// Gets a byte buffer for a single raw value.
    /// 
    /// The endianess is taken into account.
    /// </summary>
    /// <param name="rawValue">the raw value</param>
    /// <param name="dt">the datatype of the raw value</param>
    /// <param name="bo">the byteOrder of the raw value</param>
    /// <returns>the buffer (the length of the buffer is the length of the resulting datatype)</returns>
    public static byte[] getSingleRawValueBuffer(double rawValue, DATA_TYPE dt, BYTEORDER_TYPE bo)
    {
      bool turnAround = BitConverter.IsLittleEndian && bo == BYTEORDER_TYPE.BIG_ENDIAN;
      byte[] buffer = null;
      switch (dt)
      {
        case DATA_TYPE.UBYTE:
          rawValue = rawValue > Byte.MaxValue ? Byte.MaxValue : rawValue;
          rawValue = rawValue < Byte.MinValue ? Byte.MinValue : rawValue;
          buffer = new byte[1];
          buffer[0] = (byte)rawValue;
          break;
        case DATA_TYPE.SBYTE:
          rawValue = rawValue > SByte.MaxValue ? SByte.MaxValue : rawValue;
          rawValue = rawValue < SByte.MinValue ? SByte.MinValue : rawValue;
          buffer = new byte[1];
          buffer[0] = (byte)(sbyte)rawValue;
          break;
        case DATA_TYPE.UWORD:
          rawValue = rawValue > UInt16.MaxValue ? UInt16.MaxValue : rawValue;
          rawValue = rawValue < UInt16.MinValue ? UInt16.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt16)rawValue)
            : (UInt16)rawValue);
          break;
        case DATA_TYPE.SWORD:
          rawValue = rawValue > Int16.MaxValue ? Int16.MaxValue : rawValue;
          rawValue = rawValue < Int16.MinValue ? Int16.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt16)(Int16)rawValue)
            : (UInt16)(Int16)rawValue);
          break;
        case DATA_TYPE.ULONG:
          rawValue = rawValue > UInt32.MaxValue ? UInt32.MaxValue : rawValue;
          rawValue = rawValue < UInt32.MinValue ? UInt32.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt32)rawValue)
            : (UInt32)rawValue);
          break;
        case DATA_TYPE.SLONG:
          rawValue = rawValue > Int32.MaxValue ? Int32.MaxValue : rawValue;
          rawValue = rawValue < Int32.MinValue ? Int32.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt32)(Int32)rawValue)
            : (UInt32)(Int32)rawValue);
          break;
        case DATA_TYPE.A_UINT64:
          rawValue = rawValue > UInt64.MaxValue ? UInt64.MaxValue : rawValue;
          rawValue = rawValue < UInt64.MinValue ? UInt64.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt64)rawValue)
            : (UInt64)rawValue);
          break;
        case DATA_TYPE.A_INT64:
          rawValue = rawValue > Int64.MaxValue ? Int64.MaxValue : rawValue;
          rawValue = rawValue < Int64.MinValue ? Int64.MinValue : rawValue;
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess((UInt64)(Int64)rawValue)
            : (UInt64)(Int64)rawValue);
          break;
        case DATA_TYPE.FLOAT32_IEEE:
          rawValue = rawValue > float.MaxValue ? float.MaxValue : rawValue;
          rawValue = rawValue < float.MinValue ? float.MinValue : rawValue;
          UInt32 rawValue32 = BitConverter.ToUInt32(BitConverter.GetBytes((float)rawValue), 0);
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess(rawValue32)
            : rawValue32);
          break;
        case DATA_TYPE.FLOAT64_IEEE:
          rawValue = rawValue > double.MaxValue ? double.MaxValue : rawValue;
          rawValue = rawValue < double.MinValue ? double.MinValue : rawValue;
          UInt64 rawValue64 = BitConverter.ToUInt64(BitConverter.GetBytes((double)rawValue), 0);
          buffer = BitConverter.GetBytes(turnAround
            ? Extensions.changeEndianess(rawValue64)
            : rawValue64);
          break;
        default:
          throw new NotSupportedException(dt.ToString());
      }
      return buffer;
    }
    #region function and axis values
    /// <summary>
    /// Gets all values for axes containing types.
    /// </summary>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="offset">The offset to the segment base</param>
    /// <param name="format">The data format to create</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="axisDescs">The axis descriptions</param>
    /// <param name="maxAxisPoints">[in/out] The maximum axis points for each of the axes</param>
    /// <param name="axisPoints">[out] the axis points as jagged array</param>
    /// <param name="fncValues">[out] the function values as an object</param>
    static void getAllValuesForAxisTypes(MemorySegment segment, int offset
      , ValueObjectFormat format, A2LRECORD_LAYOUT layout, BYTEORDER_TYPE bo, UInt64 bm, A2LCOMPU_METHOD cm
      , List<A2LAXIS_DESCR> axisDescs, int[] maxAxisPoints, double[][] axisPoints
      , ref object fncValues
      )
    {
      var axisCMs = new List<A2LCOMPU_METHOD>();
      foreach (var axisDesc in axisDescs)
        axisCMs.Add(format == ValueObjectFormat.Physical ? axisDesc.RefCompuMethod : A2LCOMPU_METHOD.mDefaultCompuMethod);

      foreach (var itemLayout in layout.LayoutEntries)
      { // iterate over record layout positions
        int axisIdx = itemLayout.AxisIdx;
        var dt = itemLayout.DataType;
        var alignment = layout.getAlignment(dt);
        var axisDesc = axisIdx >= 0 ? axisDescs[axisIdx] : null;

        if (itemLayout is A2LRECORD_LAYOUT.FncValuesLayoutDesc)
        { // read function values
          switch (axisDescs.Count)
          {
            case 1: // Curve
              var _1values = (double[])fncValues;
              readOrWriteFncValues(false, segment, bo, bm, layout, cm, axisCMs, ref offset, ref axisPoints, ref _1values);
              break;

            case 2: // Map
              var _2values = (double[,])fncValues;
              readOrWriteFncValues(false, segment, bo, bm, layout, cm, axisCMs, ref offset, ref axisPoints, ref _2values);
              break;

            case 3: // Cuboid
              var _3values = (double[,,])fncValues;
              readOrWriteFncValues(false, segment, bo, bm, layout, cm, ref offset, ref _3values);
              break;

            case 4: // Cube4
              var _4values = (double[,,,])fncValues;
              readOrWriteFncValues(false, segment, bo, bm, layout, cm, ref offset, ref _4values);
              break;

            case 5: // Cube5
              var _5values = (double[,,,,])fncValues;
              readOrWriteFncValues(false, segment, bo, bm, layout, cm, ref offset, ref _5values);
              break;
          }
        }
        else if (itemLayout is A2LRECORD_LAYOUT.AxisRescaleLayoutDesc)
        { // read axis rescale points
          var rescaleLayout = (A2LRECORD_LAYOUT.AxisRescaleLayoutDesc)itemLayout;
          var rescalePairsX = new double[rescaleLayout.MaxNoRescalePairs * 2];
          readValues(rescaleLayout.MaxNoRescalePairs * 2
            , segment.Data, alignment, dt, axisDesc.ByteOrder
            , A2LCOMPU_METHOD.mDefaultCompuMethod, ref offset, ref rescalePairsX);
        }
        else if (itemLayout is A2LRECORD_LAYOUT.AxisPtsLayoutDesc)
        { // read axis values
          if (axisIdx < 2 && layout.FncValues.IndexMode >= INDEX_MODE.ALTERNATE_WITH_X)
            // X,Y axis values are coded within function values
            continue;

          readValues(maxAxisPoints[axisIdx]
          , segment.Data, alignment, dt, axisDesc.ByteOrder, UInt64.MaxValue
          , ((A2LRECORD_LAYOUT.AxisPtsLayoutDesc)itemLayout).IndexOrder
          , axisDesc.Deposit
          , axisCMs[axisIdx], ref offset, ref axisPoints[axisIdx]);
        }
        else if (itemLayout is A2LRECORD_LAYOUT.NoAxisPtsLayoutDesc)
        { // read number of axis points values
          if (axisIdx < 2 && layout.FncValues.IndexMode >= INDEX_MODE.ALTERNATE_WITH_X)
            // X,Y axis values are coded within function values
            continue;

          offset = Extensions.alignUp(offset, alignment);  // Alignment
          double rawValue = getSingleRawValue(segment.Data, offset, dt, bo);
          offset += dt.getSizeInByte();
          if (!layout.StaticRecordLayout && rawValue > 0 && rawValue < axisDesc.MaxAxisPoints)
          { // The MaxAxisValue in the axis description is bigger than the value read from data
            if (itemLayout.Position < layout.FncValues.Position)
            { // this works only if the Axis no position is smaller than the function values position
              // in case the function values gets read later
              maxAxisPoints[axisIdx] = (int)rawValue;
              axisPoints[axisIdx] = new double[maxAxisPoints[axisIdx]];
              switch (axisDescs.Count)
              { // set new function value space for all possible types
                case 1: fncValues = new double[maxAxisPoints[0]]; break;
                case 2: fncValues = new double[maxAxisPoints[0], maxAxisPoints[1]]; break;
                case 3: fncValues = new double[maxAxisPoints[0], maxAxisPoints[1], maxAxisPoints[2]]; break;
                case 4: fncValues = new double[maxAxisPoints[0], maxAxisPoints[1], maxAxisPoints[2], maxAxisPoints[3]]; break;
                case 5: fncValues = new double[maxAxisPoints[0], maxAxisPoints[1], maxAxisPoints[2], maxAxisPoints[3], maxAxisPoints[4]]; break;
              }
            }
          }
        }
        else
        { // read anything else
          offset = Extensions.alignUp(offset, alignment);  // Alignment
          offset += dt.getSizeInByte();
        }
      }

      for (int i = 0; i < axisDescs.Count; ++i)
      { // read the external axis values
        var axisDesc = axisDescs[i];
        var axisCompuMethod = format == ValueObjectFormat.Physical
          ? axisDesc.RefCompuMethod
          : A2LCOMPU_METHOD.mDefaultCompuMethod;
        switch (axisDesc.AxisType)
        {
          case AXIS_TYPE.FIX_AXIS:

            var par = axisDesc.FixAxisPar;
            if (axisDesc.FixAxisPar != null)
            { // fix par (shift) in description
              // Xi = Offset + (i - 1)*2^Shift i = { 1...numberapo }
              UInt32 distance = ((UInt32)0x01 << axisDesc.FixAxisPar.Shift);
              for (int j = 0; j < axisDesc.FixAxisPar.Numberapo; ++j)
                axisPoints[i][j] = axisCompuMethod.toPhysical(
                  axisDesc.FixAxisPar.Offset + j * distance
                  );
            }
            else if (layout.ShiftOp != null && layout.ShiftOp[i] != null)
            { // fix axis par in memory
              var offsetLayout = layout.Offset[i];
              int offsetPos = A2LCHARACTERISTIC.getOffsetToPosition(offsetLayout.Position, layout, axisDescs, 0);
              double fixOffset = getSingleRawValue(segment.Data, offsetPos, offsetLayout.DataType, bo);
              var shiftLayout = layout.ShiftOp[i];
              int shiftPos = A2LCHARACTERISTIC.getOffsetToPosition(shiftLayout.Position, layout, axisDescs, 0);
              double fixShift = getSingleRawValue(segment.Data, shiftPos, shiftLayout.DataType, bo);
              UInt32 distance = ((UInt32)0x01 << (int)fixShift);
              for (int j = 0; j < maxAxisPoints[i]; ++j)
                axisPoints[i][j] = axisCompuMethod.toPhysical(
                  fixOffset + j * distance
                  );
            }
            else if (axisDesc.FixAxisParDist != null)
            { // fix dist in description
              // Xi = Offset + (i - 1)*Distance
              for (int j = 0; j < axisDesc.FixAxisParDist.Numberapo; ++j)
                axisPoints[i][j] = axisCompuMethod.toPhysical(
                  axisDesc.FixAxisParDist.Offset + j * axisDesc.FixAxisParDist.Distance
                  );
            }
            else if (layout.DistOp != null && layout.DistOp[i] != null)
            { // fix dist in memory
              var offsetLayout = layout.Offset[i];
              int offsetPos = A2LCHARACTERISTIC.getOffsetToPosition(offsetLayout.Position, layout, axisDescs, 0);
              double fixOffset = getSingleRawValue(segment.Data, offsetPos, offsetLayout.DataType, bo);
              var distanceLayout = layout.DistOp[i];
              int distPos = A2LCHARACTERISTIC.getOffsetToPosition(distanceLayout.Position, layout, axisDescs, 0);
              double fixDist = getSingleRawValue(segment.Data, distPos, distanceLayout.DataType, bo);
              for (int j = 0; j < maxAxisPoints[i]; ++j)
                axisPoints[i][j] = axisCompuMethod.toPhysical(
                  fixOffset + j * fixDist
                  );
            }
            else
            { // try to get values from FIX_AXIS_PAR_LIST entity
              var list = axisDesc.getNode<A2LFIX_AXIS_PAR_LIST>(false);
              if (list != null)
                for (int j = 0; j < list.Values.Count; ++j)
                  axisPoints[i][j] = list.Values[j];
            }
            break;
          case AXIS_TYPE.COM_AXIS:
            var axisPts = axisDesc.RefAxisPtsRef;
            segment = segment.DataFile.findMemSeg(axisPts.Address);
            axisPoints[i] = getCOMAxisValues(segment, axisPts, format);
            break;
          case AXIS_TYPE.CURVE_AXIS:
            var curve = axisDesc.RefCurveAxisRef;
            segment = segment.DataFile.findMemSeg(curve.Address);
            axisPoints[i] = ((ICharValue)readFull<CurveValue>(segment, curve, format)).AxisValue[0];
            break;
#if RES_AXIS_SUPPORTED
          case AXIS_TYPE.RES_AXIS:
            //axisPts = axisDesc.RefAxisPtsRef;
            //segment = segment.DataFile.findMemSeg(axisPts.Address);
            //double[] rescaleAxis = getCOMAxisValues(segment, axisPts, format);
            //axisPoints[i] = getCOMAxisValues(segment, axisPts, format);
            break;
#endif
        }
      }
    }
    /// <summary>
    /// Reads all values for the specified characteristic.
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="characteristic">The source characteristic</param>
    /// <param name="format">The value format to create</param>
    static T readFull<T>(MemorySegment segment, A2LCHARACTERISTIC characteristic, ValueObjectFormat format) where T : ICharValue, new()
    {
      var result = new T();
      result.Characteristic = characteristic;
      result.ValueFormat = format;
      result.Unit = format == ValueObjectFormat.Physical ? characteristic.Unit : string.Empty;
      result.DecimalCount = format == ValueObjectFormat.Physical ? characteristic.DecimalCount : 0;
      var compuMethod = format == ValueObjectFormat.Physical ? characteristic.RefCompuMethod : A2LCOMPU_METHOD.mDefaultCompuMethod;
      var layout = characteristic.RefRecordLayout;
      int offset = (int)(characteristic.Address - segment.Address);
      object fncValues = null;

      switch (characteristic.CharType)
      {
        case CHAR_TYPE.VALUE:
          fncValues = compuMethod.toPhysical(getSingleRawValue(segment.Data, offset, layout.FncValues.DataType, characteristic.ByteOrder, characteristic.Bitmask));
          break;

        case CHAR_TYPE.ASCII:
          var value = new byte[characteristic.getNumberOfElements()];
#if WIN32_OPTIMIZED
          Marshal.Copy(IntPtr.Add(segment.Data, (int)(characteristic.Address - segment.Address)), value, 0, value.Length);
#else
          Array.Copy(segment.Data, (int)(characteristic.Address - segment.Address), value, 0, value.Length);
#endif
          result.ValueFormat = ValueObjectFormat.Physical;
          Encoding encoding;
          switch (characteristic.Encoding)
          {
            case EncodingType.UTF8: encoding = Encoding.UTF8; break;
            case EncodingType.UTF16: encoding = Encoding.Unicode; break;
            case EncodingType.UTF32: encoding = Encoding.UTF32; break;
            default: encoding = Encoding.ASCII; break;
          }
          fncValues = encoding.GetString(value).Trim('\0');
          break;

        case CHAR_TYPE.VAL_BLK:
          double[] values = new double[characteristic.getNumberOfElements()];
          readValues(values.Length, segment.Data
            , layout.getAlignment(layout.FncValues.DataType), layout.FncValues.DataType
            , characteristic.ByteOrder, compuMethod, ref offset, ref values
            );
          fncValues = values;
          break;

        case CHAR_TYPE.CURVE:
        case CHAR_TYPE.MAP:
        case CHAR_TYPE.CUBOID:
        case CHAR_TYPE.CUBE_4:
        case CHAR_TYPE.CUBE_5:
          var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
          var axisUnits = new List<string>();
          var axisDecimalCounts = new List<int>();
          var axisPoints = new int[axisDescs.Count];
          result.AxisValue = new double[axisDescs.Count][];

          int i = 0;
          foreach (var axisDesc in axisDescs)
          {
            axisUnits.Add(format == ValueObjectFormat.Physical ? axisDesc.Unit : string.Empty);
            axisDecimalCounts.Add(format == ValueObjectFormat.Physical ? A2LCONVERSION_REF.getDecimalCount(axisDesc.Format) : 0);
            axisPoints[i] = axisDesc.MaxAxisPoints;
            result.AxisValue[i++] = new double[axisDesc.MaxAxisPoints];
          }
          result.UnitAxis = axisUnits.ToArray();
          result.DecimalCountAxis = axisDecimalCounts.ToArray();

          switch (axisDescs.Count)
          {
            case 1: fncValues = new double[axisPoints[0]]; break;
            case 2: fncValues = new double[axisPoints[0], axisPoints[1]]; break;
            case 3: fncValues = new double[axisPoints[0], axisPoints[1], axisPoints[2]]; break;
            case 4: fncValues = new double[axisPoints[0], axisPoints[1], axisPoints[2], axisPoints[3]]; break;
            case 5: fncValues = new double[axisPoints[0], axisPoints[1], axisPoints[2], axisPoints[3], axisPoints[4]]; break;
            default: throw new NotSupportedException(axisDescs.Count.ToString());
          }

          getAllValuesForAxisTypes(segment, offset
            , format, layout
            , characteristic.ByteOrder, characteristic.Bitmask
            , compuMethod
            , axisDescs, axisPoints, result.AxisValue, ref fncValues
            );

          break;
      }

      result.Value = fncValues;
      return result;
    }
    /// <summary>
    /// Reads or writes the function values (curve).
    /// 
    /// For ALTERNATE_WITH_X, memory starts with the axis values. 
    /// Unfortunately, this is not clearly defined in the ASAP2 specification.
    /// </summary>
    /// <param name="write">true if values should be written else false</param>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="axisCMs">The corresponding axis compu methods</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="axisPoints">[in/out] The resulting axisPoints</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readOrWriteFncValues(bool write, MemorySegment segment
      , BYTEORDER_TYPE bo, ulong bm, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm, List<A2LCOMPU_METHOD> axisCMs
      , ref int offset, ref double[][] axisPoints
      , ref double[] values
      )
    {
      var dt = layout.FncValues.DataType;
      int maxX = values.GetLength(0);
      int singleSize = dt.getSizeInByte();
      int alignment = layout.getAlignment(dt);
      offset = Extensions.alignUp(offset, alignment);  // Alignment

      switch (layout.FncValues.IndexMode)
      {
        case INDEX_MODE.COLUMN_DIR:
        case INDEX_MODE.ROW_DIR:
          // column direction
          for (int x = 0; x < maxX; ++x, offset += singleSize)
          {
            if (write)
            {
              double value2Write = cm.toRaw(dt, values[x]);
              if (double.IsNaN(value2Write))
                throw new ApplicationException();
              setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
            }
            else
            {
              double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
              values[x] = cm.toPhysical(value);
            }
          }
          break;

        case INDEX_MODE.ALTERNATE_WITH_X:
          for (int x = 0; x < maxX; ++x, offset += singleSize)
          {
            if (write)
            { // x axis value
              double value2Write = axisCMs[0].toRaw(dt, axisPoints[0][x]);
              setRawValueToBuffer(value2Write, segment, offset, dt, bo, UInt64.MaxValue);

              // Function value
              offset += singleSize;
              value2Write = cm.toRaw(dt, values[x]);
              if (double.IsNaN(value2Write))
                throw new ApplicationException();
              setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
            }
            else
            { // x axis value
              axisPoints[0][x] = axisCMs[0].toPhysical(getSingleRawValue(segment.Data, offset, dt, bo));

              // Function value
              offset += singleSize;
              values[x] = cm.toPhysical(getSingleRawValue(segment.Data, offset, dt, bo, bm));
            }
          }
          break;
        default: throw new NotSupportedException(layout.FncValues.IndexMode.ToString());
      }
    }
    /// <summary>
    /// Reads or writes the function values (map).
    /// 
    /// For ALTERNATE_WITH_X, ALTERNATE_WITH_Y, memory starts with the axis values. 
    /// Unfortunately, this is not clearly defined in the ASAP2 specification.
    /// </summary>
    /// <param name="write">true if values should be written else false</param>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="axisCMs">The corresponding axis compu methods</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="axisPoints">[in/out] The resulting axisPoints</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readOrWriteFncValues(bool write, MemorySegment segment
      , BYTEORDER_TYPE bo, ulong bm, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm, List<A2LCOMPU_METHOD> axisCMs
      , ref int offset, ref double[][] axisPoints
      , ref double[,] values
      )
    {
      var dt = layout.FncValues.DataType;
      int maxX = values.GetLength(0), maxY = values.GetLength(1);
      int singleSize = dt.getSizeInByte();
      int alignment = layout.getAlignment(dt);
      offset = Extensions.alignUp(offset, alignment);  // Alignment

      switch (layout.FncValues.IndexMode)
      {
        case INDEX_MODE.COLUMN_DIR:
          // column direction
          for (int x = 0; x < maxX; ++x)
            for (int y = 0; y < maxY; ++y, offset += singleSize)
            {
              if (write)
              {
                double value2Write = cm.toRaw(dt, values[x, y]);
                if (double.IsNaN(value2Write))
                  throw new ApplicationException();
                setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
              }
              else
              {
                double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                values[x, y] = cm.toPhysical(value);
              }
            }
          break;

        case INDEX_MODE.ROW_DIR:
          // row direction
          for (int y = 0; y < maxY; ++y)
            for (int x = 0; x < maxX; ++x, offset += singleSize)
            {
              if (write)
              {
                double value2Write = cm.toRaw(dt, values[x, y]);
                if (double.IsNaN(value2Write))
                  throw new ApplicationException();
                setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
              }
              else
              {
                double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                values[x, y] = cm.toPhysical(value);
              }
            }
          break;

        case INDEX_MODE.ALTERNATE_WITH_X:
          for (int x = 0; x < maxX; ++x)
          {
            if (write)
            { // x axis value
              double value2Write = axisCMs[0].toRaw(dt, axisPoints[0][x]);
              var buffer = getSingleRawValueBuffer(value2Write, dt, bo);
#if WIN32_OPTIMIZED
              Marshal.Copy(buffer, 0, IntPtr.Add(segment.Data, offset), buffer.Length);
#else
              Array.Copy(buffer, 0, segment.Data, offset, buffer.Length);
#endif
            }
            else
            { // x axis value
              axisPoints[0][x] = axisCMs[0].toPhysical(getSingleRawValue(segment.Data, offset, dt, bo));
            }

            // function values
            offset += singleSize;
            for (int y = 0; y < maxY; ++y, offset += singleSize)
            {
              if (write)
              {
                double value2Write = cm.toRaw(dt, values[x, y]);
                if (double.IsNaN(value2Write))
                  throw new ApplicationException();
                setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
              }
              else
              {
                double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                values[x, y] = cm.toPhysical(value);
              }
            }
          }
          break;

        case INDEX_MODE.ALTERNATE_WITH_Y:
          // row direction
          for (int y = 0; y < maxY; ++y)
          {
            if (write)
            { // y axis value
              double value2Write = axisCMs[1].toRaw(dt, axisPoints[1][y]);
              var buffer = getSingleRawValueBuffer(value2Write, dt, bo);
#if WIN32_OPTIMIZED
              Marshal.Copy(buffer, 0, IntPtr.Add(segment.Data, offset), buffer.Length);
#else
              Array.Copy(buffer, 0, segment.Data, offset, buffer.Length);
#endif
            }
            else
            { // y axis value
              axisPoints[1][y] = axisCMs[1].toPhysical(getSingleRawValue(segment.Data, offset, dt, bo));
            }

            // function values
            offset += singleSize;
            for (int x = 0; x < maxX; ++x, offset += singleSize)
            {
              if (write)
              {
                double value2Write = cm.toRaw(dt, values[x, y]);
                if (double.IsNaN(value2Write))
                  throw new ApplicationException();
                setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
              }
              else
              {
                double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                values[x, y] = cm.toPhysical(value);
              }
            }
          }
          break;

        default: throw new NotSupportedException(layout.FncValues.IndexMode.ToString());
      }
    }
    /// <summary>
    /// Reads or writes the function values (cuboid).
    /// </summary>
    /// <param name="write">true if values should be written else false</param>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readOrWriteFncValues(bool write, MemorySegment segment
      , BYTEORDER_TYPE bo, UInt64 bm, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm
      , ref int offset, ref double[,,] values
      )
    {
      var dt = layout.FncValues.DataType;
      int maxX = values.GetLength(0), maxY = values.GetLength(1);
      int singleSize = dt.getSizeInByte();
      int alignment = layout.getAlignment(dt);
      offset = Extensions.alignUp(offset, alignment);  // Alignment

      switch (layout.FncValues.IndexMode)
      {
        case INDEX_MODE.COLUMN_DIR:
          // column direction
          for (int z = 0; z < values.GetLength(2); ++z)
            for (int x = 0; x < maxX; ++x)
              for (int y = 0; y < maxY; ++y, offset += singleSize)
              {
                if (write)
                {
                  double value2Write = cm.toRaw(dt, values[x, y, z]);
                  if (double.IsNaN(value2Write))
                    throw new ApplicationException();
                  setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                }
                else
                {
                  double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                  values[x, y, z] = cm.toPhysical(value);
                }
              }
          break;
        case INDEX_MODE.ROW_DIR:
          // row, default direction
          for (int z = 0; z < values.GetLength(2); ++z)
            for (int y = 0; y < maxY; ++y)
              for (int x = 0; x < maxX; ++x, offset += singleSize)
              {
                if (write)
                {
                  double value2Write = cm.toRaw(dt, values[x, y, z]);
                  if (double.IsNaN(value2Write))
                    throw new ApplicationException();
                  setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                }
                else
                {
                  double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                  values[x, y, z] = cm.toPhysical(value);
                }
              }
          break;
        default: throw new NotSupportedException(layout.FncValues.IndexMode.ToString());
      }
    }
    /// <summary>
    /// Reads or writes the function values (cube_4).
    /// </summary>
    /// <param name="write">true if values should be written else false</param>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    static void readOrWriteFncValues(bool write, MemorySegment segment
      , BYTEORDER_TYPE bo, UInt64 bm, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm
      , ref int offset, ref double[,,,] values
      )
    {
      var dt = layout.FncValues.DataType;
      int maxX = values.GetLength(0), maxY = values.GetLength(1);
      int singleSize = dt.getSizeInByte();
      int alignment = layout.getAlignment(dt);
      offset = Extensions.alignUp(offset, alignment);  // Alignment

      switch (layout.FncValues.IndexMode)
      {
        case INDEX_MODE.COLUMN_DIR:
          // column direction
          for (int z1 = 0; z1 < values.GetLength(3); ++z1)
            for (int z = 0; z < values.GetLength(2); ++z)
              for (int x = 0; x < maxX; ++x)
                for (int y = 0; y < maxY; ++y, offset += singleSize)
                {
                  if (write)
                  {
                    double value2Write = cm.toRaw(dt, values[x, y, z, z1]);
                    if (double.IsNaN(value2Write))
                      throw new ApplicationException();
                    setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                  }
                  else
                  {
                    double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                    values[x, y, z, z1] = cm.toPhysical(value);
                  }
                }
          break;
        case INDEX_MODE.ROW_DIR:
          // row, default direction
          for (int z1 = 0; z1 < values.GetLength(3); ++z1)
            for (int z = 0; z < values.GetLength(2); ++z)
              for (int y = 0; y < maxY; ++y)
                for (int x = 0; x < maxX; ++x, offset += singleSize)
                {
                  if (write)
                  {
                    double value2Write = cm.toRaw(dt, values[x, y, z, z1]);
                    if (double.IsNaN(value2Write))
                      throw new ApplicationException();
                    setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                  }
                  else
                  {
                    double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                    values[x, y, z, z1] = cm.toPhysical(value);
                  }
                }
          break;
        default: throw new NotSupportedException(layout.FncValues.IndexMode.ToString());
      }
    }
    /// <summary>
    /// Reads or writes the function values (cube_5).
    /// </summary>
    /// <param name="write">true if values should be written else false</param>
    /// <param name="segment">The corresponding memory segment</param>
    /// <param name="bo">The byte order setting</param>
    /// <param name="bm">The bitmask</param>
    /// <param name="layout">The corresponding record layout</param>
    /// <param name="cm">The corresponding compu method</param>
    /// <param name="offset">[in/out] The offset (modified by this method)</param>
    /// <param name="values">[in/out] The resulting values (output of this method)</param>
    /// <exception cref="NotSupportedException">Thrown, if index mode is not supported</exception>
    static void readOrWriteFncValues(bool write, MemorySegment segment
      , BYTEORDER_TYPE bo, ulong bm, A2LRECORD_LAYOUT layout, A2LCOMPU_METHOD cm
      , ref int offset, ref double[,,,,] values
      )
    {
      var dt = layout.FncValues.DataType;
      int maxX = values.GetLength(0), maxY = values.GetLength(1);
      int singleSize = dt.getSizeInByte();
      int alignment = layout.getAlignment(dt);
      offset = Extensions.alignUp(offset, alignment);  // Alignment

      switch (layout.FncValues.IndexMode)
      {
        case INDEX_MODE.COLUMN_DIR:
          // column direction
          for (int z2 = 0; z2 < values.GetLength(4); ++z2)
            for (int z1 = 0; z1 < values.GetLength(3); ++z1)
              for (int z = 0; z < values.GetLength(2); ++z)
                for (int x = 0; x < maxX; ++x)
                  for (int y = 0; y < maxY; ++y, offset += singleSize)
                  {
                    if (write)
                    {
                      double value2Write = cm.toRaw(dt, values[x, y, z, z1, z2]);
                      if (double.IsNaN(value2Write))
                        throw new ApplicationException();
                      setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                    }
                    else
                    {
                      double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                      values[x, y, z, z1, z2] = cm.toPhysical(value);
                    }
                  }
          break;
        case INDEX_MODE.ROW_DIR:
          // row, default direction
          for (int z2 = 0; z2 < values.GetLength(4); ++z2)
            for (int z1 = 0; z1 < values.GetLength(3); ++z1)
              for (int z = 0; z < values.GetLength(2); ++z)
                for (int y = 0; y < maxY; ++y)
                  for (int x = 0; x < maxX; ++x, offset += singleSize)
                  {
                    if (write)
                    {
                      double value2Write = cm.toRaw(dt, values[x, y, z, z1, z2]);
                      if (double.IsNaN(value2Write))
                        throw new ApplicationException();
                      setRawValueToBuffer(value2Write, segment, offset, dt, bo, bm);
                    }
                    else
                    {
                      double value = getSingleRawValue(segment.Data, offset, dt, bo, bm);
                      values[x, y, z, z1, z2] = cm.toPhysical(value);
                    }
                  }
          break;
        default: throw new NotSupportedException(layout.FncValues.IndexMode.ToString());
      }
    }
    #endregion
    #region interface
    /// <summary>
    /// Creates a value representation for the specified A2LAXIS_PTS.
    /// </summary>
    /// <example>
    /// <para>Example code:
    /// Create a physical value representation for any A2LAXIS_PTS 
    /// value within the project and store this on the 
    /// characteristic's A2LNODE.Tag property.
    /// </para>
    /// <code>
    /// var enAxisPts = mA2lParser.Project.AxisPtsDict.GetEnumerator();
    /// while (enAxisPts.MoveNext())
    /// {
    ///   var axisPts = enAxisPts.Current.Value;
    ///   var value = CharacteristicValue.getValue<ValBlkValue>(axisPts
    ///     , ValueObjectFormat.Physical
    ///     );
    ///   axisPts.Tag = value;
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">The value type to create (in this case the ValBlkValue type)</typeparam>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="axisPts">The source characteristic</param>
    /// <param name="format">The data format to create</param>
    /// <returns>a ValBlkValue instance</returns>
    /// <exception cref="ArgumentException">Thrown if T is not a ValBlkValue type</exception>
    public static T getValue<T>(IDataFile dataFile, A2LAXIS_PTS axisPts, ValueObjectFormat format) where T : BaseValue, new()
    {
      if (typeof(T) != typeof(ValBlkValue))
        throw new ArgumentException();

      // get memory segment
      var segment = dataFile.findMemSeg(axisPts.Address);
      if (segment == null)
        return default(T);

      return getAxisPtsValue<T>(segment, axisPts, format);
    }
    /// <summary>
    /// Creates a value representation for the specified A2LCHARACTERISTIC.
    /// </summary>
    /// <example>
    /// <para>Example code:
    /// Create a physical value representation for any characteristic 
    /// single value type within the project and store this on the 
    /// A2LNODE.Tag property.
    /// </para>
    /// <code>
    /// var en = mA2lParser.Project.CharDict.GetEnumerator();
    /// while (en.MoveNext())
    /// {
    ///   var characteristic = en.Current.Value;
    ///   switch (characteristic.CharType)
    ///   {
    ///     case CHAR_TYPE.VALUE:
    ///      var value = CharacteristicValue.getValue<SingleValue>(characteristic
    ///       , ValueObjectFormat.Physical
    ///       );
    ///      characteristic.Tag = value;
    ///      break;
    ///   }
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">The value type to create
    /// (Depending on the characteristic's type, this must be a ICharValue derived class)
    /// </typeparam>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="characteristic">The source characteristic</param>
    /// <param name="format">The value format to create</param>
    /// <returns>An ICharValue instance according the specified characteristic type</returns>
    /// <exception cref="ArgumentException">Thrown if the specified type doesn't fit to the characteristic's type</exception>
    public static T getValue<T>(IDataFile dataFile, A2LCHARACTERISTIC characteristic, ValueObjectFormat format) where T : BaseValue, new()
    {
      // get memory segment
      var segment = dataFile.findMemSeg(characteristic.Address);
      T result = null;
      if (segment == null)
        return result;

      switch (characteristic.CharType)
      {
        case CHAR_TYPE.VALUE:
          if (typeof(T) != typeof(SingleValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.ASCII:
          if (typeof(T) != typeof(ASCIIValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.VAL_BLK:
          if (typeof(T) != typeof(ValBlkValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.CURVE:
          if (typeof(T) != typeof(CurveValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.MAP:
          if (typeof(T) != typeof(MapValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.CUBOID:
          if (typeof(T) != typeof(CuboidValue))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.CUBE_4:
          if (typeof(T) != typeof(Cube4Value))
            throw new ArgumentException();
          break;
        case CHAR_TYPE.CUBE_5:
          if (typeof(T) != typeof(Cube5Value))
            throw new ArgumentException();
          break;
        default: throw new NotSupportedException(characteristic.CharType.ToString());

      }
      return readFull<T>(segment, characteristic, format);
    }
    /// <summary>
    /// Sets the specified value as new axis points data.
    /// </summary>
    /// <typeparam name="T">The value type (in this case the ValBlkValue type)</typeparam>
    /// <param name="dataFile">The datafile to write to</param>
    /// <param name="axisPts">The corresponding axis points instance</param>
    /// <param name="value">An ValBlkValue instance to read from</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown if the value is not a ValBlkValue instance</exception>
    public static bool setValue(IDataFile dataFile, A2LAXIS_PTS axisPts, ICharValue value)
    {
      if (!(value is ValBlkValue))
        throw new ArgumentException();

      // get memory segment
      var segment = dataFile.findMemSeg(axisPts.Address);
      if (segment == null)
        return false;

      var values = (double[])value.Value;
      setCOMAxisValues(segment, axisPts, values, value.ValueFormat);
      return true;
    }
    /// <summary>
    /// Sets a raw value into segment memory.
    /// </summary>
    /// <param name="rawValue">The raw value</param>
    /// <param name="segment">The target segment</param>
    /// <param name="offset">The offset to set the value to</param>
    /// <param name="dt">The data type</param>
    /// <param name="bo">The byte order</param>
    /// <param name="bm">The bitmask</param>
    static void setRawValueToBuffer(double rawValue, MemorySegment segment, int offset, DATA_TYPE dt, BYTEORDER_TYPE bo, UInt64 bm)
    {
      if (bm != UInt64.MaxValue)
      { // bitmask mode
        try
        { // convert to int
          // get memory value
          // delete old value by bitmask
          // set new value by bitmask
          UInt64 rawUIValue = Convert.ToUInt64(rawValue);
          UInt64 rawUIMemory = Convert.ToUInt64(getSingleRawValue(segment.Data, offset, dt, bo));
          rawUIMemory &= ~bm;
          rawUIMemory |= (rawUIValue << Extensions.getShiftCount(bm));
          rawValue = Convert.ToDouble(rawUIMemory);
        }
        catch (OverflowException)
        { // may occur if a bitmask is set to an FLOAT value
          // which doesn't make any sense
        }
      }
      var buffer = getSingleRawValueBuffer(rawValue, dt, bo);
      segment.setDataBytes(offset, buffer);
    }
    /// <summary>
    /// Sets the specified value as new characteristics data.
    /// 
    /// </summary>
    /// <typeparam name="T">The value type
    /// (Depending on the characteristic's type, this must be a ICharValue derived class)
    /// </typeparam>
    /// <param name="dataFile">The datafile to write to</param>
    /// <param name="characteristic">The corresponding characteristic</param>
    /// <param name="value">An ICharValue instance (according the specified characteristic type) to read from</param>
    /// <returns>true if successful</returns>
    /// <exception cref="ArgumentException">Thrown if the specified type doesn't fit to the characteristic's type</exception>
    public static bool setValue<T>(IDataFile dataFile, A2LCHARACTERISTIC characteristic, T value) where T : ICharValue
    {
      // get memory segment
      var segment = dataFile.findMemSeg(characteristic.Address);
      if (segment == null)
        return false;

      uint address = characteristic.Address;
      int offset = (int)(characteristic.Address - segment.Address);
      int originalOffset = offset;

      var cm = value.ValueFormat == ValueObjectFormat.Physical
        ? characteristic.RefCompuMethod
        : A2LCOMPU_METHOD.mDefaultCompuMethod;
      var layout = characteristic.RefRecordLayout;
      var bo = characteristic.ByteOrder;
      var bm = characteristic.Bitmask;
      var dt = layout.FncValues.DataType;

      switch (characteristic.CharType)
      {
        case CHAR_TYPE.VALUE:
          if (!(value is SingleValue))
            throw new ArgumentException();
          double dblVvalue2Write = (double)value.Value;
          double rawValue = cm.toRaw(dt, dblVvalue2Write);
          setRawValueToBuffer(rawValue, segment, offset, dt, bo, bm);
          break;

        case CHAR_TYPE.ASCII:
          if (!(value is ASCIIValue))
            throw new ArgumentException();
          string strValue2Write = ((string)value.Value);
          Encoding encoding;
          switch (characteristic.Encoding)
          {
            case EncodingType.UTF8: encoding = Encoding.UTF8; break;
            case EncodingType.UTF16: encoding = Encoding.Unicode; break;
            case EncodingType.UTF32: encoding = Encoding.UTF32; break;
            default: encoding = Encoding.ASCII; break;
          }

          var maxBytes = characteristic.getNumberOfElements();
          var bytes = new List<byte>(encoding.GetBytes(strValue2Write.PadRight(maxBytes)));

          if (bytes.Count > maxBytes)
            bytes.RemoveRange(maxBytes, bytes.Count - maxBytes);

          segment.setDataBytes((uint)address, bytes.ToArray());
          break;

        case CHAR_TYPE.VAL_BLK:
          if (!(value is ValBlkValue))
            throw new ArgumentException();
          var values = (double[])value.Value;
          writeValues(characteristic.getNumberOfElements(), segment
            , layout.getAlignment(dt), dt, bo, bm, cm
            , ref offset, ref values
            );
          break;
      }

      bool result = true;
      var changedNodes = new HashSet<A2LRECORD_LAYOUT_REF>(new[] { characteristic });
      if (characteristic.CharType >= CHAR_TYPE.CURVE)
      {
        var layoutEntries = layout.LayoutEntries;
        var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>(false);
        var axisCMs = new List<A2LCOMPU_METHOD>();
        foreach (var axisDesc in axisDescs)
          axisCMs.Add(value.ValueFormat == ValueObjectFormat.Physical
            ? axisDesc.RefCompuMethod
            : A2LCOMPU_METHOD.mDefaultCompuMethod);


        foreach (var itemLayout in layout.LayoutEntries)
        {
          int axisIdx = itemLayout.AxisIdx;
          dt = itemLayout.DataType;
          var alignment = layout.getAlignment(dt);
          var axisPoints = value.AxisValue;
          A2LAXIS_DESCR axisDesc = axisIdx >= 0 ? axisDescs[axisIdx] : null;

          var fncValues = itemLayout as A2LRECORD_LAYOUT.FncValuesLayoutDesc;
          if (fncValues != null)
          {
            switch (axisDescs.Count)
            {
              case 1:
                var vCurve = (double[])value.Value;
                readOrWriteFncValues(true, segment, bo, bm, layout, cm, axisCMs, ref offset, ref axisPoints, ref vCurve);
                break;
              case 2:
                var vMap = (double[,])value.Value;
                readOrWriteFncValues(true, segment, bo, bm, layout, cm, axisCMs, ref offset, ref axisPoints, ref vMap);
                break;
              case 3:
                var vCuboid = (double[,,])value.Value;
                readOrWriteFncValues(true, segment, bo, bm, layout, cm, ref offset, ref vCuboid);
                break;
              case 4:
                var vCube4 = (double[,,,])value.Value;
                readOrWriteFncValues(true, segment, bo, bm, layout, cm, ref offset, ref vCube4);
                break;
              case 5:
                var vCube5 = (double[,,,,])value.Value;
                readOrWriteFncValues(true, segment, bo, bm, layout, cm, ref offset, ref vCube5);
                break;
            }
            continue;
          }
          //var axisRescalePts = itemLayout as A2LRECORD_LAYOUT.AxisRescaleLayoutDesc;
          //if (axisRescalePts != null && axisRescalePts.AxisIdx < axisDescs.Count)
          //{
          //  switch (axisRescalePts.AdressType)
          //  {
          //    case ADDR_TYPE.DIRECT:
          //      break;
          //  }
          //  continue;
          //}

          var axisPts = itemLayout as A2LRECORD_LAYOUT.AxisPtsLayoutDesc;
          if (axisPts != null)
          {
            if (axisPts == layout.AxisPts[axisIdx])
            {
              switch (axisPts.AdressType)
              {
                case ADDR_TYPE.DIRECT:
                  var axisValues = axisPoints[axisIdx];
                  var noAxisValues = Math.Min(axisDesc.MaxAxisPoints, axisValues.Length);
                  writeValues(noAxisValues, segment, alignment, dt, bo, UInt64.MaxValue, axisPts.IndexOrder, axisDesc.Deposit, axisCMs[axisIdx], ref offset, ref axisValues);
                  break;
              }
            }
            else
              offset = A2LRECORD_LAYOUT_REF.getOffsetToPosition(itemLayout.Position + 1, layout, axisDescs, 0);
            continue;
          }

          var noAxisPts = itemLayout as A2LRECORD_LAYOUT.NoAxisPtsLayoutDesc;
          if (noAxisPts != null)
          {
            if (noAxisPts == layout.NoAxisPts[axisIdx])
            {
              var axisValues = axisPoints[axisIdx];
              var noAxisValues = new double[] { Math.Min(axisDescs[axisIdx].MaxAxisPoints, axisValues.Length) };
              writeValues(1, segment, alignment, dt, bo, UInt64.MaxValue, A2LCOMPU_METHOD.mDefaultCompuMethod, ref offset, ref noAxisValues);
            }
            else
              offset = A2LRECORD_LAYOUT_REF.getOffsetToPosition(itemLayout.Position + 1, layout, axisDescs, 0);
            continue;
          }
        }

        if (axisDescs != null)
        { // write potential COMAxis values
          var axisValues = value.AxisValue;
          for (int i = 0; i < axisDescs.Count; ++i)
          { // iterate each axis
            switch (axisDescs[i].AxisType)
            {
              case AXIS_TYPE.COM_AXIS:
                var axisPts = axisDescs[i].RefAxisPtsRef;
                if (axisPts == null || null == (segment = dataFile.findMemSeg(axisPts.Address)))
                  // ref not found or segment not found
                  result = false;
                else
                { // set axis values to referenced AxisPts
                  setCOMAxisValues(segment, axisPts, axisValues[i], value.ValueFormat);
                  changedNodes.Add(axisPts);
                  var referendedNodes = axisPts.getRefNodes();
                  foreach (var refNode in referendedNodes)
                    if (refNode is A2LCHARACTERISTIC)
                      changedNodes.Add((A2LRECORD_LAYOUT_REF)refNode);
                }
                break;
            }
          }
        }
      }

      if (segment.DataFile != null)
        foreach (var changedNode in changedNodes)
          ((DataFile)segment.DataFile).changedValue(changedNode);

      return result;
    }
    /// <summary>
    /// Creates characteristic values according the specified characteristic from the given data file.
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="recordLayoutRef">The corresponding characteristic to get the value for</param>
    /// <param name="format">The value format to create</param>
    /// <returns>The ICharValue instance</returns>
    public static ICharValue createValue(IDataFile dataFile, A2LRECORD_LAYOUT_REF recordLayoutRef, ValueObjectFormat format)
    {
      var characteristic = recordLayoutRef as A2LCHARACTERISTIC;
      if (null == characteristic)
        // A2LAXIS_PTS instance
        return getValue<ValBlkValue>(dataFile, (A2LAXIS_PTS)recordLayoutRef, format);
      else
        // A2LCHARACTERISTIC instance
        switch (characteristic.CharType)
        {
          case CHAR_TYPE.VALUE: return getValue<SingleValue>(dataFile, characteristic, format);
          case CHAR_TYPE.ASCII: return getValue<ASCIIValue>(dataFile, characteristic, format);
          case CHAR_TYPE.VAL_BLK: return getValue<ValBlkValue>(dataFile, characteristic, format);
          case CHAR_TYPE.CURVE: return getValue<CurveValue>(dataFile, characteristic, format);
          case CHAR_TYPE.MAP: return getValue<MapValue>(dataFile, characteristic, format);
          case CHAR_TYPE.CUBOID: return getValue<CuboidValue>(dataFile, characteristic, format);
          case CHAR_TYPE.CUBE_4: return getValue<Cube4Value>(dataFile, characteristic, format);
          case CHAR_TYPE.CUBE_5: return getValue<Cube5Value>(dataFile, characteristic, format);
          default: throw new NotSupportedException(characteristic.CharType.ToString());
        }
    }
    /// <summary>
    ///  Write the specified value in to the data file.
    /// </summary>
    /// <param name="dataFile">The data file to write the value to</param>
    /// <param name="value">The value to write</param>
    public static void setValue(IDataFile dataFile, ICharValue value)
    {
      if (value.Characteristic is A2LAXIS_PTS)
        // A2LAXIS_PTS instance
        setValue(dataFile, (A2LAXIS_PTS)value.Characteristic, value);
      else
        // A2LCHARACTERISTIC instance
        setValue(dataFile, (A2LCHARACTERISTIC)value.Characteristic, value);
    }
    /// <summary>
    /// Modifies the value by simply setting a new value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="value">The new value to modify the original value with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <returns>The resulting value</returns>
    public static double modifySetFunc(double originalValue, double value, bool treatValueAsPercent)
    {
      return treatValueAsPercent
        ? originalValue * (value / 100.0)
        : value;
    }
    /// <summary>
    /// Modifies the value by adding a value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="value">The new value to modify the original value with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <returns>The resulting value</returns>
    public static double modifyAddFunc(double originalValue, double value, bool treatValueAsPercent)
    {
      return treatValueAsPercent
        ? originalValue + originalValue * (value / 100.0)
        : originalValue + value;
    }
    /// <summary>
    /// Modifies the value by subtracting a value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="value">The new value to modify the original value with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <returns>The resulting value</returns>
    public static double modifySubFunc(double originalValue, double value, bool treatValueAsPercent)
    {
      return treatValueAsPercent
        ? originalValue - originalValue * (value / 100.0)
        : originalValue - value;
    }
    /// <summary>
    /// Modifies the value by multiplying with a value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="value">The new value to modify the original value with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <returns>The resulting value</returns>
    public static double modifyMulFunc(double originalValue, double value, bool treatValueAsPercent)
    {
      return treatValueAsPercent
        ? originalValue * originalValue * (value / 100.0)
        : originalValue * value;
    }
    /// <summary>
    /// Modifies the value by dividing with a value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="value">The new value to modify the original value with</param>
    /// <param name="treatValueAsPercent">true, if the value should be treated as percentage</param>
    /// <returns>The resulting value</returns>
    public static double modifyDivFunc(double originalValue, double value, bool treatValueAsPercent)
    {
      return treatValueAsPercent
        ? originalValue / originalValue * (value / 100.0)
        : originalValue / value;
    }
    /// <summary>
    /// Inverts the original value.
    /// </summary>
    /// <param name="originalValue">The original value</param>
    /// <param name="layoutRef">The corresponding characteristic</param>
    /// <returns>The resulting value</returns>
    public static double modifyInvFunc(double originalValue, A2LRECORD_LAYOUT_REF layoutRef)
    {
      double upperDiff = layoutRef.UpperLimit - originalValue;
      if (upperDiff >= 0)
        return layoutRef.LowerLimit + upperDiff;
      return layoutRef.UpperLimit - (originalValue - layoutRef.LowerLimit);
    }
    #endregion
    #endregion
  }
}
