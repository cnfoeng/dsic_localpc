/*!
 * @file    
 * @brief   Class for reading and writing Motorola S files.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Possible Motorola file format types.
  /// </summary>
  public enum MotorolaFileType
  {
    /// <summary>
    /// Not set (write format as read from source file).
    /// </summary>
    [Description("Use original file format")]
    NotSet = 0,
    /// <summary>
    /// Record address type is 4 bytes.
    /// </summary>
    [Description("S3 Records (4 byte addresses)")]
    _4Byte = 3,
    /// <summary>
    /// Record address type is 3 bytes.
    /// </summary>
    [Description("S2 Records (3 byte addresses)")]
    _3Byte = 2,
    /// <summary>
    /// Record address type is 2 bytes.
    /// </summary>
    [Description("S1 Records (2 byte addresses)")]
    _2Byte = 1,
  }
  /// <summary>
  /// Class for reading/writing Motorola S files.
  /// 
  /// - This class gets constructed by the static DataFile.open method.
  /// </summary>
  public sealed class DataFileS19 : DataFile, IDataFile
  {
    #region Record definition
    /// <summary>
    /// Motorola S file record types.
    /// </summary>
    enum RecordType
    {
      /// <summary>
      /// Block header.
      /// 
      /// The S0 record data sequence contains vendor specific 
      /// data rather than program data. String with file name 
      /// and possibly version info.
      /// </summary>
      Header,
      /// <summary>
      /// Data sequence (2 address bytes)
      /// 
      /// Data sequence, depending on size of address needed. 
      /// A 16-bit/64K system uses Data2, 24-bit address uses Data3 
      /// and full 32-bit uses Data4.
      /// </summary>
      Data2,
      /// <summary>
      /// Data sequence (3 address bytes)
      /// 
      /// Data sequence, depending on size of address needed. 
      /// A 16-bit/64K system uses Data2, 24-bit address uses Data3 
      /// and full 32-bit uses Data4.
      /// </summary>
      Data3,
      /// <summary>
      /// Data sequence (4 address bytes)
      /// 
      /// Data sequence, depending on size of address needed. 
      /// A 16-bit/64K system uses Data2, 24-bit address uses Data3 
      /// and full 32-bit uses Data4.
      /// </summary>
      Data4,
      /// <summary>
      /// Not used...
      /// </summary>
      Unused1,
      /// <summary>
      /// Record count.
      /// 
      /// Count of S1, S2 and S3 records previously appearing in the 
      /// file or transmission. The record count is stored in the 2-byte 
      /// address field. There is no data associated with this record type.
      /// </summary>
      Count,
      /// <summary>
      /// Not used...
      /// </summary>
      Unused2,
      /// <summary>
      /// End of block (Data4).
      /// 
      /// The address field of the Eob2, Eob3, or Eob4 records may contain 
      /// a starting address for the program.
      /// </summary>
      Eob4,
      /// <summary>
      /// End of block (Data3).
      /// 
      /// The address field of the Eob2, Eob3, or Eob4 records may contain 
      /// a starting address for the program.
      /// </summary>
      Eob3,
      /// <summary>
      /// End of block (Data2).
      /// 
      /// The address field of the Eob2, Eob3, or Eob4 records may contain 
      /// a starting address for the program.
      /// </summary>
      Eob2,
    }
    #endregion
    #region members
    string mHeader = "S0030000FC";
    byte mDataBytesPerLine = 32;
    MotorolaFileType mFileType = MotorolaFileType._4Byte;
    #endregion
    #region Constructor
    /// <summary>
    /// Construct a new instance of the S19 data file class.
    /// 
    /// Initialize the memory segment list with the
    /// specified segments.
    /// </summary>
    /// <param name="filename">The fully qualified path to the source filename.</param>
    /// <param name="segments">Segments to initialize with, may be null or empty.</param>
    public DataFileS19(string filename, MemorySegmentList segments)
      : base(filename, segments)
    {
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets or set the file format.
    /// 
    /// The value will be initialized if the file is read from the filesystem.
    /// </summary>
    [DefaultValue(MotorolaFileType._4Byte)]
    public MotorolaFileType WriteFileType
    {
      get { return mFileType; }
      set { mFileType = value; }
    }
    HashSet<A2LRECORD_LAYOUT_REF> IDataFile.ChangedValues { get { return base.ChangedValues; } }
    #endregion
    #region methods
    MemorySegment IDataFile.findMemSeg(UInt32 address) { return findMemSeg(address, 1); }
    bool IDataFile.IsDirty
    {
      get { return base.IsDirty; }
      set { base.IsDirty = value; }
    }
    string IDataFile.SourceFilename { get { return base.SourceFilename; } }
    bool IDataFile.checkEPK(UInt32 epkAddress, string expectedEPK) { return EPKCheck(epkAddress, expectedEPK); }
    bool IDataFile.setEPK(UInt32 epkAddress, string epkToSet) { return base.setEPK(epkAddress, epkToSet); }
    bool IDataFile.save(string fileName, bool onlyDataSegments, byte dataBytesPerLine)
    {
      if (dataBytesPerLine < 1)
        throw new ArgumentException("Value too small", "dataBytesPerLine");

      if (mSegments == null || mSegments.Count == 0)
        // no data to save
        return true;
      bool turnAround = BitConverter.IsLittleEndian;

      // Set the address type to write
      RecordType addressType = mFileType == MotorolaFileType.NotSet
        ? RecordType.Data4
        : (RecordType)mFileType;

      using (var sw = new StreamWriter(fileName))
      {
        sw.WriteLine(mHeader);
        for (int i = 0; i < mSegments.Count; ++i)
        {
          var segment = mSegments[i];
          if (!segment.IsInitialized)
            continue;
          if (onlyDataSegments && !segment.IsDataSegment)
            continue;

          var address = segment.Address;
          UInt16 blockCount = 0;
          for (int j = 0, dataLines = 0; j < segment.Size; j += dataBytesPerLine, address += dataBytesPerLine, ++dataLines)
          {
            int len = Math.Min(segment.Size - j, dataBytesPerLine);
            var buffer = getData2Write(turnAround, addressType, (uint)address, segment.Data, len, (int)(address - segment.Address));
            writeBytes(sw, ref buffer);
            ++blockCount;
          }
          if (turnAround)
            blockCount = Extensions.changeEndianess(blockCount);

#if WRITE_S5_RECORD
          var cntBuffer = new byte[] { (byte)RecordType.Count, 3
            , (byte)(blockCount & 0xff)
            , (byte)((blockCount & 0xff00)>>8)
            };
          writeBytes(sw, ref cntBuffer);
#endif
        }

        // build and write the termination S7/8/9 record
        var eofBuffer = new List<byte>();
        switch (addressType)
        {
          case RecordType.Data2:
            eofBuffer.Add(9);
            eofBuffer.Add(3);
            for (int j = 0; j < 2; ++j)
              eofBuffer.Add((byte)0);
            break;
          case RecordType.Data3:
            eofBuffer.Add(8);
            eofBuffer.Add(4);
            for (int j = 0; j < 3; ++j)
              eofBuffer.Add((byte)0);
            break;
          case RecordType.Data4:
            eofBuffer.Add(7);
            eofBuffer.Add(5);
            for (int j = 0; j < 4; ++j)
              eofBuffer.Add((byte)0);
            break;
        }
        var eofArr = eofBuffer.ToArray();
        writeBytes(sw, ref eofArr);
      }
      IsDirty = false;
      ChangedValues.Clear();
      SourceFilename = fileName;
      return true;
    }
    bool IDataFile.save(string fileName, bool onlyDataSegments)
    {
      return ((IDataFile)this).save(fileName, onlyDataSegments, mDataBytesPerLine);
    }
    void IDataFile.import(ICharValue[] values) { DataFile.import(this, values); }
    bool IDataFile.reLoad() { return load(SourceFilename); }
    void IDataFile.setSegmentsData(MemorySegmentList segments) { base.setSegmentsData(segments); }
    /// <summary>
    /// Write a Motorola S data line.
    /// </summary>
    /// <param name="sw">The stream to write to</param>
    /// <param name="buffer">The data to write</param>
    static void writeBytes(StreamWriter sw, ref byte[] buffer)
    {
      int chksum = 0;
      sw.Write("S{0}", buffer[0]);
      for (int i = 1; i < buffer.Length; ++i)
      {
        byte b = buffer[i];
        chksum += b;
        sw.Write(mStrByteFormat, b);
      }
      sw.WriteLine(mStrByteFormat, (byte)(~chksum & 0xff));
    }
#if WIN32_OPTIMIZED
    /// <summary>
    /// Builds a Intel HEX data line from the specified arguments.
    /// </summary>
    /// <param name="adjustByteOrder">true if byte order should adjusted (for big endian host systems)</param>
    /// <param name="addressType">RecordType.Data2 or RecordType.Data3 or RecordType.Data4 (2, 3 or 4 byte address length)</param>
    /// <param name="address">The data's address</param>
    /// <param name="data">The data</param>
    /// <param name="len">The data length</param>
    /// <param name="offset">The offset into the data</param>
    /// <returns>A new instance of a byte buffer</returns>
    static byte[] getData2Write(bool adjustByteOrder, RecordType addressType, UInt32 address, IntPtr data, int len, int offset)
    {
      byte addressLenVal = (byte)((int)addressType + 1);
      int dataLen = 2 + addressLenVal + len;
      var buffer = new byte[dataLen];
      buffer[0] = (byte)addressType;
      buffer[1] = (byte)(1 + addressLenVal + len);

      if (addressType == RecordType.Data2)
      {
        UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess(address) : address;
        buffer[2] = (byte)(wrAddress & 0xff);
        buffer[3] = (byte)((wrAddress & 0xff00) >> 8);
      }
      else if (addressType == RecordType.Data3)
      {
        UInt16 loWord = adjustByteOrder ? Extensions.changeEndianess((UInt16)address) : (UInt16)address;
        UInt16 hiWord = (UInt16)(adjustByteOrder ? Extensions.changeEndianess((UInt16)(address >> 16)) : (UInt16)(address >> 16));
        buffer[2] = (byte)((hiWord & 0xff00) >> 8);
        buffer[3] = (byte)(loWord & 0xff);
        buffer[4] = (byte)((loWord & 0xff00) >> 8);
      }
      else if (addressType == RecordType.Data4)
      {
        UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess(address) : address;
        buffer[2] = (byte)(wrAddress & 0xff);
        buffer[3] = (byte)((wrAddress & 0xff00) >> 8);
        buffer[4] = (byte)((wrAddress & 0xff0000) >> 16);
        buffer[5] = (byte)((wrAddress & 0xff000000) >> 24);
      }
      Marshal.Copy(IntPtr.Add(data, offset), buffer, 2 + addressLenVal, len);
      return buffer;
    }
#else
    /// <summary>
    /// Builds a Intel HEX data line from the specified arguments.
    /// </summary>
    /// <param name="adjustByteOrder">true if byte order should adjusted (for big endian host systems)</param>
    /// <param name="addressType">RecordType.Data2 or RecordType.Data3 or RecordType.Data4 (2, 3 or 4 byte address length)</param>
    /// <param name="address">The data's address</param>
    /// <param name="data">The data</param>
    /// <param name="len">The data length</param>
    /// <param name="offset">The offset into the data</param>
    /// <returns>A new instance of a byte buffer</returns>
    static byte[] getData2Write(bool adjustByteOrder, RecordType addressType, UInt32 address, byte[] data, int len, int offset)
    {
      byte addressLenVal = (byte)((int)addressType + 1);
      int dataLen = 2 + addressLenVal + len;
      var buffer = new byte[dataLen];
      buffer[0] = (byte)addressType;
      buffer[1] = (byte)(1 + addressLenVal + len);

      if (addressType == RecordType.Data2)
      {
        UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess(address) : address;
        buffer[2] = (byte)(wrAddress & 0xff);
        buffer[3] = (byte)((wrAddress & 0xff00) >> 8);
      }
      else if (addressType == RecordType.Data3)
      {
        UInt16 loWord = adjustByteOrder ? Extensions.changeEndianess((UInt16)address) : (UInt16)address;
        UInt16 hiWord = (UInt16)(adjustByteOrder ? Extensions.changeEndianess((UInt16)(address >> 16)) : (UInt16)(address >> 16));
        buffer[2] = (byte)((hiWord & 0xff00) >> 8);
        buffer[3] = (byte)(loWord & 0xff);
        buffer[4] = (byte)((loWord & 0xff00) >> 8);
      }
      else if (addressType == RecordType.Data4)
      {
        UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess(address) : address;
        buffer[2] = (byte)(wrAddress & 0xff);
        buffer[3] = (byte)((wrAddress & 0xff00) >> 8);
        buffer[4] = (byte)((wrAddress & 0xff0000) >> 16);
        buffer[5] = (byte)((wrAddress & 0xff000000) >> 24);
      }
      Array.Copy(data, offset, buffer, 2 + addressLenVal, len);
      return buffer;
    }
#endif
    /// <summary>
    /// Load the contents of an S19 File into memory.
    /// </summary>
    /// <param name="fileName">Fully qualified path to the Intel Hex File to read.</param>
    /// <returns>true if successful.</returns>
    protected override bool load(string fileName)
    {
      using (var reader = new StreamReader(fileName))
      { // iterate over lines.
        try
        {
          var partList = new List<DataPart>();
          string line;
          while (null != (line = reader.ReadLine()))
          { // Convert line into data
            line = line.Trim(mLineTrim);

            if (string.IsNullOrEmpty(line) || char.ToUpper(line[0]) != 'S')
              // not a record
              continue;

            RecordType recordType;
            try
            {
              recordType = (RecordType)(line[1] - '0');
            }
            catch
            { // invalid record
              continue;
            }

            // Convert text into record.
            switch (recordType)
            {
              case RecordType.Header:
                mHeader = line;
                continue;
              default:
                continue;
              case RecordType.Data2:
              case RecordType.Data3:
              case RecordType.Data4:
                break;
            }

            int length = toByte(ref line, 2);
            int offset = 4;
            int expLength = offset + 2 * length;
            if (expLength != line.Length)
              // bad record length
              continue;

            int chksum = length;

            UInt32 address = 0;
            byte[] data = null;
            switch (recordType)
            {
              case RecordType.Data2:
                byte b1 = toByte(ref line, offset);
                byte b2 = toByte(ref line, offset + 2);
                address = ((UInt32)(b1) << 8) + (UInt32)(b2);
                data = new byte[length - 3];
                offset += 4;
                chksum += b1;
                chksum += b2;
                mFileType = MotorolaFileType._2Byte;
                break;
              case RecordType.Data3:
                b1 = toByte(ref line, offset);
                b2 = toByte(ref line, offset + 2);
                byte b3 = toByte(ref line, offset + 4);
                address = ((UInt32)(b1) << 16) + ((UInt32)(b2) << 8) + (UInt32)(b3);
                data = new byte[length - 4];
                offset += 6;
                chksum += b1;
                chksum += b2;
                chksum += b3;
                mFileType = MotorolaFileType._3Byte;
                break;
              case RecordType.Data4:
                b1 = toByte(ref line, offset);
                b2 = toByte(ref line, offset + 2);
                b3 = toByte(ref line, offset + 4);
                byte b4 = toByte(ref line, offset + 6);
                address = ((UInt32)(b1) << 24) + ((UInt32)(b2) << 16) + ((UInt32)(b3) << 8) + (UInt32)(b4);
                data = new byte[length - 5];
                offset += 8;
                chksum += b1;
                chksum += b2;
                chksum += b3;
                chksum += b4;
                mFileType = MotorolaFileType._4Byte;
                break;
            }

            int max = offset + 2 * data.Length;
            for (int i = offset, j = 0; i < max; i += 2, ++j)
            {
              byte b = toByte(ref line, i);
              data[j] = b;
              chksum += b;
            }
            chksum = (byte)(~chksum & 0xff);
            if (chksum != toByte(ref line, max))
              // checksum failed
              return false;

            partList.Add(new DataPart(address, ref data));
          }
          partList.Sort(compare);
          createAndMergeMemSegs(ref partList);
          return partList.Count > 0;
        }
        catch { return false; }
      }
    }
    #endregion
    #region clone method
    /// <summary>
    /// Implements the IClone member.
    /// </summary>
    /// <returns>New instance of file as copy of the original.</returns>
    public override object Clone()
    {
      var s19File = new DataFileS19(SourceFilename, mSegments);
      s19File.mHeader = mHeader;
      s19File.mFileType = mFileType;
      return s19File;
    }
    #endregion
  }
}
