﻿/*!
 * @file    
 * @brief   Class for reading and writing binary data files.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Class for reading/writing binary data files.
  /// 
  /// - This class gets constructed by the static DataFile.open method.
  /// </summary>
  public sealed class DataFileBin : DataFile, IDataFile
  {
    #region Constructor
    /// <summary>
    /// Constructs a new instance of this class.
    /// 
    /// Initialize the memory segment list with the
    /// specified segments.
    /// </summary>
    /// <param name="filename">The fully qualified path to the source filename.</param>
    /// <param name="segments">Segments to initialize with, may be null or empty.</param>
    public DataFileBin(string filename, MemorySegmentList segments)
      : base(filename, segments)
    {
    }
    #endregion
    #region properties
    HashSet<A2LRECORD_LAYOUT_REF> IDataFile.ChangedValues { get { return base.ChangedValues; } }
    #endregion
    #region methods
    MemorySegment IDataFile.findMemSeg(UInt32 address) { return findMemSeg(address, 1); }
    bool IDataFile.IsDirty
    {
      get { return base.IsDirty; }
      set { base.IsDirty = value; }
    }
    string IDataFile.SourceFilename { get { return base.SourceFilename; } }
    bool IDataFile.checkEPK(UInt32 epkAddress, string expectedEPK) { return EPKCheck(epkAddress, expectedEPK); }
    bool IDataFile.setEPK(UInt32 epkAddress, string epkToSet) { return base.setEPK(epkAddress, epkToSet); }
    bool IDataFile.save(string fileName, bool onlyDataSegments, byte dataBytesPerLine)
    {
      SegmentList.Sort(MemorySegmentList.sortSegmentsByAddress);
      using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
      {
        using (var bw = new BinaryWriter(fs))
        {
          bool inWriting = false;
          for (int i = 0; i < SegmentList.Count; ++i)
          {
            var seg = SegmentList[i];
            if (!seg.IsInitialized)
            { // segment is not initialized
              inWriting = false;
              continue;
            }
            if (inWriting && i > 0 && seg.Address != SegmentList[i - 1].Address + SegmentList[i - 1].Size)
            { // Gap between the written segment and the current segment
              inWriting = false;
              continue;
            }

            // Write segment
            inWriting = true;
#if WIN32_OPTIMIZED
            var buffer = new byte[seg.Size];
            Marshal.Copy(seg.Data, buffer, 0, seg.Size);
            bw.Write(buffer);
#else
            bw.Write(seg.Data);
#endif
          }
        }
      }
      return true;
    }
    bool IDataFile.save(string fileName, bool onlyDataSegments)
    {
      return ((IDataFile)this).save(fileName, onlyDataSegments, 0);
    }
    void IDataFile.import(ICharValue[] values) { DataFile.import(this, values); }
    bool IDataFile.reLoad() { return load(SourceFilename); }
    void IDataFile.setSegmentsData(MemorySegmentList segments) { base.setSegmentsData(segments); }
    /// <summary>
    /// Load the contents of an Intel Hex File into memory.
    /// </summary>
    /// <param name="fileName">Fully qualified path to the Intel Hex File to read.</param>
    /// <returns>true if successful.</returns>
    protected override bool load(string fileName)
    {
      if (mSegments == null || mSegments.Count == 0)
        return false;

      using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        try
        {
          var data = new byte[fs.Length];
          fs.Read(data, 0, data.Length);

          UInt32 address = 0;
          using (var dlg = new DataFileBinDlg(mSegments))
          {
            if (DialogResult.OK != dlg.ShowDialog())
              return false;
            address = dlg.Address;
          }
          var partList = new List<DataPart>(new DataPart[] { new DataPart(address, ref data) });
          createAndMergeMemSegs(ref partList);
          return partList.Count > 0;
        }
        catch { return false; }
      }
    }
    #endregion
    #region clone members
    /// <summary>
    /// Provides implementation of the IClone member.
    /// </summary>
    /// <returns>New instance of class as a copy of the original.</returns>
    public override object Clone()
    {
      return new DataFileBin(SourceFilename, mSegments);
    }
    #endregion
  }
}
