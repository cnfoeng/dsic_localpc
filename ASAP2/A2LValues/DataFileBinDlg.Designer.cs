﻿namespace jnsoft.ASAP2.Values
{
  partial class DataFileBinDlg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbAddressSelector = new System.Windows.Forms.GroupBox();
      this.cbAddress = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.btnOk = new System.Windows.Forms.Button();
      this.gbAddressSelector.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbAddressSelector
      // 
      this.gbAddressSelector.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gbAddressSelector.Controls.Add(this.cbAddress);
      this.gbAddressSelector.Controls.Add(this.label1);
      this.gbAddressSelector.Location = new System.Drawing.Point(12, 12);
      this.gbAddressSelector.Name = "gbAddressSelector";
      this.gbAddressSelector.Padding = new System.Windows.Forms.Padding(10, 6, 3, 3);
      this.gbAddressSelector.Size = new System.Drawing.Size(283, 56);
      this.gbAddressSelector.TabIndex = 0;
      this.gbAddressSelector.TabStop = false;
      this.gbAddressSelector.Text = "Address selector";
      // 
      // cbAddress
      // 
      this.cbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.cbAddress.FormattingEnabled = true;
      this.cbAddress.Location = new System.Drawing.Point(117, 22);
      this.cbAddress.MaxLength = 10;
      this.cbAddress.Name = "cbAddress";
      this.cbAddress.Size = new System.Drawing.Size(152, 21);
      this.cbAddress.TabIndex = 1;
      this.cbAddress.TextUpdate += new System.EventHandler(this.cbAddress_TextUpdate);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 25);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(98, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Start address (hex):";
      // 
      // btnOk
      // 
      this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(212, 81);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(83, 28);
      this.btnOk.TabIndex = 1;
      this.btnOk.Text = "Ok";
      this.btnOk.UseVisualStyleBackColor = true;
      // 
      // DataFileBinDlg
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnOk;
      this.ClientSize = new System.Drawing.Size(307, 121);
      this.Controls.Add(this.btnOk);
      this.Controls.Add(this.gbAddressSelector);
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(323, 159);
      this.Name = "DataFileBinDlg";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Select a start address...";
      this.gbAddressSelector.ResumeLayout(false);
      this.gbAddressSelector.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbAddressSelector;
    private System.Windows.Forms.ComboBox cbAddress;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnOk;
  }
}