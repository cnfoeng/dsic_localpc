﻿/*!
 * @file    
 * @brief   Implements the CDF file parser.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    September 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Values.CDF;
using jnsoft.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Implements the Calibration Data File format reader/writer.
  /// </summary>
  public sealed class CDFFile : DataConservation
  {
    #region members
    static ILog mLogger = LogManager.GetLogger(typeof(CDFFile));
    const string mStrFeature = "FEATURE";
    #endregion
    #region methods
    #region interface
    /// <summary>
    /// Saves all characteristics of the module into a CDF file.
    /// </summary>
    /// <param name="a2LSource">The source A2L file (may be null or empty)</param>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="addParameterDesc">Add parameter descriptions as LONG-NAME</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(string a2LSource
      , IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      , bool addParameterDesc
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var layoutRef in module.enumChildNodes<A2LRECORD_LAYOUT_REF>())
      {
        ICharValue value = CharacteristicValue.createValue(dataFile, layoutRef, ValueObjectFormat.Physical);
        if (value == null)
          continue;
        charValues.Add(value);
      }
      var dict = ((A2LPROJECT)module.Parent).FuncDict;
      var functions = new A2LFUNCTION[dict.Count];
      dict.Values.CopyTo(functions, 0);
      return save(path, description, a2LSource, dataFile.SourceFilename, charValues.ToArray(), functions, addParameterDesc);
    }
    /// <summary>
    /// Saves the specified characteristics into a CDF file.
    /// </summary>
    /// <param name="a2LSource">The source A2L file (may be null or empty)</param>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="characteristics">The characteristics to save</param>
    /// <param name="exportFunctions">true if functions should be exported, too</param>
    /// <param name="addParameterDesc">Add parameter descriptions as LONG-NAME</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(string a2LSource
      , IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      , A2LCHARACTERISTIC[] characteristics
      , bool exportFunctions
      , bool addParameterDesc
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var characteristic in characteristics)
      {
        ICharValue value = CharacteristicValue.getValue<SingleValue>(dataFile, characteristic, ValueObjectFormat.Physical);
        if (value == null)
          continue;
        charValues.Add(value);
      }
      A2LFUNCTION[] functions = null;
      if (exportFunctions)
      {
        var dict = ((A2LPROJECT)module.Parent).FuncDict;
        functions = new A2LFUNCTION[dict.Count];
        dict.Values.CopyTo(functions, 0);
      }

      var modPar = module.getNode<A2LMODPAR>(false);
      description = string.IsNullOrEmpty(description)
        ? string.Format("EPK: {0}", modPar.EPK)
        : string.Format("EPK: {0}{1}{1}{2}", modPar.EPK, mEOL, description);
      return save(path, description, a2LSource, dataFile.SourceFilename, charValues.ToArray(), functions, addParameterDesc);
    }
    /// <summary>
    /// Saves the specified characteristics into a CDF file.
    /// </summary>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="a2LSource">The source A2L file (may be null or empty)</param>
    /// <param name="dataSource">The source data file (may be null or empty)</param>
    /// <param name="values">The values to save into the CDF file</param>
    /// <param name="functions">Functions to save into the CDF file (may be null or empty)</param>
    /// <param name="addParameterDesc">Add parameter descriptions as LONG-NAME</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(string path
      , string description
      , string a2LSource
      , string dataSource
      , ICharValue[] values
      , A2LFUNCTION[] functions
      , bool addParameterDesc
      )
    {
      try
      {
        var ass = Assembly.GetExecutingAssembly();
        var msrsw = new MSRSW("CDF", ass.ToString().Substring(ass.ToString().IndexOf(' ')), Application.ProductVersion, description);
        var spec = new SWINSTANCESPEC();

        List<SWCSCOLLECTION> funcColl = null;
        if (functions != null)
        { // add functions
          funcColl = new List<SWCSCOLLECTION>();
          foreach (var f in functions)
          {
            var func = new SWCSCOLLECTION(mStrFeature, f.Name);
            funcColl.Add(func);
          }
        }

        // add values
        var instances = new List<SWINSTANCE>();
        foreach (ICharValue value in values)
        {
          var layoutRef = value.Characteristic;
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          if (characteristic == null)
            // ignore AxisPts
            continue;

          var valueCont = new SWVALUECONT();

          if (!string.IsNullOrEmpty(value.Unit))
            valueCont.UNITDISPLAYNAME = value.Unit;

          var instance = new SWINSTANCE(layoutRef.Name, value.CharType.ToString(), valueCont);
          if (addParameterDesc && !string.IsNullOrEmpty(layoutRef.Description))
            instance.LONGNAME = layoutRef.Description;

          if (functions != null)
          { // add function reference
            var defFunc = layoutRef.getDefCharacteristicFunction();
            if (defFunc != null)
              instance.SWFEATUREREF = defFunc.Name;
          }

          var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();

          switch (value.CharType)
          {
            case CHAR_TYPE.VALUE:
              if (value.Value is double && double.IsNaN((double)value.Value))
                // filter out NaN values
                continue;
              switch (layoutRef.RefCompuMethod.ConversionType)
              {
                case CONVERSION_TYPE.TAB_VERB:
                  valueCont.Vs = new object[] { layoutRef.toStringValue((double)value.Value) };
                  break;
                default: valueCont.Vs = new object[] { value.Value }; break;
              }
              break;

            case CHAR_TYPE.ASCII:
              valueCont.SWARRAYSIZE = new int[] { (int)characteristic.getNumberOfElements() };
              valueCont.Vs = new object[] { toXMLString((string)value.Value) };
              break;

            case CHAR_TYPE.VAL_BLK:
              if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                // filter out NaN values
                continue;
              var noElems = (int)characteristic.getNumberOfElements();
              valueCont.SWARRAYSIZE = new int[] { noElems };
              var dstValues = new object[noElems];
              for (int i = 0; i < noElems; ++i)
              {
                double srcVvalue = ((double[])value.Value)[i];
                switch (layoutRef.RefCompuMethod.ConversionType)
                {
                  case CONVERSION_TYPE.TAB_VERB:
                    dstValues[i] = layoutRef.toStringValue(srcVvalue);
                    break;
                  default: dstValues[i] = srcVvalue; break;
                }
              }
              valueCont.Vs = dstValues;
              break;

            case CHAR_TYPE.CURVE:
              if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                // filter out NaN values
                continue;
              var srcValues = (double[])value.Value;
              dstValues = new object[srcValues.Length];
              for (int i = 0; i < srcValues.Length; ++i)
              {
                double srcVvalue = srcValues[i];
                switch (layoutRef.RefCompuMethod.ConversionType)
                {
                  case CONVERSION_TYPE.TAB_VERB:
                    dstValues[i] = layoutRef.toStringValue(srcVvalue);
                    break;
                  default: dstValues[i] = srcVvalue; break;
                }
              }
              valueCont.Vs = dstValues;

              // add axis
              var compuMethodAxis = axisDescs[0].RefCompuMethod;
              var srcAxisValues = value.AxisValue[0];
              var dstAxisValues = new object[srcAxisValues.Length];
              for (int i = 0; i < srcAxisValues.Length; ++i)
              {
                dstAxisValues[i] = compuMethodAxis.ConversionType == CONVERSION_TYPE.TAB_VERB
                  ? value.toSingleValue(i, -1, 0)
                  : (object)srcAxisValues[i];
              }
              var axisCont = new SWAXISCONT();
              axisCont.CATEGORY = characteristic.getNode<A2LAXIS_DESCR>(false).AxisType.ToString();
              if (!string.IsNullOrEmpty(value.UnitAxis[0]))
                axisCont.UNITDISPLAYNAME = value.UnitAxis[0];
              axisCont.Vs = dstAxisValues;
              instance.SWAXISCONTS = new SWAXISCONT[] { axisCont };
              break;

            case CHAR_TYPE.MAP:
              if (value.Value is double[,] && double.IsNaN(((double[,])value.Value)[0, 0]))
                // filter out NaN values
                continue;
              // fnc values
              var srcMapVvalues = (double[,])value.Value;
              var vgs = new List<VG>();
              for (int j = 0; j < srcMapVvalues.GetLength(1); ++j)
              {
                dstValues = new object[srcMapVvalues.GetLength(0)];
                for (int i = 0; i < dstValues.Length; ++i)
                {
                  double srcVvalue = srcMapVvalues[i, j];
                  switch (layoutRef.RefCompuMethod.ConversionType)
                  {
                    case CONVERSION_TYPE.TAB_VERB:
                      dstValues[i] = layoutRef.toStringValue(srcVvalue);
                      break;
                    default: dstValues[i] = srcVvalue; break;
                  }
                }
                vgs.Add(new VG(value.AxisValue[1][j].ToString(), dstValues));
              }
              valueCont.Vs = vgs.ToArray();

              // Axis
              var axisConts = new List<SWAXISCONT>();
              for (int j = 0; j < 2; ++j)
              {
                compuMethodAxis = axisDescs[j].RefCompuMethod;
                srcAxisValues = value.AxisValue[j];
                dstAxisValues = new object[srcAxisValues.Length];
                for (int i = 0; i < srcAxisValues.Length; ++i)
                {
                  dstAxisValues[i] = compuMethodAxis.ConversionType == CONVERSION_TYPE.TAB_VERB
                    ? value.toSingleValue(i, -1, 0)
                    : (object)srcAxisValues[i];
                }
                axisCont = new SWAXISCONT();
                axisCont.CATEGORY = characteristic.getNode<A2LAXIS_DESCR>(false).AxisType.ToString();
                if (!string.IsNullOrEmpty(value.UnitAxis[0]))
                  axisCont.UNITDISPLAYNAME = value.UnitAxis[0];
                axisCont.Vs = dstAxisValues;
                axisConts.Add(axisCont);
              }
              instance.SWAXISCONTS = axisConts.ToArray();
              break;
          }
          instances.Add(instance);
        }

        var treeOrg = new SWINSTANCETREEORIGIN(a2LSource, dataSource);
        var tree = new SWINSTANCETREE("sepp", "NO_VCD", treeOrg, funcColl, instances);
        spec.SWINSTANCETREEs = new List<SWINSTANCETREE>(new SWINSTANCETREE[] { tree });
        var system = new SWSYSTEM(spec);
        msrsw.SWSYSTEMS = new List<SWSYSTEM>(new SWSYSTEM[] { system });

        // serialize
        using (var tw = new XmlTextWriter(path, Encoding.UTF8))
        {
          tw.Formatting = Formatting.Indented;
          tw.IndentChar = '\t';
          tw.Indentation = 1;
          var serializer = Extensions.getXmlSerializer(typeof(MSRSW));

          // prevent from exporting a namespace
          var ns = new XmlSerializerNamespaces();
          ns.Add("", "");

          // serialize to textwriter
          serializer.Serialize(tw, msrsw, ns);
        }
        return values.Length;
      }
      catch { return -1; }
    }
    /// <summary>
    /// Opens a CDF file and returns a CDFFile instance.
    /// </summary>
    /// <param name="path">the fully qualified CDF filename</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <returns>A new CDFFile instance or null if failed</returns>
    public static DataConservation open(string path, A2LMODULE module)
    {
      var result = new CDFFile();
      try
      {
        var ser = Extensions.getXmlSerializer(typeof(MSRSW));
        MSRSW msrsw = null;
        using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
          msrsw = (MSRSW)ser.Deserialize(fs);
        if (msrsw == null)
          return null;

        var instances = msrsw.getInstances();
        var project = module.getParent<A2LPROJECT>();
        A2LRECORD_LAYOUT_REF layoutRef;
        var charDict = project.CharDict;
        ICharValue charValue = null;
        foreach (var instance in instances)
        {
          if (instance.SWVALUECONT == null || instance.SWVALUECONT.Vs.Length == 0
            || !charDict.TryGetValue(instance.SHORTNAME, out layoutRef)
            || !checkType(layoutRef, instance.CATEGORY)
            )
            // no data, no matching local characteristic, no matching characteristic type
            continue;

          try
          {
            // correct type and name found
            var characteristic = (A2LCHARACTERISTIC)layoutRef;
            var verbalTab = characteristic.RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
            var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
            switch (characteristic.CharType)
            {
              case CHAR_TYPE.VALUE:
                charValue = new SingleValue(characteristic);
                charValue.Value = verbalTab != null && instance.SWVALUECONT.Vs[0] is string
                  ? verbalTab.toRaw((string)instance.SWVALUECONT.Vs[0])
                  : (double)instance.SWVALUECONT.Vs[0];
                break;

              case CHAR_TYPE.ASCII:
                charValue = new ASCIIValue(characteristic);
                charValue.Value = (string)instance.SWVALUECONT.Vs[0];
                break;

              case CHAR_TYPE.VAL_BLK:
                charValue = new ValBlkValue(characteristic);
                var dstValues = new double[instance.SWVALUECONT.Vs.Length];
                for (int i = 0; i < instance.SWVALUECONT.Vs.Length; ++i)
                  dstValues[i] = verbalTab != null
                    ? verbalTab.toRaw((string)instance.SWVALUECONT.Vs[i])
                    : (double)instance.SWVALUECONT.Vs[i];
                charValue.Value = dstValues;
                break;

              case CHAR_TYPE.CURVE:
                charValue = new CurveValue(characteristic);
                dstValues = new double[instance.SWVALUECONT.Vs.Length];
                for (int i = 0; i < instance.SWVALUECONT.Vs.Length; ++i)
                  dstValues[i] = verbalTab != null
                    ? verbalTab.toRaw((string)instance.SWVALUECONT.Vs[i])
                    : (double)instance.SWVALUECONT.Vs[i];
                charValue.Value = dstValues;
                break;

              case CHAR_TYPE.MAP:
                charValue = new MapValue(characteristic);
                var maxX = axisDescs[0].MaxAxisPoints;
                var maxY = axisDescs[1].MaxAxisPoints;
                var dstMapValues = new double[maxX, maxY];
                for (int j = 0; j < maxY; ++j)
                {
                  VG vg = instance.SWVALUECONT.Vs[j] as VG;
                  if (null == vg)
                    continue;
                  for (int i = 0; i < vg.Vs.Length; ++i)
                    dstMapValues[i, j] = verbalTab != null
                      ? verbalTab.toRaw((string)vg.Vs[i])
                      : (double)vg.Vs[i];
                }
                charValue.Value = dstMapValues;
                break;
            }

            if (charValue != null)
            { // add the axis values
              switch (characteristic.CharType)
              {
                case CHAR_TYPE.CURVE:
                case CHAR_TYPE.MAP:
                case CHAR_TYPE.CUBOID:
                case CHAR_TYPE.CUBE_4:
                case CHAR_TYPE.CUBE_5:
                  if (charValue.AxisValue == null)
                    charValue.AxisValue = new double[axisDescs.Count][];
                  for (int j = 0; j < axisDescs.Count && j < instance.SWAXISCONTS.Length; ++j)
                  {
                    verbalTab = axisDescs[j].RefCompuMethod.RefCompuTab as A2LCOMPU_VTAB;
                    var axisCont = instance.SWAXISCONTS[j];
                    if (axisCont.Vs == null)
                      continue;
                    var dstValues = new double[axisCont.Vs.Length];
                    for (int i = 0; i < axisCont.Vs.Length; ++i)
                      dstValues[i] = verbalTab != null
                        ? verbalTab.toRaw((string)axisCont.Vs[i])
                        : (double)axisCont.Vs[i];
                    charValue.AxisValue[j] = dstValues;
                  }
                  break;
              }
            }

            // the end is reached
            if (charValue != null)
              // add characteristic
              result.mValues.Add(charValue);
          }
          catch
          { // something got wrong
          }
        }
      }
      catch (Exception ex)
      {
        mLogger.Fatal(ex);
      }
      return result;
    }
    #endregion
    /// <summary>
    /// Checks the specified CDF type string against the corresponding characteristic type.
    /// </summary>
    /// <param name="layoutRef">The characteristic to check</param>
    /// <param name="cdfType">The CDF type string</param>
    /// <returns>true if the types are matching</returns>
    static bool checkType(A2LRECORD_LAYOUT_REF layoutRef, string cdfType)
    {
      var type = CHAR_TYPE.NotSet;
      if (!Enum.TryParse(cdfType, out type))
        return false;
      var characteristic = layoutRef as A2LCHARACTERISTIC;
      if (characteristic != null)
        return type == characteristic.CharType;
      return true;
    }
    /// <summary>
    /// Filters control characters from ASCII characteristics.
    /// </summary>
    /// <param name="text">The string to filter control characters</param>
    /// <returns>The filtered string</returns>
    static object toXMLString(string text)
    {
      var sb = new StringBuilder();
      for (int i = 0; i < text.Length; ++i)
      {
        char c = text[i];
        sb.Append(char.IsControl(c) ? '?' : c);
      }
      return sb.ToString();
    }
    #endregion
  }
}
