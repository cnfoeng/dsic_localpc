﻿/*!
 * @file    
 * @brief   Defines the 'in-memory' representation for a characteristic value.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    November 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Base class for a characteristic value representation.
  /// </summary>
  public abstract class BaseValue : ICharValue
  {
    #region members
    protected A2LRECORD_LAYOUT_REF mCharacteristic;
    protected ValueObjectFormat mValueFormat;
    protected object mValue;
#if MONOTONY_SUPPORTED
    object mMappingTable;
#endif
    protected double[][] mAxisValue;
    protected int[] mDecimalCountAxis;
    protected string[] mUnitAxis;
    protected int mDecimalCount;
    protected string mUnit;
    #endregion
    #region constructors
    /// <summary>
    /// Default constructor
    /// </summary>
    public BaseValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public BaseValue(A2LRECORD_LAYOUT_REF characteristic)
    {
      mCharacteristic = characteristic;
    }
    #endregion
    #region methods
    /// <summary>
    /// Gets a string representation of the value.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      if (!(mValue is double))
        return $"Value={mValue}";
      return string.IsNullOrEmpty(mUnit)
        ? $"Value={((double)mValue).ToString($"f{mDecimalCount}", CultureInfo.InvariantCulture)}"
        : $"Value[{mUnit}]={((double)mValue).ToString($"f{mDecimalCount}", CultureInfo.InvariantCulture)}";
    }
    /// <summary>
    /// Increments or decrements a single value by the specified increment/decrement count.
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="increment">true for increment, false for decrement</param>
    /// <param name="count">The count of increments or decrements to do</param>
    /// <returns>true if successful</returns>
    protected virtual bool incrementOrDecrement(IDataFile dataFile, bool increment, int count)
    { throw new NotImplementedException(((ICharValue)this).CharType.ToString()); }
    /// <summary>
    /// Increments or decrements a CHAR_TYPE.CURVE or CHAR_TYPE.MAP value by the specified increment/decrement count.
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="increment">true for increment, false for decrement</param>
    /// <param name="count">The count of increments or decrements to do</param>
    /// <param name="cells">The selection to increment or decrement (as DataGridView cells)</param>
    /// <returns>true if successful</returns>
    protected virtual bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells)
    { throw new NotImplementedException(((ICharValue)this).CharType.ToString()); }
    /// <summary>
    /// Increments or decrements a CHAR_TYPE.CUBOID value by the specified increment/decrement count.
    /// </summary>
    /// <param name="dataFile">The datafile to read from</param>
    /// <param name="increment">true for increment, false for decrement</param>
    /// <param name="count">The count of increments or decrements to do</param>
    /// <param name="cells">The selection to increment or decrement (as DataGridView cells)</param>
    /// <param name="zIndex">The corresponding zIndex to set</param>
    /// <returns>true if successful</returns>
    protected virtual bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells, int zIndex)
    { throw new NotImplementedException(((ICharValue)this).CharType.ToString()); }
    /// <summary>
    /// Gets the next value near to the specified value (by incrementing or decrementing).
    /// 
    /// This method is used for TAB_VERB and TAB_NOINTP values.
    /// </summary>
    /// <param name="increment">true for increment, false for decrement</param>
    /// <param name="value">The raw or physical value</param>
    /// <param name="dt">The raw data type of the value</param>
    /// <param name="compuMethod">The compu method of the value</param>
    /// <returns>The minimum increment based on the count of decimal places</returns>
    /// <exception cref="ArgumentException">Thrown, if the compuMethod is of not of type CONVERSION_TYTPE.TAB_NOINTP or CONVERSION_TYTPE.TAB_VERB</exception>
    protected double getNextValue(bool increment, double value, DATA_TYPE dt, A2LCOMPU_METHOD compuMethod)
    {
      switch (compuMethod.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          break;
        default:
          throw new ArgumentException("Wrong method called for this conversion type", "compuMethod");
      }
      // compute minimum increment by data type and CompuMethod
      switch (compuMethod.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
          if (mValueFormat == ValueObjectFormat.Physical)
            value = compuMethod.toRaw(dt, value);

          // increment/decrement by dictionary
          float targetkey;
          var values = ((A2LCOMPU_TAB)compuMethod.RefCompuTab).Values;
          float key = Convert.ToSingle(value);
          float minKey = values.Keys[0];
          float maxKey = values.Keys[values.Count - 1];
          if (key < minKey)
            targetkey = increment ? minKey : maxKey;
          else if (key > maxKey)
            targetkey = increment ? minKey : maxKey;
          else
          { // value within
            while (true)
            {
              key += increment ? 1 : -1;
              if (key >= maxKey)
              {
                targetkey = maxKey;
                break;
              }
              if (key <= minKey)
              {
                targetkey = minKey;
                break;
              }
              if (values.ContainsKey(key))
              {
                targetkey = key;
                break;
              }
            }
          }
          return mValueFormat == ValueObjectFormat.Physical ? values[targetkey] : targetkey;
        case CONVERSION_TYPE.TAB_VERB:
          // increment/decrement by dictionary
          var verbs = ((A2LCOMPU_VTAB)compuMethod.RefCompuTab).Verbs;
          key = Convert.ToSingle(value);
          minKey = verbs.Keys[0];
          maxKey = verbs.Keys[verbs.Count - 1];
          if (key < minKey)
            return increment ? minKey : maxKey;
          if (key > maxKey)
            return increment ? minKey : maxKey;
          else
          { // value within
            while (true)
            {
              key += increment ? 1 : -1;
              if (key >= maxKey)
                return maxKey;
              if (key <= minKey)
                return minKey;
              if (verbs.ContainsKey(key))
                return key;
            }
          }
      }
      throw new ApplicationException("may never be reached");
    }
    /// <summary>
    /// Test, if the specified string could be pasted into the target value.
    /// </summary>
    /// <param name="cbString">The clipboard content string</param>
    /// <param name="targetCells">The selected target cells</param>
    /// <param name="values">The values parsed from the string</param>
    /// <returns>The result</returns>
    protected PasteResult canPasteFromClipboard(string cbString
      , DataGridViewSelectedCellCollection targetCells
      , out double[,] values
      )
    {
      values = null;
      switch (((A2LCHARACTERISTIC)mCharacteristic).CharType)
      {
        case CHAR_TYPE.VALUE:
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
        case CHAR_TYPE.MAP:
          break;
        default:
          // todo implement handling cuboid, cube_4, cube_5...
          return PasteResult.ErrNotSupportedByTargetValue;
      }
      var compuMethod = mCharacteristic.RefCompuMethod;
      switch (compuMethod.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          // todo implement convert to raw values...
          return PasteResult.ErrNotSupportedByTargetValue;
      }

      string origin = Clipboard.GetData(DataFormats.Text) as string;
      if (string.IsNullOrEmpty(origin))
        return PasteResult.ErrSrcFormatNotAvailable;
      var lines = origin.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
      if (lines.Length == 0)
        return PasteResult.ErrSrcFormatNotAvailable;
      var lineItems = lines[0].Split('\t');
      int xMax = lineItems.Length;
      var destArray = new string[xMax, lines.Length];
      for (int y = 0; y < lines.Length; ++y)
      {
        if (y > 0)
        {
          lineItems = lines[y].Split('\t');
          if (lineItems.Length != xMax)
            // no quadratic array
            return PasteResult.ErrSrcNonQuadratic;
        }
        for (int x = 0; x < xMax; ++x)
          destArray[x, y] = lineItems[x];
      }

      // filter potential axis values
      int startX = mAxisValue != null && mAxisValue[0].Length + 1 == destArray.GetLength(0) ? 1 : 0;
      int startY = mAxisValue != null
        && (mAxisValue.Length + 1 == destArray.GetLength(1)
          || mAxisValue.Length > 1 && mAxisValue[1].Length + 1 == destArray.GetLength(1)
           ) ? 1 : 0;
      values = new double[destArray.GetLength(0) - startX, destArray.GetLength(1) - startY];

      Rectangle boundingRect;
      getBoundingRect(targetCells, out boundingRect);

      CHAR_TYPE type = ((A2LCHARACTERISTIC)mCharacteristic).CharType;
      switch (type)
      {
        case CHAR_TYPE.VAL_BLK:
          int spaceX = targetCells == null || targetCells.Count == 0
            ? ((double[])mValue).Length
            : ((double[])mValue).Length - boundingRect.X;
          if (values.GetLength(0) > spaceX)
            return PasteResult.WarnDoesNotFit;
          break;
        case CHAR_TYPE.CURVE:
        case CHAR_TYPE.MAP:
          // check for remaining space
          spaceX = targetCells == null || targetCells.Count == 0
            ? mAxisValue[0].Length
            : mAxisValue[0].Length - boundingRect.X;
          if (values.GetLength(0) > spaceX)
            return PasteResult.WarnDoesNotFit;
          if (type == CHAR_TYPE.MAP)
          {
            int spaceY = targetCells == null || targetCells.Count == 0
              ? mAxisValue[1].Length
              : mAxisValue[1].Length - boundingRect.Y;
            if (values.GetLength(1) > spaceY)
              return PasteResult.WarnDoesNotFit;
          }
          break;
      }
      for (int y = startY; y < destArray.GetLength(1); ++y)
      {
        try
        {
          for (int x = startX; x < destArray.GetLength(0); ++x)
            values[x - startX, y - startY] = A2LParser.parse2DoubleVal(destArray[x, y]);
        }
        catch { return PasteResult.ErrSrcWrongValueFormat; }
      }
      return PasteResult.Ok;
    }
    string ICharValue.toClipboardFormat(DataGridViewSelectedCellCollection srcCells)
    {
      var characteristic = mCharacteristic as A2LCHARACTERISTIC;
      if (characteristic == null)
        throw new NotImplementedException($"Copy to clipboard not supported by {mCharacteristic.Type} type!");

      Rectangle boundingRect;
      BaseValue.getBoundingRect(srcCells, out boundingRect);

      var result = new StringBuilder();
      switch (characteristic.CharType)
      {
        case CHAR_TYPE.VALUE: result.Append(((ICharValue)this).toSingleValue()); break;
        case CHAR_TYPE.ASCII: result.Append((string)mValue); break;
        case CHAR_TYPE.VAL_BLK:
          var values = (double[])mValue;
          if (boundingRect.Width == 0)
          {
            boundingRect.Location = Point.Empty;
            boundingRect.Width = values.Length;
          }
          for (int i = boundingRect.Left; i < boundingRect.Right; ++i)
            result.Append($"{((ICharValue)this).toSingleValue(i, 0, 0)}\t");
          result.Remove(result.Length - 1, 1);
          break;
        case CHAR_TYPE.CURVE:
          var curveValues = (double[])mValue;
          if (boundingRect.Width == 0)
          {
            boundingRect.Location = Point.Empty;
            boundingRect.Width = curveValues.Length;

            // add x axis
            for (int i = 0; i < curveValues.Length; ++i)
              result.Append($"{((ICharValue)this).toSingleValue(i, -1, 0)}\t");
            result.Remove(result.Length - 1, 1);
            result.Append(Environment.NewLine);
          }
          for (int i = boundingRect.X; i < boundingRect.Right; ++i)
            result.Append($"{((ICharValue)this).toSingleValue(i, 0, 0)}\t");
          result.Remove(result.Length - 1, 1);
          break;
        case CHAR_TYPE.MAP:
          var mapValues = (double[,])mValue;
          if (boundingRect.Width == 0)
          {
            boundingRect.Location = Point.Empty;
            boundingRect.Width = mapValues.GetLength(0);
            boundingRect.Height = mapValues.GetLength(1);

            // add x axis
            result.Append($"{((ICharValue)this).toSingleValue(-1, -1, 0)}\t");
            for (int i = 0; i < mapValues.GetLength(0); ++i)
              result.Append($"{((ICharValue)this).toSingleValue(i, -1, 0)}\t");
            result.Remove(result.Length - 1, 1);
            result.Append(Environment.NewLine);
          }
          for (int y = boundingRect.Y; y < boundingRect.Bottom; ++y)
          {
            if (boundingRect.Width == mapValues.GetLength(0)
              && boundingRect.Height == mapValues.GetLength(1)
               )
              // add y axis
              result.Append($"{((ICharValue)this).toSingleValue(-1, y, 0)}\t");

            for (int x = boundingRect.X; x < boundingRect.Right; ++x)
              result.Append($"{((ICharValue)this).toSingleValue(x, y, 0)}\t");
            result.Remove(result.Length - 1, 1);
            result.Append(Environment.NewLine);
          }
          result.Remove(result.Length - 1, 1);
          break;
        case CHAR_TYPE.CUBE_4:
        case CHAR_TYPE.CUBE_5:
          throw new NotImplementedException($"Copy to clipboard not supported\nby {mCharacteristic.Type} type!");
        default:
          throw new NotSupportedException(characteristic.CharType.ToString());
      }
      return result.ToString();
    }
    /// <summary>
    /// Gets a bounding rectangle defined by the specified DataGridView cells.
    /// </summary>
    /// <param name="cells">The cells to get the bounding rectangle from</param>
    /// <param name="contentRect">The resulting rectangle (only valid if return value is true)</param>
    /// <returns>false, if cells are empty or not set, else the bounding rectangle</returns>
    public static bool getBoundingRect(DataGridViewSelectedCellCollection cells, out Rectangle contentRect)
    {
      contentRect = Rectangle.Empty;
      if (cells == null || cells.Count == 0)
        return false;
      var leftTop = new Point(int.MaxValue, int.MaxValue);
      var rightBottom = new Point(int.MinValue, int.MinValue);
      for (int i = cells.Count - 1; i >= 0; --i)
      {
        var cell = cells[i];
        leftTop.X = Math.Min(leftTop.X, cell.ColumnIndex);
        leftTop.Y = Math.Min(leftTop.Y, cell.RowIndex);
        rightBottom.X = Math.Max(rightBottom.X, cell.ColumnIndex);
        rightBottom.Y = Math.Max(rightBottom.Y, cell.RowIndex);
      }
      contentRect.Location = leftTop;
      contentRect.Size = new Size(rightBottom.X - leftTop.X + 1, rightBottom.Y - leftTop.Y + 1);
      return true;
    }
    /// <summary>
    /// Indicates if a the specified axis is not monoton.
    /// </summary>
    /// <param name="axisIndex">The eaxis index</param>
    /// <returns>true if monotony is violated</returns>
    bool isMappingRequired(int axisIndex)
    {
      double currMax = double.MaxValue;
      for (int i = mAxisValue[axisIndex].Length - 1; i >= 0; --i)
      {
        double value = mAxisValue[axisIndex][i];
        if (value > currMax)
          // monotony violated
          return true;
        currMax = value;
      }
      return false;
    }
    /// <summary>
    /// Builds the mapping table for non-monoton characteristics
    /// </summary>
    /// <param name="axisCnt">Count of availabel axis</param>
    /// <returns></returns>
    internal void buildAxisMappingTable(int axisCnt)
    {
#if MONOTONY_SUPPORTED
      bool noMappingRequired = true;
      for (int i = 0; i < axisCnt; ++i)
      {
        if (!isMappingRequired(i))
          continue;
        noMappingRequired = false;
        break;
      }
      if (noMappingRequired)
        return;

      object fncValues = null;
      switch (axisCnt)
      {
        case 1: fncValues = ((double[])mValue).Clone(); break;
        case 2: fncValues = ((double[,])mValue).Clone(); break;
        case 3: fncValues = ((double[, ,])mValue).Clone(); break;
        case 4: fncValues = ((double[, , ,])mValue).Clone(); break;
        case 5: fncValues = ((double[, , , ,])mValue).Clone(); break;
      }

      var mappingTable = new int[5][];
      var newAxisTable = new double[5][];
      for (int axisIdx = 0; axisIdx < axisCnt; ++axisIdx)
      {
        mappingTable[axisIdx] = new int[mAxisValue[axisIdx].Length];
        newAxisTable[axisIdx] = new double[mAxisValue[axisIdx].Length];
        int currMaxIdx = -1;
        double currMax = double.MinValue, lastMax = double.MaxValue;
        for (int j = mAxisValue[axisIdx].Length - 1; j >= 0; --j)
        {
          for (int i = mAxisValue[axisIdx].Length - 1; i >= 0; --i)
          {
            double value = mAxisValue[axisIdx][i];
            if (value == lastMax)
            { // special case: axis values are the same
              bool found = false;
              for (int k = mAxisValue[axisIdx].Length - 1; k > j; --k)
                if (mappingTable[axisIdx][k] == i)
                  found = true;
              if (!found)
              { // not contained yet
                currMax = value;
                currMaxIdx = i;
                newAxisTable[axisIdx][j] = value;

                switch (axisCnt)
                {
                  case 1: ((double[])fncValues)[axisIdx] = ((double[])mValue)[i]; break;
                  //case 2: fncValues = ((double[,])mValue).Clone(); break;
                  //case 3: fncValues = ((double[, ,])mValue).Clone(); break;
                  //case 4: fncValues = ((double[, , ,])mValue).Clone(); break;
                  //case 5: fncValues = ((double[, , , ,])mValue).Clone(); break;
                }
              }
            }
            else if (value > currMax && value < lastMax)
            { // next smaller one
              currMax = value;
              currMaxIdx = i;
              newAxisTable[axisIdx][j] = value;
            }
          }
          lastMax = currMax;
          currMax = double.MinValue;
          mappingTable[axisIdx][j] = currMaxIdx;
        }
      }
      // Copy the sorted axis as current axis values
      for (int i = 0; i < axisCnt; ++i)
        mAxisValue[i] = newAxisTable[i];
      mValue = fncValues;
      mMappingTable = mappingTable;
#endif
    }
    #endregion
    #region ICharValue members
    A2LRECORD_LAYOUT_REF ICharValue.Characteristic
    {
      get { return mCharacteristic; }
      set { mCharacteristic = value; }
    }
    CHAR_TYPE ICharValue.CharType
    {
      get
      {
        return mCharacteristic is A2LCHARACTERISTIC
          ? ((A2LCHARACTERISTIC)mCharacteristic).CharType
          : CHAR_TYPE.VAL_BLK;
      }
    }
    ValueObjectFormat ICharValue.ValueFormat
    {
      get { return mValueFormat; }
      set { mValueFormat = value; }
    }
    object ICharValue.Value
    {
      get { return mValue; }
      set { mValue = value; }
    }
    double[][] ICharValue.AxisValue
    {
      get { return mAxisValue; }
      set { mAxisValue = value; }
    }
    int[] ICharValue.DecimalCountAxis
    {
      get { return mDecimalCountAxis; }
      set { mDecimalCountAxis = value; }
    }
    string[] ICharValue.UnitAxis
    {
      get { return mUnitAxis; }
      set { mUnitAxis = value; }
    }
    int ICharValue.DecimalCount
    {
      get { return mDecimalCount; }
      set { mDecimalCount = value; }
    }
    string ICharValue.Unit
    {
      get { return mUnit; }
      set { mUnit = value; }
    }
    string ICharValue.toSingleValue()
    {
      CHAR_TYPE charType = ((ICharValue)this).CharType;
      switch (charType)
      {
        case CHAR_TYPE.VALUE:
          return mCharacteristic.toStringValue((double)mValue, mValueFormat, mCharacteristic.RefRecordLayout.FncValues.DataType);
        case CHAR_TYPE.ASCII:
          return (string)mValue;
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
        case CHAR_TYPE.MAP:
        case CHAR_TYPE.CUBOID:
        case CHAR_TYPE.CUBE_4:
        case CHAR_TYPE.CUBE_5:
          // no single value representation
          return string.Empty;
        default:
          throw new NotSupportedException(charType.ToString());
      }
    }

    /// <summary>
    /// Gets CompuMethod and data type from the specified axis description.
    /// </summary>
    /// <param name="layout">Record layout of the main characteristic</param>
    /// <param name="axisDescs">Axis descriptions</param>
    /// <param name="axis">Axis index (X, Y, Z,...)</param>
    /// <param name="cm">potential modified computation</param>
    /// <param name="dt">potential modified data type</param>
    static void getAxisData(A2LRECORD_LAYOUT layout
      , List<A2LAXIS_DESCR> axisDescs, int axis
      , ref A2LCOMPU_METHOD cm, ref DATA_TYPE dt
      )
    {
      try
      {
        var axisDesc = axisDescs[axis];
        if (axisDesc.RefCurveAxisRef != null)
        {
          cm = axisDesc.RefCurveAxisRef.getNode<A2LAXIS_DESCR>(false).RefCompuMethod;
          var axisPts = axisDesc.RefCurveAxisRef.RefRecordLayout.AxisPts[0];
          if (null == axisPts)
            axisPts = axisDesc.RefCurveAxisRef.RefRecordLayout.AxisRescaleX;
          dt = axisPts.DataType;
        }
        else if (axisDesc.RefAxisPtsRef != null)
        {
          cm = axisDesc.RefAxisPtsRef.RefCompuMethod;
          var axisPts = axisDesc.RefAxisPtsRef.RefRecordLayout.AxisPts[0];
          if (null == axisPts)
            axisPts = axisDesc.RefAxisPtsRef.RefRecordLayout.AxisRescaleX;
          dt = axisPts.DataType;
        }
        else
        {
          cm = axisDesc.RefCompuMethod;
          var axisPts = layout.AxisPts[axis];
          if (null == axisPts)
            axisPts = layout.AxisRescaleX;
          if (null != axisPts)
            dt = axisPts.DataType;
        }
      }
      catch { }
    }

    string ICharValue.toSingleValue(int x, int y, int z)
    {
      CHAR_TYPE charType = ((ICharValue)this).CharType;
      var compuMethod = mCharacteristic.RefCompuMethod;
      var layout = mCharacteristic.RefRecordLayout;
      var dataType = layout.FncValues != null
        ? layout.FncValues.DataType
        : layout.AxisPts[0].DataType;
      var axisDescs = mCharacteristic.getNodeList<A2LAXIS_DESCR>();

      switch (charType)
      {
        case CHAR_TYPE.VALUE:
          return ((ICharValue)this).toSingleValue();

        case CHAR_TYPE.CUBOID:
          if (y == -1 && z == -1)
          { // z axis
            getAxisData(layout, axisDescs, 2, ref compuMethod, ref dataType);
            return A2LCONVERSION_REF.toStringValue(mAxisValue[2][z], mValueFormat, dataType, compuMethod, mDecimalCountAxis[2]);
          }
          else
          {
            if (y == -1 && x >= 0)
            { // x axis
              if (x >= mAxisValue[0].Length)
                return string.Empty;
              getAxisData(layout, axisDescs, 0, ref compuMethod, ref dataType);
              return A2LCONVERSION_REF.toStringValue(mAxisValue[0][x], mValueFormat, dataType, compuMethod, mDecimalCountAxis[0]);
            }
            else if (x == -1)
            { // y axis
              if (y == -1)
                return $"[{mUnitAxis[0]}]/[{mUnitAxis[1]}]";
              if (y >= mAxisValue[1].Length)
                return string.Empty;
              getAxisData(layout, axisDescs, 1, ref compuMethod, ref dataType);
              return A2LCONVERSION_REF.toStringValue(mAxisValue[1][y], mValueFormat, dataType, compuMethod, mDecimalCountAxis[1]);
            }
            // fnc values
            return A2LCONVERSION_REF.toStringValue(((double[,,])mValue)[x, y, z], mValueFormat, dataType, compuMethod, mDecimalCount);
          }

        case CHAR_TYPE.MAP:
          if (y == -1 && x >= 0)
          { // x axis
            if (x >= mAxisValue[0].Length)
              return string.Empty;
            getAxisData(layout, axisDescs, 0, ref compuMethod, ref dataType);
            return A2LCONVERSION_REF.toStringValue(mAxisValue[0][x], mValueFormat, dataType, compuMethod, mDecimalCountAxis[0]);
          }
          else if (x == -1)
          { // y axis
            if (y == -1)
              return $"[{mUnitAxis[0]}]/[{mUnitAxis[1]}]";
            if (y >= mAxisValue[1].Length)
              return string.Empty;
            getAxisData(layout, axisDescs, 1, ref compuMethod, ref dataType);
            return A2LCONVERSION_REF.toStringValue(mAxisValue[1][y], mValueFormat, dataType, compuMethod, mDecimalCountAxis[1]);
          }
          // fnc values
          return A2LCONVERSION_REF.toStringValue(((double[,])mValue)[x, y], mValueFormat, dataType, compuMethod, mDecimalCount);

        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          if (x == -1)
          {
            if (y == -1)
              return mAxisValue == null || string.IsNullOrEmpty(mUnitAxis[0])
                ? " "
                : mUnitAxis[0];
            else
              return mUnit;
          }
          if (y == -1)
          { // x axis
            if (mAxisValue != null)
            { // curve x axis
              if (x >= mAxisValue[0].Length)
                return string.Empty;
              getAxisData(layout, axisDescs, 0, ref compuMethod, ref dataType);
              return A2LCONVERSION_REF.toStringValue(mAxisValue[0][x], mValueFormat, dataType, compuMethod, mDecimalCountAxis[0]);
            }
            else
              // value block (simply the index)
              return x.ToString();
          }

          // fnc values
          return A2LCONVERSION_REF.toStringValue(((double[])mValue)[x], mValueFormat, dataType, compuMethod, mDecimalCount);

        default: throw new NotSupportedException(charType.ToString());
      }
    }
    bool ICharValue.increment(IDataFile dataFile, int count)
    {
      return incrementOrDecrement(dataFile, true, count);
    }
    bool ICharValue.increment(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells)
    {
      return incrementOrDecrement(dataFile, true, count, cells);
    }
    bool ICharValue.increment(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells, int zIndex)
    {
      return incrementOrDecrement(dataFile, true, count, cells, zIndex);
    }
    bool ICharValue.decrement(IDataFile dataFile, int count)
    {
      return incrementOrDecrement(dataFile, false, count);
    }
    bool ICharValue.decrement(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells)
    {
      return incrementOrDecrement(dataFile, false, count, cells);
    }
    bool ICharValue.decrement(IDataFile dataFile, int count, DataGridViewSelectedCellCollection cells, int zIndex)
    {
      return incrementOrDecrement(dataFile, false, count, cells, zIndex);
    }
    public virtual PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection cells)
    {
      throw new NotImplementedException();
    }
    PasteResult ICharValue.pasteClipboardStringTest(string cbString, DataGridViewSelectedCellCollection targetCells)
    {
      double[,] values;
      return canPasteFromClipboard(cbString, targetCells, out values);
    }
    void ICharValue.modifyValue(double newValue, bool treatValueAsPercent, ModifyFuncByValue modifyFunc)
    {
      CHAR_TYPE type = ((ICharValue)this).CharType;
      switch (type)
      {
        case CHAR_TYPE.VALUE:
          mValue = modifyFunc((double)mValue, newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          var blkValue = (double[])mValue;
          for (int i = 0; i < blkValue.Length; ++i)
            blkValue[i] = modifyFunc(blkValue[i], newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.MAP:
          var mapValue = (double[,])mValue;
          for (int i = 0; i < mapValue.GetLength(0); ++i)
            for (int j = 0; j < mapValue.GetLength(1); ++j)
              mapValue[i, j] = modifyFunc(mapValue[i, j], newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.CUBOID:
          var cubeValue = (double[,,])mValue;
          for (int i = 0; i < cubeValue.GetLength(0); ++i)
            for (int j = 0; j < cubeValue.GetLength(1); ++j)
              for (int k = 0; i < cubeValue.GetLength(2); ++k)
                cubeValue[i, j, k] = modifyFunc(cubeValue[i, j, k], newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.CUBE_4:
          var cube4Value = (double[,,,])mValue;
          for (int i = 0; i < cube4Value.GetLength(0); ++i)
            for (int j = 0; j < cube4Value.GetLength(1); ++j)
              for (int k = 0; i < cube4Value.GetLength(2); ++k)
                for (int l = 0; l < cube4Value.GetLength(3); ++l)
                  cube4Value[i, j, k, l] = modifyFunc(cube4Value[i, j, k, l], newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.CUBE_5:
          var cube5Value = (double[,,,,])mValue;
          for (int i = 0; i < cube5Value.GetLength(0); ++i)
            for (int j = 0; j < cube5Value.GetLength(1); ++j)
              for (int k = 0; i < cube5Value.GetLength(2); ++k)
                for (int l = 0; l < cube5Value.GetLength(3); ++l)
                  for (int m = 0; m < cube5Value.GetLength(4); ++m)
                    cube5Value[i, j, k, l, m] = modifyFunc(cube5Value[i, j, k, l, m], newValue, treatValueAsPercent);
          break;
        default: throw new NotSupportedException(type.ToString());
      }
    }
    void ICharValue.modifyValue(double newValue, bool treatValueAsPercent, ModifyFuncByValue modifyFunc, DataGridViewSelectedCellCollection targetCells, int z)
    {
      CHAR_TYPE type = ((ICharValue)this).CharType;
      switch (type)
      {
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          var blkValue = (double[])mValue;
          foreach (DataGridViewCell cell in targetCells)
            blkValue[cell.ColumnIndex] = modifyFunc(blkValue[cell.ColumnIndex], newValue, treatValueAsPercent);
          break;
        case CHAR_TYPE.MAP:
          var mapValue = (double[,])mValue;
          foreach (DataGridViewCell cell in targetCells)
          {
            double value = mapValue[cell.ColumnIndex, cell.RowIndex];
            mapValue[cell.ColumnIndex, cell.RowIndex] = modifyFunc(value, newValue, treatValueAsPercent);
          }
          break;
        case CHAR_TYPE.CUBOID:
          var cubeValue = (double[,,])mValue;
          foreach (DataGridViewCell cell in targetCells)
          {
            double value = cubeValue[cell.ColumnIndex, cell.RowIndex, z];
            cubeValue[cell.ColumnIndex, cell.RowIndex, z] = modifyFunc(value, newValue, treatValueAsPercent);
          }
          break;
        default: throw new NotImplementedException();
      }
    }
    void ICharValue.modifyValue(ModifyFunc modifyFunc)
    {
      CHAR_TYPE type = ((ICharValue)this).CharType;
      switch (type)
      {
        case CHAR_TYPE.VALUE:
          mValue = modifyFunc((double)mValue, mCharacteristic);
          break;
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          var blkValue = (double[])mValue;
          for (int i = 0; i < blkValue.Length; ++i)
            blkValue[i] = modifyFunc(blkValue[i], mCharacteristic);
          break;
        case CHAR_TYPE.MAP:
          var mapValue = (double[,])mValue;
          for (int i = 0; i < mapValue.GetLength(0); ++i)
            for (int j = 0; j < mapValue.GetLength(1); ++j)
              mapValue[i, j] = modifyFunc(mapValue[i, j], mCharacteristic);
          break;
        case CHAR_TYPE.CUBOID:
          var cubeValue = (double[,,])mValue;
          for (int i = 0; i < cubeValue.GetLength(0); ++i)
            for (int j = 0; j < cubeValue.GetLength(1); ++j)
              for (int k = 0; i < cubeValue.GetLength(2); ++k)
                cubeValue[i, j, k] = modifyFunc(cubeValue[i, j, k], mCharacteristic);
          break;
        case CHAR_TYPE.CUBE_4:
          var cube4Value = (double[,,,])mValue;
          for (int i = 0; i < cube4Value.GetLength(0); ++i)
            for (int j = 0; j < cube4Value.GetLength(1); ++j)
              for (int k = 0; i < cube4Value.GetLength(2); ++k)
                for (int l = 0; l < cube4Value.GetLength(3); ++l)
                  cube4Value[i, j, k, l] = modifyFunc(cube4Value[i, j, k, l], mCharacteristic);
          break;
        case CHAR_TYPE.CUBE_5:
          var cube5Value = (double[,,,,])mValue;
          for (int i = 0; i < cube5Value.GetLength(0); ++i)
            for (int j = 0; j < cube5Value.GetLength(1); ++j)
              for (int k = 0; i < cube5Value.GetLength(2); ++k)
                for (int l = 0; l < cube5Value.GetLength(3); ++l)
                  for (int m = 0; m < cube5Value.GetLength(4); ++m)
                    cube5Value[i, j, k, l, m] = modifyFunc(cube5Value[i, j, k, l, m], mCharacteristic);
          break;
        default: throw new NotSupportedException(type.ToString());
      }
    }
    void ICharValue.modifyValue(ModifyFunc modifyFunc, DataGridViewSelectedCellCollection targetCells, int z)
    {
      CHAR_TYPE type = ((ICharValue)this).CharType;
      switch (type)
      {
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          var blkValue = (double[])mValue;
          foreach (DataGridViewCell cell in targetCells)
            blkValue[cell.ColumnIndex] = modifyFunc(blkValue[cell.ColumnIndex], mCharacteristic);
          break;
        case CHAR_TYPE.MAP:
          var mapValue = (double[,])mValue;
          foreach (DataGridViewCell cell in targetCells)
          {
            double value = mapValue[cell.ColumnIndex, cell.RowIndex];
            mapValue[cell.ColumnIndex, cell.RowIndex] = modifyFunc(value, mCharacteristic);
          }
          break;
        case CHAR_TYPE.CUBOID:
          var cubeValue = (double[,,])mValue;
          foreach (DataGridViewCell cell in targetCells)
          {
            double value = cubeValue[cell.ColumnIndex, cell.RowIndex, z];
            cubeValue[cell.ColumnIndex, cell.RowIndex, z] = modifyFunc(value, mCharacteristic);
          }
          break;
        default: throw new NotImplementedException();
      }
    }
    void ICharValue.setFncValue(double value) { ((ICharValue)this).setFncValue(value, 0, 0, 0); }
    void ICharValue.setFncValue(double value, int x, int y, int z)
    {
      var charType = ((ICharValue)this).CharType;
      switch (charType)
      {
        case CHAR_TYPE.VALUE: mValue = value; break;
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE: ((double[])mValue)[x] = value; break;
        case CHAR_TYPE.MAP: ((double[,])mValue)[x, y] = value; break;
        case CHAR_TYPE.CUBOID: ((double[,,])mValue)[x, y, z] = value; break;
        default: throw new NotSupportedException(charType.ToString());
      }
    }
    double ICharValue.getFncValue() { return ((ICharValue)this).getFncValue(0, 0, 0); }
    double ICharValue.getFncValue(int x, int y, int z)
    {
      var charType = ((ICharValue)this).CharType;
      switch (charType)
      {
        case CHAR_TYPE.VALUE: return (double)mValue;
        case CHAR_TYPE.ASCII: return ((string)mValue)[x];
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE: return ((double[])mValue)[x];
        case CHAR_TYPE.MAP: return ((double[,])mValue)[x, y];
        case CHAR_TYPE.CUBOID: return ((double[,,])mValue)[x, y, z];
        default: throw new NotSupportedException(charType.ToString());
      }
    }
    public IEnumerable<double> GetFncValuesEnumerator()
    {
      var charType = ((ICharValue)this).CharType;
      switch (charType)
      {
        case CHAR_TYPE.VALUE:
          // fnc values
          yield return (double)mValue; break;
        case CHAR_TYPE.VAL_BLK:
        case CHAR_TYPE.CURVE:
          // fnc values
          int max = ((double[])mValue).Length;
          for (int x = 0; x < max; ++x)
            yield return ((double[])mValue)[x];
          break;
        case CHAR_TYPE.MAP:
          // fnc values
          for (int x = 0; x < mAxisValue[0].Length; ++x)
            for (int y = 0; y < mAxisValue[1].Length; ++y)
              yield return ((double[,])mValue)[x, y];
          break;
        case CHAR_TYPE.CUBOID:
          // fnc values
          for (int x = 0; x < mAxisValue[0].Length; ++x)
            for (int y = 0; y < mAxisValue[1].Length; ++y)
              for (int z = 0; z < mAxisValue[2].Length; ++z)
                yield return ((double[,,])mValue)[x, y, z];
          break;
        default: throw new NotSupportedException(charType.ToString());
      }
    }
    #endregion
    #region IComparable<ICharValue> Members
    /// <summary>
    /// Comparable interface implementation for single numerical values.
    /// </summary>
    /// <param name="other">The value to compare to</param>
    /// <returns>see IComparer.Compare</returns>
    public int CompareTo(ICharValue other)
    {
      if (this == other)
        return 0;

      var xCharType = ((ICharValue)this).CharType;
      var yCharType = other.CharType;

      if (xCharType != CHAR_TYPE.VALUE && yCharType != CHAR_TYPE.VALUE)
        return 0;
      if (xCharType != CHAR_TYPE.VALUE)
        return -1;
      if (yCharType != CHAR_TYPE.VALUE)
        return 1;
      var xValue = ((double)mValue);
      var yValue = ((double)other.Value);
      return xValue.CompareTo(yValue);
    }
    #endregion
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.VALUE value.
  /// 
  /// The ICharValue.Value in this case is of type double.
  /// </summary>
  public sealed class SingleValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public SingleValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public SingleValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double Value
    {
      get { return (double)((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    protected override bool incrementOrDecrement(IDataFile dataFile, bool increment, int count)
    {
      var cm = mCharacteristic.RefCompuMethod;
      var layout = mCharacteristic.RefRecordLayout;
      double minInc = double.NaN;
      double targetValue = Value;
      switch (cm.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          targetValue = getNextValue(increment, targetValue, layout.FncValues.DataType, cm);
          break;
        default:
          minInc = cm.getMinIncrement(mCharacteristic.LowerLimit, mCharacteristic.UpperLimit
            , mDecimalCount, layout.FncValues.DataType
            );
          for (int i = 0; i < count; ++i)
            targetValue = increment ? targetValue + minInc : targetValue - minInc;
          break;
      }

      if (mValueFormat == ValueObjectFormat.Physical)
      { // restrict physical values to physical limits
        targetValue = Math.Max(mCharacteristic.LowerLimit, targetValue);
        targetValue = Math.Min(mCharacteristic.UpperLimit, targetValue);
      }

      if (targetValue == Value)
        return false;
      Value = targetValue;
      return CharacteristicValue.setValue<SingleValue>(dataFile, (A2LCHARACTERISTIC)mCharacteristic, this);
    }
    public override PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells)
    {
      double[,] values;
      PasteResult result;
      if (PasteResult.Ok != (result = canPasteFromClipboard(cbString, targetCells, out values)))
        return result;
      Value = values[0, 0];
      CharacteristicValue.setValue(dataFile, this);
      return PasteResult.Ok;
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.ASCII value.
  /// 
  /// The ICharValue.Value in this case is of type string.
  /// </summary>
  public sealed class ASCIIValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public ASCIIValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public ASCIIValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    string Value
    {
      get { return (string)((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    public override PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells)
    {
      Value = cbString;
      CharacteristicValue.setValue(dataFile, this);
      return PasteResult.Ok;
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.VALUE_BLOCK value.
  /// 
  /// The ICharValue.Value in this case is of type double[].
  /// </summary>
  public sealed class ValBlkValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public ValBlkValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public ValBlkValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[] Value
    {
      get { return (double[])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    string[] StrValue
    {
      get { return (string[])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    protected override bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells)
    {
      var compuMethod = mCharacteristic.RefCompuMethod;
      var recLayout = mCharacteristic.RefRecordLayout;
      double minInc = double.NaN;
      switch (compuMethod.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          break;
        default:
          minInc = compuMethod.getMinIncrement(mCharacteristic.LowerLimit, mCharacteristic.UpperLimit
            , mDecimalCount, recLayout.FncValues.DataType
            );
          break;
      }
      var targetValues = Value;
      for (int i = 0; i < cells.Count; ++i)
      {
        var cell = cells[i];
        double singleTargetValue = targetValues[cell.ColumnIndex];
        if (!double.IsNaN(minInc))
          for (int j = 0; j < count; ++j)
            singleTargetValue = increment ? singleTargetValue + minInc : singleTargetValue - minInc;
        else
          singleTargetValue = getNextValue(increment, singleTargetValue, recLayout.FncValues.DataType, compuMethod);

        if (mValueFormat == ValueObjectFormat.Physical)
        { // restrict physical values to physical limits
          singleTargetValue = Math.Max(mCharacteristic.LowerLimit, singleTargetValue);
          singleTargetValue = Math.Min(mCharacteristic.UpperLimit, singleTargetValue);
        }
        targetValues[cell.ColumnIndex] = singleTargetValue;
      }
      Value = targetValues;
      return CharacteristicValue.setValue<ValBlkValue>(dataFile, (A2LCHARACTERISTIC)mCharacteristic, this);
    }
    public override PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells)
    {
      double[,] values;
      PasteResult result;
      if (PasteResult.Ok != (result = canPasteFromClipboard(cbString, targetCells, out values)))
        return result;

      Rectangle boundingRect;
      getBoundingRect(targetCells, out boundingRect);

      for (int x = 0; x < values.GetLength(0); ++x)
        Value[x + boundingRect.X] = values[x, 0];

      CharacteristicValue.setValue(dataFile, this);
      return PasteResult.Ok;
    }
    public override string ToString()
    {
      var sb = new StringBuilder(string.IsNullOrEmpty(mUnit) ? "Values=\n" : $"Values[{mUnit}]=\n");
      if (mValue is string[])
      { // strings
        var values = StrValue;
        for (int i = 0; i < values.Length; ++i)
        {
          sb.Append($"{values[i]},");
          if ((i + 1) % 8 == 0)
          {
            sb.Remove(sb.Length - 1, 1);
            sb.Append("\n");
          }
        }
      }
      else
      {
        var values = Value;
        string formatStr = $"f{mDecimalCount}";
        for (int i = 0; i < values.Length; ++i)
        {
          sb.Append($"{values[i].ToString(formatStr, CultureInfo.InvariantCulture)},");
          if ((i + 1) % 8 == 0)
          {
            sb.Remove(sb.Length - 1, 1);
            sb.Append("\n");
          }
        }
      }
      sb.Remove(sb.Length - 1, 1);
      return sb.ToString();
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.CURVE value.
  /// 
  /// The ICharValue.Value in this case is of type double[].
  /// </summary>
  public sealed class CurveValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public CurveValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public CurveValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[] Value
    {
      get { return (double[])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    protected override bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells)
    {
      var cm = mCharacteristic.RefCompuMethod;
      var layout = mCharacteristic.RefRecordLayout;
      double minInc = double.NaN;
      switch (cm.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          break;
        default:
          minInc = cm.getMinIncrement(mCharacteristic.LowerLimit, mCharacteristic.UpperLimit
            , mDecimalCount, layout.FncValues.DataType
            );
          break;
      }
      var targetValues = Value;
      for (int i = 0; i < cells.Count; ++i)
      {
        var cell = cells[i];
        double singleTargetValue = targetValues[cell.ColumnIndex];
        if (!double.IsNaN(minInc))
          for (int j = 0; j < count; ++j)
            singleTargetValue = increment ? singleTargetValue + minInc : singleTargetValue - minInc;
        else
          singleTargetValue = getNextValue(increment, singleTargetValue, layout.FncValues.DataType, cm);

        if (mValueFormat == ValueObjectFormat.Physical)
        { // restrict physical values to physical limits
          singleTargetValue = Math.Max(mCharacteristic.LowerLimit, singleTargetValue);
          singleTargetValue = Math.Min(mCharacteristic.UpperLimit, singleTargetValue);
        }
        targetValues[cell.ColumnIndex] = singleTargetValue;
      }
      Value = targetValues;
      return CharacteristicValue.setValue<CurveValue>(dataFile, (A2LCHARACTERISTIC)mCharacteristic, this);
    }
    public override PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells)
    {
      double[,] values;
      PasteResult result;
      if (PasteResult.Ok != (result = canPasteFromClipboard(cbString, targetCells, out values)))
        return result;

      Rectangle boundingRect;
      getBoundingRect(targetCells, out boundingRect);

      for (int x = 0; x < values.GetLength(0); ++x)
        Value[x + boundingRect.X] = values[x, 0];

      CharacteristicValue.setValue(dataFile, this);
      return PasteResult.Ok;
    }
    public override string ToString()
    {
      var sb = new StringBuilder(string.IsNullOrEmpty(mUnitAxis[0]) ? "Axis\t" : $"Axis[{mUnitAxis[0]:20}]\t");
      var xValues = mAxisValue[0];
      string xFormatStr = $"f{mDecimalCountAxis[0]}";
      for (int i = 0; i < xValues.Length; ++i)
        sb.Append($"{xValues[i].ToString(xFormatStr, CultureInfo.InvariantCulture)},");
      sb.Remove(sb.Length - 1, 1);

      if (string.IsNullOrEmpty(mUnit))
        sb.Append("\nValues\t");
      else
        sb.Append($"\nValues[{mUnit:20}]\t");
      var values = Value;
      string formatStr = $"f{mDecimalCount}";
      for (int i = 0; i < values.Length; ++i)
        sb.Append($"{values[i].ToString(formatStr, CultureInfo.InvariantCulture)},");
      sb.Remove(sb.Length - 1, 1);
      return sb.ToString();
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.MAP value.
  /// 
  /// The ICharValue.Value in this case is of type double[,].
  /// </summary>
  public sealed class MapValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public MapValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public MapValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[,] Value
    {
      get { return (double[,])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    protected override bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells)
    {
      var cm = mCharacteristic.RefCompuMethod;
      var layout = mCharacteristic.RefRecordLayout;
      double minInc = double.NaN;
      switch (cm.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          break;
        default:
          minInc = cm.getMinIncrement(mCharacteristic.LowerLimit, mCharacteristic.UpperLimit
            , mDecimalCount, layout.FncValues.DataType
            );
          break;
      }
      var targetValues = Value;
      for (int i = 0; i < cells.Count; ++i)
      {
        DataGridViewCell cell = cells[i];
        double singleTargetValue = targetValues[cell.ColumnIndex, cell.RowIndex];
        if (!double.IsNaN(minInc))
        { // minimum increments valid
          // increment or decrement value according the increments count
          for (int j = 0; j < count; ++j)
            singleTargetValue = increment ? singleTargetValue + minInc : singleTargetValue - minInc;
        }
        else
          // Compute the next value (TAB_VERB/TAB_NOINTP
          singleTargetValue = getNextValue(increment, singleTargetValue, layout.FncValues.DataType, cm);

        if (mValueFormat == ValueObjectFormat.Physical)
        { // restrict physical values to physical limits
          singleTargetValue = Math.Max(mCharacteristic.LowerLimit, singleTargetValue);
          singleTargetValue = Math.Min(mCharacteristic.UpperLimit, singleTargetValue);
        }
        targetValues[cell.ColumnIndex, cell.RowIndex] = singleTargetValue;
      }
      Value = targetValues;
      return CharacteristicValue.setValue<MapValue>(dataFile, (A2LCHARACTERISTIC)mCharacteristic, this);
    }
    public override PasteResult pasteClipboardString(string cbString, IDataFile dataFile, DataGridViewSelectedCellCollection targetCells)
    {
      double[,] values;
      PasteResult result;
      if (PasteResult.Ok != (result = canPasteFromClipboard(cbString, targetCells, out values)))
        return result;

      Rectangle boundingRect;
      getBoundingRect(targetCells, out boundingRect);

      for (int y = 0; y < values.GetLength(1); ++y)
      {
        for (int x = 0; x < values.GetLength(0); ++x)
          Value[x + boundingRect.X, y + boundingRect.Y] = values[x, y];
      }

      CharacteristicValue.setValue(dataFile, this);
      return PasteResult.Ok;
    }
    public override string ToString()
    {
      var sb = new StringBuilder(string.IsNullOrEmpty(mUnitAxis[0]) ? "X\t\t" : $"X[{mUnitAxis[0]}]\t\t");
      var xValues = mAxisValue[0];
      string xFormatStr = $"f{mDecimalCountAxis[0]}";
      for (int i = 0; i < xValues.Length; ++i)
        sb.Append($"{xValues[i].ToString(xFormatStr, CultureInfo.InvariantCulture)},");
      sb.Remove(sb.Length - 1, 1);

      if (string.IsNullOrEmpty(mUnit))
      {
        if (string.IsNullOrEmpty(mUnitAxis[1]))
          sb.Append("\nY");
        else
          sb.Append($"\nY[{mUnitAxis[1]:5}]");
      }
      else
      {
        if (string.IsNullOrEmpty(mUnitAxis[1]))
          sb.Append($"\nY\t\t[{mUnit}]");
        else
          sb.Append($"\nY[{mUnitAxis[1]:5}]\t\t[{mUnit}]");
      }

      string yFormatStr = $"f{mDecimalCountAxis[1]}";
      var values = Value;
      string formatStr = $"f{mDecimalCount}";
      for (int y = 0; y < values.GetLength(1); ++y)
      {
        for (int x = 0; x < values.GetLength(0); ++x)
        {
          if (x == 0)
            sb.Append($"\n{mAxisValue[1][y].ToString(yFormatStr, CultureInfo.InvariantCulture)}\t\t");
          sb.Append($"{values[x, y].ToString(formatStr, CultureInfo.InvariantCulture)},");
        }
        sb.Remove(sb.Length - 1, 1);
      }
      return sb.ToString();
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.CUBOID value.
  /// 
  /// The ICharValue.Value in this case is of type double[,,].
  /// </summary>
  public sealed class CuboidValue : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public CuboidValue() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public CuboidValue(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[,,] Value
    {
      get { return (double[,,])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    protected override bool incrementOrDecrement(IDataFile dataFile, bool increment, int count, DataGridViewSelectedCellCollection cells, int zIndex)
    {
      var cm = mCharacteristic.RefCompuMethod;
      var layout = mCharacteristic.RefRecordLayout;
      double minInc = double.NaN;
      switch (cm.ConversionType)
      {
        case CONVERSION_TYPE.TAB_NOINTP:
        case CONVERSION_TYPE.TAB_VERB:
          break;
        default:
          minInc = cm.getMinIncrement(mCharacteristic.LowerLimit, mCharacteristic.UpperLimit
            , mDecimalCount, layout.FncValues.DataType
            );
          break;
      }
      var targetValues = Value;
      for (int i = 0; i < cells.Count; ++i)
      {
        var cell = cells[i];
        double singleTargetValue = targetValues[cell.ColumnIndex, cell.RowIndex, zIndex];
        if (!double.IsNaN(minInc))
          for (int j = 0; j < count; ++j)
            singleTargetValue = increment ? singleTargetValue + minInc : singleTargetValue - minInc;
        else
          singleTargetValue = getNextValue(increment, singleTargetValue, layout.FncValues.DataType, cm);

        if (mValueFormat == ValueObjectFormat.Physical)
        { // restrict physical values to physical limits
          singleTargetValue = Math.Max(mCharacteristic.LowerLimit, singleTargetValue);
          singleTargetValue = Math.Min(mCharacteristic.UpperLimit, singleTargetValue);
        }
        targetValues[cell.ColumnIndex, cell.RowIndex, zIndex] = singleTargetValue;
      }
      Value = targetValues;
      return CharacteristicValue.setValue<CuboidValue>(dataFile, (A2LCHARACTERISTIC)mCharacteristic, this);
    }
    public override string ToString()
    {
      var sb = new StringBuilder();
      // todo cuboid
      return sb.ToString();
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.CUBE_4 value.
  /// 
  /// The ICharValue.Value in this case is of type double[,,,].
  /// </summary>
  public sealed class Cube4Value : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public Cube4Value() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public Cube4Value(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[,,,] Value
    {
      get { return (double[,,,])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    public override string ToString()
    {
      var sb = new StringBuilder();
      // todo cube4
      return sb.ToString();
    }
  }
  /// <summary>
  /// Class representing a characteristic CHAR_TYPE.CUBE_5 value.
  /// 
  /// The ICharValue.Value in this case is of type double[,,,,].
  /// </summary>
  public sealed class Cube5Value : BaseValue
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public Cube5Value() { }
    /// <summary>
    /// Specific constructor
    /// 
    /// Used from DCM import
    /// </summary>
    /// <param name="characteristic">The corresponding characteristic</param>
    public Cube5Value(A2LRECORD_LAYOUT_REF characteristic) : base(characteristic) { }
    double[,,,,] Value
    {
      get { return (double[,,,,])((ICharValue)this).Value; }
      set { ((ICharValue)this).Value = value; }
    }
    public override string ToString()
    {
      var sb = new StringBuilder();
      // todo cube5
      return sb.ToString();
    }
  }
}