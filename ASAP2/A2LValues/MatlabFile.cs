﻿/*!
 * @file    
 * @brief   Implements the Matlab file parser.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    May 2013
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Implements the Matlab (*.m) file format reader/writer.
  /// </summary>
  public sealed class MatlabFile : DataConservation
  {
    static readonly char[] mDCMCommentedLine = new char[] { '%' };
    static readonly char[] mArrayTrim = new char[] { '[', ']' };
    static Regex mCmdMatch = new Regex("(\\w+)\\s+=\\s+([-\\d\\.]+|\\[[-\\d\\.\\s]+\\])", RegexOptions.None);
    static readonly char[] mAxisChars = { 'X', 'Y', 'Z', '4', '5' };
    const string mAxisKeyFormat = "{0}_{1}_AXIS";
    /// <summary>
    /// Saves all characteristics of the module into a Matlab file.
    /// </summary>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="addParameterDesc">Add parameter descriptions as comments</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      , bool addParameterDesc
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var layoutRef in module.enumChildNodes<A2LRECORD_LAYOUT_REF>())
      {
        ICharValue value = null;
        if (layoutRef is A2LAXIS_PTS)
          value = CharacteristicValue.getValue<ValBlkValue>(dataFile, (A2LAXIS_PTS)layoutRef, ValueObjectFormat.Physical);
        else
        {
          var characteristic = layoutRef as A2LCHARACTERISTIC;
          switch (characteristic.CharType)
          {
            case CHAR_TYPE.VALUE: value = CharacteristicValue.getValue<SingleValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.ASCII: value = CharacteristicValue.getValue<ASCIIValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.VAL_BLK: value = CharacteristicValue.getValue<ValBlkValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.CURVE: value = CharacteristicValue.getValue<CurveValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.MAP: value = CharacteristicValue.getValue<MapValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.CUBOID: value = CharacteristicValue.getValue<CuboidValue>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.CUBE_4: value = CharacteristicValue.getValue<Cube4Value>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            case CHAR_TYPE.CUBE_5: value = CharacteristicValue.getValue<Cube5Value>(dataFile, characteristic, ValueObjectFormat.Physical); break;
            default: throw new NotSupportedException(characteristic.CharType.ToString());
          }
        }
        if (value == null)
          continue;
        charValues.Add(value);
      }
      var dict = ((A2LPROJECT)module.Parent).FuncDict;
      var functions = new A2LFUNCTION[dict.Count];
      dict.Values.CopyTo(functions, 0);
      return MatlabFile.save(path, description, charValues.ToArray(), functions, addParameterDesc);
    }
    /// <summary>
    /// Saves the specified characteristics into a Matlab file.
    /// </summary>
    /// <param name="dataFile">The datafile to read the values from</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="characteristics">The characteristics to save</param>
    /// <param name="exportFunctions">true if functions should be exported, too</param>
    /// <param name="addParameterDesc">Add parameter descriptions as comments</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(IDataFile dataFile
      , A2LMODULE module
      , string path
      , string description
      , A2LCHARACTERISTIC[] characteristics
      , bool exportFunctions
      , bool addParameterDesc
      )
    {
      // Iterate over any existing characteristic
      var charValues = new List<ICharValue>();
      foreach (var characteristic in characteristics)
      {
        ICharValue value = CharacteristicValue.getValue<SingleValue>(dataFile, characteristic, ValueObjectFormat.Physical);
        if (value == null)
          continue;
        charValues.Add(value);
      }
      A2LFUNCTION[] functions = null;
      if (exportFunctions)
      {
        var dict = ((A2LPROJECT)module.Parent).FuncDict;
        functions = new A2LFUNCTION[dict.Count];
        dict.Values.CopyTo(functions, 0);
      }

      var modPar = module.getNode<A2LMODPAR>(false);
      description = string.IsNullOrEmpty(description)
        ? string.Format("EPK: {0}", modPar.EPK)
        : string.Format("EPK: {0}{1}{1}{2}", modPar.EPK, mEOL, description);
      return save(path, description, charValues.ToArray(), functions, addParameterDesc);
    }
    /// <summary>
    /// Saves the specified characteristics into a Matlab file.
    /// </summary>
    /// <param name="path">the fully qualified path to the destination file</param>
    /// <param name="description">an optional description for the file header
    /// (The '\n' delimiter is supported for multiline descriptions)
    /// </param>
    /// <param name="values">The values to save into the Matlab file</param>
    /// <param name="functions">Functions to save into the Matlab file (may be null or empty)</param>
    /// <param name="addParameterDesc">Add parameter descriptions as comments</param>
    /// <returns>count of written characteristics, -1 if failed</returns>
    public static int save(string path
      , string description
      , ICharValue[] values
      , A2LFUNCTION[] functions
      , bool addParameterDesc
      )
    {
      try
      {
        using (var sw = new StreamWriter(path, false, Encoding.Default))
        {
          int counter = 0;
          sw.WriteLine(string.Format(Resources.strMatlabCreatedBy
            , Application.ProductName
            , Application.ProductVersion
            ));
          var now = DateTime.Now;
          sw.WriteLine(string.Format(Resources.strMatlabCreatedAt
            , now.ToShortDateString()
            , now.ToLongTimeString()
            ));

          if (!string.IsNullOrEmpty(description))
          { // add file description
            var splits = description.Split(new string[] { mEOL }, StringSplitOptions.None);
            for (int i = 0; i < splits.Length; ++i)
              sw.WriteLine(string.Format("% {0}", splits[i].Trim(' ')));
          }
          sw.WriteLine();

          // Write the functions - not supported in a matlab file

          // Write the values
          foreach (var value in values)
          {
            if (addParameterDesc)
            { // try add description
              string strDesc = tryGetDescription(value.Characteristic);
              if (!string.IsNullOrEmpty(strDesc))
                sw.Write(strDesc);
            }
            var characteristic = value.Characteristic as A2LCHARACTERISTIC;
            if (null != characteristic)
            {
              switch (characteristic.CharType)
              {
                case CHAR_TYPE.VALUE:
                  if (value.Value is double && double.IsNaN((double)value.Value))
                    // filter out NaN values
                    continue;
                  bool isString = characteristic.RefCompuMethod.RefCompuTab is A2LCOMPU_VTAB;
                  if (isString)
                    sw.WriteLine("{0} = {1};", characteristic.Name, ((double)value.Value).ToString(CultureInfo.InvariantCulture));
                  else
                    sw.WriteLine("{0} = {1};", characteristic.Name, value.toSingleValue());
                  break;
                case CHAR_TYPE.ASCII:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;
                  sw.Write("{0} = [", characteristic.Name);
                  string strValue = (string)value.Value;
                  for (int i = 0; i < strValue.Length; ++i)
                  {
                    sw.Write("{0}", (byte)strValue[i]);
                    if (i < strValue.Length - 1)
                      sw.Write(' ');
                  }
                  sw.WriteLine("];");
                  break;
                case CHAR_TYPE.VAL_BLK:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;
                  sw.Write("{0} = [", characteristic.Name);
                  var blkkValues = (double[])value.Value;
                  for (int i = 0; i < blkkValues.Length; ++i)
                  {
                    sw.Write("{0}", blkkValues[i].ToString(CultureInfo.InvariantCulture));
                    if (i < blkkValues.Length - 1)
                      sw.Write(' ');
                  }
                  sw.WriteLine("];");
                  break;
                case CHAR_TYPE.CURVE:
                  if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                    // filter out NaN values
                    continue;

                  // function values
                  sw.Write("{0} = [", characteristic.Name);
                  blkkValues = (double[])value.Value;
                  for (int i = 0; i < blkkValues.Length; ++i)
                  {
                    sw.Write("{0}", blkkValues[i].ToString(CultureInfo.InvariantCulture));
                    if (i < blkkValues.Length - 1)
                      sw.Write(' ');
                  }
                  sw.WriteLine("];");

                  // Get axis descriptions
                  var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  // x axis values
                  writeAxis(sw, 0, axisDescs[0], characteristic, value.AxisValue[0], addParameterDesc);
                  break;
                case CHAR_TYPE.MAP:
                  if (value.Value is double[,] && double.IsNaN(((double[,])value.Value)[0, 0]))
                    // filter out NaN values
                    continue;

                  // function values
                  sw.Write("{0} = [", characteristic.Name);
                  var mapValues = (double[,])value.Value;
                  int maxX = mapValues.GetLength(0);
                  int maxY = mapValues.GetLength(1);
                  for (int y = 0; y < maxY; ++y)
                  {
                    for (int x = 0; x < maxX; ++x)
                    {
                      sw.Write("{0}", mapValues[x, y].ToString(CultureInfo.InvariantCulture));
                      if (x < maxX - 1)
                        sw.Write(' ');
                    }
                    if (y < maxY - 1)
                      sw.Write(mEOL);
                  }
                  sw.WriteLine("];");

                  // Get axis descriptions
                  axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  // x, y axis values
                  writeAxis(sw, 0, axisDescs[0], characteristic, value.AxisValue[0], addParameterDesc);
                  writeAxis(sw, 1, axisDescs[1], characteristic, value.AxisValue[1], addParameterDesc);
                  break;
                case CHAR_TYPE.CUBOID:
                  if (value.Value is double[,,] && double.IsNaN(((double[,,])value.Value)[0, 0, 0]))
                    // filter out NaN values
                    continue;

                  // function values
                  sw.Write("{0} = [", characteristic.Name);
                  var cubeValues = (double[,,])value.Value;
                  maxX = cubeValues.GetLength(0);
                  maxY = cubeValues.GetLength(1);
                  int maxZ = cubeValues.GetLength(2);
                  for (int z = 0; z < maxZ; ++z)
                    for (int y = 0; y < maxY; ++y)
                    {
                      for (int x = 0; x < maxX; ++x)
                      {
                        sw.Write("{0}", cubeValues[x, y, z].ToString(CultureInfo.InvariantCulture));
                        if (x < maxX - 1)
                          sw.Write(' ');
                      }
                      if (y < maxY - 1)
                        sw.Write(mEOL);
                    }
                  sw.WriteLine("];");

                  // Get axis descriptions
                  axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  // x, y, z axis values
                  writeAxis(sw, 0, axisDescs[0], characteristic, value.AxisValue[0], addParameterDesc);
                  writeAxis(sw, 1, axisDescs[1], characteristic, value.AxisValue[1], addParameterDesc);
                  writeAxis(sw, 2, axisDescs[2], characteristic, value.AxisValue[2], addParameterDesc);
                  break;
                case CHAR_TYPE.CUBE_4:
                  if (value.Value is double[,,,] && double.IsNaN(((double[,,,])value.Value)[0, 0, 0, 0]))
                    // filter out NaN values
                    continue;

                  // function values
                  sw.Write("{0} = [", characteristic.Name);
                  var cube4Values = (double[,,,])value.Value;
                  maxX = cube4Values.GetLength(0);
                  maxY = cube4Values.GetLength(1);
                  maxZ = cube4Values.GetLength(2);
                  int max4 = cube4Values.GetLength(3);
                  for (int u = 0; u < max4; ++u)
                    for (int z = 0; z < maxZ; ++z)
                      for (int y = 0; y < maxY; ++y)
                      {
                        for (int x = 0; x < maxX; ++x)
                        {
                          sw.Write("{0}", cube4Values[x, y, z, u].ToString(CultureInfo.InvariantCulture));
                          if (x < maxX - 1)
                            sw.Write(' ');
                        }
                        if (y < maxY - 1)
                          sw.Write(mEOL);
                      }
                  sw.WriteLine("];");

                  // Get axis descriptions
                  axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  // x, y, z, 4 axis values
                  writeAxis(sw, 0, axisDescs[0], characteristic, value.AxisValue[0], addParameterDesc);
                  writeAxis(sw, 1, axisDescs[1], characteristic, value.AxisValue[1], addParameterDesc);
                  writeAxis(sw, 2, axisDescs[2], characteristic, value.AxisValue[2], addParameterDesc);
                  writeAxis(sw, 3, axisDescs[3], characteristic, value.AxisValue[3], addParameterDesc);
                  break;
                case CHAR_TYPE.CUBE_5:
                  if (value.Value is double[,,,,] && double.IsNaN(((double[,,,,])value.Value)[0, 0, 0, 0, 0]))
                    // filter out NaN values
                    continue;

                  // function values
                  sw.Write("{0} = [", characteristic.Name);
                  var cube5Values = (double[,,,,])value.Value;
                  maxX = cube5Values.GetLength(0);
                  maxY = cube5Values.GetLength(1);
                  maxZ = cube5Values.GetLength(2);
                  max4 = cube5Values.GetLength(3);
                  int max5 = cube5Values.GetLength(4);
                  for (int v = 0; v < max5; ++v)
                    for (int u = 0; u < max4; ++u)
                      for (int z = 0; z < maxZ; ++z)
                        for (int y = 0; y < maxY; ++y)
                        {
                          for (int x = 0; x < maxX; ++x)
                          {
                            sw.Write("{0}", cube5Values[x, y, z, u, v].ToString(CultureInfo.InvariantCulture));
                            if (x < maxX - 1)
                              sw.Write(' ');
                          }
                          if (y < maxY - 1)
                            sw.Write(mEOL);
                        }
                  sw.WriteLine("];");

                  // Get axis descriptions
                  axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
                  // x, y, z, 4, 5 axis values
                  writeAxis(sw, 0, axisDescs[0], characteristic, value.AxisValue[0], addParameterDesc);
                  writeAxis(sw, 1, axisDescs[1], characteristic, value.AxisValue[1], addParameterDesc);
                  writeAxis(sw, 2, axisDescs[2], characteristic, value.AxisValue[2], addParameterDesc);
                  writeAxis(sw, 3, axisDescs[3], characteristic, value.AxisValue[3], addParameterDesc);
                  writeAxis(sw, 4, axisDescs[4], characteristic, value.AxisValue[4], addParameterDesc);
                  break;
                default: throw new NotSupportedException(characteristic.CharType.ToString());
              }
            }
            else
            { // A2LAxisPts
              // common axis values are not really needed in this 
              // case as any object is written including it's axis values!
              if (value.Value is double[] && double.IsNaN(((double[])value.Value)[0]))
                // filter out NaN values
                continue;
              sw.Write("{0} = [", value.Characteristic.Name);
              var blkkValues = (double[])value.Value;
              for (int i = 0; i < blkkValues.Length; ++i)
              {
                sw.Write("{0}", blkkValues[i].ToString(CultureInfo.InvariantCulture));
                if (i < blkkValues.Length - 1)
                  sw.Write(' ');
              }
              sw.WriteLine("];");
            }
            ++counter;
          }
          return counter;
        }
      }
      catch { return -1; }
    }
    /// <summary>
    /// Writes the axis values part for the specified axis
    /// </summary>
    /// <param name="sw">The stream to write to</param>
    /// <param name="index">The axis index</param>
    /// <param name="axisDesc">THe axis description</param>
    /// <param name="characteristic">The corresponding characteristic</param>
    /// <param name="value">The axis values</param>
    /// <param name="addParameterDesc">Add parameter descriptions as comments</param>
    static void writeAxis(StreamWriter sw
      , int index
      , A2LAXIS_DESCR axisDesc
      , A2LCHARACTERISTIC characteristic
      , double[] values
      , bool addParameterDesc
      )
    {
      if (addParameterDesc)
        sw.Write(string.IsNullOrEmpty(axisDesc.Unit)
          ? string.Empty
          : string.Format("% Unit: {0}{1}", axisDesc.Unit, Environment.NewLine)
          );

      sw.Write("{0}_{1}_AXIS = [", characteristic.Name, mAxisChars[index]);
      for (int i = 0; i < values.Length; ++i)
      {
        sw.Write("{0}", values[i].ToString(CultureInfo.InvariantCulture));
        if (i < values.Length - 1)
          sw.Write(' ');
      }
      sw.WriteLine("];");
    }
    /// <summary>
    /// Opens a Matlab file and returns a MatlabFile instance.
    /// </summary>
    /// <param name="path">the fully qualified Matlab filename</param>
    /// <param name="module">The module to read the characteristics from</param>
    /// <returns>A new MatlabFile instance or null if failed</returns>
    public static DataConservation open(string path, A2LMODULE module)
    {
      var instance = new MatlabFile();
      var project = module.getParent<A2LPROJECT>();
      var charDict = project.CharDict;
      var commandList = new StringBuilder();
      using (var sr = new StreamReader(path, Encoding.Default))
      {
        string line;
        while (null != (line = sr.ReadLine()))
        {
          line = line.Trim(mDelimiters);
          if (string.IsNullOrEmpty(line))
            // line is empty
            continue;
          int commentIndex;
          if (0 == (commentIndex = line.IndexOfAny(mDCMCommentedLine)))
            // line contains a comment
            line = line.Substring(0, commentIndex);

          if (string.IsNullOrEmpty(line))
            // line is empty
            continue;
          commandList.AppendFormat(" {0}", line);
        }
      }

      var completeMap = new Dictionary<string, ICharValue>();
      var axisValues = new Dictionary<string, double[]>();
      string[] commands = commandList.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
      for (int i = 0; i < commands.Length; ++i)
      {
        var matches = mCmdMatch.Matches(commands[i]);
        int count = matches.Count;
        for (int j = 0; j < count; ++j)
        {
          var m = matches[j];
          string name = m.Groups[1].Value;
          A2LRECORD_LAYOUT_REF layoutRef;
          ICharValue charValue = null;
          if (!charDict.TryGetValue(name, out layoutRef))
          { // no direct object found
            if (name.EndsWith("_AXIS"))
            { // test for corresponding axis object
              string axisName = name.Substring(0, name.Length - 7);
              if (charDict.TryGetValue(axisName, out layoutRef))
              { // corresponding object found
                var splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                var blkValues = new double[splits.Length];
                for (int k = 0; k < splits.Length; ++k)
                  blkValues[k] = A2LParser.parse2DoubleVal(splits[k]);
                axisValues[name] = blkValues;
              }
            }
            continue;
          }

          var characteristic = layoutRef as A2LCHARACTERISTIC;
          if (characteristic == null)
          { // A2LAxisPts
            charValue = new ValBlkValue(layoutRef);
            var splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
            var blkValue = new double[splits.Length];
            for (int k = 0; k < splits.Length; ++k)
              blkValue[k] = A2LParser.parse2DoubleVal(splits[k]);
            charValue.Value = blkValue;
          }
          else
          { // valid Characteristic
            var axisDescs = characteristic.getNodeList<A2LAXIS_DESCR>();
            switch (characteristic.CharType)
            {
              case CHAR_TYPE.VALUE:
                charValue = new SingleValue(characteristic);
                charValue.Value = A2LParser.parse2DoubleVal(m.Groups[2].Value);
                break;
              case CHAR_TYPE.ASCII:
                charValue = new ASCIIValue(characteristic);
                var splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                var str = new StringBuilder();
                for (int k = 0; k < splits.Length; ++k)
                  str.Append((char)byte.Parse(splits[k]));
                charValue.Value = str.ToString();
                break;
              case CHAR_TYPE.VAL_BLK:
                charValue = new ValBlkValue(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                var blkValue = new double[splits.Length];
                for (int k = 0; k < splits.Length; ++k)
                  blkValue[k] = A2LParser.parse2DoubleVal(splits[k]);
                charValue.Value = blkValue;
                break;
              case CHAR_TYPE.CURVE:
                charValue = new CurveValue(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                int maxX = axisDescs[0].MaxAxisPoints;
                if (splits.Length > maxX)
                  continue;
                blkValue = new double[maxX];
                for (int k = 0; k < splits.Length; ++k)
                  blkValue[k] = A2LParser.parse2DoubleVal(splits[k]);
                charValue.Value = blkValue;
                break;
              case CHAR_TYPE.MAP:
                charValue = new MapValue(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                maxX = axisDescs[0].MaxAxisPoints;
                int maxY = axisDescs[1].MaxAxisPoints;
                if (splits.Length > maxX * maxY)
                  continue;
                var mapValues = new double[maxX, maxY];
                int current = 0;
                for (int y = 0; y < maxY; ++y)
                  for (int x = 0; x < maxX; ++x)
                    mapValues[x, y] = A2LParser.parse2DoubleVal(splits[current++]);
                charValue.Value = mapValues;
                break;
              case CHAR_TYPE.CUBOID:
                charValue = new CuboidValue(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                maxX = axisDescs[0].MaxAxisPoints;
                maxY = axisDescs[1].MaxAxisPoints;
                int maxZ = axisDescs[2].MaxAxisPoints;
                if (splits.Length > maxX * maxY * maxZ)
                  continue;
                var cuboidValues = new double[maxX, maxY, maxZ];
                current = 0;
                for (int z = 0; z < maxZ; ++z)
                  for (int y = 0; y < maxY; ++y)
                    for (int x = 0; x < maxX; ++x)
                      cuboidValues[x, y, z] = A2LParser.parse2DoubleVal(splits[current++]);
                charValue.Value = cuboidValues;
                break;
              case CHAR_TYPE.CUBE_4:
                charValue = new Cube4Value(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                maxX = axisDescs[0].MaxAxisPoints;
                maxY = axisDescs[1].MaxAxisPoints;
                maxZ = axisDescs[2].MaxAxisPoints;
                int max4 = axisDescs[3].MaxAxisPoints;
                if (splits.Length > maxX * maxY * maxZ * max4)
                  continue;
                var cube4Values = new double[maxX, maxY, maxZ, max4];
                current = 0;
                for (int u = 0; u < max4; ++u)
                  for (int z = 0; z < maxZ; ++z)
                    for (int y = 0; y < maxY; ++y)
                      for (int x = 0; x < maxX; ++x)
                        cube4Values[x, y, z, u] = A2LParser.parse2DoubleVal(splits[current++]);
                charValue.Value = cube4Values;
                break;
              case CHAR_TYPE.CUBE_5:
                charValue = new Cube5Value(characteristic);
                splits = m.Groups[2].Value.Trim(mArrayTrim).Split(mDelimiters);
                maxX = axisDescs[0].MaxAxisPoints;
                maxY = axisDescs[1].MaxAxisPoints;
                maxZ = axisDescs[2].MaxAxisPoints;
                max4 = axisDescs[3].MaxAxisPoints;
                int max5 = axisDescs[4].MaxAxisPoints;
                if (splits.Length > maxX * maxY * maxZ * max4 * max5)
                  continue;
                var cube5Values = new double[maxX, maxY, maxZ, max4, max5];
                current = 0;
                for (int v = 0; v < max4; ++v)
                  for (int u = 0; u < max4; ++u)
                    for (int z = 0; z < maxZ; ++z)
                      for (int y = 0; y < maxY; ++y)
                        for (int x = 0; x < maxX; ++x)
                          cube5Values[x, y, z, u, v] = A2LParser.parse2DoubleVal(splits[current++]);
                charValue.Value = cube5Values;
                break;
              default: throw new NotSupportedException(characteristic.CharType.ToString());
            }
          }
          // add the value (may never be null)
          completeMap[name] = charValue;
        }
      }

      // Get the corresponding axis values
      Dictionary<string, ICharValue>.Enumerator en = completeMap.GetEnumerator();
      while (en.MoveNext())
      { // Iterate over any parsed value
        CHAR_TYPE type = en.Current.Value.CharType;
        int max = type == CHAR_TYPE.CURVE
          ? 1
          : type == CHAR_TYPE.MAP
            ? 2
            : type == CHAR_TYPE.CUBOID
            ? 3
            : type == CHAR_TYPE.CUBE_4
              ? 4
              : type == CHAR_TYPE.CUBE_5
              ? 5
              : -1;
        if (max < 0)
          // filter non axis values
          continue;

        var newValues = new double[max][];
        for (int index = 0; index < max; ++index)
        { // add all parsed axis values
          var key = string.Format(mAxisKeyFormat, en.Current.Key, mAxisChars[index]);
          if (axisValues.TryGetValue(key, out newValues[index]))
            continue;
          // Error, missing axis values!
        }
        // Takeover the values
        en.Current.Value.AxisValue = newValues;
      }

      // Takeover any parsed values
      en = completeMap.GetEnumerator();
      while (en.MoveNext())
        instance.Values.Add(en.Current.Value);
      return instance;
    }
    /// <summary>
    /// Provides a description text of the specified characteristic in Matlab comment format
    /// </summary>
    /// <param name="convRef">The characteristic to create the description for</param>
    /// <returns>The description strng (may be empty)</returns>
    static string tryGetDescription(A2LCONVERSION_REF convRef)
    {
      var desc = string.Format("{0}{1}", string.IsNullOrEmpty(convRef.Description) ? string.Empty : convRef.Description
        , string.IsNullOrEmpty(convRef.Unit) ? string.Empty : string.Format("\nUnit: {0}", convRef.Unit)
        );
      var descLines = desc.Split(mLineDelimiter, StringSplitOptions.RemoveEmptyEntries);
      var sb = new StringBuilder(Environment.NewLine);
      foreach (var line in descLines)
        sb.AppendFormat("% {0}{1}", line, Environment.NewLine);
      return sb.ToString();
    }
  }
}