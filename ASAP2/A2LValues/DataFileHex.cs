/*!
 * @file    
 * @brief   Class for reading and writing Intel hex files.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2011
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Class for reading/writing Intel HEX files.
  /// 
  /// - This class gets constructed by the static DataFile.open method.
  /// </summary>
  public sealed class DataFileHex : DataFile, IDataFile
  {
    #region Record types
    /// <summary>
    /// HEX file record types.
    /// </summary>
    enum RecordType
    {
      /// <summary>
      /// data record, contains data and 16-bit address.
      /// </summary>
      Data,
      /// <summary>
      /// End Of File record. 
      /// 
      /// Must occur exactly once per file in the last line of the file.
      /// The byte count is 00 and the data field is empty. Usually the 
      /// address field is also 0000, in which case the complete line is 
      /// ':00000001FF'. Originally the End Of File record could contain 
      /// a start address for the program being loaded, e.g. :00AB2F0125 
      /// would cause a jump to address AB2F. This was convenient when 
      /// programs were loaded from punched paper tape.
      /// </summary>
      Eof,
      /// <summary>
      /// Extended Segment Address Record.
      /// 
      /// Segment-base address (two hex  digit pairs in big endian order).
      /// Used when 16 bits are not enough, identical to 80x86 real 
      /// mode addressing. The address specified by the data field of the 
      /// most recent 02 record is multiplied by 16 (shifted 4 bits left) 
      /// and added to the subsequent 00 record addresses. This allows 
      /// addressing of up to a megabyte of address space. The address 
      /// field of this record has to be 0000, the byte count is 02 
      /// (the segment is 16-bit). The least significant hex digit of the 
      /// segment address is always 0.
      /// </summary>
      ExtendedSegmentAddr,
      /// <summary>
      /// Start Segment Address Record.
      /// 
      /// For 80x86 processors, it specifies the initial content of the CS:IP 
      /// registers. The address field is 0000, the byte count is 04, the first 
      /// two bytes are the CS value, the latter two are the IP value.
      /// </summary>
      StartSegmentAddr,
      /// <summary>
      /// Extended Linear Address Record.
      /// 
      /// Allowing for fully 32 bit addressing (up to 4GiB). The address field 
      /// is 0000, the byte count is 02. The two data bytes (two hex digit pairs
      /// in big endian order) represent the upper 16 bits of the 32 bit address
      /// for all subsequent 00 type records until the next 04 type record comes.
      /// If there is not a 04 type record, the upper 16 bits default to 0000.
      /// To get the absolute address for subsequent 00 type records, the address 
      /// specified by the data field of the most recent 04 record is added to the 
      /// 00 record addresses.
      /// </summary>
      ExtendedLinearAddr,
      /// <summary>
      /// Start Linear Address Record.
      /// 
      /// The address field is 0000, the byte count is 04. The 4 data bytes represent
      /// the 32-bit value loaded into the EIP register of the 80386 and higher CPU.
      /// </summary>
      StartLinearAddr,
    }
    #endregion
    #region members
    byte mDataBytesPerLine = 32;
    static readonly string mEOFFile = ":00000001FF";
    #endregion
    #region Constructor
    /// <summary>
    /// Constructs a new instance of this class.
    /// 
    /// Initialize the memory segment list with the
    /// specified segments.
    /// </summary>
    /// <param name="filename">The fully qualified path to the source filename.</param>
    /// <param name="segments">Segments to initialize with, may be null or empty.</param>
    public DataFileHex(string filename, MemorySegmentList segments)
      : base(filename, segments)
    {
    }
    #endregion
    #region properties
    HashSet<A2LRECORD_LAYOUT_REF> IDataFile.ChangedValues { get { return base.ChangedValues; } }
    #endregion
    #region methods
    MemorySegment IDataFile.findMemSeg(UInt32 address) { return findMemSeg(address, 1); }
    bool IDataFile.IsDirty
    {
      get { return base.IsDirty; }
      set { base.IsDirty = value; }
    }
    string IDataFile.SourceFilename { get { return base.SourceFilename; } }
    bool IDataFile.checkEPK(UInt32 epkAddress, string expectedEPK) { return EPKCheck(epkAddress, expectedEPK); }
    bool IDataFile.setEPK(UInt32 epkAddress, string epkToSet) { return base.setEPK(epkAddress, epkToSet); }
    bool IDataFile.save(string fileName, bool onlyDataSegments, byte dataBytesPerLine)
    {
      if (dataBytesPerLine < 1)
        throw new ArgumentException("Value too small", "dataBytesPerLine");

      if (mSegments == null || mSegments.Count == 0)
        // no data to save
        return true;
      bool turnAround = !BitConverter.IsLittleEndian;
      using (var sw = new StreamWriter(fileName))
      {
        for (int i = 0; i < mSegments.Count; ++i)
        {
          var segment = mSegments[i];
          if (!segment.IsInitialized)
            continue;
          if (onlyDataSegments && !segment.IsDataSegment)
            continue;

          var address = segment.Address;
          UInt16 segAddress = turnAround ? Extensions.changeEndianess((UInt16)(address >> 16)) : (UInt16)(address >> 16);

          // build header
          var segBuffer = new byte[] { 2, 0, 0
            , (byte)RecordType.ExtendedLinearAddr
            , (byte)((segAddress & 0xff00)>>8)
            , (byte)((segAddress & 0xff))
            };
          writeBytes(sw, ref segBuffer);

          var startAddress = segment.Address;
          for (int j = 0; j < segment.Size; j += dataBytesPerLine, address += dataBytesPerLine)
          {
            if (j > 0 && address % 0x10000 == 0)
            { // new segment (64k)
              startAddress = address;
              segAddress = turnAround ? Extensions.changeEndianess((UInt16)(startAddress >> 16)) : (UInt16)(startAddress >> 16);
              segBuffer = new byte[] { 2, 0, 0
                , (byte)RecordType.ExtendedLinearAddr
                , (byte)((segAddress & 0xff00)>>8)
                , (byte)((segAddress & 0xff))
                };
              writeBytes(sw, ref segBuffer);
            }

            // write data
            int len = Math.Min(segment.Size - j, dataBytesPerLine);
            var buffer = getData2Write(turnAround, address, segment.Data, len, (int)(address - segment.Address));
            writeBytes(sw, ref buffer);
          }
        }
        sw.WriteLine(mEOFFile);
      }
      IsDirty = false;
      ChangedValues.Clear();
      SourceFilename = fileName;
      return true;
    }
    bool IDataFile.save(string fileName, bool onlyDataSegments)
    {
      return ((IDataFile)this).save(fileName, onlyDataSegments, mDataBytesPerLine);
    }
    void IDataFile.import(ICharValue[] values) { DataFile.import(this, values); }
    bool IDataFile.reLoad() { return load(SourceFilename); }
    void IDataFile.setSegmentsData(MemorySegmentList segments) { base.setSegmentsData(segments); }
    /// <summary>
    /// Write a Intel HEX file data line.
    /// </summary>
    /// <param name="sw">The stream to write to</param>
    /// <param name="buffer">The data to write</param>
    static void writeBytes(StreamWriter sw, ref byte[] buffer)
    {
      UInt16 chksum = 0;
      sw.Write(":");
      for (int i = 0; i < buffer.Length; ++i)
      {
        byte b = buffer[i];
        chksum += b;
        sw.Write(mStrByteFormat, b);
      }
      sw.WriteLine(mStrByteFormat, (byte)(0x100 - (chksum & 0xff)));
    }
#if WIN32_OPTIMIZED
    /// <summary>
    /// Builds a Intel HEX data line from the specified arguments.
    /// </summary>
    /// <param name="adjustByteOrder">true if byte order should adjusted (for big endian host systems)</param>
    /// <param name="address">The data's address</param>
    /// <param name="data">The data</param>
    /// <param name="len">The data length</param>
    /// <param name="offset">The offset into the data</param>
    /// <returns>A new instance of a byte buffer</returns>
    static byte[] getData2Write(bool adjustByteOrder, UInt32 address, IntPtr data, int len, int offset)
    {
      UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess((UInt16)address) : (UInt16)address;
      int dataLen = 4 + len;
      var buffer = new byte[dataLen];
      buffer[0] = (byte)len;
      buffer[1] = (byte)((wrAddress & 0xff00) >> 8);
      buffer[2] = (byte)(wrAddress & 0xff);
      buffer[3] = (byte)RecordType.Data;
      Marshal.Copy(IntPtr.Add(data, offset), buffer, 4, len);
      return buffer;
    }
#else
    /// <summary>
    /// Builds a Intel HEX data line from the specified arguments.
    /// </summary>
    /// <param name="adjustByteOrder">true if byte order should adjusted (for big endian host systems)</param>
    /// <param name="address">The data's address</param>
    /// <param name="data">The data</param>
    /// <param name="len">The data length</param>
    /// <param name="offset">The offset into the data</param>
    /// <returns>A new instance of a byte buffer</returns>
    static byte[] getData2Write(bool adjustByteOrder, UInt32 address, byte[] data, int len, int offset)
    {
      UInt32 wrAddress = adjustByteOrder ? Extensions.changeEndianess((UInt16)address) : (UInt16)address;
      int dataLen = 4 + len;
      var buffer = new byte[dataLen];
      buffer[0] = (byte)len;
      buffer[1] = (byte)((wrAddress & 0xff00) >> 8);
      buffer[2] = (byte)(wrAddress & 0xff);
      buffer[3] = (byte)RecordType.Data;
      Array.Copy(data, offset, buffer, 4, len);
      return buffer;
    }
#endif
    /// <summary>
    /// Load the contents of an Intel Hex File into memory.
    /// </summary>
    /// <param name="fileName">Fully qualified path to the Intel Hex File to read.</param>
    /// <returns>true if successful.</returns>
    protected override bool load(string fileName)
    {
      using (var reader = new StreamReader(fileName))
      { // iterate over lines.
        try
        {
          var partList = new List<DataPart>();
          string line;
          const int addressOffset = 3;
          const int typeOffset = addressOffset + 4;
          const int dataOffset = typeOffset + 2;
          UInt32 baseAddress = 0;
          while (null != (line = reader.ReadLine()))
          { // Convert line into data
            line = line.Trim(mLineTrim);
            if (string.IsNullOrEmpty(line) || line.Length <= typeOffset || line[0] != ':')
              // not a record
              continue;

            UInt16 chksum = 0;
            byte length = toByte(ref line, 1);
            if (0 == length)
              // eof
              continue;
            chksum += length;

            RecordType recordType;
            try
            {
              byte type = toByte(ref line, typeOffset);
              chksum += type;
              recordType = (RecordType)type;
            }
            catch
            { // invalid record
              continue;
            }

            byte b1 = 0;
            byte b2 = 0;
            switch (recordType)
            {
              case RecordType.ExtendedSegmentAddr:
                b1 = toByte(ref line, dataOffset);
                b2 = toByte(ref line, dataOffset + 2);
                baseAddress = (UInt32)(b1 << 12) + ((UInt32)(b2) << 4);
                continue;
              case RecordType.StartSegmentAddr:
              case RecordType.StartLinearAddr:
                b1 = toByte(ref line, dataOffset);
                b2 = toByte(ref line, dataOffset + 2);
                byte b3 = toByte(ref line, dataOffset + 4);
                byte b4 = toByte(ref line, dataOffset + 6);
                baseAddress = ((UInt32)(b1) + ((UInt32)(b2) << 16) + (UInt32)(b3 << 8) + (UInt32)b4);
                continue;
              case RecordType.ExtendedLinearAddr:
                b1 = toByte(ref line, dataOffset);
                b2 = toByte(ref line, dataOffset + 2);
                baseAddress = ((UInt32)(b1) << 24) + ((UInt32)(b2) << 16);
                continue;
              case RecordType.Data:
                b1 = toByte(ref line, addressOffset);
                b2 = toByte(ref line, addressOffset + 2);
                chksum += b1;
                chksum += b2;

                UInt32 address = baseAddress + ((UInt32)(b1) << 8) + (UInt32)b2;

                var data = new byte[length];
                int max = dataOffset + 2 * data.Length;
                for (int i = dataOffset, j = 0; i < max; i += 2, ++j)
                {
                  byte b = toByte(ref line, i);
                  data[j] = b;
                  chksum += b;
                }

                byte chk = (byte)(0x100 - (chksum & 0xff));
                if (chk != toByte(ref line, line.Length - 2))
                  return false;

                partList.Add(new DataPart(address, ref data));
                break;
            }
          }
          partList.Sort(compare);
          createAndMergeMemSegs(ref partList);
          return partList.Count > 0;
        }
        catch { return false; }
      }
    }
    #endregion
    #region clone members
    /// <summary>
    /// Provides implementation of the IClone member.
    /// </summary>
    /// <returns>New instance of class as a copy of the original.</returns>
    public override object Clone()
    {
      return new DataFileHex(SourceFilename, mSegments);
    }
    #endregion
  }
}
