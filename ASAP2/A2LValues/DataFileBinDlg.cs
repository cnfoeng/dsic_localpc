﻿/*!
 * @file    
 * @brief   Dialog to get a start address for a data binary file.
 * @author  Joe Nachbaur
 * @author  E-mail: info@jnachbaur.de
 * @date    July 2014
 * @version 1.0
 *
 * <HR>
 * Copyright 2011-2018 JNachbaur. All rights reserved.
 *
 * Redistributions in binary form must reproduce the above copyright notice.
 *
 * Redistributions of source code is not permitted without express 
 * written permission by the author.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT,INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <HR>
 */
using jnsoft.ASAP2.Properties;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace jnsoft.ASAP2.Values
{
  /// <summary>
  /// Dialog to get a start address for a data binary file.
  /// </summary>
  public sealed partial class DataFileBinDlg : Form
  {
    #region members
    MemorySegmentList mSegments;
    UInt32 mAddress;
    #endregion
    #region ctors
    DataFileBinDlg()
    {
      InitializeComponent();
    }
    /// <summary>
    /// Creates a new instance of DataFileBinDlg.
    /// </summary>
    /// <param name="segments">The A2L defined emory segments (may be empty)</param>
    public DataFileBinDlg(MemorySegmentList segments) : this()
    {
      Icon = SystemIcons.Question;
      mSegments = segments;
      foreach (var segment in mSegments)
        cbAddress.Items.Add($"0x{segment.Address:X} {segment.PrgType}");

      if (Settings.Default.DataFileBinValue != long.MaxValue)
        setText($"0x{Settings.Default.DataFileBinValue:X}");
      else if (cbAddress.Items.Count > 0)
        setText((string)cbAddress.Items[0]);

      btnOk.Enabled = isInputValid();
    }
    #endregion
    #region properties
    /// <summary>
    /// Gets the user selected address.
    /// </summary>
    public UInt32 Address { get { return mAddress; } }
    #endregion
    #region methods
    void setText(string itemText)
    {
      cbAddress.Text = cbAddress.SelectedText = itemText;
    }

    void cbAddress_TextUpdate(object sender, EventArgs e)
    {
      btnOk.Enabled = isInputValid();
    }

    /// <summary>
    /// Calle when the dialog gets closed.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnFormClosing(FormClosingEventArgs e)
    {
      base.OnFormClosing(e);
      if (DialogResult == DialogResult.OK)
        e.Cancel = !isInputValid();
    }

    /// <summary>
    /// Test for valid input data (an UInt32 hexadcimal address).
    /// </summary>
    /// <returns>ture if the input data is valid</returns>
    bool isInputValid()
    {
      string s = cbAddress.Text.Replace("x", string.Empty).Replace("X", string.Empty);
      if (string.IsNullOrEmpty(s))
        return false;
      int end = s.Length;
      int blankIdx = s.IndexOf(' ');
      if (blankIdx > -1)
        end = blankIdx;
      s = s.Substring(0, end);
      bool result = UInt32.TryParse(s, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out mAddress);
      if (result)
        Settings.Default.DataFileBinValue = mAddress;
      return result;
    }
    #endregion
  }
}
